/*
               File: PromptContagemResultadoContagens
        Description: Selecione Contagem Resultado Contagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:2:21.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontagemresultadocontagens : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontagemresultadocontagens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontagemresultadocontagens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContagemResultado_Codigo ,
                           ref DateTime aP1_InOutContagemResultado_DataCnt ,
                           ref String aP2_InOutContagemResultado_HoraCnt ,
                           ref decimal aP3_InOutContagemResultado_PFBFS )
      {
         this.AV7InOutContagemResultado_Codigo = aP0_InOutContagemResultado_Codigo;
         this.AV8InOutContagemResultado_DataCnt = aP1_InOutContagemResultado_DataCnt;
         this.AV38InOutContagemResultado_HoraCnt = aP2_InOutContagemResultado_HoraCnt;
         this.AV9InOutContagemResultado_PFBFS = aP3_InOutContagemResultado_PFBFS;
         executePrivate();
         aP0_InOutContagemResultado_Codigo=this.AV7InOutContagemResultado_Codigo;
         aP1_InOutContagemResultado_DataCnt=this.AV8InOutContagemResultado_DataCnt;
         aP2_InOutContagemResultado_HoraCnt=this.AV38InOutContagemResultado_HoraCnt;
         aP3_InOutContagemResultado_PFBFS=this.AV9InOutContagemResultado_PFBFS;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbContagemResultado_StatusCnt = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_86 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_86_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_86_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
               AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV32ContagemResultado_DataCnt1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_DataCnt1", context.localUtil.Format(AV32ContagemResultado_DataCnt1, "99/99/99"));
               AV33ContagemResultado_DataCnt_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataCnt_To1", context.localUtil.Format(AV33ContagemResultado_DataCnt_To1, "99/99/99"));
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV34ContagemResultado_DataCnt2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_DataCnt2", context.localUtil.Format(AV34ContagemResultado_DataCnt2, "99/99/99"));
               AV35ContagemResultado_DataCnt_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultado_DataCnt_To2", context.localUtil.Format(AV35ContagemResultado_DataCnt_To2, "99/99/99"));
               AV24DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
               AV36ContagemResultado_DataCnt3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_DataCnt3", context.localUtil.Format(AV36ContagemResultado_DataCnt3, "99/99/99"));
               AV37ContagemResultado_DataCnt_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultado_DataCnt_To3", context.localUtil.Format(AV37ContagemResultado_DataCnt_To3, "99/99/99"));
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV23DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
               AV40TFContagemResultado_DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultado_DataCnt", context.localUtil.Format(AV40TFContagemResultado_DataCnt, "99/99/99"));
               AV41TFContagemResultado_DataCnt_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContagemResultado_DataCnt_To", context.localUtil.Format(AV41TFContagemResultado_DataCnt_To, "99/99/99"));
               AV46TFContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContagemResultado_ContadorFMCod), 6, 0)));
               AV47TFContagemResultado_ContadorFMCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContagemResultado_ContadorFMCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContagemResultado_ContadorFMCod_To), 6, 0)));
               AV50TFContagemResultado_PFBFS = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV50TFContagemResultado_PFBFS, 14, 5)));
               AV51TFContagemResultado_PFBFS_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContagemResultado_PFBFS_To", StringUtil.LTrim( StringUtil.Str( AV51TFContagemResultado_PFBFS_To, 14, 5)));
               AV54TFContagemResultado_PFLFS = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV54TFContagemResultado_PFLFS, 14, 5)));
               AV55TFContagemResultado_PFLFS_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContagemResultado_PFLFS_To", StringUtil.LTrim( StringUtil.Str( AV55TFContagemResultado_PFLFS_To, 14, 5)));
               AV58TFContagemResultado_PFBFM = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV58TFContagemResultado_PFBFM, 14, 5)));
               AV59TFContagemResultado_PFBFM_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemResultado_PFBFM_To", StringUtil.LTrim( StringUtil.Str( AV59TFContagemResultado_PFBFM_To, 14, 5)));
               AV62TFContagemResultado_PFLFM = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV62TFContagemResultado_PFLFM, 14, 5)));
               AV63TFContagemResultado_PFLFM_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContagemResultado_PFLFM_To", StringUtil.LTrim( StringUtil.Str( AV63TFContagemResultado_PFLFM_To, 14, 5)));
               AV66TFContagemResultado_Divergencia = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV66TFContagemResultado_Divergencia, 6, 2)));
               AV67TFContagemResultado_Divergencia_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContagemResultado_Divergencia_To", StringUtil.LTrim( StringUtil.Str( AV67TFContagemResultado_Divergencia_To, 6, 2)));
               AV70TFContagemResultado_ParecerTcn = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultado_ParecerTcn", AV70TFContagemResultado_ParecerTcn);
               AV71TFContagemResultado_ParecerTcn_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_ParecerTcn_Sel", AV71TFContagemResultado_ParecerTcn_Sel);
               AV74TFContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74TFContagemResultado_NaoCnfCntCod), 6, 0)));
               AV75TFContagemResultado_NaoCnfCntCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultado_NaoCnfCntCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFContagemResultado_NaoCnfCntCod_To), 6, 0)));
               AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace", AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace);
               AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace", AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace);
               AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace", AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace);
               AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace", AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace);
               AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace", AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace);
               AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace", AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace);
               AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace", AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace);
               AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace", AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace);
               AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace", AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace);
               AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace", AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV79TFContagemResultado_StatusCnt_Sels);
               AV88Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
               AV28DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
               AV27DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultado_DataCnt1, AV33ContagemResultado_DataCnt_To1, AV20DynamicFiltersSelector2, AV34ContagemResultado_DataCnt2, AV35ContagemResultado_DataCnt_To2, AV24DynamicFiltersSelector3, AV36ContagemResultado_DataCnt3, AV37ContagemResultado_DataCnt_To3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV40TFContagemResultado_DataCnt, AV41TFContagemResultado_DataCnt_To, AV46TFContagemResultado_ContadorFMCod, AV47TFContagemResultado_ContadorFMCod_To, AV50TFContagemResultado_PFBFS, AV51TFContagemResultado_PFBFS_To, AV54TFContagemResultado_PFLFS, AV55TFContagemResultado_PFLFS_To, AV58TFContagemResultado_PFBFM, AV59TFContagemResultado_PFBFM_To, AV62TFContagemResultado_PFLFM, AV63TFContagemResultado_PFLFM_To, AV66TFContagemResultado_Divergencia, AV67TFContagemResultado_Divergencia_To, AV70TFContagemResultado_ParecerTcn, AV71TFContagemResultado_ParecerTcn_Sel, AV74TFContagemResultado_NaoCnfCntCod, AV75TFContagemResultado_NaoCnfCntCod_To, AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace, AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace, AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace, AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace, AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace, AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace, AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace, AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace, AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace, AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace, AV79TFContagemResultado_StatusCnt_Sels, AV88Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PromptContagemResultadoContagens";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, ""));
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("promptcontagemresultadocontagens:[SendSecurityCheck value for]"+"ContagemResultado_HoraCnt:"+StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, "")));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemResultado_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContagemResultado_DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultado_DataCnt", context.localUtil.Format(AV8InOutContagemResultado_DataCnt, "99/99/99"));
                  AV38InOutContagemResultado_HoraCnt = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38InOutContagemResultado_HoraCnt", AV38InOutContagemResultado_HoraCnt);
                  AV9InOutContagemResultado_PFBFS = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV9InOutContagemResultado_PFBFS, 14, 5)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAB92( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV88Pgmname = "PromptContagemResultadoContagens";
               context.Gx_err = 0;
               WSB92( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEB92( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205181322197");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontagemresultadocontagens.aspx") + "?" + UrlEncode("" +AV7InOutContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV8InOutContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(AV38InOutContagemResultado_HoraCnt)) + "," + UrlEncode(StringUtil.Str(AV9InOutContagemResultado_PFBFS,14,5))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATACNT1", context.localUtil.Format(AV32ContagemResultado_DataCnt1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATACNT_TO1", context.localUtil.Format(AV33ContagemResultado_DataCnt_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATACNT2", context.localUtil.Format(AV34ContagemResultado_DataCnt2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATACNT_TO2", context.localUtil.Format(AV35ContagemResultado_DataCnt_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV24DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATACNT3", context.localUtil.Format(AV36ContagemResultado_DataCnt3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATACNT_TO3", context.localUtil.Format(AV37ContagemResultado_DataCnt_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV23DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DATACNT", context.localUtil.Format(AV40TFContagemResultado_DataCnt, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DATACNT_TO", context.localUtil.Format(AV41TFContagemResultado_DataCnt_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_CONTADORFMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFContagemResultado_ContadorFMCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFContagemResultado_ContadorFMCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_PFBFS", StringUtil.LTrim( StringUtil.NToC( AV50TFContagemResultado_PFBFS, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_PFBFS_TO", StringUtil.LTrim( StringUtil.NToC( AV51TFContagemResultado_PFBFS_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_PFLFS", StringUtil.LTrim( StringUtil.NToC( AV54TFContagemResultado_PFLFS, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_PFLFS_TO", StringUtil.LTrim( StringUtil.NToC( AV55TFContagemResultado_PFLFS_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_PFBFM", StringUtil.LTrim( StringUtil.NToC( AV58TFContagemResultado_PFBFM, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_PFBFM_TO", StringUtil.LTrim( StringUtil.NToC( AV59TFContagemResultado_PFBFM_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_PFLFM", StringUtil.LTrim( StringUtil.NToC( AV62TFContagemResultado_PFLFM, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_PFLFM_TO", StringUtil.LTrim( StringUtil.NToC( AV63TFContagemResultado_PFLFM_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DIVERGENCIA", StringUtil.LTrim( StringUtil.NToC( AV66TFContagemResultado_Divergencia, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DIVERGENCIA_TO", StringUtil.LTrim( StringUtil.NToC( AV67TFContagemResultado_Divergencia_To, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_PARECERTCN", AV70TFContagemResultado_ParecerTcn);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_PARECERTCN_SEL", AV71TFContagemResultado_ParecerTcn_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_NAOCNFCNTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74TFContagemResultado_NaoCnfCntCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV75TFContagemResultado_NaoCnfCntCod_To), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_86", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_86), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV84GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV81DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV81DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_DATACNTTITLEFILTERDATA", AV39ContagemResultado_DataCntTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_DATACNTTITLEFILTERDATA", AV39ContagemResultado_DataCntTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_CONTADORFMCODTITLEFILTERDATA", AV45ContagemResultado_ContadorFMCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_CONTADORFMCODTITLEFILTERDATA", AV45ContagemResultado_ContadorFMCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_PFBFSTITLEFILTERDATA", AV49ContagemResultado_PFBFSTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_PFBFSTITLEFILTERDATA", AV49ContagemResultado_PFBFSTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_PFLFSTITLEFILTERDATA", AV53ContagemResultado_PFLFSTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_PFLFSTITLEFILTERDATA", AV53ContagemResultado_PFLFSTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_PFBFMTITLEFILTERDATA", AV57ContagemResultado_PFBFMTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_PFBFMTITLEFILTERDATA", AV57ContagemResultado_PFBFMTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_PFLFMTITLEFILTERDATA", AV61ContagemResultado_PFLFMTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_PFLFMTITLEFILTERDATA", AV61ContagemResultado_PFLFMTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_DIVERGENCIATITLEFILTERDATA", AV65ContagemResultado_DivergenciaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_DIVERGENCIATITLEFILTERDATA", AV65ContagemResultado_DivergenciaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_PARECERTCNTITLEFILTERDATA", AV69ContagemResultado_ParecerTcnTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_PARECERTCNTITLEFILTERDATA", AV69ContagemResultado_ParecerTcnTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_NAOCNFCNTCODTITLEFILTERDATA", AV73ContagemResultado_NaoCnfCntCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_NAOCNFCNTCODTITLEFILTERDATA", AV73ContagemResultado_NaoCnfCntCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_STATUSCNTTITLEFILTERDATA", AV77ContagemResultado_StatusCntTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_STATUSCNTTITLEFILTERDATA", AV77ContagemResultado_StatusCntTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFCONTAGEMRESULTADO_STATUSCNT_SELS", AV79TFContagemResultado_StatusCnt_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFCONTAGEMRESULTADO_STATUSCNT_SELS", AV79TFContagemResultado_StatusCnt_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV88Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV28DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV27DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADO_DATACNT", context.localUtil.DToC( AV8InOutContagemResultado_DataCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADO_HORACNT", StringUtil.RTrim( AV38InOutContagemResultado_HoraCnt));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADO_PFBFS", StringUtil.LTrim( StringUtil.NToC( AV9InOutContagemResultado_PFBFS, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Caption", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Cls", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_datacnt_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_datacnt_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_datacnt_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_datacnt_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_datacnt_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Caption", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Cls", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_contadorfmcod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_contadorfmcod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_contadorfmcod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_contadorfmcod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_contadorfmcod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Caption", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Cls", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_pfbfs_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_pfbfs_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_pfbfs_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_pfbfs_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_pfbfs_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Caption", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Cls", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_pflfs_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_pflfs_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_pflfs_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_pflfs_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_pflfs_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Caption", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Cls", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_pfbfm_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_pfbfm_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_pfbfm_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_pfbfm_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_pfbfm_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Caption", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Cls", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_pflfm_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_pflfm_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_pflfm_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_pflfm_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_pflfm_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Caption", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Cls", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_divergencia_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_divergencia_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_divergencia_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_divergencia_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_divergencia_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Caption", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Cls", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_parecertcn_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_parecertcn_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_parecertcn_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_parecertcn_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_parecertcn_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_parecertcn_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Caption", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Cls", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_naocnfcntcod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_naocnfcntcod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_naocnfcntcod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_naocnfcntcod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_naocnfcntcod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Caption", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Cls", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_statuscnt_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_statuscnt_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_statuscnt_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_statuscnt_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contagemresultado_statuscnt_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATACNT_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultado_datacnt_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultado_contadorfmcod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFS_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultado_pfbfs_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFS_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultado_pflfs_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFBFM_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultado_pfbfm_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PFLFM_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultado_pflfm_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultado_divergencia_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_PARECERTCN_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_parecertcn_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultado_naocnfcntcod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSCNT_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_statuscnt_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PromptContagemResultadoContagens";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, ""));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("promptcontagemresultadocontagens:[SendSecurityCheck value for]"+"ContagemResultado_HoraCnt:"+StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, "")));
      }

      protected void RenderHtmlCloseFormB92( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContagemResultadoContagens" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contagem Resultado Contagens" ;
      }

      protected void WBB90( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_B92( true) ;
         }
         else
         {
            wb_table1_2_B92( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_B92e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_HoraCnt_Internalname, StringUtil.RTrim( A511ContagemResultado_HoraCnt), StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_HoraCnt_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultado_HoraCnt_Visible, 0, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoContagens.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(104, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV23DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(105, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultado_datacnt_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_datacnt_Internalname, context.localUtil.Format(AV40TFContagemResultado_DataCnt, "99/99/99"), context.localUtil.Format( AV40TFContagemResultado_DataCnt, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_datacnt_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_datacnt_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultado_datacnt_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultado_datacnt_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultado_datacnt_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_datacnt_to_Internalname, context.localUtil.Format(AV41TFContagemResultado_DataCnt_To, "99/99/99"), context.localUtil.Format( AV41TFContagemResultado_DataCnt_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_datacnt_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_datacnt_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultado_datacnt_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultado_datacnt_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultado_datacntauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultado_datacntauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultado_datacntauxdate_Internalname, context.localUtil.Format(AV42DDO_ContagemResultado_DataCntAuxDate, "99/99/99"), context.localUtil.Format( AV42DDO_ContagemResultado_DataCntAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultado_datacntauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultado_datacntauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultado_datacntauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultado_datacntauxdateto_Internalname, context.localUtil.Format(AV43DDO_ContagemResultado_DataCntAuxDateTo, "99/99/99"), context.localUtil.Format( AV43DDO_ContagemResultado_DataCntAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultado_datacntauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultado_datacntauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_contadorfmcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFContagemResultado_ContadorFMCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46TFContagemResultado_ContadorFMCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_contadorfmcod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_contadorfmcod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_contadorfmcod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFContagemResultado_ContadorFMCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV47TFContagemResultado_ContadorFMCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_contadorfmcod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_contadorfmcod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_pfbfs_Internalname, StringUtil.LTrim( StringUtil.NToC( AV50TFContagemResultado_PFBFS, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV50TFContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_pfbfs_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_pfbfs_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_pfbfs_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV51TFContagemResultado_PFBFS_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV51TFContagemResultado_PFBFS_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_pfbfs_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_pfbfs_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_pflfs_Internalname, StringUtil.LTrim( StringUtil.NToC( AV54TFContagemResultado_PFLFS, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV54TFContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_pflfs_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_pflfs_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_pflfs_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV55TFContagemResultado_PFLFS_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV55TFContagemResultado_PFLFS_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_pflfs_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_pflfs_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_pfbfm_Internalname, StringUtil.LTrim( StringUtil.NToC( AV58TFContagemResultado_PFBFM, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV58TFContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_pfbfm_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_pfbfm_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_pfbfm_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV59TFContagemResultado_PFBFM_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV59TFContagemResultado_PFBFM_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_pfbfm_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_pfbfm_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_pflfm_Internalname, StringUtil.LTrim( StringUtil.NToC( AV62TFContagemResultado_PFLFM, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV62TFContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_pflfm_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_pflfm_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_pflfm_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV63TFContagemResultado_PFLFM_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV63TFContagemResultado_PFLFM_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_pflfm_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_pflfm_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_divergencia_Internalname, StringUtil.LTrim( StringUtil.NToC( AV66TFContagemResultado_Divergencia, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV66TFContagemResultado_Divergencia, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_divergencia_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_divergencia_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_divergencia_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV67TFContagemResultado_Divergencia_To, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV67TFContagemResultado_Divergencia_To, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_divergencia_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_divergencia_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontagemresultado_parecertcn_Internalname, AV70TFContagemResultado_ParecerTcn, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", 0, edtavTfcontagemresultado_parecertcn_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoContagens.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontagemresultado_parecertcn_sel_Internalname, AV71TFContagemResultado_ParecerTcn_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", 0, edtavTfcontagemresultado_parecertcn_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_naocnfcntcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74TFContagemResultado_NaoCnfCntCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV74TFContagemResultado_NaoCnfCntCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_naocnfcntcod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_naocnfcntcod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_naocnfcntcod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV75TFContagemResultado_NaoCnfCntCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV75TFContagemResultado_NaoCnfCntCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_naocnfcntcod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_naocnfcntcod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_DATACNTContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_datacnttitlecontrolidtoreplace_Internalname, AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", 0, edtavDdo_contagemresultado_datacnttitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoContagens.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_CONTADORFMCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_contadorfmcodtitlecontrolidtoreplace_Internalname, AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"", 0, edtavDdo_contagemresultado_contadorfmcodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoContagens.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_PFBFSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_pfbfstitlecontrolidtoreplace_Internalname, AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,132);\"", 0, edtavDdo_contagemresultado_pfbfstitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoContagens.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_PFLFSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_pflfstitlecontrolidtoreplace_Internalname, AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", 0, edtavDdo_contagemresultado_pflfstitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoContagens.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_PFBFMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_pfbfmtitlecontrolidtoreplace_Internalname, AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", 0, edtavDdo_contagemresultado_pfbfmtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoContagens.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_PFLFMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_pflfmtitlecontrolidtoreplace_Internalname, AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,138);\"", 0, edtavDdo_contagemresultado_pflfmtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoContagens.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_DIVERGENCIAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_divergenciatitlecontrolidtoreplace_Internalname, AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,140);\"", 0, edtavDdo_contagemresultado_divergenciatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoContagens.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_PARECERTCNContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_parecertcntitlecontrolidtoreplace_Internalname, AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,142);\"", 0, edtavDdo_contagemresultado_parecertcntitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoContagens.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_NAOCNFCNTCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_naocnfcntcodtitlecontrolidtoreplace_Internalname, AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,144);\"", 0, edtavDdo_contagemresultado_naocnfcntcodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoContagens.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_STATUSCNTContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_statuscnttitlecontrolidtoreplace_Internalname, AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,146);\"", 0, edtavDdo_contagemresultado_statuscnttitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoContagens.htm");
         }
         wbLoad = true;
      }

      protected void STARTB92( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contagem Resultado Contagens", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPB90( ) ;
      }

      protected void WSB92( )
      {
         STARTB92( ) ;
         EVTB92( ) ;
      }

      protected void EVTB92( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11B92 */
                           E11B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_DATACNT.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12B92 */
                           E12B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_CONTADORFMCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13B92 */
                           E13B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_PFBFS.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14B92 */
                           E14B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_PFLFS.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15B92 */
                           E15B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_PFBFM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16B92 */
                           E16B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_PFLFM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17B92 */
                           E17B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_DIVERGENCIA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18B92 */
                           E18B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_PARECERTCN.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19B92 */
                           E19B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20B92 */
                           E20B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_STATUSCNT.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21B92 */
                           E21B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22B92 */
                           E22B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23B92 */
                           E23B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24B92 */
                           E24B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25B92 */
                           E25B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26B92 */
                           E26B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E27B92 */
                           E27B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E28B92 */
                           E28B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E29B92 */
                           E29B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E30B92 */
                           E30B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E31B92 */
                           E31B92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_86_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
                           SubsflControlProps_862( ) ;
                           AV29Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Select)) ? AV87Select_GXI : context.convertURL( context.PathToRelativeUrl( AV29Select))));
                           A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                           A473ContagemResultado_DataCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataCnt_Internalname), 0));
                           A470ContagemResultado_ContadorFMCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_ContadorFMCod_Internalname), ",", "."));
                           A458ContagemResultado_PFBFS = context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFS_Internalname), ",", ".");
                           n458ContagemResultado_PFBFS = false;
                           A459ContagemResultado_PFLFS = context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFS_Internalname), ",", ".");
                           n459ContagemResultado_PFLFS = false;
                           A460ContagemResultado_PFBFM = context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFM_Internalname), ",", ".");
                           n460ContagemResultado_PFBFM = false;
                           A461ContagemResultado_PFLFM = context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFM_Internalname), ",", ".");
                           n461ContagemResultado_PFLFM = false;
                           A462ContagemResultado_Divergencia = context.localUtil.CToN( cgiGet( edtContagemResultado_Divergencia_Internalname), ",", ".");
                           A463ContagemResultado_ParecerTcn = cgiGet( edtContagemResultado_ParecerTcn_Internalname);
                           n463ContagemResultado_ParecerTcn = false;
                           A469ContagemResultado_NaoCnfCntCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_NaoCnfCntCod_Internalname), ",", "."));
                           n469ContagemResultado_NaoCnfCntCod = false;
                           cmbContagemResultado_StatusCnt.Name = cmbContagemResultado_StatusCnt_Internalname;
                           cmbContagemResultado_StatusCnt.CurrentValue = cgiGet( cmbContagemResultado_StatusCnt_Internalname);
                           A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cgiGet( cmbContagemResultado_StatusCnt_Internalname), "."));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E32B92 */
                                 E32B92 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E33B92 */
                                 E33B92 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E34B92 */
                                 E34B92 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_datacnt1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATACNT1"), 0) != AV32ContagemResultado_DataCnt1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_datacnt_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATACNT_TO1"), 0) != AV33ContagemResultado_DataCnt_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_datacnt2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATACNT2"), 0) != AV34ContagemResultado_DataCnt2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_datacnt_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATACNT_TO2"), 0) != AV35ContagemResultado_DataCnt_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_datacnt3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATACNT3"), 0) != AV36ContagemResultado_DataCnt3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_datacnt_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATACNT_TO3"), 0) != AV37ContagemResultado_DataCnt_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_datacnt Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATACNT"), 0) != AV40TFContagemResultado_DataCnt )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_datacnt_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATACNT_TO"), 0) != AV41TFContagemResultado_DataCnt_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_contadorfmcod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_CONTADORFMCOD"), ",", ".") != Convert.ToDecimal( AV46TFContagemResultado_ContadorFMCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_contadorfmcod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO"), ",", ".") != Convert.ToDecimal( AV47TFContagemResultado_ContadorFMCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_pfbfs Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFBFS"), ",", ".") != AV50TFContagemResultado_PFBFS )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_pfbfs_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFBFS_TO"), ",", ".") != AV51TFContagemResultado_PFBFS_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_pflfs Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFLFS"), ",", ".") != AV54TFContagemResultado_PFLFS )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_pflfs_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFLFS_TO"), ",", ".") != AV55TFContagemResultado_PFLFS_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_pfbfm Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFBFM"), ",", ".") != AV58TFContagemResultado_PFBFM )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_pfbfm_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFBFM_TO"), ",", ".") != AV59TFContagemResultado_PFBFM_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_pflfm Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFLFM"), ",", ".") != AV62TFContagemResultado_PFLFM )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_pflfm_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFLFM_TO"), ",", ".") != AV63TFContagemResultado_PFLFM_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_divergencia Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DIVERGENCIA"), ",", ".") != AV66TFContagemResultado_Divergencia )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_divergencia_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DIVERGENCIA_TO"), ",", ".") != AV67TFContagemResultado_Divergencia_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_parecertcn Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_PARECERTCN"), AV70TFContagemResultado_ParecerTcn) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_parecertcn_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_PARECERTCN_SEL"), AV71TFContagemResultado_ParecerTcn_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_naocnfcntcod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_NAOCNFCNTCOD"), ",", ".") != Convert.ToDecimal( AV74TFContagemResultado_NaoCnfCntCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_naocnfcntcod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO"), ",", ".") != Convert.ToDecimal( AV75TFContagemResultado_NaoCnfCntCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E35B92 */
                                       E35B92 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEB92( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormB92( ) ;
            }
         }
      }

      protected void PAB92( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DATACNT", "Data da Contagem", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DATACNT", "Data da Contagem", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DATACNT", "Data da Contagem", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            }
            GXCCtl = "CONTAGEMRESULTADO_STATUSCNT_" + sGXsfl_86_idx;
            cmbContagemResultado_StatusCnt.Name = GXCCtl;
            cmbContagemResultado_StatusCnt.WebTags = "";
            cmbContagemResultado_StatusCnt.addItem("1", "Inicial", 0);
            cmbContagemResultado_StatusCnt.addItem("2", "Auditoria", 0);
            cmbContagemResultado_StatusCnt.addItem("3", "Negociac�o", 0);
            cmbContagemResultado_StatusCnt.addItem("4", "N�o Aprovada", 0);
            cmbContagemResultado_StatusCnt.addItem("5", "Entregue", 0);
            cmbContagemResultado_StatusCnt.addItem("6", "Pend�ncias", 0);
            cmbContagemResultado_StatusCnt.addItem("7", "Diverg�ncia", 0);
            cmbContagemResultado_StatusCnt.addItem("8", "Aprovada", 0);
            if ( cmbContagemResultado_StatusCnt.ItemCount > 0 )
            {
               A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cmbContagemResultado_StatusCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0))), "."));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_862( ) ;
         while ( nGXsfl_86_idx <= nRC_GXsfl_86 )
         {
            sendrow_862( ) ;
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       DateTime AV32ContagemResultado_DataCnt1 ,
                                       DateTime AV33ContagemResultado_DataCnt_To1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       DateTime AV34ContagemResultado_DataCnt2 ,
                                       DateTime AV35ContagemResultado_DataCnt_To2 ,
                                       String AV24DynamicFiltersSelector3 ,
                                       DateTime AV36ContagemResultado_DataCnt3 ,
                                       DateTime AV37ContagemResultado_DataCnt_To3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV23DynamicFiltersEnabled3 ,
                                       DateTime AV40TFContagemResultado_DataCnt ,
                                       DateTime AV41TFContagemResultado_DataCnt_To ,
                                       int AV46TFContagemResultado_ContadorFMCod ,
                                       int AV47TFContagemResultado_ContadorFMCod_To ,
                                       decimal AV50TFContagemResultado_PFBFS ,
                                       decimal AV51TFContagemResultado_PFBFS_To ,
                                       decimal AV54TFContagemResultado_PFLFS ,
                                       decimal AV55TFContagemResultado_PFLFS_To ,
                                       decimal AV58TFContagemResultado_PFBFM ,
                                       decimal AV59TFContagemResultado_PFBFM_To ,
                                       decimal AV62TFContagemResultado_PFLFM ,
                                       decimal AV63TFContagemResultado_PFLFM_To ,
                                       decimal AV66TFContagemResultado_Divergencia ,
                                       decimal AV67TFContagemResultado_Divergencia_To ,
                                       String AV70TFContagemResultado_ParecerTcn ,
                                       String AV71TFContagemResultado_ParecerTcn_Sel ,
                                       int AV74TFContagemResultado_NaoCnfCntCod ,
                                       int AV75TFContagemResultado_NaoCnfCntCod_To ,
                                       String AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace ,
                                       String AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace ,
                                       String AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace ,
                                       String AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace ,
                                       String AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace ,
                                       String AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace ,
                                       String AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace ,
                                       String AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace ,
                                       String AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace ,
                                       String AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace ,
                                       IGxCollection AV79TFContagemResultado_StatusCnt_Sels ,
                                       String AV88Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV28DynamicFiltersIgnoreFirst ,
                                       bool AV27DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFB92( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( "", A473ContagemResultado_DataCnt));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATACNT", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CONTADORFMCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A470ContagemResultado_ContadorFMCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTADORFMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_PFBFS", GetSecureSignedToken( "", context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFS", StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_PFLFS", GetSecureSignedToken( "", context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFLFS", StringUtil.LTrim( StringUtil.NToC( A459ContagemResultado_PFLFS, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_PFBFM", GetSecureSignedToken( "", context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFM", StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_PFLFM", GetSecureSignedToken( "", context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFLFM", StringUtil.LTrim( StringUtil.NToC( A461ContagemResultado_PFLFM, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DIVERGENCIA", GetSecureSignedToken( "", context.localUtil.Format( A462ContagemResultado_Divergencia, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DIVERGENCIA", StringUtil.LTrim( StringUtil.NToC( A462ContagemResultado_Divergencia, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_PARECERTCN", GetSecureSignedToken( "", A463ContagemResultado_ParecerTcn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PARECERTCN", A463ContagemResultado_ParecerTcn);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_NAOCNFCNTCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A469ContagemResultado_NaoCnfCntCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_NAOCNFCNTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_STATUSCNT", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSCNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A483ContagemResultado_StatusCnt), 2, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFB92( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV88Pgmname = "PromptContagemResultadoContagens";
         context.Gx_err = 0;
      }

      protected void RFB92( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 86;
         /* Execute user event: E33B92 */
         E33B92 ();
         nGXsfl_86_idx = 1;
         sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
         SubsflControlProps_862( ) ;
         nGXsfl_86_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_862( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A483ContagemResultado_StatusCnt ,
                                                 AV79TFContagemResultado_StatusCnt_Sels ,
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV32ContagemResultado_DataCnt1 ,
                                                 AV33ContagemResultado_DataCnt_To1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV34ContagemResultado_DataCnt2 ,
                                                 AV35ContagemResultado_DataCnt_To2 ,
                                                 AV23DynamicFiltersEnabled3 ,
                                                 AV24DynamicFiltersSelector3 ,
                                                 AV36ContagemResultado_DataCnt3 ,
                                                 AV37ContagemResultado_DataCnt_To3 ,
                                                 AV40TFContagemResultado_DataCnt ,
                                                 AV41TFContagemResultado_DataCnt_To ,
                                                 AV46TFContagemResultado_ContadorFMCod ,
                                                 AV47TFContagemResultado_ContadorFMCod_To ,
                                                 AV50TFContagemResultado_PFBFS ,
                                                 AV51TFContagemResultado_PFBFS_To ,
                                                 AV54TFContagemResultado_PFLFS ,
                                                 AV55TFContagemResultado_PFLFS_To ,
                                                 AV58TFContagemResultado_PFBFM ,
                                                 AV59TFContagemResultado_PFBFM_To ,
                                                 AV62TFContagemResultado_PFLFM ,
                                                 AV63TFContagemResultado_PFLFM_To ,
                                                 AV66TFContagemResultado_Divergencia ,
                                                 AV67TFContagemResultado_Divergencia_To ,
                                                 AV71TFContagemResultado_ParecerTcn_Sel ,
                                                 AV70TFContagemResultado_ParecerTcn ,
                                                 AV74TFContagemResultado_NaoCnfCntCod ,
                                                 AV75TFContagemResultado_NaoCnfCntCod_To ,
                                                 AV79TFContagemResultado_StatusCnt_Sels.Count ,
                                                 A473ContagemResultado_DataCnt ,
                                                 A470ContagemResultado_ContadorFMCod ,
                                                 A458ContagemResultado_PFBFS ,
                                                 A459ContagemResultado_PFLFS ,
                                                 A460ContagemResultado_PFBFM ,
                                                 A461ContagemResultado_PFLFM ,
                                                 A462ContagemResultado_Divergencia ,
                                                 A463ContagemResultado_ParecerTcn ,
                                                 A469ContagemResultado_NaoCnfCntCod ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                                 TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV70TFContagemResultado_ParecerTcn = StringUtil.Concat( StringUtil.RTrim( AV70TFContagemResultado_ParecerTcn), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultado_ParecerTcn", AV70TFContagemResultado_ParecerTcn);
            /* Using cursor H00B92 */
            pr_default.execute(0, new Object[] {AV32ContagemResultado_DataCnt1, AV33ContagemResultado_DataCnt_To1, AV34ContagemResultado_DataCnt2, AV35ContagemResultado_DataCnt_To2, AV36ContagemResultado_DataCnt3, AV37ContagemResultado_DataCnt_To3, AV40TFContagemResultado_DataCnt, AV41TFContagemResultado_DataCnt_To, AV46TFContagemResultado_ContadorFMCod, AV47TFContagemResultado_ContadorFMCod_To, AV50TFContagemResultado_PFBFS, AV51TFContagemResultado_PFBFS_To, AV54TFContagemResultado_PFLFS, AV55TFContagemResultado_PFLFS_To, AV58TFContagemResultado_PFBFM, AV59TFContagemResultado_PFBFM_To, AV62TFContagemResultado_PFLFM, AV63TFContagemResultado_PFLFM_To, AV66TFContagemResultado_Divergencia, AV67TFContagemResultado_Divergencia_To, lV70TFContagemResultado_ParecerTcn, AV71TFContagemResultado_ParecerTcn_Sel, AV74TFContagemResultado_NaoCnfCntCod, AV75TFContagemResultado_NaoCnfCntCod_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_86_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A511ContagemResultado_HoraCnt = H00B92_A511ContagemResultado_HoraCnt[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
               A483ContagemResultado_StatusCnt = H00B92_A483ContagemResultado_StatusCnt[0];
               A469ContagemResultado_NaoCnfCntCod = H00B92_A469ContagemResultado_NaoCnfCntCod[0];
               n469ContagemResultado_NaoCnfCntCod = H00B92_n469ContagemResultado_NaoCnfCntCod[0];
               A463ContagemResultado_ParecerTcn = H00B92_A463ContagemResultado_ParecerTcn[0];
               n463ContagemResultado_ParecerTcn = H00B92_n463ContagemResultado_ParecerTcn[0];
               A462ContagemResultado_Divergencia = H00B92_A462ContagemResultado_Divergencia[0];
               A461ContagemResultado_PFLFM = H00B92_A461ContagemResultado_PFLFM[0];
               n461ContagemResultado_PFLFM = H00B92_n461ContagemResultado_PFLFM[0];
               A460ContagemResultado_PFBFM = H00B92_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = H00B92_n460ContagemResultado_PFBFM[0];
               A459ContagemResultado_PFLFS = H00B92_A459ContagemResultado_PFLFS[0];
               n459ContagemResultado_PFLFS = H00B92_n459ContagemResultado_PFLFS[0];
               A458ContagemResultado_PFBFS = H00B92_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = H00B92_n458ContagemResultado_PFBFS[0];
               A470ContagemResultado_ContadorFMCod = H00B92_A470ContagemResultado_ContadorFMCod[0];
               A473ContagemResultado_DataCnt = H00B92_A473ContagemResultado_DataCnt[0];
               A456ContagemResultado_Codigo = H00B92_A456ContagemResultado_Codigo[0];
               /* Execute user event: E34B92 */
               E34B92 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 86;
            WBB90( ) ;
         }
         nGXsfl_86_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A483ContagemResultado_StatusCnt ,
                                              AV79TFContagemResultado_StatusCnt_Sels ,
                                              AV16DynamicFiltersSelector1 ,
                                              AV32ContagemResultado_DataCnt1 ,
                                              AV33ContagemResultado_DataCnt_To1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV34ContagemResultado_DataCnt2 ,
                                              AV35ContagemResultado_DataCnt_To2 ,
                                              AV23DynamicFiltersEnabled3 ,
                                              AV24DynamicFiltersSelector3 ,
                                              AV36ContagemResultado_DataCnt3 ,
                                              AV37ContagemResultado_DataCnt_To3 ,
                                              AV40TFContagemResultado_DataCnt ,
                                              AV41TFContagemResultado_DataCnt_To ,
                                              AV46TFContagemResultado_ContadorFMCod ,
                                              AV47TFContagemResultado_ContadorFMCod_To ,
                                              AV50TFContagemResultado_PFBFS ,
                                              AV51TFContagemResultado_PFBFS_To ,
                                              AV54TFContagemResultado_PFLFS ,
                                              AV55TFContagemResultado_PFLFS_To ,
                                              AV58TFContagemResultado_PFBFM ,
                                              AV59TFContagemResultado_PFBFM_To ,
                                              AV62TFContagemResultado_PFLFM ,
                                              AV63TFContagemResultado_PFLFM_To ,
                                              AV66TFContagemResultado_Divergencia ,
                                              AV67TFContagemResultado_Divergencia_To ,
                                              AV71TFContagemResultado_ParecerTcn_Sel ,
                                              AV70TFContagemResultado_ParecerTcn ,
                                              AV74TFContagemResultado_NaoCnfCntCod ,
                                              AV75TFContagemResultado_NaoCnfCntCod_To ,
                                              AV79TFContagemResultado_StatusCnt_Sels.Count ,
                                              A473ContagemResultado_DataCnt ,
                                              A470ContagemResultado_ContadorFMCod ,
                                              A458ContagemResultado_PFBFS ,
                                              A459ContagemResultado_PFLFS ,
                                              A460ContagemResultado_PFBFM ,
                                              A461ContagemResultado_PFLFM ,
                                              A462ContagemResultado_Divergencia ,
                                              A463ContagemResultado_ParecerTcn ,
                                              A469ContagemResultado_NaoCnfCntCod ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV70TFContagemResultado_ParecerTcn = StringUtil.Concat( StringUtil.RTrim( AV70TFContagemResultado_ParecerTcn), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultado_ParecerTcn", AV70TFContagemResultado_ParecerTcn);
         /* Using cursor H00B93 */
         pr_default.execute(1, new Object[] {AV32ContagemResultado_DataCnt1, AV33ContagemResultado_DataCnt_To1, AV34ContagemResultado_DataCnt2, AV35ContagemResultado_DataCnt_To2, AV36ContagemResultado_DataCnt3, AV37ContagemResultado_DataCnt_To3, AV40TFContagemResultado_DataCnt, AV41TFContagemResultado_DataCnt_To, AV46TFContagemResultado_ContadorFMCod, AV47TFContagemResultado_ContadorFMCod_To, AV50TFContagemResultado_PFBFS, AV51TFContagemResultado_PFBFS_To, AV54TFContagemResultado_PFLFS, AV55TFContagemResultado_PFLFS_To, AV58TFContagemResultado_PFBFM, AV59TFContagemResultado_PFBFM_To, AV62TFContagemResultado_PFLFM, AV63TFContagemResultado_PFLFM_To, AV66TFContagemResultado_Divergencia, AV67TFContagemResultado_Divergencia_To, lV70TFContagemResultado_ParecerTcn, AV71TFContagemResultado_ParecerTcn_Sel, AV74TFContagemResultado_NaoCnfCntCod, AV75TFContagemResultado_NaoCnfCntCod_To});
         GRID_nRecordCount = H00B93_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultado_DataCnt1, AV33ContagemResultado_DataCnt_To1, AV20DynamicFiltersSelector2, AV34ContagemResultado_DataCnt2, AV35ContagemResultado_DataCnt_To2, AV24DynamicFiltersSelector3, AV36ContagemResultado_DataCnt3, AV37ContagemResultado_DataCnt_To3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV40TFContagemResultado_DataCnt, AV41TFContagemResultado_DataCnt_To, AV46TFContagemResultado_ContadorFMCod, AV47TFContagemResultado_ContadorFMCod_To, AV50TFContagemResultado_PFBFS, AV51TFContagemResultado_PFBFS_To, AV54TFContagemResultado_PFLFS, AV55TFContagemResultado_PFLFS_To, AV58TFContagemResultado_PFBFM, AV59TFContagemResultado_PFBFM_To, AV62TFContagemResultado_PFLFM, AV63TFContagemResultado_PFLFM_To, AV66TFContagemResultado_Divergencia, AV67TFContagemResultado_Divergencia_To, AV70TFContagemResultado_ParecerTcn, AV71TFContagemResultado_ParecerTcn_Sel, AV74TFContagemResultado_NaoCnfCntCod, AV75TFContagemResultado_NaoCnfCntCod_To, AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace, AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace, AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace, AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace, AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace, AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace, AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace, AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace, AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace, AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace, AV79TFContagemResultado_StatusCnt_Sels, AV88Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultado_DataCnt1, AV33ContagemResultado_DataCnt_To1, AV20DynamicFiltersSelector2, AV34ContagemResultado_DataCnt2, AV35ContagemResultado_DataCnt_To2, AV24DynamicFiltersSelector3, AV36ContagemResultado_DataCnt3, AV37ContagemResultado_DataCnt_To3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV40TFContagemResultado_DataCnt, AV41TFContagemResultado_DataCnt_To, AV46TFContagemResultado_ContadorFMCod, AV47TFContagemResultado_ContadorFMCod_To, AV50TFContagemResultado_PFBFS, AV51TFContagemResultado_PFBFS_To, AV54TFContagemResultado_PFLFS, AV55TFContagemResultado_PFLFS_To, AV58TFContagemResultado_PFBFM, AV59TFContagemResultado_PFBFM_To, AV62TFContagemResultado_PFLFM, AV63TFContagemResultado_PFLFM_To, AV66TFContagemResultado_Divergencia, AV67TFContagemResultado_Divergencia_To, AV70TFContagemResultado_ParecerTcn, AV71TFContagemResultado_ParecerTcn_Sel, AV74TFContagemResultado_NaoCnfCntCod, AV75TFContagemResultado_NaoCnfCntCod_To, AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace, AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace, AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace, AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace, AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace, AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace, AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace, AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace, AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace, AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace, AV79TFContagemResultado_StatusCnt_Sels, AV88Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultado_DataCnt1, AV33ContagemResultado_DataCnt_To1, AV20DynamicFiltersSelector2, AV34ContagemResultado_DataCnt2, AV35ContagemResultado_DataCnt_To2, AV24DynamicFiltersSelector3, AV36ContagemResultado_DataCnt3, AV37ContagemResultado_DataCnt_To3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV40TFContagemResultado_DataCnt, AV41TFContagemResultado_DataCnt_To, AV46TFContagemResultado_ContadorFMCod, AV47TFContagemResultado_ContadorFMCod_To, AV50TFContagemResultado_PFBFS, AV51TFContagemResultado_PFBFS_To, AV54TFContagemResultado_PFLFS, AV55TFContagemResultado_PFLFS_To, AV58TFContagemResultado_PFBFM, AV59TFContagemResultado_PFBFM_To, AV62TFContagemResultado_PFLFM, AV63TFContagemResultado_PFLFM_To, AV66TFContagemResultado_Divergencia, AV67TFContagemResultado_Divergencia_To, AV70TFContagemResultado_ParecerTcn, AV71TFContagemResultado_ParecerTcn_Sel, AV74TFContagemResultado_NaoCnfCntCod, AV75TFContagemResultado_NaoCnfCntCod_To, AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace, AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace, AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace, AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace, AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace, AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace, AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace, AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace, AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace, AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace, AV79TFContagemResultado_StatusCnt_Sels, AV88Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultado_DataCnt1, AV33ContagemResultado_DataCnt_To1, AV20DynamicFiltersSelector2, AV34ContagemResultado_DataCnt2, AV35ContagemResultado_DataCnt_To2, AV24DynamicFiltersSelector3, AV36ContagemResultado_DataCnt3, AV37ContagemResultado_DataCnt_To3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV40TFContagemResultado_DataCnt, AV41TFContagemResultado_DataCnt_To, AV46TFContagemResultado_ContadorFMCod, AV47TFContagemResultado_ContadorFMCod_To, AV50TFContagemResultado_PFBFS, AV51TFContagemResultado_PFBFS_To, AV54TFContagemResultado_PFLFS, AV55TFContagemResultado_PFLFS_To, AV58TFContagemResultado_PFBFM, AV59TFContagemResultado_PFBFM_To, AV62TFContagemResultado_PFLFM, AV63TFContagemResultado_PFLFM_To, AV66TFContagemResultado_Divergencia, AV67TFContagemResultado_Divergencia_To, AV70TFContagemResultado_ParecerTcn, AV71TFContagemResultado_ParecerTcn_Sel, AV74TFContagemResultado_NaoCnfCntCod, AV75TFContagemResultado_NaoCnfCntCod_To, AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace, AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace, AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace, AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace, AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace, AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace, AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace, AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace, AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace, AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace, AV79TFContagemResultado_StatusCnt_Sels, AV88Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultado_DataCnt1, AV33ContagemResultado_DataCnt_To1, AV20DynamicFiltersSelector2, AV34ContagemResultado_DataCnt2, AV35ContagemResultado_DataCnt_To2, AV24DynamicFiltersSelector3, AV36ContagemResultado_DataCnt3, AV37ContagemResultado_DataCnt_To3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV40TFContagemResultado_DataCnt, AV41TFContagemResultado_DataCnt_To, AV46TFContagemResultado_ContadorFMCod, AV47TFContagemResultado_ContadorFMCod_To, AV50TFContagemResultado_PFBFS, AV51TFContagemResultado_PFBFS_To, AV54TFContagemResultado_PFLFS, AV55TFContagemResultado_PFLFS_To, AV58TFContagemResultado_PFBFM, AV59TFContagemResultado_PFBFM_To, AV62TFContagemResultado_PFLFM, AV63TFContagemResultado_PFLFM_To, AV66TFContagemResultado_Divergencia, AV67TFContagemResultado_Divergencia_To, AV70TFContagemResultado_ParecerTcn, AV71TFContagemResultado_ParecerTcn_Sel, AV74TFContagemResultado_NaoCnfCntCod, AV75TFContagemResultado_NaoCnfCntCod_To, AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace, AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace, AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace, AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace, AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace, AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace, AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace, AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace, AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace, AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace, AV79TFContagemResultado_StatusCnt_Sels, AV88Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPB90( )
      {
         /* Before Start, stand alone formulas. */
         AV88Pgmname = "PromptContagemResultadoContagens";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E32B92 */
         E32B92 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV81DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_DATACNTTITLEFILTERDATA"), AV39ContagemResultado_DataCntTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_CONTADORFMCODTITLEFILTERDATA"), AV45ContagemResultado_ContadorFMCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_PFBFSTITLEFILTERDATA"), AV49ContagemResultado_PFBFSTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_PFLFSTITLEFILTERDATA"), AV53ContagemResultado_PFLFSTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_PFBFMTITLEFILTERDATA"), AV57ContagemResultado_PFBFMTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_PFLFMTITLEFILTERDATA"), AV61ContagemResultado_PFLFMTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_DIVERGENCIATITLEFILTERDATA"), AV65ContagemResultado_DivergenciaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_PARECERTCNTITLEFILTERDATA"), AV69ContagemResultado_ParecerTcnTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_NAOCNFCNTCODTITLEFILTERDATA"), AV73ContagemResultado_NaoCnfCntCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_STATUSCNTTITLEFILTERDATA"), AV77ContagemResultado_StatusCntTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datacnt1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Cnt1"}), 1, "vCONTAGEMRESULTADO_DATACNT1");
               GX_FocusControl = edtavContagemresultado_datacnt1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32ContagemResultado_DataCnt1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_DataCnt1", context.localUtil.Format(AV32ContagemResultado_DataCnt1, "99/99/99"));
            }
            else
            {
               AV32ContagemResultado_DataCnt1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datacnt1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_DataCnt1", context.localUtil.Format(AV32ContagemResultado_DataCnt1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datacnt_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Cnt_To1"}), 1, "vCONTAGEMRESULTADO_DATACNT_TO1");
               GX_FocusControl = edtavContagemresultado_datacnt_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33ContagemResultado_DataCnt_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataCnt_To1", context.localUtil.Format(AV33ContagemResultado_DataCnt_To1, "99/99/99"));
            }
            else
            {
               AV33ContagemResultado_DataCnt_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datacnt_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataCnt_To1", context.localUtil.Format(AV33ContagemResultado_DataCnt_To1, "99/99/99"));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datacnt2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Cnt2"}), 1, "vCONTAGEMRESULTADO_DATACNT2");
               GX_FocusControl = edtavContagemresultado_datacnt2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34ContagemResultado_DataCnt2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_DataCnt2", context.localUtil.Format(AV34ContagemResultado_DataCnt2, "99/99/99"));
            }
            else
            {
               AV34ContagemResultado_DataCnt2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datacnt2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_DataCnt2", context.localUtil.Format(AV34ContagemResultado_DataCnt2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datacnt_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Cnt_To2"}), 1, "vCONTAGEMRESULTADO_DATACNT_TO2");
               GX_FocusControl = edtavContagemresultado_datacnt_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35ContagemResultado_DataCnt_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultado_DataCnt_To2", context.localUtil.Format(AV35ContagemResultado_DataCnt_To2, "99/99/99"));
            }
            else
            {
               AV35ContagemResultado_DataCnt_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datacnt_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultado_DataCnt_To2", context.localUtil.Format(AV35ContagemResultado_DataCnt_To2, "99/99/99"));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV24DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datacnt3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Cnt3"}), 1, "vCONTAGEMRESULTADO_DATACNT3");
               GX_FocusControl = edtavContagemresultado_datacnt3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36ContagemResultado_DataCnt3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_DataCnt3", context.localUtil.Format(AV36ContagemResultado_DataCnt3, "99/99/99"));
            }
            else
            {
               AV36ContagemResultado_DataCnt3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datacnt3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_DataCnt3", context.localUtil.Format(AV36ContagemResultado_DataCnt3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datacnt_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Cnt_To3"}), 1, "vCONTAGEMRESULTADO_DATACNT_TO3");
               GX_FocusControl = edtavContagemresultado_datacnt_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37ContagemResultado_DataCnt_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultado_DataCnt_To3", context.localUtil.Format(AV37ContagemResultado_DataCnt_To3, "99/99/99"));
            }
            else
            {
               AV37ContagemResultado_DataCnt_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datacnt_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultado_DataCnt_To3", context.localUtil.Format(AV37ContagemResultado_DataCnt_To3, "99/99/99"));
            }
            A511ContagemResultado_HoraCnt = cgiGet( edtContagemResultado_HoraCnt_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV23DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultado_datacnt_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContagem Resultado_Data Cnt"}), 1, "vTFCONTAGEMRESULTADO_DATACNT");
               GX_FocusControl = edtavTfcontagemresultado_datacnt_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFContagemResultado_DataCnt = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultado_DataCnt", context.localUtil.Format(AV40TFContagemResultado_DataCnt, "99/99/99"));
            }
            else
            {
               AV40TFContagemResultado_DataCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontagemresultado_datacnt_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultado_DataCnt", context.localUtil.Format(AV40TFContagemResultado_DataCnt, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultado_datacnt_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContagem Resultado_Data Cnt_To"}), 1, "vTFCONTAGEMRESULTADO_DATACNT_TO");
               GX_FocusControl = edtavTfcontagemresultado_datacnt_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41TFContagemResultado_DataCnt_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContagemResultado_DataCnt_To", context.localUtil.Format(AV41TFContagemResultado_DataCnt_To, "99/99/99"));
            }
            else
            {
               AV41TFContagemResultado_DataCnt_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontagemresultado_datacnt_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContagemResultado_DataCnt_To", context.localUtil.Format(AV41TFContagemResultado_DataCnt_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultado_datacntauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado_Data Cnt Aux Date"}), 1, "vDDO_CONTAGEMRESULTADO_DATACNTAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultado_datacntauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42DDO_ContagemResultado_DataCntAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DDO_ContagemResultado_DataCntAuxDate", context.localUtil.Format(AV42DDO_ContagemResultado_DataCntAuxDate, "99/99/99"));
            }
            else
            {
               AV42DDO_ContagemResultado_DataCntAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultado_datacntauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DDO_ContagemResultado_DataCntAuxDate", context.localUtil.Format(AV42DDO_ContagemResultado_DataCntAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultado_datacntauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado_Data Cnt Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADO_DATACNTAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultado_datacntauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43DDO_ContagemResultado_DataCntAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43DDO_ContagemResultado_DataCntAuxDateTo", context.localUtil.Format(AV43DDO_ContagemResultado_DataCntAuxDateTo, "99/99/99"));
            }
            else
            {
               AV43DDO_ContagemResultado_DataCntAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultado_datacntauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43DDO_ContagemResultado_DataCntAuxDateTo", context.localUtil.Format(AV43DDO_ContagemResultado_DataCntAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_contadorfmcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_contadorfmcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_CONTADORFMCOD");
               GX_FocusControl = edtavTfcontagemresultado_contadorfmcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFContagemResultado_ContadorFMCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContagemResultado_ContadorFMCod), 6, 0)));
            }
            else
            {
               AV46TFContagemResultado_ContadorFMCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_contadorfmcod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContagemResultado_ContadorFMCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_contadorfmcod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_contadorfmcod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO");
               GX_FocusControl = edtavTfcontagemresultado_contadorfmcod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFContagemResultado_ContadorFMCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContagemResultado_ContadorFMCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContagemResultado_ContadorFMCod_To), 6, 0)));
            }
            else
            {
               AV47TFContagemResultado_ContadorFMCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_contadorfmcod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContagemResultado_ContadorFMCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContagemResultado_ContadorFMCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pfbfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pfbfs_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_PFBFS");
               GX_FocusControl = edtavTfcontagemresultado_pfbfs_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50TFContagemResultado_PFBFS = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV50TFContagemResultado_PFBFS, 14, 5)));
            }
            else
            {
               AV50TFContagemResultado_PFBFS = context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pfbfs_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV50TFContagemResultado_PFBFS, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pfbfs_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pfbfs_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_PFBFS_TO");
               GX_FocusControl = edtavTfcontagemresultado_pfbfs_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51TFContagemResultado_PFBFS_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContagemResultado_PFBFS_To", StringUtil.LTrim( StringUtil.Str( AV51TFContagemResultado_PFBFS_To, 14, 5)));
            }
            else
            {
               AV51TFContagemResultado_PFBFS_To = context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pfbfs_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContagemResultado_PFBFS_To", StringUtil.LTrim( StringUtil.Str( AV51TFContagemResultado_PFBFS_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pflfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pflfs_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_PFLFS");
               GX_FocusControl = edtavTfcontagemresultado_pflfs_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFContagemResultado_PFLFS = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV54TFContagemResultado_PFLFS, 14, 5)));
            }
            else
            {
               AV54TFContagemResultado_PFLFS = context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pflfs_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV54TFContagemResultado_PFLFS, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pflfs_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pflfs_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_PFLFS_TO");
               GX_FocusControl = edtavTfcontagemresultado_pflfs_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFContagemResultado_PFLFS_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContagemResultado_PFLFS_To", StringUtil.LTrim( StringUtil.Str( AV55TFContagemResultado_PFLFS_To, 14, 5)));
            }
            else
            {
               AV55TFContagemResultado_PFLFS_To = context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pflfs_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContagemResultado_PFLFS_To", StringUtil.LTrim( StringUtil.Str( AV55TFContagemResultado_PFLFS_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pfbfm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pfbfm_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_PFBFM");
               GX_FocusControl = edtavTfcontagemresultado_pfbfm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58TFContagemResultado_PFBFM = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV58TFContagemResultado_PFBFM, 14, 5)));
            }
            else
            {
               AV58TFContagemResultado_PFBFM = context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pfbfm_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV58TFContagemResultado_PFBFM, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pfbfm_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pfbfm_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_PFBFM_TO");
               GX_FocusControl = edtavTfcontagemresultado_pfbfm_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFContagemResultado_PFBFM_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemResultado_PFBFM_To", StringUtil.LTrim( StringUtil.Str( AV59TFContagemResultado_PFBFM_To, 14, 5)));
            }
            else
            {
               AV59TFContagemResultado_PFBFM_To = context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pfbfm_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemResultado_PFBFM_To", StringUtil.LTrim( StringUtil.Str( AV59TFContagemResultado_PFBFM_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pflfm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pflfm_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_PFLFM");
               GX_FocusControl = edtavTfcontagemresultado_pflfm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62TFContagemResultado_PFLFM = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV62TFContagemResultado_PFLFM, 14, 5)));
            }
            else
            {
               AV62TFContagemResultado_PFLFM = context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pflfm_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV62TFContagemResultado_PFLFM, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pflfm_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pflfm_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_PFLFM_TO");
               GX_FocusControl = edtavTfcontagemresultado_pflfm_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63TFContagemResultado_PFLFM_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContagemResultado_PFLFM_To", StringUtil.LTrim( StringUtil.Str( AV63TFContagemResultado_PFLFM_To, 14, 5)));
            }
            else
            {
               AV63TFContagemResultado_PFLFM_To = context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_pflfm_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContagemResultado_PFLFM_To", StringUtil.LTrim( StringUtil.Str( AV63TFContagemResultado_PFLFM_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_divergencia_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_divergencia_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_DIVERGENCIA");
               GX_FocusControl = edtavTfcontagemresultado_divergencia_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66TFContagemResultado_Divergencia = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV66TFContagemResultado_Divergencia, 6, 2)));
            }
            else
            {
               AV66TFContagemResultado_Divergencia = context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_divergencia_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV66TFContagemResultado_Divergencia, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_divergencia_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_divergencia_to_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_DIVERGENCIA_TO");
               GX_FocusControl = edtavTfcontagemresultado_divergencia_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67TFContagemResultado_Divergencia_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContagemResultado_Divergencia_To", StringUtil.LTrim( StringUtil.Str( AV67TFContagemResultado_Divergencia_To, 6, 2)));
            }
            else
            {
               AV67TFContagemResultado_Divergencia_To = context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_divergencia_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContagemResultado_Divergencia_To", StringUtil.LTrim( StringUtil.Str( AV67TFContagemResultado_Divergencia_To, 6, 2)));
            }
            AV70TFContagemResultado_ParecerTcn = cgiGet( edtavTfcontagemresultado_parecertcn_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultado_ParecerTcn", AV70TFContagemResultado_ParecerTcn);
            AV71TFContagemResultado_ParecerTcn_Sel = cgiGet( edtavTfcontagemresultado_parecertcn_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_ParecerTcn_Sel", AV71TFContagemResultado_ParecerTcn_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_naocnfcntcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_naocnfcntcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_NAOCNFCNTCOD");
               GX_FocusControl = edtavTfcontagemresultado_naocnfcntcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV74TFContagemResultado_NaoCnfCntCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74TFContagemResultado_NaoCnfCntCod), 6, 0)));
            }
            else
            {
               AV74TFContagemResultado_NaoCnfCntCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_naocnfcntcod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74TFContagemResultado_NaoCnfCntCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_naocnfcntcod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_naocnfcntcod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO");
               GX_FocusControl = edtavTfcontagemresultado_naocnfcntcod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV75TFContagemResultado_NaoCnfCntCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultado_NaoCnfCntCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFContagemResultado_NaoCnfCntCod_To), 6, 0)));
            }
            else
            {
               AV75TFContagemResultado_NaoCnfCntCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_naocnfcntcod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultado_NaoCnfCntCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFContagemResultado_NaoCnfCntCod_To), 6, 0)));
            }
            AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_datacnttitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace", AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace);
            AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_contadorfmcodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace", AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace);
            AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_pfbfstitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace", AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace);
            AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_pflfstitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace", AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace);
            AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_pfbfmtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace", AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace);
            AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_pflfmtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace", AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace);
            AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_divergenciatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace", AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace);
            AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_parecertcntitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace", AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace);
            AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_naocnfcntcodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace", AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace);
            AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_statuscnttitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace", AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_86 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_86"), ",", "."));
            AV83GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV84GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contagemresultado_datacnt_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Caption");
            Ddo_contagemresultado_datacnt_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Tooltip");
            Ddo_contagemresultado_datacnt_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Cls");
            Ddo_contagemresultado_datacnt_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Filteredtext_set");
            Ddo_contagemresultado_datacnt_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Filteredtextto_set");
            Ddo_contagemresultado_datacnt_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Dropdownoptionstype");
            Ddo_contagemresultado_datacnt_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Titlecontrolidtoreplace");
            Ddo_contagemresultado_datacnt_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Includesortasc"));
            Ddo_contagemresultado_datacnt_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Includesortdsc"));
            Ddo_contagemresultado_datacnt_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Sortedstatus");
            Ddo_contagemresultado_datacnt_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Includefilter"));
            Ddo_contagemresultado_datacnt_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Filtertype");
            Ddo_contagemresultado_datacnt_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Filterisrange"));
            Ddo_contagemresultado_datacnt_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Includedatalist"));
            Ddo_contagemresultado_datacnt_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Sortasc");
            Ddo_contagemresultado_datacnt_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Sortdsc");
            Ddo_contagemresultado_datacnt_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Cleanfilter");
            Ddo_contagemresultado_datacnt_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Rangefilterfrom");
            Ddo_contagemresultado_datacnt_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Rangefilterto");
            Ddo_contagemresultado_datacnt_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Searchbuttontext");
            Ddo_contagemresultado_contadorfmcod_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Caption");
            Ddo_contagemresultado_contadorfmcod_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Tooltip");
            Ddo_contagemresultado_contadorfmcod_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Cls");
            Ddo_contagemresultado_contadorfmcod_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Filteredtext_set");
            Ddo_contagemresultado_contadorfmcod_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Filteredtextto_set");
            Ddo_contagemresultado_contadorfmcod_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Dropdownoptionstype");
            Ddo_contagemresultado_contadorfmcod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Titlecontrolidtoreplace");
            Ddo_contagemresultado_contadorfmcod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Includesortasc"));
            Ddo_contagemresultado_contadorfmcod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Includesortdsc"));
            Ddo_contagemresultado_contadorfmcod_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Sortedstatus");
            Ddo_contagemresultado_contadorfmcod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Includefilter"));
            Ddo_contagemresultado_contadorfmcod_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Filtertype");
            Ddo_contagemresultado_contadorfmcod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Filterisrange"));
            Ddo_contagemresultado_contadorfmcod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Includedatalist"));
            Ddo_contagemresultado_contadorfmcod_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Sortasc");
            Ddo_contagemresultado_contadorfmcod_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Sortdsc");
            Ddo_contagemresultado_contadorfmcod_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Cleanfilter");
            Ddo_contagemresultado_contadorfmcod_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Rangefilterfrom");
            Ddo_contagemresultado_contadorfmcod_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Rangefilterto");
            Ddo_contagemresultado_contadorfmcod_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Searchbuttontext");
            Ddo_contagemresultado_pfbfs_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Caption");
            Ddo_contagemresultado_pfbfs_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Tooltip");
            Ddo_contagemresultado_pfbfs_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Cls");
            Ddo_contagemresultado_pfbfs_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Filteredtext_set");
            Ddo_contagemresultado_pfbfs_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Filteredtextto_set");
            Ddo_contagemresultado_pfbfs_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Dropdownoptionstype");
            Ddo_contagemresultado_pfbfs_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Titlecontrolidtoreplace");
            Ddo_contagemresultado_pfbfs_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Includesortasc"));
            Ddo_contagemresultado_pfbfs_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Includesortdsc"));
            Ddo_contagemresultado_pfbfs_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Sortedstatus");
            Ddo_contagemresultado_pfbfs_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Includefilter"));
            Ddo_contagemresultado_pfbfs_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Filtertype");
            Ddo_contagemresultado_pfbfs_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Filterisrange"));
            Ddo_contagemresultado_pfbfs_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Includedatalist"));
            Ddo_contagemresultado_pfbfs_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Sortasc");
            Ddo_contagemresultado_pfbfs_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Sortdsc");
            Ddo_contagemresultado_pfbfs_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Cleanfilter");
            Ddo_contagemresultado_pfbfs_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Rangefilterfrom");
            Ddo_contagemresultado_pfbfs_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Rangefilterto");
            Ddo_contagemresultado_pfbfs_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Searchbuttontext");
            Ddo_contagemresultado_pflfs_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Caption");
            Ddo_contagemresultado_pflfs_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Tooltip");
            Ddo_contagemresultado_pflfs_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Cls");
            Ddo_contagemresultado_pflfs_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Filteredtext_set");
            Ddo_contagemresultado_pflfs_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Filteredtextto_set");
            Ddo_contagemresultado_pflfs_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Dropdownoptionstype");
            Ddo_contagemresultado_pflfs_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Titlecontrolidtoreplace");
            Ddo_contagemresultado_pflfs_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Includesortasc"));
            Ddo_contagemresultado_pflfs_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Includesortdsc"));
            Ddo_contagemresultado_pflfs_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Sortedstatus");
            Ddo_contagemresultado_pflfs_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Includefilter"));
            Ddo_contagemresultado_pflfs_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Filtertype");
            Ddo_contagemresultado_pflfs_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Filterisrange"));
            Ddo_contagemresultado_pflfs_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Includedatalist"));
            Ddo_contagemresultado_pflfs_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Sortasc");
            Ddo_contagemresultado_pflfs_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Sortdsc");
            Ddo_contagemresultado_pflfs_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Cleanfilter");
            Ddo_contagemresultado_pflfs_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Rangefilterfrom");
            Ddo_contagemresultado_pflfs_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Rangefilterto");
            Ddo_contagemresultado_pflfs_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Searchbuttontext");
            Ddo_contagemresultado_pfbfm_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Caption");
            Ddo_contagemresultado_pfbfm_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Tooltip");
            Ddo_contagemresultado_pfbfm_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Cls");
            Ddo_contagemresultado_pfbfm_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Filteredtext_set");
            Ddo_contagemresultado_pfbfm_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Filteredtextto_set");
            Ddo_contagemresultado_pfbfm_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Dropdownoptionstype");
            Ddo_contagemresultado_pfbfm_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Titlecontrolidtoreplace");
            Ddo_contagemresultado_pfbfm_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Includesortasc"));
            Ddo_contagemresultado_pfbfm_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Includesortdsc"));
            Ddo_contagemresultado_pfbfm_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Sortedstatus");
            Ddo_contagemresultado_pfbfm_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Includefilter"));
            Ddo_contagemresultado_pfbfm_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Filtertype");
            Ddo_contagemresultado_pfbfm_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Filterisrange"));
            Ddo_contagemresultado_pfbfm_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Includedatalist"));
            Ddo_contagemresultado_pfbfm_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Sortasc");
            Ddo_contagemresultado_pfbfm_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Sortdsc");
            Ddo_contagemresultado_pfbfm_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Cleanfilter");
            Ddo_contagemresultado_pfbfm_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Rangefilterfrom");
            Ddo_contagemresultado_pfbfm_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Rangefilterto");
            Ddo_contagemresultado_pfbfm_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Searchbuttontext");
            Ddo_contagemresultado_pflfm_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Caption");
            Ddo_contagemresultado_pflfm_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Tooltip");
            Ddo_contagemresultado_pflfm_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Cls");
            Ddo_contagemresultado_pflfm_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Filteredtext_set");
            Ddo_contagemresultado_pflfm_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Filteredtextto_set");
            Ddo_contagemresultado_pflfm_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Dropdownoptionstype");
            Ddo_contagemresultado_pflfm_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Titlecontrolidtoreplace");
            Ddo_contagemresultado_pflfm_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Includesortasc"));
            Ddo_contagemresultado_pflfm_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Includesortdsc"));
            Ddo_contagemresultado_pflfm_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Sortedstatus");
            Ddo_contagemresultado_pflfm_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Includefilter"));
            Ddo_contagemresultado_pflfm_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Filtertype");
            Ddo_contagemresultado_pflfm_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Filterisrange"));
            Ddo_contagemresultado_pflfm_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Includedatalist"));
            Ddo_contagemresultado_pflfm_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Sortasc");
            Ddo_contagemresultado_pflfm_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Sortdsc");
            Ddo_contagemresultado_pflfm_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Cleanfilter");
            Ddo_contagemresultado_pflfm_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Rangefilterfrom");
            Ddo_contagemresultado_pflfm_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Rangefilterto");
            Ddo_contagemresultado_pflfm_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Searchbuttontext");
            Ddo_contagemresultado_divergencia_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Caption");
            Ddo_contagemresultado_divergencia_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Tooltip");
            Ddo_contagemresultado_divergencia_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Cls");
            Ddo_contagemresultado_divergencia_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Filteredtext_set");
            Ddo_contagemresultado_divergencia_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Filteredtextto_set");
            Ddo_contagemresultado_divergencia_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Dropdownoptionstype");
            Ddo_contagemresultado_divergencia_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Titlecontrolidtoreplace");
            Ddo_contagemresultado_divergencia_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Includesortasc"));
            Ddo_contagemresultado_divergencia_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Includesortdsc"));
            Ddo_contagemresultado_divergencia_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Sortedstatus");
            Ddo_contagemresultado_divergencia_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Includefilter"));
            Ddo_contagemresultado_divergencia_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Filtertype");
            Ddo_contagemresultado_divergencia_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Filterisrange"));
            Ddo_contagemresultado_divergencia_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Includedatalist"));
            Ddo_contagemresultado_divergencia_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Sortasc");
            Ddo_contagemresultado_divergencia_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Sortdsc");
            Ddo_contagemresultado_divergencia_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Cleanfilter");
            Ddo_contagemresultado_divergencia_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Rangefilterfrom");
            Ddo_contagemresultado_divergencia_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Rangefilterto");
            Ddo_contagemresultado_divergencia_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Searchbuttontext");
            Ddo_contagemresultado_parecertcn_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Caption");
            Ddo_contagemresultado_parecertcn_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Tooltip");
            Ddo_contagemresultado_parecertcn_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Cls");
            Ddo_contagemresultado_parecertcn_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Filteredtext_set");
            Ddo_contagemresultado_parecertcn_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Selectedvalue_set");
            Ddo_contagemresultado_parecertcn_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Dropdownoptionstype");
            Ddo_contagemresultado_parecertcn_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Titlecontrolidtoreplace");
            Ddo_contagemresultado_parecertcn_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Includesortasc"));
            Ddo_contagemresultado_parecertcn_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Includesortdsc"));
            Ddo_contagemresultado_parecertcn_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Sortedstatus");
            Ddo_contagemresultado_parecertcn_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Includefilter"));
            Ddo_contagemresultado_parecertcn_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Filtertype");
            Ddo_contagemresultado_parecertcn_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Filterisrange"));
            Ddo_contagemresultado_parecertcn_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Includedatalist"));
            Ddo_contagemresultado_parecertcn_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Datalisttype");
            Ddo_contagemresultado_parecertcn_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Datalistproc");
            Ddo_contagemresultado_parecertcn_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_parecertcn_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Sortasc");
            Ddo_contagemresultado_parecertcn_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Sortdsc");
            Ddo_contagemresultado_parecertcn_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Loadingdata");
            Ddo_contagemresultado_parecertcn_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Cleanfilter");
            Ddo_contagemresultado_parecertcn_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Noresultsfound");
            Ddo_contagemresultado_parecertcn_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Searchbuttontext");
            Ddo_contagemresultado_naocnfcntcod_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Caption");
            Ddo_contagemresultado_naocnfcntcod_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Tooltip");
            Ddo_contagemresultado_naocnfcntcod_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Cls");
            Ddo_contagemresultado_naocnfcntcod_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Filteredtext_set");
            Ddo_contagemresultado_naocnfcntcod_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Filteredtextto_set");
            Ddo_contagemresultado_naocnfcntcod_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Dropdownoptionstype");
            Ddo_contagemresultado_naocnfcntcod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Titlecontrolidtoreplace");
            Ddo_contagemresultado_naocnfcntcod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Includesortasc"));
            Ddo_contagemresultado_naocnfcntcod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Includesortdsc"));
            Ddo_contagemresultado_naocnfcntcod_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Sortedstatus");
            Ddo_contagemresultado_naocnfcntcod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Includefilter"));
            Ddo_contagemresultado_naocnfcntcod_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Filtertype");
            Ddo_contagemresultado_naocnfcntcod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Filterisrange"));
            Ddo_contagemresultado_naocnfcntcod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Includedatalist"));
            Ddo_contagemresultado_naocnfcntcod_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Sortasc");
            Ddo_contagemresultado_naocnfcntcod_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Sortdsc");
            Ddo_contagemresultado_naocnfcntcod_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Cleanfilter");
            Ddo_contagemresultado_naocnfcntcod_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Rangefilterfrom");
            Ddo_contagemresultado_naocnfcntcod_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Rangefilterto");
            Ddo_contagemresultado_naocnfcntcod_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Searchbuttontext");
            Ddo_contagemresultado_statuscnt_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Caption");
            Ddo_contagemresultado_statuscnt_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Tooltip");
            Ddo_contagemresultado_statuscnt_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Cls");
            Ddo_contagemresultado_statuscnt_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Selectedvalue_set");
            Ddo_contagemresultado_statuscnt_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Dropdownoptionstype");
            Ddo_contagemresultado_statuscnt_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Titlecontrolidtoreplace");
            Ddo_contagemresultado_statuscnt_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Includesortasc"));
            Ddo_contagemresultado_statuscnt_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Includesortdsc"));
            Ddo_contagemresultado_statuscnt_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Sortedstatus");
            Ddo_contagemresultado_statuscnt_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Includefilter"));
            Ddo_contagemresultado_statuscnt_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Includedatalist"));
            Ddo_contagemresultado_statuscnt_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Datalisttype");
            Ddo_contagemresultado_statuscnt_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Allowmultipleselection"));
            Ddo_contagemresultado_statuscnt_Datalistfixedvalues = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Datalistfixedvalues");
            Ddo_contagemresultado_statuscnt_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Sortasc");
            Ddo_contagemresultado_statuscnt_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Sortdsc");
            Ddo_contagemresultado_statuscnt_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Cleanfilter");
            Ddo_contagemresultado_statuscnt_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contagemresultado_datacnt_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Activeeventkey");
            Ddo_contagemresultado_datacnt_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Filteredtext_get");
            Ddo_contagemresultado_datacnt_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADO_DATACNT_Filteredtextto_get");
            Ddo_contagemresultado_contadorfmcod_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Activeeventkey");
            Ddo_contagemresultado_contadorfmcod_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Filteredtext_get");
            Ddo_contagemresultado_contadorfmcod_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADO_CONTADORFMCOD_Filteredtextto_get");
            Ddo_contagemresultado_pfbfs_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Activeeventkey");
            Ddo_contagemresultado_pfbfs_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Filteredtext_get");
            Ddo_contagemresultado_pfbfs_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFS_Filteredtextto_get");
            Ddo_contagemresultado_pflfs_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Activeeventkey");
            Ddo_contagemresultado_pflfs_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Filteredtext_get");
            Ddo_contagemresultado_pflfs_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFS_Filteredtextto_get");
            Ddo_contagemresultado_pfbfm_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Activeeventkey");
            Ddo_contagemresultado_pfbfm_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Filteredtext_get");
            Ddo_contagemresultado_pfbfm_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADO_PFBFM_Filteredtextto_get");
            Ddo_contagemresultado_pflfm_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Activeeventkey");
            Ddo_contagemresultado_pflfm_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Filteredtext_get");
            Ddo_contagemresultado_pflfm_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADO_PFLFM_Filteredtextto_get");
            Ddo_contagemresultado_divergencia_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Activeeventkey");
            Ddo_contagemresultado_divergencia_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Filteredtext_get");
            Ddo_contagemresultado_divergencia_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADO_DIVERGENCIA_Filteredtextto_get");
            Ddo_contagemresultado_parecertcn_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Activeeventkey");
            Ddo_contagemresultado_parecertcn_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Filteredtext_get");
            Ddo_contagemresultado_parecertcn_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_PARECERTCN_Selectedvalue_get");
            Ddo_contagemresultado_naocnfcntcod_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Activeeventkey");
            Ddo_contagemresultado_naocnfcntcod_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Filteredtext_get");
            Ddo_contagemresultado_naocnfcntcod_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD_Filteredtextto_get");
            Ddo_contagemresultado_statuscnt_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Activeeventkey");
            Ddo_contagemresultado_statuscnt_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSCNT_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "PromptContagemResultadoContagens";
            A511ContagemResultado_HoraCnt = cgiGet( edtContagemResultado_HoraCnt_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, ""));
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("promptcontagemresultadocontagens:[SecurityCheckFailed value for]"+"ContagemResultado_HoraCnt:"+StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, "")));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATACNT1"), 0) != AV32ContagemResultado_DataCnt1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATACNT_TO1"), 0) != AV33ContagemResultado_DataCnt_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATACNT2"), 0) != AV34ContagemResultado_DataCnt2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATACNT_TO2"), 0) != AV35ContagemResultado_DataCnt_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATACNT3"), 0) != AV36ContagemResultado_DataCnt3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATACNT_TO3"), 0) != AV37ContagemResultado_DataCnt_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATACNT"), 0) != AV40TFContagemResultado_DataCnt )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATACNT_TO"), 0) != AV41TFContagemResultado_DataCnt_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_CONTADORFMCOD"), ",", ".") != Convert.ToDecimal( AV46TFContagemResultado_ContadorFMCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO"), ",", ".") != Convert.ToDecimal( AV47TFContagemResultado_ContadorFMCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFBFS"), ",", ".") != AV50TFContagemResultado_PFBFS )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFBFS_TO"), ",", ".") != AV51TFContagemResultado_PFBFS_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFLFS"), ",", ".") != AV54TFContagemResultado_PFLFS )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFLFS_TO"), ",", ".") != AV55TFContagemResultado_PFLFS_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFBFM"), ",", ".") != AV58TFContagemResultado_PFBFM )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFBFM_TO"), ",", ".") != AV59TFContagemResultado_PFBFM_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFLFM"), ",", ".") != AV62TFContagemResultado_PFLFM )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_PFLFM_TO"), ",", ".") != AV63TFContagemResultado_PFLFM_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DIVERGENCIA"), ",", ".") != AV66TFContagemResultado_Divergencia )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DIVERGENCIA_TO"), ",", ".") != AV67TFContagemResultado_Divergencia_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_PARECERTCN"), AV70TFContagemResultado_ParecerTcn) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_PARECERTCN_SEL"), AV71TFContagemResultado_ParecerTcn_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_NAOCNFCNTCOD"), ",", ".") != Convert.ToDecimal( AV74TFContagemResultado_NaoCnfCntCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO"), ",", ".") != Convert.ToDecimal( AV75TFContagemResultado_NaoCnfCntCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E32B92 */
         E32B92 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E32B92( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "CONTAGEMRESULTADO_DATACNT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "CONTAGEMRESULTADO_DATACNT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersSelector3 = "CONTAGEMRESULTADO_DATACNT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontagemresultado_datacnt_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_datacnt_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_datacnt_Visible), 5, 0)));
         edtavTfcontagemresultado_datacnt_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_datacnt_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_datacnt_to_Visible), 5, 0)));
         edtavTfcontagemresultado_contadorfmcod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_contadorfmcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_contadorfmcod_Visible), 5, 0)));
         edtavTfcontagemresultado_contadorfmcod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_contadorfmcod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_contadorfmcod_to_Visible), 5, 0)));
         edtavTfcontagemresultado_pfbfs_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_pfbfs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_pfbfs_Visible), 5, 0)));
         edtavTfcontagemresultado_pfbfs_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_pfbfs_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_pfbfs_to_Visible), 5, 0)));
         edtavTfcontagemresultado_pflfs_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_pflfs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_pflfs_Visible), 5, 0)));
         edtavTfcontagemresultado_pflfs_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_pflfs_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_pflfs_to_Visible), 5, 0)));
         edtavTfcontagemresultado_pfbfm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_pfbfm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_pfbfm_Visible), 5, 0)));
         edtavTfcontagemresultado_pfbfm_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_pfbfm_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_pfbfm_to_Visible), 5, 0)));
         edtavTfcontagemresultado_pflfm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_pflfm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_pflfm_Visible), 5, 0)));
         edtavTfcontagemresultado_pflfm_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_pflfm_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_pflfm_to_Visible), 5, 0)));
         edtavTfcontagemresultado_divergencia_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_divergencia_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_divergencia_Visible), 5, 0)));
         edtavTfcontagemresultado_divergencia_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_divergencia_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_divergencia_to_Visible), 5, 0)));
         edtavTfcontagemresultado_parecertcn_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_parecertcn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_parecertcn_Visible), 5, 0)));
         edtavTfcontagemresultado_parecertcn_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_parecertcn_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_parecertcn_sel_Visible), 5, 0)));
         edtavTfcontagemresultado_naocnfcntcod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_naocnfcntcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_naocnfcntcod_Visible), 5, 0)));
         edtavTfcontagemresultado_naocnfcntcod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_naocnfcntcod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_naocnfcntcod_to_Visible), 5, 0)));
         Ddo_contagemresultado_datacnt_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_DataCnt";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datacnt_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_datacnt_Titlecontrolidtoreplace);
         AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace = Ddo_contagemresultado_datacnt_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace", AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace);
         edtavDdo_contagemresultado_datacnttitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_datacnttitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_datacnttitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_contadorfmcod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_ContadorFMCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contadorfmcod_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_contadorfmcod_Titlecontrolidtoreplace);
         AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace = Ddo_contagemresultado_contadorfmcod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace", AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace);
         edtavDdo_contagemresultado_contadorfmcodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_contadorfmcodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_contadorfmcodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_pfbfs_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_PFBFS";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfs_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_pfbfs_Titlecontrolidtoreplace);
         AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace = Ddo_contagemresultado_pfbfs_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace", AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace);
         edtavDdo_contagemresultado_pfbfstitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_pfbfstitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_pfbfstitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_pflfs_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_PFLFS";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfs_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_pflfs_Titlecontrolidtoreplace);
         AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace = Ddo_contagemresultado_pflfs_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace", AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace);
         edtavDdo_contagemresultado_pflfstitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_pflfstitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_pflfstitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_pfbfm_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_PFBFM";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfm_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_pfbfm_Titlecontrolidtoreplace);
         AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace = Ddo_contagemresultado_pfbfm_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace", AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace);
         edtavDdo_contagemresultado_pfbfmtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_pfbfmtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_pfbfmtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_pflfm_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_PFLFM";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfm_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_pflfm_Titlecontrolidtoreplace);
         AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace = Ddo_contagemresultado_pflfm_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace", AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace);
         edtavDdo_contagemresultado_pflfmtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_pflfmtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_pflfmtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_divergencia_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_Divergencia";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_divergencia_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_divergencia_Titlecontrolidtoreplace);
         AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace = Ddo_contagemresultado_divergencia_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace", AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace);
         edtavDdo_contagemresultado_divergenciatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_divergenciatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_divergenciatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_parecertcn_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_ParecerTcn";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_parecertcn_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_parecertcn_Titlecontrolidtoreplace);
         AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace = Ddo_contagemresultado_parecertcn_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace", AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace);
         edtavDdo_contagemresultado_parecertcntitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_parecertcntitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_parecertcntitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_naocnfcntcod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_NaoCnfCntCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_naocnfcntcod_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_naocnfcntcod_Titlecontrolidtoreplace);
         AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace = Ddo_contagemresultado_naocnfcntcod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace", AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace);
         edtavDdo_contagemresultado_naocnfcntcodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_naocnfcntcodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_naocnfcntcodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_statuscnt_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_StatusCnt";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_statuscnt_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_statuscnt_Titlecontrolidtoreplace);
         AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace = Ddo_contagemresultado_statuscnt_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace", AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace);
         edtavDdo_contagemresultado_statuscnttitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_statuscnttitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_statuscnttitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Contagem Resultado Contagens";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtContagemResultado_HoraCnt_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_HoraCnt_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_HoraCnt_Visible), 5, 0)));
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data", 0);
         cmbavOrderedby.addItem("2", "FM", 0);
         cmbavOrderedby.addItem("3", "Bruto FS", 0);
         cmbavOrderedby.addItem("4", "L�quido FS", 0);
         cmbavOrderedby.addItem("5", "Bruto FM", 0);
         cmbavOrderedby.addItem("6", "L�quido FM", 0);
         cmbavOrderedby.addItem("7", "Diverg�ncia", 0);
         cmbavOrderedby.addItem("8", "t�cnico", 0);
         cmbavOrderedby.addItem("9", "conformidade", 0);
         cmbavOrderedby.addItem("10", "Status", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV81DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV81DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E33B92( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV39ContagemResultado_DataCntTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45ContagemResultado_ContadorFMCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49ContagemResultado_PFBFSTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53ContagemResultado_PFLFSTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57ContagemResultado_PFBFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61ContagemResultado_PFLFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65ContagemResultado_DivergenciaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContagemResultado_ParecerTcnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73ContagemResultado_NaoCnfCntCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV77ContagemResultado_StatusCntTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemResultado_DataCnt_Titleformat = 2;
         edtContagemResultado_DataCnt_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataCnt_Internalname, "Title", edtContagemResultado_DataCnt_Title);
         edtContagemResultado_ContadorFMCod_Titleformat = 2;
         edtContagemResultado_ContadorFMCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "FM", AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_ContadorFMCod_Internalname, "Title", edtContagemResultado_ContadorFMCod_Title);
         edtContagemResultado_PFBFS_Titleformat = 2;
         edtContagemResultado_PFBFS_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Bruto FS", AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFBFS_Internalname, "Title", edtContagemResultado_PFBFS_Title);
         edtContagemResultado_PFLFS_Titleformat = 2;
         edtContagemResultado_PFLFS_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "L�quido FS", AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFLFS_Internalname, "Title", edtContagemResultado_PFLFS_Title);
         edtContagemResultado_PFBFM_Titleformat = 2;
         edtContagemResultado_PFBFM_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Bruto FM", AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFBFM_Internalname, "Title", edtContagemResultado_PFBFM_Title);
         edtContagemResultado_PFLFM_Titleformat = 2;
         edtContagemResultado_PFLFM_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "L�quido FM", AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFLFM_Internalname, "Title", edtContagemResultado_PFLFM_Title);
         edtContagemResultado_Divergencia_Titleformat = 2;
         edtContagemResultado_Divergencia_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Diverg�ncia", AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Divergencia_Internalname, "Title", edtContagemResultado_Divergencia_Title);
         edtContagemResultado_ParecerTcn_Titleformat = 2;
         edtContagemResultado_ParecerTcn_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "t�cnico", AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_ParecerTcn_Internalname, "Title", edtContagemResultado_ParecerTcn_Title);
         edtContagemResultado_NaoCnfCntCod_Titleformat = 2;
         edtContagemResultado_NaoCnfCntCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "conformidade", AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_NaoCnfCntCod_Internalname, "Title", edtContagemResultado_NaoCnfCntCod_Title);
         cmbContagemResultado_StatusCnt_Titleformat = 2;
         cmbContagemResultado_StatusCnt.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Status", AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_StatusCnt_Internalname, "Title", cmbContagemResultado_StatusCnt.Title.Text);
         AV83GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83GridCurrentPage), 10, 0)));
         AV84GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV39ContagemResultado_DataCntTitleFilterData", AV39ContagemResultado_DataCntTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45ContagemResultado_ContadorFMCodTitleFilterData", AV45ContagemResultado_ContadorFMCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49ContagemResultado_PFBFSTitleFilterData", AV49ContagemResultado_PFBFSTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV53ContagemResultado_PFLFSTitleFilterData", AV53ContagemResultado_PFLFSTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57ContagemResultado_PFBFMTitleFilterData", AV57ContagemResultado_PFBFMTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV61ContagemResultado_PFLFMTitleFilterData", AV61ContagemResultado_PFLFMTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV65ContagemResultado_DivergenciaTitleFilterData", AV65ContagemResultado_DivergenciaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69ContagemResultado_ParecerTcnTitleFilterData", AV69ContagemResultado_ParecerTcnTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV73ContagemResultado_NaoCnfCntCodTitleFilterData", AV73ContagemResultado_NaoCnfCntCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV77ContagemResultado_StatusCntTitleFilterData", AV77ContagemResultado_StatusCntTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
      }

      protected void E11B92( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV82PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV82PageToGo) ;
         }
      }

      protected void E12B92( )
      {
         /* Ddo_contagemresultado_datacnt_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_datacnt_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_datacnt_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datacnt_Internalname, "SortedStatus", Ddo_contagemresultado_datacnt_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_datacnt_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_datacnt_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datacnt_Internalname, "SortedStatus", Ddo_contagemresultado_datacnt_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_datacnt_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFContagemResultado_DataCnt = context.localUtil.CToD( Ddo_contagemresultado_datacnt_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultado_DataCnt", context.localUtil.Format(AV40TFContagemResultado_DataCnt, "99/99/99"));
            AV41TFContagemResultado_DataCnt_To = context.localUtil.CToD( Ddo_contagemresultado_datacnt_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContagemResultado_DataCnt_To", context.localUtil.Format(AV41TFContagemResultado_DataCnt_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13B92( )
      {
         /* Ddo_contagemresultado_contadorfmcod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_contadorfmcod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_contadorfmcod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contadorfmcod_Internalname, "SortedStatus", Ddo_contagemresultado_contadorfmcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_contadorfmcod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_contadorfmcod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contadorfmcod_Internalname, "SortedStatus", Ddo_contagemresultado_contadorfmcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_contadorfmcod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( Ddo_contagemresultado_contadorfmcod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContagemResultado_ContadorFMCod), 6, 0)));
            AV47TFContagemResultado_ContadorFMCod_To = (int)(NumberUtil.Val( Ddo_contagemresultado_contadorfmcod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContagemResultado_ContadorFMCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContagemResultado_ContadorFMCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14B92( )
      {
         /* Ddo_contagemresultado_pfbfs_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_pfbfs_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_pfbfs_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfs_Internalname, "SortedStatus", Ddo_contagemresultado_pfbfs_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_pfbfs_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_pfbfs_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfs_Internalname, "SortedStatus", Ddo_contagemresultado_pfbfs_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_pfbfs_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFContagemResultado_PFBFS = NumberUtil.Val( Ddo_contagemresultado_pfbfs_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV50TFContagemResultado_PFBFS, 14, 5)));
            AV51TFContagemResultado_PFBFS_To = NumberUtil.Val( Ddo_contagemresultado_pfbfs_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContagemResultado_PFBFS_To", StringUtil.LTrim( StringUtil.Str( AV51TFContagemResultado_PFBFS_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15B92( )
      {
         /* Ddo_contagemresultado_pflfs_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_pflfs_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_pflfs_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfs_Internalname, "SortedStatus", Ddo_contagemresultado_pflfs_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_pflfs_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_pflfs_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfs_Internalname, "SortedStatus", Ddo_contagemresultado_pflfs_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_pflfs_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV54TFContagemResultado_PFLFS = NumberUtil.Val( Ddo_contagemresultado_pflfs_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV54TFContagemResultado_PFLFS, 14, 5)));
            AV55TFContagemResultado_PFLFS_To = NumberUtil.Val( Ddo_contagemresultado_pflfs_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContagemResultado_PFLFS_To", StringUtil.LTrim( StringUtil.Str( AV55TFContagemResultado_PFLFS_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16B92( )
      {
         /* Ddo_contagemresultado_pfbfm_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_pfbfm_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_pfbfm_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfm_Internalname, "SortedStatus", Ddo_contagemresultado_pfbfm_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_pfbfm_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_pfbfm_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfm_Internalname, "SortedStatus", Ddo_contagemresultado_pfbfm_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_pfbfm_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV58TFContagemResultado_PFBFM = NumberUtil.Val( Ddo_contagemresultado_pfbfm_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV58TFContagemResultado_PFBFM, 14, 5)));
            AV59TFContagemResultado_PFBFM_To = NumberUtil.Val( Ddo_contagemresultado_pfbfm_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemResultado_PFBFM_To", StringUtil.LTrim( StringUtil.Str( AV59TFContagemResultado_PFBFM_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17B92( )
      {
         /* Ddo_contagemresultado_pflfm_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_pflfm_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_pflfm_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfm_Internalname, "SortedStatus", Ddo_contagemresultado_pflfm_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_pflfm_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_pflfm_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfm_Internalname, "SortedStatus", Ddo_contagemresultado_pflfm_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_pflfm_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV62TFContagemResultado_PFLFM = NumberUtil.Val( Ddo_contagemresultado_pflfm_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV62TFContagemResultado_PFLFM, 14, 5)));
            AV63TFContagemResultado_PFLFM_To = NumberUtil.Val( Ddo_contagemresultado_pflfm_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContagemResultado_PFLFM_To", StringUtil.LTrim( StringUtil.Str( AV63TFContagemResultado_PFLFM_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18B92( )
      {
         /* Ddo_contagemresultado_divergencia_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_divergencia_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_divergencia_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_divergencia_Internalname, "SortedStatus", Ddo_contagemresultado_divergencia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_divergencia_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_divergencia_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_divergencia_Internalname, "SortedStatus", Ddo_contagemresultado_divergencia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_divergencia_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV66TFContagemResultado_Divergencia = NumberUtil.Val( Ddo_contagemresultado_divergencia_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV66TFContagemResultado_Divergencia, 6, 2)));
            AV67TFContagemResultado_Divergencia_To = NumberUtil.Val( Ddo_contagemresultado_divergencia_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContagemResultado_Divergencia_To", StringUtil.LTrim( StringUtil.Str( AV67TFContagemResultado_Divergencia_To, 6, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19B92( )
      {
         /* Ddo_contagemresultado_parecertcn_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_parecertcn_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_parecertcn_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_parecertcn_Internalname, "SortedStatus", Ddo_contagemresultado_parecertcn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_parecertcn_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_parecertcn_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_parecertcn_Internalname, "SortedStatus", Ddo_contagemresultado_parecertcn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_parecertcn_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFContagemResultado_ParecerTcn = Ddo_contagemresultado_parecertcn_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultado_ParecerTcn", AV70TFContagemResultado_ParecerTcn);
            AV71TFContagemResultado_ParecerTcn_Sel = Ddo_contagemresultado_parecertcn_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_ParecerTcn_Sel", AV71TFContagemResultado_ParecerTcn_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E20B92( )
      {
         /* Ddo_contagemresultado_naocnfcntcod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_naocnfcntcod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_naocnfcntcod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_naocnfcntcod_Internalname, "SortedStatus", Ddo_contagemresultado_naocnfcntcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_naocnfcntcod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_naocnfcntcod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_naocnfcntcod_Internalname, "SortedStatus", Ddo_contagemresultado_naocnfcntcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_naocnfcntcod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( Ddo_contagemresultado_naocnfcntcod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74TFContagemResultado_NaoCnfCntCod), 6, 0)));
            AV75TFContagemResultado_NaoCnfCntCod_To = (int)(NumberUtil.Val( Ddo_contagemresultado_naocnfcntcod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultado_NaoCnfCntCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFContagemResultado_NaoCnfCntCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E21B92( )
      {
         /* Ddo_contagemresultado_statuscnt_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_statuscnt_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_statuscnt_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_statuscnt_Internalname, "SortedStatus", Ddo_contagemresultado_statuscnt_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_statuscnt_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_statuscnt_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_statuscnt_Internalname, "SortedStatus", Ddo_contagemresultado_statuscnt_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_statuscnt_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV78TFContagemResultado_StatusCnt_SelsJson = Ddo_contagemresultado_statuscnt_Selectedvalue_get;
            AV79TFContagemResultado_StatusCnt_Sels.FromJSonString(StringUtil.StringReplace( AV78TFContagemResultado_StatusCnt_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV79TFContagemResultado_StatusCnt_Sels", AV79TFContagemResultado_StatusCnt_Sels);
      }

      private void E34B92( )
      {
         /* Grid_Load Routine */
         AV29Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV29Select);
         AV87Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 86;
         }
         sendrow_862( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_86_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(86, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E35B92 */
         E35B92 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E35B92( )
      {
         /* Enter Routine */
         AV7InOutContagemResultado_Codigo = A456ContagemResultado_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemResultado_Codigo), 6, 0)));
         AV8InOutContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultado_DataCnt", context.localUtil.Format(AV8InOutContagemResultado_DataCnt, "99/99/99"));
         AV38InOutContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38InOutContagemResultado_HoraCnt", AV38InOutContagemResultado_HoraCnt);
         AV9InOutContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV9InOutContagemResultado_PFBFS, 14, 5)));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContagemResultado_Codigo,context.localUtil.Format( AV8InOutContagemResultado_DataCnt, "99/99/99"),(String)AV38InOutContagemResultado_HoraCnt,(decimal)AV9InOutContagemResultado_PFBFS});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E22B92( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E27B92( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E23B92( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultado_DataCnt1, AV33ContagemResultado_DataCnt_To1, AV20DynamicFiltersSelector2, AV34ContagemResultado_DataCnt2, AV35ContagemResultado_DataCnt_To2, AV24DynamicFiltersSelector3, AV36ContagemResultado_DataCnt3, AV37ContagemResultado_DataCnt_To3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV40TFContagemResultado_DataCnt, AV41TFContagemResultado_DataCnt_To, AV46TFContagemResultado_ContadorFMCod, AV47TFContagemResultado_ContadorFMCod_To, AV50TFContagemResultado_PFBFS, AV51TFContagemResultado_PFBFS_To, AV54TFContagemResultado_PFLFS, AV55TFContagemResultado_PFLFS_To, AV58TFContagemResultado_PFBFM, AV59TFContagemResultado_PFBFM_To, AV62TFContagemResultado_PFLFM, AV63TFContagemResultado_PFLFM_To, AV66TFContagemResultado_Divergencia, AV67TFContagemResultado_Divergencia_To, AV70TFContagemResultado_ParecerTcn, AV71TFContagemResultado_ParecerTcn_Sel, AV74TFContagemResultado_NaoCnfCntCod, AV75TFContagemResultado_NaoCnfCntCod_To, AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace, AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace, AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace, AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace, AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace, AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace, AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace, AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace, AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace, AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace, AV79TFContagemResultado_StatusCnt_Sels, AV88Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E28B92( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E29B92( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV23DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E24B92( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultado_DataCnt1, AV33ContagemResultado_DataCnt_To1, AV20DynamicFiltersSelector2, AV34ContagemResultado_DataCnt2, AV35ContagemResultado_DataCnt_To2, AV24DynamicFiltersSelector3, AV36ContagemResultado_DataCnt3, AV37ContagemResultado_DataCnt_To3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV40TFContagemResultado_DataCnt, AV41TFContagemResultado_DataCnt_To, AV46TFContagemResultado_ContadorFMCod, AV47TFContagemResultado_ContadorFMCod_To, AV50TFContagemResultado_PFBFS, AV51TFContagemResultado_PFBFS_To, AV54TFContagemResultado_PFLFS, AV55TFContagemResultado_PFLFS_To, AV58TFContagemResultado_PFBFM, AV59TFContagemResultado_PFBFM_To, AV62TFContagemResultado_PFLFM, AV63TFContagemResultado_PFLFM_To, AV66TFContagemResultado_Divergencia, AV67TFContagemResultado_Divergencia_To, AV70TFContagemResultado_ParecerTcn, AV71TFContagemResultado_ParecerTcn_Sel, AV74TFContagemResultado_NaoCnfCntCod, AV75TFContagemResultado_NaoCnfCntCod_To, AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace, AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace, AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace, AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace, AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace, AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace, AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace, AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace, AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace, AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace, AV79TFContagemResultado_StatusCnt_Sels, AV88Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E30B92( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25B92( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultado_DataCnt1, AV33ContagemResultado_DataCnt_To1, AV20DynamicFiltersSelector2, AV34ContagemResultado_DataCnt2, AV35ContagemResultado_DataCnt_To2, AV24DynamicFiltersSelector3, AV36ContagemResultado_DataCnt3, AV37ContagemResultado_DataCnt_To3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV40TFContagemResultado_DataCnt, AV41TFContagemResultado_DataCnt_To, AV46TFContagemResultado_ContadorFMCod, AV47TFContagemResultado_ContadorFMCod_To, AV50TFContagemResultado_PFBFS, AV51TFContagemResultado_PFBFS_To, AV54TFContagemResultado_PFLFS, AV55TFContagemResultado_PFLFS_To, AV58TFContagemResultado_PFBFM, AV59TFContagemResultado_PFBFM_To, AV62TFContagemResultado_PFLFM, AV63TFContagemResultado_PFLFM_To, AV66TFContagemResultado_Divergencia, AV67TFContagemResultado_Divergencia_To, AV70TFContagemResultado_ParecerTcn, AV71TFContagemResultado_ParecerTcn_Sel, AV74TFContagemResultado_NaoCnfCntCod, AV75TFContagemResultado_NaoCnfCntCod_To, AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace, AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace, AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace, AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace, AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace, AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace, AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace, AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace, AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace, AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace, AV79TFContagemResultado_StatusCnt_Sels, AV88Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E31B92( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26B92( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV79TFContagemResultado_StatusCnt_Sels", AV79TFContagemResultado_StatusCnt_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contagemresultado_datacnt_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datacnt_Internalname, "SortedStatus", Ddo_contagemresultado_datacnt_Sortedstatus);
         Ddo_contagemresultado_contadorfmcod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contadorfmcod_Internalname, "SortedStatus", Ddo_contagemresultado_contadorfmcod_Sortedstatus);
         Ddo_contagemresultado_pfbfs_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfs_Internalname, "SortedStatus", Ddo_contagemresultado_pfbfs_Sortedstatus);
         Ddo_contagemresultado_pflfs_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfs_Internalname, "SortedStatus", Ddo_contagemresultado_pflfs_Sortedstatus);
         Ddo_contagemresultado_pfbfm_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfm_Internalname, "SortedStatus", Ddo_contagemresultado_pfbfm_Sortedstatus);
         Ddo_contagemresultado_pflfm_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfm_Internalname, "SortedStatus", Ddo_contagemresultado_pflfm_Sortedstatus);
         Ddo_contagemresultado_divergencia_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_divergencia_Internalname, "SortedStatus", Ddo_contagemresultado_divergencia_Sortedstatus);
         Ddo_contagemresultado_parecertcn_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_parecertcn_Internalname, "SortedStatus", Ddo_contagemresultado_parecertcn_Sortedstatus);
         Ddo_contagemresultado_naocnfcntcod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_naocnfcntcod_Internalname, "SortedStatus", Ddo_contagemresultado_naocnfcntcod_Sortedstatus);
         Ddo_contagemresultado_statuscnt_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_statuscnt_Internalname, "SortedStatus", Ddo_contagemresultado_statuscnt_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 1 )
         {
            Ddo_contagemresultado_datacnt_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datacnt_Internalname, "SortedStatus", Ddo_contagemresultado_datacnt_Sortedstatus);
         }
         else if ( AV14OrderedBy == 2 )
         {
            Ddo_contagemresultado_contadorfmcod_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contadorfmcod_Internalname, "SortedStatus", Ddo_contagemresultado_contadorfmcod_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_contagemresultado_pfbfs_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfs_Internalname, "SortedStatus", Ddo_contagemresultado_pfbfs_Sortedstatus);
         }
         else if ( AV14OrderedBy == 4 )
         {
            Ddo_contagemresultado_pflfs_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfs_Internalname, "SortedStatus", Ddo_contagemresultado_pflfs_Sortedstatus);
         }
         else if ( AV14OrderedBy == 5 )
         {
            Ddo_contagemresultado_pfbfm_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfm_Internalname, "SortedStatus", Ddo_contagemresultado_pfbfm_Sortedstatus);
         }
         else if ( AV14OrderedBy == 6 )
         {
            Ddo_contagemresultado_pflfm_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfm_Internalname, "SortedStatus", Ddo_contagemresultado_pflfm_Sortedstatus);
         }
         else if ( AV14OrderedBy == 7 )
         {
            Ddo_contagemresultado_divergencia_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_divergencia_Internalname, "SortedStatus", Ddo_contagemresultado_divergencia_Sortedstatus);
         }
         else if ( AV14OrderedBy == 8 )
         {
            Ddo_contagemresultado_parecertcn_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_parecertcn_Internalname, "SortedStatus", Ddo_contagemresultado_parecertcn_Sortedstatus);
         }
         else if ( AV14OrderedBy == 9 )
         {
            Ddo_contagemresultado_naocnfcntcod_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_naocnfcntcod_Internalname, "SortedStatus", Ddo_contagemresultado_naocnfcntcod_Sortedstatus);
         }
         else if ( AV14OrderedBy == 10 )
         {
            Ddo_contagemresultado_statuscnt_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_statuscnt_Internalname, "SortedStatus", Ddo_contagemresultado_statuscnt_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontagemresultado_datacnt1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datacnt1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datacnt1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_datacnt1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datacnt1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datacnt1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontagemresultado_datacnt2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datacnt2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datacnt2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_datacnt2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datacnt2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datacnt2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterscontagemresultado_datacnt3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datacnt3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datacnt3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_datacnt3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datacnt3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datacnt3_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "CONTAGEMRESULTADO_DATACNT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV34ContagemResultado_DataCnt2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_DataCnt2", context.localUtil.Format(AV34ContagemResultado_DataCnt2, "99/99/99"));
         AV35ContagemResultado_DataCnt_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultado_DataCnt_To2", context.localUtil.Format(AV35ContagemResultado_DataCnt_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         AV24DynamicFiltersSelector3 = "CONTAGEMRESULTADO_DATACNT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         AV36ContagemResultado_DataCnt3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_DataCnt3", context.localUtil.Format(AV36ContagemResultado_DataCnt3, "99/99/99"));
         AV37ContagemResultado_DataCnt_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultado_DataCnt_To3", context.localUtil.Format(AV37ContagemResultado_DataCnt_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV40TFContagemResultado_DataCnt = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultado_DataCnt", context.localUtil.Format(AV40TFContagemResultado_DataCnt, "99/99/99"));
         Ddo_contagemresultado_datacnt_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datacnt_Internalname, "FilteredText_set", Ddo_contagemresultado_datacnt_Filteredtext_set);
         AV41TFContagemResultado_DataCnt_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContagemResultado_DataCnt_To", context.localUtil.Format(AV41TFContagemResultado_DataCnt_To, "99/99/99"));
         Ddo_contagemresultado_datacnt_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datacnt_Internalname, "FilteredTextTo_set", Ddo_contagemresultado_datacnt_Filteredtextto_set);
         AV46TFContagemResultado_ContadorFMCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContagemResultado_ContadorFMCod), 6, 0)));
         Ddo_contagemresultado_contadorfmcod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contadorfmcod_Internalname, "FilteredText_set", Ddo_contagemresultado_contadorfmcod_Filteredtext_set);
         AV47TFContagemResultado_ContadorFMCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContagemResultado_ContadorFMCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContagemResultado_ContadorFMCod_To), 6, 0)));
         Ddo_contagemresultado_contadorfmcod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contadorfmcod_Internalname, "FilteredTextTo_set", Ddo_contagemresultado_contadorfmcod_Filteredtextto_set);
         AV50TFContagemResultado_PFBFS = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV50TFContagemResultado_PFBFS, 14, 5)));
         Ddo_contagemresultado_pfbfs_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfs_Internalname, "FilteredText_set", Ddo_contagemresultado_pfbfs_Filteredtext_set);
         AV51TFContagemResultado_PFBFS_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContagemResultado_PFBFS_To", StringUtil.LTrim( StringUtil.Str( AV51TFContagemResultado_PFBFS_To, 14, 5)));
         Ddo_contagemresultado_pfbfs_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfs_Internalname, "FilteredTextTo_set", Ddo_contagemresultado_pfbfs_Filteredtextto_set);
         AV54TFContagemResultado_PFLFS = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV54TFContagemResultado_PFLFS, 14, 5)));
         Ddo_contagemresultado_pflfs_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfs_Internalname, "FilteredText_set", Ddo_contagemresultado_pflfs_Filteredtext_set);
         AV55TFContagemResultado_PFLFS_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContagemResultado_PFLFS_To", StringUtil.LTrim( StringUtil.Str( AV55TFContagemResultado_PFLFS_To, 14, 5)));
         Ddo_contagemresultado_pflfs_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfs_Internalname, "FilteredTextTo_set", Ddo_contagemresultado_pflfs_Filteredtextto_set);
         AV58TFContagemResultado_PFBFM = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV58TFContagemResultado_PFBFM, 14, 5)));
         Ddo_contagemresultado_pfbfm_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfm_Internalname, "FilteredText_set", Ddo_contagemresultado_pfbfm_Filteredtext_set);
         AV59TFContagemResultado_PFBFM_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemResultado_PFBFM_To", StringUtil.LTrim( StringUtil.Str( AV59TFContagemResultado_PFBFM_To, 14, 5)));
         Ddo_contagemresultado_pfbfm_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pfbfm_Internalname, "FilteredTextTo_set", Ddo_contagemresultado_pfbfm_Filteredtextto_set);
         AV62TFContagemResultado_PFLFM = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV62TFContagemResultado_PFLFM, 14, 5)));
         Ddo_contagemresultado_pflfm_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfm_Internalname, "FilteredText_set", Ddo_contagemresultado_pflfm_Filteredtext_set);
         AV63TFContagemResultado_PFLFM_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContagemResultado_PFLFM_To", StringUtil.LTrim( StringUtil.Str( AV63TFContagemResultado_PFLFM_To, 14, 5)));
         Ddo_contagemresultado_pflfm_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_pflfm_Internalname, "FilteredTextTo_set", Ddo_contagemresultado_pflfm_Filteredtextto_set);
         AV66TFContagemResultado_Divergencia = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV66TFContagemResultado_Divergencia, 6, 2)));
         Ddo_contagemresultado_divergencia_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_divergencia_Internalname, "FilteredText_set", Ddo_contagemresultado_divergencia_Filteredtext_set);
         AV67TFContagemResultado_Divergencia_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContagemResultado_Divergencia_To", StringUtil.LTrim( StringUtil.Str( AV67TFContagemResultado_Divergencia_To, 6, 2)));
         Ddo_contagemresultado_divergencia_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_divergencia_Internalname, "FilteredTextTo_set", Ddo_contagemresultado_divergencia_Filteredtextto_set);
         AV70TFContagemResultado_ParecerTcn = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultado_ParecerTcn", AV70TFContagemResultado_ParecerTcn);
         Ddo_contagemresultado_parecertcn_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_parecertcn_Internalname, "FilteredText_set", Ddo_contagemresultado_parecertcn_Filteredtext_set);
         AV71TFContagemResultado_ParecerTcn_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_ParecerTcn_Sel", AV71TFContagemResultado_ParecerTcn_Sel);
         Ddo_contagemresultado_parecertcn_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_parecertcn_Internalname, "SelectedValue_set", Ddo_contagemresultado_parecertcn_Selectedvalue_set);
         AV74TFContagemResultado_NaoCnfCntCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74TFContagemResultado_NaoCnfCntCod), 6, 0)));
         Ddo_contagemresultado_naocnfcntcod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_naocnfcntcod_Internalname, "FilteredText_set", Ddo_contagemresultado_naocnfcntcod_Filteredtext_set);
         AV75TFContagemResultado_NaoCnfCntCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultado_NaoCnfCntCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFContagemResultado_NaoCnfCntCod_To), 6, 0)));
         Ddo_contagemresultado_naocnfcntcod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_naocnfcntcod_Internalname, "FilteredTextTo_set", Ddo_contagemresultado_naocnfcntcod_Filteredtextto_set);
         AV79TFContagemResultado_StatusCnt_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_contagemresultado_statuscnt_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_statuscnt_Internalname, "SelectedValue_set", Ddo_contagemresultado_statuscnt_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "CONTAGEMRESULTADO_DATACNT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV32ContagemResultado_DataCnt1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_DataCnt1", context.localUtil.Format(AV32ContagemResultado_DataCnt1, "99/99/99"));
         AV33ContagemResultado_DataCnt_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataCnt_To1", context.localUtil.Format(AV33ContagemResultado_DataCnt_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 )
            {
               AV32ContagemResultado_DataCnt1 = context.localUtil.CToD( AV13GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_DataCnt1", context.localUtil.Format(AV32ContagemResultado_DataCnt1, "99/99/99"));
               AV33ContagemResultado_DataCnt_To1 = context.localUtil.CToD( AV13GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataCnt_To1", context.localUtil.Format(AV33ContagemResultado_DataCnt_To1, "99/99/99"));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 )
               {
                  AV34ContagemResultado_DataCnt2 = context.localUtil.CToD( AV13GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_DataCnt2", context.localUtil.Format(AV34ContagemResultado_DataCnt2, "99/99/99"));
                  AV35ContagemResultado_DataCnt_To2 = context.localUtil.CToD( AV13GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultado_DataCnt_To2", context.localUtil.Format(AV35ContagemResultado_DataCnt_To2, "99/99/99"));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV23DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV24DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 )
                  {
                     AV36ContagemResultado_DataCnt3 = context.localUtil.CToD( AV13GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_DataCnt3", context.localUtil.Format(AV36ContagemResultado_DataCnt3, "99/99/99"));
                     AV37ContagemResultado_DataCnt_To3 = context.localUtil.CToD( AV13GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultado_DataCnt_To3", context.localUtil.Format(AV37ContagemResultado_DataCnt_To3, "99/99/99"));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV27DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (DateTime.MinValue==AV40TFContagemResultado_DataCnt) && (DateTime.MinValue==AV41TFContagemResultado_DataCnt_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DATACNT";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV40TFContagemResultado_DataCnt, 2, "/");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV41TFContagemResultado_DataCnt_To, 2, "/");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV46TFContagemResultado_ContadorFMCod) && (0==AV47TFContagemResultado_ContadorFMCod_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_CONTADORFMCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV46TFContagemResultado_ContadorFMCod), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV47TFContagemResultado_ContadorFMCod_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV50TFContagemResultado_PFBFS) && (Convert.ToDecimal(0)==AV51TFContagemResultado_PFBFS_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_PFBFS";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV50TFContagemResultado_PFBFS, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV51TFContagemResultado_PFBFS_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV54TFContagemResultado_PFLFS) && (Convert.ToDecimal(0)==AV55TFContagemResultado_PFLFS_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_PFLFS";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV54TFContagemResultado_PFLFS, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV55TFContagemResultado_PFLFS_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV58TFContagemResultado_PFBFM) && (Convert.ToDecimal(0)==AV59TFContagemResultado_PFBFM_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_PFBFM";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV58TFContagemResultado_PFBFM, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV59TFContagemResultado_PFBFM_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV62TFContagemResultado_PFLFM) && (Convert.ToDecimal(0)==AV63TFContagemResultado_PFLFM_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_PFLFM";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV62TFContagemResultado_PFLFM, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV63TFContagemResultado_PFLFM_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV66TFContagemResultado_Divergencia) && (Convert.ToDecimal(0)==AV67TFContagemResultado_Divergencia_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DIVERGENCIA";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV66TFContagemResultado_Divergencia, 6, 2);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV67TFContagemResultado_Divergencia_To, 6, 2);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFContagemResultado_ParecerTcn)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_PARECERTCN";
            AV12GridStateFilterValue.gxTpr_Value = AV70TFContagemResultado_ParecerTcn;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFContagemResultado_ParecerTcn_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_PARECERTCN_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV71TFContagemResultado_ParecerTcn_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV74TFContagemResultado_NaoCnfCntCod) && (0==AV75TFContagemResultado_NaoCnfCntCod_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_NAOCNFCNTCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV74TFContagemResultado_NaoCnfCntCod), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV75TFContagemResultado_NaoCnfCntCod_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV79TFContagemResultado_StatusCnt_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_STATUSCNT_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV79TFContagemResultado_StatusCnt_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV88Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV28DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ! ( (DateTime.MinValue==AV32ContagemResultado_DataCnt1) && (DateTime.MinValue==AV33ContagemResultado_DataCnt_To1) ) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV32ContagemResultado_DataCnt1, 2, "/");
               AV13GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV33ContagemResultado_DataCnt_To1, 2, "/");
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ! ( (DateTime.MinValue==AV34ContagemResultado_DataCnt2) && (DateTime.MinValue==AV35ContagemResultado_DataCnt_To2) ) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV34ContagemResultado_DataCnt2, 2, "/");
               AV13GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV35ContagemResultado_DataCnt_To2, 2, "/");
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV23DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV24DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ! ( (DateTime.MinValue==AV36ContagemResultado_DataCnt3) && (DateTime.MinValue==AV37ContagemResultado_DataCnt_To3) ) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV36ContagemResultado_DataCnt3, 2, "/");
               AV13GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV37ContagemResultado_DataCnt_To3, 2, "/");
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_B92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_B92( true) ;
         }
         else
         {
            wb_table2_5_B92( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_B92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_80_B92( true) ;
         }
         else
         {
            wb_table3_80_B92( false) ;
         }
         return  ;
      }

      protected void wb_table3_80_B92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_B92e( true) ;
         }
         else
         {
            wb_table1_2_B92e( false) ;
         }
      }

      protected void wb_table3_80_B92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_83_B92( true) ;
         }
         else
         {
            wb_table4_83_B92( false) ;
         }
         return  ;
      }

      protected void wb_table4_83_B92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_80_B92e( true) ;
         }
         else
         {
            wb_table3_80_B92e( false) ;
         }
      }

      protected void wb_table4_83_B92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"86\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contagem Resultado_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_DataCnt_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_DataCnt_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_DataCnt_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_ContadorFMCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_ContadorFMCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_ContadorFMCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_PFBFS_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_PFBFS_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_PFBFS_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_PFLFS_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_PFLFS_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_PFLFS_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_PFBFM_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_PFBFM_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_PFBFM_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_PFLFM_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_PFLFM_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_PFLFM_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_Divergencia_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_Divergencia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_Divergencia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_ParecerTcn_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_ParecerTcn_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_ParecerTcn_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_NaoCnfCntCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_NaoCnfCntCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_NaoCnfCntCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemResultado_StatusCnt_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemResultado_StatusCnt.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemResultado_StatusCnt.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_DataCnt_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_DataCnt_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_ContadorFMCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_ContadorFMCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_PFBFS_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_PFBFS_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A459ContagemResultado_PFLFS, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_PFLFS_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_PFLFS_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_PFBFM_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_PFBFM_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A461ContagemResultado_PFLFM, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_PFLFM_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_PFLFM_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A462ContagemResultado_Divergencia, 6, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_Divergencia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Divergencia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A463ContagemResultado_ParecerTcn);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_ParecerTcn_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_ParecerTcn_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_NaoCnfCntCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_NaoCnfCntCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A483ContagemResultado_StatusCnt), 2, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemResultado_StatusCnt.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemResultado_StatusCnt_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 86 )
         {
            wbEnd = 0;
            nRC_GXsfl_86 = (short)(nGXsfl_86_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_83_B92e( true) ;
         }
         else
         {
            wb_table4_83_B92e( false) ;
         }
      }

      protected void wb_table2_5_B92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContagemResultadoContagens.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_B92( true) ;
         }
         else
         {
            wb_table5_14_B92( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_B92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_B92e( true) ;
         }
         else
         {
            wb_table2_5_B92e( false) ;
         }
      }

      protected void wb_table5_14_B92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_B92( true) ;
         }
         else
         {
            wb_table6_19_B92( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_B92e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_B92e( true) ;
         }
         else
         {
            wb_table5_14_B92e( false) ;
         }
      }

      protected void wb_table6_19_B92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContagemResultadoContagens.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_B92( true) ;
         }
         else
         {
            wb_table7_28_B92( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_B92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_PromptContagemResultadoContagens.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_47_B92( true) ;
         }
         else
         {
            wb_table8_47_B92( false) ;
         }
         return  ;
      }

      protected void wb_table8_47_B92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV24DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_PromptContagemResultadoContagens.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_66_B92( true) ;
         }
         else
         {
            wb_table9_66_B92( false) ;
         }
         return  ;
      }

      protected void wb_table9_66_B92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_B92e( true) ;
         }
         else
         {
            wb_table6_19_B92e( false) ;
         }
      }

      protected void wb_table9_66_B92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_datacnt3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_datacnt3_Internalname, tblTablemergeddynamicfilterscontagemresultado_datacnt3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datacnt3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datacnt3_Internalname, context.localUtil.Format(AV36ContagemResultado_DataCnt3, "99/99/99"), context.localUtil.Format( AV36ContagemResultado_DataCnt3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datacnt3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datacnt3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultado_datacnt_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontagemresultado_datacnt_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datacnt_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datacnt_to3_Internalname, context.localUtil.Format(AV37ContagemResultado_DataCnt_To3, "99/99/99"), context.localUtil.Format( AV37ContagemResultado_DataCnt_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datacnt_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datacnt_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_66_B92e( true) ;
         }
         else
         {
            wb_table9_66_B92e( false) ;
         }
      }

      protected void wb_table8_47_B92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_datacnt2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_datacnt2_Internalname, tblTablemergeddynamicfilterscontagemresultado_datacnt2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datacnt2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datacnt2_Internalname, context.localUtil.Format(AV34ContagemResultado_DataCnt2, "99/99/99"), context.localUtil.Format( AV34ContagemResultado_DataCnt2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datacnt2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datacnt2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultado_datacnt_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontagemresultado_datacnt_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datacnt_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datacnt_to2_Internalname, context.localUtil.Format(AV35ContagemResultado_DataCnt_To2, "99/99/99"), context.localUtil.Format( AV35ContagemResultado_DataCnt_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datacnt_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datacnt_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_47_B92e( true) ;
         }
         else
         {
            wb_table8_47_B92e( false) ;
         }
      }

      protected void wb_table7_28_B92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_datacnt1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_datacnt1_Internalname, tblTablemergeddynamicfilterscontagemresultado_datacnt1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datacnt1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datacnt1_Internalname, context.localUtil.Format(AV32ContagemResultado_DataCnt1, "99/99/99"), context.localUtil.Format( AV32ContagemResultado_DataCnt1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datacnt1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datacnt1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultado_datacnt_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontagemresultado_datacnt_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datacnt_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datacnt_to1_Internalname, context.localUtil.Format(AV33ContagemResultado_DataCnt_To1, "99/99/99"), context.localUtil.Format( AV33ContagemResultado_DataCnt_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datacnt_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoContagens.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datacnt_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoContagens.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_B92e( true) ;
         }
         else
         {
            wb_table7_28_B92e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemResultado_Codigo), 6, 0)));
         AV8InOutContagemResultado_DataCnt = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultado_DataCnt", context.localUtil.Format(AV8InOutContagemResultado_DataCnt, "99/99/99"));
         AV38InOutContagemResultado_HoraCnt = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38InOutContagemResultado_HoraCnt", AV38InOutContagemResultado_HoraCnt);
         AV9InOutContagemResultado_PFBFS = (decimal)(Convert.ToDecimal((decimal)getParm(obj,3)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV9InOutContagemResultado_PFBFS, 14, 5)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAB92( ) ;
         WSB92( ) ;
         WEB92( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181324091");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontagemresultadocontagens.js", "?20205181324092");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_idx;
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_86_idx;
         edtContagemResultado_DataCnt_Internalname = "CONTAGEMRESULTADO_DATACNT_"+sGXsfl_86_idx;
         edtContagemResultado_ContadorFMCod_Internalname = "CONTAGEMRESULTADO_CONTADORFMCOD_"+sGXsfl_86_idx;
         edtContagemResultado_PFBFS_Internalname = "CONTAGEMRESULTADO_PFBFS_"+sGXsfl_86_idx;
         edtContagemResultado_PFLFS_Internalname = "CONTAGEMRESULTADO_PFLFS_"+sGXsfl_86_idx;
         edtContagemResultado_PFBFM_Internalname = "CONTAGEMRESULTADO_PFBFM_"+sGXsfl_86_idx;
         edtContagemResultado_PFLFM_Internalname = "CONTAGEMRESULTADO_PFLFM_"+sGXsfl_86_idx;
         edtContagemResultado_Divergencia_Internalname = "CONTAGEMRESULTADO_DIVERGENCIA_"+sGXsfl_86_idx;
         edtContagemResultado_ParecerTcn_Internalname = "CONTAGEMRESULTADO_PARECERTCN_"+sGXsfl_86_idx;
         edtContagemResultado_NaoCnfCntCod_Internalname = "CONTAGEMRESULTADO_NAOCNFCNTCOD_"+sGXsfl_86_idx;
         cmbContagemResultado_StatusCnt_Internalname = "CONTAGEMRESULTADO_STATUSCNT_"+sGXsfl_86_idx;
      }

      protected void SubsflControlProps_fel_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_fel_idx;
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_86_fel_idx;
         edtContagemResultado_DataCnt_Internalname = "CONTAGEMRESULTADO_DATACNT_"+sGXsfl_86_fel_idx;
         edtContagemResultado_ContadorFMCod_Internalname = "CONTAGEMRESULTADO_CONTADORFMCOD_"+sGXsfl_86_fel_idx;
         edtContagemResultado_PFBFS_Internalname = "CONTAGEMRESULTADO_PFBFS_"+sGXsfl_86_fel_idx;
         edtContagemResultado_PFLFS_Internalname = "CONTAGEMRESULTADO_PFLFS_"+sGXsfl_86_fel_idx;
         edtContagemResultado_PFBFM_Internalname = "CONTAGEMRESULTADO_PFBFM_"+sGXsfl_86_fel_idx;
         edtContagemResultado_PFLFM_Internalname = "CONTAGEMRESULTADO_PFLFM_"+sGXsfl_86_fel_idx;
         edtContagemResultado_Divergencia_Internalname = "CONTAGEMRESULTADO_DIVERGENCIA_"+sGXsfl_86_fel_idx;
         edtContagemResultado_ParecerTcn_Internalname = "CONTAGEMRESULTADO_PARECERTCN_"+sGXsfl_86_fel_idx;
         edtContagemResultado_NaoCnfCntCod_Internalname = "CONTAGEMRESULTADO_NAOCNFCNTCOD_"+sGXsfl_86_fel_idx;
         cmbContagemResultado_StatusCnt_Internalname = "CONTAGEMRESULTADO_STATUSCNT_"+sGXsfl_86_fel_idx;
      }

      protected void sendrow_862( )
      {
         SubsflControlProps_862( ) ;
         WBB90( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_86_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_86_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_86_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 87,'',false,'',86)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV29Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV87Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Select)) ? AV87Select_GXI : context.PathToRelativeUrl( AV29Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_86_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV29Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataCnt_Internalname,context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"),context.localUtil.Format( A473ContagemResultado_DataCnt, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DataCnt_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ContadorFMCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A470ContagemResultado_ContadorFMCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ContadorFMCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PFBFS_Internalname,StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ",", "")),context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PFBFS_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PFLFS_Internalname,StringUtil.LTrim( StringUtil.NToC( A459ContagemResultado_PFLFS, 14, 5, ",", "")),context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PFLFS_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PFBFM_Internalname,StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ",", "")),context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PFBFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PFLFM_Internalname,StringUtil.LTrim( StringUtil.NToC( A461ContagemResultado_PFLFM, 14, 5, ",", "")),context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PFLFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Divergencia_Internalname,StringUtil.LTrim( StringUtil.NToC( A462ContagemResultado_Divergencia, 6, 2, ",", "")),context.localUtil.Format( A462ContagemResultado_Divergencia, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Divergencia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ParecerTcn_Internalname,(String)A463ContagemResultado_ParecerTcn,(String)A463ContagemResultado_ParecerTcn,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ParecerTcn_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)86,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_NaoCnfCntCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A469ContagemResultado_NaoCnfCntCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_NaoCnfCntCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_86_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMRESULTADO_STATUSCNT_" + sGXsfl_86_idx;
               cmbContagemResultado_StatusCnt.Name = GXCCtl;
               cmbContagemResultado_StatusCnt.WebTags = "";
               cmbContagemResultado_StatusCnt.addItem("1", "Inicial", 0);
               cmbContagemResultado_StatusCnt.addItem("2", "Auditoria", 0);
               cmbContagemResultado_StatusCnt.addItem("3", "Negociac�o", 0);
               cmbContagemResultado_StatusCnt.addItem("4", "N�o Aprovada", 0);
               cmbContagemResultado_StatusCnt.addItem("5", "Entregue", 0);
               cmbContagemResultado_StatusCnt.addItem("6", "Pend�ncias", 0);
               cmbContagemResultado_StatusCnt.addItem("7", "Diverg�ncia", 0);
               cmbContagemResultado_StatusCnt.addItem("8", "Aprovada", 0);
               if ( cmbContagemResultado_StatusCnt.ItemCount > 0 )
               {
                  A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cmbContagemResultado_StatusCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultado_StatusCnt,(String)cmbContagemResultado_StatusCnt_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)),(short)1,(String)cmbContagemResultado_StatusCnt_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemResultado_StatusCnt.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_StatusCnt_Internalname, "Values", (String)(cmbContagemResultado_StatusCnt.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CODIGO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DATACNT"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, A473ContagemResultado_DataCnt));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CONTADORFMCOD"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A470ContagemResultado_ContadorFMCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_PFBFS"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_PFLFS"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_PFBFM"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_PFLFM"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DIVERGENCIA"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A462ContagemResultado_Divergencia, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_PARECERTCN"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, A463ContagemResultado_ParecerTcn));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_NAOCNFCNTCOD"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A469ContagemResultado_NaoCnfCntCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_STATUSCNT"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         /* End function sendrow_862 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavContagemresultado_datacnt1_Internalname = "vCONTAGEMRESULTADO_DATACNT1";
         lblDynamicfilterscontagemresultado_datacnt_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADO_DATACNT_RANGEMIDDLETEXT1";
         edtavContagemresultado_datacnt_to1_Internalname = "vCONTAGEMRESULTADO_DATACNT_TO1";
         tblTablemergeddynamicfilterscontagemresultado_datacnt1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavContagemresultado_datacnt2_Internalname = "vCONTAGEMRESULTADO_DATACNT2";
         lblDynamicfilterscontagemresultado_datacnt_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADO_DATACNT_RANGEMIDDLETEXT2";
         edtavContagemresultado_datacnt_to2_Internalname = "vCONTAGEMRESULTADO_DATACNT_TO2";
         tblTablemergeddynamicfilterscontagemresultado_datacnt2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavContagemresultado_datacnt3_Internalname = "vCONTAGEMRESULTADO_DATACNT3";
         lblDynamicfilterscontagemresultado_datacnt_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADO_DATACNT_RANGEMIDDLETEXT3";
         edtavContagemresultado_datacnt_to3_Internalname = "vCONTAGEMRESULTADO_DATACNT_TO3";
         tblTablemergeddynamicfilterscontagemresultado_datacnt3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO";
         edtContagemResultado_DataCnt_Internalname = "CONTAGEMRESULTADO_DATACNT";
         edtContagemResultado_ContadorFMCod_Internalname = "CONTAGEMRESULTADO_CONTADORFMCOD";
         edtContagemResultado_PFBFS_Internalname = "CONTAGEMRESULTADO_PFBFS";
         edtContagemResultado_PFLFS_Internalname = "CONTAGEMRESULTADO_PFLFS";
         edtContagemResultado_PFBFM_Internalname = "CONTAGEMRESULTADO_PFBFM";
         edtContagemResultado_PFLFM_Internalname = "CONTAGEMRESULTADO_PFLFM";
         edtContagemResultado_Divergencia_Internalname = "CONTAGEMRESULTADO_DIVERGENCIA";
         edtContagemResultado_ParecerTcn_Internalname = "CONTAGEMRESULTADO_PARECERTCN";
         edtContagemResultado_NaoCnfCntCod_Internalname = "CONTAGEMRESULTADO_NAOCNFCNTCOD";
         cmbContagemResultado_StatusCnt_Internalname = "CONTAGEMRESULTADO_STATUSCNT";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContagemResultado_HoraCnt_Internalname = "CONTAGEMRESULTADO_HORACNT";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontagemresultado_datacnt_Internalname = "vTFCONTAGEMRESULTADO_DATACNT";
         edtavTfcontagemresultado_datacnt_to_Internalname = "vTFCONTAGEMRESULTADO_DATACNT_TO";
         edtavDdo_contagemresultado_datacntauxdate_Internalname = "vDDO_CONTAGEMRESULTADO_DATACNTAUXDATE";
         edtavDdo_contagemresultado_datacntauxdateto_Internalname = "vDDO_CONTAGEMRESULTADO_DATACNTAUXDATETO";
         divDdo_contagemresultado_datacntauxdates_Internalname = "DDO_CONTAGEMRESULTADO_DATACNTAUXDATES";
         edtavTfcontagemresultado_contadorfmcod_Internalname = "vTFCONTAGEMRESULTADO_CONTADORFMCOD";
         edtavTfcontagemresultado_contadorfmcod_to_Internalname = "vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO";
         edtavTfcontagemresultado_pfbfs_Internalname = "vTFCONTAGEMRESULTADO_PFBFS";
         edtavTfcontagemresultado_pfbfs_to_Internalname = "vTFCONTAGEMRESULTADO_PFBFS_TO";
         edtavTfcontagemresultado_pflfs_Internalname = "vTFCONTAGEMRESULTADO_PFLFS";
         edtavTfcontagemresultado_pflfs_to_Internalname = "vTFCONTAGEMRESULTADO_PFLFS_TO";
         edtavTfcontagemresultado_pfbfm_Internalname = "vTFCONTAGEMRESULTADO_PFBFM";
         edtavTfcontagemresultado_pfbfm_to_Internalname = "vTFCONTAGEMRESULTADO_PFBFM_TO";
         edtavTfcontagemresultado_pflfm_Internalname = "vTFCONTAGEMRESULTADO_PFLFM";
         edtavTfcontagemresultado_pflfm_to_Internalname = "vTFCONTAGEMRESULTADO_PFLFM_TO";
         edtavTfcontagemresultado_divergencia_Internalname = "vTFCONTAGEMRESULTADO_DIVERGENCIA";
         edtavTfcontagemresultado_divergencia_to_Internalname = "vTFCONTAGEMRESULTADO_DIVERGENCIA_TO";
         edtavTfcontagemresultado_parecertcn_Internalname = "vTFCONTAGEMRESULTADO_PARECERTCN";
         edtavTfcontagemresultado_parecertcn_sel_Internalname = "vTFCONTAGEMRESULTADO_PARECERTCN_SEL";
         edtavTfcontagemresultado_naocnfcntcod_Internalname = "vTFCONTAGEMRESULTADO_NAOCNFCNTCOD";
         edtavTfcontagemresultado_naocnfcntcod_to_Internalname = "vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO";
         Ddo_contagemresultado_datacnt_Internalname = "DDO_CONTAGEMRESULTADO_DATACNT";
         edtavDdo_contagemresultado_datacnttitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_contadorfmcod_Internalname = "DDO_CONTAGEMRESULTADO_CONTADORFMCOD";
         edtavDdo_contagemresultado_contadorfmcodtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_pfbfs_Internalname = "DDO_CONTAGEMRESULTADO_PFBFS";
         edtavDdo_contagemresultado_pfbfstitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_pflfs_Internalname = "DDO_CONTAGEMRESULTADO_PFLFS";
         edtavDdo_contagemresultado_pflfstitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_pfbfm_Internalname = "DDO_CONTAGEMRESULTADO_PFBFM";
         edtavDdo_contagemresultado_pfbfmtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_pflfm_Internalname = "DDO_CONTAGEMRESULTADO_PFLFM";
         edtavDdo_contagemresultado_pflfmtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_divergencia_Internalname = "DDO_CONTAGEMRESULTADO_DIVERGENCIA";
         edtavDdo_contagemresultado_divergenciatitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_parecertcn_Internalname = "DDO_CONTAGEMRESULTADO_PARECERTCN";
         edtavDdo_contagemresultado_parecertcntitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_naocnfcntcod_Internalname = "DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD";
         edtavDdo_contagemresultado_naocnfcntcodtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_statuscnt_Internalname = "DDO_CONTAGEMRESULTADO_STATUSCNT";
         edtavDdo_contagemresultado_statuscnttitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbContagemResultado_StatusCnt_Jsonclick = "";
         edtContagemResultado_NaoCnfCntCod_Jsonclick = "";
         edtContagemResultado_ParecerTcn_Jsonclick = "";
         edtContagemResultado_Divergencia_Jsonclick = "";
         edtContagemResultado_PFLFM_Jsonclick = "";
         edtContagemResultado_PFBFM_Jsonclick = "";
         edtContagemResultado_PFLFS_Jsonclick = "";
         edtContagemResultado_PFBFS_Jsonclick = "";
         edtContagemResultado_ContadorFMCod_Jsonclick = "";
         edtContagemResultado_DataCnt_Jsonclick = "";
         edtContagemResultado_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContagemresultado_datacnt_to1_Jsonclick = "";
         edtavContagemresultado_datacnt1_Jsonclick = "";
         edtavContagemresultado_datacnt_to2_Jsonclick = "";
         edtavContagemresultado_datacnt2_Jsonclick = "";
         edtavContagemresultado_datacnt_to3_Jsonclick = "";
         edtavContagemresultado_datacnt3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         cmbContagemResultado_StatusCnt_Titleformat = 0;
         edtContagemResultado_NaoCnfCntCod_Titleformat = 0;
         edtContagemResultado_ParecerTcn_Titleformat = 0;
         edtContagemResultado_Divergencia_Titleformat = 0;
         edtContagemResultado_PFLFM_Titleformat = 0;
         edtContagemResultado_PFBFM_Titleformat = 0;
         edtContagemResultado_PFLFS_Titleformat = 0;
         edtContagemResultado_PFBFS_Titleformat = 0;
         edtContagemResultado_ContadorFMCod_Titleformat = 0;
         edtContagemResultado_DataCnt_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfilterscontagemresultado_datacnt3_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_datacnt2_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_datacnt1_Visible = 1;
         cmbContagemResultado_StatusCnt.Title.Text = "Status";
         edtContagemResultado_NaoCnfCntCod_Title = "conformidade";
         edtContagemResultado_ParecerTcn_Title = "t�cnico";
         edtContagemResultado_Divergencia_Title = "Diverg�ncia";
         edtContagemResultado_PFLFM_Title = "L�quido FM";
         edtContagemResultado_PFBFM_Title = "Bruto FM";
         edtContagemResultado_PFLFS_Title = "L�quido FS";
         edtContagemResultado_PFBFS_Title = "Bruto FS";
         edtContagemResultado_ContadorFMCod_Title = "FM";
         edtContagemResultado_DataCnt_Title = "Data";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contagemresultado_statuscnttitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_naocnfcntcodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_parecertcntitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_divergenciatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_pflfmtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_pfbfmtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_pflfstitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_pfbfstitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_contadorfmcodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_datacnttitlecontrolidtoreplace_Visible = 1;
         edtavTfcontagemresultado_naocnfcntcod_to_Jsonclick = "";
         edtavTfcontagemresultado_naocnfcntcod_to_Visible = 1;
         edtavTfcontagemresultado_naocnfcntcod_Jsonclick = "";
         edtavTfcontagemresultado_naocnfcntcod_Visible = 1;
         edtavTfcontagemresultado_parecertcn_sel_Visible = 1;
         edtavTfcontagemresultado_parecertcn_Visible = 1;
         edtavTfcontagemresultado_divergencia_to_Jsonclick = "";
         edtavTfcontagemresultado_divergencia_to_Visible = 1;
         edtavTfcontagemresultado_divergencia_Jsonclick = "";
         edtavTfcontagemresultado_divergencia_Visible = 1;
         edtavTfcontagemresultado_pflfm_to_Jsonclick = "";
         edtavTfcontagemresultado_pflfm_to_Visible = 1;
         edtavTfcontagemresultado_pflfm_Jsonclick = "";
         edtavTfcontagemresultado_pflfm_Visible = 1;
         edtavTfcontagemresultado_pfbfm_to_Jsonclick = "";
         edtavTfcontagemresultado_pfbfm_to_Visible = 1;
         edtavTfcontagemresultado_pfbfm_Jsonclick = "";
         edtavTfcontagemresultado_pfbfm_Visible = 1;
         edtavTfcontagemresultado_pflfs_to_Jsonclick = "";
         edtavTfcontagemresultado_pflfs_to_Visible = 1;
         edtavTfcontagemresultado_pflfs_Jsonclick = "";
         edtavTfcontagemresultado_pflfs_Visible = 1;
         edtavTfcontagemresultado_pfbfs_to_Jsonclick = "";
         edtavTfcontagemresultado_pfbfs_to_Visible = 1;
         edtavTfcontagemresultado_pfbfs_Jsonclick = "";
         edtavTfcontagemresultado_pfbfs_Visible = 1;
         edtavTfcontagemresultado_contadorfmcod_to_Jsonclick = "";
         edtavTfcontagemresultado_contadorfmcod_to_Visible = 1;
         edtavTfcontagemresultado_contadorfmcod_Jsonclick = "";
         edtavTfcontagemresultado_contadorfmcod_Visible = 1;
         edtavDdo_contagemresultado_datacntauxdateto_Jsonclick = "";
         edtavDdo_contagemresultado_datacntauxdate_Jsonclick = "";
         edtavTfcontagemresultado_datacnt_to_Jsonclick = "";
         edtavTfcontagemresultado_datacnt_to_Visible = 1;
         edtavTfcontagemresultado_datacnt_Jsonclick = "";
         edtavTfcontagemresultado_datacnt_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtContagemResultado_HoraCnt_Jsonclick = "";
         edtContagemResultado_HoraCnt_Visible = 1;
         Ddo_contagemresultado_statuscnt_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contagemresultado_statuscnt_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_statuscnt_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_statuscnt_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_statuscnt_Datalistfixedvalues = "1:Inicial,2:Auditoria,3:Negociac�o,4:N�o Aprovada,5:Entregue,6:Pend�ncias,7:Diverg�ncia,8:Aprovada";
         Ddo_contagemresultado_statuscnt_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contagemresultado_statuscnt_Datalisttype = "FixedValues";
         Ddo_contagemresultado_statuscnt_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_statuscnt_Includefilter = Convert.ToBoolean( 0);
         Ddo_contagemresultado_statuscnt_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_statuscnt_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_statuscnt_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_statuscnt_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_statuscnt_Cls = "ColumnSettings";
         Ddo_contagemresultado_statuscnt_Tooltip = "Op��es";
         Ddo_contagemresultado_statuscnt_Caption = "";
         Ddo_contagemresultado_naocnfcntcod_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_naocnfcntcod_Rangefilterto = "At�";
         Ddo_contagemresultado_naocnfcntcod_Rangefilterfrom = "Desde";
         Ddo_contagemresultado_naocnfcntcod_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_naocnfcntcod_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_naocnfcntcod_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_naocnfcntcod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultado_naocnfcntcod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultado_naocnfcntcod_Filtertype = "Numeric";
         Ddo_contagemresultado_naocnfcntcod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_naocnfcntcod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_naocnfcntcod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_naocnfcntcod_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_naocnfcntcod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_naocnfcntcod_Cls = "ColumnSettings";
         Ddo_contagemresultado_naocnfcntcod_Tooltip = "Op��es";
         Ddo_contagemresultado_naocnfcntcod_Caption = "";
         Ddo_contagemresultado_parecertcn_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_parecertcn_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_parecertcn_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_parecertcn_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_parecertcn_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_parecertcn_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_parecertcn_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_parecertcn_Datalistproc = "GetPromptContagemResultadoContagensFilterData";
         Ddo_contagemresultado_parecertcn_Datalisttype = "Dynamic";
         Ddo_contagemresultado_parecertcn_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_parecertcn_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_parecertcn_Filtertype = "Character";
         Ddo_contagemresultado_parecertcn_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_parecertcn_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_parecertcn_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_parecertcn_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_parecertcn_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_parecertcn_Cls = "ColumnSettings";
         Ddo_contagemresultado_parecertcn_Tooltip = "Op��es";
         Ddo_contagemresultado_parecertcn_Caption = "";
         Ddo_contagemresultado_divergencia_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_divergencia_Rangefilterto = "At�";
         Ddo_contagemresultado_divergencia_Rangefilterfrom = "Desde";
         Ddo_contagemresultado_divergencia_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_divergencia_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_divergencia_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_divergencia_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultado_divergencia_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultado_divergencia_Filtertype = "Numeric";
         Ddo_contagemresultado_divergencia_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_divergencia_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_divergencia_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_divergencia_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_divergencia_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_divergencia_Cls = "ColumnSettings";
         Ddo_contagemresultado_divergencia_Tooltip = "Op��es";
         Ddo_contagemresultado_divergencia_Caption = "";
         Ddo_contagemresultado_pflfm_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_pflfm_Rangefilterto = "At�";
         Ddo_contagemresultado_pflfm_Rangefilterfrom = "Desde";
         Ddo_contagemresultado_pflfm_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_pflfm_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_pflfm_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_pflfm_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultado_pflfm_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pflfm_Filtertype = "Numeric";
         Ddo_contagemresultado_pflfm_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pflfm_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pflfm_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pflfm_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_pflfm_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_pflfm_Cls = "ColumnSettings";
         Ddo_contagemresultado_pflfm_Tooltip = "Op��es";
         Ddo_contagemresultado_pflfm_Caption = "";
         Ddo_contagemresultado_pfbfm_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_pfbfm_Rangefilterto = "At�";
         Ddo_contagemresultado_pfbfm_Rangefilterfrom = "Desde";
         Ddo_contagemresultado_pfbfm_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_pfbfm_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_pfbfm_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_pfbfm_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultado_pfbfm_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pfbfm_Filtertype = "Numeric";
         Ddo_contagemresultado_pfbfm_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pfbfm_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pfbfm_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pfbfm_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_pfbfm_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_pfbfm_Cls = "ColumnSettings";
         Ddo_contagemresultado_pfbfm_Tooltip = "Op��es";
         Ddo_contagemresultado_pfbfm_Caption = "";
         Ddo_contagemresultado_pflfs_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_pflfs_Rangefilterto = "At�";
         Ddo_contagemresultado_pflfs_Rangefilterfrom = "Desde";
         Ddo_contagemresultado_pflfs_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_pflfs_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_pflfs_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_pflfs_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultado_pflfs_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pflfs_Filtertype = "Numeric";
         Ddo_contagemresultado_pflfs_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pflfs_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pflfs_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pflfs_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_pflfs_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_pflfs_Cls = "ColumnSettings";
         Ddo_contagemresultado_pflfs_Tooltip = "Op��es";
         Ddo_contagemresultado_pflfs_Caption = "";
         Ddo_contagemresultado_pfbfs_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_pfbfs_Rangefilterto = "At�";
         Ddo_contagemresultado_pfbfs_Rangefilterfrom = "Desde";
         Ddo_contagemresultado_pfbfs_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_pfbfs_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_pfbfs_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_pfbfs_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultado_pfbfs_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pfbfs_Filtertype = "Numeric";
         Ddo_contagemresultado_pfbfs_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pfbfs_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pfbfs_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_pfbfs_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_pfbfs_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_pfbfs_Cls = "ColumnSettings";
         Ddo_contagemresultado_pfbfs_Tooltip = "Op��es";
         Ddo_contagemresultado_pfbfs_Caption = "";
         Ddo_contagemresultado_contadorfmcod_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_contadorfmcod_Rangefilterto = "At�";
         Ddo_contagemresultado_contadorfmcod_Rangefilterfrom = "Desde";
         Ddo_contagemresultado_contadorfmcod_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_contadorfmcod_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_contadorfmcod_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_contadorfmcod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultado_contadorfmcod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultado_contadorfmcod_Filtertype = "Numeric";
         Ddo_contagemresultado_contadorfmcod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_contadorfmcod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_contadorfmcod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_contadorfmcod_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_contadorfmcod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_contadorfmcod_Cls = "ColumnSettings";
         Ddo_contagemresultado_contadorfmcod_Tooltip = "Op��es";
         Ddo_contagemresultado_contadorfmcod_Caption = "";
         Ddo_contagemresultado_datacnt_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_datacnt_Rangefilterto = "At�";
         Ddo_contagemresultado_datacnt_Rangefilterfrom = "Desde";
         Ddo_contagemresultado_datacnt_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_datacnt_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_datacnt_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_datacnt_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultado_datacnt_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultado_datacnt_Filtertype = "Date";
         Ddo_contagemresultado_datacnt_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_datacnt_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_datacnt_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_datacnt_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_datacnt_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_datacnt_Cls = "ColumnSettings";
         Ddo_contagemresultado_datacnt_Tooltip = "Op��es";
         Ddo_contagemresultado_datacnt_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contagem Resultado Contagens";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''}],oparms:[{av:'AV39ContagemResultado_DataCntTitleFilterData',fld:'vCONTAGEMRESULTADO_DATACNTTITLEFILTERDATA',pic:'',nv:null},{av:'AV45ContagemResultado_ContadorFMCodTitleFilterData',fld:'vCONTAGEMRESULTADO_CONTADORFMCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV49ContagemResultado_PFBFSTitleFilterData',fld:'vCONTAGEMRESULTADO_PFBFSTITLEFILTERDATA',pic:'',nv:null},{av:'AV53ContagemResultado_PFLFSTitleFilterData',fld:'vCONTAGEMRESULTADO_PFLFSTITLEFILTERDATA',pic:'',nv:null},{av:'AV57ContagemResultado_PFBFMTitleFilterData',fld:'vCONTAGEMRESULTADO_PFBFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV61ContagemResultado_PFLFMTitleFilterData',fld:'vCONTAGEMRESULTADO_PFLFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV65ContagemResultado_DivergenciaTitleFilterData',fld:'vCONTAGEMRESULTADO_DIVERGENCIATITLEFILTERDATA',pic:'',nv:null},{av:'AV69ContagemResultado_ParecerTcnTitleFilterData',fld:'vCONTAGEMRESULTADO_PARECERTCNTITLEFILTERDATA',pic:'',nv:null},{av:'AV73ContagemResultado_NaoCnfCntCodTitleFilterData',fld:'vCONTAGEMRESULTADO_NAOCNFCNTCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV77ContagemResultado_StatusCntTitleFilterData',fld:'vCONTAGEMRESULTADO_STATUSCNTTITLEFILTERDATA',pic:'',nv:null},{av:'edtContagemResultado_DataCnt_Titleformat',ctrl:'CONTAGEMRESULTADO_DATACNT',prop:'Titleformat'},{av:'edtContagemResultado_DataCnt_Title',ctrl:'CONTAGEMRESULTADO_DATACNT',prop:'Title'},{av:'edtContagemResultado_ContadorFMCod_Titleformat',ctrl:'CONTAGEMRESULTADO_CONTADORFMCOD',prop:'Titleformat'},{av:'edtContagemResultado_ContadorFMCod_Title',ctrl:'CONTAGEMRESULTADO_CONTADORFMCOD',prop:'Title'},{av:'edtContagemResultado_PFBFS_Titleformat',ctrl:'CONTAGEMRESULTADO_PFBFS',prop:'Titleformat'},{av:'edtContagemResultado_PFBFS_Title',ctrl:'CONTAGEMRESULTADO_PFBFS',prop:'Title'},{av:'edtContagemResultado_PFLFS_Titleformat',ctrl:'CONTAGEMRESULTADO_PFLFS',prop:'Titleformat'},{av:'edtContagemResultado_PFLFS_Title',ctrl:'CONTAGEMRESULTADO_PFLFS',prop:'Title'},{av:'edtContagemResultado_PFBFM_Titleformat',ctrl:'CONTAGEMRESULTADO_PFBFM',prop:'Titleformat'},{av:'edtContagemResultado_PFBFM_Title',ctrl:'CONTAGEMRESULTADO_PFBFM',prop:'Title'},{av:'edtContagemResultado_PFLFM_Titleformat',ctrl:'CONTAGEMRESULTADO_PFLFM',prop:'Titleformat'},{av:'edtContagemResultado_PFLFM_Title',ctrl:'CONTAGEMRESULTADO_PFLFM',prop:'Title'},{av:'edtContagemResultado_Divergencia_Titleformat',ctrl:'CONTAGEMRESULTADO_DIVERGENCIA',prop:'Titleformat'},{av:'edtContagemResultado_Divergencia_Title',ctrl:'CONTAGEMRESULTADO_DIVERGENCIA',prop:'Title'},{av:'edtContagemResultado_ParecerTcn_Titleformat',ctrl:'CONTAGEMRESULTADO_PARECERTCN',prop:'Titleformat'},{av:'edtContagemResultado_ParecerTcn_Title',ctrl:'CONTAGEMRESULTADO_PARECERTCN',prop:'Title'},{av:'edtContagemResultado_NaoCnfCntCod_Titleformat',ctrl:'CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'Titleformat'},{av:'edtContagemResultado_NaoCnfCntCod_Title',ctrl:'CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'Title'},{av:'cmbContagemResultado_StatusCnt'},{av:'AV83GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV84GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_DATACNT.ONOPTIONCLICKED","{handler:'E12B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_datacnt_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_datacnt_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_datacnt_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_datacnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'SortedStatus'},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'Ddo_contagemresultado_contadorfmcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_divergencia_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_parecertcn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_naocnfcntcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statuscnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSCNT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_CONTADORFMCOD.ONOPTIONCLICKED","{handler:'E13B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_contadorfmcod_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_contadorfmcod_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_contadorfmcod_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_contadorfmcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'SortedStatus'},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_datacnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_divergencia_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_parecertcn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_naocnfcntcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statuscnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSCNT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_PFBFS.ONOPTIONCLICKED","{handler:'E14B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_pfbfs_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_pfbfs_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_pfbfs_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_pfbfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'SortedStatus'},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemresultado_datacnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contadorfmcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_divergencia_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_parecertcn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_naocnfcntcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statuscnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSCNT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_PFLFS.ONOPTIONCLICKED","{handler:'E15B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_pflfs_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_pflfs_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_pflfs_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_pflfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'SortedStatus'},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemresultado_datacnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contadorfmcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_divergencia_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_parecertcn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_naocnfcntcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statuscnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSCNT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_PFBFM.ONOPTIONCLICKED","{handler:'E16B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_pfbfm_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_pfbfm_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_pfbfm_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_pfbfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'SortedStatus'},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemresultado_datacnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contadorfmcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_divergencia_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_parecertcn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_naocnfcntcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statuscnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSCNT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_PFLFM.ONOPTIONCLICKED","{handler:'E17B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_pflfm_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_pflfm_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_pflfm_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_pflfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'SortedStatus'},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemresultado_datacnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contadorfmcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_divergencia_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_parecertcn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_naocnfcntcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statuscnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSCNT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_DIVERGENCIA.ONOPTIONCLICKED","{handler:'E18B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_divergencia_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_divergencia_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_divergencia_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_divergencia_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'SortedStatus'},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'Ddo_contagemresultado_datacnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contadorfmcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_parecertcn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_naocnfcntcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statuscnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSCNT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_PARECERTCN.ONOPTIONCLICKED","{handler:'E19B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_parecertcn_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_parecertcn_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_parecertcn_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_parecertcn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'SortedStatus'},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'Ddo_contagemresultado_datacnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contadorfmcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_divergencia_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_naocnfcntcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statuscnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSCNT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD.ONOPTIONCLICKED","{handler:'E20B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_naocnfcntcod_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_naocnfcntcod_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_naocnfcntcod_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_naocnfcntcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'SortedStatus'},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_datacnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contadorfmcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_divergencia_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_parecertcn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statuscnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSCNT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_STATUSCNT.ONOPTIONCLICKED","{handler:'E21B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_statuscnt_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_STATUSCNT',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_statuscnt_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_STATUSCNT',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_statuscnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSCNT',prop:'SortedStatus'},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'Ddo_contagemresultado_datacnt_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contadorfmcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfs_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pfbfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_pflfm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_divergencia_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_parecertcn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_naocnfcntcod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E34B92',iparms:[],oparms:[{av:'AV29Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E35B92',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',hsh:true,nv:''},{av:'A511ContagemResultado_HoraCnt',fld:'CONTAGEMRESULTADO_HORACNT',pic:'',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0}],oparms:[{av:'AV7InOutContagemResultado_Codigo',fld:'vINOUTCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContagemResultado_DataCnt',fld:'vINOUTCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV38InOutContagemResultado_HoraCnt',fld:'vINOUTCONTAGEMRESULTADO_HORACNT',pic:'',nv:''},{av:'AV9InOutContagemResultado_PFBFS',fld:'vINOUTCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E22B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E27B92',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E23B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E28B92',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E29B92',iparms:[],oparms:[{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E24B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E30B92',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E25B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E31B92',iparms:[{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E26B92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATACNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTADORFMCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFBFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PFLFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DIVERGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_PARECERTCNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_NAOCNFCNTCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSCNTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV40TFContagemResultado_DataCnt',fld:'vTFCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'Ddo_contagemresultado_datacnt_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'FilteredText_set'},{av:'AV41TFContagemResultado_DataCnt_To',fld:'vTFCONTAGEMRESULTADO_DATACNT_TO',pic:'',nv:''},{av:'Ddo_contagemresultado_datacnt_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADO_DATACNT',prop:'FilteredTextTo_set'},{av:'AV46TFContagemResultado_ContadorFMCod',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_contadorfmcod_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'FilteredText_set'},{av:'AV47TFContagemResultado_ContadorFMCod_To',fld:'vTFCONTAGEMRESULTADO_CONTADORFMCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_contadorfmcod_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADO_CONTADORFMCOD',prop:'FilteredTextTo_set'},{av:'AV50TFContagemResultado_PFBFS',fld:'vTFCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemresultado_pfbfs_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'FilteredText_set'},{av:'AV51TFContagemResultado_PFBFS_To',fld:'vTFCONTAGEMRESULTADO_PFBFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemresultado_pfbfs_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADO_PFBFS',prop:'FilteredTextTo_set'},{av:'AV54TFContagemResultado_PFLFS',fld:'vTFCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemresultado_pflfs_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'FilteredText_set'},{av:'AV55TFContagemResultado_PFLFS_To',fld:'vTFCONTAGEMRESULTADO_PFLFS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemresultado_pflfs_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADO_PFLFS',prop:'FilteredTextTo_set'},{av:'AV58TFContagemResultado_PFBFM',fld:'vTFCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemresultado_pfbfm_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'FilteredText_set'},{av:'AV59TFContagemResultado_PFBFM_To',fld:'vTFCONTAGEMRESULTADO_PFBFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemresultado_pfbfm_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADO_PFBFM',prop:'FilteredTextTo_set'},{av:'AV62TFContagemResultado_PFLFM',fld:'vTFCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemresultado_pflfm_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'FilteredText_set'},{av:'AV63TFContagemResultado_PFLFM_To',fld:'vTFCONTAGEMRESULTADO_PFLFM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemresultado_pflfm_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADO_PFLFM',prop:'FilteredTextTo_set'},{av:'AV66TFContagemResultado_Divergencia',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'Ddo_contagemresultado_divergencia_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'FilteredText_set'},{av:'AV67TFContagemResultado_Divergencia_To',fld:'vTFCONTAGEMRESULTADO_DIVERGENCIA_TO',pic:'ZZ9.99',nv:0.0},{av:'Ddo_contagemresultado_divergencia_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADO_DIVERGENCIA',prop:'FilteredTextTo_set'},{av:'AV70TFContagemResultado_ParecerTcn',fld:'vTFCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'Ddo_contagemresultado_parecertcn_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'FilteredText_set'},{av:'AV71TFContagemResultado_ParecerTcn_Sel',fld:'vTFCONTAGEMRESULTADO_PARECERTCN_SEL',pic:'',nv:''},{av:'Ddo_contagemresultado_parecertcn_Selectedvalue_set',ctrl:'DDO_CONTAGEMRESULTADO_PARECERTCN',prop:'SelectedValue_set'},{av:'AV74TFContagemResultado_NaoCnfCntCod',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_naocnfcntcod_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'FilteredText_set'},{av:'AV75TFContagemResultado_NaoCnfCntCod_To',fld:'vTFCONTAGEMRESULTADO_NAOCNFCNTCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_naocnfcntcod_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADO_NAOCNFCNTCOD',prop:'FilteredTextTo_set'},{av:'AV79TFContagemResultado_StatusCnt_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSCNT_SELS',pic:'',nv:null},{av:'Ddo_contagemresultado_statuscnt_Selectedvalue_set',ctrl:'DDO_CONTAGEMRESULTADO_STATUSCNT',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultado_DataCnt1',fld:'vCONTAGEMRESULTADO_DATACNT1',pic:'',nv:''},{av:'AV33ContagemResultado_DataCnt_To1',fld:'vCONTAGEMRESULTADO_DATACNT_TO1',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT1',prop:'Visible'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_DataCnt2',fld:'vCONTAGEMRESULTADO_DATACNT2',pic:'',nv:''},{av:'AV35ContagemResultado_DataCnt_To2',fld:'vCONTAGEMRESULTADO_DATACNT_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV36ContagemResultado_DataCnt3',fld:'vCONTAGEMRESULTADO_DATACNT3',pic:'',nv:''},{av:'AV37ContagemResultado_DataCnt_To3',fld:'vCONTAGEMRESULTADO_DATACNT_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datacnt3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATACNT3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutContagemResultado_DataCnt = DateTime.MinValue;
         wcpOAV38InOutContagemResultado_HoraCnt = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_contagemresultado_datacnt_Activeeventkey = "";
         Ddo_contagemresultado_datacnt_Filteredtext_get = "";
         Ddo_contagemresultado_datacnt_Filteredtextto_get = "";
         Ddo_contagemresultado_contadorfmcod_Activeeventkey = "";
         Ddo_contagemresultado_contadorfmcod_Filteredtext_get = "";
         Ddo_contagemresultado_contadorfmcod_Filteredtextto_get = "";
         Ddo_contagemresultado_pfbfs_Activeeventkey = "";
         Ddo_contagemresultado_pfbfs_Filteredtext_get = "";
         Ddo_contagemresultado_pfbfs_Filteredtextto_get = "";
         Ddo_contagemresultado_pflfs_Activeeventkey = "";
         Ddo_contagemresultado_pflfs_Filteredtext_get = "";
         Ddo_contagemresultado_pflfs_Filteredtextto_get = "";
         Ddo_contagemresultado_pfbfm_Activeeventkey = "";
         Ddo_contagemresultado_pfbfm_Filteredtext_get = "";
         Ddo_contagemresultado_pfbfm_Filteredtextto_get = "";
         Ddo_contagemresultado_pflfm_Activeeventkey = "";
         Ddo_contagemresultado_pflfm_Filteredtext_get = "";
         Ddo_contagemresultado_pflfm_Filteredtextto_get = "";
         Ddo_contagemresultado_divergencia_Activeeventkey = "";
         Ddo_contagemresultado_divergencia_Filteredtext_get = "";
         Ddo_contagemresultado_divergencia_Filteredtextto_get = "";
         Ddo_contagemresultado_parecertcn_Activeeventkey = "";
         Ddo_contagemresultado_parecertcn_Filteredtext_get = "";
         Ddo_contagemresultado_parecertcn_Selectedvalue_get = "";
         Ddo_contagemresultado_naocnfcntcod_Activeeventkey = "";
         Ddo_contagemresultado_naocnfcntcod_Filteredtext_get = "";
         Ddo_contagemresultado_naocnfcntcod_Filteredtextto_get = "";
         Ddo_contagemresultado_statuscnt_Activeeventkey = "";
         Ddo_contagemresultado_statuscnt_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV32ContagemResultado_DataCnt1 = DateTime.MinValue;
         AV33ContagemResultado_DataCnt_To1 = DateTime.MinValue;
         AV20DynamicFiltersSelector2 = "";
         AV34ContagemResultado_DataCnt2 = DateTime.MinValue;
         AV35ContagemResultado_DataCnt_To2 = DateTime.MinValue;
         AV24DynamicFiltersSelector3 = "";
         AV36ContagemResultado_DataCnt3 = DateTime.MinValue;
         AV37ContagemResultado_DataCnt_To3 = DateTime.MinValue;
         AV40TFContagemResultado_DataCnt = DateTime.MinValue;
         AV41TFContagemResultado_DataCnt_To = DateTime.MinValue;
         AV70TFContagemResultado_ParecerTcn = "";
         AV71TFContagemResultado_ParecerTcn_Sel = "";
         AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace = "";
         AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace = "";
         AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace = "";
         AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace = "";
         AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace = "";
         AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace = "";
         AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace = "";
         AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace = "";
         AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace = "";
         AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace = "";
         AV79TFContagemResultado_StatusCnt_Sels = new GxSimpleCollection();
         AV88Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         A511ContagemResultado_HoraCnt = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV81DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV39ContagemResultado_DataCntTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45ContagemResultado_ContadorFMCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49ContagemResultado_PFBFSTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53ContagemResultado_PFLFSTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57ContagemResultado_PFBFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61ContagemResultado_PFLFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65ContagemResultado_DivergenciaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContagemResultado_ParecerTcnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73ContagemResultado_NaoCnfCntCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV77ContagemResultado_StatusCntTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contagemresultado_datacnt_Filteredtext_set = "";
         Ddo_contagemresultado_datacnt_Filteredtextto_set = "";
         Ddo_contagemresultado_datacnt_Sortedstatus = "";
         Ddo_contagemresultado_contadorfmcod_Filteredtext_set = "";
         Ddo_contagemresultado_contadorfmcod_Filteredtextto_set = "";
         Ddo_contagemresultado_contadorfmcod_Sortedstatus = "";
         Ddo_contagemresultado_pfbfs_Filteredtext_set = "";
         Ddo_contagemresultado_pfbfs_Filteredtextto_set = "";
         Ddo_contagemresultado_pfbfs_Sortedstatus = "";
         Ddo_contagemresultado_pflfs_Filteredtext_set = "";
         Ddo_contagemresultado_pflfs_Filteredtextto_set = "";
         Ddo_contagemresultado_pflfs_Sortedstatus = "";
         Ddo_contagemresultado_pfbfm_Filteredtext_set = "";
         Ddo_contagemresultado_pfbfm_Filteredtextto_set = "";
         Ddo_contagemresultado_pfbfm_Sortedstatus = "";
         Ddo_contagemresultado_pflfm_Filteredtext_set = "";
         Ddo_contagemresultado_pflfm_Filteredtextto_set = "";
         Ddo_contagemresultado_pflfm_Sortedstatus = "";
         Ddo_contagemresultado_divergencia_Filteredtext_set = "";
         Ddo_contagemresultado_divergencia_Filteredtextto_set = "";
         Ddo_contagemresultado_divergencia_Sortedstatus = "";
         Ddo_contagemresultado_parecertcn_Filteredtext_set = "";
         Ddo_contagemresultado_parecertcn_Selectedvalue_set = "";
         Ddo_contagemresultado_parecertcn_Sortedstatus = "";
         Ddo_contagemresultado_naocnfcntcod_Filteredtext_set = "";
         Ddo_contagemresultado_naocnfcntcod_Filteredtextto_set = "";
         Ddo_contagemresultado_naocnfcntcod_Sortedstatus = "";
         Ddo_contagemresultado_statuscnt_Selectedvalue_set = "";
         Ddo_contagemresultado_statuscnt_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV42DDO_ContagemResultado_DataCntAuxDate = DateTime.MinValue;
         AV43DDO_ContagemResultado_DataCntAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV29Select = "";
         AV87Select_GXI = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A463ContagemResultado_ParecerTcn = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV70TFContagemResultado_ParecerTcn = "";
         H00B92_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00B92_A483ContagemResultado_StatusCnt = new short[1] ;
         H00B92_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         H00B92_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         H00B92_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         H00B92_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         H00B92_A462ContagemResultado_Divergencia = new decimal[1] ;
         H00B92_A461ContagemResultado_PFLFM = new decimal[1] ;
         H00B92_n461ContagemResultado_PFLFM = new bool[] {false} ;
         H00B92_A460ContagemResultado_PFBFM = new decimal[1] ;
         H00B92_n460ContagemResultado_PFBFM = new bool[] {false} ;
         H00B92_A459ContagemResultado_PFLFS = new decimal[1] ;
         H00B92_n459ContagemResultado_PFLFS = new bool[] {false} ;
         H00B92_A458ContagemResultado_PFBFS = new decimal[1] ;
         H00B92_n458ContagemResultado_PFBFS = new bool[] {false} ;
         H00B92_A470ContagemResultado_ContadorFMCod = new int[1] ;
         H00B92_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00B92_A456ContagemResultado_Codigo = new int[1] ;
         H00B93_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV78TFContagemResultado_StatusCnt_SelsJson = "";
         GridRow = new GXWebRow();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontagemresultado_datacnt_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontagemresultado_datacnt_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontagemresultado_datacnt_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontagemresultadocontagens__default(),
            new Object[][] {
                new Object[] {
               H00B92_A511ContagemResultado_HoraCnt, H00B92_A483ContagemResultado_StatusCnt, H00B92_A469ContagemResultado_NaoCnfCntCod, H00B92_n469ContagemResultado_NaoCnfCntCod, H00B92_A463ContagemResultado_ParecerTcn, H00B92_n463ContagemResultado_ParecerTcn, H00B92_A462ContagemResultado_Divergencia, H00B92_A461ContagemResultado_PFLFM, H00B92_n461ContagemResultado_PFLFM, H00B92_A460ContagemResultado_PFBFM,
               H00B92_n460ContagemResultado_PFBFM, H00B92_A459ContagemResultado_PFLFS, H00B92_n459ContagemResultado_PFLFS, H00B92_A458ContagemResultado_PFBFS, H00B92_n458ContagemResultado_PFBFS, H00B92_A470ContagemResultado_ContadorFMCod, H00B92_A473ContagemResultado_DataCnt, H00B92_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00B93_AGRID_nRecordCount
               }
            }
         );
         AV88Pgmname = "PromptContagemResultadoContagens";
         /* GeneXus formulas. */
         AV88Pgmname = "PromptContagemResultadoContagens";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_86 ;
      private short nGXsfl_86_idx=1 ;
      private short AV14OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A483ContagemResultado_StatusCnt ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_86_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemResultado_DataCnt_Titleformat ;
      private short edtContagemResultado_ContadorFMCod_Titleformat ;
      private short edtContagemResultado_PFBFS_Titleformat ;
      private short edtContagemResultado_PFLFS_Titleformat ;
      private short edtContagemResultado_PFBFM_Titleformat ;
      private short edtContagemResultado_PFLFM_Titleformat ;
      private short edtContagemResultado_Divergencia_Titleformat ;
      private short edtContagemResultado_ParecerTcn_Titleformat ;
      private short edtContagemResultado_NaoCnfCntCod_Titleformat ;
      private short cmbContagemResultado_StatusCnt_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContagemResultado_Codigo ;
      private int wcpOAV7InOutContagemResultado_Codigo ;
      private int subGrid_Rows ;
      private int AV46TFContagemResultado_ContadorFMCod ;
      private int AV47TFContagemResultado_ContadorFMCod_To ;
      private int AV74TFContagemResultado_NaoCnfCntCod ;
      private int AV75TFContagemResultado_NaoCnfCntCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contagemresultado_parecertcn_Datalistupdateminimumcharacters ;
      private int edtContagemResultado_HoraCnt_Visible ;
      private int edtavTfcontagemresultado_datacnt_Visible ;
      private int edtavTfcontagemresultado_datacnt_to_Visible ;
      private int edtavTfcontagemresultado_contadorfmcod_Visible ;
      private int edtavTfcontagemresultado_contadorfmcod_to_Visible ;
      private int edtavTfcontagemresultado_pfbfs_Visible ;
      private int edtavTfcontagemresultado_pfbfs_to_Visible ;
      private int edtavTfcontagemresultado_pflfs_Visible ;
      private int edtavTfcontagemresultado_pflfs_to_Visible ;
      private int edtavTfcontagemresultado_pfbfm_Visible ;
      private int edtavTfcontagemresultado_pfbfm_to_Visible ;
      private int edtavTfcontagemresultado_pflfm_Visible ;
      private int edtavTfcontagemresultado_pflfm_to_Visible ;
      private int edtavTfcontagemresultado_divergencia_Visible ;
      private int edtavTfcontagemresultado_divergencia_to_Visible ;
      private int edtavTfcontagemresultado_parecertcn_Visible ;
      private int edtavTfcontagemresultado_parecertcn_sel_Visible ;
      private int edtavTfcontagemresultado_naocnfcntcod_Visible ;
      private int edtavTfcontagemresultado_naocnfcntcod_to_Visible ;
      private int edtavDdo_contagemresultado_datacnttitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_contadorfmcodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_pfbfstitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_pflfstitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_pfbfmtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_pflfmtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_divergenciatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_parecertcntitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_naocnfcntcodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_statuscnttitlecontrolidtoreplace_Visible ;
      private int A456ContagemResultado_Codigo ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV79TFContagemResultado_StatusCnt_Sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int AV82PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_datacnt1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_datacnt2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_datacnt3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV83GridCurrentPage ;
      private long AV84GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV9InOutContagemResultado_PFBFS ;
      private decimal wcpOAV9InOutContagemResultado_PFBFS ;
      private decimal AV50TFContagemResultado_PFBFS ;
      private decimal AV51TFContagemResultado_PFBFS_To ;
      private decimal AV54TFContagemResultado_PFLFS ;
      private decimal AV55TFContagemResultado_PFLFS_To ;
      private decimal AV58TFContagemResultado_PFBFM ;
      private decimal AV59TFContagemResultado_PFBFM_To ;
      private decimal AV62TFContagemResultado_PFLFM ;
      private decimal AV63TFContagemResultado_PFLFM_To ;
      private decimal AV66TFContagemResultado_Divergencia ;
      private decimal AV67TFContagemResultado_Divergencia_To ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A462ContagemResultado_Divergencia ;
      private String AV38InOutContagemResultado_HoraCnt ;
      private String wcpOAV38InOutContagemResultado_HoraCnt ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contagemresultado_datacnt_Activeeventkey ;
      private String Ddo_contagemresultado_datacnt_Filteredtext_get ;
      private String Ddo_contagemresultado_datacnt_Filteredtextto_get ;
      private String Ddo_contagemresultado_contadorfmcod_Activeeventkey ;
      private String Ddo_contagemresultado_contadorfmcod_Filteredtext_get ;
      private String Ddo_contagemresultado_contadorfmcod_Filteredtextto_get ;
      private String Ddo_contagemresultado_pfbfs_Activeeventkey ;
      private String Ddo_contagemresultado_pfbfs_Filteredtext_get ;
      private String Ddo_contagemresultado_pfbfs_Filteredtextto_get ;
      private String Ddo_contagemresultado_pflfs_Activeeventkey ;
      private String Ddo_contagemresultado_pflfs_Filteredtext_get ;
      private String Ddo_contagemresultado_pflfs_Filteredtextto_get ;
      private String Ddo_contagemresultado_pfbfm_Activeeventkey ;
      private String Ddo_contagemresultado_pfbfm_Filteredtext_get ;
      private String Ddo_contagemresultado_pfbfm_Filteredtextto_get ;
      private String Ddo_contagemresultado_pflfm_Activeeventkey ;
      private String Ddo_contagemresultado_pflfm_Filteredtext_get ;
      private String Ddo_contagemresultado_pflfm_Filteredtextto_get ;
      private String Ddo_contagemresultado_divergencia_Activeeventkey ;
      private String Ddo_contagemresultado_divergencia_Filteredtext_get ;
      private String Ddo_contagemresultado_divergencia_Filteredtextto_get ;
      private String Ddo_contagemresultado_parecertcn_Activeeventkey ;
      private String Ddo_contagemresultado_parecertcn_Filteredtext_get ;
      private String Ddo_contagemresultado_parecertcn_Selectedvalue_get ;
      private String Ddo_contagemresultado_naocnfcntcod_Activeeventkey ;
      private String Ddo_contagemresultado_naocnfcntcod_Filteredtext_get ;
      private String Ddo_contagemresultado_naocnfcntcod_Filteredtextto_get ;
      private String Ddo_contagemresultado_statuscnt_Activeeventkey ;
      private String Ddo_contagemresultado_statuscnt_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_86_idx="0001" ;
      private String AV88Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String A511ContagemResultado_HoraCnt ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contagemresultado_datacnt_Caption ;
      private String Ddo_contagemresultado_datacnt_Tooltip ;
      private String Ddo_contagemresultado_datacnt_Cls ;
      private String Ddo_contagemresultado_datacnt_Filteredtext_set ;
      private String Ddo_contagemresultado_datacnt_Filteredtextto_set ;
      private String Ddo_contagemresultado_datacnt_Dropdownoptionstype ;
      private String Ddo_contagemresultado_datacnt_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_datacnt_Sortedstatus ;
      private String Ddo_contagemresultado_datacnt_Filtertype ;
      private String Ddo_contagemresultado_datacnt_Sortasc ;
      private String Ddo_contagemresultado_datacnt_Sortdsc ;
      private String Ddo_contagemresultado_datacnt_Cleanfilter ;
      private String Ddo_contagemresultado_datacnt_Rangefilterfrom ;
      private String Ddo_contagemresultado_datacnt_Rangefilterto ;
      private String Ddo_contagemresultado_datacnt_Searchbuttontext ;
      private String Ddo_contagemresultado_contadorfmcod_Caption ;
      private String Ddo_contagemresultado_contadorfmcod_Tooltip ;
      private String Ddo_contagemresultado_contadorfmcod_Cls ;
      private String Ddo_contagemresultado_contadorfmcod_Filteredtext_set ;
      private String Ddo_contagemresultado_contadorfmcod_Filteredtextto_set ;
      private String Ddo_contagemresultado_contadorfmcod_Dropdownoptionstype ;
      private String Ddo_contagemresultado_contadorfmcod_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_contadorfmcod_Sortedstatus ;
      private String Ddo_contagemresultado_contadorfmcod_Filtertype ;
      private String Ddo_contagemresultado_contadorfmcod_Sortasc ;
      private String Ddo_contagemresultado_contadorfmcod_Sortdsc ;
      private String Ddo_contagemresultado_contadorfmcod_Cleanfilter ;
      private String Ddo_contagemresultado_contadorfmcod_Rangefilterfrom ;
      private String Ddo_contagemresultado_contadorfmcod_Rangefilterto ;
      private String Ddo_contagemresultado_contadorfmcod_Searchbuttontext ;
      private String Ddo_contagemresultado_pfbfs_Caption ;
      private String Ddo_contagemresultado_pfbfs_Tooltip ;
      private String Ddo_contagemresultado_pfbfs_Cls ;
      private String Ddo_contagemresultado_pfbfs_Filteredtext_set ;
      private String Ddo_contagemresultado_pfbfs_Filteredtextto_set ;
      private String Ddo_contagemresultado_pfbfs_Dropdownoptionstype ;
      private String Ddo_contagemresultado_pfbfs_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_pfbfs_Sortedstatus ;
      private String Ddo_contagemresultado_pfbfs_Filtertype ;
      private String Ddo_contagemresultado_pfbfs_Sortasc ;
      private String Ddo_contagemresultado_pfbfs_Sortdsc ;
      private String Ddo_contagemresultado_pfbfs_Cleanfilter ;
      private String Ddo_contagemresultado_pfbfs_Rangefilterfrom ;
      private String Ddo_contagemresultado_pfbfs_Rangefilterto ;
      private String Ddo_contagemresultado_pfbfs_Searchbuttontext ;
      private String Ddo_contagemresultado_pflfs_Caption ;
      private String Ddo_contagemresultado_pflfs_Tooltip ;
      private String Ddo_contagemresultado_pflfs_Cls ;
      private String Ddo_contagemresultado_pflfs_Filteredtext_set ;
      private String Ddo_contagemresultado_pflfs_Filteredtextto_set ;
      private String Ddo_contagemresultado_pflfs_Dropdownoptionstype ;
      private String Ddo_contagemresultado_pflfs_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_pflfs_Sortedstatus ;
      private String Ddo_contagemresultado_pflfs_Filtertype ;
      private String Ddo_contagemresultado_pflfs_Sortasc ;
      private String Ddo_contagemresultado_pflfs_Sortdsc ;
      private String Ddo_contagemresultado_pflfs_Cleanfilter ;
      private String Ddo_contagemresultado_pflfs_Rangefilterfrom ;
      private String Ddo_contagemresultado_pflfs_Rangefilterto ;
      private String Ddo_contagemresultado_pflfs_Searchbuttontext ;
      private String Ddo_contagemresultado_pfbfm_Caption ;
      private String Ddo_contagemresultado_pfbfm_Tooltip ;
      private String Ddo_contagemresultado_pfbfm_Cls ;
      private String Ddo_contagemresultado_pfbfm_Filteredtext_set ;
      private String Ddo_contagemresultado_pfbfm_Filteredtextto_set ;
      private String Ddo_contagemresultado_pfbfm_Dropdownoptionstype ;
      private String Ddo_contagemresultado_pfbfm_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_pfbfm_Sortedstatus ;
      private String Ddo_contagemresultado_pfbfm_Filtertype ;
      private String Ddo_contagemresultado_pfbfm_Sortasc ;
      private String Ddo_contagemresultado_pfbfm_Sortdsc ;
      private String Ddo_contagemresultado_pfbfm_Cleanfilter ;
      private String Ddo_contagemresultado_pfbfm_Rangefilterfrom ;
      private String Ddo_contagemresultado_pfbfm_Rangefilterto ;
      private String Ddo_contagemresultado_pfbfm_Searchbuttontext ;
      private String Ddo_contagemresultado_pflfm_Caption ;
      private String Ddo_contagemresultado_pflfm_Tooltip ;
      private String Ddo_contagemresultado_pflfm_Cls ;
      private String Ddo_contagemresultado_pflfm_Filteredtext_set ;
      private String Ddo_contagemresultado_pflfm_Filteredtextto_set ;
      private String Ddo_contagemresultado_pflfm_Dropdownoptionstype ;
      private String Ddo_contagemresultado_pflfm_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_pflfm_Sortedstatus ;
      private String Ddo_contagemresultado_pflfm_Filtertype ;
      private String Ddo_contagemresultado_pflfm_Sortasc ;
      private String Ddo_contagemresultado_pflfm_Sortdsc ;
      private String Ddo_contagemresultado_pflfm_Cleanfilter ;
      private String Ddo_contagemresultado_pflfm_Rangefilterfrom ;
      private String Ddo_contagemresultado_pflfm_Rangefilterto ;
      private String Ddo_contagemresultado_pflfm_Searchbuttontext ;
      private String Ddo_contagemresultado_divergencia_Caption ;
      private String Ddo_contagemresultado_divergencia_Tooltip ;
      private String Ddo_contagemresultado_divergencia_Cls ;
      private String Ddo_contagemresultado_divergencia_Filteredtext_set ;
      private String Ddo_contagemresultado_divergencia_Filteredtextto_set ;
      private String Ddo_contagemresultado_divergencia_Dropdownoptionstype ;
      private String Ddo_contagemresultado_divergencia_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_divergencia_Sortedstatus ;
      private String Ddo_contagemresultado_divergencia_Filtertype ;
      private String Ddo_contagemresultado_divergencia_Sortasc ;
      private String Ddo_contagemresultado_divergencia_Sortdsc ;
      private String Ddo_contagemresultado_divergencia_Cleanfilter ;
      private String Ddo_contagemresultado_divergencia_Rangefilterfrom ;
      private String Ddo_contagemresultado_divergencia_Rangefilterto ;
      private String Ddo_contagemresultado_divergencia_Searchbuttontext ;
      private String Ddo_contagemresultado_parecertcn_Caption ;
      private String Ddo_contagemresultado_parecertcn_Tooltip ;
      private String Ddo_contagemresultado_parecertcn_Cls ;
      private String Ddo_contagemresultado_parecertcn_Filteredtext_set ;
      private String Ddo_contagemresultado_parecertcn_Selectedvalue_set ;
      private String Ddo_contagemresultado_parecertcn_Dropdownoptionstype ;
      private String Ddo_contagemresultado_parecertcn_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_parecertcn_Sortedstatus ;
      private String Ddo_contagemresultado_parecertcn_Filtertype ;
      private String Ddo_contagemresultado_parecertcn_Datalisttype ;
      private String Ddo_contagemresultado_parecertcn_Datalistproc ;
      private String Ddo_contagemresultado_parecertcn_Sortasc ;
      private String Ddo_contagemresultado_parecertcn_Sortdsc ;
      private String Ddo_contagemresultado_parecertcn_Loadingdata ;
      private String Ddo_contagemresultado_parecertcn_Cleanfilter ;
      private String Ddo_contagemresultado_parecertcn_Noresultsfound ;
      private String Ddo_contagemresultado_parecertcn_Searchbuttontext ;
      private String Ddo_contagemresultado_naocnfcntcod_Caption ;
      private String Ddo_contagemresultado_naocnfcntcod_Tooltip ;
      private String Ddo_contagemresultado_naocnfcntcod_Cls ;
      private String Ddo_contagemresultado_naocnfcntcod_Filteredtext_set ;
      private String Ddo_contagemresultado_naocnfcntcod_Filteredtextto_set ;
      private String Ddo_contagemresultado_naocnfcntcod_Dropdownoptionstype ;
      private String Ddo_contagemresultado_naocnfcntcod_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_naocnfcntcod_Sortedstatus ;
      private String Ddo_contagemresultado_naocnfcntcod_Filtertype ;
      private String Ddo_contagemresultado_naocnfcntcod_Sortasc ;
      private String Ddo_contagemresultado_naocnfcntcod_Sortdsc ;
      private String Ddo_contagemresultado_naocnfcntcod_Cleanfilter ;
      private String Ddo_contagemresultado_naocnfcntcod_Rangefilterfrom ;
      private String Ddo_contagemresultado_naocnfcntcod_Rangefilterto ;
      private String Ddo_contagemresultado_naocnfcntcod_Searchbuttontext ;
      private String Ddo_contagemresultado_statuscnt_Caption ;
      private String Ddo_contagemresultado_statuscnt_Tooltip ;
      private String Ddo_contagemresultado_statuscnt_Cls ;
      private String Ddo_contagemresultado_statuscnt_Selectedvalue_set ;
      private String Ddo_contagemresultado_statuscnt_Dropdownoptionstype ;
      private String Ddo_contagemresultado_statuscnt_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_statuscnt_Sortedstatus ;
      private String Ddo_contagemresultado_statuscnt_Datalisttype ;
      private String Ddo_contagemresultado_statuscnt_Datalistfixedvalues ;
      private String Ddo_contagemresultado_statuscnt_Sortasc ;
      private String Ddo_contagemresultado_statuscnt_Sortdsc ;
      private String Ddo_contagemresultado_statuscnt_Cleanfilter ;
      private String Ddo_contagemresultado_statuscnt_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String edtContagemResultado_HoraCnt_Internalname ;
      private String edtContagemResultado_HoraCnt_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontagemresultado_datacnt_Internalname ;
      private String edtavTfcontagemresultado_datacnt_Jsonclick ;
      private String edtavTfcontagemresultado_datacnt_to_Internalname ;
      private String edtavTfcontagemresultado_datacnt_to_Jsonclick ;
      private String divDdo_contagemresultado_datacntauxdates_Internalname ;
      private String edtavDdo_contagemresultado_datacntauxdate_Internalname ;
      private String edtavDdo_contagemresultado_datacntauxdate_Jsonclick ;
      private String edtavDdo_contagemresultado_datacntauxdateto_Internalname ;
      private String edtavDdo_contagemresultado_datacntauxdateto_Jsonclick ;
      private String edtavTfcontagemresultado_contadorfmcod_Internalname ;
      private String edtavTfcontagemresultado_contadorfmcod_Jsonclick ;
      private String edtavTfcontagemresultado_contadorfmcod_to_Internalname ;
      private String edtavTfcontagemresultado_contadorfmcod_to_Jsonclick ;
      private String edtavTfcontagemresultado_pfbfs_Internalname ;
      private String edtavTfcontagemresultado_pfbfs_Jsonclick ;
      private String edtavTfcontagemresultado_pfbfs_to_Internalname ;
      private String edtavTfcontagemresultado_pfbfs_to_Jsonclick ;
      private String edtavTfcontagemresultado_pflfs_Internalname ;
      private String edtavTfcontagemresultado_pflfs_Jsonclick ;
      private String edtavTfcontagemresultado_pflfs_to_Internalname ;
      private String edtavTfcontagemresultado_pflfs_to_Jsonclick ;
      private String edtavTfcontagemresultado_pfbfm_Internalname ;
      private String edtavTfcontagemresultado_pfbfm_Jsonclick ;
      private String edtavTfcontagemresultado_pfbfm_to_Internalname ;
      private String edtavTfcontagemresultado_pfbfm_to_Jsonclick ;
      private String edtavTfcontagemresultado_pflfm_Internalname ;
      private String edtavTfcontagemresultado_pflfm_Jsonclick ;
      private String edtavTfcontagemresultado_pflfm_to_Internalname ;
      private String edtavTfcontagemresultado_pflfm_to_Jsonclick ;
      private String edtavTfcontagemresultado_divergencia_Internalname ;
      private String edtavTfcontagemresultado_divergencia_Jsonclick ;
      private String edtavTfcontagemresultado_divergencia_to_Internalname ;
      private String edtavTfcontagemresultado_divergencia_to_Jsonclick ;
      private String edtavTfcontagemresultado_parecertcn_Internalname ;
      private String edtavTfcontagemresultado_parecertcn_sel_Internalname ;
      private String edtavTfcontagemresultado_naocnfcntcod_Internalname ;
      private String edtavTfcontagemresultado_naocnfcntcod_Jsonclick ;
      private String edtavTfcontagemresultado_naocnfcntcod_to_Internalname ;
      private String edtavTfcontagemresultado_naocnfcntcod_to_Jsonclick ;
      private String edtavDdo_contagemresultado_datacnttitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_contadorfmcodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_pfbfstitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_pflfstitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_pfbfmtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_pflfmtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_divergenciatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_parecertcntitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_naocnfcntcodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_statuscnttitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_DataCnt_Internalname ;
      private String edtContagemResultado_ContadorFMCod_Internalname ;
      private String edtContagemResultado_PFBFS_Internalname ;
      private String edtContagemResultado_PFLFS_Internalname ;
      private String edtContagemResultado_PFBFM_Internalname ;
      private String edtContagemResultado_PFLFM_Internalname ;
      private String edtContagemResultado_Divergencia_Internalname ;
      private String edtContagemResultado_ParecerTcn_Internalname ;
      private String edtContagemResultado_NaoCnfCntCod_Internalname ;
      private String cmbContagemResultado_StatusCnt_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContagemresultado_datacnt1_Internalname ;
      private String edtavContagemresultado_datacnt_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavContagemresultado_datacnt2_Internalname ;
      private String edtavContagemresultado_datacnt_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavContagemresultado_datacnt3_Internalname ;
      private String edtavContagemresultado_datacnt_to3_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contagemresultado_datacnt_Internalname ;
      private String Ddo_contagemresultado_contadorfmcod_Internalname ;
      private String Ddo_contagemresultado_pfbfs_Internalname ;
      private String Ddo_contagemresultado_pflfs_Internalname ;
      private String Ddo_contagemresultado_pfbfm_Internalname ;
      private String Ddo_contagemresultado_pflfm_Internalname ;
      private String Ddo_contagemresultado_divergencia_Internalname ;
      private String Ddo_contagemresultado_parecertcn_Internalname ;
      private String Ddo_contagemresultado_naocnfcntcod_Internalname ;
      private String Ddo_contagemresultado_statuscnt_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContagemResultado_DataCnt_Title ;
      private String edtContagemResultado_ContadorFMCod_Title ;
      private String edtContagemResultado_PFBFS_Title ;
      private String edtContagemResultado_PFLFS_Title ;
      private String edtContagemResultado_PFBFM_Title ;
      private String edtContagemResultado_PFLFM_Title ;
      private String edtContagemResultado_Divergencia_Title ;
      private String edtContagemResultado_ParecerTcn_Title ;
      private String edtContagemResultado_NaoCnfCntCod_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfilterscontagemresultado_datacnt1_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_datacnt2_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_datacnt3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavContagemresultado_datacnt3_Jsonclick ;
      private String lblDynamicfilterscontagemresultado_datacnt_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontagemresultado_datacnt_rangemiddletext3_Jsonclick ;
      private String edtavContagemresultado_datacnt_to3_Jsonclick ;
      private String edtavContagemresultado_datacnt2_Jsonclick ;
      private String lblDynamicfilterscontagemresultado_datacnt_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontagemresultado_datacnt_rangemiddletext2_Jsonclick ;
      private String edtavContagemresultado_datacnt_to2_Jsonclick ;
      private String edtavContagemresultado_datacnt1_Jsonclick ;
      private String lblDynamicfilterscontagemresultado_datacnt_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontagemresultado_datacnt_rangemiddletext1_Jsonclick ;
      private String edtavContagemresultado_datacnt_to1_Jsonclick ;
      private String sGXsfl_86_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String edtContagemResultado_DataCnt_Jsonclick ;
      private String edtContagemResultado_ContadorFMCod_Jsonclick ;
      private String edtContagemResultado_PFBFS_Jsonclick ;
      private String edtContagemResultado_PFLFS_Jsonclick ;
      private String edtContagemResultado_PFBFM_Jsonclick ;
      private String edtContagemResultado_PFLFM_Jsonclick ;
      private String edtContagemResultado_Divergencia_Jsonclick ;
      private String edtContagemResultado_ParecerTcn_Jsonclick ;
      private String edtContagemResultado_NaoCnfCntCod_Jsonclick ;
      private String cmbContagemResultado_StatusCnt_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV8InOutContagemResultado_DataCnt ;
      private DateTime wcpOAV8InOutContagemResultado_DataCnt ;
      private DateTime AV32ContagemResultado_DataCnt1 ;
      private DateTime AV33ContagemResultado_DataCnt_To1 ;
      private DateTime AV34ContagemResultado_DataCnt2 ;
      private DateTime AV35ContagemResultado_DataCnt_To2 ;
      private DateTime AV36ContagemResultado_DataCnt3 ;
      private DateTime AV37ContagemResultado_DataCnt_To3 ;
      private DateTime AV40TFContagemResultado_DataCnt ;
      private DateTime AV41TFContagemResultado_DataCnt_To ;
      private DateTime AV42DDO_ContagemResultado_DataCntAuxDate ;
      private DateTime AV43DDO_ContagemResultado_DataCntAuxDateTo ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV23DynamicFiltersEnabled3 ;
      private bool AV28DynamicFiltersIgnoreFirst ;
      private bool AV27DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contagemresultado_datacnt_Includesortasc ;
      private bool Ddo_contagemresultado_datacnt_Includesortdsc ;
      private bool Ddo_contagemresultado_datacnt_Includefilter ;
      private bool Ddo_contagemresultado_datacnt_Filterisrange ;
      private bool Ddo_contagemresultado_datacnt_Includedatalist ;
      private bool Ddo_contagemresultado_contadorfmcod_Includesortasc ;
      private bool Ddo_contagemresultado_contadorfmcod_Includesortdsc ;
      private bool Ddo_contagemresultado_contadorfmcod_Includefilter ;
      private bool Ddo_contagemresultado_contadorfmcod_Filterisrange ;
      private bool Ddo_contagemresultado_contadorfmcod_Includedatalist ;
      private bool Ddo_contagemresultado_pfbfs_Includesortasc ;
      private bool Ddo_contagemresultado_pfbfs_Includesortdsc ;
      private bool Ddo_contagemresultado_pfbfs_Includefilter ;
      private bool Ddo_contagemresultado_pfbfs_Filterisrange ;
      private bool Ddo_contagemresultado_pfbfs_Includedatalist ;
      private bool Ddo_contagemresultado_pflfs_Includesortasc ;
      private bool Ddo_contagemresultado_pflfs_Includesortdsc ;
      private bool Ddo_contagemresultado_pflfs_Includefilter ;
      private bool Ddo_contagemresultado_pflfs_Filterisrange ;
      private bool Ddo_contagemresultado_pflfs_Includedatalist ;
      private bool Ddo_contagemresultado_pfbfm_Includesortasc ;
      private bool Ddo_contagemresultado_pfbfm_Includesortdsc ;
      private bool Ddo_contagemresultado_pfbfm_Includefilter ;
      private bool Ddo_contagemresultado_pfbfm_Filterisrange ;
      private bool Ddo_contagemresultado_pfbfm_Includedatalist ;
      private bool Ddo_contagemresultado_pflfm_Includesortasc ;
      private bool Ddo_contagemresultado_pflfm_Includesortdsc ;
      private bool Ddo_contagemresultado_pflfm_Includefilter ;
      private bool Ddo_contagemresultado_pflfm_Filterisrange ;
      private bool Ddo_contagemresultado_pflfm_Includedatalist ;
      private bool Ddo_contagemresultado_divergencia_Includesortasc ;
      private bool Ddo_contagemresultado_divergencia_Includesortdsc ;
      private bool Ddo_contagemresultado_divergencia_Includefilter ;
      private bool Ddo_contagemresultado_divergencia_Filterisrange ;
      private bool Ddo_contagemresultado_divergencia_Includedatalist ;
      private bool Ddo_contagemresultado_parecertcn_Includesortasc ;
      private bool Ddo_contagemresultado_parecertcn_Includesortdsc ;
      private bool Ddo_contagemresultado_parecertcn_Includefilter ;
      private bool Ddo_contagemresultado_parecertcn_Filterisrange ;
      private bool Ddo_contagemresultado_parecertcn_Includedatalist ;
      private bool Ddo_contagemresultado_naocnfcntcod_Includesortasc ;
      private bool Ddo_contagemresultado_naocnfcntcod_Includesortdsc ;
      private bool Ddo_contagemresultado_naocnfcntcod_Includefilter ;
      private bool Ddo_contagemresultado_naocnfcntcod_Filterisrange ;
      private bool Ddo_contagemresultado_naocnfcntcod_Includedatalist ;
      private bool Ddo_contagemresultado_statuscnt_Includesortasc ;
      private bool Ddo_contagemresultado_statuscnt_Includesortdsc ;
      private bool Ddo_contagemresultado_statuscnt_Includefilter ;
      private bool Ddo_contagemresultado_statuscnt_Includedatalist ;
      private bool Ddo_contagemresultado_statuscnt_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV29Select_IsBlob ;
      private String A463ContagemResultado_ParecerTcn ;
      private String AV78TFContagemResultado_StatusCnt_SelsJson ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV24DynamicFiltersSelector3 ;
      private String AV70TFContagemResultado_ParecerTcn ;
      private String AV71TFContagemResultado_ParecerTcn_Sel ;
      private String AV44ddo_ContagemResultado_DataCntTitleControlIdToReplace ;
      private String AV48ddo_ContagemResultado_ContadorFMCodTitleControlIdToReplace ;
      private String AV52ddo_ContagemResultado_PFBFSTitleControlIdToReplace ;
      private String AV56ddo_ContagemResultado_PFLFSTitleControlIdToReplace ;
      private String AV60ddo_ContagemResultado_PFBFMTitleControlIdToReplace ;
      private String AV64ddo_ContagemResultado_PFLFMTitleControlIdToReplace ;
      private String AV68ddo_ContagemResultado_DivergenciaTitleControlIdToReplace ;
      private String AV72ddo_ContagemResultado_ParecerTcnTitleControlIdToReplace ;
      private String AV76ddo_ContagemResultado_NaoCnfCntCodTitleControlIdToReplace ;
      private String AV80ddo_ContagemResultado_StatusCntTitleControlIdToReplace ;
      private String AV87Select_GXI ;
      private String lV70TFContagemResultado_ParecerTcn ;
      private String AV29Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContagemResultado_Codigo ;
      private DateTime aP1_InOutContagemResultado_DataCnt ;
      private String aP2_InOutContagemResultado_HoraCnt ;
      private decimal aP3_InOutContagemResultado_PFBFS ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbContagemResultado_StatusCnt ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00B92_A511ContagemResultado_HoraCnt ;
      private short[] H00B92_A483ContagemResultado_StatusCnt ;
      private int[] H00B92_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] H00B92_n469ContagemResultado_NaoCnfCntCod ;
      private String[] H00B92_A463ContagemResultado_ParecerTcn ;
      private bool[] H00B92_n463ContagemResultado_ParecerTcn ;
      private decimal[] H00B92_A462ContagemResultado_Divergencia ;
      private decimal[] H00B92_A461ContagemResultado_PFLFM ;
      private bool[] H00B92_n461ContagemResultado_PFLFM ;
      private decimal[] H00B92_A460ContagemResultado_PFBFM ;
      private bool[] H00B92_n460ContagemResultado_PFBFM ;
      private decimal[] H00B92_A459ContagemResultado_PFLFS ;
      private bool[] H00B92_n459ContagemResultado_PFLFS ;
      private decimal[] H00B92_A458ContagemResultado_PFBFS ;
      private bool[] H00B92_n458ContagemResultado_PFBFS ;
      private int[] H00B92_A470ContagemResultado_ContadorFMCod ;
      private DateTime[] H00B92_A473ContagemResultado_DataCnt ;
      private int[] H00B92_A456ContagemResultado_Codigo ;
      private long[] H00B93_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV79TFContagemResultado_StatusCnt_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39ContagemResultado_DataCntTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45ContagemResultado_ContadorFMCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49ContagemResultado_PFBFSTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV53ContagemResultado_PFLFSTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV57ContagemResultado_PFBFMTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV61ContagemResultado_PFLFMTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV65ContagemResultado_DivergenciaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69ContagemResultado_ParecerTcnTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73ContagemResultado_NaoCnfCntCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV77ContagemResultado_StatusCntTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV81DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontagemresultadocontagens__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00B92( IGxContext context ,
                                             short A483ContagemResultado_StatusCnt ,
                                             IGxCollection AV79TFContagemResultado_StatusCnt_Sels ,
                                             String AV16DynamicFiltersSelector1 ,
                                             DateTime AV32ContagemResultado_DataCnt1 ,
                                             DateTime AV33ContagemResultado_DataCnt_To1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             DateTime AV34ContagemResultado_DataCnt2 ,
                                             DateTime AV35ContagemResultado_DataCnt_To2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             DateTime AV36ContagemResultado_DataCnt3 ,
                                             DateTime AV37ContagemResultado_DataCnt_To3 ,
                                             DateTime AV40TFContagemResultado_DataCnt ,
                                             DateTime AV41TFContagemResultado_DataCnt_To ,
                                             int AV46TFContagemResultado_ContadorFMCod ,
                                             int AV47TFContagemResultado_ContadorFMCod_To ,
                                             decimal AV50TFContagemResultado_PFBFS ,
                                             decimal AV51TFContagemResultado_PFBFS_To ,
                                             decimal AV54TFContagemResultado_PFLFS ,
                                             decimal AV55TFContagemResultado_PFLFS_To ,
                                             decimal AV58TFContagemResultado_PFBFM ,
                                             decimal AV59TFContagemResultado_PFBFM_To ,
                                             decimal AV62TFContagemResultado_PFLFM ,
                                             decimal AV63TFContagemResultado_PFLFM_To ,
                                             decimal AV66TFContagemResultado_Divergencia ,
                                             decimal AV67TFContagemResultado_Divergencia_To ,
                                             String AV71TFContagemResultado_ParecerTcn_Sel ,
                                             String AV70TFContagemResultado_ParecerTcn ,
                                             int AV74TFContagemResultado_NaoCnfCntCod ,
                                             int AV75TFContagemResultado_NaoCnfCntCod_To ,
                                             int AV79TFContagemResultado_StatusCnt_Sels_Count ,
                                             DateTime A473ContagemResultado_DataCnt ,
                                             int A470ContagemResultado_ContadorFMCod ,
                                             decimal A458ContagemResultado_PFBFS ,
                                             decimal A459ContagemResultado_PFLFS ,
                                             decimal A460ContagemResultado_PFBFM ,
                                             decimal A461ContagemResultado_PFLFM ,
                                             decimal A462ContagemResultado_Divergencia ,
                                             String A463ContagemResultado_ParecerTcn ,
                                             int A469ContagemResultado_NaoCnfCntCod ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [29] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ContagemResultado_HoraCnt], [ContagemResultado_StatusCnt], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_ParecerTcn], [ContagemResultado_Divergencia], [ContagemResultado_PFLFM], [ContagemResultado_PFBFM], [ContagemResultado_PFLFS], [ContagemResultado_PFBFS], [ContagemResultado_ContadorFMCod], [ContagemResultado_DataCnt], [ContagemResultado_Codigo]";
         sFromString = " FROM [ContagemResultadoContagens] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV32ContagemResultado_DataCnt1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] >= @AV32ContagemResultado_DataCnt1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] >= @AV32ContagemResultado_DataCnt1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV33ContagemResultado_DataCnt_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] <= @AV33ContagemResultado_DataCnt_To1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] <= @AV33ContagemResultado_DataCnt_To1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV34ContagemResultado_DataCnt2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] >= @AV34ContagemResultado_DataCnt2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] >= @AV34ContagemResultado_DataCnt2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV35ContagemResultado_DataCnt_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] <= @AV35ContagemResultado_DataCnt_To2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] <= @AV35ContagemResultado_DataCnt_To2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV36ContagemResultado_DataCnt3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] >= @AV36ContagemResultado_DataCnt3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] >= @AV36ContagemResultado_DataCnt3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV37ContagemResultado_DataCnt_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] <= @AV37ContagemResultado_DataCnt_To3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] <= @AV37ContagemResultado_DataCnt_To3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV40TFContagemResultado_DataCnt) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] >= @AV40TFContagemResultado_DataCnt)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] >= @AV40TFContagemResultado_DataCnt)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (DateTime.MinValue==AV41TFContagemResultado_DataCnt_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] <= @AV41TFContagemResultado_DataCnt_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] <= @AV41TFContagemResultado_DataCnt_To)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV46TFContagemResultado_ContadorFMCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_ContadorFMCod] >= @AV46TFContagemResultado_ContadorFMCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_ContadorFMCod] >= @AV46TFContagemResultado_ContadorFMCod)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV47TFContagemResultado_ContadorFMCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_ContadorFMCod] <= @AV47TFContagemResultado_ContadorFMCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_ContadorFMCod] <= @AV47TFContagemResultado_ContadorFMCod_To)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV50TFContagemResultado_PFBFS) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFBFS] >= @AV50TFContagemResultado_PFBFS)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFBFS] >= @AV50TFContagemResultado_PFBFS)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV51TFContagemResultado_PFBFS_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFBFS] <= @AV51TFContagemResultado_PFBFS_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFBFS] <= @AV51TFContagemResultado_PFBFS_To)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV54TFContagemResultado_PFLFS) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFLFS] >= @AV54TFContagemResultado_PFLFS)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFLFS] >= @AV54TFContagemResultado_PFLFS)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV55TFContagemResultado_PFLFS_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFLFS] <= @AV55TFContagemResultado_PFLFS_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFLFS] <= @AV55TFContagemResultado_PFLFS_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV58TFContagemResultado_PFBFM) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFBFM] >= @AV58TFContagemResultado_PFBFM)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFBFM] >= @AV58TFContagemResultado_PFBFM)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV59TFContagemResultado_PFBFM_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFBFM] <= @AV59TFContagemResultado_PFBFM_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFBFM] <= @AV59TFContagemResultado_PFBFM_To)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV62TFContagemResultado_PFLFM) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFLFM] >= @AV62TFContagemResultado_PFLFM)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFLFM] >= @AV62TFContagemResultado_PFLFM)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV63TFContagemResultado_PFLFM_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFLFM] <= @AV63TFContagemResultado_PFLFM_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFLFM] <= @AV63TFContagemResultado_PFLFM_To)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV66TFContagemResultado_Divergencia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Divergencia] >= @AV66TFContagemResultado_Divergencia)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Divergencia] >= @AV66TFContagemResultado_Divergencia)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV67TFContagemResultado_Divergencia_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Divergencia] <= @AV67TFContagemResultado_Divergencia_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Divergencia] <= @AV67TFContagemResultado_Divergencia_To)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71TFContagemResultado_ParecerTcn_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFContagemResultado_ParecerTcn)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_ParecerTcn] like @lV70TFContagemResultado_ParecerTcn)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_ParecerTcn] like @lV70TFContagemResultado_ParecerTcn)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFContagemResultado_ParecerTcn_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_ParecerTcn] = @AV71TFContagemResultado_ParecerTcn_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_ParecerTcn] = @AV71TFContagemResultado_ParecerTcn_Sel)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! (0==AV74TFContagemResultado_NaoCnfCntCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_NaoCnfCntCod] >= @AV74TFContagemResultado_NaoCnfCntCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_NaoCnfCntCod] >= @AV74TFContagemResultado_NaoCnfCntCod)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! (0==AV75TFContagemResultado_NaoCnfCntCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_NaoCnfCntCod] <= @AV75TFContagemResultado_NaoCnfCntCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_NaoCnfCntCod] <= @AV75TFContagemResultado_NaoCnfCntCod_To)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( AV79TFContagemResultado_StatusCnt_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV79TFContagemResultado_StatusCnt_Sels, "[ContagemResultado_StatusCnt] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV79TFContagemResultado_StatusCnt_Sels, "[ContagemResultado_StatusCnt] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_DataCnt]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_DataCnt] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_ContadorFMCod]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_ContadorFMCod] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_PFBFS]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_PFBFS] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_PFLFS]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_PFLFS] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_PFBFM]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_PFBFM] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_PFLFM]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_PFLFM] DESC";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_Divergencia]";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_Divergencia] DESC";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_ParecerTcn]";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_ParecerTcn] DESC";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_NaoCnfCntCod]";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_NaoCnfCntCod] DESC";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_StatusCnt]";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_StatusCnt] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00B93( IGxContext context ,
                                             short A483ContagemResultado_StatusCnt ,
                                             IGxCollection AV79TFContagemResultado_StatusCnt_Sels ,
                                             String AV16DynamicFiltersSelector1 ,
                                             DateTime AV32ContagemResultado_DataCnt1 ,
                                             DateTime AV33ContagemResultado_DataCnt_To1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             DateTime AV34ContagemResultado_DataCnt2 ,
                                             DateTime AV35ContagemResultado_DataCnt_To2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             DateTime AV36ContagemResultado_DataCnt3 ,
                                             DateTime AV37ContagemResultado_DataCnt_To3 ,
                                             DateTime AV40TFContagemResultado_DataCnt ,
                                             DateTime AV41TFContagemResultado_DataCnt_To ,
                                             int AV46TFContagemResultado_ContadorFMCod ,
                                             int AV47TFContagemResultado_ContadorFMCod_To ,
                                             decimal AV50TFContagemResultado_PFBFS ,
                                             decimal AV51TFContagemResultado_PFBFS_To ,
                                             decimal AV54TFContagemResultado_PFLFS ,
                                             decimal AV55TFContagemResultado_PFLFS_To ,
                                             decimal AV58TFContagemResultado_PFBFM ,
                                             decimal AV59TFContagemResultado_PFBFM_To ,
                                             decimal AV62TFContagemResultado_PFLFM ,
                                             decimal AV63TFContagemResultado_PFLFM_To ,
                                             decimal AV66TFContagemResultado_Divergencia ,
                                             decimal AV67TFContagemResultado_Divergencia_To ,
                                             String AV71TFContagemResultado_ParecerTcn_Sel ,
                                             String AV70TFContagemResultado_ParecerTcn ,
                                             int AV74TFContagemResultado_NaoCnfCntCod ,
                                             int AV75TFContagemResultado_NaoCnfCntCod_To ,
                                             int AV79TFContagemResultado_StatusCnt_Sels_Count ,
                                             DateTime A473ContagemResultado_DataCnt ,
                                             int A470ContagemResultado_ContadorFMCod ,
                                             decimal A458ContagemResultado_PFBFS ,
                                             decimal A459ContagemResultado_PFLFS ,
                                             decimal A460ContagemResultado_PFBFM ,
                                             decimal A461ContagemResultado_PFLFM ,
                                             decimal A462ContagemResultado_Divergencia ,
                                             String A463ContagemResultado_ParecerTcn ,
                                             int A469ContagemResultado_NaoCnfCntCod ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [24] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ContagemResultadoContagens] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV32ContagemResultado_DataCnt1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] >= @AV32ContagemResultado_DataCnt1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] >= @AV32ContagemResultado_DataCnt1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV33ContagemResultado_DataCnt_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] <= @AV33ContagemResultado_DataCnt_To1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] <= @AV33ContagemResultado_DataCnt_To1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV34ContagemResultado_DataCnt2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] >= @AV34ContagemResultado_DataCnt2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] >= @AV34ContagemResultado_DataCnt2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV35ContagemResultado_DataCnt_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] <= @AV35ContagemResultado_DataCnt_To2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] <= @AV35ContagemResultado_DataCnt_To2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV36ContagemResultado_DataCnt3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] >= @AV36ContagemResultado_DataCnt3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] >= @AV36ContagemResultado_DataCnt3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV37ContagemResultado_DataCnt_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] <= @AV37ContagemResultado_DataCnt_To3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] <= @AV37ContagemResultado_DataCnt_To3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV40TFContagemResultado_DataCnt) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] >= @AV40TFContagemResultado_DataCnt)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] >= @AV40TFContagemResultado_DataCnt)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (DateTime.MinValue==AV41TFContagemResultado_DataCnt_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_DataCnt] <= @AV41TFContagemResultado_DataCnt_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_DataCnt] <= @AV41TFContagemResultado_DataCnt_To)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV46TFContagemResultado_ContadorFMCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_ContadorFMCod] >= @AV46TFContagemResultado_ContadorFMCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_ContadorFMCod] >= @AV46TFContagemResultado_ContadorFMCod)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV47TFContagemResultado_ContadorFMCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_ContadorFMCod] <= @AV47TFContagemResultado_ContadorFMCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_ContadorFMCod] <= @AV47TFContagemResultado_ContadorFMCod_To)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV50TFContagemResultado_PFBFS) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFBFS] >= @AV50TFContagemResultado_PFBFS)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFBFS] >= @AV50TFContagemResultado_PFBFS)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV51TFContagemResultado_PFBFS_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFBFS] <= @AV51TFContagemResultado_PFBFS_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFBFS] <= @AV51TFContagemResultado_PFBFS_To)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV54TFContagemResultado_PFLFS) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFLFS] >= @AV54TFContagemResultado_PFLFS)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFLFS] >= @AV54TFContagemResultado_PFLFS)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV55TFContagemResultado_PFLFS_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFLFS] <= @AV55TFContagemResultado_PFLFS_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFLFS] <= @AV55TFContagemResultado_PFLFS_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV58TFContagemResultado_PFBFM) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFBFM] >= @AV58TFContagemResultado_PFBFM)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFBFM] >= @AV58TFContagemResultado_PFBFM)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV59TFContagemResultado_PFBFM_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFBFM] <= @AV59TFContagemResultado_PFBFM_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFBFM] <= @AV59TFContagemResultado_PFBFM_To)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV62TFContagemResultado_PFLFM) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFLFM] >= @AV62TFContagemResultado_PFLFM)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFLFM] >= @AV62TFContagemResultado_PFLFM)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV63TFContagemResultado_PFLFM_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_PFLFM] <= @AV63TFContagemResultado_PFLFM_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_PFLFM] <= @AV63TFContagemResultado_PFLFM_To)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV66TFContagemResultado_Divergencia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Divergencia] >= @AV66TFContagemResultado_Divergencia)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Divergencia] >= @AV66TFContagemResultado_Divergencia)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV67TFContagemResultado_Divergencia_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Divergencia] <= @AV67TFContagemResultado_Divergencia_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Divergencia] <= @AV67TFContagemResultado_Divergencia_To)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71TFContagemResultado_ParecerTcn_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFContagemResultado_ParecerTcn)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_ParecerTcn] like @lV70TFContagemResultado_ParecerTcn)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_ParecerTcn] like @lV70TFContagemResultado_ParecerTcn)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFContagemResultado_ParecerTcn_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_ParecerTcn] = @AV71TFContagemResultado_ParecerTcn_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_ParecerTcn] = @AV71TFContagemResultado_ParecerTcn_Sel)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! (0==AV74TFContagemResultado_NaoCnfCntCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_NaoCnfCntCod] >= @AV74TFContagemResultado_NaoCnfCntCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_NaoCnfCntCod] >= @AV74TFContagemResultado_NaoCnfCntCod)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! (0==AV75TFContagemResultado_NaoCnfCntCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_NaoCnfCntCod] <= @AV75TFContagemResultado_NaoCnfCntCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_NaoCnfCntCod] <= @AV75TFContagemResultado_NaoCnfCntCod_To)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( AV79TFContagemResultado_StatusCnt_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV79TFContagemResultado_StatusCnt_Sels, "[ContagemResultado_StatusCnt] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV79TFContagemResultado_StatusCnt_Sels, "[ContagemResultado_StatusCnt] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00B92(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (decimal)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (DateTime)dynConstraints[32] , (int)dynConstraints[33] , (decimal)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (String)dynConstraints[39] , (int)dynConstraints[40] , (short)dynConstraints[41] , (bool)dynConstraints[42] );
               case 1 :
                     return conditional_H00B93(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (decimal)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (DateTime)dynConstraints[32] , (int)dynConstraints[33] , (decimal)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (String)dynConstraints[39] , (int)dynConstraints[40] , (short)dynConstraints[41] , (bool)dynConstraints[42] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00B92 ;
          prmH00B92 = new Object[] {
          new Object[] {"@AV32ContagemResultado_DataCnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33ContagemResultado_DataCnt_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34ContagemResultado_DataCnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV35ContagemResultado_DataCnt_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV36ContagemResultado_DataCnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV37ContagemResultado_DataCnt_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV40TFContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV41TFContagemResultado_DataCnt_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV46TFContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47TFContagemResultado_ContadorFMCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50TFContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV51TFContagemResultado_PFBFS_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV54TFContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV55TFContagemResultado_PFLFS_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV58TFContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV59TFContagemResultado_PFBFM_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV62TFContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV63TFContagemResultado_PFLFM_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV66TFContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV67TFContagemResultado_Divergencia_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@lV70TFContagemResultado_ParecerTcn",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV71TFContagemResultado_ParecerTcn_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV74TFContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV75TFContagemResultado_NaoCnfCntCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00B93 ;
          prmH00B93 = new Object[] {
          new Object[] {"@AV32ContagemResultado_DataCnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33ContagemResultado_DataCnt_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34ContagemResultado_DataCnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV35ContagemResultado_DataCnt_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV36ContagemResultado_DataCnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV37ContagemResultado_DataCnt_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV40TFContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV41TFContagemResultado_DataCnt_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV46TFContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47TFContagemResultado_ContadorFMCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50TFContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV51TFContagemResultado_PFBFS_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV54TFContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV55TFContagemResultado_PFLFS_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV58TFContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV59TFContagemResultado_PFBFM_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV62TFContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV63TFContagemResultado_PFLFM_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV66TFContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV67TFContagemResultado_Divergencia_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@lV70TFContagemResultado_ParecerTcn",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV71TFContagemResultado_ParecerTcn_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV74TFContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV75TFContagemResultado_NaoCnfCntCod_To",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00B92", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B92,11,0,true,false )
             ,new CursorDef("H00B93", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B93,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(11) ;
                ((int[]) buf[17])[0] = rslt.getInt(12) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[44]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[45]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[48]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                return;
       }
    }

 }

}
