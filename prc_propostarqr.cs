/*
               File: PRC_PropostaRqr
        Description: Proposta Requer
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:28:29.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_propostarqr : GXProcedure
   {
      public prc_propostarqr( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_propostarqr( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AreaTrabalho_Codigo ,
                           out bool aP1_PropostaRqrSrv ,
                           out bool aP2_PropostaRqrPrz ,
                           out bool aP3_PropostaRqrEsf ,
                           out bool aP4_PropostaRqrCnt ,
                           out short aP5_PropostaNvlCnt )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV12PropostaRqrSrv = false ;
         this.AV11PropostaRqrPrz = false ;
         this.AV10PropostaRqrEsf = false ;
         this.AV9PropostaRqrCnt = false ;
         this.AV8PropostaNvlCnt = 0 ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_PropostaRqrSrv=this.AV12PropostaRqrSrv;
         aP2_PropostaRqrPrz=this.AV11PropostaRqrPrz;
         aP3_PropostaRqrEsf=this.AV10PropostaRqrEsf;
         aP4_PropostaRqrCnt=this.AV9PropostaRqrCnt;
         aP5_PropostaNvlCnt=this.AV8PropostaNvlCnt;
      }

      public short executeUdp( ref int aP0_AreaTrabalho_Codigo ,
                               out bool aP1_PropostaRqrSrv ,
                               out bool aP2_PropostaRqrPrz ,
                               out bool aP3_PropostaRqrEsf ,
                               out bool aP4_PropostaRqrCnt )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV12PropostaRqrSrv = false ;
         this.AV11PropostaRqrPrz = false ;
         this.AV10PropostaRqrEsf = false ;
         this.AV9PropostaRqrCnt = false ;
         this.AV8PropostaNvlCnt = 0 ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_PropostaRqrSrv=this.AV12PropostaRqrSrv;
         aP2_PropostaRqrPrz=this.AV11PropostaRqrPrz;
         aP3_PropostaRqrEsf=this.AV10PropostaRqrEsf;
         aP4_PropostaRqrCnt=this.AV9PropostaRqrCnt;
         aP5_PropostaNvlCnt=this.AV8PropostaNvlCnt;
         return AV8PropostaNvlCnt ;
      }

      public void executeSubmit( ref int aP0_AreaTrabalho_Codigo ,
                                 out bool aP1_PropostaRqrSrv ,
                                 out bool aP2_PropostaRqrPrz ,
                                 out bool aP3_PropostaRqrEsf ,
                                 out bool aP4_PropostaRqrCnt ,
                                 out short aP5_PropostaNvlCnt )
      {
         prc_propostarqr objprc_propostarqr;
         objprc_propostarqr = new prc_propostarqr();
         objprc_propostarqr.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_propostarqr.AV12PropostaRqrSrv = false ;
         objprc_propostarqr.AV11PropostaRqrPrz = false ;
         objprc_propostarqr.AV10PropostaRqrEsf = false ;
         objprc_propostarqr.AV9PropostaRqrCnt = false ;
         objprc_propostarqr.AV8PropostaNvlCnt = 0 ;
         objprc_propostarqr.context.SetSubmitInitialConfig(context);
         objprc_propostarqr.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_propostarqr);
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_PropostaRqrSrv=this.AV12PropostaRqrSrv;
         aP2_PropostaRqrPrz=this.AV11PropostaRqrPrz;
         aP3_PropostaRqrEsf=this.AV10PropostaRqrEsf;
         aP4_PropostaRqrCnt=this.AV9PropostaRqrCnt;
         aP5_PropostaNvlCnt=this.AV8PropostaNvlCnt;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_propostarqr)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CN2 */
         pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A29Contratante_Codigo = P00CN2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00CN2_n29Contratante_Codigo[0];
            A1738Contratante_PropostaRqrSrv = P00CN2_A1738Contratante_PropostaRqrSrv[0];
            n1738Contratante_PropostaRqrSrv = P00CN2_n1738Contratante_PropostaRqrSrv[0];
            A1739Contratante_PropostaRqrPrz = P00CN2_A1739Contratante_PropostaRqrPrz[0];
            n1739Contratante_PropostaRqrPrz = P00CN2_n1739Contratante_PropostaRqrPrz[0];
            A1740Contratante_PropostaRqrEsf = P00CN2_A1740Contratante_PropostaRqrEsf[0];
            n1740Contratante_PropostaRqrEsf = P00CN2_n1740Contratante_PropostaRqrEsf[0];
            A1741Contratante_PropostaRqrCnt = P00CN2_A1741Contratante_PropostaRqrCnt[0];
            n1741Contratante_PropostaRqrCnt = P00CN2_n1741Contratante_PropostaRqrCnt[0];
            A1742Contratante_PropostaNvlCnt = P00CN2_A1742Contratante_PropostaNvlCnt[0];
            n1742Contratante_PropostaNvlCnt = P00CN2_n1742Contratante_PropostaNvlCnt[0];
            A1738Contratante_PropostaRqrSrv = P00CN2_A1738Contratante_PropostaRqrSrv[0];
            n1738Contratante_PropostaRqrSrv = P00CN2_n1738Contratante_PropostaRqrSrv[0];
            A1739Contratante_PropostaRqrPrz = P00CN2_A1739Contratante_PropostaRqrPrz[0];
            n1739Contratante_PropostaRqrPrz = P00CN2_n1739Contratante_PropostaRqrPrz[0];
            A1740Contratante_PropostaRqrEsf = P00CN2_A1740Contratante_PropostaRqrEsf[0];
            n1740Contratante_PropostaRqrEsf = P00CN2_n1740Contratante_PropostaRqrEsf[0];
            A1741Contratante_PropostaRqrCnt = P00CN2_A1741Contratante_PropostaRqrCnt[0];
            n1741Contratante_PropostaRqrCnt = P00CN2_n1741Contratante_PropostaRqrCnt[0];
            A1742Contratante_PropostaNvlCnt = P00CN2_A1742Contratante_PropostaNvlCnt[0];
            n1742Contratante_PropostaNvlCnt = P00CN2_n1742Contratante_PropostaNvlCnt[0];
            OV8PropostaNvlCnt = AV8PropostaNvlCnt;
            OV12PropostaRqrSrv = AV12PropostaRqrSrv;
            AV12PropostaRqrSrv = A1738Contratante_PropostaRqrSrv;
            AV11PropostaRqrPrz = A1739Contratante_PropostaRqrPrz;
            AV10PropostaRqrEsf = A1740Contratante_PropostaRqrEsf;
            AV9PropostaRqrCnt = A1741Contratante_PropostaRqrCnt;
            AV8PropostaNvlCnt = A1742Contratante_PropostaNvlCnt;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00CN2_A29Contratante_Codigo = new int[1] ;
         P00CN2_n29Contratante_Codigo = new bool[] {false} ;
         P00CN2_A5AreaTrabalho_Codigo = new int[1] ;
         P00CN2_A1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         P00CN2_n1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         P00CN2_A1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         P00CN2_n1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         P00CN2_A1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         P00CN2_n1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         P00CN2_A1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         P00CN2_n1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         P00CN2_A1742Contratante_PropostaNvlCnt = new short[1] ;
         P00CN2_n1742Contratante_PropostaNvlCnt = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_propostarqr__default(),
            new Object[][] {
                new Object[] {
               P00CN2_A29Contratante_Codigo, P00CN2_n29Contratante_Codigo, P00CN2_A5AreaTrabalho_Codigo, P00CN2_A1738Contratante_PropostaRqrSrv, P00CN2_n1738Contratante_PropostaRqrSrv, P00CN2_A1739Contratante_PropostaRqrPrz, P00CN2_n1739Contratante_PropostaRqrPrz, P00CN2_A1740Contratante_PropostaRqrEsf, P00CN2_n1740Contratante_PropostaRqrEsf, P00CN2_A1741Contratante_PropostaRqrCnt,
               P00CN2_n1741Contratante_PropostaRqrCnt, P00CN2_A1742Contratante_PropostaNvlCnt, P00CN2_n1742Contratante_PropostaNvlCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8PropostaNvlCnt ;
      private short A1742Contratante_PropostaNvlCnt ;
      private short OV8PropostaNvlCnt ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private String scmdbuf ;
      private bool n29Contratante_Codigo ;
      private bool A1738Contratante_PropostaRqrSrv ;
      private bool n1738Contratante_PropostaRqrSrv ;
      private bool A1739Contratante_PropostaRqrPrz ;
      private bool n1739Contratante_PropostaRqrPrz ;
      private bool A1740Contratante_PropostaRqrEsf ;
      private bool n1740Contratante_PropostaRqrEsf ;
      private bool A1741Contratante_PropostaRqrCnt ;
      private bool n1741Contratante_PropostaRqrCnt ;
      private bool n1742Contratante_PropostaNvlCnt ;
      private bool OV12PropostaRqrSrv ;
      private bool AV12PropostaRqrSrv ;
      private bool AV11PropostaRqrPrz ;
      private bool AV10PropostaRqrEsf ;
      private bool AV9PropostaRqrCnt ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AreaTrabalho_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00CN2_A29Contratante_Codigo ;
      private bool[] P00CN2_n29Contratante_Codigo ;
      private int[] P00CN2_A5AreaTrabalho_Codigo ;
      private bool[] P00CN2_A1738Contratante_PropostaRqrSrv ;
      private bool[] P00CN2_n1738Contratante_PropostaRqrSrv ;
      private bool[] P00CN2_A1739Contratante_PropostaRqrPrz ;
      private bool[] P00CN2_n1739Contratante_PropostaRqrPrz ;
      private bool[] P00CN2_A1740Contratante_PropostaRqrEsf ;
      private bool[] P00CN2_n1740Contratante_PropostaRqrEsf ;
      private bool[] P00CN2_A1741Contratante_PropostaRqrCnt ;
      private bool[] P00CN2_n1741Contratante_PropostaRqrCnt ;
      private short[] P00CN2_A1742Contratante_PropostaNvlCnt ;
      private bool[] P00CN2_n1742Contratante_PropostaNvlCnt ;
      private bool aP1_PropostaRqrSrv ;
      private bool aP2_PropostaRqrPrz ;
      private bool aP3_PropostaRqrEsf ;
      private bool aP4_PropostaRqrCnt ;
      private short aP5_PropostaNvlCnt ;
   }

   public class prc_propostarqr__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CN2 ;
          prmP00CN2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CN2", "SELECT TOP 1 T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_PropostaRqrSrv], T2.[Contratante_PropostaRqrPrz], T2.[Contratante_PropostaRqrEsf], T2.[Contratante_PropostaRqrCnt], T2.[Contratante_PropostaNvlCnt] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CN2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
