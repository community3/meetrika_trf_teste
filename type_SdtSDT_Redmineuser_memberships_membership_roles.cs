/*
               File: type_SdtSDT_Redmineuser_memberships_membership_roles
        Description: SDT_Redmineuser
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:6.95
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Redmineuser.memberships.membership.roles" )]
   [XmlType(TypeName =  "SDT_Redmineuser.memberships.membership.roles" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_Redmineuser_memberships_membership_roles_role ))]
   [Serializable]
   public class SdtSDT_Redmineuser_memberships_membership_roles : GxUserType
   {
      public SdtSDT_Redmineuser_memberships_membership_roles( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Type = "";
      }

      public SdtSDT_Redmineuser_memberships_membership_roles( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Redmineuser_memberships_membership_roles deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Redmineuser_memberships_membership_roles)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Redmineuser_memberships_membership_roles obj ;
         obj = this;
         obj.gxTpr_Type = deserialized.gxTpr_Type;
         obj.gxTpr_Roles = deserialized.gxTpr_Roles;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("type") == 1 )
         {
            gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Type = oReader.GetAttributeByName("type");
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            if ( gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles != null )
            {
               gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles.ClearCollection();
            }
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp(oReader.LocalName, "role") == 0 )
               {
                  if ( gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles == null )
                  {
                     gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles = new GxObjectCollection( context, "SDT_Redmineuser.memberships.membership.roles.role", "", "SdtSDT_Redmineuser_memberships_membership_roles_role", "GeneXus.Programs");
                  }
                  GXSoapError = gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles.readxmlcollection(oReader, "", "role");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Redmineuser.memberships.membership.roles";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("type", StringUtil.RTrim( gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Type));
         if ( gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles.writexmlcollection(oWriter, "", sNameSpace1, "role", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("type", gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Type, false);
         if ( gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles != null )
         {
            AddObjectProperty("roles", gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles, false);
         }
         return  ;
      }

      [SoapAttribute( AttributeName = "type" )]
      [XmlAttribute( AttributeName = "type" )]
      public String gxTpr_Type
      {
         get {
            return gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Type ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Type = (String)(value);
         }

      }

      public class gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles_SdtSDT_Redmineuser_memberships_membership_roles_role_80compatibility:SdtSDT_Redmineuser_memberships_membership_roles_role {}
      [  SoapElement( ElementName = "role" )]
      [  XmlElement( ElementName = "role" , Namespace = "" , Type= typeof( SdtSDT_Redmineuser_memberships_membership_roles_role ))]
      public GxObjectCollection gxTpr_Roles_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles == null )
            {
               gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles = new GxObjectCollection( context, "SDT_Redmineuser.memberships.membership.roles.role", "", "SdtSDT_Redmineuser_memberships_membership_roles_role", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles ;
         }

         set {
            if ( gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles == null )
            {
               gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles = new GxObjectCollection( context, "SDT_Redmineuser.memberships.membership.roles.role", "", "SdtSDT_Redmineuser_memberships_membership_roles_role", "GeneXus.Programs");
            }
            gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Roles
      {
         get {
            if ( gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles == null )
            {
               gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles = new GxObjectCollection( context, "SDT_Redmineuser.memberships.membership.roles.role", "", "SdtSDT_Redmineuser_memberships_membership_roles_role", "GeneXus.Programs");
            }
            return gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles = value;
         }

      }

      public void gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles_SetNull( )
      {
         gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles_IsNull( )
      {
         if ( gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Type = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Type ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Redmineuser_memberships_membership_roles_role ))]
      protected IGxCollection gxTv_SdtSDT_Redmineuser_memberships_membership_roles_Roles=null ;
   }

   [DataContract(Name = @"SDT_Redmineuser.memberships.membership.roles", Namespace = "")]
   public class SdtSDT_Redmineuser_memberships_membership_roles_RESTInterface : GxGenericCollectionItem<SdtSDT_Redmineuser_memberships_membership_roles>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Redmineuser_memberships_membership_roles_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Redmineuser_memberships_membership_roles_RESTInterface( SdtSDT_Redmineuser_memberships_membership_roles psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "type" , Order = 0 )]
      public String gxTpr_Type
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Type) ;
         }

         set {
            sdt.gxTpr_Type = (String)(value);
         }

      }

      [DataMember( Name = "roles" , Order = 1 )]
      public GxGenericCollection<SdtSDT_Redmineuser_memberships_membership_roles_role_RESTInterface> gxTpr_Roles
      {
         get {
            return new GxGenericCollection<SdtSDT_Redmineuser_memberships_membership_roles_role_RESTInterface>(sdt.gxTpr_Roles) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Roles);
         }

      }

      public SdtSDT_Redmineuser_memberships_membership_roles sdt
      {
         get {
            return (SdtSDT_Redmineuser_memberships_membership_roles)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Redmineuser_memberships_membership_roles() ;
         }
      }

   }

}
