/*
               File: PRC_NaoTemPreposto
        Description: Nao Tem Preposto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:16.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_naotempreposto : GXProcedure
   {
      public prc_naotempreposto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_naotempreposto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo ,
                           int aP1_Contrato_Codigo ,
                           out bool aP2_NaoTemPreposto )
      {
         this.AV9ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV12Contrato_Codigo = aP1_Contrato_Codigo;
         this.AV8NaoTemPreposto = false ;
         initialize();
         executePrivate();
         aP2_NaoTemPreposto=this.AV8NaoTemPreposto;
      }

      public bool executeUdp( int aP0_ContratoServicos_Codigo ,
                              int aP1_Contrato_Codigo )
      {
         this.AV9ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV12Contrato_Codigo = aP1_Contrato_Codigo;
         this.AV8NaoTemPreposto = false ;
         initialize();
         executePrivate();
         aP2_NaoTemPreposto=this.AV8NaoTemPreposto;
         return AV8NaoTemPreposto ;
      }

      public void executeSubmit( int aP0_ContratoServicos_Codigo ,
                                 int aP1_Contrato_Codigo ,
                                 out bool aP2_NaoTemPreposto )
      {
         prc_naotempreposto objprc_naotempreposto;
         objprc_naotempreposto = new prc_naotempreposto();
         objprc_naotempreposto.AV9ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_naotempreposto.AV12Contrato_Codigo = aP1_Contrato_Codigo;
         objprc_naotempreposto.AV8NaoTemPreposto = false ;
         objprc_naotempreposto.context.SetSubmitInitialConfig(context);
         objprc_naotempreposto.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_naotempreposto);
         aP2_NaoTemPreposto=this.AV8NaoTemPreposto;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_naotempreposto)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV9ContratoServicos_Codigo > 0 )
         {
            AV15GXLvl3 = 0;
            /* Using cursor P00XF2 */
            pr_default.execute(0, new Object[] {AV9ContratoServicos_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A74Contrato_Codigo = P00XF2_A74Contrato_Codigo[0];
               A1013Contrato_PrepostoCod = P00XF2_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = P00XF2_n1013Contrato_PrepostoCod[0];
               A160ContratoServicos_Codigo = P00XF2_A160ContratoServicos_Codigo[0];
               A1013Contrato_PrepostoCod = P00XF2_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = P00XF2_n1013Contrato_PrepostoCod[0];
               AV15GXLvl3 = 1;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( AV15GXLvl3 == 0 )
            {
               AV8NaoTemPreposto = true;
            }
         }
         else
         {
            AV16GXLvl12 = 0;
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV12Contrato_Codigo ,
                                                 A74Contrato_Codigo ,
                                                 A1013Contrato_PrepostoCod },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor P00XF3 */
            pr_default.execute(1, new Object[] {AV12Contrato_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1013Contrato_PrepostoCod = P00XF3_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = P00XF3_n1013Contrato_PrepostoCod[0];
               A74Contrato_Codigo = P00XF3_A74Contrato_Codigo[0];
               A39Contratada_Codigo = P00XF3_A39Contratada_Codigo[0];
               AV16GXLvl12 = 1;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            if ( AV16GXLvl12 == 0 )
            {
               AV8NaoTemPreposto = true;
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00XF2_A74Contrato_Codigo = new int[1] ;
         P00XF2_A1013Contrato_PrepostoCod = new int[1] ;
         P00XF2_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P00XF2_A160ContratoServicos_Codigo = new int[1] ;
         P00XF3_A1013Contrato_PrepostoCod = new int[1] ;
         P00XF3_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P00XF3_A74Contrato_Codigo = new int[1] ;
         P00XF3_A39Contratada_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_naotempreposto__default(),
            new Object[][] {
                new Object[] {
               P00XF2_A74Contrato_Codigo, P00XF2_A1013Contrato_PrepostoCod, P00XF2_n1013Contrato_PrepostoCod, P00XF2_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P00XF3_A1013Contrato_PrepostoCod, P00XF3_n1013Contrato_PrepostoCod, P00XF3_A74Contrato_Codigo, P00XF3_A39Contratada_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV15GXLvl3 ;
      private short AV16GXLvl12 ;
      private int AV9ContratoServicos_Codigo ;
      private int AV12Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int A160ContratoServicos_Codigo ;
      private int A39Contratada_Codigo ;
      private String scmdbuf ;
      private bool AV8NaoTemPreposto ;
      private bool n1013Contrato_PrepostoCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00XF2_A74Contrato_Codigo ;
      private int[] P00XF2_A1013Contrato_PrepostoCod ;
      private bool[] P00XF2_n1013Contrato_PrepostoCod ;
      private int[] P00XF2_A160ContratoServicos_Codigo ;
      private int[] P00XF3_A1013Contrato_PrepostoCod ;
      private bool[] P00XF3_n1013Contrato_PrepostoCod ;
      private int[] P00XF3_A74Contrato_Codigo ;
      private int[] P00XF3_A39Contratada_Codigo ;
      private bool aP2_NaoTemPreposto ;
   }

   public class prc_naotempreposto__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00XF3( IGxContext context ,
                                             int AV12Contrato_Codigo ,
                                             int A74Contrato_Codigo ,
                                             int A1013Contrato_PrepostoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [1] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 [Contrato_PrepostoCod], [Contrato_Codigo], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contrato_PrepostoCod] > 0)";
         if ( AV12Contrato_Codigo > 0 )
         {
            sWhereString = sWhereString + " and ([Contrato_Codigo] = @AV12Contrato_Codigo)";
         }
         else
         {
            GXv_int1[0] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV12Contrato_Codigo > 0 )
         {
            scmdbuf = scmdbuf + " ORDER BY [Contrato_Codigo]";
         }
         else if ( (0==AV12Contrato_Codigo) )
         {
            scmdbuf = scmdbuf + " ORDER BY [Contratada_Codigo]";
         }
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P00XF3(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XF2 ;
          prmP00XF2 = new Object[] {
          new Object[] {"@AV9ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XF3 ;
          prmP00XF3 = new Object[] {
          new Object[] {"@AV12Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XF2", "SELECT TOP 1 T1.[Contrato_Codigo], T2.[Contrato_PrepostoCod], T1.[ContratoServicos_Codigo] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[ContratoServicos_Codigo] = @AV9ContratoServicos_Codigo) AND (T2.[Contrato_PrepostoCod] > 0) ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XF2,1,0,false,true )
             ,new CursorDef("P00XF3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XF3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
       }
    }

 }

}
