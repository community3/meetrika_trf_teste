/*
               File: WWContagemResultadoIncidentes
        Description:  Contagem Resultado Incidentes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:23:43.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontagemresultadoincidentes : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontagemresultadoincidentes( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontagemresultadoincidentes( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_97 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_97_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_97_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16Incidentes_Data1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Incidentes_Data1", context.localUtil.TToC( AV16Incidentes_Data1, 8, 5, 0, 3, "/", ":", " "));
               AV17Incidentes_Data_To1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Incidentes_Data_To1", context.localUtil.TToC( AV17Incidentes_Data_To1, 8, 5, 0, 3, "/", ":", " "));
               AV18Incidentes_DemandaNum1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Incidentes_DemandaNum1", AV18Incidentes_DemandaNum1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21Incidentes_Data2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Incidentes_Data2", context.localUtil.TToC( AV21Incidentes_Data2, 8, 5, 0, 3, "/", ":", " "));
               AV22Incidentes_Data_To2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Incidentes_Data_To2", context.localUtil.TToC( AV22Incidentes_Data_To2, 8, 5, 0, 3, "/", ":", " "));
               AV23Incidentes_DemandaNum2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Incidentes_DemandaNum2", AV23Incidentes_DemandaNum2);
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV26Incidentes_Data3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Incidentes_Data3", context.localUtil.TToC( AV26Incidentes_Data3, 8, 5, 0, 3, "/", ":", " "));
               AV27Incidentes_Data_To3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Incidentes_Data_To3", context.localUtil.TToC( AV27Incidentes_Data_To3, 8, 5, 0, 3, "/", ":", " "));
               AV28Incidentes_DemandaNum3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Incidentes_DemandaNum3", AV28Incidentes_DemandaNum3);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV37TFIncidentes_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFIncidentes_Data", context.localUtil.TToC( AV37TFIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
               AV38TFIncidentes_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFIncidentes_Data_To", context.localUtil.TToC( AV38TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " "));
               AV43TFIncidentes_DemandaNum = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFIncidentes_DemandaNum", AV43TFIncidentes_DemandaNum);
               AV44TFIncidentes_DemandaNum_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFIncidentes_DemandaNum_Sel", AV44TFIncidentes_DemandaNum_Sel);
               AV41ddo_Incidentes_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Incidentes_DataTitleControlIdToReplace", AV41ddo_Incidentes_DataTitleControlIdToReplace);
               AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace", AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace);
               AV72Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               A1241Incidentes_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV18Incidentes_DemandaNum1, AV20DynamicFiltersSelector2, AV21Incidentes_Data2, AV22Incidentes_Data_To2, AV23Incidentes_DemandaNum2, AV25DynamicFiltersSelector3, AV26Incidentes_Data3, AV27Incidentes_Data_To3, AV28Incidentes_DemandaNum3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFIncidentes_Data, AV38TFIncidentes_Data_To, AV43TFIncidentes_DemandaNum, AV44TFIncidentes_DemandaNum_Sel, AV41ddo_Incidentes_DataTitleControlIdToReplace, AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A1241Incidentes_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAJ42( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTJ42( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216234424");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontagemresultadoincidentes.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DATA1", context.localUtil.TToC( AV16Incidentes_Data1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DATA_TO1", context.localUtil.TToC( AV17Incidentes_Data_To1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DEMANDANUM1", AV18Incidentes_DemandaNum1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DATA2", context.localUtil.TToC( AV21Incidentes_Data2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DATA_TO2", context.localUtil.TToC( AV22Incidentes_Data_To2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DEMANDANUM2", AV23Incidentes_DemandaNum2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DATA3", context.localUtil.TToC( AV26Incidentes_Data3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DATA_TO3", context.localUtil.TToC( AV27Incidentes_Data_To3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DEMANDANUM3", AV28Incidentes_DemandaNum3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_DATA", context.localUtil.TToC( AV37TFIncidentes_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_DATA_TO", context.localUtil.TToC( AV38TFIncidentes_Data_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_DEMANDANUM", AV43TFIncidentes_DemandaNum);
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_DEMANDANUM_SEL", AV44TFIncidentes_DemandaNum_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_97", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_97), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV46DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV46DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vINCIDENTES_DATATITLEFILTERDATA", AV36Incidentes_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vINCIDENTES_DATATITLEFILTERDATA", AV36Incidentes_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vINCIDENTES_DEMANDANUMTITLEFILTERDATA", AV42Incidentes_DemandaNumTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vINCIDENTES_DEMANDANUMTITLEFILTERDATA", AV42Incidentes_DemandaNumTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV72Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Caption", StringUtil.RTrim( Ddo_incidentes_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Tooltip", StringUtil.RTrim( Ddo_incidentes_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Cls", StringUtil.RTrim( Ddo_incidentes_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_incidentes_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_incidentes_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_incidentes_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_incidentes_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_incidentes_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_incidentes_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Sortedstatus", StringUtil.RTrim( Ddo_incidentes_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Includefilter", StringUtil.BoolToStr( Ddo_incidentes_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Filtertype", StringUtil.RTrim( Ddo_incidentes_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_incidentes_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_incidentes_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Sortasc", StringUtil.RTrim( Ddo_incidentes_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Sortdsc", StringUtil.RTrim( Ddo_incidentes_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Cleanfilter", StringUtil.RTrim( Ddo_incidentes_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_incidentes_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Rangefilterto", StringUtil.RTrim( Ddo_incidentes_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_incidentes_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Caption", StringUtil.RTrim( Ddo_incidentes_demandanum_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Tooltip", StringUtil.RTrim( Ddo_incidentes_demandanum_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Cls", StringUtil.RTrim( Ddo_incidentes_demandanum_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Filteredtext_set", StringUtil.RTrim( Ddo_incidentes_demandanum_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Selectedvalue_set", StringUtil.RTrim( Ddo_incidentes_demandanum_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Dropdownoptionstype", StringUtil.RTrim( Ddo_incidentes_demandanum_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_incidentes_demandanum_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Includesortasc", StringUtil.BoolToStr( Ddo_incidentes_demandanum_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Includesortdsc", StringUtil.BoolToStr( Ddo_incidentes_demandanum_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Sortedstatus", StringUtil.RTrim( Ddo_incidentes_demandanum_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Includefilter", StringUtil.BoolToStr( Ddo_incidentes_demandanum_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Filtertype", StringUtil.RTrim( Ddo_incidentes_demandanum_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Filterisrange", StringUtil.BoolToStr( Ddo_incidentes_demandanum_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Includedatalist", StringUtil.BoolToStr( Ddo_incidentes_demandanum_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Datalisttype", StringUtil.RTrim( Ddo_incidentes_demandanum_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Datalistproc", StringUtil.RTrim( Ddo_incidentes_demandanum_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_incidentes_demandanum_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Sortasc", StringUtil.RTrim( Ddo_incidentes_demandanum_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Sortdsc", StringUtil.RTrim( Ddo_incidentes_demandanum_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Loadingdata", StringUtil.RTrim( Ddo_incidentes_demandanum_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Cleanfilter", StringUtil.RTrim( Ddo_incidentes_demandanum_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Noresultsfound", StringUtil.RTrim( Ddo_incidentes_demandanum_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Searchbuttontext", StringUtil.RTrim( Ddo_incidentes_demandanum_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Activeeventkey", StringUtil.RTrim( Ddo_incidentes_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_incidentes_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_incidentes_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Activeeventkey", StringUtil.RTrim( Ddo_incidentes_demandanum_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Filteredtext_get", StringUtil.RTrim( Ddo_incidentes_demandanum_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Selectedvalue_get", StringUtil.RTrim( Ddo_incidentes_demandanum_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEJ42( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTJ42( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontagemresultadoincidentes.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContagemResultadoIncidentes" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contagem Resultado Incidentes" ;
      }

      protected void WBJ40( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_J42( true) ;
         }
         else
         {
            wb_table1_2_J42( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_J42e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(108, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(109, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_97_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfincidentes_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfincidentes_data_Internalname, context.localUtil.TToC( AV37TFIncidentes_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV37TFIncidentes_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfincidentes_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfincidentes_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavTfincidentes_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfincidentes_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_97_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfincidentes_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfincidentes_data_to_Internalname, context.localUtil.TToC( AV38TFIncidentes_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV38TFIncidentes_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfincidentes_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfincidentes_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavTfincidentes_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfincidentes_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_incidentes_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_97_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_incidentes_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_incidentes_dataauxdate_Internalname, context.localUtil.Format(AV39DDO_Incidentes_DataAuxDate, "99/99/99"), context.localUtil.Format( AV39DDO_Incidentes_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_incidentes_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_incidentes_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_97_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_incidentes_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_incidentes_dataauxdateto_Internalname, context.localUtil.Format(AV40DDO_Incidentes_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV40DDO_Incidentes_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_incidentes_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_incidentes_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfincidentes_demandanum_Internalname, AV43TFIncidentes_DemandaNum, StringUtil.RTrim( context.localUtil.Format( AV43TFIncidentes_DemandaNum, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfincidentes_demandanum_Jsonclick, 0, "Attribute", "", "", "", edtavTfincidentes_demandanum_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemResultadoIncidentes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfincidentes_demandanum_sel_Internalname, AV44TFIncidentes_DemandaNum_Sel, StringUtil.RTrim( context.localUtil.Format( AV44TFIncidentes_DemandaNum_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfincidentes_demandanum_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfincidentes_demandanum_sel_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemResultadoIncidentes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_INCIDENTES_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_incidentes_datatitlecontrolidtoreplace_Internalname, AV41ddo_Incidentes_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", 0, edtavDdo_incidentes_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemResultadoIncidentes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_INCIDENTES_DEMANDANUMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Internalname, AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"", 0, edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemResultadoIncidentes.htm");
         }
         wbLoad = true;
      }

      protected void STARTJ42( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contagem Resultado Incidentes", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPJ40( ) ;
      }

      protected void WSJ42( )
      {
         STARTJ42( ) ;
         EVTJ42( ) ;
      }

      protected void EVTJ42( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11J42 */
                              E11J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_INCIDENTES_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12J42 */
                              E12J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_INCIDENTES_DEMANDANUM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13J42 */
                              E13J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14J42 */
                              E14J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15J42 */
                              E15J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16J42 */
                              E16J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17J42 */
                              E17J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18J42 */
                              E18J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19J42 */
                              E19J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20J42 */
                              E20J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21J42 */
                              E21J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22J42 */
                              E22J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23J42 */
                              E23J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24J42 */
                              E24J42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_97_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx), 4, 0)), 4, "0");
                              SubsflControlProps_972( ) ;
                              AV31Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV70Update_GXI : context.convertURL( context.PathToRelativeUrl( AV31Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV71Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              A1241Incidentes_Codigo = (int)(context.localUtil.CToN( cgiGet( edtIncidentes_Codigo_Internalname), ",", "."));
                              A1242Incidentes_Data = context.localUtil.CToT( cgiGet( edtIncidentes_Data_Internalname), 0);
                              A1239Incidentes_DemandaCod = (int)(context.localUtil.CToN( cgiGet( edtIncidentes_DemandaCod_Internalname), ",", "."));
                              A1240Incidentes_DemandaNum = StringUtil.Upper( cgiGet( edtIncidentes_DemandaNum_Internalname));
                              n1240Incidentes_DemandaNum = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25J42 */
                                    E25J42 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26J42 */
                                    E26J42 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27J42 */
                                    E27J42 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Incidentes_data1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA1"), 0) != AV16Incidentes_Data1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Incidentes_data_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA_TO1"), 0) != AV17Incidentes_Data_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Incidentes_demandanum1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vINCIDENTES_DEMANDANUM1"), AV18Incidentes_DemandaNum1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Incidentes_data2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA2"), 0) != AV21Incidentes_Data2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Incidentes_data_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA_TO2"), 0) != AV22Incidentes_Data_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Incidentes_demandanum2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vINCIDENTES_DEMANDANUM2"), AV23Incidentes_DemandaNum2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Incidentes_data3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA3"), 0) != AV26Incidentes_Data3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Incidentes_data_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA_TO3"), 0) != AV27Incidentes_Data_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Incidentes_demandanum3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vINCIDENTES_DEMANDANUM3"), AV28Incidentes_DemandaNum3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfincidentes_data Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFINCIDENTES_DATA"), 0) != AV37TFIncidentes_Data )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfincidentes_data_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFINCIDENTES_DATA_TO"), 0) != AV38TFIncidentes_Data_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfincidentes_demandanum Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINCIDENTES_DEMANDANUM"), AV43TFIncidentes_DemandaNum) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfincidentes_demandanum_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINCIDENTES_DEMANDANUM_SEL"), AV44TFIncidentes_DemandaNum_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEJ42( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAJ42( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("INCIDENTES_DATA", "Data", 0);
            cmbavDynamicfiltersselector1.addItem("INCIDENTES_DEMANDANUM", "Demanda", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("INCIDENTES_DATA", "Data", 0);
            cmbavDynamicfiltersselector2.addItem("INCIDENTES_DEMANDANUM", "Demanda", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("INCIDENTES_DATA", "Data", 0);
            cmbavDynamicfiltersselector3.addItem("INCIDENTES_DEMANDANUM", "Demanda", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_972( ) ;
         while ( nGXsfl_97_idx <= nRC_GXsfl_97 )
         {
            sendrow_972( ) ;
            nGXsfl_97_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_97_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_97_idx+1));
            sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx), 4, 0)), 4, "0");
            SubsflControlProps_972( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       DateTime AV16Incidentes_Data1 ,
                                       DateTime AV17Incidentes_Data_To1 ,
                                       String AV18Incidentes_DemandaNum1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       DateTime AV21Incidentes_Data2 ,
                                       DateTime AV22Incidentes_Data_To2 ,
                                       String AV23Incidentes_DemandaNum2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       DateTime AV26Incidentes_Data3 ,
                                       DateTime AV27Incidentes_Data_To3 ,
                                       String AV28Incidentes_DemandaNum3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       DateTime AV37TFIncidentes_Data ,
                                       DateTime AV38TFIncidentes_Data_To ,
                                       String AV43TFIncidentes_DemandaNum ,
                                       String AV44TFIncidentes_DemandaNum_Sel ,
                                       String AV41ddo_Incidentes_DataTitleControlIdToReplace ,
                                       String AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace ,
                                       String AV72Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving ,
                                       int A1241Incidentes_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFJ42( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1241Incidentes_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "INCIDENTES_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1241Incidentes_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_DATA", GetSecureSignedToken( "", context.localUtil.Format( A1242Incidentes_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "INCIDENTES_DATA", context.localUtil.TToC( A1242Incidentes_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_DEMANDACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1239Incidentes_DemandaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "INCIDENTES_DEMANDACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1239Incidentes_DemandaCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJ42( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV72Pgmname = "WWContagemResultadoIncidentes";
         context.Gx_err = 0;
      }

      protected void RFJ42( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 97;
         /* Execute user event: E26J42 */
         E26J42 ();
         nGXsfl_97_idx = 1;
         sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx), 4, 0)), 4, "0");
         SubsflControlProps_972( ) ;
         nGXsfl_97_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_972( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 ,
                                                 AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1 ,
                                                 AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 ,
                                                 AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 ,
                                                 AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 ,
                                                 AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 ,
                                                 AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2 ,
                                                 AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 ,
                                                 AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 ,
                                                 AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 ,
                                                 AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 ,
                                                 AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3 ,
                                                 AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 ,
                                                 AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 ,
                                                 AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data ,
                                                 AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to ,
                                                 AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel ,
                                                 AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum ,
                                                 A1242Incidentes_Data ,
                                                 A1240Incidentes_DemandaNum ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = StringUtil.Concat( StringUtil.RTrim( AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1), "%", "");
            lV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = StringUtil.Concat( StringUtil.RTrim( AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2), "%", "");
            lV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = StringUtil.Concat( StringUtil.RTrim( AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3), "%", "");
            lV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = StringUtil.Concat( StringUtil.RTrim( AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum), "%", "");
            /* Using cursor H00J42 */
            pr_default.execute(0, new Object[] {AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1, AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1, lV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1, AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2, AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2, lV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2, AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3, AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3, lV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3, AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data, AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to, lV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum, AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_97_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1240Incidentes_DemandaNum = H00J42_A1240Incidentes_DemandaNum[0];
               n1240Incidentes_DemandaNum = H00J42_n1240Incidentes_DemandaNum[0];
               A1239Incidentes_DemandaCod = H00J42_A1239Incidentes_DemandaCod[0];
               A1242Incidentes_Data = H00J42_A1242Incidentes_Data[0];
               A1241Incidentes_Codigo = H00J42_A1241Incidentes_Codigo[0];
               A1240Incidentes_DemandaNum = H00J42_A1240Incidentes_DemandaNum[0];
               n1240Incidentes_DemandaNum = H00J42_n1240Incidentes_DemandaNum[0];
               /* Execute user event: E27J42 */
               E27J42 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 97;
            WBJ40( ) ;
         }
         nGXsfl_97_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1 = AV16Incidentes_Data1;
         AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 = AV17Incidentes_Data_To1;
         AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = AV18Incidentes_DemandaNum1;
         AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2 = AV21Incidentes_Data2;
         AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 = AV22Incidentes_Data_To2;
         AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = AV23Incidentes_DemandaNum2;
         AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3 = AV26Incidentes_Data3;
         AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 = AV27Incidentes_Data_To3;
         AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = AV28Incidentes_DemandaNum3;
         AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data = AV37TFIncidentes_Data;
         AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to = AV38TFIncidentes_Data_To;
         AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = AV43TFIncidentes_DemandaNum;
         AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel = AV44TFIncidentes_DemandaNum_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 ,
                                              AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1 ,
                                              AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 ,
                                              AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 ,
                                              AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 ,
                                              AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 ,
                                              AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2 ,
                                              AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 ,
                                              AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 ,
                                              AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 ,
                                              AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 ,
                                              AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3 ,
                                              AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 ,
                                              AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 ,
                                              AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data ,
                                              AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to ,
                                              AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel ,
                                              AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum ,
                                              A1242Incidentes_Data ,
                                              A1240Incidentes_DemandaNum ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = StringUtil.Concat( StringUtil.RTrim( AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1), "%", "");
         lV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = StringUtil.Concat( StringUtil.RTrim( AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2), "%", "");
         lV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = StringUtil.Concat( StringUtil.RTrim( AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3), "%", "");
         lV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = StringUtil.Concat( StringUtil.RTrim( AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum), "%", "");
         /* Using cursor H00J43 */
         pr_default.execute(1, new Object[] {AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1, AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1, lV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1, AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2, AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2, lV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2, AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3, AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3, lV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3, AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data, AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to, lV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum, AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel});
         GRID_nRecordCount = H00J43_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1 = AV16Incidentes_Data1;
         AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 = AV17Incidentes_Data_To1;
         AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = AV18Incidentes_DemandaNum1;
         AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2 = AV21Incidentes_Data2;
         AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 = AV22Incidentes_Data_To2;
         AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = AV23Incidentes_DemandaNum2;
         AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3 = AV26Incidentes_Data3;
         AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 = AV27Incidentes_Data_To3;
         AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = AV28Incidentes_DemandaNum3;
         AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data = AV37TFIncidentes_Data;
         AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to = AV38TFIncidentes_Data_To;
         AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = AV43TFIncidentes_DemandaNum;
         AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel = AV44TFIncidentes_DemandaNum_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV18Incidentes_DemandaNum1, AV20DynamicFiltersSelector2, AV21Incidentes_Data2, AV22Incidentes_Data_To2, AV23Incidentes_DemandaNum2, AV25DynamicFiltersSelector3, AV26Incidentes_Data3, AV27Incidentes_Data_To3, AV28Incidentes_DemandaNum3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFIncidentes_Data, AV38TFIncidentes_Data_To, AV43TFIncidentes_DemandaNum, AV44TFIncidentes_DemandaNum_Sel, AV41ddo_Incidentes_DataTitleControlIdToReplace, AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A1241Incidentes_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1 = AV16Incidentes_Data1;
         AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 = AV17Incidentes_Data_To1;
         AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = AV18Incidentes_DemandaNum1;
         AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2 = AV21Incidentes_Data2;
         AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 = AV22Incidentes_Data_To2;
         AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = AV23Incidentes_DemandaNum2;
         AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3 = AV26Incidentes_Data3;
         AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 = AV27Incidentes_Data_To3;
         AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = AV28Incidentes_DemandaNum3;
         AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data = AV37TFIncidentes_Data;
         AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to = AV38TFIncidentes_Data_To;
         AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = AV43TFIncidentes_DemandaNum;
         AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel = AV44TFIncidentes_DemandaNum_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV18Incidentes_DemandaNum1, AV20DynamicFiltersSelector2, AV21Incidentes_Data2, AV22Incidentes_Data_To2, AV23Incidentes_DemandaNum2, AV25DynamicFiltersSelector3, AV26Incidentes_Data3, AV27Incidentes_Data_To3, AV28Incidentes_DemandaNum3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFIncidentes_Data, AV38TFIncidentes_Data_To, AV43TFIncidentes_DemandaNum, AV44TFIncidentes_DemandaNum_Sel, AV41ddo_Incidentes_DataTitleControlIdToReplace, AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A1241Incidentes_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1 = AV16Incidentes_Data1;
         AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 = AV17Incidentes_Data_To1;
         AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = AV18Incidentes_DemandaNum1;
         AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2 = AV21Incidentes_Data2;
         AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 = AV22Incidentes_Data_To2;
         AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = AV23Incidentes_DemandaNum2;
         AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3 = AV26Incidentes_Data3;
         AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 = AV27Incidentes_Data_To3;
         AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = AV28Incidentes_DemandaNum3;
         AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data = AV37TFIncidentes_Data;
         AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to = AV38TFIncidentes_Data_To;
         AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = AV43TFIncidentes_DemandaNum;
         AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel = AV44TFIncidentes_DemandaNum_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV18Incidentes_DemandaNum1, AV20DynamicFiltersSelector2, AV21Incidentes_Data2, AV22Incidentes_Data_To2, AV23Incidentes_DemandaNum2, AV25DynamicFiltersSelector3, AV26Incidentes_Data3, AV27Incidentes_Data_To3, AV28Incidentes_DemandaNum3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFIncidentes_Data, AV38TFIncidentes_Data_To, AV43TFIncidentes_DemandaNum, AV44TFIncidentes_DemandaNum_Sel, AV41ddo_Incidentes_DataTitleControlIdToReplace, AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A1241Incidentes_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1 = AV16Incidentes_Data1;
         AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 = AV17Incidentes_Data_To1;
         AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = AV18Incidentes_DemandaNum1;
         AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2 = AV21Incidentes_Data2;
         AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 = AV22Incidentes_Data_To2;
         AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = AV23Incidentes_DemandaNum2;
         AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3 = AV26Incidentes_Data3;
         AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 = AV27Incidentes_Data_To3;
         AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = AV28Incidentes_DemandaNum3;
         AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data = AV37TFIncidentes_Data;
         AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to = AV38TFIncidentes_Data_To;
         AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = AV43TFIncidentes_DemandaNum;
         AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel = AV44TFIncidentes_DemandaNum_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV18Incidentes_DemandaNum1, AV20DynamicFiltersSelector2, AV21Incidentes_Data2, AV22Incidentes_Data_To2, AV23Incidentes_DemandaNum2, AV25DynamicFiltersSelector3, AV26Incidentes_Data3, AV27Incidentes_Data_To3, AV28Incidentes_DemandaNum3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFIncidentes_Data, AV38TFIncidentes_Data_To, AV43TFIncidentes_DemandaNum, AV44TFIncidentes_DemandaNum_Sel, AV41ddo_Incidentes_DataTitleControlIdToReplace, AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A1241Incidentes_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1 = AV16Incidentes_Data1;
         AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 = AV17Incidentes_Data_To1;
         AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = AV18Incidentes_DemandaNum1;
         AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2 = AV21Incidentes_Data2;
         AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 = AV22Incidentes_Data_To2;
         AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = AV23Incidentes_DemandaNum2;
         AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3 = AV26Incidentes_Data3;
         AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 = AV27Incidentes_Data_To3;
         AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = AV28Incidentes_DemandaNum3;
         AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data = AV37TFIncidentes_Data;
         AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to = AV38TFIncidentes_Data_To;
         AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = AV43TFIncidentes_DemandaNum;
         AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel = AV44TFIncidentes_DemandaNum_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV18Incidentes_DemandaNum1, AV20DynamicFiltersSelector2, AV21Incidentes_Data2, AV22Incidentes_Data_To2, AV23Incidentes_DemandaNum2, AV25DynamicFiltersSelector3, AV26Incidentes_Data3, AV27Incidentes_Data_To3, AV28Incidentes_DemandaNum3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFIncidentes_Data, AV38TFIncidentes_Data_To, AV43TFIncidentes_DemandaNum, AV44TFIncidentes_DemandaNum_Sel, AV41ddo_Incidentes_DataTitleControlIdToReplace, AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A1241Incidentes_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPJ40( )
      {
         /* Before Start, stand alone formulas. */
         AV72Pgmname = "WWContagemResultadoIncidentes";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25J42 */
         E25J42 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV46DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vINCIDENTES_DATATITLEFILTERDATA"), AV36Incidentes_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vINCIDENTES_DEMANDANUMTITLEFILTERDATA"), AV42Incidentes_DemandaNumTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavIncidentes_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Incidentes_Data1"}), 1, "vINCIDENTES_DATA1");
               GX_FocusControl = edtavIncidentes_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16Incidentes_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Incidentes_Data1", context.localUtil.TToC( AV16Incidentes_Data1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV16Incidentes_Data1 = context.localUtil.CToT( cgiGet( edtavIncidentes_data1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Incidentes_Data1", context.localUtil.TToC( AV16Incidentes_Data1, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavIncidentes_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Incidentes_Data_To1"}), 1, "vINCIDENTES_DATA_TO1");
               GX_FocusControl = edtavIncidentes_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17Incidentes_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Incidentes_Data_To1", context.localUtil.TToC( AV17Incidentes_Data_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV17Incidentes_Data_To1 = context.localUtil.CToT( cgiGet( edtavIncidentes_data_to1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Incidentes_Data_To1", context.localUtil.TToC( AV17Incidentes_Data_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            AV18Incidentes_DemandaNum1 = StringUtil.Upper( cgiGet( edtavIncidentes_demandanum1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Incidentes_DemandaNum1", AV18Incidentes_DemandaNum1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavIncidentes_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Incidentes_Data2"}), 1, "vINCIDENTES_DATA2");
               GX_FocusControl = edtavIncidentes_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21Incidentes_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Incidentes_Data2", context.localUtil.TToC( AV21Incidentes_Data2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV21Incidentes_Data2 = context.localUtil.CToT( cgiGet( edtavIncidentes_data2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Incidentes_Data2", context.localUtil.TToC( AV21Incidentes_Data2, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavIncidentes_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Incidentes_Data_To2"}), 1, "vINCIDENTES_DATA_TO2");
               GX_FocusControl = edtavIncidentes_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22Incidentes_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Incidentes_Data_To2", context.localUtil.TToC( AV22Incidentes_Data_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV22Incidentes_Data_To2 = context.localUtil.CToT( cgiGet( edtavIncidentes_data_to2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Incidentes_Data_To2", context.localUtil.TToC( AV22Incidentes_Data_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            AV23Incidentes_DemandaNum2 = StringUtil.Upper( cgiGet( edtavIncidentes_demandanum2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Incidentes_DemandaNum2", AV23Incidentes_DemandaNum2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavIncidentes_data3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Incidentes_Data3"}), 1, "vINCIDENTES_DATA3");
               GX_FocusControl = edtavIncidentes_data3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26Incidentes_Data3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Incidentes_Data3", context.localUtil.TToC( AV26Incidentes_Data3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV26Incidentes_Data3 = context.localUtil.CToT( cgiGet( edtavIncidentes_data3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Incidentes_Data3", context.localUtil.TToC( AV26Incidentes_Data3, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavIncidentes_data_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Incidentes_Data_To3"}), 1, "vINCIDENTES_DATA_TO3");
               GX_FocusControl = edtavIncidentes_data_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV27Incidentes_Data_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Incidentes_Data_To3", context.localUtil.TToC( AV27Incidentes_Data_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV27Incidentes_Data_To3 = context.localUtil.CToT( cgiGet( edtavIncidentes_data_to3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Incidentes_Data_To3", context.localUtil.TToC( AV27Incidentes_Data_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            AV28Incidentes_DemandaNum3 = StringUtil.Upper( cgiGet( edtavIncidentes_demandanum3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Incidentes_DemandaNum3", AV28Incidentes_DemandaNum3);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfincidentes_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFIncidentes_Data"}), 1, "vTFINCIDENTES_DATA");
               GX_FocusControl = edtavTfincidentes_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37TFIncidentes_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFIncidentes_Data", context.localUtil.TToC( AV37TFIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV37TFIncidentes_Data = context.localUtil.CToT( cgiGet( edtavTfincidentes_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFIncidentes_Data", context.localUtil.TToC( AV37TFIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfincidentes_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFIncidentes_Data_To"}), 1, "vTFINCIDENTES_DATA_TO");
               GX_FocusControl = edtavTfincidentes_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFIncidentes_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFIncidentes_Data_To", context.localUtil.TToC( AV38TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV38TFIncidentes_Data_To = context.localUtil.CToT( cgiGet( edtavTfincidentes_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFIncidentes_Data_To", context.localUtil.TToC( AV38TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_incidentes_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Incidentes_Data Aux Date"}), 1, "vDDO_INCIDENTES_DATAAUXDATE");
               GX_FocusControl = edtavDdo_incidentes_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39DDO_Incidentes_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DDO_Incidentes_DataAuxDate", context.localUtil.Format(AV39DDO_Incidentes_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV39DDO_Incidentes_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_incidentes_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DDO_Incidentes_DataAuxDate", context.localUtil.Format(AV39DDO_Incidentes_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_incidentes_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Incidentes_Data Aux Date To"}), 1, "vDDO_INCIDENTES_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_incidentes_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40DDO_Incidentes_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DDO_Incidentes_DataAuxDateTo", context.localUtil.Format(AV40DDO_Incidentes_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV40DDO_Incidentes_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_incidentes_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DDO_Incidentes_DataAuxDateTo", context.localUtil.Format(AV40DDO_Incidentes_DataAuxDateTo, "99/99/99"));
            }
            AV43TFIncidentes_DemandaNum = StringUtil.Upper( cgiGet( edtavTfincidentes_demandanum_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFIncidentes_DemandaNum", AV43TFIncidentes_DemandaNum);
            AV44TFIncidentes_DemandaNum_Sel = StringUtil.Upper( cgiGet( edtavTfincidentes_demandanum_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFIncidentes_DemandaNum_Sel", AV44TFIncidentes_DemandaNum_Sel);
            AV41ddo_Incidentes_DataTitleControlIdToReplace = cgiGet( edtavDdo_incidentes_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Incidentes_DataTitleControlIdToReplace", AV41ddo_Incidentes_DataTitleControlIdToReplace);
            AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace = cgiGet( edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace", AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_97 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_97"), ",", "."));
            AV48GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV49GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_incidentes_data_Caption = cgiGet( "DDO_INCIDENTES_DATA_Caption");
            Ddo_incidentes_data_Tooltip = cgiGet( "DDO_INCIDENTES_DATA_Tooltip");
            Ddo_incidentes_data_Cls = cgiGet( "DDO_INCIDENTES_DATA_Cls");
            Ddo_incidentes_data_Filteredtext_set = cgiGet( "DDO_INCIDENTES_DATA_Filteredtext_set");
            Ddo_incidentes_data_Filteredtextto_set = cgiGet( "DDO_INCIDENTES_DATA_Filteredtextto_set");
            Ddo_incidentes_data_Dropdownoptionstype = cgiGet( "DDO_INCIDENTES_DATA_Dropdownoptionstype");
            Ddo_incidentes_data_Titlecontrolidtoreplace = cgiGet( "DDO_INCIDENTES_DATA_Titlecontrolidtoreplace");
            Ddo_incidentes_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DATA_Includesortasc"));
            Ddo_incidentes_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DATA_Includesortdsc"));
            Ddo_incidentes_data_Sortedstatus = cgiGet( "DDO_INCIDENTES_DATA_Sortedstatus");
            Ddo_incidentes_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DATA_Includefilter"));
            Ddo_incidentes_data_Filtertype = cgiGet( "DDO_INCIDENTES_DATA_Filtertype");
            Ddo_incidentes_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DATA_Filterisrange"));
            Ddo_incidentes_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DATA_Includedatalist"));
            Ddo_incidentes_data_Sortasc = cgiGet( "DDO_INCIDENTES_DATA_Sortasc");
            Ddo_incidentes_data_Sortdsc = cgiGet( "DDO_INCIDENTES_DATA_Sortdsc");
            Ddo_incidentes_data_Cleanfilter = cgiGet( "DDO_INCIDENTES_DATA_Cleanfilter");
            Ddo_incidentes_data_Rangefilterfrom = cgiGet( "DDO_INCIDENTES_DATA_Rangefilterfrom");
            Ddo_incidentes_data_Rangefilterto = cgiGet( "DDO_INCIDENTES_DATA_Rangefilterto");
            Ddo_incidentes_data_Searchbuttontext = cgiGet( "DDO_INCIDENTES_DATA_Searchbuttontext");
            Ddo_incidentes_demandanum_Caption = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Caption");
            Ddo_incidentes_demandanum_Tooltip = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Tooltip");
            Ddo_incidentes_demandanum_Cls = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Cls");
            Ddo_incidentes_demandanum_Filteredtext_set = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Filteredtext_set");
            Ddo_incidentes_demandanum_Selectedvalue_set = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Selectedvalue_set");
            Ddo_incidentes_demandanum_Dropdownoptionstype = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Dropdownoptionstype");
            Ddo_incidentes_demandanum_Titlecontrolidtoreplace = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Titlecontrolidtoreplace");
            Ddo_incidentes_demandanum_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDANUM_Includesortasc"));
            Ddo_incidentes_demandanum_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDANUM_Includesortdsc"));
            Ddo_incidentes_demandanum_Sortedstatus = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Sortedstatus");
            Ddo_incidentes_demandanum_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDANUM_Includefilter"));
            Ddo_incidentes_demandanum_Filtertype = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Filtertype");
            Ddo_incidentes_demandanum_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDANUM_Filterisrange"));
            Ddo_incidentes_demandanum_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDANUM_Includedatalist"));
            Ddo_incidentes_demandanum_Datalisttype = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Datalisttype");
            Ddo_incidentes_demandanum_Datalistproc = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Datalistproc");
            Ddo_incidentes_demandanum_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_INCIDENTES_DEMANDANUM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_incidentes_demandanum_Sortasc = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Sortasc");
            Ddo_incidentes_demandanum_Sortdsc = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Sortdsc");
            Ddo_incidentes_demandanum_Loadingdata = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Loadingdata");
            Ddo_incidentes_demandanum_Cleanfilter = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Cleanfilter");
            Ddo_incidentes_demandanum_Noresultsfound = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Noresultsfound");
            Ddo_incidentes_demandanum_Searchbuttontext = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_incidentes_data_Activeeventkey = cgiGet( "DDO_INCIDENTES_DATA_Activeeventkey");
            Ddo_incidentes_data_Filteredtext_get = cgiGet( "DDO_INCIDENTES_DATA_Filteredtext_get");
            Ddo_incidentes_data_Filteredtextto_get = cgiGet( "DDO_INCIDENTES_DATA_Filteredtextto_get");
            Ddo_incidentes_demandanum_Activeeventkey = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Activeeventkey");
            Ddo_incidentes_demandanum_Filteredtext_get = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Filteredtext_get");
            Ddo_incidentes_demandanum_Selectedvalue_get = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA1"), 0) != AV16Incidentes_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA_TO1"), 0) != AV17Incidentes_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vINCIDENTES_DEMANDANUM1"), AV18Incidentes_DemandaNum1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA2"), 0) != AV21Incidentes_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA_TO2"), 0) != AV22Incidentes_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vINCIDENTES_DEMANDANUM2"), AV23Incidentes_DemandaNum2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA3"), 0) != AV26Incidentes_Data3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA_TO3"), 0) != AV27Incidentes_Data_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vINCIDENTES_DEMANDANUM3"), AV28Incidentes_DemandaNum3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFINCIDENTES_DATA"), 0) != AV37TFIncidentes_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFINCIDENTES_DATA_TO"), 0) != AV38TFIncidentes_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINCIDENTES_DEMANDANUM"), AV43TFIncidentes_DemandaNum) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINCIDENTES_DEMANDANUM_SEL"), AV44TFIncidentes_DemandaNum_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25J42 */
         E25J42 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25J42( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "INCIDENTES_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "INCIDENTES_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "INCIDENTES_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfincidentes_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_data_Visible), 5, 0)));
         edtavTfincidentes_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_data_to_Visible), 5, 0)));
         edtavTfincidentes_demandanum_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_demandanum_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_demandanum_Visible), 5, 0)));
         edtavTfincidentes_demandanum_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_demandanum_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_demandanum_sel_Visible), 5, 0)));
         Ddo_incidentes_data_Titlecontrolidtoreplace = subGrid_Internalname+"_Incidentes_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "TitleControlIdToReplace", Ddo_incidentes_data_Titlecontrolidtoreplace);
         AV41ddo_Incidentes_DataTitleControlIdToReplace = Ddo_incidentes_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Incidentes_DataTitleControlIdToReplace", AV41ddo_Incidentes_DataTitleControlIdToReplace);
         edtavDdo_incidentes_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_incidentes_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_incidentes_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_incidentes_demandanum_Titlecontrolidtoreplace = subGrid_Internalname+"_Incidentes_DemandaNum";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "TitleControlIdToReplace", Ddo_incidentes_demandanum_Titlecontrolidtoreplace);
         AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace = Ddo_incidentes_demandanum_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace", AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace);
         edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contagem Resultado Incidentes";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data", 0);
         cmbavOrderedby.addItem("2", "Demanda", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV46DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV46DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E26J42( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV36Incidentes_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42Incidentes_DemandaNumTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtIncidentes_Data_Titleformat = 2;
         edtIncidentes_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV41ddo_Incidentes_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_Data_Internalname, "Title", edtIncidentes_Data_Title);
         edtIncidentes_DemandaNum_Titleformat = 2;
         edtIncidentes_DemandaNum_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Demanda", AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_DemandaNum_Internalname, "Title", edtIncidentes_DemandaNum_Title);
         AV48GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48GridCurrentPage), 10, 0)));
         AV49GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49GridPageCount), 10, 0)));
         AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1 = AV16Incidentes_Data1;
         AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 = AV17Incidentes_Data_To1;
         AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = AV18Incidentes_DemandaNum1;
         AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2 = AV21Incidentes_Data2;
         AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 = AV22Incidentes_Data_To2;
         AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = AV23Incidentes_DemandaNum2;
         AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3 = AV26Incidentes_Data3;
         AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 = AV27Incidentes_Data_To3;
         AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = AV28Incidentes_DemandaNum3;
         AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data = AV37TFIncidentes_Data;
         AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to = AV38TFIncidentes_Data_To;
         AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = AV43TFIncidentes_DemandaNum;
         AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel = AV44TFIncidentes_DemandaNum_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36Incidentes_DataTitleFilterData", AV36Incidentes_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42Incidentes_DemandaNumTitleFilterData", AV42Incidentes_DemandaNumTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11J42( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV47PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV47PageToGo) ;
         }
      }

      protected void E12J42( )
      {
         /* Ddo_incidentes_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_incidentes_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "SortedStatus", Ddo_incidentes_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "SortedStatus", Ddo_incidentes_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFIncidentes_Data = context.localUtil.CToT( Ddo_incidentes_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFIncidentes_Data", context.localUtil.TToC( AV37TFIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
            AV38TFIncidentes_Data_To = context.localUtil.CToT( Ddo_incidentes_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFIncidentes_Data_To", context.localUtil.TToC( AV38TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV38TFIncidentes_Data_To) )
            {
               AV38TFIncidentes_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV38TFIncidentes_Data_To)), (short)(DateTimeUtil.Month( AV38TFIncidentes_Data_To)), (short)(DateTimeUtil.Day( AV38TFIncidentes_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFIncidentes_Data_To", context.localUtil.TToC( AV38TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13J42( )
      {
         /* Ddo_incidentes_demandanum_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_incidentes_demandanum_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_demandanum_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "SortedStatus", Ddo_incidentes_demandanum_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_demandanum_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_demandanum_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "SortedStatus", Ddo_incidentes_demandanum_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_demandanum_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFIncidentes_DemandaNum = Ddo_incidentes_demandanum_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFIncidentes_DemandaNum", AV43TFIncidentes_DemandaNum);
            AV44TFIncidentes_DemandaNum_Sel = Ddo_incidentes_demandanum_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFIncidentes_DemandaNum_Sel", AV44TFIncidentes_DemandaNum_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E27J42( )
      {
         /* Grid_Load Routine */
         AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
         AV70Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contagemresultadoincidentes.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1241Incidentes_Codigo);
         AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
         AV71Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contagemresultadoincidentes.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1241Incidentes_Codigo);
         edtIncidentes_Data_Link = formatLink("viewcontagemresultadoincidentes.aspx") + "?" + UrlEncode("" +A1241Incidentes_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 97;
         }
         sendrow_972( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_97_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(97, GridRow);
         }
      }

      protected void E14J42( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E20J42( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E15J42( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV18Incidentes_DemandaNum1, AV20DynamicFiltersSelector2, AV21Incidentes_Data2, AV22Incidentes_Data_To2, AV23Incidentes_DemandaNum2, AV25DynamicFiltersSelector3, AV26Incidentes_Data3, AV27Incidentes_Data_To3, AV28Incidentes_DemandaNum3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFIncidentes_Data, AV38TFIncidentes_Data_To, AV43TFIncidentes_DemandaNum, AV44TFIncidentes_DemandaNum_Sel, AV41ddo_Incidentes_DataTitleControlIdToReplace, AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A1241Incidentes_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21J42( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22J42( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E16J42( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV18Incidentes_DemandaNum1, AV20DynamicFiltersSelector2, AV21Incidentes_Data2, AV22Incidentes_Data_To2, AV23Incidentes_DemandaNum2, AV25DynamicFiltersSelector3, AV26Incidentes_Data3, AV27Incidentes_Data_To3, AV28Incidentes_DemandaNum3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFIncidentes_Data, AV38TFIncidentes_Data_To, AV43TFIncidentes_DemandaNum, AV44TFIncidentes_DemandaNum_Sel, AV41ddo_Incidentes_DataTitleControlIdToReplace, AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A1241Incidentes_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23J42( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17J42( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV18Incidentes_DemandaNum1, AV20DynamicFiltersSelector2, AV21Incidentes_Data2, AV22Incidentes_Data_To2, AV23Incidentes_DemandaNum2, AV25DynamicFiltersSelector3, AV26Incidentes_Data3, AV27Incidentes_Data_To3, AV28Incidentes_DemandaNum3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFIncidentes_Data, AV38TFIncidentes_Data_To, AV43TFIncidentes_DemandaNum, AV44TFIncidentes_DemandaNum_Sel, AV41ddo_Incidentes_DataTitleControlIdToReplace, AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A1241Incidentes_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E24J42( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18J42( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E19J42( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contagemresultadoincidentes.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_incidentes_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "SortedStatus", Ddo_incidentes_data_Sortedstatus);
         Ddo_incidentes_demandanum_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "SortedStatus", Ddo_incidentes_demandanum_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_incidentes_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "SortedStatus", Ddo_incidentes_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_incidentes_demandanum_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "SortedStatus", Ddo_incidentes_demandanum_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfiltersincidentes_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersincidentes_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersincidentes_data1_Visible), 5, 0)));
         edtavIncidentes_demandanum1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIncidentes_demandanum1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIncidentes_demandanum1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersincidentes_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersincidentes_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersincidentes_data1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "INCIDENTES_DEMANDANUM") == 0 )
         {
            edtavIncidentes_demandanum1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIncidentes_demandanum1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIncidentes_demandanum1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfiltersincidentes_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersincidentes_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersincidentes_data2_Visible), 5, 0)));
         edtavIncidentes_demandanum2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIncidentes_demandanum2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIncidentes_demandanum2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersincidentes_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersincidentes_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersincidentes_data2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "INCIDENTES_DEMANDANUM") == 0 )
         {
            edtavIncidentes_demandanum2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIncidentes_demandanum2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIncidentes_demandanum2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfiltersincidentes_data3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersincidentes_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersincidentes_data3_Visible), 5, 0)));
         edtavIncidentes_demandanum3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIncidentes_demandanum3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIncidentes_demandanum3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersincidentes_data3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersincidentes_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersincidentes_data3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "INCIDENTES_DEMANDANUM") == 0 )
         {
            edtavIncidentes_demandanum3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIncidentes_demandanum3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIncidentes_demandanum3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "INCIDENTES_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21Incidentes_Data2 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Incidentes_Data2", context.localUtil.TToC( AV21Incidentes_Data2, 8, 5, 0, 3, "/", ":", " "));
         AV22Incidentes_Data_To2 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Incidentes_Data_To2", context.localUtil.TToC( AV22Incidentes_Data_To2, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "INCIDENTES_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV26Incidentes_Data3 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Incidentes_Data3", context.localUtil.TToC( AV26Incidentes_Data3, 8, 5, 0, 3, "/", ":", " "));
         AV27Incidentes_Data_To3 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Incidentes_Data_To3", context.localUtil.TToC( AV27Incidentes_Data_To3, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV37TFIncidentes_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFIncidentes_Data", context.localUtil.TToC( AV37TFIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
         Ddo_incidentes_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "FilteredText_set", Ddo_incidentes_data_Filteredtext_set);
         AV38TFIncidentes_Data_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFIncidentes_Data_To", context.localUtil.TToC( AV38TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_incidentes_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "FilteredTextTo_set", Ddo_incidentes_data_Filteredtextto_set);
         AV43TFIncidentes_DemandaNum = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFIncidentes_DemandaNum", AV43TFIncidentes_DemandaNum);
         Ddo_incidentes_demandanum_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "FilteredText_set", Ddo_incidentes_demandanum_Filteredtext_set);
         AV44TFIncidentes_DemandaNum_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFIncidentes_DemandaNum_Sel", AV44TFIncidentes_DemandaNum_Sel);
         Ddo_incidentes_demandanum_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "SelectedValue_set", Ddo_incidentes_demandanum_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "INCIDENTES_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16Incidentes_Data1 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Incidentes_Data1", context.localUtil.TToC( AV16Incidentes_Data1, 8, 5, 0, 3, "/", ":", " "));
         AV17Incidentes_Data_To1 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Incidentes_Data_To1", context.localUtil.TToC( AV17Incidentes_Data_To1, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get(AV72Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV72Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV33Session.Get(AV72Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV73GXV1 = 1;
         while ( AV73GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV73GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFINCIDENTES_DATA") == 0 )
            {
               AV37TFIncidentes_Data = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFIncidentes_Data", context.localUtil.TToC( AV37TFIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
               AV38TFIncidentes_Data_To = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFIncidentes_Data_To", context.localUtil.TToC( AV38TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV37TFIncidentes_Data) )
               {
                  AV39DDO_Incidentes_DataAuxDate = DateTimeUtil.ResetTime(AV37TFIncidentes_Data);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DDO_Incidentes_DataAuxDate", context.localUtil.Format(AV39DDO_Incidentes_DataAuxDate, "99/99/99"));
                  Ddo_incidentes_data_Filteredtext_set = context.localUtil.DToC( AV39DDO_Incidentes_DataAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "FilteredText_set", Ddo_incidentes_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV38TFIncidentes_Data_To) )
               {
                  AV40DDO_Incidentes_DataAuxDateTo = DateTimeUtil.ResetTime(AV38TFIncidentes_Data_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DDO_Incidentes_DataAuxDateTo", context.localUtil.Format(AV40DDO_Incidentes_DataAuxDateTo, "99/99/99"));
                  Ddo_incidentes_data_Filteredtextto_set = context.localUtil.DToC( AV40DDO_Incidentes_DataAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "FilteredTextTo_set", Ddo_incidentes_data_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFINCIDENTES_DEMANDANUM") == 0 )
            {
               AV43TFIncidentes_DemandaNum = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFIncidentes_DemandaNum", AV43TFIncidentes_DemandaNum);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFIncidentes_DemandaNum)) )
               {
                  Ddo_incidentes_demandanum_Filteredtext_set = AV43TFIncidentes_DemandaNum;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "FilteredText_set", Ddo_incidentes_demandanum_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFINCIDENTES_DEMANDANUM_SEL") == 0 )
            {
               AV44TFIncidentes_DemandaNum_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFIncidentes_DemandaNum_Sel", AV44TFIncidentes_DemandaNum_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFIncidentes_DemandaNum_Sel)) )
               {
                  Ddo_incidentes_demandanum_Selectedvalue_set = AV44TFIncidentes_DemandaNum_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "SelectedValue_set", Ddo_incidentes_demandanum_Selectedvalue_set);
               }
            }
            AV73GXV1 = (int)(AV73GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 )
            {
               AV16Incidentes_Data1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Incidentes_Data1", context.localUtil.TToC( AV16Incidentes_Data1, 8, 5, 0, 3, "/", ":", " "));
               AV17Incidentes_Data_To1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Incidentes_Data_To1", context.localUtil.TToC( AV17Incidentes_Data_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "INCIDENTES_DEMANDANUM") == 0 )
            {
               AV18Incidentes_DemandaNum1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Incidentes_DemandaNum1", AV18Incidentes_DemandaNum1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 )
               {
                  AV21Incidentes_Data2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Incidentes_Data2", context.localUtil.TToC( AV21Incidentes_Data2, 8, 5, 0, 3, "/", ":", " "));
                  AV22Incidentes_Data_To2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Incidentes_Data_To2", context.localUtil.TToC( AV22Incidentes_Data_To2, 8, 5, 0, 3, "/", ":", " "));
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "INCIDENTES_DEMANDANUM") == 0 )
               {
                  AV23Incidentes_DemandaNum2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Incidentes_DemandaNum2", AV23Incidentes_DemandaNum2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 )
                  {
                     AV26Incidentes_Data3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Incidentes_Data3", context.localUtil.TToC( AV26Incidentes_Data3, 8, 5, 0, 3, "/", ":", " "));
                     AV27Incidentes_Data_To3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Incidentes_Data_To3", context.localUtil.TToC( AV27Incidentes_Data_To3, 8, 5, 0, 3, "/", ":", " "));
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "INCIDENTES_DEMANDANUM") == 0 )
                  {
                     AV28Incidentes_DemandaNum3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Incidentes_DemandaNum3", AV28Incidentes_DemandaNum3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV33Session.Get(AV72Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (DateTime.MinValue==AV37TFIncidentes_Data) && (DateTime.MinValue==AV38TFIncidentes_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINCIDENTES_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV37TFIncidentes_Data, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV38TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFIncidentes_DemandaNum)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINCIDENTES_DEMANDANUM";
            AV11GridStateFilterValue.gxTpr_Value = AV43TFIncidentes_DemandaNum;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFIncidentes_DemandaNum_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINCIDENTES_DEMANDANUM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFIncidentes_DemandaNum_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV72Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 ) && ! ( (DateTime.MinValue==AV16Incidentes_Data1) && (DateTime.MinValue==AV17Incidentes_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV16Incidentes_Data1, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV17Incidentes_Data_To1, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "INCIDENTES_DEMANDANUM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Incidentes_DemandaNum1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18Incidentes_DemandaNum1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 ) && ! ( (DateTime.MinValue==AV21Incidentes_Data2) && (DateTime.MinValue==AV22Incidentes_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV21Incidentes_Data2, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV22Incidentes_Data_To2, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "INCIDENTES_DEMANDANUM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Incidentes_DemandaNum2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23Incidentes_DemandaNum2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 ) && ! ( (DateTime.MinValue==AV26Incidentes_Data3) && (DateTime.MinValue==AV27Incidentes_Data_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV26Incidentes_Data3, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV27Incidentes_Data_To3, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "INCIDENTES_DEMANDANUM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Incidentes_DemandaNum3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV28Incidentes_DemandaNum3;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV72Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemResultadoIncidentes";
         AV33Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_J42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_J42( true) ;
         }
         else
         {
            wb_table2_8_J42( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_J42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_91_J42( true) ;
         }
         else
         {
            wb_table3_91_J42( false) ;
         }
         return  ;
      }

      protected void wb_table3_91_J42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_J42e( true) ;
         }
         else
         {
            wb_table1_2_J42e( false) ;
         }
      }

      protected void wb_table3_91_J42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_94_J42( true) ;
         }
         else
         {
            wb_table4_94_J42( false) ;
         }
         return  ;
      }

      protected void wb_table4_94_J42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_91_J42e( true) ;
         }
         else
         {
            wb_table3_91_J42e( false) ;
         }
      }

      protected void wb_table4_94_J42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"97\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtIncidentes_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtIncidentes_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtIncidentes_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtIncidentes_DemandaNum_Titleformat == 0 )
               {
                  context.SendWebValue( edtIncidentes_DemandaNum_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtIncidentes_DemandaNum_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1241Incidentes_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1242Incidentes_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtIncidentes_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIncidentes_Data_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtIncidentes_Data_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1239Incidentes_DemandaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1240Incidentes_DemandaNum);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtIncidentes_DemandaNum_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIncidentes_DemandaNum_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 97 )
         {
            wbEnd = 0;
            nRC_GXsfl_97 = (short)(nGXsfl_97_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_94_J42e( true) ;
         }
         else
         {
            wb_table4_94_J42e( false) ;
         }
      }

      protected void wb_table2_8_J42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadoincidentestitle_Internalname, " Incidentes", "", "", lblContagemresultadoincidentestitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_J42( true) ;
         }
         else
         {
            wb_table5_13_J42( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_J42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWContagemResultadoIncidentes.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_J42( true) ;
         }
         else
         {
            wb_table6_23_J42( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_J42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_J42e( true) ;
         }
         else
         {
            wb_table2_8_J42e( false) ;
         }
      }

      protected void wb_table6_23_J42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_J42( true) ;
         }
         else
         {
            wb_table7_28_J42( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_J42e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_J42e( true) ;
         }
         else
         {
            wb_table6_23_J42e( false) ;
         }
      }

      protected void wb_table7_28_J42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWContagemResultadoIncidentes.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_J42( true) ;
         }
         else
         {
            wb_table8_37_J42( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_J42e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_demandanum1_Internalname, AV18Incidentes_DemandaNum1, StringUtil.RTrim( context.localUtil.Format( AV18Incidentes_DemandaNum1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_demandanum1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavIncidentes_demandanum1_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_WWContagemResultadoIncidentes.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_57_J42( true) ;
         }
         else
         {
            wb_table9_57_J42( false) ;
         }
         return  ;
      }

      protected void wb_table9_57_J42e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_demandanum2_Internalname, AV23Incidentes_DemandaNum2, StringUtil.RTrim( context.localUtil.Format( AV23Incidentes_DemandaNum2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_demandanum2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavIncidentes_demandanum2_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", "", true, "HLP_WWContagemResultadoIncidentes.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_77_J42( true) ;
         }
         else
         {
            wb_table10_77_J42( false) ;
         }
         return  ;
      }

      protected void wb_table10_77_J42e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_demandanum3_Internalname, AV28Incidentes_DemandaNum3, StringUtil.RTrim( context.localUtil.Format( AV28Incidentes_DemandaNum3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_demandanum3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavIncidentes_demandanum3_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_J42e( true) ;
         }
         else
         {
            wb_table7_28_J42e( false) ;
         }
      }

      protected void wb_table10_77_J42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersincidentes_data3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersincidentes_data3_Internalname, tblTablemergeddynamicfiltersincidentes_data3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_97_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIncidentes_data3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_data3_Internalname, context.localUtil.TToC( AV26Incidentes_Data3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV26Incidentes_Data3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_data3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavIncidentes_data3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersincidentes_data_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfiltersincidentes_data_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_97_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIncidentes_data_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_data_to3_Internalname, context.localUtil.TToC( AV27Incidentes_Data_To3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV27Incidentes_Data_To3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_data_to3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavIncidentes_data_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_77_J42e( true) ;
         }
         else
         {
            wb_table10_77_J42e( false) ;
         }
      }

      protected void wb_table9_57_J42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersincidentes_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersincidentes_data2_Internalname, tblTablemergeddynamicfiltersincidentes_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_97_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIncidentes_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_data2_Internalname, context.localUtil.TToC( AV21Incidentes_Data2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV21Incidentes_Data2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_data2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavIncidentes_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersincidentes_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfiltersincidentes_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_97_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIncidentes_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_data_to2_Internalname, context.localUtil.TToC( AV22Incidentes_Data_To2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV22Incidentes_Data_To2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_data_to2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavIncidentes_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_57_J42e( true) ;
         }
         else
         {
            wb_table9_57_J42e( false) ;
         }
      }

      protected void wb_table8_37_J42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersincidentes_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersincidentes_data1_Internalname, tblTablemergeddynamicfiltersincidentes_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_97_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIncidentes_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_data1_Internalname, context.localUtil.TToC( AV16Incidentes_Data1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV16Incidentes_Data1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_data1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavIncidentes_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersincidentes_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfiltersincidentes_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_97_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIncidentes_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_data_to1_Internalname, context.localUtil.TToC( AV17Incidentes_Data_To1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV17Incidentes_Data_To1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_data_to1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavIncidentes_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_J42e( true) ;
         }
         else
         {
            wb_table8_37_J42e( false) ;
         }
      }

      protected void wb_table5_13_J42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_J42e( true) ;
         }
         else
         {
            wb_table5_13_J42e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJ42( ) ;
         WSJ42( ) ;
         WEJ42( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216234855");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontagemresultadoincidentes.js", "?20206216234856");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_972( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_97_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_97_idx;
         edtIncidentes_Codigo_Internalname = "INCIDENTES_CODIGO_"+sGXsfl_97_idx;
         edtIncidentes_Data_Internalname = "INCIDENTES_DATA_"+sGXsfl_97_idx;
         edtIncidentes_DemandaCod_Internalname = "INCIDENTES_DEMANDACOD_"+sGXsfl_97_idx;
         edtIncidentes_DemandaNum_Internalname = "INCIDENTES_DEMANDANUM_"+sGXsfl_97_idx;
      }

      protected void SubsflControlProps_fel_972( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_97_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_97_fel_idx;
         edtIncidentes_Codigo_Internalname = "INCIDENTES_CODIGO_"+sGXsfl_97_fel_idx;
         edtIncidentes_Data_Internalname = "INCIDENTES_DATA_"+sGXsfl_97_fel_idx;
         edtIncidentes_DemandaCod_Internalname = "INCIDENTES_DEMANDACOD_"+sGXsfl_97_fel_idx;
         edtIncidentes_DemandaNum_Internalname = "INCIDENTES_DEMANDANUM_"+sGXsfl_97_fel_idx;
      }

      protected void sendrow_972( )
      {
         SubsflControlProps_972( ) ;
         WBJ40( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_97_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_97_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_97_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV70Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV70Update_GXI : context.PathToRelativeUrl( AV31Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV71Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV71Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIncidentes_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1241Incidentes_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1241Incidentes_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtIncidentes_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIncidentes_Data_Internalname,context.localUtil.TToC( A1242Incidentes_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1242Incidentes_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtIncidentes_Data_Link,(String)"",(String)"",(String)"",(String)edtIncidentes_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIncidentes_DemandaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1239Incidentes_DemandaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1239Incidentes_DemandaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtIncidentes_DemandaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIncidentes_DemandaNum_Internalname,(String)A1240Incidentes_DemandaNum,StringUtil.RTrim( context.localUtil.Format( A1240Incidentes_DemandaNum, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtIncidentes_DemandaNum_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_CODIGO"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, context.localUtil.Format( (decimal)(A1241Incidentes_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_DATA"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, context.localUtil.Format( A1242Incidentes_Data, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_DEMANDACOD"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, context.localUtil.Format( (decimal)(A1239Incidentes_DemandaCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_97_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_97_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_97_idx+1));
            sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx), 4, 0)), 4, "0");
            SubsflControlProps_972( ) ;
         }
         /* End function sendrow_972 */
      }

      protected void init_default_properties( )
      {
         lblContagemresultadoincidentestitle_Internalname = "CONTAGEMRESULTADOINCIDENTESTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavIncidentes_data1_Internalname = "vINCIDENTES_DATA1";
         lblDynamicfiltersincidentes_data_rangemiddletext1_Internalname = "DYNAMICFILTERSINCIDENTES_DATA_RANGEMIDDLETEXT1";
         edtavIncidentes_data_to1_Internalname = "vINCIDENTES_DATA_TO1";
         tblTablemergeddynamicfiltersincidentes_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1";
         edtavIncidentes_demandanum1_Internalname = "vINCIDENTES_DEMANDANUM1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavIncidentes_data2_Internalname = "vINCIDENTES_DATA2";
         lblDynamicfiltersincidentes_data_rangemiddletext2_Internalname = "DYNAMICFILTERSINCIDENTES_DATA_RANGEMIDDLETEXT2";
         edtavIncidentes_data_to2_Internalname = "vINCIDENTES_DATA_TO2";
         tblTablemergeddynamicfiltersincidentes_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2";
         edtavIncidentes_demandanum2_Internalname = "vINCIDENTES_DEMANDANUM2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavIncidentes_data3_Internalname = "vINCIDENTES_DATA3";
         lblDynamicfiltersincidentes_data_rangemiddletext3_Internalname = "DYNAMICFILTERSINCIDENTES_DATA_RANGEMIDDLETEXT3";
         edtavIncidentes_data_to3_Internalname = "vINCIDENTES_DATA_TO3";
         tblTablemergeddynamicfiltersincidentes_data3_Internalname = "TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3";
         edtavIncidentes_demandanum3_Internalname = "vINCIDENTES_DEMANDANUM3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtIncidentes_Codigo_Internalname = "INCIDENTES_CODIGO";
         edtIncidentes_Data_Internalname = "INCIDENTES_DATA";
         edtIncidentes_DemandaCod_Internalname = "INCIDENTES_DEMANDACOD";
         edtIncidentes_DemandaNum_Internalname = "INCIDENTES_DEMANDANUM";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfincidentes_data_Internalname = "vTFINCIDENTES_DATA";
         edtavTfincidentes_data_to_Internalname = "vTFINCIDENTES_DATA_TO";
         edtavDdo_incidentes_dataauxdate_Internalname = "vDDO_INCIDENTES_DATAAUXDATE";
         edtavDdo_incidentes_dataauxdateto_Internalname = "vDDO_INCIDENTES_DATAAUXDATETO";
         divDdo_incidentes_dataauxdates_Internalname = "DDO_INCIDENTES_DATAAUXDATES";
         edtavTfincidentes_demandanum_Internalname = "vTFINCIDENTES_DEMANDANUM";
         edtavTfincidentes_demandanum_sel_Internalname = "vTFINCIDENTES_DEMANDANUM_SEL";
         Ddo_incidentes_data_Internalname = "DDO_INCIDENTES_DATA";
         edtavDdo_incidentes_datatitlecontrolidtoreplace_Internalname = "vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE";
         Ddo_incidentes_demandanum_Internalname = "DDO_INCIDENTES_DEMANDANUM";
         edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Internalname = "vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtIncidentes_DemandaNum_Jsonclick = "";
         edtIncidentes_DemandaCod_Jsonclick = "";
         edtIncidentes_Data_Jsonclick = "";
         edtIncidentes_Codigo_Jsonclick = "";
         edtavIncidentes_data_to1_Jsonclick = "";
         edtavIncidentes_data1_Jsonclick = "";
         edtavIncidentes_data_to2_Jsonclick = "";
         edtavIncidentes_data2_Jsonclick = "";
         edtavIncidentes_data_to3_Jsonclick = "";
         edtavIncidentes_data3_Jsonclick = "";
         edtavIncidentes_demandanum3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavIncidentes_demandanum2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavIncidentes_demandanum1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtIncidentes_Data_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtIncidentes_DemandaNum_Titleformat = 0;
         edtIncidentes_Data_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavIncidentes_demandanum3_Visible = 1;
         tblTablemergeddynamicfiltersincidentes_data3_Visible = 1;
         edtavIncidentes_demandanum2_Visible = 1;
         tblTablemergeddynamicfiltersincidentes_data2_Visible = 1;
         edtavIncidentes_demandanum1_Visible = 1;
         tblTablemergeddynamicfiltersincidentes_data1_Visible = 1;
         edtIncidentes_DemandaNum_Title = "Demanda";
         edtIncidentes_Data_Title = "Data";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_incidentes_datatitlecontrolidtoreplace_Visible = 1;
         edtavTfincidentes_demandanum_sel_Jsonclick = "";
         edtavTfincidentes_demandanum_sel_Visible = 1;
         edtavTfincidentes_demandanum_Jsonclick = "";
         edtavTfincidentes_demandanum_Visible = 1;
         edtavDdo_incidentes_dataauxdateto_Jsonclick = "";
         edtavDdo_incidentes_dataauxdate_Jsonclick = "";
         edtavTfincidentes_data_to_Jsonclick = "";
         edtavTfincidentes_data_to_Visible = 1;
         edtavTfincidentes_data_Jsonclick = "";
         edtavTfincidentes_data_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_incidentes_demandanum_Searchbuttontext = "Pesquisar";
         Ddo_incidentes_demandanum_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_incidentes_demandanum_Cleanfilter = "Limpar pesquisa";
         Ddo_incidentes_demandanum_Loadingdata = "Carregando dados...";
         Ddo_incidentes_demandanum_Sortdsc = "Ordenar de Z � A";
         Ddo_incidentes_demandanum_Sortasc = "Ordenar de A � Z";
         Ddo_incidentes_demandanum_Datalistupdateminimumcharacters = 0;
         Ddo_incidentes_demandanum_Datalistproc = "GetWWContagemResultadoIncidentesFilterData";
         Ddo_incidentes_demandanum_Datalisttype = "Dynamic";
         Ddo_incidentes_demandanum_Includedatalist = Convert.ToBoolean( -1);
         Ddo_incidentes_demandanum_Filterisrange = Convert.ToBoolean( 0);
         Ddo_incidentes_demandanum_Filtertype = "Character";
         Ddo_incidentes_demandanum_Includefilter = Convert.ToBoolean( -1);
         Ddo_incidentes_demandanum_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_incidentes_demandanum_Includesortasc = Convert.ToBoolean( -1);
         Ddo_incidentes_demandanum_Titlecontrolidtoreplace = "";
         Ddo_incidentes_demandanum_Dropdownoptionstype = "GridTitleSettings";
         Ddo_incidentes_demandanum_Cls = "ColumnSettings";
         Ddo_incidentes_demandanum_Tooltip = "Op��es";
         Ddo_incidentes_demandanum_Caption = "";
         Ddo_incidentes_data_Searchbuttontext = "Pesquisar";
         Ddo_incidentes_data_Rangefilterto = "At�";
         Ddo_incidentes_data_Rangefilterfrom = "Desde";
         Ddo_incidentes_data_Cleanfilter = "Limpar pesquisa";
         Ddo_incidentes_data_Sortdsc = "Ordenar de Z � A";
         Ddo_incidentes_data_Sortasc = "Ordenar de A � Z";
         Ddo_incidentes_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_incidentes_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_incidentes_data_Filtertype = "Date";
         Ddo_incidentes_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_incidentes_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_incidentes_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_incidentes_data_Titlecontrolidtoreplace = "";
         Ddo_incidentes_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_incidentes_data_Cls = "ColumnSettings";
         Ddo_incidentes_data_Tooltip = "Op��es";
         Ddo_incidentes_data_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contagem Resultado Incidentes";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV36Incidentes_DataTitleFilterData',fld:'vINCIDENTES_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV42Incidentes_DemandaNumTitleFilterData',fld:'vINCIDENTES_DEMANDANUMTITLEFILTERDATA',pic:'',nv:null},{av:'edtIncidentes_Data_Titleformat',ctrl:'INCIDENTES_DATA',prop:'Titleformat'},{av:'edtIncidentes_Data_Title',ctrl:'INCIDENTES_DATA',prop:'Title'},{av:'edtIncidentes_DemandaNum_Titleformat',ctrl:'INCIDENTES_DEMANDANUM',prop:'Titleformat'},{av:'edtIncidentes_DemandaNum_Title',ctrl:'INCIDENTES_DEMANDANUM',prop:'Title'},{av:'AV48GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV49GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11J42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_INCIDENTES_DATA.ONOPTIONCLICKED","{handler:'E12J42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_incidentes_data_Activeeventkey',ctrl:'DDO_INCIDENTES_DATA',prop:'ActiveEventKey'},{av:'Ddo_incidentes_data_Filteredtext_get',ctrl:'DDO_INCIDENTES_DATA',prop:'FilteredText_get'},{av:'Ddo_incidentes_data_Filteredtextto_get',ctrl:'DDO_INCIDENTES_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_incidentes_data_Sortedstatus',ctrl:'DDO_INCIDENTES_DATA',prop:'SortedStatus'},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_incidentes_demandanum_Sortedstatus',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_INCIDENTES_DEMANDANUM.ONOPTIONCLICKED","{handler:'E13J42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_incidentes_demandanum_Activeeventkey',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'ActiveEventKey'},{av:'Ddo_incidentes_demandanum_Filteredtext_get',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'FilteredText_get'},{av:'Ddo_incidentes_demandanum_Selectedvalue_get',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_incidentes_demandanum_Sortedstatus',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SortedStatus'},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'Ddo_incidentes_data_Sortedstatus',ctrl:'DDO_INCIDENTES_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E27J42',iparms:[{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtIncidentes_Data_Link',ctrl:'INCIDENTES_DATA',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14J42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20J42',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15J42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfiltersincidentes_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'edtavIncidentes_demandanum2_Visible',ctrl:'vINCIDENTES_DEMANDANUM2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'},{av:'edtavIncidentes_demandanum3_Visible',ctrl:'vINCIDENTES_DEMANDANUM3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'},{av:'edtavIncidentes_demandanum1_Visible',ctrl:'vINCIDENTES_DEMANDANUM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21J42',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersincidentes_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'},{av:'edtavIncidentes_demandanum1_Visible',ctrl:'vINCIDENTES_DEMANDANUM1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22J42',iparms:[],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16J42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfiltersincidentes_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'edtavIncidentes_demandanum2_Visible',ctrl:'vINCIDENTES_DEMANDANUM2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'},{av:'edtavIncidentes_demandanum3_Visible',ctrl:'vINCIDENTES_DEMANDANUM3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'},{av:'edtavIncidentes_demandanum1_Visible',ctrl:'vINCIDENTES_DEMANDANUM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23J42',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersincidentes_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'edtavIncidentes_demandanum2_Visible',ctrl:'vINCIDENTES_DEMANDANUM2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E17J42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfiltersincidentes_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'edtavIncidentes_demandanum2_Visible',ctrl:'vINCIDENTES_DEMANDANUM2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'},{av:'edtavIncidentes_demandanum3_Visible',ctrl:'vINCIDENTES_DEMANDANUM3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'},{av:'edtavIncidentes_demandanum1_Visible',ctrl:'vINCIDENTES_DEMANDANUM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E24J42',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersincidentes_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'},{av:'edtavIncidentes_demandanum3_Visible',ctrl:'vINCIDENTES_DEMANDANUM3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E18J42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_incidentes_data_Filteredtext_set',ctrl:'DDO_INCIDENTES_DATA',prop:'FilteredText_set'},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_incidentes_data_Filteredtextto_set',ctrl:'DDO_INCIDENTES_DATA',prop:'FilteredTextTo_set'},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'Ddo_incidentes_demandanum_Filteredtext_set',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'FilteredText_set'},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'Ddo_incidentes_demandanum_Selectedvalue_set',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfiltersincidentes_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'},{av:'edtavIncidentes_demandanum1_Visible',ctrl:'vINCIDENTES_DEMANDANUM1',prop:'Visible'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfiltersincidentes_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'edtavIncidentes_demandanum2_Visible',ctrl:'vINCIDENTES_DEMANDANUM2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'},{av:'edtavIncidentes_demandanum3_Visible',ctrl:'vINCIDENTES_DEMANDANUM3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E19J42',iparms:[{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_incidentes_data_Activeeventkey = "";
         Ddo_incidentes_data_Filteredtext_get = "";
         Ddo_incidentes_data_Filteredtextto_get = "";
         Ddo_incidentes_demandanum_Activeeventkey = "";
         Ddo_incidentes_demandanum_Filteredtext_get = "";
         Ddo_incidentes_demandanum_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16Incidentes_Data1 = (DateTime)(DateTime.MinValue);
         AV17Incidentes_Data_To1 = (DateTime)(DateTime.MinValue);
         AV18Incidentes_DemandaNum1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV21Incidentes_Data2 = (DateTime)(DateTime.MinValue);
         AV22Incidentes_Data_To2 = (DateTime)(DateTime.MinValue);
         AV23Incidentes_DemandaNum2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV26Incidentes_Data3 = (DateTime)(DateTime.MinValue);
         AV27Incidentes_Data_To3 = (DateTime)(DateTime.MinValue);
         AV28Incidentes_DemandaNum3 = "";
         AV37TFIncidentes_Data = (DateTime)(DateTime.MinValue);
         AV38TFIncidentes_Data_To = (DateTime)(DateTime.MinValue);
         AV43TFIncidentes_DemandaNum = "";
         AV44TFIncidentes_DemandaNum_Sel = "";
         AV41ddo_Incidentes_DataTitleControlIdToReplace = "";
         AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace = "";
         AV72Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV46DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV36Incidentes_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42Incidentes_DemandaNumTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_incidentes_data_Filteredtext_set = "";
         Ddo_incidentes_data_Filteredtextto_set = "";
         Ddo_incidentes_data_Sortedstatus = "";
         Ddo_incidentes_demandanum_Filteredtext_set = "";
         Ddo_incidentes_demandanum_Selectedvalue_set = "";
         Ddo_incidentes_demandanum_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV39DDO_Incidentes_DataAuxDate = DateTime.MinValue;
         AV40DDO_Incidentes_DataAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Update = "";
         AV70Update_GXI = "";
         AV32Delete = "";
         AV71Delete_GXI = "";
         A1242Incidentes_Data = (DateTime)(DateTime.MinValue);
         A1240Incidentes_DemandaNum = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = "";
         lV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = "";
         lV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = "";
         lV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = "";
         AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 = "";
         AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1 = (DateTime)(DateTime.MinValue);
         AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 = (DateTime)(DateTime.MinValue);
         AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = "";
         AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 = "";
         AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2 = (DateTime)(DateTime.MinValue);
         AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 = (DateTime)(DateTime.MinValue);
         AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = "";
         AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 = "";
         AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3 = (DateTime)(DateTime.MinValue);
         AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 = (DateTime)(DateTime.MinValue);
         AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = "";
         AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data = (DateTime)(DateTime.MinValue);
         AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to = (DateTime)(DateTime.MinValue);
         AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel = "";
         AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = "";
         H00J42_A1240Incidentes_DemandaNum = new String[] {""} ;
         H00J42_n1240Incidentes_DemandaNum = new bool[] {false} ;
         H00J42_A1239Incidentes_DemandaCod = new int[1] ;
         H00J42_A1242Incidentes_Data = new DateTime[] {DateTime.MinValue} ;
         H00J42_A1241Incidentes_Codigo = new int[1] ;
         H00J43_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV33Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContagemresultadoincidentestitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfiltersincidentes_data_rangemiddletext3_Jsonclick = "";
         lblDynamicfiltersincidentes_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfiltersincidentes_data_rangemiddletext1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontagemresultadoincidentes__default(),
            new Object[][] {
                new Object[] {
               H00J42_A1240Incidentes_DemandaNum, H00J42_n1240Incidentes_DemandaNum, H00J42_A1239Incidentes_DemandaCod, H00J42_A1242Incidentes_Data, H00J42_A1241Incidentes_Codigo
               }
               , new Object[] {
               H00J43_AGRID_nRecordCount
               }
            }
         );
         AV72Pgmname = "WWContagemResultadoIncidentes";
         /* GeneXus formulas. */
         AV72Pgmname = "WWContagemResultadoIncidentes";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_97 ;
      private short nGXsfl_97_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_97_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtIncidentes_Data_Titleformat ;
      private short edtIncidentes_DemandaNum_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A1241Incidentes_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_incidentes_demandanum_Datalistupdateminimumcharacters ;
      private int edtavTfincidentes_data_Visible ;
      private int edtavTfincidentes_data_to_Visible ;
      private int edtavTfincidentes_demandanum_Visible ;
      private int edtavTfincidentes_demandanum_sel_Visible ;
      private int edtavDdo_incidentes_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Visible ;
      private int A1239Incidentes_DemandaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV47PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfiltersincidentes_data1_Visible ;
      private int edtavIncidentes_demandanum1_Visible ;
      private int tblTablemergeddynamicfiltersincidentes_data2_Visible ;
      private int edtavIncidentes_demandanum2_Visible ;
      private int tblTablemergeddynamicfiltersincidentes_data3_Visible ;
      private int edtavIncidentes_demandanum3_Visible ;
      private int AV73GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV48GridCurrentPage ;
      private long AV49GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_incidentes_data_Activeeventkey ;
      private String Ddo_incidentes_data_Filteredtext_get ;
      private String Ddo_incidentes_data_Filteredtextto_get ;
      private String Ddo_incidentes_demandanum_Activeeventkey ;
      private String Ddo_incidentes_demandanum_Filteredtext_get ;
      private String Ddo_incidentes_demandanum_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_97_idx="0001" ;
      private String AV72Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_incidentes_data_Caption ;
      private String Ddo_incidentes_data_Tooltip ;
      private String Ddo_incidentes_data_Cls ;
      private String Ddo_incidentes_data_Filteredtext_set ;
      private String Ddo_incidentes_data_Filteredtextto_set ;
      private String Ddo_incidentes_data_Dropdownoptionstype ;
      private String Ddo_incidentes_data_Titlecontrolidtoreplace ;
      private String Ddo_incidentes_data_Sortedstatus ;
      private String Ddo_incidentes_data_Filtertype ;
      private String Ddo_incidentes_data_Sortasc ;
      private String Ddo_incidentes_data_Sortdsc ;
      private String Ddo_incidentes_data_Cleanfilter ;
      private String Ddo_incidentes_data_Rangefilterfrom ;
      private String Ddo_incidentes_data_Rangefilterto ;
      private String Ddo_incidentes_data_Searchbuttontext ;
      private String Ddo_incidentes_demandanum_Caption ;
      private String Ddo_incidentes_demandanum_Tooltip ;
      private String Ddo_incidentes_demandanum_Cls ;
      private String Ddo_incidentes_demandanum_Filteredtext_set ;
      private String Ddo_incidentes_demandanum_Selectedvalue_set ;
      private String Ddo_incidentes_demandanum_Dropdownoptionstype ;
      private String Ddo_incidentes_demandanum_Titlecontrolidtoreplace ;
      private String Ddo_incidentes_demandanum_Sortedstatus ;
      private String Ddo_incidentes_demandanum_Filtertype ;
      private String Ddo_incidentes_demandanum_Datalisttype ;
      private String Ddo_incidentes_demandanum_Datalistproc ;
      private String Ddo_incidentes_demandanum_Sortasc ;
      private String Ddo_incidentes_demandanum_Sortdsc ;
      private String Ddo_incidentes_demandanum_Loadingdata ;
      private String Ddo_incidentes_demandanum_Cleanfilter ;
      private String Ddo_incidentes_demandanum_Noresultsfound ;
      private String Ddo_incidentes_demandanum_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfincidentes_data_Internalname ;
      private String edtavTfincidentes_data_Jsonclick ;
      private String edtavTfincidentes_data_to_Internalname ;
      private String edtavTfincidentes_data_to_Jsonclick ;
      private String divDdo_incidentes_dataauxdates_Internalname ;
      private String edtavDdo_incidentes_dataauxdate_Internalname ;
      private String edtavDdo_incidentes_dataauxdate_Jsonclick ;
      private String edtavDdo_incidentes_dataauxdateto_Internalname ;
      private String edtavDdo_incidentes_dataauxdateto_Jsonclick ;
      private String edtavTfincidentes_demandanum_Internalname ;
      private String edtavTfincidentes_demandanum_Jsonclick ;
      private String edtavTfincidentes_demandanum_sel_Internalname ;
      private String edtavTfincidentes_demandanum_sel_Jsonclick ;
      private String edtavDdo_incidentes_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtIncidentes_Codigo_Internalname ;
      private String edtIncidentes_Data_Internalname ;
      private String edtIncidentes_DemandaCod_Internalname ;
      private String edtIncidentes_DemandaNum_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavIncidentes_data1_Internalname ;
      private String edtavIncidentes_data_to1_Internalname ;
      private String edtavIncidentes_demandanum1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavIncidentes_data2_Internalname ;
      private String edtavIncidentes_data_to2_Internalname ;
      private String edtavIncidentes_demandanum2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavIncidentes_data3_Internalname ;
      private String edtavIncidentes_data_to3_Internalname ;
      private String edtavIncidentes_demandanum3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_incidentes_data_Internalname ;
      private String Ddo_incidentes_demandanum_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtIncidentes_Data_Title ;
      private String edtIncidentes_DemandaNum_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtIncidentes_Data_Link ;
      private String tblTablemergeddynamicfiltersincidentes_data1_Internalname ;
      private String tblTablemergeddynamicfiltersincidentes_data2_Internalname ;
      private String tblTablemergeddynamicfiltersincidentes_data3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContagemresultadoincidentestitle_Internalname ;
      private String lblContagemresultadoincidentestitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavIncidentes_demandanum1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavIncidentes_demandanum2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavIncidentes_demandanum3_Jsonclick ;
      private String edtavIncidentes_data3_Jsonclick ;
      private String lblDynamicfiltersincidentes_data_rangemiddletext3_Internalname ;
      private String lblDynamicfiltersincidentes_data_rangemiddletext3_Jsonclick ;
      private String edtavIncidentes_data_to3_Jsonclick ;
      private String edtavIncidentes_data2_Jsonclick ;
      private String lblDynamicfiltersincidentes_data_rangemiddletext2_Internalname ;
      private String lblDynamicfiltersincidentes_data_rangemiddletext2_Jsonclick ;
      private String edtavIncidentes_data_to2_Jsonclick ;
      private String edtavIncidentes_data1_Jsonclick ;
      private String lblDynamicfiltersincidentes_data_rangemiddletext1_Internalname ;
      private String lblDynamicfiltersincidentes_data_rangemiddletext1_Jsonclick ;
      private String edtavIncidentes_data_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_97_fel_idx="0001" ;
      private String ROClassString ;
      private String edtIncidentes_Codigo_Jsonclick ;
      private String edtIncidentes_Data_Jsonclick ;
      private String edtIncidentes_DemandaCod_Jsonclick ;
      private String edtIncidentes_DemandaNum_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV16Incidentes_Data1 ;
      private DateTime AV17Incidentes_Data_To1 ;
      private DateTime AV21Incidentes_Data2 ;
      private DateTime AV22Incidentes_Data_To2 ;
      private DateTime AV26Incidentes_Data3 ;
      private DateTime AV27Incidentes_Data_To3 ;
      private DateTime AV37TFIncidentes_Data ;
      private DateTime AV38TFIncidentes_Data_To ;
      private DateTime A1242Incidentes_Data ;
      private DateTime AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1 ;
      private DateTime AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 ;
      private DateTime AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2 ;
      private DateTime AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 ;
      private DateTime AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3 ;
      private DateTime AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 ;
      private DateTime AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data ;
      private DateTime AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to ;
      private DateTime AV39DDO_Incidentes_DataAuxDate ;
      private DateTime AV40DDO_Incidentes_DataAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_incidentes_data_Includesortasc ;
      private bool Ddo_incidentes_data_Includesortdsc ;
      private bool Ddo_incidentes_data_Includefilter ;
      private bool Ddo_incidentes_data_Filterisrange ;
      private bool Ddo_incidentes_data_Includedatalist ;
      private bool Ddo_incidentes_demandanum_Includesortasc ;
      private bool Ddo_incidentes_demandanum_Includesortdsc ;
      private bool Ddo_incidentes_demandanum_Includefilter ;
      private bool Ddo_incidentes_demandanum_Filterisrange ;
      private bool Ddo_incidentes_demandanum_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1240Incidentes_DemandaNum ;
      private bool AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 ;
      private bool AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV18Incidentes_DemandaNum1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV23Incidentes_DemandaNum2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV28Incidentes_DemandaNum3 ;
      private String AV43TFIncidentes_DemandaNum ;
      private String AV44TFIncidentes_DemandaNum_Sel ;
      private String AV41ddo_Incidentes_DataTitleControlIdToReplace ;
      private String AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace ;
      private String AV70Update_GXI ;
      private String AV71Delete_GXI ;
      private String A1240Incidentes_DemandaNum ;
      private String lV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 ;
      private String lV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 ;
      private String lV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 ;
      private String lV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum ;
      private String AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 ;
      private String AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 ;
      private String AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 ;
      private String AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 ;
      private String AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 ;
      private String AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 ;
      private String AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel ;
      private String AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum ;
      private String AV31Update ;
      private String AV32Delete ;
      private IGxSession AV33Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00J42_A1240Incidentes_DemandaNum ;
      private bool[] H00J42_n1240Incidentes_DemandaNum ;
      private int[] H00J42_A1239Incidentes_DemandaCod ;
      private DateTime[] H00J42_A1242Incidentes_Data ;
      private int[] H00J42_A1241Incidentes_Codigo ;
      private long[] H00J43_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36Incidentes_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42Incidentes_DemandaNumTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV46DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontagemresultadoincidentes__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00J42( IGxContext context ,
                                             String AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1 ,
                                             DateTime AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 ,
                                             String AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 ,
                                             bool AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 ,
                                             String AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 ,
                                             DateTime AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2 ,
                                             DateTime AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 ,
                                             String AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 ,
                                             bool AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 ,
                                             String AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 ,
                                             DateTime AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3 ,
                                             DateTime AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 ,
                                             String AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 ,
                                             DateTime AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data ,
                                             DateTime AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to ,
                                             String AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel ,
                                             String AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum ,
                                             DateTime A1242Incidentes_Data ,
                                             String A1240Incidentes_DemandaNum ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [18] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[ContagemResultado_Demanda] AS Incidentes_DemandaNum, T1.[Incidentes_DemandaCod] AS Incidentes_DemandaCod, T1.[Incidentes_Data], T1.[Incidentes_Codigo]";
         sFromString = " FROM ([ContagemResultadoIncidentes] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[Incidentes_DemandaCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1, "INCIDENTES_DEMANDANUM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 + '%')";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2, "INCIDENTES_DEMANDANUM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 + '%')";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3, "INCIDENTES_DEMANDANUM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 + '%')";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] = @AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Incidentes_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Incidentes_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ContagemResultado_Demanda]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ContagemResultado_Demanda] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Incidentes_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00J43( IGxContext context ,
                                             String AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1 ,
                                             DateTime AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 ,
                                             String AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 ,
                                             bool AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 ,
                                             String AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 ,
                                             DateTime AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2 ,
                                             DateTime AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 ,
                                             String AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 ,
                                             bool AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 ,
                                             String AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 ,
                                             DateTime AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3 ,
                                             DateTime AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 ,
                                             String AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 ,
                                             DateTime AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data ,
                                             DateTime AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to ,
                                             String AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel ,
                                             String AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum ,
                                             DateTime A1242Incidentes_Data ,
                                             String A1240Incidentes_DemandaNum ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [13] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContagemResultadoIncidentes] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[Incidentes_DemandaCod])";
         if ( ( StringUtil.StrCmp(AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1, "INCIDENTES_DEMANDANUM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 + '%')";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV56WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2, "INCIDENTES_DEMANDANUM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 + '%')";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV61WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3, "INCIDENTES_DEMANDANUM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 + '%')";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] = @AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00J42(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
               case 1 :
                     return conditional_H00J43(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00J42 ;
          prmH00J42 = new Object[] {
          new Object[] {"@AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00J43 ;
          prmH00J43 = new Object[] {
          new Object[] {"@AV53WWContagemResultadoIncidentesDS_2_Incidentes_data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV54WWContagemResultadoIncidentesDS_3_Incidentes_data_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV55WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV58WWContagemResultadoIncidentesDS_7_Incidentes_data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV59WWContagemResultadoIncidentesDS_8_Incidentes_data_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV60WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV63WWContagemResultadoIncidentesDS_12_Incidentes_data3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV64WWContagemResultadoIncidentesDS_13_Incidentes_data_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV65WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV66WWContagemResultadoIncidentesDS_15_Tfincidentes_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV67WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV68WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV69WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel",SqlDbType.VarChar,30,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00J42", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00J42,11,0,true,false )
             ,new CursorDef("H00J43", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00J43,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
       }
    }

 }

}
