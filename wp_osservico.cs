/*
               File: WP_OSServico
        Description: Servi�o Vinculado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:21:30.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_osservico : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_osservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_osservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           ref bool aP1_Confirmado )
      {
         this.AV5ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV32Confirmado = aP1_Confirmado;
         executePrivate();
         aP1_Confirmado=this.AV32Confirmado;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContratadaorigem_codigo = new GXCombobox();
         dynavContagemresultado_contratadacod = new GXCombobox();
         dynavContratada_codigo = new GXCombobox();
         dynavContagemresultado_contadorfmcod = new GXCombobox();
         dynavContagemresultado_servico = new GXCombobox();
         dynavContagemresultado_naocnfdmncod = new GXCombobox();
         dynavContagemresultado_naocnfcntcod = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADAORIGEM_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV20WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADAORIGEM_CODIGODX2( AV20WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV20WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTRATADACODDX2( AV20WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV20WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGODX2( AV20WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTADORFMCOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV20WWPContext);
               AV17Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTADORFMCODDX2( AV20WWPContext, AV17Contratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV17Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SERVICODX2( AV17Contratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV20WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_NAOCNFDMNCODDX2( AV20WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_NAOCNFCNTCOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV20WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_NAOCNFCNTCODDX2( AV20WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV5ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV32Confirmado = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Confirmado", AV32Confirmado);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PADX2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTDX2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216213018");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_osservico.aspx") + "?" + UrlEncode("" +AV5ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV32Confirmado))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_OBSERVACAO", AV26ContagemResultado_Observacao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A684ContagemResultado_PFBFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFLFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A685ContagemResultado_PFLFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A489ContagemResultado_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vRETORNO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23Retorno), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vQUEMCADASTROU", StringUtil.RTrim( AV22QuemCadastrou));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_DEMANDA", AV28Sdt_Demanda);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_DEMANDA", AV28Sdt_Demanda);
         }
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33Sistema_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_DEMANDAS", AV27Sdt_Demandas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_DEMANDAS", AV27Sdt_Demandas);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vCONFIRMADO", AV32Confirmado);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV20WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV20WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OBSERVACAO_Enabled", StringUtil.BoolToStr( Contagemresultado_observacao_Enabled));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_SERVICO_Text", StringUtil.RTrim( dynavContagemresultado_servico.Description));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEDX2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTDX2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_osservico.aspx") + "?" + UrlEncode("" +AV5ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV32Confirmado)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_OSServico" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servi�o Vinculado" ;
      }

      protected void WBDX0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_DX2( true) ;
         }
         else
         {
            wb_table1_2_DX2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DX2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTDX2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Servi�o Vinculado", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPDX0( ) ;
      }

      protected void WSDX2( )
      {
         STARTDX2( ) ;
         EVTDX2( ) ;
      }

      protected void EVTDX2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11DX2 */
                              E11DX2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_CONTRATADACOD.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12DX2 */
                              E12DX2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_SERVICO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13DX2 */
                              E13DX2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VNEWCONTAGEMRESULTADO_DEMANDA.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14DX2 */
                              E14DX2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E15DX2 */
                                    E15DX2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16DX2 */
                              E16DX2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDX2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PADX2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContratadaorigem_codigo.Name = "vCONTRATADAORIGEM_CODIGO";
            dynavContratadaorigem_codigo.WebTags = "";
            dynavContagemresultado_contratadacod.Name = "vCONTAGEMRESULTADO_CONTRATADACOD";
            dynavContagemresultado_contratadacod.WebTags = "";
            dynavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            dynavContratada_codigo.WebTags = "";
            dynavContagemresultado_contadorfmcod.Name = "vCONTAGEMRESULTADO_CONTADORFMCOD";
            dynavContagemresultado_contadorfmcod.WebTags = "";
            dynavContagemresultado_servico.Name = "vCONTAGEMRESULTADO_SERVICO";
            dynavContagemresultado_servico.WebTags = "";
            dynavContagemresultado_naocnfdmncod.Name = "vCONTAGEMRESULTADO_NAOCNFDMNCOD";
            dynavContagemresultado_naocnfdmncod.WebTags = "";
            dynavContagemresultado_naocnfcntcod.Name = "vCONTAGEMRESULTADO_NAOCNFCNTCOD";
            dynavContagemresultado_naocnfcntcod.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavContratadaorigem_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADAORIGEM_CODIGODX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADAORIGEM_CODIGO_dataDX2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADAORIGEM_CODIGO_htmlDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADAORIGEM_CODIGO_dataDX2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratadaorigem_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratadaorigem_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratadaorigem_codigo.ItemCount > 0 )
         {
            AV31ContratadaOrigem_Codigo = (int)(NumberUtil.Val( dynavContratadaorigem_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31ContratadaOrigem_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContratadaOrigem_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADAORIGEM_CODIGO_dataDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00DX2 */
         pr_default.execute(0, new Object[] {AV20WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00DX2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00DX2_A41Contratada_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACODDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataDX2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataDX2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contratadacod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contratadacod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contratadacod.ItemCount > 0 )
         {
            AV6ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H00DX3 */
         pr_default.execute(1, new Object[] {AV20WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00DX3_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00DX3_A41Contratada_PessoaNom[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvCONTRATADA_CODIGODX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO_dataDX2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO_htmlDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO_dataDX2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV17Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO_dataDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00DX4 */
         pr_default.execute(2, new Object[] {AV20WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00DX4_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00DX4_A41Contratada_PessoaNom[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFMCODDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext ,
                                                               int AV17Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTADORFMCOD_dataDX2( AV20WWPContext, AV17Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext ,
                                                                  int AV17Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTADORFMCOD_dataDX2( AV20WWPContext, AV17Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contadorfmcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contadorfmcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contadorfmcod.ItemCount > 0 )
         {
            AV21ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFMCOD_dataDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext ,
                                                                    int AV17Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00DX5 */
         pr_default.execute(3, new Object[] {AV17Contratada_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00DX5_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00DX5_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICODX2( int AV17Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SERVICO_dataDX2( AV17Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SERVICO_htmlDX2( int AV17Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SERVICO_dataDX2( AV17Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_servico.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_servico.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV15ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICO_dataDX2( int AV17Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00DX6 */
         pr_default.execute(4, new Object[] {AV17Contratada_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Servico_Codigo), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00DX6_A605Servico_Sigla[0]));
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void GXDLVvCONTAGEMRESULTADO_NAOCNFDMNCODDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_NAOCNFDMNCOD_dataDX2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_NAOCNFDMNCOD_htmlDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_NAOCNFDMNCOD_dataDX2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_naocnfdmncod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_naocnfdmncod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_naocnfdmncod.ItemCount > 0 )
         {
            AV24ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( dynavContagemresultado_naocnfdmncod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContagemResultado_NaoCnfDmnCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ContagemResultado_NaoCnfDmnCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_NAOCNFDMNCOD_dataDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00DX7 */
         pr_default.execute(5, new Object[] {AV20WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00DX7_A426NaoConformidade_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00DX7_A427NaoConformidade_Nome[0]));
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void GXDLVvCONTAGEMRESULTADO_NAOCNFCNTCODDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_NAOCNFCNTCOD_dataDX2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_NAOCNFCNTCOD_htmlDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_NAOCNFCNTCOD_dataDX2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_naocnfcntcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_naocnfcntcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_naocnfcntcod.ItemCount > 0 )
         {
            AV25ContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( dynavContagemresultado_naocnfcntcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25ContagemResultado_NaoCnfCntCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContagemResultado_NaoCnfCntCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_NAOCNFCNTCOD_dataDX2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00DX8 */
         pr_default.execute(6, new Object[] {AV20WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00DX8_A426NaoConformidade_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00DX8_A427NaoConformidade_Nome[0]));
            pr_default.readNext(6);
         }
         pr_default.close(6);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContratadaorigem_codigo.ItemCount > 0 )
         {
            AV31ContratadaOrigem_Codigo = (int)(NumberUtil.Val( dynavContratadaorigem_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31ContratadaOrigem_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContratadaOrigem_Codigo), 6, 0)));
         }
         if ( dynavContagemresultado_contratadacod.ItemCount > 0 )
         {
            AV6ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0)));
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV17Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
         }
         if ( dynavContagemresultado_contadorfmcod.ItemCount > 0 )
         {
            AV21ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0)));
         }
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV15ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0)));
         }
         if ( dynavContagemresultado_naocnfdmncod.ItemCount > 0 )
         {
            AV24ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( dynavContagemresultado_naocnfdmncod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContagemResultado_NaoCnfDmnCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ContagemResultado_NaoCnfDmnCod), 6, 0)));
         }
         if ( dynavContagemresultado_naocnfcntcod.ItemCount > 0 )
         {
            AV25ContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( dynavContagemresultado_naocnfcntcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25ContagemResultado_NaoCnfCntCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContagemResultado_NaoCnfCntCod), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDX2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         dynavContratadaorigem_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratadaorigem_codigo.Enabled), 5, 0)));
         dynavContagemresultado_contratadacod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contratadacod.Enabled), 5, 0)));
         edtavContagemresultado_demanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda_Enabled), 5, 0)));
         edtavContagemresultado_pfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfs_Enabled), 5, 0)));
         edtavContagemresultado_pflfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pflfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pflfs_Enabled), 5, 0)));
      }

      protected void RFDX2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E16DX2 */
            E16DX2 ();
            WBDX0( ) ;
         }
      }

      protected void STRUPDX0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         dynavContratadaorigem_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratadaorigem_codigo.Enabled), 5, 0)));
         dynavContagemresultado_contratadacod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contratadacod.Enabled), 5, 0)));
         edtavContagemresultado_demanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda_Enabled), 5, 0)));
         edtavContagemresultado_pfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfs_Enabled), 5, 0)));
         edtavContagemresultado_pflfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pflfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pflfs_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11DX2 */
         E11DX2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTRATADAORIGEM_CODIGO_htmlDX2( AV20WWPContext) ;
         GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlDX2( AV20WWPContext) ;
         GXVvCONTRATADA_CODIGO_htmlDX2( AV20WWPContext) ;
         GXVvCONTAGEMRESULTADO_NAOCNFDMNCOD_htmlDX2( AV20WWPContext) ;
         GXVvCONTAGEMRESULTADO_NAOCNFCNTCOD_htmlDX2( AV20WWPContext) ;
         GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlDX2( AV20WWPContext, AV17Contratada_Codigo) ;
         GXVvCONTAGEMRESULTADO_SERVICO_htmlDX2( AV17Contratada_Codigo) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavContratadaorigem_codigo.CurrentValue = cgiGet( dynavContratadaorigem_codigo_Internalname);
            AV31ContratadaOrigem_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratadaorigem_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContratadaOrigem_Codigo), 6, 0)));
            dynavContagemresultado_contratadacod.CurrentValue = cgiGet( dynavContagemresultado_contratadacod_Internalname);
            AV6ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contratadacod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0)));
            AV8ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_Demanda", AV8ContagemResultado_Demanda);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfs_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFBFS");
               GX_FocusControl = edtavContagemresultado_pfbfs_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12ContagemResultado_PFBFS = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV12ContagemResultado_PFBFS, 14, 5)));
            }
            else
            {
               AV12ContagemResultado_PFBFS = context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfs_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV12ContagemResultado_PFBFS, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfs_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFLFS");
               GX_FocusControl = edtavContagemresultado_pflfs_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14ContagemResultado_PFLFS = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFLFS, 14, 5)));
            }
            else
            {
               AV14ContagemResultado_PFLFS = context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfs_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFLFS, 14, 5)));
            }
            dynavContratada_codigo.CurrentValue = cgiGet( dynavContratada_codigo_Internalname);
            AV17Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
            dynavContagemresultado_contadorfmcod.CurrentValue = cgiGet( dynavContagemresultado_contadorfmcod_Internalname);
            AV21ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contadorfmcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0)));
            dynavContagemresultado_servico.CurrentValue = cgiGet( dynavContagemresultado_servico_Internalname);
            AV15ContagemResultado_Servico = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_servico_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0)));
            AV19NewContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtavNewcontagemresultado_demanda_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NewContagemResultado_Demanda", AV19NewContagemResultado_Demanda);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datacnt_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data"}), 1, "vCONTAGEMRESULTADO_DATACNT");
               GX_FocusControl = edtavContagemresultado_datacnt_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7ContagemResultado_DataCnt = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_DataCnt", context.localUtil.Format(AV7ContagemResultado_DataCnt, "99/99/99"));
            }
            else
            {
               AV7ContagemResultado_DataCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datacnt_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_DataCnt", context.localUtil.Format(AV7ContagemResultado_DataCnt, "99/99/99"));
            }
            dynavContagemresultado_naocnfdmncod.CurrentValue = cgiGet( dynavContagemresultado_naocnfdmncod_Internalname);
            AV24ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_naocnfdmncod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ContagemResultado_NaoCnfDmnCod), 6, 0)));
            dynavContagemresultado_naocnfcntcod.CurrentValue = cgiGet( dynavContagemresultado_naocnfcntcod_Internalname);
            AV25ContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_naocnfcntcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContagemResultado_NaoCnfCntCod), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfm_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFBFM");
               GX_FocusControl = edtavContagemresultado_pfbfm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11ContagemResultado_PFBFM = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV11ContagemResultado_PFBFM, 14, 5)));
            }
            else
            {
               AV11ContagemResultado_PFBFM = context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfm_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV11ContagemResultado_PFBFM, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfm_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFLFM");
               GX_FocusControl = edtavContagemresultado_pflfm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13ContagemResultado_PFLFM = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV13ContagemResultado_PFLFM, 14, 5)));
            }
            else
            {
               AV13ContagemResultado_PFLFM = context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfm_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV13ContagemResultado_PFLFM, 14, 5)));
            }
            /* Read saved values. */
            AV26ContagemResultado_Observacao = cgiGet( "vCONTAGEMRESULTADO_OBSERVACAO");
            Contagemresultado_observacao_Enabled = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Enabled"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvCONTRATADAORIGEM_CODIGO_htmlDX2( AV20WWPContext) ;
            GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlDX2( AV20WWPContext) ;
            GXVvCONTRATADA_CODIGO_htmlDX2( AV20WWPContext) ;
            GXVvCONTAGEMRESULTADO_NAOCNFDMNCOD_htmlDX2( AV20WWPContext) ;
            GXVvCONTAGEMRESULTADO_NAOCNFCNTCOD_htmlDX2( AV20WWPContext) ;
            GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlDX2( AV20WWPContext, AV17Contratada_Codigo) ;
            GXVvCONTAGEMRESULTADO_SERVICO_htmlDX2( AV17Contratada_Codigo) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11DX2 */
         E11DX2 ();
         if (returnInSub) return;
      }

      protected void E11DX2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV20WWPContext) ;
         bttBtnenter_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
         AV17Contratada_Codigo = AV20WWPContext.gxTpr_Contratada_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
         AV21ContagemResultado_ContadorFMCod = AV20WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0)));
         AV7ContagemResultado_DataCnt = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_DataCnt", context.localUtil.Format(AV7ContagemResultado_DataCnt, "99/99/99"));
         dynavContratada_codigo.Enabled = ((AV20WWPContext.gxTpr_Contratada_codigo==0)||AV20WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratada_codigo.Enabled), 5, 0)));
         dynavContagemresultado_contadorfmcod.Enabled = ((AV20WWPContext.gxTpr_Contratada_codigo==0)||AV20WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfmcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contadorfmcod.Enabled), 5, 0)));
         edtavContagemresultado_datacnt_Enabled = ((AV20WWPContext.gxTpr_Contratada_codigo==0)||AV20WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_datacnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_datacnt_Enabled), 5, 0)));
         /* Execute user subroutine: 'BUSCADEMANDA' */
         S112 ();
         if (returnInSub) return;
      }

      protected void E12DX2( )
      {
         /* Contagemresultado_contratadacod_Click Routine */
         if ( ! (0==AV6ContagemResultado_ContratadaCod) )
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8ContagemResultado_Demanda)) )
            {
               /* Execute user subroutine: 'BUSCADEMANDA' */
               S112 ();
               if (returnInSub) return;
            }
            else
            {
               bttBtnenter_Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
               bttBtnenter_Tooltiptext = "Confirmar";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            }
         }
         dynavContagemresultado_contratadacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod_Internalname, "Values", dynavContagemresultado_contratadacod.ToJavascriptSource());
         dynavContratadaorigem_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31ContratadaOrigem_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_codigo_Internalname, "Values", dynavContratadaorigem_codigo.ToJavascriptSource());
      }

      protected void E13DX2( )
      {
         /* Contagemresultado_servico_Click Routine */
         if ( ! (0==AV15ContagemResultado_Servico) )
         {
            bttBtnenter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8ContagemResultado_Demanda)) )
            {
               AV19NewContagemResultado_Demanda = StringUtil.Trim( AV8ContagemResultado_Demanda) + StringUtil.Substring( dynavContagemresultado_servico.Description, 1, 1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NewContagemResultado_Demanda", AV19NewContagemResultado_Demanda);
            }
         }
      }

      protected void E14DX2( )
      {
         /* Newcontagemresultado_demanda_Isvalid Routine */
         GXt_int1 = AV23Retorno;
         if ( new prc_existedemanda(context).executeUdp(  AV19NewContagemResultado_Demanda,  0,  "",  AV20WWPContext.gxTpr_Areatrabalho_codigo,  false, ref  GXt_int1,  AV15ContagemResultado_Servico,  AV6ContagemResultado_ContratadaCod, out  AV22QuemCadastrou) )
         {
            GX_msglist.addItem("Nova OS j� cadastrada por "+StringUtil.Trim( StringUtil.Substring( AV22QuemCadastrou, StringUtil.StringSearch( AV22QuemCadastrou, ",", 1)+2, 100))+"!");
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Nova OS j� cadastrada!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
         }
         else
         {
            bttBtnenter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E15DX2 */
         E15DX2 ();
         if (returnInSub) return;
      }

      protected void E15DX2( )
      {
         /* Enter Routine */
         GXt_int1 = AV23Retorno;
         if ( new prc_existedemanda(context).executeUdp(  AV19NewContagemResultado_Demanda,  0,  "",  AV20WWPContext.gxTpr_Areatrabalho_codigo,  false, ref  GXt_int1,  AV15ContagemResultado_Servico,  AV6ContagemResultado_ContratadaCod, out  AV22QuemCadastrou) )
         {
            GX_msglist.addItem("Nova OS j� cadastrada por "+StringUtil.Trim( StringUtil.Substring( AV22QuemCadastrou, StringUtil.StringSearch( AV22QuemCadastrou, ",", 1)+2, 100))+"!");
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Nova OS j� cadastrada";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
         }
         else if ( (0==AV6ContagemResultado_ContratadaCod) )
         {
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Informe a Contratada que exwcutou a OS!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            GX_msglist.addItem("Informe a Contratada que exwcutou a OS!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV8ContagemResultado_Demanda)) )
         {
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Informe a OS vinculada!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            GX_msglist.addItem("Informe a OS vinculada!");
         }
         else if ( (0==AV15ContagemResultado_Servico) )
         {
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Informe o Servi�o executado!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            GX_msglist.addItem("Informe o Servi�o executado!");
         }
         else if ( (0==AV21ContagemResultado_ContadorFMCod) )
         {
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Informe o Respons�vel!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            GX_msglist.addItem("Informe o Respons�vel!");
         }
         else if ( (DateTime.MinValue==AV7ContagemResultado_DataCnt) )
         {
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Informe a data do Servi�o!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            GX_msglist.addItem("Informe a data do Servi�o!");
         }
         else
         {
            AV32Confirmado = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Confirmado", AV32Confirmado);
            /* Execute user subroutine: 'CARREGASDT' */
            S122 ();
            if (returnInSub) return;
            new prc_novaosvinculada(context ).execute( ref  AV5ContagemResultado_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Codigo), 6, 0)));
            context.setWebReturnParms(new Object[] {(bool)AV32Confirmado});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV28Sdt_Demanda", AV28Sdt_Demanda);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV27Sdt_Demandas", AV27Sdt_Demandas);
      }

      protected void S112( )
      {
         /* 'BUSCADEMANDA' Routine */
         /* Using cursor H00DX10 */
         pr_default.execute(7, new Object[] {AV5ContagemResultado_Codigo});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A456ContagemResultado_Codigo = H00DX10_A456ContagemResultado_Codigo[0];
            A490ContagemResultado_ContratadaCod = H00DX10_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00DX10_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = H00DX10_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = H00DX10_n805ContagemResultado_ContratadaOrigemCod[0];
            A457ContagemResultado_Demanda = H00DX10_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00DX10_n457ContagemResultado_Demanda[0];
            A489ContagemResultado_SistemaCod = H00DX10_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00DX10_n489ContagemResultado_SistemaCod[0];
            A684ContagemResultado_PFBFSUltima = H00DX10_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00DX10_A685ContagemResultado_PFLFSUltima[0];
            A684ContagemResultado_PFBFSUltima = H00DX10_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00DX10_A685ContagemResultado_PFLFSUltima[0];
            bttBtnenter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            AV6ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0)));
            AV31ContratadaOrigem_Codigo = A805ContagemResultado_ContratadaOrigemCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContratadaOrigem_Codigo), 6, 0)));
            AV8ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_Demanda", AV8ContagemResultado_Demanda);
            AV12ContagemResultado_PFBFS = A684ContagemResultado_PFBFSUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV12ContagemResultado_PFBFS, 14, 5)));
            AV14ContagemResultado_PFLFS = A685ContagemResultado_PFLFSUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFLFS, 14, 5)));
            AV33Sistema_Codigo = A489ContagemResultado_SistemaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Sistema_Codigo), 6, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(7);
      }

      protected void S122( )
      {
         /* 'CARREGASDT' Routine */
         AV28Sdt_Demanda.gxTpr_Contratada_codigo = AV17Contratada_Codigo;
         AV28Sdt_Demanda.gxTpr_Contadorfmcod = AV21ContagemResultado_ContadorFMCod;
         AV28Sdt_Demanda.gxTpr_Demanda = AV19NewContagemResultado_Demanda;
         AV28Sdt_Demanda.gxTpr_Observacao = AV26ContagemResultado_Observacao;
         AV28Sdt_Demanda.gxTpr_Pfbfm = AV11ContagemResultado_PFBFM;
         AV28Sdt_Demanda.gxTpr_Pflfm = AV13ContagemResultado_PFLFM;
         AV28Sdt_Demanda.gxTpr_Servico_codigo = AV15ContagemResultado_Servico;
         AV28Sdt_Demanda.gxTpr_Naoconformidadedmn = AV24ContagemResultado_NaoCnfDmnCod;
         AV28Sdt_Demanda.gxTpr_Naoconformidadecnt = AV25ContagemResultado_NaoCnfCntCod;
         AV28Sdt_Demanda.gxTpr_Datacnt = AV7ContagemResultado_DataCnt;
         AV28Sdt_Demanda.gxTpr_Sistemacod = AV33Sistema_Codigo;
         AV27Sdt_Demandas.Add(AV28Sdt_Demanda, 0);
         AV29WebSession.Set("SDT_Demandas", AV27Sdt_Demandas.ToXml(false, true, "SDT_Demandas", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void nextLoad( )
      {
      }

      protected void E16DX2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_DX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock14_Internalname, "Contratada Origem:", "", "", lblTextblock14_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratadaorigem_codigo, dynavContratadaorigem_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV31ContratadaOrigem_Codigo), 6, 0)), 1, dynavContratadaorigem_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavContratadaorigem_codigo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_WP_OSServico.htm");
            dynavContratadaorigem_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31ContratadaOrigem_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_codigo_Internalname, "Values", (String)(dynavContratadaorigem_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock12_Internalname, "Quem executou:", "", "", lblTextblock12_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contratadacod, dynavContagemresultado_contratadacod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0)), 1, dynavContagemresultado_contratadacod_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_CONTRATADACOD.CLICK."+"'", "int", "", 1, dynavContagemresultado_contratadacod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "", true, "HLP_WP_OSServico.htm");
            dynavContagemresultado_contratadacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod_Internalname, "Values", (String)(dynavContagemresultado_contratadacod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "OS Vinculada:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda_Internalname, AV8ContagemResultado_Demanda, StringUtil.RTrim( context.localUtil.Format( AV8ContagemResultado_Demanda, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demanda_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_demanda_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table2_23_DX2( true) ;
         }
         else
         {
            wb_table2_23_DX2( false) ;
         }
         return  ;
      }

      protected void wb_table2_23_DX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Prestadora:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo, dynavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)), 1, dynavContratada_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavContratada_codigo.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_WP_OSServico.htm");
            dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", (String)(dynavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Respons�vel:", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contadorfmcod, dynavContagemresultado_contadorfmcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0)), 1, dynavContagemresultado_contadorfmcod_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e17dx1_client"+"'", "int", "", 1, dynavContagemresultado_contadorfmcod.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_WP_OSServico.htm");
            dynavContagemresultado_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfmcod_Internalname, "Values", (String)(dynavContagemresultado_contadorfmcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Servi�o", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_servico, dynavContagemresultado_servico_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0)), 1, dynavContagemresultado_servico_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_SERVICO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_WP_OSServico.htm");
            dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_servico_Internalname, "Values", (String)(dynavContagemresultado_servico.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Nova OS:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNewcontagemresultado_demanda_Internalname, AV19NewContagemResultado_Demanda, StringUtil.RTrim( context.localUtil.Format( AV19NewContagemResultado_Demanda, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNewcontagemresultado_demanda_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "Data do Servi�o:", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datacnt_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datacnt_Internalname, context.localUtil.Format(AV7ContagemResultado_DataCnt, "99/99/99"), context.localUtil.Format( AV7ContagemResultado_DataCnt, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datacnt_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_datacnt_Enabled, 1, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OSServico.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datacnt_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_datacnt_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_OSServico.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock7_Internalname, "Observa��o:", "", "", lblTextblock7_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONTAGEMRESULTADO_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock8_Internalname, "N�o conformidade na Demanda:", "", "", lblTextblock8_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_naocnfdmncod, dynavContagemresultado_naocnfdmncod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContagemResultado_NaoCnfDmnCod), 6, 0)), 1, dynavContagemresultado_naocnfdmncod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_WP_OSServico.htm");
            dynavContagemresultado_naocnfdmncod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContagemResultado_NaoCnfDmnCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_naocnfdmncod_Internalname, "Values", (String)(dynavContagemresultado_naocnfdmncod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock11_Internalname, "N�o conformidade na Contagem:", "", "", lblTextblock11_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_naocnfcntcod, dynavContagemresultado_naocnfcntcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25ContagemResultado_NaoCnfCntCod), 6, 0)), 1, dynavContagemresultado_naocnfcntcod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", "", true, "HLP_WP_OSServico.htm");
            dynavContagemresultado_naocnfcntcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25ContagemResultado_NaoCnfCntCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_naocnfcntcod_Internalname, "Values", (String)(dynavContagemresultado_naocnfcntcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock13_Internalname, "Pontos de Fun��o B/L:", "", "", lblTextblock13_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pfbfm_Internalname, StringUtil.LTrim( StringUtil.NToC( AV11ContagemResultado_PFBFM, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV11ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pfbfm_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "&nbsp;- ") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pflfm_Internalname, StringUtil.LTrim( StringUtil.NToC( AV13ContagemResultado_PFLFM, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV13ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pflfm_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImage1_Internalname, context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Mesmos PF da OS", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgImage1_Jsonclick, "'"+""+"'"+",false,"+"'"+"e18dx1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Criar OS vinculada", bttBtnenter_Jsonclick, 5, bttBtnenter_Tooltiptext, "", StyleString, ClassString, 1, bttBtnenter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_OSServico.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Fechar", bttButton1_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DX2e( true) ;
         }
         else
         {
            wb_table1_2_DX2e( false) ;
         }
      }

      protected void wb_table2_23_DX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(325), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock9_Internalname, "Qtd. Entregue Bruto", "", "", lblTextblock9_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pfbfs_Internalname, StringUtil.LTrim( StringUtil.NToC( AV12ContagemResultado_PFBFS, 14, 5, ",", "")), ((edtavContagemresultado_pfbfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV12ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV12ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pfbfs_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_pfbfs_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock10_Internalname, "Qtd. Entregue Liquido", "", "", lblTextblock10_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pflfs_Internalname, StringUtil.LTrim( StringUtil.NToC( AV14ContagemResultado_PFLFS, 14, 5, ",", "")), ((edtavContagemresultado_pflfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV14ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV14ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pflfs_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_pflfs_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OSServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_23_DX2e( true) ;
         }
         else
         {
            wb_table2_23_DX2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV5ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Codigo), 6, 0)));
         AV32Confirmado = (bool)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Confirmado", AV32Confirmado);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADX2( ) ;
         WSDX2( ) ;
         WEDX2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621621316");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_osservico.js", "?2020621621316");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock14_Internalname = "TEXTBLOCK14";
         dynavContratadaorigem_codigo_Internalname = "vCONTRATADAORIGEM_CODIGO";
         lblTextblock12_Internalname = "TEXTBLOCK12";
         dynavContagemresultado_contratadacod_Internalname = "vCONTAGEMRESULTADO_CONTRATADACOD";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavContagemresultado_demanda_Internalname = "vCONTAGEMRESULTADO_DEMANDA";
         lblTextblock9_Internalname = "TEXTBLOCK9";
         edtavContagemresultado_pfbfs_Internalname = "vCONTAGEMRESULTADO_PFBFS";
         lblTextblock10_Internalname = "TEXTBLOCK10";
         edtavContagemresultado_pflfs_Internalname = "vCONTAGEMRESULTADO_PFLFS";
         tblTable3_Internalname = "TABLE3";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         dynavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         dynavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         dynavContagemresultado_servico_Internalname = "vCONTAGEMRESULTADO_SERVICO";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavNewcontagemresultado_demanda_Internalname = "vNEWCONTAGEMRESULTADO_DEMANDA";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavContagemresultado_datacnt_Internalname = "vCONTAGEMRESULTADO_DATACNT";
         lblTextblock7_Internalname = "TEXTBLOCK7";
         Contagemresultado_observacao_Internalname = "CONTAGEMRESULTADO_OBSERVACAO";
         lblTextblock8_Internalname = "TEXTBLOCK8";
         dynavContagemresultado_naocnfdmncod_Internalname = "vCONTAGEMRESULTADO_NAOCNFDMNCOD";
         lblTextblock11_Internalname = "TEXTBLOCK11";
         dynavContagemresultado_naocnfcntcod_Internalname = "vCONTAGEMRESULTADO_NAOCNFCNTCOD";
         lblTextblock13_Internalname = "TEXTBLOCK13";
         edtavContagemresultado_pfbfm_Internalname = "vCONTAGEMRESULTADO_PFBFM";
         edtavContagemresultado_pflfm_Internalname = "vCONTAGEMRESULTADO_PFLFM";
         imgImage1_Internalname = "IMAGE1";
         bttBtnenter_Internalname = "BTNENTER";
         bttButton1_Internalname = "BUTTON1";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavContagemresultado_pflfs_Jsonclick = "";
         edtavContagemresultado_pflfs_Enabled = 1;
         edtavContagemresultado_pfbfs_Jsonclick = "";
         edtavContagemresultado_pfbfs_Enabled = 1;
         bttBtnenter_Enabled = 1;
         edtavContagemresultado_pflfm_Jsonclick = "";
         edtavContagemresultado_pfbfm_Jsonclick = "";
         dynavContagemresultado_naocnfcntcod_Jsonclick = "";
         dynavContagemresultado_naocnfdmncod_Jsonclick = "";
         Contagemresultado_observacao_Enabled = Convert.ToBoolean( 1);
         edtavContagemresultado_datacnt_Jsonclick = "";
         edtavNewcontagemresultado_demanda_Jsonclick = "";
         dynavContagemresultado_servico_Jsonclick = "";
         dynavContagemresultado_contadorfmcod_Jsonclick = "";
         dynavContratada_codigo_Jsonclick = "";
         edtavContagemresultado_demanda_Jsonclick = "";
         edtavContagemresultado_demanda_Enabled = 1;
         dynavContagemresultado_contratadacod_Jsonclick = "";
         dynavContagemresultado_contratadacod.Enabled = 1;
         dynavContratadaorigem_codigo_Jsonclick = "";
         dynavContratadaorigem_codigo.Enabled = 1;
         bttBtnenter_Tooltiptext = "Criar OS vinculada";
         edtavContagemresultado_datacnt_Enabled = 1;
         dynavContagemresultado_contadorfmcod.Enabled = 1;
         dynavContratada_codigo.Enabled = 1;
         dynavContagemresultado_servico.Description = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Servi�o Vinculado";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Contratada_codigo( wwpbaseobjects.SdtWWPContext GX_Parm1 ,
                                            GXCombobox dynGX_Parm2 ,
                                            GXCombobox dynGX_Parm3 ,
                                            GXCombobox dynGX_Parm4 )
      {
         AV20WWPContext = GX_Parm1;
         dynavContratada_codigo = dynGX_Parm2;
         AV17Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.CurrentValue, "."));
         dynavContagemresultado_contadorfmcod = dynGX_Parm3;
         AV21ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.CurrentValue, "."));
         dynavContagemresultado_servico = dynGX_Parm4;
         AV15ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlDX2( AV20WWPContext, AV17Contratada_Codigo) ;
         GXVvCONTAGEMRESULTADO_SERVICO_htmlDX2( AV17Contratada_Codigo) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_contadorfmcod.ItemCount > 0 )
         {
            AV21ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0))), "."));
         }
         dynavContagemresultado_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0));
         isValidOutput.Add(dynavContagemresultado_contadorfmcod);
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV15ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0))), "."));
         }
         dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0));
         isValidOutput.Add(dynavContagemresultado_servico);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("VCONTAGEMRESULTADO_CONTRATADACOD.CLICK","{handler:'E12DX2',iparms:[{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A805ContagemResultado_ContratadaOrigemCod',fld:'CONTAGEMRESULTADO_CONTRATADAORIGEMCOD',pic:'ZZZZZ9',nv:0},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A685ContagemResultado_PFLFSUltima',fld:'CONTAGEMRESULTADO_PFLFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV31ContratadaOrigem_Codigo',fld:'vCONTRATADAORIGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV12ContagemResultado_PFBFS',fld:'vCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_PFLFS',fld:'vCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VCONTAGEMRESULTADO_SERVICO.CLICK","{handler:'E13DX2',iparms:[{av:'AV15ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'dynavContagemresultado_servico'}],oparms:[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{av:'AV19NewContagemResultado_Demanda',fld:'vNEWCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''}]}");
         setEventMetadata("VNEWCONTAGEMRESULTADO_DEMANDA.ISVALID","{handler:'E14DX2',iparms:[{av:'AV19NewContagemResultado_Demanda',fld:'vNEWCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV20WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV23Retorno',fld:'vRETORNO',pic:'ZZZ9',nv:0},{av:'AV15ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV22QuemCadastrou',fld:'vQUEMCADASTROU',pic:'',nv:''}],oparms:[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'}]}");
         setEventMetadata("VCONTAGEMRESULTADO_CONTADORFMCOD.CLICK","{handler:'E17DX1',iparms:[{av:'AV21ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E15DX2',iparms:[{av:'AV22QuemCadastrou',fld:'vQUEMCADASTROU',pic:'',nv:''},{av:'AV23Retorno',fld:'vRETORNO',pic:'ZZZ9',nv:0},{av:'AV20WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV19NewContagemResultado_Demanda',fld:'vNEWCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV15ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV21ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_DataCnt',fld:'vCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV17Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV28Sdt_Demanda',fld:'vSDT_DEMANDA',pic:'',nv:null},{av:'AV26ContagemResultado_Observacao',fld:'vCONTAGEMRESULTADO_OBSERVACAO',pic:'',nv:''},{av:'AV11ContagemResultado_PFBFM',fld:'vCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV13ContagemResultado_PFLFM',fld:'vCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24ContagemResultado_NaoCnfDmnCod',fld:'vCONTAGEMRESULTADO_NAOCNFDMNCOD',pic:'ZZZZZ9',nv:0},{av:'AV25ContagemResultado_NaoCnfCntCod',fld:'vCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV33Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27Sdt_Demandas',fld:'vSDT_DEMANDAS',pic:'',nv:null}],oparms:[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{av:'AV32Confirmado',fld:'vCONFIRMADO',pic:'',nv:false},{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV28Sdt_Demanda',fld:'vSDT_DEMANDA',pic:'',nv:null},{av:'AV27Sdt_Demandas',fld:'vSDT_DEMANDAS',pic:'',nv:null}]}");
         setEventMetadata("'IGUALARPF'","{handler:'E18DX1',iparms:[{av:'AV12ContagemResultado_PFBFS',fld:'vCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_PFLFS',fld:'vCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV11ContagemResultado_PFBFM',fld:'vCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV13ContagemResultado_PFLFM',fld:'vCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV20WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV26ContagemResultado_Observacao = "";
         A457ContagemResultado_Demanda = "";
         AV22QuemCadastrou = "";
         AV28Sdt_Demanda = new SdtSDT_Demandas_Demanda(context);
         AV27Sdt_Demandas = new GxObjectCollection( context, "SDT_Demandas.Demanda", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Demandas_Demanda", "GeneXus.Programs");
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00DX2_A40Contratada_PessoaCod = new int[1] ;
         H00DX2_A39Contratada_Codigo = new int[1] ;
         H00DX2_A41Contratada_PessoaNom = new String[] {""} ;
         H00DX2_n41Contratada_PessoaNom = new bool[] {false} ;
         H00DX2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00DX3_A40Contratada_PessoaCod = new int[1] ;
         H00DX3_A39Contratada_Codigo = new int[1] ;
         H00DX3_A41Contratada_PessoaNom = new String[] {""} ;
         H00DX3_n41Contratada_PessoaNom = new bool[] {false} ;
         H00DX3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00DX4_A40Contratada_PessoaCod = new int[1] ;
         H00DX4_A39Contratada_Codigo = new int[1] ;
         H00DX4_A41Contratada_PessoaNom = new String[] {""} ;
         H00DX4_n41Contratada_PessoaNom = new bool[] {false} ;
         H00DX4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00DX5_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00DX5_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00DX5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00DX5_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00DX5_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00DX5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00DX6_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         H00DX6_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         H00DX6_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00DX6_A605Servico_Sigla = new String[] {""} ;
         H00DX6_n605Servico_Sigla = new bool[] {false} ;
         H00DX6_A39Contratada_Codigo = new int[1] ;
         H00DX7_A426NaoConformidade_Codigo = new int[1] ;
         H00DX7_A427NaoConformidade_Nome = new String[] {""} ;
         H00DX7_A429NaoConformidade_Tipo = new short[1] ;
         H00DX7_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00DX8_A426NaoConformidade_Codigo = new int[1] ;
         H00DX8_A427NaoConformidade_Nome = new String[] {""} ;
         H00DX8_A429NaoConformidade_Tipo = new short[1] ;
         H00DX8_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         AV8ContagemResultado_Demanda = "";
         AV19NewContagemResultado_Demanda = "";
         AV7ContagemResultado_DataCnt = DateTime.MinValue;
         H00DX10_A456ContagemResultado_Codigo = new int[1] ;
         H00DX10_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00DX10_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00DX10_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00DX10_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00DX10_A457ContagemResultado_Demanda = new String[] {""} ;
         H00DX10_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00DX10_A489ContagemResultado_SistemaCod = new int[1] ;
         H00DX10_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00DX10_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00DX10_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         AV29WebSession = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock14_Jsonclick = "";
         TempTags = "";
         lblTextblock12_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock7_Jsonclick = "";
         lblTextblock8_Jsonclick = "";
         lblTextblock11_Jsonclick = "";
         lblTextblock13_Jsonclick = "";
         imgImage1_Jsonclick = "";
         bttBtnenter_Jsonclick = "";
         bttButton1_Jsonclick = "";
         lblTextblock9_Jsonclick = "";
         lblTextblock10_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_osservico__default(),
            new Object[][] {
                new Object[] {
               H00DX2_A40Contratada_PessoaCod, H00DX2_A39Contratada_Codigo, H00DX2_A41Contratada_PessoaNom, H00DX2_n41Contratada_PessoaNom, H00DX2_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00DX3_A40Contratada_PessoaCod, H00DX3_A39Contratada_Codigo, H00DX3_A41Contratada_PessoaNom, H00DX3_n41Contratada_PessoaNom, H00DX3_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00DX4_A40Contratada_PessoaCod, H00DX4_A39Contratada_Codigo, H00DX4_A41Contratada_PessoaNom, H00DX4_n41Contratada_PessoaNom, H00DX4_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00DX5_A70ContratadaUsuario_UsuarioPessoaCod, H00DX5_n70ContratadaUsuario_UsuarioPessoaCod, H00DX5_A69ContratadaUsuario_UsuarioCod, H00DX5_A71ContratadaUsuario_UsuarioPessoaNom, H00DX5_n71ContratadaUsuario_UsuarioPessoaNom, H00DX5_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00DX6_A1595Contratada_AreaTrbSrvPdr, H00DX6_n1595Contratada_AreaTrbSrvPdr, H00DX6_A52Contratada_AreaTrabalhoCod, H00DX6_A605Servico_Sigla, H00DX6_n605Servico_Sigla, H00DX6_A39Contratada_Codigo
               }
               , new Object[] {
               H00DX7_A426NaoConformidade_Codigo, H00DX7_A427NaoConformidade_Nome, H00DX7_A429NaoConformidade_Tipo, H00DX7_A428NaoConformidade_AreaTrabalhoCod
               }
               , new Object[] {
               H00DX8_A426NaoConformidade_Codigo, H00DX8_A427NaoConformidade_Nome, H00DX8_A429NaoConformidade_Tipo, H00DX8_A428NaoConformidade_AreaTrabalhoCod
               }
               , new Object[] {
               H00DX10_A456ContagemResultado_Codigo, H00DX10_A490ContagemResultado_ContratadaCod, H00DX10_n490ContagemResultado_ContratadaCod, H00DX10_A805ContagemResultado_ContratadaOrigemCod, H00DX10_n805ContagemResultado_ContratadaOrigemCod, H00DX10_A457ContagemResultado_Demanda, H00DX10_n457ContagemResultado_Demanda, H00DX10_A489ContagemResultado_SistemaCod, H00DX10_n489ContagemResultado_SistemaCod, H00DX10_A684ContagemResultado_PFBFSUltima,
               H00DX10_A685ContagemResultado_PFLFSUltima
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         dynavContratadaorigem_codigo.Enabled = 0;
         dynavContagemresultado_contratadacod.Enabled = 0;
         edtavContagemresultado_demanda_Enabled = 0;
         edtavContagemresultado_pfbfs_Enabled = 0;
         edtavContagemresultado_pflfs_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV23Retorno ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private short wbTemp ;
      private int AV5ContagemResultado_Codigo ;
      private int wcpOAV5ContagemResultado_Codigo ;
      private int AV17Contratada_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int AV33Sistema_Codigo ;
      private int gxdynajaxindex ;
      private int AV31ContratadaOrigem_Codigo ;
      private int AV6ContagemResultado_ContratadaCod ;
      private int AV21ContagemResultado_ContadorFMCod ;
      private int AV15ContagemResultado_Servico ;
      private int Servico_Codigo ;
      private int AV24ContagemResultado_NaoCnfDmnCod ;
      private int AV25ContagemResultado_NaoCnfCntCod ;
      private int edtavContagemresultado_demanda_Enabled ;
      private int edtavContagemresultado_pfbfs_Enabled ;
      private int edtavContagemresultado_pflfs_Enabled ;
      private int bttBtnenter_Enabled ;
      private int edtavContagemresultado_datacnt_Enabled ;
      private int GXt_int1 ;
      private int idxLst ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal AV12ContagemResultado_PFBFS ;
      private decimal AV14ContagemResultado_PFLFS ;
      private decimal AV11ContagemResultado_PFBFM ;
      private decimal AV13ContagemResultado_PFLFM ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV22QuemCadastrou ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String dynavContratadaorigem_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynavContagemresultado_contratadacod_Internalname ;
      private String edtavContagemresultado_demanda_Internalname ;
      private String edtavContagemresultado_pfbfs_Internalname ;
      private String edtavContagemresultado_pflfs_Internalname ;
      private String dynavContratada_codigo_Internalname ;
      private String dynavContagemresultado_contadorfmcod_Internalname ;
      private String dynavContagemresultado_servico_Internalname ;
      private String edtavNewcontagemresultado_demanda_Internalname ;
      private String edtavContagemresultado_datacnt_Internalname ;
      private String dynavContagemresultado_naocnfdmncod_Internalname ;
      private String dynavContagemresultado_naocnfcntcod_Internalname ;
      private String edtavContagemresultado_pfbfm_Internalname ;
      private String edtavContagemresultado_pflfm_Internalname ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Tooltiptext ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock14_Internalname ;
      private String lblTextblock14_Jsonclick ;
      private String TempTags ;
      private String dynavContratadaorigem_codigo_Jsonclick ;
      private String lblTextblock12_Internalname ;
      private String lblTextblock12_Jsonclick ;
      private String dynavContagemresultado_contratadacod_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavContagemresultado_demanda_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String dynavContratada_codigo_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String dynavContagemresultado_contadorfmcod_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String dynavContagemresultado_servico_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavNewcontagemresultado_demanda_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String edtavContagemresultado_datacnt_Jsonclick ;
      private String lblTextblock7_Internalname ;
      private String lblTextblock7_Jsonclick ;
      private String lblTextblock8_Internalname ;
      private String lblTextblock8_Jsonclick ;
      private String dynavContagemresultado_naocnfdmncod_Jsonclick ;
      private String lblTextblock11_Internalname ;
      private String lblTextblock11_Jsonclick ;
      private String dynavContagemresultado_naocnfcntcod_Jsonclick ;
      private String lblTextblock13_Internalname ;
      private String lblTextblock13_Jsonclick ;
      private String edtavContagemresultado_pfbfm_Jsonclick ;
      private String edtavContagemresultado_pflfm_Jsonclick ;
      private String imgImage1_Internalname ;
      private String imgImage1_Jsonclick ;
      private String bttBtnenter_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String tblTable3_Internalname ;
      private String lblTextblock9_Internalname ;
      private String lblTextblock9_Jsonclick ;
      private String edtavContagemresultado_pfbfs_Jsonclick ;
      private String lblTextblock10_Internalname ;
      private String lblTextblock10_Jsonclick ;
      private String edtavContagemresultado_pflfs_Jsonclick ;
      private String Contagemresultado_observacao_Internalname ;
      private DateTime AV7ContagemResultado_DataCnt ;
      private bool AV32Confirmado ;
      private bool wcpOAV32Confirmado ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Contagemresultado_observacao_Enabled ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n489ContagemResultado_SistemaCod ;
      private String AV26ContagemResultado_Observacao ;
      private String A457ContagemResultado_Demanda ;
      private String AV8ContagemResultado_Demanda ;
      private String AV19NewContagemResultado_Demanda ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private bool aP1_Confirmado ;
      private GXCombobox dynavContratadaorigem_codigo ;
      private GXCombobox dynavContagemresultado_contratadacod ;
      private GXCombobox dynavContratada_codigo ;
      private GXCombobox dynavContagemresultado_contadorfmcod ;
      private GXCombobox dynavContagemresultado_servico ;
      private GXCombobox dynavContagemresultado_naocnfdmncod ;
      private GXCombobox dynavContagemresultado_naocnfcntcod ;
      private IDataStoreProvider pr_default ;
      private int[] H00DX2_A40Contratada_PessoaCod ;
      private int[] H00DX2_A39Contratada_Codigo ;
      private String[] H00DX2_A41Contratada_PessoaNom ;
      private bool[] H00DX2_n41Contratada_PessoaNom ;
      private int[] H00DX2_A52Contratada_AreaTrabalhoCod ;
      private int[] H00DX3_A40Contratada_PessoaCod ;
      private int[] H00DX3_A39Contratada_Codigo ;
      private String[] H00DX3_A41Contratada_PessoaNom ;
      private bool[] H00DX3_n41Contratada_PessoaNom ;
      private int[] H00DX3_A52Contratada_AreaTrabalhoCod ;
      private int[] H00DX4_A40Contratada_PessoaCod ;
      private int[] H00DX4_A39Contratada_Codigo ;
      private String[] H00DX4_A41Contratada_PessoaNom ;
      private bool[] H00DX4_n41Contratada_PessoaNom ;
      private int[] H00DX4_A52Contratada_AreaTrabalhoCod ;
      private int[] H00DX5_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00DX5_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00DX5_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00DX5_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00DX5_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00DX5_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00DX6_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] H00DX6_n1595Contratada_AreaTrbSrvPdr ;
      private int[] H00DX6_A52Contratada_AreaTrabalhoCod ;
      private String[] H00DX6_A605Servico_Sigla ;
      private bool[] H00DX6_n605Servico_Sigla ;
      private int[] H00DX6_A39Contratada_Codigo ;
      private int[] H00DX7_A426NaoConformidade_Codigo ;
      private String[] H00DX7_A427NaoConformidade_Nome ;
      private short[] H00DX7_A429NaoConformidade_Tipo ;
      private int[] H00DX7_A428NaoConformidade_AreaTrabalhoCod ;
      private int[] H00DX8_A426NaoConformidade_Codigo ;
      private String[] H00DX8_A427NaoConformidade_Nome ;
      private short[] H00DX8_A429NaoConformidade_Tipo ;
      private int[] H00DX8_A428NaoConformidade_AreaTrabalhoCod ;
      private int[] H00DX10_A456ContagemResultado_Codigo ;
      private int[] H00DX10_A490ContagemResultado_ContratadaCod ;
      private bool[] H00DX10_n490ContagemResultado_ContratadaCod ;
      private int[] H00DX10_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00DX10_n805ContagemResultado_ContratadaOrigemCod ;
      private String[] H00DX10_A457ContagemResultado_Demanda ;
      private bool[] H00DX10_n457ContagemResultado_Demanda ;
      private int[] H00DX10_A489ContagemResultado_SistemaCod ;
      private bool[] H00DX10_n489ContagemResultado_SistemaCod ;
      private decimal[] H00DX10_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00DX10_A685ContagemResultado_PFLFSUltima ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV29WebSession ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Demandas_Demanda ))]
      private IGxCollection AV27Sdt_Demandas ;
      private GXWebForm Form ;
      private SdtSDT_Demandas_Demanda AV28Sdt_Demanda ;
      private wwpbaseobjects.SdtWWPContext AV20WWPContext ;
   }

   public class wp_osservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DX2 ;
          prmH00DX2 = new Object[] {
          new Object[] {"@AV20WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DX3 ;
          prmH00DX3 = new Object[] {
          new Object[] {"@AV20WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DX4 ;
          prmH00DX4 = new Object[] {
          new Object[] {"@AV20WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DX5 ;
          prmH00DX5 = new Object[] {
          new Object[] {"@AV17Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DX6 ;
          prmH00DX6 = new Object[] {
          new Object[] {"@AV17Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DX7 ;
          prmH00DX7 = new Object[] {
          new Object[] {"@AV20WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DX8 ;
          prmH00DX8 = new Object[] {
          new Object[] {"@AV20WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DX10 ;
          prmH00DX10 = new Object[] {
          new Object[] {"@AV5ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DX2", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV20WWPC_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DX2,0,0,true,false )
             ,new CursorDef("H00DX3", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV20WWPC_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DX3,0,0,true,false )
             ,new CursorDef("H00DX4", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV20WWPC_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DX4,0,0,true,false )
             ,new CursorDef("H00DX5", "SELECT T4.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE T1.[ContratadaUsuario_ContratadaCod] = @AV17Contratada_Codigo ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DX5,0,0,true,false )
             ,new CursorDef("H00DX6", "SELECT T3.[Servico_Codigo] AS Contratada_AreaTrbSrvPdr, T2.[AreaTrabalho_Codigo] AS Contratada_AreaTrabalhoCod, T3.[Servico_Sigla], T1.[Contratada_Codigo] FROM (([Contratada] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[AreaTrabalho_ServicoPadrao]) WHERE T1.[Contratada_Codigo] = @AV17Contratada_Codigo ORDER BY T3.[Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DX6,0,0,true,false )
             ,new CursorDef("H00DX7", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Nome], [NaoConformidade_Tipo], [NaoConformidade_AreaTrabalhoCod] FROM [NaoConformidade] WITH (NOLOCK) WHERE ([NaoConformidade_Tipo] = 1) AND ([NaoConformidade_AreaTrabalhoCod] = @AV20WWPC_1Areatrabalho_codigo) ORDER BY [NaoConformidade_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DX7,0,0,true,false )
             ,new CursorDef("H00DX8", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Nome], [NaoConformidade_Tipo], [NaoConformidade_AreaTrabalhoCod] FROM [NaoConformidade] WITH (NOLOCK) WHERE ([NaoConformidade_Tipo] = 2) AND ([NaoConformidade_AreaTrabalhoCod] = @AV20WWPC_1Areatrabalho_codigo) ORDER BY [NaoConformidade_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DX8,0,0,true,false )
             ,new CursorDef("H00DX10", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_ContratadaOrigemCod], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_SistemaCod], COALESCE( T2.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T2.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV5ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DX10,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
