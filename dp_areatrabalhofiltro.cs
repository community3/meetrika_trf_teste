/*
               File: DP_AreaTrabalhoFiltro
        Description: Filtro Area de Trabalho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:47:47.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_areatrabalhofiltro : GXProcedure
   {
      public dp_areatrabalhofiltro( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_areatrabalhofiltro( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo ,
                           int aP1_Usuario_Codigo ,
                           out IGxCollection aP2_Gxm2rootcol )
      {
         this.AV5Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV6Usuario_Codigo = aP1_Usuario_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP2_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( int aP0_Contratada_Codigo ,
                                       int aP1_Usuario_Codigo )
      {
         this.AV5Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV6Usuario_Codigo = aP1_Usuario_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP2_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( int aP0_Contratada_Codigo ,
                                 int aP1_Usuario_Codigo ,
                                 out IGxCollection aP2_Gxm2rootcol )
      {
         dp_areatrabalhofiltro objdp_areatrabalhofiltro;
         objdp_areatrabalhofiltro = new dp_areatrabalhofiltro();
         objdp_areatrabalhofiltro.AV5Contratada_Codigo = aP0_Contratada_Codigo;
         objdp_areatrabalhofiltro.AV6Usuario_Codigo = aP1_Usuario_Codigo;
         objdp_areatrabalhofiltro.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs") ;
         objdp_areatrabalhofiltro.context.SetSubmitInitialConfig(context);
         objdp_areatrabalhofiltro.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_areatrabalhofiltro);
         aP2_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_areatrabalhofiltro)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P000R2 */
         pr_default.execute(0, new Object[] {AV6Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A66ContratadaUsuario_ContratadaCod = P000R2_A66ContratadaUsuario_ContratadaCod[0];
            A43Contratada_Ativo = P000R2_A43Contratada_Ativo[0];
            n43Contratada_Ativo = P000R2_n43Contratada_Ativo[0];
            A1394ContratadaUsuario_UsuarioAtivo = P000R2_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = P000R2_n1394ContratadaUsuario_UsuarioAtivo[0];
            A69ContratadaUsuario_UsuarioCod = P000R2_A69ContratadaUsuario_UsuarioCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = P000R2_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = P000R2_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A1297ContratadaUsuario_AreaTrabalhoDes = P000R2_A1297ContratadaUsuario_AreaTrabalhoDes[0];
            n1297ContratadaUsuario_AreaTrabalhoDes = P000R2_n1297ContratadaUsuario_AreaTrabalhoDes[0];
            A43Contratada_Ativo = P000R2_A43Contratada_Ativo[0];
            n43Contratada_Ativo = P000R2_n43Contratada_Ativo[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = P000R2_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = P000R2_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A1297ContratadaUsuario_AreaTrabalhoDes = P000R2_A1297ContratadaUsuario_AreaTrabalhoDes[0];
            n1297ContratadaUsuario_AreaTrabalhoDes = P000R2_n1297ContratadaUsuario_AreaTrabalhoDes[0];
            A1394ContratadaUsuario_UsuarioAtivo = P000R2_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = P000R2_n1394ContratadaUsuario_UsuarioAtivo[0];
            Gxm1sdt_codigos = new SdtSDT_Codigos(context);
            Gxm2rootcol.Add(Gxm1sdt_codigos, 0);
            Gxm1sdt_codigos.gxTpr_Codigo = A1228ContratadaUsuario_AreaTrabalhoCod;
            Gxm1sdt_codigos.gxTpr_Descricao = A1297ContratadaUsuario_AreaTrabalhoDes;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         /* Using cursor P000R4 */
         pr_default.execute(1, new Object[] {AV6Usuario_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A63ContratanteUsuario_ContratanteCod = P000R4_A63ContratanteUsuario_ContratanteCod[0];
            A54Usuario_Ativo = P000R4_A54Usuario_Ativo[0];
            n54Usuario_Ativo = P000R4_n54Usuario_Ativo[0];
            A60ContratanteUsuario_UsuarioCod = P000R4_A60ContratanteUsuario_UsuarioCod[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = P000R4_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            A40000AreaTrabalho_Descricao = P000R4_A40000AreaTrabalho_Descricao[0];
            n40000AreaTrabalho_Descricao = P000R4_n40000AreaTrabalho_Descricao[0];
            A54Usuario_Ativo = P000R4_A54Usuario_Ativo[0];
            n54Usuario_Ativo = P000R4_n54Usuario_Ativo[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = P000R4_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            A40000AreaTrabalho_Descricao = P000R4_A40000AreaTrabalho_Descricao[0];
            n40000AreaTrabalho_Descricao = P000R4_n40000AreaTrabalho_Descricao[0];
            Gxm1sdt_codigos = new SdtSDT_Codigos(context);
            Gxm2rootcol.Add(Gxm1sdt_codigos, 0);
            Gxm1sdt_codigos.gxTpr_Codigo = A1020ContratanteUsuario_AreaTrabalhoCod;
            Gxm1sdt_codigos.gxTpr_Descricao = A40000AreaTrabalho_Descricao;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P000R2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P000R2_A43Contratada_Ativo = new bool[] {false} ;
         P000R2_n43Contratada_Ativo = new bool[] {false} ;
         P000R2_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P000R2_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P000R2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P000R2_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         P000R2_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P000R2_A1297ContratadaUsuario_AreaTrabalhoDes = new String[] {""} ;
         P000R2_n1297ContratadaUsuario_AreaTrabalhoDes = new bool[] {false} ;
         A1297ContratadaUsuario_AreaTrabalhoDes = "";
         Gxm1sdt_codigos = new SdtSDT_Codigos(context);
         P000R4_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P000R4_A54Usuario_Ativo = new bool[] {false} ;
         P000R4_n54Usuario_Ativo = new bool[] {false} ;
         P000R4_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P000R4_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         P000R4_A40000AreaTrabalho_Descricao = new String[] {""} ;
         P000R4_n40000AreaTrabalho_Descricao = new bool[] {false} ;
         A40000AreaTrabalho_Descricao = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_areatrabalhofiltro__default(),
            new Object[][] {
                new Object[] {
               P000R2_A66ContratadaUsuario_ContratadaCod, P000R2_A43Contratada_Ativo, P000R2_n43Contratada_Ativo, P000R2_A1394ContratadaUsuario_UsuarioAtivo, P000R2_n1394ContratadaUsuario_UsuarioAtivo, P000R2_A69ContratadaUsuario_UsuarioCod, P000R2_A1228ContratadaUsuario_AreaTrabalhoCod, P000R2_n1228ContratadaUsuario_AreaTrabalhoCod, P000R2_A1297ContratadaUsuario_AreaTrabalhoDes, P000R2_n1297ContratadaUsuario_AreaTrabalhoDes
               }
               , new Object[] {
               P000R4_A63ContratanteUsuario_ContratanteCod, P000R4_A54Usuario_Ativo, P000R4_n54Usuario_Ativo, P000R4_A60ContratanteUsuario_UsuarioCod, P000R4_A1020ContratanteUsuario_AreaTrabalhoCod, P000R4_A40000AreaTrabalho_Descricao, P000R4_n40000AreaTrabalho_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV5Contratada_Codigo ;
      private int AV6Usuario_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private String scmdbuf ;
      private bool A43Contratada_Ativo ;
      private bool n43Contratada_Ativo ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool A54Usuario_Ativo ;
      private bool n54Usuario_Ativo ;
      private bool n40000AreaTrabalho_Descricao ;
      private String A1297ContratadaUsuario_AreaTrabalhoDes ;
      private String A40000AreaTrabalho_Descricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P000R2_A66ContratadaUsuario_ContratadaCod ;
      private bool[] P000R2_A43Contratada_Ativo ;
      private bool[] P000R2_n43Contratada_Ativo ;
      private bool[] P000R2_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] P000R2_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] P000R2_A69ContratadaUsuario_UsuarioCod ;
      private int[] P000R2_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] P000R2_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private String[] P000R2_A1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool[] P000R2_n1297ContratadaUsuario_AreaTrabalhoDes ;
      private int[] P000R4_A63ContratanteUsuario_ContratanteCod ;
      private bool[] P000R4_A54Usuario_Ativo ;
      private bool[] P000R4_n54Usuario_Ativo ;
      private int[] P000R4_A60ContratanteUsuario_UsuarioCod ;
      private int[] P000R4_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private String[] P000R4_A40000AreaTrabalho_Descricao ;
      private bool[] P000R4_n40000AreaTrabalho_Descricao ;
      private IGxCollection aP2_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSDT_Codigos Gxm1sdt_codigos ;
   }

   public class dp_areatrabalhofiltro__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000R2 ;
          prmP000R2 = new Object[] {
          new Object[] {"@AV6Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP000R4 ;
          prmP000R4 = new Object[] {
          new Object[] {"@AV6Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000R2", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_Ativo], T4.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T3.[AreaTrabalho_Descricao] AS ContratadaUsuario_AreaTrabalhoDes FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contratada_AreaTrabalhoCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV6Usuario_Codigo) AND (T4.[Usuario_Ativo] = 1) AND (T2.[Contratada_Ativo] = 1) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000R2,100,0,false,false )
             ,new CursorDef("P000R4", "SELECT T1.[ContratanteUsuario_ContratanteCod], T2.[Usuario_Ativo], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, COALESCE( T3.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod, COALESCE( T3.[AreaTrabalho_Descricao], '') AS AreaTrabalho_Descricao FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN (SELECT MIN(T4.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T5.[ContratanteUsuario_ContratanteCod], T5.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, MIN(T4.[AreaTrabalho_Descricao]) AS AreaTrabalho_Descricao FROM [AreaTrabalho] T4 WITH (NOLOCK),  [ContratanteUsuario] T5 WITH (NOLOCK) WHERE T4.[Contratante_Codigo] = T5.[ContratanteUsuario_ContratanteCod] GROUP BY T5.[ContratanteUsuario_ContratanteCod], T5.[ContratanteUsuario_UsuarioCod] ) T3 ON T3.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T3.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T1.[ContratanteUsuario_UsuarioCod] = @AV6Usuario_Codigo) AND (T2.[Usuario_Ativo] = 1) ORDER BY T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000R4,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
