/*
               File: type_SdtSDT_CheckList_Item
        Description: SDT_CheckList
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:59.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_CheckList.Item" )]
   [XmlType(TypeName =  "SDT_CheckList.Item" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_CheckList_Item : GxUserType
   {
      public SdtSDT_CheckList_Item( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_CheckList_Item_Descricao = "";
         gxTv_SdtSDT_CheckList_Item_Cumpre = "";
         gxTv_SdtSDT_CheckList_Item_Naocnfqdo = "";
      }

      public SdtSDT_CheckList_Item( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_CheckList_Item deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_CheckList_Item)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_CheckList_Item obj ;
         obj = this;
         obj.gxTpr_Codigo = deserialized.gxTpr_Codigo;
         obj.gxTpr_Descricao = deserialized.gxTpr_Descricao;
         obj.gxTpr_Cumpre = deserialized.gxTpr_Cumpre;
         obj.gxTpr_Obrigatorio = deserialized.gxTpr_Obrigatorio;
         obj.gxTpr_Naoconformidade = deserialized.gxTpr_Naoconformidade;
         obj.gxTpr_Senaocumpre = deserialized.gxTpr_Senaocumpre;
         obj.gxTpr_Naocnfqdo = deserialized.gxTpr_Naocnfqdo;
         obj.gxTpr_Impeditivo = deserialized.gxTpr_Impeditivo;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Codigo") )
               {
                  gxTv_SdtSDT_CheckList_Item_Codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Descricao") )
               {
                  gxTv_SdtSDT_CheckList_Item_Descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Cumpre") )
               {
                  gxTv_SdtSDT_CheckList_Item_Cumpre = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Obrigatorio") )
               {
                  gxTv_SdtSDT_CheckList_Item_Obrigatorio = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "NaoConformidade") )
               {
                  gxTv_SdtSDT_CheckList_Item_Naoconformidade = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SeNaoCumpre") )
               {
                  gxTv_SdtSDT_CheckList_Item_Senaocumpre = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "NaoCnfQdo") )
               {
                  gxTv_SdtSDT_CheckList_Item_Naocnfqdo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Impeditivo") )
               {
                  gxTv_SdtSDT_CheckList_Item_Impeditivo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_CheckList.Item";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_CheckList_Item_Codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Descricao", StringUtil.RTrim( gxTv_SdtSDT_CheckList_Item_Descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Cumpre", StringUtil.RTrim( gxTv_SdtSDT_CheckList_Item_Cumpre));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Obrigatorio", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_CheckList_Item_Obrigatorio)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("NaoConformidade", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_CheckList_Item_Naoconformidade), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("SeNaoCumpre", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_CheckList_Item_Senaocumpre), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("NaoCnfQdo", StringUtil.RTrim( gxTv_SdtSDT_CheckList_Item_Naocnfqdo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Impeditivo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_CheckList_Item_Impeditivo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Codigo", gxTv_SdtSDT_CheckList_Item_Codigo, false);
         AddObjectProperty("Descricao", gxTv_SdtSDT_CheckList_Item_Descricao, false);
         AddObjectProperty("Cumpre", gxTv_SdtSDT_CheckList_Item_Cumpre, false);
         AddObjectProperty("Obrigatorio", gxTv_SdtSDT_CheckList_Item_Obrigatorio, false);
         AddObjectProperty("NaoConformidade", gxTv_SdtSDT_CheckList_Item_Naoconformidade, false);
         AddObjectProperty("SeNaoCumpre", gxTv_SdtSDT_CheckList_Item_Senaocumpre, false);
         AddObjectProperty("NaoCnfQdo", gxTv_SdtSDT_CheckList_Item_Naocnfqdo, false);
         AddObjectProperty("Impeditivo", gxTv_SdtSDT_CheckList_Item_Impeditivo, false);
         return  ;
      }

      [  SoapElement( ElementName = "Codigo" )]
      [  XmlElement( ElementName = "Codigo"   )]
      public int gxTpr_Codigo
      {
         get {
            return gxTv_SdtSDT_CheckList_Item_Codigo ;
         }

         set {
            gxTv_SdtSDT_CheckList_Item_Codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Descricao" )]
      [  XmlElement( ElementName = "Descricao"   )]
      public String gxTpr_Descricao
      {
         get {
            return gxTv_SdtSDT_CheckList_Item_Descricao ;
         }

         set {
            gxTv_SdtSDT_CheckList_Item_Descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Cumpre" )]
      [  XmlElement( ElementName = "Cumpre"   )]
      public String gxTpr_Cumpre
      {
         get {
            return gxTv_SdtSDT_CheckList_Item_Cumpre ;
         }

         set {
            gxTv_SdtSDT_CheckList_Item_Cumpre = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Obrigatorio" )]
      [  XmlElement( ElementName = "Obrigatorio"   )]
      public bool gxTpr_Obrigatorio
      {
         get {
            return gxTv_SdtSDT_CheckList_Item_Obrigatorio ;
         }

         set {
            gxTv_SdtSDT_CheckList_Item_Obrigatorio = value;
         }

      }

      [  SoapElement( ElementName = "NaoConformidade" )]
      [  XmlElement( ElementName = "NaoConformidade"   )]
      public int gxTpr_Naoconformidade
      {
         get {
            return gxTv_SdtSDT_CheckList_Item_Naoconformidade ;
         }

         set {
            gxTv_SdtSDT_CheckList_Item_Naoconformidade = (int)(value);
         }

      }

      [  SoapElement( ElementName = "SeNaoCumpre" )]
      [  XmlElement( ElementName = "SeNaoCumpre"   )]
      public short gxTpr_Senaocumpre
      {
         get {
            return gxTv_SdtSDT_CheckList_Item_Senaocumpre ;
         }

         set {
            gxTv_SdtSDT_CheckList_Item_Senaocumpre = (short)(value);
         }

      }

      [  SoapElement( ElementName = "NaoCnfQdo" )]
      [  XmlElement( ElementName = "NaoCnfQdo"   )]
      public String gxTpr_Naocnfqdo
      {
         get {
            return gxTv_SdtSDT_CheckList_Item_Naocnfqdo ;
         }

         set {
            gxTv_SdtSDT_CheckList_Item_Naocnfqdo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Impeditivo" )]
      [  XmlElement( ElementName = "Impeditivo"   )]
      public bool gxTpr_Impeditivo
      {
         get {
            return gxTv_SdtSDT_CheckList_Item_Impeditivo ;
         }

         set {
            gxTv_SdtSDT_CheckList_Item_Impeditivo = value;
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_CheckList_Item_Descricao = "";
         gxTv_SdtSDT_CheckList_Item_Cumpre = "";
         gxTv_SdtSDT_CheckList_Item_Naocnfqdo = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_CheckList_Item_Senaocumpre ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_CheckList_Item_Codigo ;
      protected int gxTv_SdtSDT_CheckList_Item_Naoconformidade ;
      protected String gxTv_SdtSDT_CheckList_Item_Cumpre ;
      protected String gxTv_SdtSDT_CheckList_Item_Naocnfqdo ;
      protected String sTagName ;
      protected bool gxTv_SdtSDT_CheckList_Item_Obrigatorio ;
      protected bool gxTv_SdtSDT_CheckList_Item_Impeditivo ;
      protected String gxTv_SdtSDT_CheckList_Item_Descricao ;
   }

   [DataContract(Name = @"SDT_CheckList.Item", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_CheckList_Item_RESTInterface : GxGenericCollectionItem<SdtSDT_CheckList_Item>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_CheckList_Item_RESTInterface( ) : base()
      {
      }

      public SdtSDT_CheckList_Item_RESTInterface( SdtSDT_CheckList_Item psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Codigo
      {
         get {
            return sdt.gxTpr_Codigo ;
         }

         set {
            sdt.gxTpr_Codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Descricao" , Order = 1 )]
      public String gxTpr_Descricao
      {
         get {
            return sdt.gxTpr_Descricao ;
         }

         set {
            sdt.gxTpr_Descricao = (String)(value);
         }

      }

      [DataMember( Name = "Cumpre" , Order = 2 )]
      public String gxTpr_Cumpre
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Cumpre) ;
         }

         set {
            sdt.gxTpr_Cumpre = (String)(value);
         }

      }

      [DataMember( Name = "Obrigatorio" , Order = 3 )]
      public bool gxTpr_Obrigatorio
      {
         get {
            return sdt.gxTpr_Obrigatorio ;
         }

         set {
            sdt.gxTpr_Obrigatorio = value;
         }

      }

      [DataMember( Name = "NaoConformidade" , Order = 4 )]
      public Nullable<int> gxTpr_Naoconformidade
      {
         get {
            return sdt.gxTpr_Naoconformidade ;
         }

         set {
            sdt.gxTpr_Naoconformidade = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "SeNaoCumpre" , Order = 5 )]
      public Nullable<short> gxTpr_Senaocumpre
      {
         get {
            return sdt.gxTpr_Senaocumpre ;
         }

         set {
            sdt.gxTpr_Senaocumpre = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "NaoCnfQdo" , Order = 6 )]
      public String gxTpr_Naocnfqdo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Naocnfqdo) ;
         }

         set {
            sdt.gxTpr_Naocnfqdo = (String)(value);
         }

      }

      [DataMember( Name = "Impeditivo" , Order = 7 )]
      public bool gxTpr_Impeditivo
      {
         get {
            return sdt.gxTpr_Impeditivo ;
         }

         set {
            sdt.gxTpr_Impeditivo = value;
         }

      }

      public SdtSDT_CheckList_Item sdt
      {
         get {
            return (SdtSDT_CheckList_Item)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_CheckList_Item() ;
         }
      }

   }

}
