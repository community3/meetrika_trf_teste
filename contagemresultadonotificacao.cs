/*
               File: ContagemResultadoNotificacao
        Description: Notifica��es da Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:38.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadonotificacao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_21") == 0 )
         {
            A1412ContagemResultadoNotificacao_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_21( A1412ContagemResultadoNotificacao_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_22") == 0 )
         {
            A1412ContagemResultadoNotificacao_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_22( A1412ContagemResultadoNotificacao_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_17") == 0 )
         {
            A1413ContagemResultadoNotificacao_UsuCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1413ContagemResultadoNotificacao_UsuCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_17( A1413ContagemResultadoNotificacao_UsuCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_20") == 0 )
         {
            A1427ContagemResultadoNotificacao_UsuPesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1427ContagemResultadoNotificacao_UsuPesCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1427ContagemResultadoNotificacao_UsuPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_20( A1427ContagemResultadoNotificacao_UsuPesCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_18") == 0 )
         {
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1966ContagemResultadoNotificacao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1966ContagemResultadoNotificacao_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_18( A1966ContagemResultadoNotificacao_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_19") == 0 )
         {
            A1965ContagemResultadoNotificacao_ReenvioCod = (long)(NumberUtil.Val( GetNextPar( ), "."));
            n1965ContagemResultadoNotificacao_ReenvioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1965ContagemResultadoNotificacao_ReenvioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1965ContagemResultadoNotificacao_ReenvioCod), 10, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_19( A1965ContagemResultadoNotificacao_ReenvioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_24") == 0 )
         {
            A1414ContagemResultadoNotificacao_DestCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_24( A1414ContagemResultadoNotificacao_DestCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_25") == 0 )
         {
            A1426ContagemResultadoNotificacao_DestPesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1426ContagemResultadoNotificacao_DestPesCod = false;
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_25( A1426ContagemResultadoNotificacao_DestPesCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_27") == 0 )
         {
            A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_27( A456ContagemResultado_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_28") == 0 )
         {
            A1553ContagemResultado_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1553ContagemResultado_CntSrvCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_28( A1553ContagemResultado_CntSrvCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_29") == 0 )
         {
            A601ContagemResultado_Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n601ContagemResultado_Servico = false;
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_29( A601ContagemResultado_Servico) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridlevel_destinatario") == 0 )
         {
            nRC_GXsfl_99 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_99_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_99_idx = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridlevel_destinatario_newrow( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridlevel_demanda") == 0 )
         {
            nRC_GXsfl_111 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_111_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_111_idx = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridlevel_demanda_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContagemResultadoNotificacao_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContagemResultadoNotificacao_Midia.Name = "CONTAGEMRESULTADONOTIFICACAO_MIDIA";
         cmbContagemResultadoNotificacao_Midia.WebTags = "";
         cmbContagemResultadoNotificacao_Midia.addItem("EML", "Email", 0);
         cmbContagemResultadoNotificacao_Midia.addItem("SMS", "SMS", 0);
         cmbContagemResultadoNotificacao_Midia.addItem("TEL", "Telefone", 0);
         if ( cmbContagemResultadoNotificacao_Midia.ItemCount > 0 )
         {
            A1420ContagemResultadoNotificacao_Midia = cmbContagemResultadoNotificacao_Midia.getValidValue(A1420ContagemResultadoNotificacao_Midia);
            n1420ContagemResultadoNotificacao_Midia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1420ContagemResultadoNotificacao_Midia", A1420ContagemResultadoNotificacao_Midia);
         }
         cmbContagemResultadoNotificacao_Aut.Name = "CONTAGEMRESULTADONOTIFICACAO_AUT";
         cmbContagemResultadoNotificacao_Aut.WebTags = "";
         cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContagemResultadoNotificacao_Aut.ItemCount > 0 )
         {
            A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cmbContagemResultadoNotificacao_Aut.getValidValue(StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut)));
            n1959ContagemResultadoNotificacao_Aut = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1959ContagemResultadoNotificacao_Aut", A1959ContagemResultadoNotificacao_Aut);
         }
         cmbContagemResultadoNotificacao_Sec.Name = "CONTAGEMRESULTADONOTIFICACAO_SEC";
         cmbContagemResultadoNotificacao_Sec.WebTags = "";
         cmbContagemResultadoNotificacao_Sec.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Nenhuma", 0);
         cmbContagemResultadoNotificacao_Sec.addItem("1", "SSL e TLS", 0);
         if ( cmbContagemResultadoNotificacao_Sec.ItemCount > 0 )
         {
            A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Sec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0))), "."));
            n1960ContagemResultadoNotificacao_Sec = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)));
         }
         cmbContagemResultadoNotificacao_Logged.Name = "CONTAGEMRESULTADONOTIFICACAO_LOGGED";
         cmbContagemResultadoNotificacao_Logged.WebTags = "";
         cmbContagemResultadoNotificacao_Logged.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Sem dados", 0);
         cmbContagemResultadoNotificacao_Logged.addItem("1", "Com sucesso", 0);
         cmbContagemResultadoNotificacao_Logged.addItem("2", "Mal sucedido", 0);
         if ( cmbContagemResultadoNotificacao_Logged.ItemCount > 0 )
         {
            A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Logged.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0))), "."));
            n1961ContagemResultadoNotificacao_Logged = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1961ContagemResultadoNotificacao_Logged", StringUtil.LTrim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Notifica��es da Contagem", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultadoNotificacao_DataHora_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadonotificacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadonotificacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           long aP1_ContagemResultadoNotificacao_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContagemResultadoNotificacao_Codigo = aP1_ContagemResultadoNotificacao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContagemResultadoNotificacao_Midia = new GXCombobox();
         cmbContagemResultadoNotificacao_Aut = new GXCombobox();
         cmbContagemResultadoNotificacao_Sec = new GXCombobox();
         cmbContagemResultadoNotificacao_Logged = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContagemResultadoNotificacao_Midia.ItemCount > 0 )
         {
            A1420ContagemResultadoNotificacao_Midia = cmbContagemResultadoNotificacao_Midia.getValidValue(A1420ContagemResultadoNotificacao_Midia);
            n1420ContagemResultadoNotificacao_Midia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1420ContagemResultadoNotificacao_Midia", A1420ContagemResultadoNotificacao_Midia);
         }
         if ( cmbContagemResultadoNotificacao_Aut.ItemCount > 0 )
         {
            A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cmbContagemResultadoNotificacao_Aut.getValidValue(StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut)));
            n1959ContagemResultadoNotificacao_Aut = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1959ContagemResultadoNotificacao_Aut", A1959ContagemResultadoNotificacao_Aut);
         }
         if ( cmbContagemResultadoNotificacao_Sec.ItemCount > 0 )
         {
            A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Sec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0))), "."));
            n1960ContagemResultadoNotificacao_Sec = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)));
         }
         if ( cmbContagemResultadoNotificacao_Logged.ItemCount > 0 )
         {
            A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Logged.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0))), "."));
            n1961ContagemResultadoNotificacao_Logged = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1961ContagemResultadoNotificacao_Logged", StringUtil.LTrim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3O167( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3O167e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3O167( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadonotificacaotitle_Internalname, "Notifica��es da Contagem", "", "", lblContagemresultadonotificacaotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_3O167( true) ;
         }
         return  ;
      }

      protected void wb_table2_8_3O167e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLELEAFLEVEL_DESTINATARIOContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLELEAFLEVEL_DESTINATARIOContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table3_96_3O167( true) ;
         }
         return  ;
      }

      protected void wb_table3_96_3O167e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLELEAFLEVEL_DEMANDAContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLELEAFLEVEL_DEMANDAContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_108_3O167( true) ;
         }
         return  ;
      }

      protected void wb_table4_108_3O167e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_120_3O167( true) ;
         }
         return  ;
      }

      protected void wb_table5_120_3O167e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3O167e( true) ;
         }
         else
         {
            wb_table1_2_3O167e( false) ;
         }
      }

      protected void wb_table5_120_3O167( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_120_3O167e( true) ;
         }
         else
         {
            wb_table5_120_3O167e( false) ;
         }
      }

      protected void wb_table4_108_3O167( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableleaflevel_demanda_Internalname, tblTableleaflevel_demanda_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            Gridlevel_demandaContainer.AddObjectProperty("GridName", "Gridlevel_demanda");
            Gridlevel_demandaContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
            Gridlevel_demandaContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            Gridlevel_demandaContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            Gridlevel_demandaContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_demanda_Backcolorstyle), 1, 0, ".", "")));
            Gridlevel_demandaContainer.AddObjectProperty("CmpContext", "");
            Gridlevel_demandaContainer.AddObjectProperty("InMasterPage", "false");
            Gridlevel_demandaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_demandaColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
            Gridlevel_demandaColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0, ".", "")));
            Gridlevel_demandaContainer.AddColumnProperties(Gridlevel_demandaColumn);
            Gridlevel_demandaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_demandaColumn.AddObjectProperty("Value", A457ContagemResultado_Demanda);
            Gridlevel_demandaColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Demanda_Enabled), 5, 0, ".", "")));
            Gridlevel_demandaContainer.AddColumnProperties(Gridlevel_demandaColumn);
            Gridlevel_demandaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_demandaColumn.AddObjectProperty("Value", A493ContagemResultado_DemandaFM);
            Gridlevel_demandaColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_DemandaFM_Enabled), 5, 0, ".", "")));
            Gridlevel_demandaContainer.AddColumnProperties(Gridlevel_demandaColumn);
            Gridlevel_demandaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_demandaColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ".", "")));
            Gridlevel_demandaColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Servico_Enabled), 5, 0, ".", "")));
            Gridlevel_demandaContainer.AddColumnProperties(Gridlevel_demandaColumn);
            Gridlevel_demandaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_demandaColumn.AddObjectProperty("Value", StringUtil.RTrim( A801ContagemResultado_ServicoSigla));
            Gridlevel_demandaColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_ServicoSigla_Enabled), 5, 0, ".", "")));
            Gridlevel_demandaContainer.AddColumnProperties(Gridlevel_demandaColumn);
            Gridlevel_demandaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_demandaColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0, ".", "")));
            Gridlevel_demandaColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_PrazoInicialDias_Enabled), 5, 0, ".", "")));
            Gridlevel_demandaContainer.AddColumnProperties(Gridlevel_demandaColumn);
            Gridlevel_demandaContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_demanda_Allowselection), 1, 0, ".", "")));
            Gridlevel_demandaContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_demanda_Selectioncolor), 9, 0, ".", "")));
            Gridlevel_demandaContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_demanda_Allowhovering), 1, 0, ".", "")));
            Gridlevel_demandaContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_demanda_Hoveringcolor), 9, 0, ".", "")));
            Gridlevel_demandaContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_demanda_Allowcollapsing), 1, 0, ".", "")));
            Gridlevel_demandaContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_demanda_Collapsed), 1, 0, ".", "")));
            nGXsfl_111_idx = 0;
            if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
            {
               /* Enter key processing. */
               nBlankRcdCount169 = 5;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  /* Display confirmed (stored) records */
                  nRcdExists_169 = 1;
                  ScanStart3O169( ) ;
                  while ( RcdFound169 != 0 )
                  {
                     init_level_properties169( ) ;
                     getByPrimaryKey3O169( ) ;
                     AddRow3O169( ) ;
                     ScanNext3O169( ) ;
                  }
                  ScanEnd3O169( ) ;
                  nBlankRcdCount169 = 5;
               }
            }
            else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
            {
               /* Button check  or addlines. */
               B1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
               standaloneNotModal3O169( ) ;
               standaloneModal3O169( ) ;
               sMode169 = Gx_mode;
               while ( nGXsfl_111_idx < nRC_GXsfl_111 )
               {
                  ReadRow3O169( ) ;
                  edtContagemResultado_Codigo_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_111_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
                  edtContagemResultado_Demanda_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_111_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Demanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Demanda_Enabled), 5, 0)));
                  edtContagemResultado_DemandaFM_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_111_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DemandaFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_DemandaFM_Enabled), 5, 0)));
                  edtContagemResultado_Servico_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_SERVICO_"+sGXsfl_111_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Servico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Servico_Enabled), 5, 0)));
                  edtContagemResultado_ServicoSigla_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_111_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_ServicoSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_ServicoSigla_Enabled), 5, 0)));
                  edtContagemResultado_PrazoInicialDias_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_PRAZOINICIALDIAS_"+sGXsfl_111_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PrazoInicialDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PrazoInicialDias_Enabled), 5, 0)));
                  if ( ( nRcdExists_169 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     standaloneModal3O169( ) ;
                  }
                  SendRow3O169( ) ;
               }
               Gx_mode = sMode169;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A1962ContagemResultadoNotificacao_Destinatarios = B1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
            }
            else
            {
               /* Get or get-alike key processing. */
               nBlankRcdCount169 = 5;
               nRcdExists_169 = 1;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  ScanStart3O169( ) ;
                  while ( RcdFound169 != 0 )
                  {
                     sGXsfl_111_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_111_idx+1), 4, 0)), 4, "0");
                     SubsflControlProps_111169( ) ;
                     init_level_properties169( ) ;
                     standaloneNotModal3O169( ) ;
                     getByPrimaryKey3O169( ) ;
                     standaloneModal3O169( ) ;
                     AddRow3O169( ) ;
                     ScanNext3O169( ) ;
                  }
                  ScanEnd3O169( ) ;
               }
            }
            /* Initialize fields for 'new' records and send them. */
            if ( ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
            {
               sMode169 = Gx_mode;
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               sGXsfl_111_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_111_idx+1), 4, 0)), 4, "0");
               SubsflControlProps_111169( ) ;
               InitAll3O169( ) ;
               init_level_properties169( ) ;
               B1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
               standaloneNotModal3O169( ) ;
               standaloneModal3O169( ) ;
               nRcdExists_169 = 0;
               nIsMod_169 = 0;
               nRcdDeleted_169 = 0;
               nBlankRcdCount169 = (short)(nBlankRcdUsr169+nBlankRcdCount169);
               fRowAdded = 0;
               while ( nBlankRcdCount169 > 0 )
               {
                  AddRow3O169( ) ;
                  if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
                  {
                     fRowAdded = 1;
                     GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  nBlankRcdCount169 = (short)(nBlankRcdCount169-1);
               }
               Gx_mode = sMode169;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A1962ContagemResultadoNotificacao_Destinatarios = B1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
            }
            sStyleString = "";
            context.WriteHtmlText( "<div id=\""+"Gridlevel_demandaContainer"+"Div\" "+sStyleString+">"+"</div>") ;
            context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridlevel_demanda", Gridlevel_demandaContainer);
            if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Gridlevel_demandaContainerData", Gridlevel_demandaContainer.ToJavascriptSource());
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Gridlevel_demandaContainerData"+"V", Gridlevel_demandaContainer.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridlevel_demandaContainerData"+"V"+"\" value='"+Gridlevel_demandaContainer.GridValuesHidden()+"'/>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_108_3O167e( true) ;
         }
         else
         {
            wb_table4_108_3O167e( false) ;
         }
      }

      protected void wb_table3_96_3O167( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableleaflevel_destinatario_Internalname, tblTableleaflevel_destinatario_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            Gridlevel_destinatarioContainer.AddObjectProperty("GridName", "Gridlevel_destinatario");
            Gridlevel_destinatarioContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
            Gridlevel_destinatarioContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            Gridlevel_destinatarioContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            Gridlevel_destinatarioContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_destinatario_Backcolorstyle), 1, 0, ".", "")));
            Gridlevel_destinatarioContainer.AddObjectProperty("CmpContext", "");
            Gridlevel_destinatarioContainer.AddObjectProperty("InMasterPage", "false");
            Gridlevel_destinatarioColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_destinatarioColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1414ContagemResultadoNotificacao_DestCod), 6, 0, ".", "")));
            Gridlevel_destinatarioColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestCod_Enabled), 5, 0, ".", "")));
            Gridlevel_destinatarioContainer.AddColumnProperties(Gridlevel_destinatarioColumn);
            Gridlevel_destinatarioColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_destinatarioContainer.AddColumnProperties(Gridlevel_destinatarioColumn);
            Gridlevel_destinatarioColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_destinatarioColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1426ContagemResultadoNotificacao_DestPesCod), 6, 0, ".", "")));
            Gridlevel_destinatarioColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestPesCod_Enabled), 5, 0, ".", "")));
            Gridlevel_destinatarioContainer.AddColumnProperties(Gridlevel_destinatarioColumn);
            Gridlevel_destinatarioColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_destinatarioColumn.AddObjectProperty("Value", StringUtil.RTrim( A1421ContagemResultadoNotificacao_DestNome));
            Gridlevel_destinatarioColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestNome_Enabled), 5, 0, ".", "")));
            Gridlevel_destinatarioContainer.AddColumnProperties(Gridlevel_destinatarioColumn);
            Gridlevel_destinatarioColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_destinatarioColumn.AddObjectProperty("Value", A1419ContagemResultadoNotificacao_DestEmail);
            Gridlevel_destinatarioColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestEmail_Enabled), 5, 0, ".", "")));
            Gridlevel_destinatarioContainer.AddColumnProperties(Gridlevel_destinatarioColumn);
            Gridlevel_destinatarioContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_destinatario_Allowselection), 1, 0, ".", "")));
            Gridlevel_destinatarioContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_destinatario_Selectioncolor), 9, 0, ".", "")));
            Gridlevel_destinatarioContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_destinatario_Allowhovering), 1, 0, ".", "")));
            Gridlevel_destinatarioContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_destinatario_Hoveringcolor), 9, 0, ".", "")));
            Gridlevel_destinatarioContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_destinatario_Allowcollapsing), 1, 0, ".", "")));
            Gridlevel_destinatarioContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_destinatario_Collapsed), 1, 0, ".", "")));
            nGXsfl_99_idx = 0;
            if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
            {
               /* Enter key processing. */
               nBlankRcdCount168 = 5;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  /* Display confirmed (stored) records */
                  nRcdExists_168 = 1;
                  ScanStart3O168( ) ;
                  while ( RcdFound168 != 0 )
                  {
                     init_level_properties168( ) ;
                     getByPrimaryKey3O168( ) ;
                     AddRow3O168( ) ;
                     ScanNext3O168( ) ;
                  }
                  ScanEnd3O168( ) ;
                  nBlankRcdCount168 = 5;
               }
            }
            else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
            {
               /* Button check  or addlines. */
               B1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
               standaloneNotModal3O168( ) ;
               standaloneModal3O168( ) ;
               sMode168 = Gx_mode;
               while ( nGXsfl_99_idx < nRC_GXsfl_99 )
               {
                  ReadRow3O168( ) ;
                  edtContagemResultadoNotificacao_DestCod_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_"+sGXsfl_99_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_DestCod_Enabled), 5, 0)));
                  edtContagemResultadoNotificacao_DestPesCod_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD_"+sGXsfl_99_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestPesCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_DestPesCod_Enabled), 5, 0)));
                  edtContagemResultadoNotificacao_DestNome_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_DESTNOME_"+sGXsfl_99_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_DestNome_Enabled), 5, 0)));
                  edtContagemResultadoNotificacao_DestEmail_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL_"+sGXsfl_99_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestEmail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_DestEmail_Enabled), 5, 0)));
                  imgprompt_1413_Link = cgiGet( "PROMPT_1414_"+sGXsfl_99_idx+"Link");
                  if ( ( nRcdExists_168 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     standaloneModal3O168( ) ;
                  }
                  SendRow3O168( ) ;
               }
               Gx_mode = sMode168;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A1962ContagemResultadoNotificacao_Destinatarios = B1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
            }
            else
            {
               /* Get or get-alike key processing. */
               nBlankRcdCount168 = 5;
               nRcdExists_168 = 1;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  ScanStart3O168( ) ;
                  while ( RcdFound168 != 0 )
                  {
                     sGXsfl_99_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_99_idx+1), 4, 0)), 4, "0");
                     SubsflControlProps_99168( ) ;
                     init_level_properties168( ) ;
                     standaloneNotModal3O168( ) ;
                     getByPrimaryKey3O168( ) ;
                     standaloneModal3O168( ) ;
                     AddRow3O168( ) ;
                     ScanNext3O168( ) ;
                  }
                  ScanEnd3O168( ) ;
               }
            }
            /* Initialize fields for 'new' records and send them. */
            if ( ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
            {
               sMode168 = Gx_mode;
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               sGXsfl_99_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_99_idx+1), 4, 0)), 4, "0");
               SubsflControlProps_99168( ) ;
               InitAll3O168( ) ;
               init_level_properties168( ) ;
               B1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
               standaloneNotModal3O168( ) ;
               standaloneModal3O168( ) ;
               nRcdExists_168 = 0;
               nIsMod_168 = 0;
               nRcdDeleted_168 = 0;
               nBlankRcdCount168 = (short)(nBlankRcdUsr168+nBlankRcdCount168);
               fRowAdded = 0;
               while ( nBlankRcdCount168 > 0 )
               {
                  AddRow3O168( ) ;
                  if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
                  {
                     fRowAdded = 1;
                     GX_FocusControl = edtContagemResultadoNotificacao_DestCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  nBlankRcdCount168 = (short)(nBlankRcdCount168-1);
               }
               Gx_mode = sMode168;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A1962ContagemResultadoNotificacao_Destinatarios = B1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
            }
            sStyleString = "";
            context.WriteHtmlText( "<div id=\""+"Gridlevel_destinatarioContainer"+"Div\" "+sStyleString+">"+"</div>") ;
            context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridlevel_destinatario", Gridlevel_destinatarioContainer);
            if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Gridlevel_destinatarioContainerData", Gridlevel_destinatarioContainer.ToJavascriptSource());
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Gridlevel_destinatarioContainerData"+"V", Gridlevel_destinatarioContainer.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridlevel_destinatarioContainerData"+"V"+"\" value='"+Gridlevel_destinatarioContainer.GridValuesHidden()+"'/>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_96_3O167e( true) ;
         }
         else
         {
            wb_table3_96_3O167e( false) ;
         }
      }

      protected void wb_table2_8_3O167( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table6_16_3O167( true) ;
         }
         return  ;
      }

      protected void wb_table6_16_3O167e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_3O167e( true) ;
         }
         else
         {
            wb_table2_8_3O167e( false) ;
         }
      }

      protected void wb_table6_16_3O167( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_codigo_Internalname, "da Notifica��o", "", "", lblTextblockcontagemresultadonotificacao_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")), ((edtContagemResultadoNotificacao_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultadoNotificacao_Codigo_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "Codigo10", "right", false, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_datahora_Internalname, "da Notifica��o", "", "", lblTextblockcontagemresultadonotificacao_datahora_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoNotificacao_DataHora_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_DataHora_Internalname, context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1416ContagemResultadoNotificacao_DataHora, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_DataHora_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtContagemResultadoNotificacao_DataHora_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_ContagemResultadoNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoNotificacao_DataHora_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultadoNotificacao_DataHora_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_usucod_Internalname, "Codigo", "", "", lblTextblockcontagemresultadonotificacao_usucod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_UsuCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_UsuCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultadoNotificacao_UsuCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoNotificacao.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_1413_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_1413_Link, "", "", context.GetTheme( ), imgprompt_1413_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_usupescod_Internalname, "codigo", "", "", lblTextblockcontagemresultadonotificacao_usupescod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_UsuPesCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), 6, 0, ",", "")), ((edtContagemResultadoNotificacao_UsuPesCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_UsuPesCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultadoNotificacao_UsuPesCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_usunom_Internalname, "Remetente", "", "", lblTextblockcontagemresultadonotificacao_usunom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_UsuNom_Internalname, StringUtil.RTrim( A1422ContagemResultadoNotificacao_UsuNom), StringUtil.RTrim( context.localUtil.Format( A1422ContagemResultadoNotificacao_UsuNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_UsuNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultadoNotificacao_UsuNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_assunto_Internalname, "Assunto", "", "", lblTextblockcontagemresultadonotificacao_assunto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultadoNotificacao_Assunto_Internalname, A1417ContagemResultadoNotificacao_Assunto, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", 0, 1, edtContagemResultadoNotificacao_Assunto_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "1000", -1, "", "", -1, true, "Observacao", "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_corpoemail_Internalname, "do Email", "", "", lblTextblockcontagemresultadonotificacao_corpoemail_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultadoNotificacao_CorpoEmail_Internalname, A1418ContagemResultadoNotificacao_CorpoEmail, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", 0, 1, edtContagemResultadoNotificacao_CorpoEmail_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "8000", -1, "", "", -1, true, "", "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_midia_Internalname, "da Notifica��o", "", "", lblTextblockcontagemresultadonotificacao_midia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultadoNotificacao_Midia, cmbContagemResultadoNotificacao_Midia_Internalname, StringUtil.RTrim( A1420ContagemResultadoNotificacao_Midia), 1, cmbContagemResultadoNotificacao_Midia_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContagemResultadoNotificacao_Midia.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_ContagemResultadoNotificacao.htm");
            cmbContagemResultadoNotificacao_Midia.CurrentValue = StringUtil.RTrim( A1420ContagemResultadoNotificacao_Midia);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Midia_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Midia.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_observacao_Internalname, "da Notifica��o", "", "", lblTextblockcontagemresultadonotificacao_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONTAGEMRESULTADONOTIFICACAO_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_host_Internalname, "Host", "", "", lblTextblockcontagemresultadonotificacao_host_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_Host_Internalname, StringUtil.RTrim( A1956ContagemResultadoNotificacao_Host), StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_Host_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultadoNotificacao_Host_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_user_Internalname, "Usu�rio", "", "", lblTextblockcontagemresultadonotificacao_user_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_User_Internalname, StringUtil.RTrim( A1957ContagemResultadoNotificacao_User), StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_User_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultadoNotificacao_User_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_port_Internalname, "Porta", "", "", lblTextblockcontagemresultadonotificacao_port_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_Port_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0, ",", "")), ((edtContagemResultadoNotificacao_Port_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9")) : context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_Port_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultadoNotificacao_Port_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_aut_Internalname, "Autentica��o", "", "", lblTextblockcontagemresultadonotificacao_aut_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultadoNotificacao_Aut, cmbContagemResultadoNotificacao_Aut_Internalname, StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut), 1, cmbContagemResultadoNotificacao_Aut_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContagemResultadoNotificacao_Aut.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", "", true, "HLP_ContagemResultadoNotificacao.htm");
            cmbContagemResultadoNotificacao_Aut.CurrentValue = StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Aut_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Aut.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_sec_Internalname, "Seguran�a", "", "", lblTextblockcontagemresultadonotificacao_sec_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultadoNotificacao_Sec, cmbContagemResultadoNotificacao_Sec_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)), 1, cmbContagemResultadoNotificacao_Sec_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContagemResultadoNotificacao_Sec.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"", "", true, "HLP_ContagemResultadoNotificacao.htm");
            cmbContagemResultadoNotificacao_Sec.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Sec_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Sec.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_logged_Internalname, "Login", "", "", lblTextblockcontagemresultadonotificacao_logged_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultadoNotificacao_Logged, cmbContagemResultadoNotificacao_Logged_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)), 1, cmbContagemResultadoNotificacao_Logged_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContagemResultadoNotificacao_Logged.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", "", true, "HLP_ContagemResultadoNotificacao.htm");
            cmbContagemResultadoNotificacao_Logged.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Logged_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Logged.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_16_3O167e( true) ;
         }
         else
         {
            wb_table6_16_3O167e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113O2 */
         E113O2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1412ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
               if ( context.localUtil.VCDateTime( cgiGet( edtContagemResultadoNotificacao_DataHora_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data e Hora da Notifica��o"}), 1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoNotificacao_DataHora_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
                  n1416ContagemResultadoNotificacao_DataHora = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1416ContagemResultadoNotificacao_DataHora", context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1416ContagemResultadoNotificacao_DataHora = context.localUtil.CToT( cgiGet( edtContagemResultadoNotificacao_DataHora_Internalname));
                  n1416ContagemResultadoNotificacao_DataHora = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1416ContagemResultadoNotificacao_DataHora", context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
               }
               n1416ContagemResultadoNotificacao_DataHora = ((DateTime.MinValue==A1416ContagemResultadoNotificacao_DataHora) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_UsuCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_UsuCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADONOTIFICACAO_USUCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoNotificacao_UsuCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1413ContagemResultadoNotificacao_UsuCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1413ContagemResultadoNotificacao_UsuCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0)));
               }
               else
               {
                  A1413ContagemResultadoNotificacao_UsuCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_UsuCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1413ContagemResultadoNotificacao_UsuCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0)));
               }
               A1427ContagemResultadoNotificacao_UsuPesCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_UsuPesCod_Internalname), ",", "."));
               n1427ContagemResultadoNotificacao_UsuPesCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1427ContagemResultadoNotificacao_UsuPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), 6, 0)));
               A1422ContagemResultadoNotificacao_UsuNom = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_UsuNom_Internalname));
               n1422ContagemResultadoNotificacao_UsuNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1422ContagemResultadoNotificacao_UsuNom", A1422ContagemResultadoNotificacao_UsuNom);
               A1417ContagemResultadoNotificacao_Assunto = cgiGet( edtContagemResultadoNotificacao_Assunto_Internalname);
               n1417ContagemResultadoNotificacao_Assunto = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1417ContagemResultadoNotificacao_Assunto", A1417ContagemResultadoNotificacao_Assunto);
               n1417ContagemResultadoNotificacao_Assunto = (String.IsNullOrEmpty(StringUtil.RTrim( A1417ContagemResultadoNotificacao_Assunto)) ? true : false);
               A1418ContagemResultadoNotificacao_CorpoEmail = cgiGet( edtContagemResultadoNotificacao_CorpoEmail_Internalname);
               n1418ContagemResultadoNotificacao_CorpoEmail = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1418ContagemResultadoNotificacao_CorpoEmail", A1418ContagemResultadoNotificacao_CorpoEmail);
               n1418ContagemResultadoNotificacao_CorpoEmail = (String.IsNullOrEmpty(StringUtil.RTrim( A1418ContagemResultadoNotificacao_CorpoEmail)) ? true : false);
               cmbContagemResultadoNotificacao_Midia.Name = cmbContagemResultadoNotificacao_Midia_Internalname;
               cmbContagemResultadoNotificacao_Midia.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Midia_Internalname);
               A1420ContagemResultadoNotificacao_Midia = cgiGet( cmbContagemResultadoNotificacao_Midia_Internalname);
               n1420ContagemResultadoNotificacao_Midia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1420ContagemResultadoNotificacao_Midia", A1420ContagemResultadoNotificacao_Midia);
               n1420ContagemResultadoNotificacao_Midia = (String.IsNullOrEmpty(StringUtil.RTrim( A1420ContagemResultadoNotificacao_Midia)) ? true : false);
               A1956ContagemResultadoNotificacao_Host = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_Host_Internalname));
               n1956ContagemResultadoNotificacao_Host = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1956ContagemResultadoNotificacao_Host", A1956ContagemResultadoNotificacao_Host);
               n1956ContagemResultadoNotificacao_Host = (String.IsNullOrEmpty(StringUtil.RTrim( A1956ContagemResultadoNotificacao_Host)) ? true : false);
               A1957ContagemResultadoNotificacao_User = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_User_Internalname));
               n1957ContagemResultadoNotificacao_User = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1957ContagemResultadoNotificacao_User", A1957ContagemResultadoNotificacao_User);
               n1957ContagemResultadoNotificacao_User = (String.IsNullOrEmpty(StringUtil.RTrim( A1957ContagemResultadoNotificacao_User)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Port_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Port_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADONOTIFICACAO_PORT");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoNotificacao_Port_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1958ContagemResultadoNotificacao_Port = 0;
                  n1958ContagemResultadoNotificacao_Port = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1958ContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.Str( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0)));
               }
               else
               {
                  A1958ContagemResultadoNotificacao_Port = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Port_Internalname), ",", "."));
                  n1958ContagemResultadoNotificacao_Port = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1958ContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.Str( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0)));
               }
               n1958ContagemResultadoNotificacao_Port = ((0==A1958ContagemResultadoNotificacao_Port) ? true : false);
               cmbContagemResultadoNotificacao_Aut.Name = cmbContagemResultadoNotificacao_Aut_Internalname;
               cmbContagemResultadoNotificacao_Aut.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Aut_Internalname);
               A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cgiGet( cmbContagemResultadoNotificacao_Aut_Internalname));
               n1959ContagemResultadoNotificacao_Aut = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1959ContagemResultadoNotificacao_Aut", A1959ContagemResultadoNotificacao_Aut);
               n1959ContagemResultadoNotificacao_Aut = ((false==A1959ContagemResultadoNotificacao_Aut) ? true : false);
               cmbContagemResultadoNotificacao_Sec.Name = cmbContagemResultadoNotificacao_Sec_Internalname;
               cmbContagemResultadoNotificacao_Sec.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Sec_Internalname);
               A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cgiGet( cmbContagemResultadoNotificacao_Sec_Internalname), "."));
               n1960ContagemResultadoNotificacao_Sec = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)));
               n1960ContagemResultadoNotificacao_Sec = ((0==A1960ContagemResultadoNotificacao_Sec) ? true : false);
               cmbContagemResultadoNotificacao_Logged.Name = cmbContagemResultadoNotificacao_Logged_Internalname;
               cmbContagemResultadoNotificacao_Logged.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Logged_Internalname);
               A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cgiGet( cmbContagemResultadoNotificacao_Logged_Internalname), "."));
               n1961ContagemResultadoNotificacao_Logged = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1961ContagemResultadoNotificacao_Logged", StringUtil.LTrim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)));
               n1961ContagemResultadoNotificacao_Logged = ((0==A1961ContagemResultadoNotificacao_Logged) ? true : false);
               /* Read saved values. */
               Z1412ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( "Z1412ContagemResultadoNotificacao_Codigo"), ",", "."));
               Z1416ContagemResultadoNotificacao_DataHora = context.localUtil.CToT( cgiGet( "Z1416ContagemResultadoNotificacao_DataHora"), 0);
               n1416ContagemResultadoNotificacao_DataHora = ((DateTime.MinValue==A1416ContagemResultadoNotificacao_DataHora) ? true : false);
               Z1418ContagemResultadoNotificacao_CorpoEmail = cgiGet( "Z1418ContagemResultadoNotificacao_CorpoEmail");
               n1418ContagemResultadoNotificacao_CorpoEmail = (String.IsNullOrEmpty(StringUtil.RTrim( A1418ContagemResultadoNotificacao_CorpoEmail)) ? true : false);
               Z1420ContagemResultadoNotificacao_Midia = cgiGet( "Z1420ContagemResultadoNotificacao_Midia");
               n1420ContagemResultadoNotificacao_Midia = (String.IsNullOrEmpty(StringUtil.RTrim( A1420ContagemResultadoNotificacao_Midia)) ? true : false);
               Z1956ContagemResultadoNotificacao_Host = cgiGet( "Z1956ContagemResultadoNotificacao_Host");
               n1956ContagemResultadoNotificacao_Host = (String.IsNullOrEmpty(StringUtil.RTrim( A1956ContagemResultadoNotificacao_Host)) ? true : false);
               Z1957ContagemResultadoNotificacao_User = cgiGet( "Z1957ContagemResultadoNotificacao_User");
               n1957ContagemResultadoNotificacao_User = (String.IsNullOrEmpty(StringUtil.RTrim( A1957ContagemResultadoNotificacao_User)) ? true : false);
               Z1958ContagemResultadoNotificacao_Port = (short)(context.localUtil.CToN( cgiGet( "Z1958ContagemResultadoNotificacao_Port"), ",", "."));
               n1958ContagemResultadoNotificacao_Port = ((0==A1958ContagemResultadoNotificacao_Port) ? true : false);
               Z1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cgiGet( "Z1959ContagemResultadoNotificacao_Aut"));
               n1959ContagemResultadoNotificacao_Aut = ((false==A1959ContagemResultadoNotificacao_Aut) ? true : false);
               Z1960ContagemResultadoNotificacao_Sec = (short)(context.localUtil.CToN( cgiGet( "Z1960ContagemResultadoNotificacao_Sec"), ",", "."));
               n1960ContagemResultadoNotificacao_Sec = ((0==A1960ContagemResultadoNotificacao_Sec) ? true : false);
               Z1961ContagemResultadoNotificacao_Logged = (short)(context.localUtil.CToN( cgiGet( "Z1961ContagemResultadoNotificacao_Logged"), ",", "."));
               n1961ContagemResultadoNotificacao_Logged = ((0==A1961ContagemResultadoNotificacao_Logged) ? true : false);
               Z1413ContagemResultadoNotificacao_UsuCod = (int)(context.localUtil.CToN( cgiGet( "Z1413ContagemResultadoNotificacao_UsuCod"), ",", "."));
               Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z1966ContagemResultadoNotificacao_AreaTrabalhoCod"), ",", "."));
               n1966ContagemResultadoNotificacao_AreaTrabalhoCod = ((0==A1966ContagemResultadoNotificacao_AreaTrabalhoCod) ? true : false);
               Z1965ContagemResultadoNotificacao_ReenvioCod = (long)(context.localUtil.CToN( cgiGet( "Z1965ContagemResultadoNotificacao_ReenvioCod"), ",", "."));
               n1965ContagemResultadoNotificacao_ReenvioCod = ((0==A1965ContagemResultadoNotificacao_ReenvioCod) ? true : false);
               A1966ContagemResultadoNotificacao_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z1966ContagemResultadoNotificacao_AreaTrabalhoCod"), ",", "."));
               n1966ContagemResultadoNotificacao_AreaTrabalhoCod = false;
               n1966ContagemResultadoNotificacao_AreaTrabalhoCod = ((0==A1966ContagemResultadoNotificacao_AreaTrabalhoCod) ? true : false);
               A1965ContagemResultadoNotificacao_ReenvioCod = (long)(context.localUtil.CToN( cgiGet( "Z1965ContagemResultadoNotificacao_ReenvioCod"), ",", "."));
               n1965ContagemResultadoNotificacao_ReenvioCod = false;
               n1965ContagemResultadoNotificacao_ReenvioCod = ((0==A1965ContagemResultadoNotificacao_ReenvioCod) ? true : false);
               O1962ContagemResultadoNotificacao_Destinatarios = (int)(context.localUtil.CToN( cgiGet( "O1962ContagemResultadoNotificacao_Destinatarios"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               nRC_GXsfl_99 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_99"), ",", "."));
               nRC_GXsfl_111 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_111"), ",", "."));
               N1966ContagemResultadoNotificacao_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N1966ContagemResultadoNotificacao_AreaTrabalhoCod"), ",", "."));
               n1966ContagemResultadoNotificacao_AreaTrabalhoCod = ((0==A1966ContagemResultadoNotificacao_AreaTrabalhoCod) ? true : false);
               N1413ContagemResultadoNotificacao_UsuCod = (int)(context.localUtil.CToN( cgiGet( "N1413ContagemResultadoNotificacao_UsuCod"), ",", "."));
               N1965ContagemResultadoNotificacao_ReenvioCod = (long)(context.localUtil.CToN( cgiGet( "N1965ContagemResultadoNotificacao_ReenvioCod"), ",", "."));
               n1965ContagemResultadoNotificacao_ReenvioCod = ((0==A1965ContagemResultadoNotificacao_ReenvioCod) ? true : false);
               AV7ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( "vCONTAGEMRESULTADONOTIFICACAO_CODIGO"), ",", "."));
               AV15Insert_ContagemResultadoNotificacao_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEMRESULTADONOTIFICACAO_AREATRABALHOCOD"), ",", "."));
               A1966ContagemResultadoNotificacao_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_AREATRABALHOCOD"), ",", "."));
               n1966ContagemResultadoNotificacao_AreaTrabalhoCod = ((0==A1966ContagemResultadoNotificacao_AreaTrabalhoCod) ? true : false);
               AV11Insert_ContagemResultadoNotificacao_UsuCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEMRESULTADONOTIFICACAO_USUCOD"), ",", "."));
               AV14Insert_ContagemResultadoNotificacao_ReenvioCod = (long)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEMRESULTADONOTIFICACAO_REENVIOCOD"), ",", "."));
               A1965ContagemResultadoNotificacao_ReenvioCod = (long)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_REENVIOCOD"), ",", "."));
               n1965ContagemResultadoNotificacao_ReenvioCod = ((0==A1965ContagemResultadoNotificacao_ReenvioCod) ? true : false);
               A1415ContagemResultadoNotificacao_Observacao = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO");
               n1415ContagemResultadoNotificacao_Observacao = false;
               n1415ContagemResultadoNotificacao_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A1415ContagemResultadoNotificacao_Observacao)) ? true : false);
               A1962ContagemResultadoNotificacao_Destinatarios = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS"), ",", "."));
               A1963ContagemResultadoNotificacao_Demandas = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_DEMANDAS"), ",", "."));
               n1963ContagemResultadoNotificacao_Demandas = false;
               AV16Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               A1553ContagemResultado_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_CNTSRVCOD"), ",", "."));
               Contagemresultadonotificacao_observacao_Width = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Width");
               Contagemresultadonotificacao_observacao_Height = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Height");
               Contagemresultadonotificacao_observacao_Skin = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Skin");
               Contagemresultadonotificacao_observacao_Toolbar = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Toolbar");
               Contagemresultadonotificacao_observacao_Color = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Color"), ",", "."));
               Contagemresultadonotificacao_observacao_Enabled = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Enabled"));
               Contagemresultadonotificacao_observacao_Class = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Class");
               Contagemresultadonotificacao_observacao_Customtoolbar = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Customtoolbar");
               Contagemresultadonotificacao_observacao_Customconfiguration = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Customconfiguration");
               Contagemresultadonotificacao_observacao_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Toolbarcancollapse"));
               Contagemresultadonotificacao_observacao_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Toolbarexpanded"));
               Contagemresultadonotificacao_observacao_Buttonpressedid = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Buttonpressedid");
               Contagemresultadonotificacao_observacao_Captionvalue = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Captionvalue");
               Contagemresultadonotificacao_observacao_Captionclass = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Captionclass");
               Contagemresultadonotificacao_observacao_Captionposition = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Captionposition");
               Contagemresultadonotificacao_observacao_Coltitle = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Coltitle");
               Contagemresultadonotificacao_observacao_Coltitlefont = cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Coltitlefont");
               Contagemresultadonotificacao_observacao_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Coltitlecolor"), ",", "."));
               Contagemresultadonotificacao_observacao_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Usercontroliscolumn"));
               Contagemresultadonotificacao_observacao_Visible = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Visible"));
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               Dvpanel_tableleaflevel_destinatario_Width = cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Width");
               Dvpanel_tableleaflevel_destinatario_Height = cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Height");
               Dvpanel_tableleaflevel_destinatario_Cls = cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Cls");
               Dvpanel_tableleaflevel_destinatario_Title = cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Title");
               Dvpanel_tableleaflevel_destinatario_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Collapsible"));
               Dvpanel_tableleaflevel_destinatario_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Collapsed"));
               Dvpanel_tableleaflevel_destinatario_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Enabled"));
               Dvpanel_tableleaflevel_destinatario_Class = cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Class");
               Dvpanel_tableleaflevel_destinatario_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Autowidth"));
               Dvpanel_tableleaflevel_destinatario_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Autoheight"));
               Dvpanel_tableleaflevel_destinatario_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Showheader"));
               Dvpanel_tableleaflevel_destinatario_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Showcollapseicon"));
               Dvpanel_tableleaflevel_destinatario_Iconposition = cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Iconposition");
               Dvpanel_tableleaflevel_destinatario_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Autoscroll"));
               Dvpanel_tableleaflevel_destinatario_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Visible"));
               Dvpanel_tableleaflevel_demanda_Width = cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Width");
               Dvpanel_tableleaflevel_demanda_Height = cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Height");
               Dvpanel_tableleaflevel_demanda_Cls = cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Cls");
               Dvpanel_tableleaflevel_demanda_Title = cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Title");
               Dvpanel_tableleaflevel_demanda_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Collapsible"));
               Dvpanel_tableleaflevel_demanda_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Collapsed"));
               Dvpanel_tableleaflevel_demanda_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Enabled"));
               Dvpanel_tableleaflevel_demanda_Class = cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Class");
               Dvpanel_tableleaflevel_demanda_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Autowidth"));
               Dvpanel_tableleaflevel_demanda_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Autoheight"));
               Dvpanel_tableleaflevel_demanda_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Showheader"));
               Dvpanel_tableleaflevel_demanda_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Showcollapseicon"));
               Dvpanel_tableleaflevel_demanda_Iconposition = cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Iconposition");
               Dvpanel_tableleaflevel_demanda_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Autoscroll"));
               Dvpanel_tableleaflevel_demanda_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLELEAFLEVEL_DEMANDA_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContagemResultadoNotificacao";
               A1412ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1966ContagemResultadoNotificacao_AreaTrabalhoCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1965ContagemResultadoNotificacao_ReenvioCod), "ZZZZZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1412ContagemResultadoNotificacao_Codigo != Z1412ContagemResultadoNotificacao_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[SecurityCheckFailed value for]"+"ContagemResultadoNotificacao_Codigo:"+context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9"));
                  GXUtil.WriteLog("contagemresultadonotificacao:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contagemresultadonotificacao:[SecurityCheckFailed value for]"+"ContagemResultadoNotificacao_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A1966ContagemResultadoNotificacao_AreaTrabalhoCod), "ZZZZZ9"));
                  GXUtil.WriteLog("contagemresultadonotificacao:[SecurityCheckFailed value for]"+"ContagemResultadoNotificacao_ReenvioCod:"+context.localUtil.Format( (decimal)(A1965ContagemResultadoNotificacao_ReenvioCod), "ZZZZZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               /* Check if conditions changed and reset current page numbers */
               /* Check if conditions changed and reset current page numbers */
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1412ContagemResultadoNotificacao_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode167 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode167;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound167 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3O0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTAGEMRESULTADONOTIFICACAO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContagemResultadoNotificacao_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113O2 */
                           E113O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123O2 */
                           E123O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123O2 */
            E123O2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3O167( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3O167( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3O0( )
      {
         BeforeValidate3O167( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3O167( ) ;
            }
            else
            {
               CheckExtendedTable3O167( ) ;
               CloseExtendedTableCursors3O167( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode167 = Gx_mode;
            CONFIRM_3O168( ) ;
            if ( AnyError == 0 )
            {
               CONFIRM_3O169( ) ;
               if ( AnyError == 0 )
               {
                  /* Restore parent mode. */
                  Gx_mode = sMode167;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  IsConfirmed = 1;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
               }
            }
            /* Restore parent mode. */
            Gx_mode = sMode167;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void CONFIRM_3O169( )
      {
         nGXsfl_111_idx = 0;
         while ( nGXsfl_111_idx < nRC_GXsfl_111 )
         {
            ReadRow3O169( ) ;
            if ( ( nRcdExists_169 != 0 ) || ( nIsMod_169 != 0 ) )
            {
               GetKey3O169( ) ;
               if ( ( nRcdExists_169 == 0 ) && ( nRcdDeleted_169 == 0 ) )
               {
                  if ( RcdFound169 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     BeforeValidate3O169( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable3O169( ) ;
                        CloseExtendedTableCursors3O169( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                     }
                  }
                  else
                  {
                     GXCCtl = "CONTAGEMRESULTADO_CODIGO_" + sGXsfl_111_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound169 != 0 )
                  {
                     if ( nRcdDeleted_169 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        getByPrimaryKey3O169( ) ;
                        Load3O169( ) ;
                        BeforeValidate3O169( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls3O169( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_169 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           BeforeValidate3O169( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable3O169( ) ;
                              CloseExtendedTableCursors3O169( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_169 == 0 )
                     {
                        GXCCtl = "CONTAGEMRESULTADO_CODIGO_" + sGXsfl_111_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtContagemResultado_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultado_Demanda_Internalname, A457ContagemResultado_Demanda) ;
            ChangePostValue( edtContagemResultado_DemandaFM_Internalname, A493ContagemResultado_DemandaFM) ;
            ChangePostValue( edtContagemResultado_Servico_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultado_ServicoSigla_Internalname, StringUtil.RTrim( A801ContagemResultado_ServicoSigla)) ;
            ChangePostValue( edtContagemResultado_PrazoInicialDias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z456ContagemResultado_Codigo_"+sGXsfl_111_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z456ContagemResultado_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_169_"+sGXsfl_111_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_169), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_169_"+sGXsfl_111_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_169), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_169_"+sGXsfl_111_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_169), 4, 0, ",", ""))) ;
            if ( nIsMod_169 != 0 )
            {
               ChangePostValue( "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Demanda_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_DemandaFM_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADO_SERVICO_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Servico_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_ServicoSigla_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADO_PRAZOINICIALDIAS_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_PrazoInicialDias_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* Using cursor T003O8 */
         pr_default.execute(5, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            A1963ContagemResultadoNotificacao_Demandas = T003O8_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = T003O8_n1963ContagemResultadoNotificacao_Demandas[0];
         }
         else
         {
            A1963ContagemResultadoNotificacao_Demandas = 0;
            n1963ContagemResultadoNotificacao_Demandas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1963ContagemResultadoNotificacao_Demandas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1963ContagemResultadoNotificacao_Demandas), 6, 0)));
         }
         /* End of After( level) rules */
      }

      protected void CONFIRM_3O168( )
      {
         s1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
         nGXsfl_99_idx = 0;
         while ( nGXsfl_99_idx < nRC_GXsfl_99 )
         {
            ReadRow3O168( ) ;
            if ( ( nRcdExists_168 != 0 ) || ( nIsMod_168 != 0 ) )
            {
               GetKey3O168( ) ;
               if ( ( nRcdExists_168 == 0 ) && ( nRcdDeleted_168 == 0 ) )
               {
                  if ( RcdFound168 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     BeforeValidate3O168( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable3O168( ) ;
                        CloseExtendedTableCursors3O168( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                        O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
                        n1962ContagemResultadoNotificacao_Destinatarios = false;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
                     }
                  }
                  else
                  {
                     GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_" + sGXsfl_99_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoNotificacao_DestCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound168 != 0 )
                  {
                     if ( nRcdDeleted_168 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        getByPrimaryKey3O168( ) ;
                        Load3O168( ) ;
                        BeforeValidate3O168( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls3O168( ) ;
                           O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
                           n1962ContagemResultadoNotificacao_Destinatarios = false;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
                        }
                     }
                     else
                     {
                        if ( nIsMod_168 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           BeforeValidate3O168( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable3O168( ) ;
                              CloseExtendedTableCursors3O168( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                              O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
                              n1962ContagemResultadoNotificacao_Destinatarios = false;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_168 == 0 )
                     {
                        GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_" + sGXsfl_99_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtContagemResultadoNotificacao_DestCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtContagemResultadoNotificacao_DestCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1414ContagemResultadoNotificacao_DestCod), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultadoNotificacao_DestPesCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1426ContagemResultadoNotificacao_DestPesCod), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultadoNotificacao_DestNome_Internalname, StringUtil.RTrim( A1421ContagemResultadoNotificacao_DestNome)) ;
            ChangePostValue( edtContagemResultadoNotificacao_DestEmail_Internalname, A1419ContagemResultadoNotificacao_DestEmail) ;
            ChangePostValue( "ZT_"+"Z1414ContagemResultadoNotificacao_DestCod_"+sGXsfl_99_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1414ContagemResultadoNotificacao_DestCod), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1419ContagemResultadoNotificacao_DestEmail_"+sGXsfl_99_idx, Z1419ContagemResultadoNotificacao_DestEmail) ;
            ChangePostValue( "nRcdDeleted_168_"+sGXsfl_99_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_168), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_168_"+sGXsfl_99_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_168), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_168_"+sGXsfl_99_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_168), 4, 0, ",", ""))) ;
            if ( nIsMod_168 != 0 )
            {
               ChangePostValue( "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_"+sGXsfl_99_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestCod_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD_"+sGXsfl_99_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestPesCod_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADONOTIFICACAO_DESTNOME_"+sGXsfl_99_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestNome_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL_"+sGXsfl_99_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestEmail_Enabled), 5, 0, ".", ""))) ;
            }
         }
         O1962ContagemResultadoNotificacao_Destinatarios = s1962ContagemResultadoNotificacao_Destinatarios;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption3O0( )
      {
      }

      protected void E113O2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV16Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV17GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            while ( AV17GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV17GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ContagemResultadoNotificacao_AreaTrabalhoCod") == 0 )
               {
                  AV15Insert_ContagemResultadoNotificacao_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Insert_ContagemResultadoNotificacao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Insert_ContagemResultadoNotificacao_AreaTrabalhoCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ContagemResultadoNotificacao_UsuCod") == 0 )
               {
                  AV11Insert_ContagemResultadoNotificacao_UsuCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ContagemResultadoNotificacao_UsuCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ContagemResultadoNotificacao_UsuCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ContagemResultadoNotificacao_ReenvioCod") == 0 )
               {
                  AV14Insert_ContagemResultadoNotificacao_ReenvioCod = (long)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Insert_ContagemResultadoNotificacao_ReenvioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Insert_ContagemResultadoNotificacao_ReenvioCod), 10, 0)));
               }
               AV17GXV1 = (int)(AV17GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            }
         }
      }

      protected void E123O2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontagemresultadonotificacao.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM3O167( short GX_JID )
      {
         if ( ( GX_JID == 16 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1416ContagemResultadoNotificacao_DataHora = T003O14_A1416ContagemResultadoNotificacao_DataHora[0];
               Z1418ContagemResultadoNotificacao_CorpoEmail = T003O14_A1418ContagemResultadoNotificacao_CorpoEmail[0];
               Z1420ContagemResultadoNotificacao_Midia = T003O14_A1420ContagemResultadoNotificacao_Midia[0];
               Z1956ContagemResultadoNotificacao_Host = T003O14_A1956ContagemResultadoNotificacao_Host[0];
               Z1957ContagemResultadoNotificacao_User = T003O14_A1957ContagemResultadoNotificacao_User[0];
               Z1958ContagemResultadoNotificacao_Port = T003O14_A1958ContagemResultadoNotificacao_Port[0];
               Z1959ContagemResultadoNotificacao_Aut = T003O14_A1959ContagemResultadoNotificacao_Aut[0];
               Z1960ContagemResultadoNotificacao_Sec = T003O14_A1960ContagemResultadoNotificacao_Sec[0];
               Z1961ContagemResultadoNotificacao_Logged = T003O14_A1961ContagemResultadoNotificacao_Logged[0];
               Z1413ContagemResultadoNotificacao_UsuCod = T003O14_A1413ContagemResultadoNotificacao_UsuCod[0];
               Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = T003O14_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
               Z1965ContagemResultadoNotificacao_ReenvioCod = T003O14_A1965ContagemResultadoNotificacao_ReenvioCod[0];
            }
            else
            {
               Z1416ContagemResultadoNotificacao_DataHora = A1416ContagemResultadoNotificacao_DataHora;
               Z1418ContagemResultadoNotificacao_CorpoEmail = A1418ContagemResultadoNotificacao_CorpoEmail;
               Z1420ContagemResultadoNotificacao_Midia = A1420ContagemResultadoNotificacao_Midia;
               Z1956ContagemResultadoNotificacao_Host = A1956ContagemResultadoNotificacao_Host;
               Z1957ContagemResultadoNotificacao_User = A1957ContagemResultadoNotificacao_User;
               Z1958ContagemResultadoNotificacao_Port = A1958ContagemResultadoNotificacao_Port;
               Z1959ContagemResultadoNotificacao_Aut = A1959ContagemResultadoNotificacao_Aut;
               Z1960ContagemResultadoNotificacao_Sec = A1960ContagemResultadoNotificacao_Sec;
               Z1961ContagemResultadoNotificacao_Logged = A1961ContagemResultadoNotificacao_Logged;
               Z1413ContagemResultadoNotificacao_UsuCod = A1413ContagemResultadoNotificacao_UsuCod;
               Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = A1966ContagemResultadoNotificacao_AreaTrabalhoCod;
               Z1965ContagemResultadoNotificacao_ReenvioCod = A1965ContagemResultadoNotificacao_ReenvioCod;
            }
         }
         if ( GX_JID == -16 )
         {
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
            Z1416ContagemResultadoNotificacao_DataHora = A1416ContagemResultadoNotificacao_DataHora;
            Z1417ContagemResultadoNotificacao_Assunto = A1417ContagemResultadoNotificacao_Assunto;
            Z1418ContagemResultadoNotificacao_CorpoEmail = A1418ContagemResultadoNotificacao_CorpoEmail;
            Z1420ContagemResultadoNotificacao_Midia = A1420ContagemResultadoNotificacao_Midia;
            Z1415ContagemResultadoNotificacao_Observacao = A1415ContagemResultadoNotificacao_Observacao;
            Z1956ContagemResultadoNotificacao_Host = A1956ContagemResultadoNotificacao_Host;
            Z1957ContagemResultadoNotificacao_User = A1957ContagemResultadoNotificacao_User;
            Z1958ContagemResultadoNotificacao_Port = A1958ContagemResultadoNotificacao_Port;
            Z1959ContagemResultadoNotificacao_Aut = A1959ContagemResultadoNotificacao_Aut;
            Z1960ContagemResultadoNotificacao_Sec = A1960ContagemResultadoNotificacao_Sec;
            Z1961ContagemResultadoNotificacao_Logged = A1961ContagemResultadoNotificacao_Logged;
            Z1413ContagemResultadoNotificacao_UsuCod = A1413ContagemResultadoNotificacao_UsuCod;
            Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = A1966ContagemResultadoNotificacao_AreaTrabalhoCod;
            Z1965ContagemResultadoNotificacao_ReenvioCod = A1965ContagemResultadoNotificacao_ReenvioCod;
            Z1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            Z1963ContagemResultadoNotificacao_Demandas = A1963ContagemResultadoNotificacao_Demandas;
            Z1427ContagemResultadoNotificacao_UsuPesCod = A1427ContagemResultadoNotificacao_UsuPesCod;
            Z1422ContagemResultadoNotificacao_UsuNom = A1422ContagemResultadoNotificacao_UsuNom;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContagemResultadoNotificacao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_Codigo_Enabled), 5, 0)));
         AV16Pgmname = "ContagemResultadoNotificacao";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Pgmname", AV16Pgmname);
         imgprompt_1413_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptusuario.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CONTAGEMRESULTADONOTIFICACAO_USUCOD"+"'), id:'"+"CONTAGEMRESULTADONOTIFICACAO_USUCOD"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"CONTAGEMRESULTADONOTIFICACAO_USUNOM"+"'), id:'"+"CONTAGEMRESULTADONOTIFICACAO_USUNOM"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         edtContagemResultadoNotificacao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContagemResultadoNotificacao_Codigo) )
         {
            A1412ContagemResultadoNotificacao_Codigo = AV7ContagemResultadoNotificacao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContagemResultadoNotificacao_UsuCod) )
         {
            edtContagemResultadoNotificacao_UsuCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_UsuCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_UsuCod_Enabled), 5, 0)));
         }
         else
         {
            edtContagemResultadoNotificacao_UsuCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_UsuCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_UsuCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_ContagemResultadoNotificacao_ReenvioCod) )
         {
            A1965ContagemResultadoNotificacao_ReenvioCod = AV14Insert_ContagemResultadoNotificacao_ReenvioCod;
            n1965ContagemResultadoNotificacao_ReenvioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1965ContagemResultadoNotificacao_ReenvioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1965ContagemResultadoNotificacao_ReenvioCod), 10, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContagemResultadoNotificacao_UsuCod) )
         {
            A1413ContagemResultadoNotificacao_UsuCod = AV11Insert_ContagemResultadoNotificacao_UsuCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1413ContagemResultadoNotificacao_UsuCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV15Insert_ContagemResultadoNotificacao_AreaTrabalhoCod) )
         {
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = AV15Insert_ContagemResultadoNotificacao_AreaTrabalhoCod;
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1966ContagemResultadoNotificacao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1966ContagemResultadoNotificacao_AreaTrabalhoCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T003O20 */
            pr_default.execute(16, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               A1962ContagemResultadoNotificacao_Destinatarios = T003O20_A1962ContagemResultadoNotificacao_Destinatarios[0];
               n1962ContagemResultadoNotificacao_Destinatarios = T003O20_n1962ContagemResultadoNotificacao_Destinatarios[0];
            }
            else
            {
               A1962ContagemResultadoNotificacao_Destinatarios = 0;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
            }
            O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
            n1962ContagemResultadoNotificacao_Destinatarios = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
            pr_default.close(16);
            /* Using cursor T003O8 */
            pr_default.execute(5, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
            if ( (pr_default.getStatus(5) != 101) )
            {
               A1963ContagemResultadoNotificacao_Demandas = T003O8_A1963ContagemResultadoNotificacao_Demandas[0];
               n1963ContagemResultadoNotificacao_Demandas = T003O8_n1963ContagemResultadoNotificacao_Demandas[0];
            }
            else
            {
               A1963ContagemResultadoNotificacao_Demandas = 0;
               n1963ContagemResultadoNotificacao_Demandas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1963ContagemResultadoNotificacao_Demandas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1963ContagemResultadoNotificacao_Demandas), 6, 0)));
            }
            pr_default.close(5);
            /* Using cursor T003O15 */
            pr_default.execute(12, new Object[] {A1413ContagemResultadoNotificacao_UsuCod});
            A1427ContagemResultadoNotificacao_UsuPesCod = T003O15_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1427ContagemResultadoNotificacao_UsuPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), 6, 0)));
            n1427ContagemResultadoNotificacao_UsuPesCod = T003O15_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            pr_default.close(12);
            /* Using cursor T003O18 */
            pr_default.execute(15, new Object[] {n1427ContagemResultadoNotificacao_UsuPesCod, A1427ContagemResultadoNotificacao_UsuPesCod});
            A1422ContagemResultadoNotificacao_UsuNom = T003O18_A1422ContagemResultadoNotificacao_UsuNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1422ContagemResultadoNotificacao_UsuNom", A1422ContagemResultadoNotificacao_UsuNom);
            n1422ContagemResultadoNotificacao_UsuNom = T003O18_n1422ContagemResultadoNotificacao_UsuNom[0];
            pr_default.close(15);
         }
      }

      protected void Load3O167( )
      {
         /* Using cursor T003O23 */
         pr_default.execute(17, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound167 = 1;
            A1416ContagemResultadoNotificacao_DataHora = T003O23_A1416ContagemResultadoNotificacao_DataHora[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1416ContagemResultadoNotificacao_DataHora", context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
            n1416ContagemResultadoNotificacao_DataHora = T003O23_n1416ContagemResultadoNotificacao_DataHora[0];
            A1422ContagemResultadoNotificacao_UsuNom = T003O23_A1422ContagemResultadoNotificacao_UsuNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1422ContagemResultadoNotificacao_UsuNom", A1422ContagemResultadoNotificacao_UsuNom);
            n1422ContagemResultadoNotificacao_UsuNom = T003O23_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1417ContagemResultadoNotificacao_Assunto = T003O23_A1417ContagemResultadoNotificacao_Assunto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1417ContagemResultadoNotificacao_Assunto", A1417ContagemResultadoNotificacao_Assunto);
            n1417ContagemResultadoNotificacao_Assunto = T003O23_n1417ContagemResultadoNotificacao_Assunto[0];
            A1418ContagemResultadoNotificacao_CorpoEmail = T003O23_A1418ContagemResultadoNotificacao_CorpoEmail[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1418ContagemResultadoNotificacao_CorpoEmail", A1418ContagemResultadoNotificacao_CorpoEmail);
            n1418ContagemResultadoNotificacao_CorpoEmail = T003O23_n1418ContagemResultadoNotificacao_CorpoEmail[0];
            A1420ContagemResultadoNotificacao_Midia = T003O23_A1420ContagemResultadoNotificacao_Midia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1420ContagemResultadoNotificacao_Midia", A1420ContagemResultadoNotificacao_Midia);
            n1420ContagemResultadoNotificacao_Midia = T003O23_n1420ContagemResultadoNotificacao_Midia[0];
            A1415ContagemResultadoNotificacao_Observacao = T003O23_A1415ContagemResultadoNotificacao_Observacao[0];
            n1415ContagemResultadoNotificacao_Observacao = T003O23_n1415ContagemResultadoNotificacao_Observacao[0];
            A1956ContagemResultadoNotificacao_Host = T003O23_A1956ContagemResultadoNotificacao_Host[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1956ContagemResultadoNotificacao_Host", A1956ContagemResultadoNotificacao_Host);
            n1956ContagemResultadoNotificacao_Host = T003O23_n1956ContagemResultadoNotificacao_Host[0];
            A1957ContagemResultadoNotificacao_User = T003O23_A1957ContagemResultadoNotificacao_User[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1957ContagemResultadoNotificacao_User", A1957ContagemResultadoNotificacao_User);
            n1957ContagemResultadoNotificacao_User = T003O23_n1957ContagemResultadoNotificacao_User[0];
            A1958ContagemResultadoNotificacao_Port = T003O23_A1958ContagemResultadoNotificacao_Port[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1958ContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.Str( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0)));
            n1958ContagemResultadoNotificacao_Port = T003O23_n1958ContagemResultadoNotificacao_Port[0];
            A1959ContagemResultadoNotificacao_Aut = T003O23_A1959ContagemResultadoNotificacao_Aut[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1959ContagemResultadoNotificacao_Aut", A1959ContagemResultadoNotificacao_Aut);
            n1959ContagemResultadoNotificacao_Aut = T003O23_n1959ContagemResultadoNotificacao_Aut[0];
            A1960ContagemResultadoNotificacao_Sec = T003O23_A1960ContagemResultadoNotificacao_Sec[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)));
            n1960ContagemResultadoNotificacao_Sec = T003O23_n1960ContagemResultadoNotificacao_Sec[0];
            A1961ContagemResultadoNotificacao_Logged = T003O23_A1961ContagemResultadoNotificacao_Logged[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1961ContagemResultadoNotificacao_Logged", StringUtil.LTrim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)));
            n1961ContagemResultadoNotificacao_Logged = T003O23_n1961ContagemResultadoNotificacao_Logged[0];
            A1413ContagemResultadoNotificacao_UsuCod = T003O23_A1413ContagemResultadoNotificacao_UsuCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1413ContagemResultadoNotificacao_UsuCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0)));
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = T003O23_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = T003O23_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1965ContagemResultadoNotificacao_ReenvioCod = T003O23_A1965ContagemResultadoNotificacao_ReenvioCod[0];
            n1965ContagemResultadoNotificacao_ReenvioCod = T003O23_n1965ContagemResultadoNotificacao_ReenvioCod[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = T003O23_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1427ContagemResultadoNotificacao_UsuPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), 6, 0)));
            n1427ContagemResultadoNotificacao_UsuPesCod = T003O23_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1962ContagemResultadoNotificacao_Destinatarios = T003O23_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = T003O23_n1962ContagemResultadoNotificacao_Destinatarios[0];
            A1963ContagemResultadoNotificacao_Demandas = T003O23_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = T003O23_n1963ContagemResultadoNotificacao_Demandas[0];
            ZM3O167( -16) ;
         }
         pr_default.close(17);
         OnLoadActions3O167( ) ;
      }

      protected void OnLoadActions3O167( )
      {
         O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
      }

      protected void CheckExtendedTable3O167( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T003O20 */
         pr_default.execute(16, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(16) != 101) )
         {
            A1962ContagemResultadoNotificacao_Destinatarios = T003O20_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = T003O20_n1962ContagemResultadoNotificacao_Destinatarios[0];
         }
         else
         {
            A1962ContagemResultadoNotificacao_Destinatarios = 0;
            n1962ContagemResultadoNotificacao_Destinatarios = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
         }
         pr_default.close(16);
         /* Using cursor T003O8 */
         pr_default.execute(5, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            A1963ContagemResultadoNotificacao_Demandas = T003O8_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = T003O8_n1963ContagemResultadoNotificacao_Demandas[0];
         }
         else
         {
            A1963ContagemResultadoNotificacao_Demandas = 0;
            n1963ContagemResultadoNotificacao_Demandas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1963ContagemResultadoNotificacao_Demandas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1963ContagemResultadoNotificacao_Demandas), 6, 0)));
         }
         pr_default.close(5);
         if ( ! ( (DateTime.MinValue==A1416ContagemResultadoNotificacao_DataHora) || ( A1416ContagemResultadoNotificacao_DataHora >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data e Hora da Notifica��o fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNotificacao_DataHora_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T003O15 */
         pr_default.execute(12, new Object[] {A1413ContagemResultadoNotificacao_UsuCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usu�rio Respons�vel pela Gera��o da Notifica��o'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONOTIFICACAO_USUCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNotificacao_UsuCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1427ContagemResultadoNotificacao_UsuPesCod = T003O15_A1427ContagemResultadoNotificacao_UsuPesCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1427ContagemResultadoNotificacao_UsuPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), 6, 0)));
         n1427ContagemResultadoNotificacao_UsuPesCod = T003O15_n1427ContagemResultadoNotificacao_UsuPesCod[0];
         pr_default.close(12);
         /* Using cursor T003O18 */
         pr_default.execute(15, new Object[] {n1427ContagemResultadoNotificacao_UsuPesCod, A1427ContagemResultadoNotificacao_UsuPesCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1422ContagemResultadoNotificacao_UsuNom = T003O18_A1422ContagemResultadoNotificacao_UsuNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1422ContagemResultadoNotificacao_UsuNom", A1422ContagemResultadoNotificacao_UsuNom);
         n1422ContagemResultadoNotificacao_UsuNom = T003O18_n1422ContagemResultadoNotificacao_UsuNom[0];
         pr_default.close(15);
         if ( ! ( ( StringUtil.StrCmp(A1420ContagemResultadoNotificacao_Midia, "EML") == 0 ) || ( StringUtil.StrCmp(A1420ContagemResultadoNotificacao_Midia, "SMS") == 0 ) || ( StringUtil.StrCmp(A1420ContagemResultadoNotificacao_Midia, "TEL") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1420ContagemResultadoNotificacao_Midia)) ) )
         {
            GX_msglist.addItem("Campo M�dia da Notifica��o fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADONOTIFICACAO_MIDIA");
            AnyError = 1;
            GX_FocusControl = cmbContagemResultadoNotificacao_Midia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T003O16 */
         pr_default.execute(13, new Object[] {n1966ContagemResultadoNotificacao_AreaTrabalhoCod, A1966ContagemResultadoNotificacao_AreaTrabalhoCod});
         if ( (pr_default.getStatus(13) == 101) )
         {
            if ( ! ( (0==A1966ContagemResultadoNotificacao_AreaTrabalhoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Notificacao_AreaTrabalhoCod'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         pr_default.close(13);
         /* Using cursor T003O17 */
         pr_default.execute(14, new Object[] {n1965ContagemResultadoNotificacao_ReenvioCod, A1965ContagemResultadoNotificacao_ReenvioCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            if ( ! ( (0==A1965ContagemResultadoNotificacao_ReenvioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Notificacao_ReenvioCod'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         pr_default.close(14);
      }

      protected void CloseExtendedTableCursors3O167( )
      {
         pr_default.close(16);
         pr_default.close(5);
         pr_default.close(12);
         pr_default.close(15);
         pr_default.close(13);
         pr_default.close(14);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_21( long A1412ContagemResultadoNotificacao_Codigo )
      {
         /* Using cursor T003O25 */
         pr_default.execute(18, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(18) != 101) )
         {
            A1962ContagemResultadoNotificacao_Destinatarios = T003O25_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = T003O25_n1962ContagemResultadoNotificacao_Destinatarios[0];
         }
         else
         {
            A1962ContagemResultadoNotificacao_Destinatarios = 0;
            n1962ContagemResultadoNotificacao_Destinatarios = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(18) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(18);
      }

      protected void gxLoad_22( long A1412ContagemResultadoNotificacao_Codigo )
      {
         /* Using cursor T003O27 */
         pr_default.execute(19, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(19) != 101) )
         {
            A1963ContagemResultadoNotificacao_Demandas = T003O27_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = T003O27_n1963ContagemResultadoNotificacao_Demandas[0];
         }
         else
         {
            A1963ContagemResultadoNotificacao_Demandas = 0;
            n1963ContagemResultadoNotificacao_Demandas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1963ContagemResultadoNotificacao_Demandas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1963ContagemResultadoNotificacao_Demandas), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1963ContagemResultadoNotificacao_Demandas), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(19) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(19);
      }

      protected void gxLoad_17( int A1413ContagemResultadoNotificacao_UsuCod )
      {
         /* Using cursor T003O28 */
         pr_default.execute(20, new Object[] {A1413ContagemResultadoNotificacao_UsuCod});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usu�rio Respons�vel pela Gera��o da Notifica��o'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONOTIFICACAO_USUCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNotificacao_UsuCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1427ContagemResultadoNotificacao_UsuPesCod = T003O28_A1427ContagemResultadoNotificacao_UsuPesCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1427ContagemResultadoNotificacao_UsuPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), 6, 0)));
         n1427ContagemResultadoNotificacao_UsuPesCod = T003O28_n1427ContagemResultadoNotificacao_UsuPesCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(20) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(20);
      }

      protected void gxLoad_20( int A1427ContagemResultadoNotificacao_UsuPesCod )
      {
         /* Using cursor T003O29 */
         pr_default.execute(21, new Object[] {n1427ContagemResultadoNotificacao_UsuPesCod, A1427ContagemResultadoNotificacao_UsuPesCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1422ContagemResultadoNotificacao_UsuNom = T003O29_A1422ContagemResultadoNotificacao_UsuNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1422ContagemResultadoNotificacao_UsuNom", A1422ContagemResultadoNotificacao_UsuNom);
         n1422ContagemResultadoNotificacao_UsuNom = T003O29_n1422ContagemResultadoNotificacao_UsuNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1422ContagemResultadoNotificacao_UsuNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(21) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(21);
      }

      protected void gxLoad_18( int A1966ContagemResultadoNotificacao_AreaTrabalhoCod )
      {
         /* Using cursor T003O30 */
         pr_default.execute(22, new Object[] {n1966ContagemResultadoNotificacao_AreaTrabalhoCod, A1966ContagemResultadoNotificacao_AreaTrabalhoCod});
         if ( (pr_default.getStatus(22) == 101) )
         {
            if ( ! ( (0==A1966ContagemResultadoNotificacao_AreaTrabalhoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Notificacao_AreaTrabalhoCod'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(22) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(22);
      }

      protected void gxLoad_19( long A1965ContagemResultadoNotificacao_ReenvioCod )
      {
         /* Using cursor T003O31 */
         pr_default.execute(23, new Object[] {n1965ContagemResultadoNotificacao_ReenvioCod, A1965ContagemResultadoNotificacao_ReenvioCod});
         if ( (pr_default.getStatus(23) == 101) )
         {
            if ( ! ( (0==A1965ContagemResultadoNotificacao_ReenvioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Notificacao_ReenvioCod'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(23) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(23);
      }

      protected void GetKey3O167( )
      {
         /* Using cursor T003O32 */
         pr_default.execute(24, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(24) != 101) )
         {
            RcdFound167 = 1;
         }
         else
         {
            RcdFound167 = 0;
         }
         pr_default.close(24);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003O14 */
         pr_default.execute(11, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            ZM3O167( 16) ;
            RcdFound167 = 1;
            A1412ContagemResultadoNotificacao_Codigo = T003O14_A1412ContagemResultadoNotificacao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
            A1416ContagemResultadoNotificacao_DataHora = T003O14_A1416ContagemResultadoNotificacao_DataHora[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1416ContagemResultadoNotificacao_DataHora", context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
            n1416ContagemResultadoNotificacao_DataHora = T003O14_n1416ContagemResultadoNotificacao_DataHora[0];
            A1417ContagemResultadoNotificacao_Assunto = T003O14_A1417ContagemResultadoNotificacao_Assunto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1417ContagemResultadoNotificacao_Assunto", A1417ContagemResultadoNotificacao_Assunto);
            n1417ContagemResultadoNotificacao_Assunto = T003O14_n1417ContagemResultadoNotificacao_Assunto[0];
            A1418ContagemResultadoNotificacao_CorpoEmail = T003O14_A1418ContagemResultadoNotificacao_CorpoEmail[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1418ContagemResultadoNotificacao_CorpoEmail", A1418ContagemResultadoNotificacao_CorpoEmail);
            n1418ContagemResultadoNotificacao_CorpoEmail = T003O14_n1418ContagemResultadoNotificacao_CorpoEmail[0];
            A1420ContagemResultadoNotificacao_Midia = T003O14_A1420ContagemResultadoNotificacao_Midia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1420ContagemResultadoNotificacao_Midia", A1420ContagemResultadoNotificacao_Midia);
            n1420ContagemResultadoNotificacao_Midia = T003O14_n1420ContagemResultadoNotificacao_Midia[0];
            A1415ContagemResultadoNotificacao_Observacao = T003O14_A1415ContagemResultadoNotificacao_Observacao[0];
            n1415ContagemResultadoNotificacao_Observacao = T003O14_n1415ContagemResultadoNotificacao_Observacao[0];
            A1956ContagemResultadoNotificacao_Host = T003O14_A1956ContagemResultadoNotificacao_Host[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1956ContagemResultadoNotificacao_Host", A1956ContagemResultadoNotificacao_Host);
            n1956ContagemResultadoNotificacao_Host = T003O14_n1956ContagemResultadoNotificacao_Host[0];
            A1957ContagemResultadoNotificacao_User = T003O14_A1957ContagemResultadoNotificacao_User[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1957ContagemResultadoNotificacao_User", A1957ContagemResultadoNotificacao_User);
            n1957ContagemResultadoNotificacao_User = T003O14_n1957ContagemResultadoNotificacao_User[0];
            A1958ContagemResultadoNotificacao_Port = T003O14_A1958ContagemResultadoNotificacao_Port[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1958ContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.Str( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0)));
            n1958ContagemResultadoNotificacao_Port = T003O14_n1958ContagemResultadoNotificacao_Port[0];
            A1959ContagemResultadoNotificacao_Aut = T003O14_A1959ContagemResultadoNotificacao_Aut[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1959ContagemResultadoNotificacao_Aut", A1959ContagemResultadoNotificacao_Aut);
            n1959ContagemResultadoNotificacao_Aut = T003O14_n1959ContagemResultadoNotificacao_Aut[0];
            A1960ContagemResultadoNotificacao_Sec = T003O14_A1960ContagemResultadoNotificacao_Sec[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)));
            n1960ContagemResultadoNotificacao_Sec = T003O14_n1960ContagemResultadoNotificacao_Sec[0];
            A1961ContagemResultadoNotificacao_Logged = T003O14_A1961ContagemResultadoNotificacao_Logged[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1961ContagemResultadoNotificacao_Logged", StringUtil.LTrim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)));
            n1961ContagemResultadoNotificacao_Logged = T003O14_n1961ContagemResultadoNotificacao_Logged[0];
            A1413ContagemResultadoNotificacao_UsuCod = T003O14_A1413ContagemResultadoNotificacao_UsuCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1413ContagemResultadoNotificacao_UsuCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0)));
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = T003O14_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = T003O14_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1965ContagemResultadoNotificacao_ReenvioCod = T003O14_A1965ContagemResultadoNotificacao_ReenvioCod[0];
            n1965ContagemResultadoNotificacao_ReenvioCod = T003O14_n1965ContagemResultadoNotificacao_ReenvioCod[0];
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
            sMode167 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3O167( ) ;
            if ( AnyError == 1 )
            {
               RcdFound167 = 0;
               InitializeNonKey3O167( ) ;
            }
            Gx_mode = sMode167;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound167 = 0;
            InitializeNonKey3O167( ) ;
            sMode167 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode167;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(11);
      }

      protected void getEqualNoModal( )
      {
         GetKey3O167( ) ;
         if ( RcdFound167 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound167 = 0;
         /* Using cursor T003O33 */
         pr_default.execute(25, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(25) != 101) )
         {
            while ( (pr_default.getStatus(25) != 101) && ( ( T003O33_A1412ContagemResultadoNotificacao_Codigo[0] < A1412ContagemResultadoNotificacao_Codigo ) ) )
            {
               pr_default.readNext(25);
            }
            if ( (pr_default.getStatus(25) != 101) && ( ( T003O33_A1412ContagemResultadoNotificacao_Codigo[0] > A1412ContagemResultadoNotificacao_Codigo ) ) )
            {
               A1412ContagemResultadoNotificacao_Codigo = T003O33_A1412ContagemResultadoNotificacao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
               RcdFound167 = 1;
            }
         }
         pr_default.close(25);
      }

      protected void move_previous( )
      {
         RcdFound167 = 0;
         /* Using cursor T003O34 */
         pr_default.execute(26, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(26) != 101) )
         {
            while ( (pr_default.getStatus(26) != 101) && ( ( T003O34_A1412ContagemResultadoNotificacao_Codigo[0] > A1412ContagemResultadoNotificacao_Codigo ) ) )
            {
               pr_default.readNext(26);
            }
            if ( (pr_default.getStatus(26) != 101) && ( ( T003O34_A1412ContagemResultadoNotificacao_Codigo[0] < A1412ContagemResultadoNotificacao_Codigo ) ) )
            {
               A1412ContagemResultadoNotificacao_Codigo = T003O34_A1412ContagemResultadoNotificacao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
               RcdFound167 = 1;
            }
         }
         pr_default.close(26);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3O167( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
            n1962ContagemResultadoNotificacao_Destinatarios = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
            GX_FocusControl = edtContagemResultadoNotificacao_DataHora_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3O167( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound167 == 1 )
            {
               if ( A1412ContagemResultadoNotificacao_Codigo != Z1412ContagemResultadoNotificacao_Codigo )
               {
                  A1412ContagemResultadoNotificacao_Codigo = Z1412ContagemResultadoNotificacao_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADONOTIFICACAO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoNotificacao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
                  n1962ContagemResultadoNotificacao_Destinatarios = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultadoNotificacao_DataHora_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
                  n1962ContagemResultadoNotificacao_Destinatarios = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
                  Update3O167( ) ;
                  GX_FocusControl = edtContagemResultadoNotificacao_DataHora_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1412ContagemResultadoNotificacao_Codigo != Z1412ContagemResultadoNotificacao_Codigo )
               {
                  /* Insert record */
                  A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
                  n1962ContagemResultadoNotificacao_Destinatarios = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
                  GX_FocusControl = edtContagemResultadoNotificacao_DataHora_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3O167( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADONOTIFICACAO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoNotificacao_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
                     n1962ContagemResultadoNotificacao_Destinatarios = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
                     GX_FocusControl = edtContagemResultadoNotificacao_DataHora_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3O167( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1412ContagemResultadoNotificacao_Codigo != Z1412ContagemResultadoNotificacao_Codigo )
         {
            A1412ContagemResultadoNotificacao_Codigo = Z1412ContagemResultadoNotificacao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADONOTIFICACAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNotificacao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
            n1962ContagemResultadoNotificacao_Destinatarios = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultadoNotificacao_DataHora_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3O167( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003O13 */
            pr_default.execute(10, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
            if ( (pr_default.getStatus(10) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNotificacao"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(10) == 101) || ( Z1416ContagemResultadoNotificacao_DataHora != T003O13_A1416ContagemResultadoNotificacao_DataHora[0] ) || ( StringUtil.StrCmp(Z1418ContagemResultadoNotificacao_CorpoEmail, T003O13_A1418ContagemResultadoNotificacao_CorpoEmail[0]) != 0 ) || ( StringUtil.StrCmp(Z1420ContagemResultadoNotificacao_Midia, T003O13_A1420ContagemResultadoNotificacao_Midia[0]) != 0 ) || ( StringUtil.StrCmp(Z1956ContagemResultadoNotificacao_Host, T003O13_A1956ContagemResultadoNotificacao_Host[0]) != 0 ) || ( StringUtil.StrCmp(Z1957ContagemResultadoNotificacao_User, T003O13_A1957ContagemResultadoNotificacao_User[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1958ContagemResultadoNotificacao_Port != T003O13_A1958ContagemResultadoNotificacao_Port[0] ) || ( Z1959ContagemResultadoNotificacao_Aut != T003O13_A1959ContagemResultadoNotificacao_Aut[0] ) || ( Z1960ContagemResultadoNotificacao_Sec != T003O13_A1960ContagemResultadoNotificacao_Sec[0] ) || ( Z1961ContagemResultadoNotificacao_Logged != T003O13_A1961ContagemResultadoNotificacao_Logged[0] ) || ( Z1413ContagemResultadoNotificacao_UsuCod != T003O13_A1413ContagemResultadoNotificacao_UsuCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1966ContagemResultadoNotificacao_AreaTrabalhoCod != T003O13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0] ) || ( Z1965ContagemResultadoNotificacao_ReenvioCod != T003O13_A1965ContagemResultadoNotificacao_ReenvioCod[0] ) )
            {
               if ( Z1416ContagemResultadoNotificacao_DataHora != T003O13_A1416ContagemResultadoNotificacao_DataHora[0] )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[seudo value changed for attri]"+"ContagemResultadoNotificacao_DataHora");
                  GXUtil.WriteLogRaw("Old: ",Z1416ContagemResultadoNotificacao_DataHora);
                  GXUtil.WriteLogRaw("Current: ",T003O13_A1416ContagemResultadoNotificacao_DataHora[0]);
               }
               if ( StringUtil.StrCmp(Z1418ContagemResultadoNotificacao_CorpoEmail, T003O13_A1418ContagemResultadoNotificacao_CorpoEmail[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[seudo value changed for attri]"+"ContagemResultadoNotificacao_CorpoEmail");
                  GXUtil.WriteLogRaw("Old: ",Z1418ContagemResultadoNotificacao_CorpoEmail);
                  GXUtil.WriteLogRaw("Current: ",T003O13_A1418ContagemResultadoNotificacao_CorpoEmail[0]);
               }
               if ( StringUtil.StrCmp(Z1420ContagemResultadoNotificacao_Midia, T003O13_A1420ContagemResultadoNotificacao_Midia[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[seudo value changed for attri]"+"ContagemResultadoNotificacao_Midia");
                  GXUtil.WriteLogRaw("Old: ",Z1420ContagemResultadoNotificacao_Midia);
                  GXUtil.WriteLogRaw("Current: ",T003O13_A1420ContagemResultadoNotificacao_Midia[0]);
               }
               if ( StringUtil.StrCmp(Z1956ContagemResultadoNotificacao_Host, T003O13_A1956ContagemResultadoNotificacao_Host[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[seudo value changed for attri]"+"ContagemResultadoNotificacao_Host");
                  GXUtil.WriteLogRaw("Old: ",Z1956ContagemResultadoNotificacao_Host);
                  GXUtil.WriteLogRaw("Current: ",T003O13_A1956ContagemResultadoNotificacao_Host[0]);
               }
               if ( StringUtil.StrCmp(Z1957ContagemResultadoNotificacao_User, T003O13_A1957ContagemResultadoNotificacao_User[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[seudo value changed for attri]"+"ContagemResultadoNotificacao_User");
                  GXUtil.WriteLogRaw("Old: ",Z1957ContagemResultadoNotificacao_User);
                  GXUtil.WriteLogRaw("Current: ",T003O13_A1957ContagemResultadoNotificacao_User[0]);
               }
               if ( Z1958ContagemResultadoNotificacao_Port != T003O13_A1958ContagemResultadoNotificacao_Port[0] )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[seudo value changed for attri]"+"ContagemResultadoNotificacao_Port");
                  GXUtil.WriteLogRaw("Old: ",Z1958ContagemResultadoNotificacao_Port);
                  GXUtil.WriteLogRaw("Current: ",T003O13_A1958ContagemResultadoNotificacao_Port[0]);
               }
               if ( Z1959ContagemResultadoNotificacao_Aut != T003O13_A1959ContagemResultadoNotificacao_Aut[0] )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[seudo value changed for attri]"+"ContagemResultadoNotificacao_Aut");
                  GXUtil.WriteLogRaw("Old: ",Z1959ContagemResultadoNotificacao_Aut);
                  GXUtil.WriteLogRaw("Current: ",T003O13_A1959ContagemResultadoNotificacao_Aut[0]);
               }
               if ( Z1960ContagemResultadoNotificacao_Sec != T003O13_A1960ContagemResultadoNotificacao_Sec[0] )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[seudo value changed for attri]"+"ContagemResultadoNotificacao_Sec");
                  GXUtil.WriteLogRaw("Old: ",Z1960ContagemResultadoNotificacao_Sec);
                  GXUtil.WriteLogRaw("Current: ",T003O13_A1960ContagemResultadoNotificacao_Sec[0]);
               }
               if ( Z1961ContagemResultadoNotificacao_Logged != T003O13_A1961ContagemResultadoNotificacao_Logged[0] )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[seudo value changed for attri]"+"ContagemResultadoNotificacao_Logged");
                  GXUtil.WriteLogRaw("Old: ",Z1961ContagemResultadoNotificacao_Logged);
                  GXUtil.WriteLogRaw("Current: ",T003O13_A1961ContagemResultadoNotificacao_Logged[0]);
               }
               if ( Z1413ContagemResultadoNotificacao_UsuCod != T003O13_A1413ContagemResultadoNotificacao_UsuCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[seudo value changed for attri]"+"ContagemResultadoNotificacao_UsuCod");
                  GXUtil.WriteLogRaw("Old: ",Z1413ContagemResultadoNotificacao_UsuCod);
                  GXUtil.WriteLogRaw("Current: ",T003O13_A1413ContagemResultadoNotificacao_UsuCod[0]);
               }
               if ( Z1966ContagemResultadoNotificacao_AreaTrabalhoCod != T003O13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[seudo value changed for attri]"+"ContagemResultadoNotificacao_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1966ContagemResultadoNotificacao_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T003O13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0]);
               }
               if ( Z1965ContagemResultadoNotificacao_ReenvioCod != T003O13_A1965ContagemResultadoNotificacao_ReenvioCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[seudo value changed for attri]"+"ContagemResultadoNotificacao_ReenvioCod");
                  GXUtil.WriteLogRaw("Old: ",Z1965ContagemResultadoNotificacao_ReenvioCod);
                  GXUtil.WriteLogRaw("Current: ",T003O13_A1965ContagemResultadoNotificacao_ReenvioCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoNotificacao"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3O167( )
      {
         BeforeValidate3O167( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3O167( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3O167( 0) ;
            CheckOptimisticConcurrency3O167( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3O167( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3O167( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003O35 */
                     pr_default.execute(27, new Object[] {n1416ContagemResultadoNotificacao_DataHora, A1416ContagemResultadoNotificacao_DataHora, n1417ContagemResultadoNotificacao_Assunto, A1417ContagemResultadoNotificacao_Assunto, n1418ContagemResultadoNotificacao_CorpoEmail, A1418ContagemResultadoNotificacao_CorpoEmail, n1420ContagemResultadoNotificacao_Midia, A1420ContagemResultadoNotificacao_Midia, n1415ContagemResultadoNotificacao_Observacao, A1415ContagemResultadoNotificacao_Observacao, n1956ContagemResultadoNotificacao_Host, A1956ContagemResultadoNotificacao_Host, n1957ContagemResultadoNotificacao_User, A1957ContagemResultadoNotificacao_User, n1958ContagemResultadoNotificacao_Port, A1958ContagemResultadoNotificacao_Port, n1959ContagemResultadoNotificacao_Aut, A1959ContagemResultadoNotificacao_Aut, n1960ContagemResultadoNotificacao_Sec, A1960ContagemResultadoNotificacao_Sec, n1961ContagemResultadoNotificacao_Logged, A1961ContagemResultadoNotificacao_Logged, A1413ContagemResultadoNotificacao_UsuCod, n1966ContagemResultadoNotificacao_AreaTrabalhoCod, A1966ContagemResultadoNotificacao_AreaTrabalhoCod, n1965ContagemResultadoNotificacao_ReenvioCod, A1965ContagemResultadoNotificacao_ReenvioCod});
                     A1412ContagemResultadoNotificacao_Codigo = T003O35_A1412ContagemResultadoNotificacao_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
                     pr_default.close(27);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel3O167( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                              ResetCaption3O0( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3O167( ) ;
            }
            EndLevel3O167( ) ;
         }
         CloseExtendedTableCursors3O167( ) ;
      }

      protected void Update3O167( )
      {
         BeforeValidate3O167( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3O167( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3O167( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3O167( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3O167( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003O36 */
                     pr_default.execute(28, new Object[] {n1416ContagemResultadoNotificacao_DataHora, A1416ContagemResultadoNotificacao_DataHora, n1417ContagemResultadoNotificacao_Assunto, A1417ContagemResultadoNotificacao_Assunto, n1418ContagemResultadoNotificacao_CorpoEmail, A1418ContagemResultadoNotificacao_CorpoEmail, n1420ContagemResultadoNotificacao_Midia, A1420ContagemResultadoNotificacao_Midia, n1415ContagemResultadoNotificacao_Observacao, A1415ContagemResultadoNotificacao_Observacao, n1956ContagemResultadoNotificacao_Host, A1956ContagemResultadoNotificacao_Host, n1957ContagemResultadoNotificacao_User, A1957ContagemResultadoNotificacao_User, n1958ContagemResultadoNotificacao_Port, A1958ContagemResultadoNotificacao_Port, n1959ContagemResultadoNotificacao_Aut, A1959ContagemResultadoNotificacao_Aut, n1960ContagemResultadoNotificacao_Sec, A1960ContagemResultadoNotificacao_Sec, n1961ContagemResultadoNotificacao_Logged, A1961ContagemResultadoNotificacao_Logged, A1413ContagemResultadoNotificacao_UsuCod, n1966ContagemResultadoNotificacao_AreaTrabalhoCod, A1966ContagemResultadoNotificacao_AreaTrabalhoCod, n1965ContagemResultadoNotificacao_ReenvioCod, A1965ContagemResultadoNotificacao_ReenvioCod, A1412ContagemResultadoNotificacao_Codigo});
                     pr_default.close(28);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacao") ;
                     if ( (pr_default.getStatus(28) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNotificacao"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3O167( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel3O167( ) ;
                           if ( AnyError == 0 )
                           {
                              if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                              {
                                 if ( AnyError == 0 )
                                 {
                                    context.nUserReturn = 1;
                                 }
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3O167( ) ;
         }
         CloseExtendedTableCursors3O167( ) ;
      }

      protected void DeferredUpdate3O167( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3O167( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3O167( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3O167( ) ;
            AfterConfirm3O167( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3O167( ) ;
               if ( AnyError == 0 )
               {
                  ScanStart3O169( ) ;
                  while ( RcdFound169 != 0 )
                  {
                     getByPrimaryKey3O169( ) ;
                     Delete3O169( ) ;
                     ScanNext3O169( ) ;
                  }
                  ScanEnd3O169( ) ;
                  A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
                  n1962ContagemResultadoNotificacao_Destinatarios = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
                  ScanStart3O168( ) ;
                  while ( RcdFound168 != 0 )
                  {
                     getByPrimaryKey3O168( ) ;
                     Delete3O168( ) ;
                     ScanNext3O168( ) ;
                     O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
                     n1962ContagemResultadoNotificacao_Destinatarios = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
                  }
                  ScanEnd3O168( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003O37 */
                     pr_default.execute(29, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
                     pr_default.close(29);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode167 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3O167( ) ;
         Gx_mode = sMode167;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3O167( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003O39 */
            pr_default.execute(30, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
            if ( (pr_default.getStatus(30) != 101) )
            {
               A1962ContagemResultadoNotificacao_Destinatarios = T003O39_A1962ContagemResultadoNotificacao_Destinatarios[0];
               n1962ContagemResultadoNotificacao_Destinatarios = T003O39_n1962ContagemResultadoNotificacao_Destinatarios[0];
            }
            else
            {
               A1962ContagemResultadoNotificacao_Destinatarios = 0;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
            }
            pr_default.close(30);
            /* Using cursor T003O41 */
            pr_default.execute(31, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
            if ( (pr_default.getStatus(31) != 101) )
            {
               A1963ContagemResultadoNotificacao_Demandas = T003O41_A1963ContagemResultadoNotificacao_Demandas[0];
               n1963ContagemResultadoNotificacao_Demandas = T003O41_n1963ContagemResultadoNotificacao_Demandas[0];
            }
            else
            {
               A1963ContagemResultadoNotificacao_Demandas = 0;
               n1963ContagemResultadoNotificacao_Demandas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1963ContagemResultadoNotificacao_Demandas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1963ContagemResultadoNotificacao_Demandas), 6, 0)));
            }
            pr_default.close(31);
            /* Using cursor T003O42 */
            pr_default.execute(32, new Object[] {A1413ContagemResultadoNotificacao_UsuCod});
            A1427ContagemResultadoNotificacao_UsuPesCod = T003O42_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1427ContagemResultadoNotificacao_UsuPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), 6, 0)));
            n1427ContagemResultadoNotificacao_UsuPesCod = T003O42_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            pr_default.close(32);
            /* Using cursor T003O43 */
            pr_default.execute(33, new Object[] {n1427ContagemResultadoNotificacao_UsuPesCod, A1427ContagemResultadoNotificacao_UsuPesCod});
            A1422ContagemResultadoNotificacao_UsuNom = T003O43_A1422ContagemResultadoNotificacao_UsuNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1422ContagemResultadoNotificacao_UsuNom", A1422ContagemResultadoNotificacao_UsuNom);
            n1422ContagemResultadoNotificacao_UsuNom = T003O43_n1422ContagemResultadoNotificacao_UsuNom[0];
            pr_default.close(33);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T003O44 */
            pr_default.execute(34, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Notifica��es da Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(34);
         }
      }

      protected void ProcessNestedLevel3O168( )
      {
         s1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
         nGXsfl_99_idx = 0;
         while ( nGXsfl_99_idx < nRC_GXsfl_99 )
         {
            ReadRow3O168( ) ;
            if ( ( nRcdExists_168 != 0 ) || ( nIsMod_168 != 0 ) )
            {
               standaloneNotModal3O168( ) ;
               GetKey3O168( ) ;
               if ( ( nRcdExists_168 == 0 ) && ( nRcdDeleted_168 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  Insert3O168( ) ;
               }
               else
               {
                  if ( RcdFound168 != 0 )
                  {
                     if ( ( nRcdDeleted_168 != 0 ) && ( nRcdExists_168 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        Delete3O168( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_168 != 0 ) && ( nRcdExists_168 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           Update3O168( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_168 == 0 )
                     {
                        GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_" + sGXsfl_99_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtContagemResultadoNotificacao_DestCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
               O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
            }
            ChangePostValue( edtContagemResultadoNotificacao_DestCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1414ContagemResultadoNotificacao_DestCod), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultadoNotificacao_DestPesCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1426ContagemResultadoNotificacao_DestPesCod), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultadoNotificacao_DestNome_Internalname, StringUtil.RTrim( A1421ContagemResultadoNotificacao_DestNome)) ;
            ChangePostValue( edtContagemResultadoNotificacao_DestEmail_Internalname, A1419ContagemResultadoNotificacao_DestEmail) ;
            ChangePostValue( "ZT_"+"Z1414ContagemResultadoNotificacao_DestCod_"+sGXsfl_99_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1414ContagemResultadoNotificacao_DestCod), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1419ContagemResultadoNotificacao_DestEmail_"+sGXsfl_99_idx, Z1419ContagemResultadoNotificacao_DestEmail) ;
            ChangePostValue( "nRcdDeleted_168_"+sGXsfl_99_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_168), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_168_"+sGXsfl_99_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_168), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_168_"+sGXsfl_99_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_168), 4, 0, ",", ""))) ;
            if ( nIsMod_168 != 0 )
            {
               ChangePostValue( "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_"+sGXsfl_99_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestCod_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD_"+sGXsfl_99_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestPesCod_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADONOTIFICACAO_DESTNOME_"+sGXsfl_99_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestNome_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL_"+sGXsfl_99_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestEmail_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll3O168( ) ;
         if ( AnyError != 0 )
         {
            O1962ContagemResultadoNotificacao_Destinatarios = s1962ContagemResultadoNotificacao_Destinatarios;
            n1962ContagemResultadoNotificacao_Destinatarios = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
         }
         nRcdExists_168 = 0;
         nIsMod_168 = 0;
         nRcdDeleted_168 = 0;
      }

      protected void ProcessNestedLevel3O169( )
      {
         nGXsfl_111_idx = 0;
         while ( nGXsfl_111_idx < nRC_GXsfl_111 )
         {
            ReadRow3O169( ) ;
            if ( ( nRcdExists_169 != 0 ) || ( nIsMod_169 != 0 ) )
            {
               standaloneNotModal3O169( ) ;
               GetKey3O169( ) ;
               if ( ( nRcdExists_169 == 0 ) && ( nRcdDeleted_169 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  Insert3O169( ) ;
               }
               else
               {
                  if ( RcdFound169 != 0 )
                  {
                     if ( ( nRcdDeleted_169 != 0 ) && ( nRcdExists_169 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        Delete3O169( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_169 != 0 ) && ( nRcdExists_169 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           Update3O169( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_169 == 0 )
                     {
                        GXCCtl = "CONTAGEMRESULTADO_CODIGO_" + sGXsfl_111_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtContagemResultado_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultado_Demanda_Internalname, A457ContagemResultado_Demanda) ;
            ChangePostValue( edtContagemResultado_DemandaFM_Internalname, A493ContagemResultado_DemandaFM) ;
            ChangePostValue( edtContagemResultado_Servico_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultado_ServicoSigla_Internalname, StringUtil.RTrim( A801ContagemResultado_ServicoSigla)) ;
            ChangePostValue( edtContagemResultado_PrazoInicialDias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z456ContagemResultado_Codigo_"+sGXsfl_111_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z456ContagemResultado_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_169_"+sGXsfl_111_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_169), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_169_"+sGXsfl_111_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_169), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_169_"+sGXsfl_111_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_169), 4, 0, ",", ""))) ;
            if ( nIsMod_169 != 0 )
            {
               ChangePostValue( "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Demanda_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_DemandaFM_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADO_SERVICO_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Servico_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_ServicoSigla_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADO_PRAZOINICIALDIAS_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_PrazoInicialDias_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* Using cursor T003O41 */
         pr_default.execute(31, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(31) != 101) )
         {
            A1963ContagemResultadoNotificacao_Demandas = T003O41_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = T003O41_n1963ContagemResultadoNotificacao_Demandas[0];
         }
         else
         {
            A1963ContagemResultadoNotificacao_Demandas = 0;
            n1963ContagemResultadoNotificacao_Demandas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1963ContagemResultadoNotificacao_Demandas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1963ContagemResultadoNotificacao_Demandas), 6, 0)));
         }
         /* End of After( level) rules */
         InitAll3O169( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_169 = 0;
         nIsMod_169 = 0;
         nRcdDeleted_169 = 0;
      }

      protected void ProcessLevel3O167( )
      {
         /* Save parent mode. */
         sMode167 = Gx_mode;
         ProcessNestedLevel3O168( ) ;
         ProcessNestedLevel3O169( ) ;
         if ( AnyError != 0 )
         {
            O1962ContagemResultadoNotificacao_Destinatarios = s1962ContagemResultadoNotificacao_Destinatarios;
            n1962ContagemResultadoNotificacao_Destinatarios = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
         }
         /* Restore parent mode. */
         Gx_mode = sMode167;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         /* ' Update level parameters */
      }

      protected void EndLevel3O167( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(10);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3O167( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(11);
            pr_default.close(7);
            pr_default.close(6);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(32);
            pr_default.close(33);
            pr_default.close(30);
            pr_default.close(31);
            pr_default.close(8);
            pr_default.close(9);
            pr_default.close(2);
            pr_default.close(3);
            pr_default.close(4);
            context.CommitDataStores( "ContagemResultadoNotificacao");
            if ( AnyError == 0 )
            {
               ConfirmValues3O0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(11);
            pr_default.close(7);
            pr_default.close(6);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(32);
            pr_default.close(33);
            pr_default.close(30);
            pr_default.close(31);
            pr_default.close(8);
            pr_default.close(9);
            pr_default.close(2);
            pr_default.close(3);
            pr_default.close(4);
            context.RollbackDataStores( "ContagemResultadoNotificacao");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3O167( )
      {
         /* Scan By routine */
         /* Using cursor T003O45 */
         pr_default.execute(35);
         RcdFound167 = 0;
         if ( (pr_default.getStatus(35) != 101) )
         {
            RcdFound167 = 1;
            A1412ContagemResultadoNotificacao_Codigo = T003O45_A1412ContagemResultadoNotificacao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3O167( )
      {
         /* Scan next routine */
         pr_default.readNext(35);
         RcdFound167 = 0;
         if ( (pr_default.getStatus(35) != 101) )
         {
            RcdFound167 = 1;
            A1412ContagemResultadoNotificacao_Codigo = T003O45_A1412ContagemResultadoNotificacao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
         }
      }

      protected void ScanEnd3O167( )
      {
         pr_default.close(35);
      }

      protected void AfterConfirm3O167( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3O167( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3O167( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3O167( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3O167( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3O167( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3O167( )
      {
         edtContagemResultadoNotificacao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_Codigo_Enabled), 5, 0)));
         edtContagemResultadoNotificacao_DataHora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DataHora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_DataHora_Enabled), 5, 0)));
         edtContagemResultadoNotificacao_UsuCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_UsuCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_UsuCod_Enabled), 5, 0)));
         edtContagemResultadoNotificacao_UsuPesCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_UsuPesCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_UsuPesCod_Enabled), 5, 0)));
         edtContagemResultadoNotificacao_UsuNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_UsuNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_UsuNom_Enabled), 5, 0)));
         edtContagemResultadoNotificacao_Assunto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_Assunto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_Assunto_Enabled), 5, 0)));
         edtContagemResultadoNotificacao_CorpoEmail_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_CorpoEmail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_CorpoEmail_Enabled), 5, 0)));
         cmbContagemResultadoNotificacao_Midia.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Midia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemResultadoNotificacao_Midia.Enabled), 5, 0)));
         edtContagemResultadoNotificacao_Host_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_Host_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_Host_Enabled), 5, 0)));
         edtContagemResultadoNotificacao_User_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_User_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_User_Enabled), 5, 0)));
         edtContagemResultadoNotificacao_Port_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_Port_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_Port_Enabled), 5, 0)));
         cmbContagemResultadoNotificacao_Aut.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Aut_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemResultadoNotificacao_Aut.Enabled), 5, 0)));
         cmbContagemResultadoNotificacao_Sec.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Sec_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemResultadoNotificacao_Sec.Enabled), 5, 0)));
         cmbContagemResultadoNotificacao_Logged.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Logged_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemResultadoNotificacao_Logged.Enabled), 5, 0)));
         Contagemresultadonotificacao_observacao_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Contagemresultadonotificacao_observacao_Internalname, "Enabled", StringUtil.BoolToStr( Contagemresultadonotificacao_observacao_Enabled));
      }

      protected void ZM3O168( short GX_JID )
      {
         if ( ( GX_JID == 23 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1419ContagemResultadoNotificacao_DestEmail = T003O10_A1419ContagemResultadoNotificacao_DestEmail[0];
            }
            else
            {
               Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
            }
         }
         if ( GX_JID == -23 )
         {
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
            Z1419ContagemResultadoNotificacao_DestEmail = A1419ContagemResultadoNotificacao_DestEmail;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            Z1426ContagemResultadoNotificacao_DestPesCod = A1426ContagemResultadoNotificacao_DestPesCod;
            Z1421ContagemResultadoNotificacao_DestNome = A1421ContagemResultadoNotificacao_DestNome;
         }
      }

      protected void standaloneNotModal3O168( )
      {
      }

      protected void standaloneModal3O168( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            edtContagemResultadoNotificacao_DestCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_DestCod_Enabled), 5, 0)));
         }
         else
         {
            edtContagemResultadoNotificacao_DestCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_DestCod_Enabled), 5, 0)));
         }
      }

      protected void Load3O168( )
      {
         /* Using cursor T003O46 */
         pr_default.execute(36, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A1414ContagemResultadoNotificacao_DestCod});
         if ( (pr_default.getStatus(36) != 101) )
         {
            RcdFound168 = 1;
            A1421ContagemResultadoNotificacao_DestNome = T003O46_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = T003O46_n1421ContagemResultadoNotificacao_DestNome[0];
            A1419ContagemResultadoNotificacao_DestEmail = T003O46_A1419ContagemResultadoNotificacao_DestEmail[0];
            n1419ContagemResultadoNotificacao_DestEmail = T003O46_n1419ContagemResultadoNotificacao_DestEmail[0];
            A1426ContagemResultadoNotificacao_DestPesCod = T003O46_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = T003O46_n1426ContagemResultadoNotificacao_DestPesCod[0];
            ZM3O168( -23) ;
         }
         pr_default.close(36);
         OnLoadActions3O168( ) ;
      }

      protected void OnLoadActions3O168( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1962ContagemResultadoNotificacao_Destinatarios = (int)(O1962ContagemResultadoNotificacao_Destinatarios+1);
            n1962ContagemResultadoNotificacao_Destinatarios = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A1962ContagemResultadoNotificacao_Destinatarios = (int)(O1962ContagemResultadoNotificacao_Destinatarios-1);
                  n1962ContagemResultadoNotificacao_Destinatarios = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
               }
            }
         }
      }

      protected void CheckExtendedTable3O168( )
      {
         Gx_BScreen = 1;
         standaloneModal3O168( ) ;
         /* Using cursor T003O11 */
         pr_default.execute(8, new Object[] {A1414ContagemResultadoNotificacao_DestCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_" + sGXsfl_99_idx;
            GX_msglist.addItem("N�o existe 'Destinat�rio da Notifica��o da Contagem'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNotificacao_DestCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1426ContagemResultadoNotificacao_DestPesCod = T003O11_A1426ContagemResultadoNotificacao_DestPesCod[0];
         n1426ContagemResultadoNotificacao_DestPesCod = T003O11_n1426ContagemResultadoNotificacao_DestPesCod[0];
         pr_default.close(8);
         /* Using cursor T003O12 */
         pr_default.execute(9, new Object[] {n1426ContagemResultadoNotificacao_DestPesCod, A1426ContagemResultadoNotificacao_DestPesCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1421ContagemResultadoNotificacao_DestNome = T003O12_A1421ContagemResultadoNotificacao_DestNome[0];
         n1421ContagemResultadoNotificacao_DestNome = T003O12_n1421ContagemResultadoNotificacao_DestNome[0];
         pr_default.close(9);
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1962ContagemResultadoNotificacao_Destinatarios = (int)(O1962ContagemResultadoNotificacao_Destinatarios+1);
            n1962ContagemResultadoNotificacao_Destinatarios = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A1962ContagemResultadoNotificacao_Destinatarios = (int)(O1962ContagemResultadoNotificacao_Destinatarios-1);
                  n1962ContagemResultadoNotificacao_Destinatarios = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
               }
            }
         }
         if ( ! ( GxRegex.IsMatch(A1419ContagemResultadoNotificacao_DestEmail,"^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") || String.IsNullOrEmpty(StringUtil.RTrim( A1419ContagemResultadoNotificacao_DestEmail)) ) )
         {
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL_" + sGXsfl_99_idx;
            GX_msglist.addItem("O valor de Destinat�rio Email n�o coincide com o padr�o especificado", "OutOfRange", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNotificacao_DestEmail_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors3O168( )
      {
         pr_default.close(8);
         pr_default.close(9);
      }

      protected void enableDisable3O168( )
      {
      }

      protected void gxLoad_24( int A1414ContagemResultadoNotificacao_DestCod )
      {
         /* Using cursor T003O47 */
         pr_default.execute(37, new Object[] {A1414ContagemResultadoNotificacao_DestCod});
         if ( (pr_default.getStatus(37) == 101) )
         {
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_" + sGXsfl_99_idx;
            GX_msglist.addItem("N�o existe 'Destinat�rio da Notifica��o da Contagem'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNotificacao_DestCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1426ContagemResultadoNotificacao_DestPesCod = T003O47_A1426ContagemResultadoNotificacao_DestPesCod[0];
         n1426ContagemResultadoNotificacao_DestPesCod = T003O47_n1426ContagemResultadoNotificacao_DestPesCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1426ContagemResultadoNotificacao_DestPesCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(37) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(37);
      }

      protected void gxLoad_25( int A1426ContagemResultadoNotificacao_DestPesCod )
      {
         /* Using cursor T003O48 */
         pr_default.execute(38, new Object[] {n1426ContagemResultadoNotificacao_DestPesCod, A1426ContagemResultadoNotificacao_DestPesCod});
         if ( (pr_default.getStatus(38) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1421ContagemResultadoNotificacao_DestNome = T003O48_A1421ContagemResultadoNotificacao_DestNome[0];
         n1421ContagemResultadoNotificacao_DestNome = T003O48_n1421ContagemResultadoNotificacao_DestNome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1421ContagemResultadoNotificacao_DestNome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(38) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(38);
      }

      protected void GetKey3O168( )
      {
         /* Using cursor T003O49 */
         pr_default.execute(39, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A1414ContagemResultadoNotificacao_DestCod});
         if ( (pr_default.getStatus(39) != 101) )
         {
            RcdFound168 = 1;
         }
         else
         {
            RcdFound168 = 0;
         }
         pr_default.close(39);
      }

      protected void getByPrimaryKey3O168( )
      {
         /* Using cursor T003O10 */
         pr_default.execute(7, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A1414ContagemResultadoNotificacao_DestCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            ZM3O168( 23) ;
            RcdFound168 = 1;
            InitializeNonKey3O168( ) ;
            A1419ContagemResultadoNotificacao_DestEmail = T003O10_A1419ContagemResultadoNotificacao_DestEmail[0];
            n1419ContagemResultadoNotificacao_DestEmail = T003O10_n1419ContagemResultadoNotificacao_DestEmail[0];
            A1414ContagemResultadoNotificacao_DestCod = T003O10_A1414ContagemResultadoNotificacao_DestCod[0];
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
            Z1414ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
            sMode168 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3O168( ) ;
            Gx_mode = sMode168;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound168 = 0;
            InitializeNonKey3O168( ) ;
            sMode168 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal3O168( ) ;
            Gx_mode = sMode168;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes3O168( ) ;
         }
         pr_default.close(7);
      }

      protected void CheckOptimisticConcurrency3O168( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003O9 */
            pr_default.execute(6, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A1414ContagemResultadoNotificacao_DestCod});
            if ( (pr_default.getStatus(6) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNotificacaoDestinatario"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(6) == 101) || ( StringUtil.StrCmp(Z1419ContagemResultadoNotificacao_DestEmail, T003O9_A1419ContagemResultadoNotificacao_DestEmail[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z1419ContagemResultadoNotificacao_DestEmail, T003O9_A1419ContagemResultadoNotificacao_DestEmail[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadonotificacao:[seudo value changed for attri]"+"ContagemResultadoNotificacao_DestEmail");
                  GXUtil.WriteLogRaw("Old: ",Z1419ContagemResultadoNotificacao_DestEmail);
                  GXUtil.WriteLogRaw("Current: ",T003O9_A1419ContagemResultadoNotificacao_DestEmail[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoNotificacaoDestinatario"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3O168( )
      {
         BeforeValidate3O168( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3O168( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3O168( 0) ;
            CheckOptimisticConcurrency3O168( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3O168( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3O168( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003O50 */
                     pr_default.execute(40, new Object[] {A1412ContagemResultadoNotificacao_Codigo, n1419ContagemResultadoNotificacao_DestEmail, A1419ContagemResultadoNotificacao_DestEmail, A1414ContagemResultadoNotificacao_DestCod});
                     pr_default.close(40);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacaoDestinatario") ;
                     if ( (pr_default.getStatus(40) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3O168( ) ;
            }
            EndLevel3O168( ) ;
         }
         CloseExtendedTableCursors3O168( ) ;
      }

      protected void Update3O168( )
      {
         BeforeValidate3O168( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3O168( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3O168( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3O168( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3O168( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003O51 */
                     pr_default.execute(41, new Object[] {n1419ContagemResultadoNotificacao_DestEmail, A1419ContagemResultadoNotificacao_DestEmail, A1412ContagemResultadoNotificacao_Codigo, A1414ContagemResultadoNotificacao_DestCod});
                     pr_default.close(41);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacaoDestinatario") ;
                     if ( (pr_default.getStatus(41) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNotificacaoDestinatario"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3O168( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey3O168( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3O168( ) ;
         }
         CloseExtendedTableCursors3O168( ) ;
      }

      protected void DeferredUpdate3O168( )
      {
      }

      protected void Delete3O168( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         BeforeValidate3O168( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3O168( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3O168( ) ;
            AfterConfirm3O168( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3O168( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003O52 */
                  pr_default.execute(42, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A1414ContagemResultadoNotificacao_DestCod});
                  pr_default.close(42);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacaoDestinatario") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode168 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3O168( ) ;
         Gx_mode = sMode168;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3O168( )
      {
         standaloneModal3O168( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003O53 */
            pr_default.execute(43, new Object[] {A1414ContagemResultadoNotificacao_DestCod});
            A1426ContagemResultadoNotificacao_DestPesCod = T003O53_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = T003O53_n1426ContagemResultadoNotificacao_DestPesCod[0];
            pr_default.close(43);
            /* Using cursor T003O54 */
            pr_default.execute(44, new Object[] {n1426ContagemResultadoNotificacao_DestPesCod, A1426ContagemResultadoNotificacao_DestPesCod});
            A1421ContagemResultadoNotificacao_DestNome = T003O54_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = T003O54_n1421ContagemResultadoNotificacao_DestNome[0];
            pr_default.close(44);
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A1962ContagemResultadoNotificacao_Destinatarios = (int)(O1962ContagemResultadoNotificacao_Destinatarios+1);
               n1962ContagemResultadoNotificacao_Destinatarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
               {
                  A1962ContagemResultadoNotificacao_Destinatarios = O1962ContagemResultadoNotificacao_Destinatarios;
                  n1962ContagemResultadoNotificacao_Destinatarios = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
               }
               else
               {
                  if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
                  {
                     A1962ContagemResultadoNotificacao_Destinatarios = (int)(O1962ContagemResultadoNotificacao_Destinatarios-1);
                     n1962ContagemResultadoNotificacao_Destinatarios = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
                  }
               }
            }
         }
      }

      protected void EndLevel3O168( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(6);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3O168( )
      {
         /* Scan By routine */
         /* Using cursor T003O55 */
         pr_default.execute(45, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         RcdFound168 = 0;
         if ( (pr_default.getStatus(45) != 101) )
         {
            RcdFound168 = 1;
            A1414ContagemResultadoNotificacao_DestCod = T003O55_A1414ContagemResultadoNotificacao_DestCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3O168( )
      {
         /* Scan next routine */
         pr_default.readNext(45);
         RcdFound168 = 0;
         if ( (pr_default.getStatus(45) != 101) )
         {
            RcdFound168 = 1;
            A1414ContagemResultadoNotificacao_DestCod = T003O55_A1414ContagemResultadoNotificacao_DestCod[0];
         }
      }

      protected void ScanEnd3O168( )
      {
         pr_default.close(45);
      }

      protected void AfterConfirm3O168( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3O168( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3O168( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3O168( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3O168( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3O168( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3O168( )
      {
         edtContagemResultadoNotificacao_DestCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_DestCod_Enabled), 5, 0)));
         edtContagemResultadoNotificacao_DestPesCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestPesCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_DestPesCod_Enabled), 5, 0)));
         edtContagemResultadoNotificacao_DestNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_DestNome_Enabled), 5, 0)));
         edtContagemResultadoNotificacao_DestEmail_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestEmail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_DestEmail_Enabled), 5, 0)));
      }

      protected void ZM3O169( short GX_JID )
      {
         if ( ( GX_JID == 26 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -26 )
         {
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z1553ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z801ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
         }
      }

      protected void standaloneNotModal3O169( )
      {
      }

      protected void standaloneModal3O169( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            edtContagemResultado_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtContagemResultado_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
         }
      }

      protected void Load3O169( )
      {
         /* Using cursor T003O56 */
         pr_default.execute(46, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(46) != 101) )
         {
            RcdFound169 = 1;
            A1553ContagemResultado_CntSrvCod = T003O56_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = T003O56_n1553ContagemResultado_CntSrvCod[0];
            A457ContagemResultado_Demanda = T003O56_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T003O56_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = T003O56_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T003O56_n493ContagemResultado_DemandaFM[0];
            A801ContagemResultado_ServicoSigla = T003O56_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = T003O56_n801ContagemResultado_ServicoSigla[0];
            A1227ContagemResultado_PrazoInicialDias = T003O56_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = T003O56_n1227ContagemResultado_PrazoInicialDias[0];
            A601ContagemResultado_Servico = T003O56_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = T003O56_n601ContagemResultado_Servico[0];
            ZM3O169( -26) ;
         }
         pr_default.close(46);
         OnLoadActions3O169( ) ;
      }

      protected void OnLoadActions3O169( )
      {
      }

      protected void CheckExtendedTable3O169( )
      {
         Gx_BScreen = 1;
         standaloneModal3O169( ) ;
         /* Using cursor T003O4 */
         pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GXCCtl = "CONTAGEMRESULTADO_CODIGO_" + sGXsfl_111_idx;
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1553ContagemResultado_CntSrvCod = T003O4_A1553ContagemResultado_CntSrvCod[0];
         n1553ContagemResultado_CntSrvCod = T003O4_n1553ContagemResultado_CntSrvCod[0];
         A457ContagemResultado_Demanda = T003O4_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T003O4_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = T003O4_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T003O4_n493ContagemResultado_DemandaFM[0];
         A1227ContagemResultado_PrazoInicialDias = T003O4_A1227ContagemResultado_PrazoInicialDias[0];
         n1227ContagemResultado_PrazoInicialDias = T003O4_n1227ContagemResultado_PrazoInicialDias[0];
         pr_default.close(2);
         /* Using cursor T003O5 */
         pr_default.execute(3, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A601ContagemResultado_Servico = T003O5_A601ContagemResultado_Servico[0];
         n601ContagemResultado_Servico = T003O5_n601ContagemResultado_Servico[0];
         pr_default.close(3);
         /* Using cursor T003O6 */
         pr_default.execute(4, new Object[] {n601ContagemResultado_Servico, A601ContagemResultado_Servico});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A601ContagemResultado_Servico) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A801ContagemResultado_ServicoSigla = T003O6_A801ContagemResultado_ServicoSigla[0];
         n801ContagemResultado_ServicoSigla = T003O6_n801ContagemResultado_ServicoSigla[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors3O169( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable3O169( )
      {
      }

      protected void gxLoad_27( int A456ContagemResultado_Codigo )
      {
         /* Using cursor T003O57 */
         pr_default.execute(47, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(47) == 101) )
         {
            GXCCtl = "CONTAGEMRESULTADO_CODIGO_" + sGXsfl_111_idx;
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1553ContagemResultado_CntSrvCod = T003O57_A1553ContagemResultado_CntSrvCod[0];
         n1553ContagemResultado_CntSrvCod = T003O57_n1553ContagemResultado_CntSrvCod[0];
         A457ContagemResultado_Demanda = T003O57_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T003O57_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = T003O57_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T003O57_n493ContagemResultado_DemandaFM[0];
         A1227ContagemResultado_PrazoInicialDias = T003O57_A1227ContagemResultado_PrazoInicialDias[0];
         n1227ContagemResultado_PrazoInicialDias = T003O57_n1227ContagemResultado_PrazoInicialDias[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( A457ContagemResultado_Demanda)+"\""+","+"\""+GXUtil.EncodeJSConstant( A493ContagemResultado_DemandaFM)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(47) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(47);
      }

      protected void gxLoad_28( int A1553ContagemResultado_CntSrvCod )
      {
         /* Using cursor T003O58 */
         pr_default.execute(48, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(48) == 101) )
         {
            if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A601ContagemResultado_Servico = T003O58_A601ContagemResultado_Servico[0];
         n601ContagemResultado_Servico = T003O58_n601ContagemResultado_Servico[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(48) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(48);
      }

      protected void gxLoad_29( int A601ContagemResultado_Servico )
      {
         /* Using cursor T003O59 */
         pr_default.execute(49, new Object[] {n601ContagemResultado_Servico, A601ContagemResultado_Servico});
         if ( (pr_default.getStatus(49) == 101) )
         {
            if ( ! ( (0==A601ContagemResultado_Servico) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A801ContagemResultado_ServicoSigla = T003O59_A801ContagemResultado_ServicoSigla[0];
         n801ContagemResultado_ServicoSigla = T003O59_n801ContagemResultado_ServicoSigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A801ContagemResultado_ServicoSigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(49) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(49);
      }

      protected void GetKey3O169( )
      {
         /* Using cursor T003O60 */
         pr_default.execute(50, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(50) != 101) )
         {
            RcdFound169 = 1;
         }
         else
         {
            RcdFound169 = 0;
         }
         pr_default.close(50);
      }

      protected void getByPrimaryKey3O169( )
      {
         /* Using cursor T003O3 */
         pr_default.execute(1, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3O169( 26) ;
            RcdFound169 = 1;
            InitializeNonKey3O169( ) ;
            A456ContagemResultado_Codigo = T003O3_A456ContagemResultado_Codigo[0];
            Z1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            sMode169 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3O169( ) ;
            Gx_mode = sMode169;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound169 = 0;
            InitializeNonKey3O169( ) ;
            sMode169 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal3O169( ) ;
            Gx_mode = sMode169;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes3O169( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency3O169( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003O2 */
            pr_default.execute(0, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNotificacaoDemanda"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoNotificacaoDemanda"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3O169( )
      {
         BeforeValidate3O169( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3O169( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3O169( 0) ;
            CheckOptimisticConcurrency3O169( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3O169( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3O169( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003O61 */
                     pr_default.execute(51, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A456ContagemResultado_Codigo});
                     pr_default.close(51);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacaoDemanda") ;
                     if ( (pr_default.getStatus(51) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3O169( ) ;
            }
            EndLevel3O169( ) ;
         }
         CloseExtendedTableCursors3O169( ) ;
      }

      protected void Update3O169( )
      {
         BeforeValidate3O169( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3O169( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3O169( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3O169( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3O169( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ContagemResultadoNotificacaoDemanda] */
                     DeferredUpdate3O169( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey3O169( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3O169( ) ;
         }
         CloseExtendedTableCursors3O169( ) ;
      }

      protected void DeferredUpdate3O169( )
      {
      }

      protected void Delete3O169( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         BeforeValidate3O169( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3O169( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3O169( ) ;
            AfterConfirm3O169( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3O169( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003O62 */
                  pr_default.execute(52, new Object[] {A1412ContagemResultadoNotificacao_Codigo, A456ContagemResultado_Codigo});
                  pr_default.close(52);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotificacaoDemanda") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode169 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3O169( ) ;
         Gx_mode = sMode169;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3O169( )
      {
         standaloneModal3O169( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003O63 */
            pr_default.execute(53, new Object[] {A456ContagemResultado_Codigo});
            A1553ContagemResultado_CntSrvCod = T003O63_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = T003O63_n1553ContagemResultado_CntSrvCod[0];
            A457ContagemResultado_Demanda = T003O63_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T003O63_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = T003O63_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T003O63_n493ContagemResultado_DemandaFM[0];
            A1227ContagemResultado_PrazoInicialDias = T003O63_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = T003O63_n1227ContagemResultado_PrazoInicialDias[0];
            pr_default.close(53);
            /* Using cursor T003O64 */
            pr_default.execute(54, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
            A601ContagemResultado_Servico = T003O64_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = T003O64_n601ContagemResultado_Servico[0];
            pr_default.close(54);
            /* Using cursor T003O65 */
            pr_default.execute(55, new Object[] {n601ContagemResultado_Servico, A601ContagemResultado_Servico});
            A801ContagemResultado_ServicoSigla = T003O65_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = T003O65_n801ContagemResultado_ServicoSigla[0];
            pr_default.close(55);
         }
      }

      protected void EndLevel3O169( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3O169( )
      {
         /* Scan By routine */
         /* Using cursor T003O66 */
         pr_default.execute(56, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         RcdFound169 = 0;
         if ( (pr_default.getStatus(56) != 101) )
         {
            RcdFound169 = 1;
            A456ContagemResultado_Codigo = T003O66_A456ContagemResultado_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3O169( )
      {
         /* Scan next routine */
         pr_default.readNext(56);
         RcdFound169 = 0;
         if ( (pr_default.getStatus(56) != 101) )
         {
            RcdFound169 = 1;
            A456ContagemResultado_Codigo = T003O66_A456ContagemResultado_Codigo[0];
         }
      }

      protected void ScanEnd3O169( )
      {
         pr_default.close(56);
      }

      protected void AfterConfirm3O169( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3O169( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3O169( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3O169( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3O169( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3O169( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3O169( )
      {
         edtContagemResultado_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
         edtContagemResultado_Demanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Demanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Demanda_Enabled), 5, 0)));
         edtContagemResultado_DemandaFM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DemandaFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_DemandaFM_Enabled), 5, 0)));
         edtContagemResultado_Servico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Servico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Servico_Enabled), 5, 0)));
         edtContagemResultado_ServicoSigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_ServicoSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_ServicoSigla_Enabled), 5, 0)));
         edtContagemResultado_PrazoInicialDias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PrazoInicialDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PrazoInicialDias_Enabled), 5, 0)));
      }

      protected void SubsflControlProps_99168( )
      {
         edtContagemResultadoNotificacao_DestCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_"+sGXsfl_99_idx;
         imgprompt_1414_Internalname = "PROMPT_1414_"+sGXsfl_99_idx;
         edtContagemResultadoNotificacao_DestPesCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD_"+sGXsfl_99_idx;
         edtContagemResultadoNotificacao_DestNome_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTNOME_"+sGXsfl_99_idx;
         edtContagemResultadoNotificacao_DestEmail_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL_"+sGXsfl_99_idx;
      }

      protected void SubsflControlProps_fel_99168( )
      {
         edtContagemResultadoNotificacao_DestCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_"+sGXsfl_99_fel_idx;
         imgprompt_1414_Internalname = "PROMPT_1414_"+sGXsfl_99_fel_idx;
         edtContagemResultadoNotificacao_DestPesCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD_"+sGXsfl_99_fel_idx;
         edtContagemResultadoNotificacao_DestNome_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTNOME_"+sGXsfl_99_fel_idx;
         edtContagemResultadoNotificacao_DestEmail_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL_"+sGXsfl_99_fel_idx;
      }

      protected void AddRow3O168( )
      {
         nGXsfl_99_idx = (short)(nGXsfl_99_idx+1);
         sGXsfl_99_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_99_idx), 4, 0)), 4, "0");
         SubsflControlProps_99168( ) ;
         SendRow3O168( ) ;
      }

      protected void SendRow3O168( )
      {
         Gridlevel_destinatarioRow = GXWebRow.GetNew(context);
         if ( subGridlevel_destinatario_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridlevel_destinatario_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridlevel_destinatario_Class, "") != 0 )
            {
               subGridlevel_destinatario_Linesclass = subGridlevel_destinatario_Class+"Odd";
            }
         }
         else if ( subGridlevel_destinatario_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridlevel_destinatario_Backstyle = 0;
            subGridlevel_destinatario_Backcolor = subGridlevel_destinatario_Allbackcolor;
            if ( StringUtil.StrCmp(subGridlevel_destinatario_Class, "") != 0 )
            {
               subGridlevel_destinatario_Linesclass = subGridlevel_destinatario_Class+"Uniform";
            }
         }
         else if ( subGridlevel_destinatario_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridlevel_destinatario_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridlevel_destinatario_Class, "") != 0 )
            {
               subGridlevel_destinatario_Linesclass = subGridlevel_destinatario_Class+"Odd";
            }
            subGridlevel_destinatario_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGridlevel_destinatario_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridlevel_destinatario_Backstyle = 1;
            if ( ((int)((nGXsfl_99_idx) % (2))) == 0 )
            {
               subGridlevel_destinatario_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridlevel_destinatario_Class, "") != 0 )
               {
                  subGridlevel_destinatario_Linesclass = subGridlevel_destinatario_Class+"Even";
               }
            }
            else
            {
               subGridlevel_destinatario_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridlevel_destinatario_Class, "") != 0 )
               {
                  subGridlevel_destinatario_Linesclass = subGridlevel_destinatario_Class+"Odd";
               }
            }
         }
         imgprompt_1414_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptusuario.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CONTAGEMRESULTADONOTIFICACAO_DESTCOD_"+sGXsfl_99_idx+"'), id:'"+"CONTAGEMRESULTADONOTIFICACAO_DESTCOD_"+sGXsfl_99_idx+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"CONTAGEMRESULTADONOTIFICACAO_DESTNOME_"+sGXsfl_99_idx+"'), id:'"+"CONTAGEMRESULTADONOTIFICACAO_DESTNOME_"+sGXsfl_99_idx+"'"+",IOType:'inout'}"+"],"+"gx.dom.form()."+"nIsMod_168_"+sGXsfl_99_idx+","+"'', false"+","+"false"+");");
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_168_" + sGXsfl_99_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_99_idx + "',99)\"";
         ROClassString = "BootstrapAttribute";
         Gridlevel_destinatarioRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_DestCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1414ContagemResultadoNotificacao_DestCod), 6, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(A1414ContagemResultadoNotificacao_DestCod), "ZZZZZ9")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,100);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_DestCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultadoNotificacao_DestCod_Enabled,(short)1,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)99,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         Gridlevel_destinatarioRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)imgprompt_1414_Internalname,context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )),(String)imgprompt_1414_Link,(String)"",(String)"",context.GetTheme( ),(int)imgprompt_1414_Visible,(short)1,(String)"",(String)"",(short)0,(short)0,(short)0,(String)"",(short)0,(String)"",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)false,(bool)false});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Gridlevel_destinatarioRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_DestPesCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1426ContagemResultadoNotificacao_DestPesCod), 6, 0, ",", "")),((edtContagemResultadoNotificacao_DestPesCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1426ContagemResultadoNotificacao_DestPesCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1426ContagemResultadoNotificacao_DestPesCod), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_DestPesCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultadoNotificacao_DestPesCod_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)99,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Gridlevel_destinatarioRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_DestNome_Internalname,StringUtil.RTrim( A1421ContagemResultadoNotificacao_DestNome),StringUtil.RTrim( context.localUtil.Format( A1421ContagemResultadoNotificacao_DestNome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_DestNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultadoNotificacao_DestNome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)99,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_168_" + sGXsfl_99_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_99_idx + "',99)\"";
         ROClassString = "BootstrapAttribute";
         Gridlevel_destinatarioRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_DestEmail_Internalname,(String)A1419ContagemResultadoNotificacao_DestEmail,(String)"",TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"mailto:"+A1419ContagemResultadoNotificacao_DestEmail,(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_DestEmail_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultadoNotificacao_DestEmail_Enabled,(short)0,(String)"email",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)99,(short)1,(short)-1,(short)0,(bool)true,(String)"Email",(String)"left",(bool)true});
         context.httpAjaxContext.ajax_sending_grid_row(Gridlevel_destinatarioRow);
         GXCCtl = "Z1414ContagemResultadoNotificacao_DestCod_" + sGXsfl_99_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1414ContagemResultadoNotificacao_DestCod), 6, 0, ",", "")));
         GXCCtl = "Z1419ContagemResultadoNotificacao_DestEmail_" + sGXsfl_99_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, Z1419ContagemResultadoNotificacao_DestEmail);
         GXCCtl = "nRcdDeleted_168_" + sGXsfl_99_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_168), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_168_" + sGXsfl_99_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_168), 4, 0, ",", "")));
         GXCCtl = "nIsMod_168_" + sGXsfl_99_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_168), 4, 0, ",", "")));
         GXCCtl = "vMODE_" + sGXsfl_99_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Gx_mode));
         GXCCtl = "vTRNCONTEXT_" + sGXsfl_99_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV9TrnContext);
         }
         GXCCtl = "vCONTAGEMRESULTADONOTIFICACAO_CODIGO_" + sGXsfl_99_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_"+sGXsfl_99_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestCod_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD_"+sGXsfl_99_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestPesCod_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME_"+sGXsfl_99_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestNome_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL_"+sGXsfl_99_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestEmail_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PROMPT_1414_"+sGXsfl_99_idx+"Link", StringUtil.RTrim( imgprompt_1414_Link));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridlevel_destinatarioContainer.AddRow(Gridlevel_destinatarioRow);
      }

      protected void ReadRow3O168( )
      {
         nGXsfl_99_idx = (short)(nGXsfl_99_idx+1);
         sGXsfl_99_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_99_idx), 4, 0)), 4, "0");
         SubsflControlProps_99168( ) ;
         edtContagemResultadoNotificacao_DestCod_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_"+sGXsfl_99_idx+"Enabled"), ",", "."));
         edtContagemResultadoNotificacao_DestPesCod_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD_"+sGXsfl_99_idx+"Enabled"), ",", "."));
         edtContagemResultadoNotificacao_DestNome_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_DESTNOME_"+sGXsfl_99_idx+"Enabled"), ",", "."));
         edtContagemResultadoNotificacao_DestEmail_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL_"+sGXsfl_99_idx+"Enabled"), ",", "."));
         imgprompt_1413_Link = cgiGet( "PROMPT_1414_"+sGXsfl_99_idx+"Link");
         if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_DestCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_DestCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
         {
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_DESTCOD_" + sGXsfl_99_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNotificacao_DestCod_Internalname;
            wbErr = true;
            A1414ContagemResultadoNotificacao_DestCod = 0;
         }
         else
         {
            A1414ContagemResultadoNotificacao_DestCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_DestCod_Internalname), ",", "."));
         }
         A1426ContagemResultadoNotificacao_DestPesCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_DestPesCod_Internalname), ",", "."));
         n1426ContagemResultadoNotificacao_DestPesCod = false;
         A1421ContagemResultadoNotificacao_DestNome = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_DestNome_Internalname));
         n1421ContagemResultadoNotificacao_DestNome = false;
         A1419ContagemResultadoNotificacao_DestEmail = cgiGet( edtContagemResultadoNotificacao_DestEmail_Internalname);
         n1419ContagemResultadoNotificacao_DestEmail = false;
         n1419ContagemResultadoNotificacao_DestEmail = (String.IsNullOrEmpty(StringUtil.RTrim( A1419ContagemResultadoNotificacao_DestEmail)) ? true : false);
         GXCCtl = "Z1414ContagemResultadoNotificacao_DestCod_" + sGXsfl_99_idx;
         Z1414ContagemResultadoNotificacao_DestCod = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z1419ContagemResultadoNotificacao_DestEmail_" + sGXsfl_99_idx;
         Z1419ContagemResultadoNotificacao_DestEmail = cgiGet( GXCCtl);
         n1419ContagemResultadoNotificacao_DestEmail = (String.IsNullOrEmpty(StringUtil.RTrim( A1419ContagemResultadoNotificacao_DestEmail)) ? true : false);
         GXCCtl = "nRcdDeleted_168_" + sGXsfl_99_idx;
         nRcdDeleted_168 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_168_" + sGXsfl_99_idx;
         nRcdExists_168 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_168_" + sGXsfl_99_idx;
         nIsMod_168 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
      }

      protected void SubsflControlProps_111169( )
      {
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_111_idx;
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_111_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_111_idx;
         edtContagemResultado_Servico_Internalname = "CONTAGEMRESULTADO_SERVICO_"+sGXsfl_111_idx;
         edtContagemResultado_ServicoSigla_Internalname = "CONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_111_idx;
         edtContagemResultado_PrazoInicialDias_Internalname = "CONTAGEMRESULTADO_PRAZOINICIALDIAS_"+sGXsfl_111_idx;
      }

      protected void SubsflControlProps_fel_111169( )
      {
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_111_fel_idx;
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_111_fel_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_111_fel_idx;
         edtContagemResultado_Servico_Internalname = "CONTAGEMRESULTADO_SERVICO_"+sGXsfl_111_fel_idx;
         edtContagemResultado_ServicoSigla_Internalname = "CONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_111_fel_idx;
         edtContagemResultado_PrazoInicialDias_Internalname = "CONTAGEMRESULTADO_PRAZOINICIALDIAS_"+sGXsfl_111_fel_idx;
      }

      protected void AddRow3O169( )
      {
         nGXsfl_111_idx = (short)(nGXsfl_111_idx+1);
         sGXsfl_111_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_111_idx), 4, 0)), 4, "0");
         SubsflControlProps_111169( ) ;
         SendRow3O169( ) ;
      }

      protected void SendRow3O169( )
      {
         Gridlevel_demandaRow = GXWebRow.GetNew(context);
         if ( subGridlevel_demanda_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridlevel_demanda_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridlevel_demanda_Class, "") != 0 )
            {
               subGridlevel_demanda_Linesclass = subGridlevel_demanda_Class+"Odd";
            }
         }
         else if ( subGridlevel_demanda_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridlevel_demanda_Backstyle = 0;
            subGridlevel_demanda_Backcolor = subGridlevel_demanda_Allbackcolor;
            if ( StringUtil.StrCmp(subGridlevel_demanda_Class, "") != 0 )
            {
               subGridlevel_demanda_Linesclass = subGridlevel_demanda_Class+"Uniform";
            }
         }
         else if ( subGridlevel_demanda_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridlevel_demanda_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridlevel_demanda_Class, "") != 0 )
            {
               subGridlevel_demanda_Linesclass = subGridlevel_demanda_Class+"Odd";
            }
            subGridlevel_demanda_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGridlevel_demanda_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridlevel_demanda_Backstyle = 1;
            if ( ((int)((nGXsfl_111_idx) % (2))) == 0 )
            {
               subGridlevel_demanda_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridlevel_demanda_Class, "") != 0 )
               {
                  subGridlevel_demanda_Linesclass = subGridlevel_demanda_Class+"Even";
               }
            }
            else
            {
               subGridlevel_demanda_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridlevel_demanda_Class, "") != 0 )
               {
                  subGridlevel_demanda_Linesclass = subGridlevel_demanda_Class+"Odd";
               }
            }
         }
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_169_" + sGXsfl_111_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_111_idx + "',111)\"";
         ROClassString = "BootstrapAttribute";
         Gridlevel_demandaRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,112);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultado_Codigo_Enabled,(short)1,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)111,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Gridlevel_demandaRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Demanda_Internalname,(String)A457ContagemResultado_Demanda,StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Demanda_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultado_Demanda_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)111,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Gridlevel_demandaRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DemandaFM_Internalname,(String)A493ContagemResultado_DemandaFM,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DemandaFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultado_DemandaFM_Enabled,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)111,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Gridlevel_demandaRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Servico_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", "")),((edtContagemResultado_Servico_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A601ContagemResultado_Servico), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A601ContagemResultado_Servico), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Servico_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultado_Servico_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)111,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Gridlevel_demandaRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ServicoSigla_Internalname,StringUtil.RTrim( A801ContagemResultado_ServicoSigla),StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ServicoSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultado_ServicoSigla_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)111,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Gridlevel_demandaRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PrazoInicialDias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0, ",", "")),((edtContagemResultado_PrazoInicialDias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1227ContagemResultado_PrazoInicialDias), "ZZZ9")) : context.localUtil.Format( (decimal)(A1227ContagemResultado_PrazoInicialDias), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PrazoInicialDias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultado_PrazoInicialDias_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)111,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         context.httpAjaxContext.ajax_sending_grid_row(Gridlevel_demandaRow);
         GXCCtl = "Z456ContagemResultado_Codigo_" + sGXsfl_111_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z456ContagemResultado_Codigo), 6, 0, ",", "")));
         GXCCtl = "nRcdDeleted_169_" + sGXsfl_111_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_169), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_169_" + sGXsfl_111_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_169), 4, 0, ",", "")));
         GXCCtl = "nIsMod_169_" + sGXsfl_111_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_169), 4, 0, ",", "")));
         GXCCtl = "vMODE_" + sGXsfl_111_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Gx_mode));
         GXCCtl = "vTRNCONTEXT_" + sGXsfl_111_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV9TrnContext);
         }
         GXCCtl = "vCONTAGEMRESULTADONOTIFICACAO_CODIGO_" + sGXsfl_111_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Demanda_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_DemandaFM_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICO_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Servico_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_ServicoSigla_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PRAZOINICIALDIAS_"+sGXsfl_111_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_PrazoInicialDias_Enabled), 5, 0, ".", "")));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridlevel_demandaContainer.AddRow(Gridlevel_demandaRow);
      }

      protected void ReadRow3O169( )
      {
         nGXsfl_111_idx = (short)(nGXsfl_111_idx+1);
         sGXsfl_111_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_111_idx), 4, 0)), 4, "0");
         SubsflControlProps_111169( ) ;
         edtContagemResultado_Codigo_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_111_idx+"Enabled"), ",", "."));
         edtContagemResultado_Demanda_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_111_idx+"Enabled"), ",", "."));
         edtContagemResultado_DemandaFM_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_111_idx+"Enabled"), ",", "."));
         edtContagemResultado_Servico_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_SERVICO_"+sGXsfl_111_idx+"Enabled"), ",", "."));
         edtContagemResultado_ServicoSigla_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_111_idx+"Enabled"), ",", "."));
         edtContagemResultado_PrazoInicialDias_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_PRAZOINICIALDIAS_"+sGXsfl_111_idx+"Enabled"), ",", "."));
         if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
         {
            GXCCtl = "CONTAGEMRESULTADO_CODIGO_" + sGXsfl_111_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            wbErr = true;
            A456ContagemResultado_Codigo = 0;
         }
         else
         {
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
         }
         A457ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtContagemResultado_Demanda_Internalname));
         n457ContagemResultado_Demanda = false;
         A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
         n493ContagemResultado_DemandaFM = false;
         A601ContagemResultado_Servico = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Servico_Internalname), ",", "."));
         n601ContagemResultado_Servico = false;
         A801ContagemResultado_ServicoSigla = StringUtil.Upper( cgiGet( edtContagemResultado_ServicoSigla_Internalname));
         n801ContagemResultado_ServicoSigla = false;
         A1227ContagemResultado_PrazoInicialDias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultado_PrazoInicialDias_Internalname), ",", "."));
         n1227ContagemResultado_PrazoInicialDias = false;
         GXCCtl = "Z456ContagemResultado_Codigo_" + sGXsfl_111_idx;
         Z456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdDeleted_169_" + sGXsfl_111_idx;
         nRcdDeleted_169 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_169_" + sGXsfl_111_idx;
         nRcdExists_169 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_169_" + sGXsfl_111_idx;
         nIsMod_169 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
      }

      protected void assign_properties_default( )
      {
         defedtContagemResultadoNotificacao_DestCod_Enabled = edtContagemResultadoNotificacao_DestCod_Enabled;
         defedtContagemResultado_Codigo_Enabled = edtContagemResultado_Codigo_Enabled;
      }

      protected void ConfirmValues3O0( )
      {
         nGXsfl_99_idx = 0;
         sGXsfl_99_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_99_idx), 4, 0)), 4, "0");
         SubsflControlProps_99168( ) ;
         while ( nGXsfl_99_idx < nRC_GXsfl_99 )
         {
            nGXsfl_99_idx = (short)(nGXsfl_99_idx+1);
            sGXsfl_99_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_99_idx), 4, 0)), 4, "0");
            SubsflControlProps_99168( ) ;
            ChangePostValue( "Z1414ContagemResultadoNotificacao_DestCod_"+sGXsfl_99_idx, cgiGet( "ZT_"+"Z1414ContagemResultadoNotificacao_DestCod_"+sGXsfl_99_idx)) ;
            DeletePostValue( "ZT_"+"Z1414ContagemResultadoNotificacao_DestCod_"+sGXsfl_99_idx) ;
            ChangePostValue( "Z1419ContagemResultadoNotificacao_DestEmail_"+sGXsfl_99_idx, cgiGet( "ZT_"+"Z1419ContagemResultadoNotificacao_DestEmail_"+sGXsfl_99_idx)) ;
            DeletePostValue( "ZT_"+"Z1419ContagemResultadoNotificacao_DestEmail_"+sGXsfl_99_idx) ;
         }
         nGXsfl_111_idx = 0;
         sGXsfl_111_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_111_idx), 4, 0)), 4, "0");
         SubsflControlProps_111169( ) ;
         while ( nGXsfl_111_idx < nRC_GXsfl_111 )
         {
            nGXsfl_111_idx = (short)(nGXsfl_111_idx+1);
            sGXsfl_111_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_111_idx), 4, 0)), 4, "0");
            SubsflControlProps_111169( ) ;
            ChangePostValue( "Z456ContagemResultado_Codigo_"+sGXsfl_111_idx, cgiGet( "ZT_"+"Z456ContagemResultado_Codigo_"+sGXsfl_111_idx)) ;
            DeletePostValue( "ZT_"+"Z456ContagemResultado_Codigo_"+sGXsfl_111_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020621617450");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadonotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContagemResultadoNotificacao_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1412ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1416ContagemResultadoNotificacao_DataHora", context.localUtil.TToC( Z1416ContagemResultadoNotificacao_DataHora, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1418ContagemResultadoNotificacao_CorpoEmail", Z1418ContagemResultadoNotificacao_CorpoEmail);
         GxWebStd.gx_hidden_field( context, "Z1420ContagemResultadoNotificacao_Midia", StringUtil.RTrim( Z1420ContagemResultadoNotificacao_Midia));
         GxWebStd.gx_hidden_field( context, "Z1956ContagemResultadoNotificacao_Host", StringUtil.RTrim( Z1956ContagemResultadoNotificacao_Host));
         GxWebStd.gx_hidden_field( context, "Z1957ContagemResultadoNotificacao_User", StringUtil.RTrim( Z1957ContagemResultadoNotificacao_User));
         GxWebStd.gx_hidden_field( context, "Z1958ContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1958ContagemResultadoNotificacao_Port), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1959ContagemResultadoNotificacao_Aut", Z1959ContagemResultadoNotificacao_Aut);
         GxWebStd.gx_hidden_field( context, "Z1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1960ContagemResultadoNotificacao_Sec), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1961ContagemResultadoNotificacao_Logged", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1961ContagemResultadoNotificacao_Logged), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1413ContagemResultadoNotificacao_UsuCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1413ContagemResultadoNotificacao_UsuCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1966ContagemResultadoNotificacao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1966ContagemResultadoNotificacao_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1965ContagemResultadoNotificacao_ReenvioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1965ContagemResultadoNotificacao_ReenvioCod), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.NToC( (decimal)(O1962ContagemResultadoNotificacao_Destinatarios), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_99", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_99_idx), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_111", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_111_idx), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1966ContagemResultadoNotificacao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1966ContagemResultadoNotificacao_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1413ContagemResultadoNotificacao_UsuCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1965ContagemResultadoNotificacao_ReenvioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1965ContagemResultadoNotificacao_ReenvioCod), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADONOTIFICACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEMRESULTADONOTIFICACAO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Insert_ContagemResultadoNotificacao_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1966ContagemResultadoNotificacao_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEMRESULTADONOTIFICACAO_USUCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ContagemResultadoNotificacao_UsuCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEMRESULTADONOTIFICACAO_REENVIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Insert_ContagemResultadoNotificacao_ReenvioCod), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_REENVIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1965ContagemResultadoNotificacao_ReenvioCod), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO", A1415ContagemResultadoNotificacao_Observacao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_DEMANDAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1963ContagemResultadoNotificacao_Demandas), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV16Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Enabled", StringUtil.BoolToStr( Contagemresultadonotificacao_observacao_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Width", StringUtil.RTrim( Dvpanel_tableleaflevel_destinatario_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Cls", StringUtil.RTrim( Dvpanel_tableleaflevel_destinatario_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Title", StringUtil.RTrim( Dvpanel_tableleaflevel_destinatario_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Collapsible", StringUtil.BoolToStr( Dvpanel_tableleaflevel_destinatario_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Collapsed", StringUtil.BoolToStr( Dvpanel_tableleaflevel_destinatario_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Enabled", StringUtil.BoolToStr( Dvpanel_tableleaflevel_destinatario_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Autowidth", StringUtil.BoolToStr( Dvpanel_tableleaflevel_destinatario_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Autoheight", StringUtil.BoolToStr( Dvpanel_tableleaflevel_destinatario_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableleaflevel_destinatario_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Iconposition", StringUtil.RTrim( Dvpanel_tableleaflevel_destinatario_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DESTINATARIO_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableleaflevel_destinatario_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DEMANDA_Width", StringUtil.RTrim( Dvpanel_tableleaflevel_demanda_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DEMANDA_Cls", StringUtil.RTrim( Dvpanel_tableleaflevel_demanda_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DEMANDA_Title", StringUtil.RTrim( Dvpanel_tableleaflevel_demanda_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DEMANDA_Collapsible", StringUtil.BoolToStr( Dvpanel_tableleaflevel_demanda_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DEMANDA_Collapsed", StringUtil.BoolToStr( Dvpanel_tableleaflevel_demanda_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DEMANDA_Enabled", StringUtil.BoolToStr( Dvpanel_tableleaflevel_demanda_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DEMANDA_Autowidth", StringUtil.BoolToStr( Dvpanel_tableleaflevel_demanda_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DEMANDA_Autoheight", StringUtil.BoolToStr( Dvpanel_tableleaflevel_demanda_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DEMANDA_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableleaflevel_demanda_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DEMANDA_Iconposition", StringUtil.RTrim( Dvpanel_tableleaflevel_demanda_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLELEAFLEVEL_DEMANDA_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableleaflevel_demanda_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContagemResultadoNotificacao";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1966ContagemResultadoNotificacao_AreaTrabalhoCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1965ContagemResultadoNotificacao_ReenvioCod), "ZZZZZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemresultadonotificacao:[SendSecurityCheck value for]"+"ContagemResultadoNotificacao_Codigo:"+context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9"));
         GXUtil.WriteLog("contagemresultadonotificacao:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contagemresultadonotificacao:[SendSecurityCheck value for]"+"ContagemResultadoNotificacao_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A1966ContagemResultadoNotificacao_AreaTrabalhoCod), "ZZZZZ9"));
         GXUtil.WriteLog("contagemresultadonotificacao:[SendSecurityCheck value for]"+"ContagemResultadoNotificacao_ReenvioCod:"+context.localUtil.Format( (decimal)(A1965ContagemResultadoNotificacao_ReenvioCod), "ZZZZZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadonotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContagemResultadoNotificacao_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoNotificacao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Notifica��es da Contagem" ;
      }

      protected void InitializeNonKey3O167( )
      {
         A1966ContagemResultadoNotificacao_AreaTrabalhoCod = 0;
         n1966ContagemResultadoNotificacao_AreaTrabalhoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1966ContagemResultadoNotificacao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1966ContagemResultadoNotificacao_AreaTrabalhoCod), 6, 0)));
         A1413ContagemResultadoNotificacao_UsuCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1413ContagemResultadoNotificacao_UsuCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0)));
         A1965ContagemResultadoNotificacao_ReenvioCod = 0;
         n1965ContagemResultadoNotificacao_ReenvioCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1965ContagemResultadoNotificacao_ReenvioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1965ContagemResultadoNotificacao_ReenvioCod), 10, 0)));
         A1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         n1416ContagemResultadoNotificacao_DataHora = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1416ContagemResultadoNotificacao_DataHora", context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
         n1416ContagemResultadoNotificacao_DataHora = ((DateTime.MinValue==A1416ContagemResultadoNotificacao_DataHora) ? true : false);
         A1427ContagemResultadoNotificacao_UsuPesCod = 0;
         n1427ContagemResultadoNotificacao_UsuPesCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1427ContagemResultadoNotificacao_UsuPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), 6, 0)));
         A1422ContagemResultadoNotificacao_UsuNom = "";
         n1422ContagemResultadoNotificacao_UsuNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1422ContagemResultadoNotificacao_UsuNom", A1422ContagemResultadoNotificacao_UsuNom);
         A1417ContagemResultadoNotificacao_Assunto = "";
         n1417ContagemResultadoNotificacao_Assunto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1417ContagemResultadoNotificacao_Assunto", A1417ContagemResultadoNotificacao_Assunto);
         n1417ContagemResultadoNotificacao_Assunto = (String.IsNullOrEmpty(StringUtil.RTrim( A1417ContagemResultadoNotificacao_Assunto)) ? true : false);
         A1418ContagemResultadoNotificacao_CorpoEmail = "";
         n1418ContagemResultadoNotificacao_CorpoEmail = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1418ContagemResultadoNotificacao_CorpoEmail", A1418ContagemResultadoNotificacao_CorpoEmail);
         n1418ContagemResultadoNotificacao_CorpoEmail = (String.IsNullOrEmpty(StringUtil.RTrim( A1418ContagemResultadoNotificacao_CorpoEmail)) ? true : false);
         A1420ContagemResultadoNotificacao_Midia = "";
         n1420ContagemResultadoNotificacao_Midia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1420ContagemResultadoNotificacao_Midia", A1420ContagemResultadoNotificacao_Midia);
         n1420ContagemResultadoNotificacao_Midia = (String.IsNullOrEmpty(StringUtil.RTrim( A1420ContagemResultadoNotificacao_Midia)) ? true : false);
         A1415ContagemResultadoNotificacao_Observacao = "";
         n1415ContagemResultadoNotificacao_Observacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1415ContagemResultadoNotificacao_Observacao", A1415ContagemResultadoNotificacao_Observacao);
         A1956ContagemResultadoNotificacao_Host = "";
         n1956ContagemResultadoNotificacao_Host = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1956ContagemResultadoNotificacao_Host", A1956ContagemResultadoNotificacao_Host);
         n1956ContagemResultadoNotificacao_Host = (String.IsNullOrEmpty(StringUtil.RTrim( A1956ContagemResultadoNotificacao_Host)) ? true : false);
         A1957ContagemResultadoNotificacao_User = "";
         n1957ContagemResultadoNotificacao_User = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1957ContagemResultadoNotificacao_User", A1957ContagemResultadoNotificacao_User);
         n1957ContagemResultadoNotificacao_User = (String.IsNullOrEmpty(StringUtil.RTrim( A1957ContagemResultadoNotificacao_User)) ? true : false);
         A1958ContagemResultadoNotificacao_Port = 0;
         n1958ContagemResultadoNotificacao_Port = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1958ContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.Str( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0)));
         n1958ContagemResultadoNotificacao_Port = ((0==A1958ContagemResultadoNotificacao_Port) ? true : false);
         A1959ContagemResultadoNotificacao_Aut = false;
         n1959ContagemResultadoNotificacao_Aut = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1959ContagemResultadoNotificacao_Aut", A1959ContagemResultadoNotificacao_Aut);
         n1959ContagemResultadoNotificacao_Aut = ((false==A1959ContagemResultadoNotificacao_Aut) ? true : false);
         A1960ContagemResultadoNotificacao_Sec = 0;
         n1960ContagemResultadoNotificacao_Sec = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)));
         n1960ContagemResultadoNotificacao_Sec = ((0==A1960ContagemResultadoNotificacao_Sec) ? true : false);
         A1961ContagemResultadoNotificacao_Logged = 0;
         n1961ContagemResultadoNotificacao_Logged = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1961ContagemResultadoNotificacao_Logged", StringUtil.LTrim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)));
         n1961ContagemResultadoNotificacao_Logged = ((0==A1961ContagemResultadoNotificacao_Logged) ? true : false);
         A1962ContagemResultadoNotificacao_Destinatarios = 0;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
         A1963ContagemResultadoNotificacao_Demandas = 0;
         n1963ContagemResultadoNotificacao_Demandas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1963ContagemResultadoNotificacao_Demandas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1963ContagemResultadoNotificacao_Demandas), 6, 0)));
         O1962ContagemResultadoNotificacao_Destinatarios = A1962ContagemResultadoNotificacao_Destinatarios;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1962ContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0)));
         Z1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         Z1418ContagemResultadoNotificacao_CorpoEmail = "";
         Z1420ContagemResultadoNotificacao_Midia = "";
         Z1956ContagemResultadoNotificacao_Host = "";
         Z1957ContagemResultadoNotificacao_User = "";
         Z1958ContagemResultadoNotificacao_Port = 0;
         Z1959ContagemResultadoNotificacao_Aut = false;
         Z1960ContagemResultadoNotificacao_Sec = 0;
         Z1961ContagemResultadoNotificacao_Logged = 0;
         Z1413ContagemResultadoNotificacao_UsuCod = 0;
         Z1966ContagemResultadoNotificacao_AreaTrabalhoCod = 0;
         Z1965ContagemResultadoNotificacao_ReenvioCod = 0;
      }

      protected void InitAll3O167( )
      {
         A1412ContagemResultadoNotificacao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
         InitializeNonKey3O167( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey3O168( )
      {
         A1426ContagemResultadoNotificacao_DestPesCod = 0;
         n1426ContagemResultadoNotificacao_DestPesCod = false;
         A1421ContagemResultadoNotificacao_DestNome = "";
         n1421ContagemResultadoNotificacao_DestNome = false;
         A1419ContagemResultadoNotificacao_DestEmail = "";
         n1419ContagemResultadoNotificacao_DestEmail = false;
         Z1419ContagemResultadoNotificacao_DestEmail = "";
      }

      protected void InitAll3O168( )
      {
         A1414ContagemResultadoNotificacao_DestCod = 0;
         InitializeNonKey3O168( ) ;
      }

      protected void StandaloneModalInsert3O168( )
      {
      }

      protected void InitializeNonKey3O169( )
      {
         A1553ContagemResultado_CntSrvCod = 0;
         n1553ContagemResultado_CntSrvCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
         A457ContagemResultado_Demanda = "";
         n457ContagemResultado_Demanda = false;
         A493ContagemResultado_DemandaFM = "";
         n493ContagemResultado_DemandaFM = false;
         A601ContagemResultado_Servico = 0;
         n601ContagemResultado_Servico = false;
         A801ContagemResultado_ServicoSigla = "";
         n801ContagemResultado_ServicoSigla = false;
         A1227ContagemResultado_PrazoInicialDias = 0;
         n1227ContagemResultado_PrazoInicialDias = false;
      }

      protected void InitAll3O169( )
      {
         A456ContagemResultado_Codigo = 0;
         InitializeNonKey3O169( ) ;
      }

      protected void StandaloneModalInsert3O169( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216174576");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadonotificacao.js", "?20206216174577");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_level_properties168( )
      {
         edtContagemResultadoNotificacao_DestCod_Enabled = defedtContagemResultadoNotificacao_DestCod_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DestCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_DestCod_Enabled), 5, 0)));
      }

      protected void init_level_properties169( )
      {
         edtContagemResultado_Codigo_Enabled = defedtContagemResultado_Codigo_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
      }

      protected void init_default_properties( )
      {
         lblContagemresultadonotificacaotitle_Internalname = "CONTAGEMRESULTADONOTIFICACAOTITLE";
         lblTextblockcontagemresultadonotificacao_codigo_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_CODIGO";
         edtContagemResultadoNotificacao_Codigo_Internalname = "CONTAGEMRESULTADONOTIFICACAO_CODIGO";
         lblTextblockcontagemresultadonotificacao_datahora_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         edtContagemResultadoNotificacao_DataHora_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         lblTextblockcontagemresultadonotificacao_usucod_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_USUCOD";
         edtContagemResultadoNotificacao_UsuCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USUCOD";
         lblTextblockcontagemresultadonotificacao_usupescod_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_USUPESCOD";
         edtContagemResultadoNotificacao_UsuPesCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USUPESCOD";
         lblTextblockcontagemresultadonotificacao_usunom_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_USUNOM";
         edtContagemResultadoNotificacao_UsuNom_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USUNOM";
         lblTextblockcontagemresultadonotificacao_assunto_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_ASSUNTO";
         edtContagemResultadoNotificacao_Assunto_Internalname = "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO";
         lblTextblockcontagemresultadonotificacao_corpoemail_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL";
         edtContagemResultadoNotificacao_CorpoEmail_Internalname = "CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL";
         lblTextblockcontagemresultadonotificacao_midia_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_MIDIA";
         cmbContagemResultadoNotificacao_Midia_Internalname = "CONTAGEMRESULTADONOTIFICACAO_MIDIA";
         lblTextblockcontagemresultadonotificacao_observacao_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_OBSERVACAO";
         Contagemresultadonotificacao_observacao_Internalname = "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO";
         lblTextblockcontagemresultadonotificacao_host_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_HOST";
         edtContagemResultadoNotificacao_Host_Internalname = "CONTAGEMRESULTADONOTIFICACAO_HOST";
         lblTextblockcontagemresultadonotificacao_user_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_USER";
         edtContagemResultadoNotificacao_User_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USER";
         lblTextblockcontagemresultadonotificacao_port_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_PORT";
         edtContagemResultadoNotificacao_Port_Internalname = "CONTAGEMRESULTADONOTIFICACAO_PORT";
         lblTextblockcontagemresultadonotificacao_aut_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_AUT";
         cmbContagemResultadoNotificacao_Aut_Internalname = "CONTAGEMRESULTADONOTIFICACAO_AUT";
         lblTextblockcontagemresultadonotificacao_sec_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_SEC";
         cmbContagemResultadoNotificacao_Sec_Internalname = "CONTAGEMRESULTADONOTIFICACAO_SEC";
         lblTextblockcontagemresultadonotificacao_logged_Internalname = "TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_LOGGED";
         cmbContagemResultadoNotificacao_Logged_Internalname = "CONTAGEMRESULTADONOTIFICACAO_LOGGED";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         edtContagemResultadoNotificacao_DestCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTCOD";
         edtContagemResultadoNotificacao_DestPesCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTPESCOD";
         edtContagemResultadoNotificacao_DestNome_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTNOME";
         edtContagemResultadoNotificacao_DestEmail_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL";
         tblTableleaflevel_destinatario_Internalname = "TABLELEAFLEVEL_DESTINATARIO";
         Dvpanel_tableleaflevel_destinatario_Internalname = "DVPANEL_TABLELEAFLEVEL_DESTINATARIO";
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO";
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA";
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM";
         edtContagemResultado_Servico_Internalname = "CONTAGEMRESULTADO_SERVICO";
         edtContagemResultado_ServicoSigla_Internalname = "CONTAGEMRESULTADO_SERVICOSIGLA";
         edtContagemResultado_PrazoInicialDias_Internalname = "CONTAGEMRESULTADO_PRAZOINICIALDIAS";
         tblTableleaflevel_demanda_Internalname = "TABLELEAFLEVEL_DEMANDA";
         Dvpanel_tableleaflevel_demanda_Internalname = "DVPANEL_TABLELEAFLEVEL_DEMANDA";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         imgprompt_1413_Internalname = "PROMPT_1413";
         imgprompt_1414_Internalname = "PROMPT_1414";
         subGridlevel_destinatario_Internalname = "GRIDLEVEL_DESTINATARIO";
         subGridlevel_demanda_Internalname = "GRIDLEVEL_DEMANDA";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableleaflevel_demanda_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableleaflevel_demanda_Iconposition = "left";
         Dvpanel_tableleaflevel_demanda_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableleaflevel_demanda_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableleaflevel_demanda_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableleaflevel_demanda_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableleaflevel_demanda_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableleaflevel_demanda_Title = "Demanda";
         Dvpanel_tableleaflevel_demanda_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableleaflevel_demanda_Width = "100%";
         Dvpanel_tableleaflevel_destinatario_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableleaflevel_destinatario_Iconposition = "left";
         Dvpanel_tableleaflevel_destinatario_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableleaflevel_destinatario_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableleaflevel_destinatario_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableleaflevel_destinatario_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableleaflevel_destinatario_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableleaflevel_destinatario_Title = "Destinatario";
         Dvpanel_tableleaflevel_destinatario_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableleaflevel_destinatario_Width = "100%";
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Informa��es Gerais";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Notifica��es da Contagem";
         edtContagemResultado_PrazoInicialDias_Jsonclick = "";
         edtContagemResultado_ServicoSigla_Jsonclick = "";
         edtContagemResultado_Servico_Jsonclick = "";
         edtContagemResultado_DemandaFM_Jsonclick = "";
         edtContagemResultado_Demanda_Jsonclick = "";
         edtContagemResultado_Codigo_Jsonclick = "";
         subGridlevel_demanda_Class = "WorkWithBorder WorkWith";
         edtContagemResultadoNotificacao_DestEmail_Jsonclick = "";
         edtContagemResultadoNotificacao_DestNome_Jsonclick = "";
         edtContagemResultadoNotificacao_DestPesCod_Jsonclick = "";
         imgprompt_1414_Visible = 1;
         imgprompt_1414_Link = "";
         imgprompt_1413_Visible = 1;
         edtContagemResultadoNotificacao_DestCod_Jsonclick = "";
         subGridlevel_destinatario_Class = "WorkWithBorder WorkWith";
         cmbContagemResultadoNotificacao_Logged_Jsonclick = "";
         cmbContagemResultadoNotificacao_Logged.Enabled = 1;
         cmbContagemResultadoNotificacao_Sec_Jsonclick = "";
         cmbContagemResultadoNotificacao_Sec.Enabled = 1;
         cmbContagemResultadoNotificacao_Aut_Jsonclick = "";
         cmbContagemResultadoNotificacao_Aut.Enabled = 1;
         edtContagemResultadoNotificacao_Port_Jsonclick = "";
         edtContagemResultadoNotificacao_Port_Enabled = 1;
         edtContagemResultadoNotificacao_User_Jsonclick = "";
         edtContagemResultadoNotificacao_User_Enabled = 1;
         edtContagemResultadoNotificacao_Host_Jsonclick = "";
         edtContagemResultadoNotificacao_Host_Enabled = 1;
         Contagemresultadonotificacao_observacao_Enabled = Convert.ToBoolean( 1);
         cmbContagemResultadoNotificacao_Midia_Jsonclick = "";
         cmbContagemResultadoNotificacao_Midia.Enabled = 1;
         edtContagemResultadoNotificacao_CorpoEmail_Enabled = 1;
         edtContagemResultadoNotificacao_Assunto_Enabled = 1;
         edtContagemResultadoNotificacao_UsuNom_Jsonclick = "";
         edtContagemResultadoNotificacao_UsuNom_Enabled = 0;
         edtContagemResultadoNotificacao_UsuPesCod_Jsonclick = "";
         edtContagemResultadoNotificacao_UsuPesCod_Enabled = 0;
         imgprompt_1413_Visible = 1;
         imgprompt_1413_Link = "";
         edtContagemResultadoNotificacao_UsuCod_Jsonclick = "";
         edtContagemResultadoNotificacao_UsuCod_Enabled = 1;
         edtContagemResultadoNotificacao_DataHora_Jsonclick = "";
         edtContagemResultadoNotificacao_DataHora_Enabled = 1;
         edtContagemResultadoNotificacao_Codigo_Jsonclick = "";
         edtContagemResultadoNotificacao_Codigo_Enabled = 0;
         subGridlevel_destinatario_Allowcollapsing = 0;
         subGridlevel_destinatario_Allowselection = 0;
         edtContagemResultadoNotificacao_DestEmail_Enabled = 1;
         edtContagemResultadoNotificacao_DestNome_Enabled = 0;
         edtContagemResultadoNotificacao_DestPesCod_Enabled = 0;
         edtContagemResultadoNotificacao_DestCod_Enabled = 1;
         subGridlevel_destinatario_Backcolorstyle = 3;
         subGridlevel_demanda_Allowcollapsing = 0;
         subGridlevel_demanda_Allowselection = 0;
         edtContagemResultado_PrazoInicialDias_Enabled = 0;
         edtContagemResultado_ServicoSigla_Enabled = 0;
         edtContagemResultado_Servico_Enabled = 0;
         edtContagemResultado_DemandaFM_Enabled = 0;
         edtContagemResultado_Demanda_Enabled = 0;
         edtContagemResultado_Codigo_Enabled = 1;
         subGridlevel_demanda_Backcolorstyle = 3;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridlevel_destinatario_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         SubsflControlProps_99168( ) ;
         while ( nGXsfl_99_idx <= nRC_GXsfl_99 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal3O168( ) ;
            standaloneModal3O168( ) ;
            cmbContagemResultadoNotificacao_Midia.Name = "CONTAGEMRESULTADONOTIFICACAO_MIDIA";
            cmbContagemResultadoNotificacao_Midia.WebTags = "";
            cmbContagemResultadoNotificacao_Midia.addItem("EML", "Email", 0);
            cmbContagemResultadoNotificacao_Midia.addItem("SMS", "SMS", 0);
            cmbContagemResultadoNotificacao_Midia.addItem("TEL", "Telefone", 0);
            if ( cmbContagemResultadoNotificacao_Midia.ItemCount > 0 )
            {
               A1420ContagemResultadoNotificacao_Midia = cmbContagemResultadoNotificacao_Midia.getValidValue(A1420ContagemResultadoNotificacao_Midia);
               n1420ContagemResultadoNotificacao_Midia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1420ContagemResultadoNotificacao_Midia", A1420ContagemResultadoNotificacao_Midia);
            }
            cmbContagemResultadoNotificacao_Aut.Name = "CONTAGEMRESULTADONOTIFICACAO_AUT";
            cmbContagemResultadoNotificacao_Aut.WebTags = "";
            cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContagemResultadoNotificacao_Aut.ItemCount > 0 )
            {
               A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cmbContagemResultadoNotificacao_Aut.getValidValue(StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut)));
               n1959ContagemResultadoNotificacao_Aut = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1959ContagemResultadoNotificacao_Aut", A1959ContagemResultadoNotificacao_Aut);
            }
            cmbContagemResultadoNotificacao_Sec.Name = "CONTAGEMRESULTADONOTIFICACAO_SEC";
            cmbContagemResultadoNotificacao_Sec.WebTags = "";
            cmbContagemResultadoNotificacao_Sec.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Nenhuma", 0);
            cmbContagemResultadoNotificacao_Sec.addItem("1", "SSL e TLS", 0);
            if ( cmbContagemResultadoNotificacao_Sec.ItemCount > 0 )
            {
               A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Sec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0))), "."));
               n1960ContagemResultadoNotificacao_Sec = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)));
            }
            cmbContagemResultadoNotificacao_Logged.Name = "CONTAGEMRESULTADONOTIFICACAO_LOGGED";
            cmbContagemResultadoNotificacao_Logged.WebTags = "";
            cmbContagemResultadoNotificacao_Logged.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Sem dados", 0);
            cmbContagemResultadoNotificacao_Logged.addItem("1", "Com sucesso", 0);
            cmbContagemResultadoNotificacao_Logged.addItem("2", "Mal sucedido", 0);
            if ( cmbContagemResultadoNotificacao_Logged.ItemCount > 0 )
            {
               A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Logged.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0))), "."));
               n1961ContagemResultadoNotificacao_Logged = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1961ContagemResultadoNotificacao_Logged", StringUtil.LTrim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)));
            }
            dynload_actions( ) ;
            SendRow3O168( ) ;
            nGXsfl_99_idx = (short)(nGXsfl_99_idx+1);
            sGXsfl_99_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_99_idx), 4, 0)), 4, "0");
            SubsflControlProps_99168( ) ;
         }
         context.GX_webresponse.AddString(Gridlevel_destinatarioContainer.ToJavascriptSource());
         /* End function gxnrGridlevel_destinatario_newrow */
      }

      protected void gxnrGridlevel_demanda_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         SubsflControlProps_111169( ) ;
         while ( nGXsfl_111_idx <= nRC_GXsfl_111 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal3O169( ) ;
            standaloneModal3O169( ) ;
            cmbContagemResultadoNotificacao_Midia.Name = "CONTAGEMRESULTADONOTIFICACAO_MIDIA";
            cmbContagemResultadoNotificacao_Midia.WebTags = "";
            cmbContagemResultadoNotificacao_Midia.addItem("EML", "Email", 0);
            cmbContagemResultadoNotificacao_Midia.addItem("SMS", "SMS", 0);
            cmbContagemResultadoNotificacao_Midia.addItem("TEL", "Telefone", 0);
            if ( cmbContagemResultadoNotificacao_Midia.ItemCount > 0 )
            {
               A1420ContagemResultadoNotificacao_Midia = cmbContagemResultadoNotificacao_Midia.getValidValue(A1420ContagemResultadoNotificacao_Midia);
               n1420ContagemResultadoNotificacao_Midia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1420ContagemResultadoNotificacao_Midia", A1420ContagemResultadoNotificacao_Midia);
            }
            cmbContagemResultadoNotificacao_Aut.Name = "CONTAGEMRESULTADONOTIFICACAO_AUT";
            cmbContagemResultadoNotificacao_Aut.WebTags = "";
            cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContagemResultadoNotificacao_Aut.ItemCount > 0 )
            {
               A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cmbContagemResultadoNotificacao_Aut.getValidValue(StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut)));
               n1959ContagemResultadoNotificacao_Aut = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1959ContagemResultadoNotificacao_Aut", A1959ContagemResultadoNotificacao_Aut);
            }
            cmbContagemResultadoNotificacao_Sec.Name = "CONTAGEMRESULTADONOTIFICACAO_SEC";
            cmbContagemResultadoNotificacao_Sec.WebTags = "";
            cmbContagemResultadoNotificacao_Sec.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Nenhuma", 0);
            cmbContagemResultadoNotificacao_Sec.addItem("1", "SSL e TLS", 0);
            if ( cmbContagemResultadoNotificacao_Sec.ItemCount > 0 )
            {
               A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Sec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0))), "."));
               n1960ContagemResultadoNotificacao_Sec = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)));
            }
            cmbContagemResultadoNotificacao_Logged.Name = "CONTAGEMRESULTADONOTIFICACAO_LOGGED";
            cmbContagemResultadoNotificacao_Logged.WebTags = "";
            cmbContagemResultadoNotificacao_Logged.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Sem dados", 0);
            cmbContagemResultadoNotificacao_Logged.addItem("1", "Com sucesso", 0);
            cmbContagemResultadoNotificacao_Logged.addItem("2", "Mal sucedido", 0);
            if ( cmbContagemResultadoNotificacao_Logged.ItemCount > 0 )
            {
               A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Logged.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0))), "."));
               n1961ContagemResultadoNotificacao_Logged = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1961ContagemResultadoNotificacao_Logged", StringUtil.LTrim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)));
            }
            dynload_actions( ) ;
            SendRow3O169( ) ;
            nGXsfl_111_idx = (short)(nGXsfl_111_idx+1);
            sGXsfl_111_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_111_idx), 4, 0)), 4, "0");
            SubsflControlProps_111169( ) ;
         }
         context.GX_webresponse.AddString(Gridlevel_demandaContainer.ToJavascriptSource());
         /* End function gxnrGridlevel_demanda_newrow */
      }

      public void Valid_Contagemresultadonotificacao_codigo( long GX_Parm1 ,
                                                             int GX_Parm2 ,
                                                             int GX_Parm3 )
      {
         A1412ContagemResultadoNotificacao_Codigo = GX_Parm1;
         A1962ContagemResultadoNotificacao_Destinatarios = GX_Parm2;
         n1962ContagemResultadoNotificacao_Destinatarios = false;
         A1963ContagemResultadoNotificacao_Demandas = GX_Parm3;
         n1963ContagemResultadoNotificacao_Demandas = false;
         /* Using cursor T003O39 */
         pr_default.execute(30, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(30) != 101) )
         {
            A1962ContagemResultadoNotificacao_Destinatarios = T003O39_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = T003O39_n1962ContagemResultadoNotificacao_Destinatarios[0];
         }
         else
         {
            A1962ContagemResultadoNotificacao_Destinatarios = 0;
            n1962ContagemResultadoNotificacao_Destinatarios = false;
         }
         pr_default.close(30);
         /* Using cursor T003O41 */
         pr_default.execute(31, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
         if ( (pr_default.getStatus(31) != 101) )
         {
            A1963ContagemResultadoNotificacao_Demandas = T003O41_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = T003O41_n1963ContagemResultadoNotificacao_Demandas[0];
         }
         else
         {
            A1963ContagemResultadoNotificacao_Demandas = 0;
            n1963ContagemResultadoNotificacao_Demandas = false;
         }
         pr_default.close(31);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1962ContagemResultadoNotificacao_Destinatarios = 0;
            n1962ContagemResultadoNotificacao_Destinatarios = false;
            A1963ContagemResultadoNotificacao_Demandas = 0;
            n1963ContagemResultadoNotificacao_Demandas = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1963ContagemResultadoNotificacao_Demandas), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadonotificacao_usucod( int GX_Parm1 ,
                                                             int GX_Parm2 ,
                                                             String GX_Parm3 )
      {
         A1413ContagemResultadoNotificacao_UsuCod = GX_Parm1;
         A1427ContagemResultadoNotificacao_UsuPesCod = GX_Parm2;
         n1427ContagemResultadoNotificacao_UsuPesCod = false;
         A1422ContagemResultadoNotificacao_UsuNom = GX_Parm3;
         n1422ContagemResultadoNotificacao_UsuNom = false;
         /* Using cursor T003O42 */
         pr_default.execute(32, new Object[] {A1413ContagemResultadoNotificacao_UsuCod});
         if ( (pr_default.getStatus(32) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usu�rio Respons�vel pela Gera��o da Notifica��o'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONOTIFICACAO_USUCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNotificacao_UsuCod_Internalname;
         }
         A1427ContagemResultadoNotificacao_UsuPesCod = T003O42_A1427ContagemResultadoNotificacao_UsuPesCod[0];
         n1427ContagemResultadoNotificacao_UsuPesCod = T003O42_n1427ContagemResultadoNotificacao_UsuPesCod[0];
         pr_default.close(32);
         /* Using cursor T003O43 */
         pr_default.execute(33, new Object[] {n1427ContagemResultadoNotificacao_UsuPesCod, A1427ContagemResultadoNotificacao_UsuPesCod});
         if ( (pr_default.getStatus(33) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1422ContagemResultadoNotificacao_UsuNom = T003O43_A1422ContagemResultadoNotificacao_UsuNom[0];
         n1422ContagemResultadoNotificacao_UsuNom = T003O43_n1422ContagemResultadoNotificacao_UsuNom[0];
         pr_default.close(33);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1427ContagemResultadoNotificacao_UsuPesCod = 0;
            n1427ContagemResultadoNotificacao_UsuPesCod = false;
            A1422ContagemResultadoNotificacao_UsuNom = "";
            n1422ContagemResultadoNotificacao_UsuNom = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1422ContagemResultadoNotificacao_UsuNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadonotificacao_destcod( String GX_Parm1 ,
                                                              int GX_Parm2 ,
                                                              int GX_Parm3 ,
                                                              int GX_Parm4 ,
                                                              String GX_Parm5 )
      {
         Gx_mode = GX_Parm1;
         O1962ContagemResultadoNotificacao_Destinatarios = GX_Parm2;
         A1414ContagemResultadoNotificacao_DestCod = GX_Parm3;
         A1426ContagemResultadoNotificacao_DestPesCod = GX_Parm4;
         n1426ContagemResultadoNotificacao_DestPesCod = false;
         A1421ContagemResultadoNotificacao_DestNome = GX_Parm5;
         n1421ContagemResultadoNotificacao_DestNome = false;
         /* Using cursor T003O53 */
         pr_default.execute(43, new Object[] {A1414ContagemResultadoNotificacao_DestCod});
         if ( (pr_default.getStatus(43) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Destinat�rio da Notifica��o da Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONOTIFICACAO_DESTCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNotificacao_DestCod_Internalname;
         }
         A1426ContagemResultadoNotificacao_DestPesCod = T003O53_A1426ContagemResultadoNotificacao_DestPesCod[0];
         n1426ContagemResultadoNotificacao_DestPesCod = T003O53_n1426ContagemResultadoNotificacao_DestPesCod[0];
         pr_default.close(43);
         /* Using cursor T003O54 */
         pr_default.execute(44, new Object[] {n1426ContagemResultadoNotificacao_DestPesCod, A1426ContagemResultadoNotificacao_DestPesCod});
         if ( (pr_default.getStatus(44) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1421ContagemResultadoNotificacao_DestNome = T003O54_A1421ContagemResultadoNotificacao_DestNome[0];
         n1421ContagemResultadoNotificacao_DestNome = T003O54_n1421ContagemResultadoNotificacao_DestNome[0];
         pr_default.close(44);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1426ContagemResultadoNotificacao_DestPesCod = 0;
            n1426ContagemResultadoNotificacao_DestPesCod = false;
            A1421ContagemResultadoNotificacao_DestNome = "";
            n1421ContagemResultadoNotificacao_DestNome = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1426ContagemResultadoNotificacao_DestPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1421ContagemResultadoNotificacao_DestNome));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_codigo( int GX_Parm1 ,
                                                  int GX_Parm2 ,
                                                  int GX_Parm3 ,
                                                  String GX_Parm4 ,
                                                  String GX_Parm5 ,
                                                  short GX_Parm6 ,
                                                  String GX_Parm7 )
      {
         A456ContagemResultado_Codigo = GX_Parm1;
         A1553ContagemResultado_CntSrvCod = GX_Parm2;
         n1553ContagemResultado_CntSrvCod = false;
         A601ContagemResultado_Servico = GX_Parm3;
         n601ContagemResultado_Servico = false;
         A457ContagemResultado_Demanda = GX_Parm4;
         n457ContagemResultado_Demanda = false;
         A493ContagemResultado_DemandaFM = GX_Parm5;
         n493ContagemResultado_DemandaFM = false;
         A1227ContagemResultado_PrazoInicialDias = GX_Parm6;
         n1227ContagemResultado_PrazoInicialDias = false;
         A801ContagemResultado_ServicoSigla = GX_Parm7;
         n801ContagemResultado_ServicoSigla = false;
         /* Using cursor T003O63 */
         pr_default.execute(53, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(53) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
         }
         A1553ContagemResultado_CntSrvCod = T003O63_A1553ContagemResultado_CntSrvCod[0];
         n1553ContagemResultado_CntSrvCod = T003O63_n1553ContagemResultado_CntSrvCod[0];
         A457ContagemResultado_Demanda = T003O63_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T003O63_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = T003O63_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T003O63_n493ContagemResultado_DemandaFM[0];
         A1227ContagemResultado_PrazoInicialDias = T003O63_A1227ContagemResultado_PrazoInicialDias[0];
         n1227ContagemResultado_PrazoInicialDias = T003O63_n1227ContagemResultado_PrazoInicialDias[0];
         pr_default.close(53);
         /* Using cursor T003O64 */
         pr_default.execute(54, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(54) == 101) )
         {
            if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A601ContagemResultado_Servico = T003O64_A601ContagemResultado_Servico[0];
         n601ContagemResultado_Servico = T003O64_n601ContagemResultado_Servico[0];
         pr_default.close(54);
         /* Using cursor T003O65 */
         pr_default.execute(55, new Object[] {n601ContagemResultado_Servico, A601ContagemResultado_Servico});
         if ( (pr_default.getStatus(55) == 101) )
         {
            if ( ! ( (0==A601ContagemResultado_Servico) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A801ContagemResultado_ServicoSigla = T003O65_A801ContagemResultado_ServicoSigla[0];
         n801ContagemResultado_ServicoSigla = T003O65_n801ContagemResultado_ServicoSigla[0];
         pr_default.close(55);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1553ContagemResultado_CntSrvCod = 0;
            n1553ContagemResultado_CntSrvCod = false;
            A457ContagemResultado_Demanda = "";
            n457ContagemResultado_Demanda = false;
            A493ContagemResultado_DemandaFM = "";
            n493ContagemResultado_DemandaFM = false;
            A1227ContagemResultado_PrazoInicialDias = 0;
            n1227ContagemResultado_PrazoInicialDias = false;
            A601ContagemResultado_Servico = 0;
            n601ContagemResultado_Servico = false;
            A801ContagemResultado_ServicoSigla = "";
            n801ContagemResultado_ServicoSigla = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ".", "")));
         isValidOutput.Add(A457ContagemResultado_Demanda);
         isValidOutput.Add(A493ContagemResultado_DemandaFM);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A801ContagemResultado_ServicoSigla));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContagemResultadoNotificacao_Codigo',fld:'vCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123O2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(53);
         pr_default.close(54);
         pr_default.close(55);
         pr_default.close(7);
         pr_default.close(43);
         pr_default.close(44);
         pr_default.close(11);
         pr_default.close(32);
         pr_default.close(33);
         pr_default.close(30);
         pr_default.close(31);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         Z1418ContagemResultadoNotificacao_CorpoEmail = "";
         Z1420ContagemResultadoNotificacao_Midia = "";
         Z1956ContagemResultadoNotificacao_Host = "";
         Z1957ContagemResultadoNotificacao_User = "";
         Z1419ContagemResultadoNotificacao_DestEmail = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A1420ContagemResultadoNotificacao_Midia = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         lblContagemresultadonotificacaotitle_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         Gridlevel_demandaContainer = new GXWebGrid( context);
         Gridlevel_demandaColumn = new GXWebColumn();
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A801ContagemResultado_ServicoSigla = "";
         sMode169 = "";
         Gridlevel_destinatarioContainer = new GXWebGrid( context);
         Gridlevel_destinatarioColumn = new GXWebColumn();
         A1421ContagemResultadoNotificacao_DestNome = "";
         A1419ContagemResultadoNotificacao_DestEmail = "";
         sMode168 = "";
         lblTextblockcontagemresultadonotificacao_codigo_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_datahora_Jsonclick = "";
         A1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         lblTextblockcontagemresultadonotificacao_usucod_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_usupescod_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_usunom_Jsonclick = "";
         A1422ContagemResultadoNotificacao_UsuNom = "";
         lblTextblockcontagemresultadonotificacao_assunto_Jsonclick = "";
         A1417ContagemResultadoNotificacao_Assunto = "";
         lblTextblockcontagemresultadonotificacao_corpoemail_Jsonclick = "";
         A1418ContagemResultadoNotificacao_CorpoEmail = "";
         lblTextblockcontagemresultadonotificacao_midia_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_observacao_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_host_Jsonclick = "";
         A1956ContagemResultadoNotificacao_Host = "";
         lblTextblockcontagemresultadonotificacao_user_Jsonclick = "";
         A1957ContagemResultadoNotificacao_User = "";
         lblTextblockcontagemresultadonotificacao_port_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_aut_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_sec_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_logged_Jsonclick = "";
         A1415ContagemResultadoNotificacao_Observacao = "";
         AV16Pgmname = "";
         Contagemresultadonotificacao_observacao_Width = "";
         Contagemresultadonotificacao_observacao_Height = "";
         Contagemresultadonotificacao_observacao_Skin = "";
         Contagemresultadonotificacao_observacao_Toolbar = "";
         Contagemresultadonotificacao_observacao_Class = "";
         Contagemresultadonotificacao_observacao_Customtoolbar = "";
         Contagemresultadonotificacao_observacao_Customconfiguration = "";
         Contagemresultadonotificacao_observacao_Buttonpressedid = "";
         Contagemresultadonotificacao_observacao_Captionvalue = "";
         Contagemresultadonotificacao_observacao_Captionclass = "";
         Contagemresultadonotificacao_observacao_Captionposition = "";
         Contagemresultadonotificacao_observacao_Coltitle = "";
         Contagemresultadonotificacao_observacao_Coltitlefont = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         Dvpanel_tableleaflevel_destinatario_Height = "";
         Dvpanel_tableleaflevel_destinatario_Class = "";
         Dvpanel_tableleaflevel_demanda_Height = "";
         Dvpanel_tableleaflevel_demanda_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode167 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         T003O8_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         T003O8_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1417ContagemResultadoNotificacao_Assunto = "";
         Z1415ContagemResultadoNotificacao_Observacao = "";
         Z1422ContagemResultadoNotificacao_UsuNom = "";
         T003O20_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         T003O20_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         T003O15_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         T003O15_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         T003O18_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         T003O18_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         T003O23_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O23_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         T003O23_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         T003O23_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         T003O23_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         T003O23_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         T003O23_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         T003O23_A1418ContagemResultadoNotificacao_CorpoEmail = new String[] {""} ;
         T003O23_n1418ContagemResultadoNotificacao_CorpoEmail = new bool[] {false} ;
         T003O23_A1420ContagemResultadoNotificacao_Midia = new String[] {""} ;
         T003O23_n1420ContagemResultadoNotificacao_Midia = new bool[] {false} ;
         T003O23_A1415ContagemResultadoNotificacao_Observacao = new String[] {""} ;
         T003O23_n1415ContagemResultadoNotificacao_Observacao = new bool[] {false} ;
         T003O23_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         T003O23_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         T003O23_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         T003O23_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         T003O23_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         T003O23_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         T003O23_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         T003O23_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         T003O23_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         T003O23_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         T003O23_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         T003O23_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         T003O23_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         T003O23_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         T003O23_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         T003O23_A1965ContagemResultadoNotificacao_ReenvioCod = new long[1] ;
         T003O23_n1965ContagemResultadoNotificacao_ReenvioCod = new bool[] {false} ;
         T003O23_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         T003O23_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         T003O23_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         T003O23_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         T003O23_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         T003O23_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         T003O16_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         T003O16_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         T003O17_A1965ContagemResultadoNotificacao_ReenvioCod = new long[1] ;
         T003O17_n1965ContagemResultadoNotificacao_ReenvioCod = new bool[] {false} ;
         T003O25_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         T003O25_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         T003O27_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         T003O27_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         T003O28_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         T003O28_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         T003O29_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         T003O29_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         T003O30_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         T003O30_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         T003O31_A1965ContagemResultadoNotificacao_ReenvioCod = new long[1] ;
         T003O31_n1965ContagemResultadoNotificacao_ReenvioCod = new bool[] {false} ;
         T003O32_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O14_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O14_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         T003O14_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         T003O14_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         T003O14_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         T003O14_A1418ContagemResultadoNotificacao_CorpoEmail = new String[] {""} ;
         T003O14_n1418ContagemResultadoNotificacao_CorpoEmail = new bool[] {false} ;
         T003O14_A1420ContagemResultadoNotificacao_Midia = new String[] {""} ;
         T003O14_n1420ContagemResultadoNotificacao_Midia = new bool[] {false} ;
         T003O14_A1415ContagemResultadoNotificacao_Observacao = new String[] {""} ;
         T003O14_n1415ContagemResultadoNotificacao_Observacao = new bool[] {false} ;
         T003O14_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         T003O14_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         T003O14_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         T003O14_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         T003O14_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         T003O14_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         T003O14_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         T003O14_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         T003O14_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         T003O14_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         T003O14_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         T003O14_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         T003O14_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         T003O14_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         T003O14_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         T003O14_A1965ContagemResultadoNotificacao_ReenvioCod = new long[1] ;
         T003O14_n1965ContagemResultadoNotificacao_ReenvioCod = new bool[] {false} ;
         T003O33_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O34_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O13_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O13_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         T003O13_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         T003O13_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         T003O13_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         T003O13_A1418ContagemResultadoNotificacao_CorpoEmail = new String[] {""} ;
         T003O13_n1418ContagemResultadoNotificacao_CorpoEmail = new bool[] {false} ;
         T003O13_A1420ContagemResultadoNotificacao_Midia = new String[] {""} ;
         T003O13_n1420ContagemResultadoNotificacao_Midia = new bool[] {false} ;
         T003O13_A1415ContagemResultadoNotificacao_Observacao = new String[] {""} ;
         T003O13_n1415ContagemResultadoNotificacao_Observacao = new bool[] {false} ;
         T003O13_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         T003O13_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         T003O13_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         T003O13_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         T003O13_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         T003O13_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         T003O13_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         T003O13_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         T003O13_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         T003O13_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         T003O13_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         T003O13_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         T003O13_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         T003O13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         T003O13_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         T003O13_A1965ContagemResultadoNotificacao_ReenvioCod = new long[1] ;
         T003O13_n1965ContagemResultadoNotificacao_ReenvioCod = new bool[] {false} ;
         T003O35_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O39_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         T003O39_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         T003O41_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         T003O41_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         T003O42_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         T003O42_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         T003O43_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         T003O43_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         T003O44_A1965ContagemResultadoNotificacao_ReenvioCod = new long[1] ;
         T003O44_n1965ContagemResultadoNotificacao_ReenvioCod = new bool[] {false} ;
         T003O45_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         Z1421ContagemResultadoNotificacao_DestNome = "";
         T003O46_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O46_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         T003O46_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         T003O46_A1419ContagemResultadoNotificacao_DestEmail = new String[] {""} ;
         T003O46_n1419ContagemResultadoNotificacao_DestEmail = new bool[] {false} ;
         T003O46_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         T003O46_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         T003O46_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         T003O11_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         T003O11_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         T003O12_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         T003O12_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         T003O47_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         T003O47_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         T003O48_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         T003O48_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         T003O49_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O49_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         T003O10_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O10_A1419ContagemResultadoNotificacao_DestEmail = new String[] {""} ;
         T003O10_n1419ContagemResultadoNotificacao_DestEmail = new bool[] {false} ;
         T003O10_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         T003O9_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O9_A1419ContagemResultadoNotificacao_DestEmail = new String[] {""} ;
         T003O9_n1419ContagemResultadoNotificacao_DestEmail = new bool[] {false} ;
         T003O9_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         T003O53_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         T003O53_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         T003O54_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         T003O54_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         T003O55_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O55_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         Z457ContagemResultado_Demanda = "";
         Z493ContagemResultado_DemandaFM = "";
         Z801ContagemResultado_ServicoSigla = "";
         T003O56_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T003O56_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T003O56_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O56_A457ContagemResultado_Demanda = new String[] {""} ;
         T003O56_n457ContagemResultado_Demanda = new bool[] {false} ;
         T003O56_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T003O56_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T003O56_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         T003O56_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         T003O56_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         T003O56_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         T003O56_A456ContagemResultado_Codigo = new int[1] ;
         T003O56_A601ContagemResultado_Servico = new int[1] ;
         T003O56_n601ContagemResultado_Servico = new bool[] {false} ;
         T003O4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T003O4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T003O4_A457ContagemResultado_Demanda = new String[] {""} ;
         T003O4_n457ContagemResultado_Demanda = new bool[] {false} ;
         T003O4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T003O4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T003O4_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         T003O4_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         T003O5_A601ContagemResultado_Servico = new int[1] ;
         T003O5_n601ContagemResultado_Servico = new bool[] {false} ;
         T003O6_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         T003O6_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         T003O57_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T003O57_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T003O57_A457ContagemResultado_Demanda = new String[] {""} ;
         T003O57_n457ContagemResultado_Demanda = new bool[] {false} ;
         T003O57_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T003O57_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T003O57_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         T003O57_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         T003O58_A601ContagemResultado_Servico = new int[1] ;
         T003O58_n601ContagemResultado_Servico = new bool[] {false} ;
         T003O59_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         T003O59_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         T003O60_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O60_A456ContagemResultado_Codigo = new int[1] ;
         T003O3_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O3_A456ContagemResultado_Codigo = new int[1] ;
         T003O2_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O2_A456ContagemResultado_Codigo = new int[1] ;
         T003O63_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T003O63_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T003O63_A457ContagemResultado_Demanda = new String[] {""} ;
         T003O63_n457ContagemResultado_Demanda = new bool[] {false} ;
         T003O63_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T003O63_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T003O63_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         T003O63_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         T003O64_A601ContagemResultado_Servico = new int[1] ;
         T003O64_n601ContagemResultado_Servico = new bool[] {false} ;
         T003O65_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         T003O65_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         T003O66_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T003O66_A456ContagemResultado_Codigo = new int[1] ;
         Gridlevel_destinatarioRow = new GXWebRow();
         subGridlevel_destinatario_Linesclass = "";
         ROClassString = "";
         Gridlevel_demandaRow = new GXWebRow();
         subGridlevel_demanda_Linesclass = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadonotificacao__default(),
            new Object[][] {
                new Object[] {
               T003O2_A1412ContagemResultadoNotificacao_Codigo, T003O2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T003O3_A1412ContagemResultadoNotificacao_Codigo, T003O3_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T003O4_A1553ContagemResultado_CntSrvCod, T003O4_n1553ContagemResultado_CntSrvCod, T003O4_A457ContagemResultado_Demanda, T003O4_n457ContagemResultado_Demanda, T003O4_A493ContagemResultado_DemandaFM, T003O4_n493ContagemResultado_DemandaFM, T003O4_A1227ContagemResultado_PrazoInicialDias, T003O4_n1227ContagemResultado_PrazoInicialDias
               }
               , new Object[] {
               T003O5_A601ContagemResultado_Servico, T003O5_n601ContagemResultado_Servico
               }
               , new Object[] {
               T003O6_A801ContagemResultado_ServicoSigla, T003O6_n801ContagemResultado_ServicoSigla
               }
               , new Object[] {
               T003O8_A1963ContagemResultadoNotificacao_Demandas, T003O8_n1963ContagemResultadoNotificacao_Demandas
               }
               , new Object[] {
               T003O9_A1412ContagemResultadoNotificacao_Codigo, T003O9_A1419ContagemResultadoNotificacao_DestEmail, T003O9_n1419ContagemResultadoNotificacao_DestEmail, T003O9_A1414ContagemResultadoNotificacao_DestCod
               }
               , new Object[] {
               T003O10_A1412ContagemResultadoNotificacao_Codigo, T003O10_A1419ContagemResultadoNotificacao_DestEmail, T003O10_n1419ContagemResultadoNotificacao_DestEmail, T003O10_A1414ContagemResultadoNotificacao_DestCod
               }
               , new Object[] {
               T003O11_A1426ContagemResultadoNotificacao_DestPesCod, T003O11_n1426ContagemResultadoNotificacao_DestPesCod
               }
               , new Object[] {
               T003O12_A1421ContagemResultadoNotificacao_DestNome, T003O12_n1421ContagemResultadoNotificacao_DestNome
               }
               , new Object[] {
               T003O13_A1412ContagemResultadoNotificacao_Codigo, T003O13_A1416ContagemResultadoNotificacao_DataHora, T003O13_n1416ContagemResultadoNotificacao_DataHora, T003O13_A1417ContagemResultadoNotificacao_Assunto, T003O13_n1417ContagemResultadoNotificacao_Assunto, T003O13_A1418ContagemResultadoNotificacao_CorpoEmail, T003O13_n1418ContagemResultadoNotificacao_CorpoEmail, T003O13_A1420ContagemResultadoNotificacao_Midia, T003O13_n1420ContagemResultadoNotificacao_Midia, T003O13_A1415ContagemResultadoNotificacao_Observacao,
               T003O13_n1415ContagemResultadoNotificacao_Observacao, T003O13_A1956ContagemResultadoNotificacao_Host, T003O13_n1956ContagemResultadoNotificacao_Host, T003O13_A1957ContagemResultadoNotificacao_User, T003O13_n1957ContagemResultadoNotificacao_User, T003O13_A1958ContagemResultadoNotificacao_Port, T003O13_n1958ContagemResultadoNotificacao_Port, T003O13_A1959ContagemResultadoNotificacao_Aut, T003O13_n1959ContagemResultadoNotificacao_Aut, T003O13_A1960ContagemResultadoNotificacao_Sec,
               T003O13_n1960ContagemResultadoNotificacao_Sec, T003O13_A1961ContagemResultadoNotificacao_Logged, T003O13_n1961ContagemResultadoNotificacao_Logged, T003O13_A1413ContagemResultadoNotificacao_UsuCod, T003O13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod, T003O13_n1966ContagemResultadoNotificacao_AreaTrabalhoCod, T003O13_A1965ContagemResultadoNotificacao_ReenvioCod, T003O13_n1965ContagemResultadoNotificacao_ReenvioCod
               }
               , new Object[] {
               T003O14_A1412ContagemResultadoNotificacao_Codigo, T003O14_A1416ContagemResultadoNotificacao_DataHora, T003O14_n1416ContagemResultadoNotificacao_DataHora, T003O14_A1417ContagemResultadoNotificacao_Assunto, T003O14_n1417ContagemResultadoNotificacao_Assunto, T003O14_A1418ContagemResultadoNotificacao_CorpoEmail, T003O14_n1418ContagemResultadoNotificacao_CorpoEmail, T003O14_A1420ContagemResultadoNotificacao_Midia, T003O14_n1420ContagemResultadoNotificacao_Midia, T003O14_A1415ContagemResultadoNotificacao_Observacao,
               T003O14_n1415ContagemResultadoNotificacao_Observacao, T003O14_A1956ContagemResultadoNotificacao_Host, T003O14_n1956ContagemResultadoNotificacao_Host, T003O14_A1957ContagemResultadoNotificacao_User, T003O14_n1957ContagemResultadoNotificacao_User, T003O14_A1958ContagemResultadoNotificacao_Port, T003O14_n1958ContagemResultadoNotificacao_Port, T003O14_A1959ContagemResultadoNotificacao_Aut, T003O14_n1959ContagemResultadoNotificacao_Aut, T003O14_A1960ContagemResultadoNotificacao_Sec,
               T003O14_n1960ContagemResultadoNotificacao_Sec, T003O14_A1961ContagemResultadoNotificacao_Logged, T003O14_n1961ContagemResultadoNotificacao_Logged, T003O14_A1413ContagemResultadoNotificacao_UsuCod, T003O14_A1966ContagemResultadoNotificacao_AreaTrabalhoCod, T003O14_n1966ContagemResultadoNotificacao_AreaTrabalhoCod, T003O14_A1965ContagemResultadoNotificacao_ReenvioCod, T003O14_n1965ContagemResultadoNotificacao_ReenvioCod
               }
               , new Object[] {
               T003O15_A1427ContagemResultadoNotificacao_UsuPesCod, T003O15_n1427ContagemResultadoNotificacao_UsuPesCod
               }
               , new Object[] {
               T003O16_A1966ContagemResultadoNotificacao_AreaTrabalhoCod
               }
               , new Object[] {
               T003O17_A1965ContagemResultadoNotificacao_ReenvioCod
               }
               , new Object[] {
               T003O18_A1422ContagemResultadoNotificacao_UsuNom, T003O18_n1422ContagemResultadoNotificacao_UsuNom
               }
               , new Object[] {
               T003O20_A1962ContagemResultadoNotificacao_Destinatarios, T003O20_n1962ContagemResultadoNotificacao_Destinatarios
               }
               , new Object[] {
               T003O23_A1412ContagemResultadoNotificacao_Codigo, T003O23_A1416ContagemResultadoNotificacao_DataHora, T003O23_n1416ContagemResultadoNotificacao_DataHora, T003O23_A1422ContagemResultadoNotificacao_UsuNom, T003O23_n1422ContagemResultadoNotificacao_UsuNom, T003O23_A1417ContagemResultadoNotificacao_Assunto, T003O23_n1417ContagemResultadoNotificacao_Assunto, T003O23_A1418ContagemResultadoNotificacao_CorpoEmail, T003O23_n1418ContagemResultadoNotificacao_CorpoEmail, T003O23_A1420ContagemResultadoNotificacao_Midia,
               T003O23_n1420ContagemResultadoNotificacao_Midia, T003O23_A1415ContagemResultadoNotificacao_Observacao, T003O23_n1415ContagemResultadoNotificacao_Observacao, T003O23_A1956ContagemResultadoNotificacao_Host, T003O23_n1956ContagemResultadoNotificacao_Host, T003O23_A1957ContagemResultadoNotificacao_User, T003O23_n1957ContagemResultadoNotificacao_User, T003O23_A1958ContagemResultadoNotificacao_Port, T003O23_n1958ContagemResultadoNotificacao_Port, T003O23_A1959ContagemResultadoNotificacao_Aut,
               T003O23_n1959ContagemResultadoNotificacao_Aut, T003O23_A1960ContagemResultadoNotificacao_Sec, T003O23_n1960ContagemResultadoNotificacao_Sec, T003O23_A1961ContagemResultadoNotificacao_Logged, T003O23_n1961ContagemResultadoNotificacao_Logged, T003O23_A1413ContagemResultadoNotificacao_UsuCod, T003O23_A1966ContagemResultadoNotificacao_AreaTrabalhoCod, T003O23_n1966ContagemResultadoNotificacao_AreaTrabalhoCod, T003O23_A1965ContagemResultadoNotificacao_ReenvioCod, T003O23_n1965ContagemResultadoNotificacao_ReenvioCod,
               T003O23_A1427ContagemResultadoNotificacao_UsuPesCod, T003O23_n1427ContagemResultadoNotificacao_UsuPesCod, T003O23_A1962ContagemResultadoNotificacao_Destinatarios, T003O23_n1962ContagemResultadoNotificacao_Destinatarios, T003O23_A1963ContagemResultadoNotificacao_Demandas, T003O23_n1963ContagemResultadoNotificacao_Demandas
               }
               , new Object[] {
               T003O25_A1962ContagemResultadoNotificacao_Destinatarios, T003O25_n1962ContagemResultadoNotificacao_Destinatarios
               }
               , new Object[] {
               T003O27_A1963ContagemResultadoNotificacao_Demandas, T003O27_n1963ContagemResultadoNotificacao_Demandas
               }
               , new Object[] {
               T003O28_A1427ContagemResultadoNotificacao_UsuPesCod, T003O28_n1427ContagemResultadoNotificacao_UsuPesCod
               }
               , new Object[] {
               T003O29_A1422ContagemResultadoNotificacao_UsuNom, T003O29_n1422ContagemResultadoNotificacao_UsuNom
               }
               , new Object[] {
               T003O30_A1966ContagemResultadoNotificacao_AreaTrabalhoCod
               }
               , new Object[] {
               T003O31_A1965ContagemResultadoNotificacao_ReenvioCod
               }
               , new Object[] {
               T003O32_A1412ContagemResultadoNotificacao_Codigo
               }
               , new Object[] {
               T003O33_A1412ContagemResultadoNotificacao_Codigo
               }
               , new Object[] {
               T003O34_A1412ContagemResultadoNotificacao_Codigo
               }
               , new Object[] {
               T003O35_A1412ContagemResultadoNotificacao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003O39_A1962ContagemResultadoNotificacao_Destinatarios, T003O39_n1962ContagemResultadoNotificacao_Destinatarios
               }
               , new Object[] {
               T003O41_A1963ContagemResultadoNotificacao_Demandas, T003O41_n1963ContagemResultadoNotificacao_Demandas
               }
               , new Object[] {
               T003O42_A1427ContagemResultadoNotificacao_UsuPesCod, T003O42_n1427ContagemResultadoNotificacao_UsuPesCod
               }
               , new Object[] {
               T003O43_A1422ContagemResultadoNotificacao_UsuNom, T003O43_n1422ContagemResultadoNotificacao_UsuNom
               }
               , new Object[] {
               T003O44_A1965ContagemResultadoNotificacao_ReenvioCod
               }
               , new Object[] {
               T003O45_A1412ContagemResultadoNotificacao_Codigo
               }
               , new Object[] {
               T003O46_A1412ContagemResultadoNotificacao_Codigo, T003O46_A1421ContagemResultadoNotificacao_DestNome, T003O46_n1421ContagemResultadoNotificacao_DestNome, T003O46_A1419ContagemResultadoNotificacao_DestEmail, T003O46_n1419ContagemResultadoNotificacao_DestEmail, T003O46_A1414ContagemResultadoNotificacao_DestCod, T003O46_A1426ContagemResultadoNotificacao_DestPesCod, T003O46_n1426ContagemResultadoNotificacao_DestPesCod
               }
               , new Object[] {
               T003O47_A1426ContagemResultadoNotificacao_DestPesCod, T003O47_n1426ContagemResultadoNotificacao_DestPesCod
               }
               , new Object[] {
               T003O48_A1421ContagemResultadoNotificacao_DestNome, T003O48_n1421ContagemResultadoNotificacao_DestNome
               }
               , new Object[] {
               T003O49_A1412ContagemResultadoNotificacao_Codigo, T003O49_A1414ContagemResultadoNotificacao_DestCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003O53_A1426ContagemResultadoNotificacao_DestPesCod, T003O53_n1426ContagemResultadoNotificacao_DestPesCod
               }
               , new Object[] {
               T003O54_A1421ContagemResultadoNotificacao_DestNome, T003O54_n1421ContagemResultadoNotificacao_DestNome
               }
               , new Object[] {
               T003O55_A1412ContagemResultadoNotificacao_Codigo, T003O55_A1414ContagemResultadoNotificacao_DestCod
               }
               , new Object[] {
               T003O56_A1553ContagemResultado_CntSrvCod, T003O56_n1553ContagemResultado_CntSrvCod, T003O56_A1412ContagemResultadoNotificacao_Codigo, T003O56_A457ContagemResultado_Demanda, T003O56_n457ContagemResultado_Demanda, T003O56_A493ContagemResultado_DemandaFM, T003O56_n493ContagemResultado_DemandaFM, T003O56_A801ContagemResultado_ServicoSigla, T003O56_n801ContagemResultado_ServicoSigla, T003O56_A1227ContagemResultado_PrazoInicialDias,
               T003O56_n1227ContagemResultado_PrazoInicialDias, T003O56_A456ContagemResultado_Codigo, T003O56_A601ContagemResultado_Servico, T003O56_n601ContagemResultado_Servico
               }
               , new Object[] {
               T003O57_A1553ContagemResultado_CntSrvCod, T003O57_n1553ContagemResultado_CntSrvCod, T003O57_A457ContagemResultado_Demanda, T003O57_n457ContagemResultado_Demanda, T003O57_A493ContagemResultado_DemandaFM, T003O57_n493ContagemResultado_DemandaFM, T003O57_A1227ContagemResultado_PrazoInicialDias, T003O57_n1227ContagemResultado_PrazoInicialDias
               }
               , new Object[] {
               T003O58_A601ContagemResultado_Servico, T003O58_n601ContagemResultado_Servico
               }
               , new Object[] {
               T003O59_A801ContagemResultado_ServicoSigla, T003O59_n801ContagemResultado_ServicoSigla
               }
               , new Object[] {
               T003O60_A1412ContagemResultadoNotificacao_Codigo, T003O60_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003O63_A1553ContagemResultado_CntSrvCod, T003O63_n1553ContagemResultado_CntSrvCod, T003O63_A457ContagemResultado_Demanda, T003O63_n457ContagemResultado_Demanda, T003O63_A493ContagemResultado_DemandaFM, T003O63_n493ContagemResultado_DemandaFM, T003O63_A1227ContagemResultado_PrazoInicialDias, T003O63_n1227ContagemResultado_PrazoInicialDias
               }
               , new Object[] {
               T003O64_A601ContagemResultado_Servico, T003O64_n601ContagemResultado_Servico
               }
               , new Object[] {
               T003O65_A801ContagemResultado_ServicoSigla, T003O65_n801ContagemResultado_ServicoSigla
               }
               , new Object[] {
               T003O66_A1412ContagemResultadoNotificacao_Codigo, T003O66_A456ContagemResultado_Codigo
               }
            }
         );
         AV16Pgmname = "ContagemResultadoNotificacao";
      }

      private short nIsMod_168 ;
      private short Z1958ContagemResultadoNotificacao_Port ;
      private short Z1960ContagemResultadoNotificacao_Sec ;
      private short Z1961ContagemResultadoNotificacao_Logged ;
      private short nRC_GXsfl_99 ;
      private short nGXsfl_99_idx=1 ;
      private short nRC_GXsfl_111 ;
      private short nGXsfl_111_idx=1 ;
      private short nRcdDeleted_168 ;
      private short nRcdExists_168 ;
      private short nRcdDeleted_169 ;
      private short nRcdExists_169 ;
      private short nIsMod_169 ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A1960ContagemResultadoNotificacao_Sec ;
      private short A1961ContagemResultadoNotificacao_Logged ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short subGridlevel_demanda_Backcolorstyle ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short subGridlevel_demanda_Allowselection ;
      private short subGridlevel_demanda_Allowhovering ;
      private short subGridlevel_demanda_Allowcollapsing ;
      private short subGridlevel_demanda_Collapsed ;
      private short nBlankRcdCount169 ;
      private short RcdFound169 ;
      private short nBlankRcdUsr169 ;
      private short subGridlevel_destinatario_Backcolorstyle ;
      private short subGridlevel_destinatario_Allowselection ;
      private short subGridlevel_destinatario_Allowhovering ;
      private short subGridlevel_destinatario_Allowcollapsing ;
      private short subGridlevel_destinatario_Collapsed ;
      private short nBlankRcdCount168 ;
      private short RcdFound168 ;
      private short nBlankRcdUsr168 ;
      private short A1958ContagemResultadoNotificacao_Port ;
      private short RcdFound167 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short Z1227ContagemResultado_PrazoInicialDias ;
      private short subGridlevel_destinatario_Backstyle ;
      private short subGridlevel_demanda_Backstyle ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1413ContagemResultadoNotificacao_UsuCod ;
      private int Z1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private int O1962ContagemResultadoNotificacao_Destinatarios ;
      private int N1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private int N1413ContagemResultadoNotificacao_UsuCod ;
      private int Z1414ContagemResultadoNotificacao_DestCod ;
      private int Z456ContagemResultado_Codigo ;
      private int A1413ContagemResultadoNotificacao_UsuCod ;
      private int A1427ContagemResultadoNotificacao_UsuPesCod ;
      private int A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private int A1414ContagemResultadoNotificacao_DestCod ;
      private int A1426ContagemResultadoNotificacao_DestPesCod ;
      private int A456ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContagemResultado_Codigo_Enabled ;
      private int edtContagemResultado_Demanda_Enabled ;
      private int edtContagemResultado_DemandaFM_Enabled ;
      private int edtContagemResultado_Servico_Enabled ;
      private int edtContagemResultado_ServicoSigla_Enabled ;
      private int edtContagemResultado_PrazoInicialDias_Enabled ;
      private int subGridlevel_demanda_Selectioncolor ;
      private int subGridlevel_demanda_Hoveringcolor ;
      private int B1962ContagemResultadoNotificacao_Destinatarios ;
      private int A1962ContagemResultadoNotificacao_Destinatarios ;
      private int fRowAdded ;
      private int edtContagemResultadoNotificacao_DestCod_Enabled ;
      private int edtContagemResultadoNotificacao_DestPesCod_Enabled ;
      private int edtContagemResultadoNotificacao_DestNome_Enabled ;
      private int edtContagemResultadoNotificacao_DestEmail_Enabled ;
      private int subGridlevel_destinatario_Selectioncolor ;
      private int subGridlevel_destinatario_Hoveringcolor ;
      private int edtContagemResultadoNotificacao_Codigo_Enabled ;
      private int edtContagemResultadoNotificacao_DataHora_Enabled ;
      private int edtContagemResultadoNotificacao_UsuCod_Enabled ;
      private int imgprompt_1413_Visible ;
      private int edtContagemResultadoNotificacao_UsuPesCod_Enabled ;
      private int edtContagemResultadoNotificacao_UsuNom_Enabled ;
      private int edtContagemResultadoNotificacao_Assunto_Enabled ;
      private int edtContagemResultadoNotificacao_CorpoEmail_Enabled ;
      private int edtContagemResultadoNotificacao_Host_Enabled ;
      private int edtContagemResultadoNotificacao_User_Enabled ;
      private int edtContagemResultadoNotificacao_Port_Enabled ;
      private int AV15Insert_ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private int AV11Insert_ContagemResultadoNotificacao_UsuCod ;
      private int A1963ContagemResultadoNotificacao_Demandas ;
      private int Contagemresultadonotificacao_observacao_Color ;
      private int Contagemresultadonotificacao_observacao_Coltitlecolor ;
      private int s1962ContagemResultadoNotificacao_Destinatarios ;
      private int AV17GXV1 ;
      private int Z1962ContagemResultadoNotificacao_Destinatarios ;
      private int Z1963ContagemResultadoNotificacao_Demandas ;
      private int Z1427ContagemResultadoNotificacao_UsuPesCod ;
      private int Z1426ContagemResultadoNotificacao_DestPesCod ;
      private int Z1553ContagemResultado_CntSrvCod ;
      private int Z601ContagemResultado_Servico ;
      private int subGridlevel_destinatario_Backcolor ;
      private int subGridlevel_destinatario_Allbackcolor ;
      private int imgprompt_1414_Visible ;
      private int subGridlevel_demanda_Backcolor ;
      private int subGridlevel_demanda_Allbackcolor ;
      private int defedtContagemResultadoNotificacao_DestCod_Enabled ;
      private int defedtContagemResultado_Codigo_Enabled ;
      private int idxLst ;
      private long wcpOAV7ContagemResultadoNotificacao_Codigo ;
      private long Z1412ContagemResultadoNotificacao_Codigo ;
      private long Z1965ContagemResultadoNotificacao_ReenvioCod ;
      private long N1965ContagemResultadoNotificacao_ReenvioCod ;
      private long A1412ContagemResultadoNotificacao_Codigo ;
      private long A1965ContagemResultadoNotificacao_ReenvioCod ;
      private long AV7ContagemResultadoNotificacao_Codigo ;
      private long AV14Insert_ContagemResultadoNotificacao_ReenvioCod ;
      private long GRIDLEVEL_DESTINATARIO_nFirstRecordOnPage ;
      private long GRIDLEVEL_DEMANDA_nFirstRecordOnPage ;
      private String sPrefix ;
      private String sGXsfl_99_idx="0001" ;
      private String wcpOGx_mode ;
      private String Z1420ContagemResultadoNotificacao_Midia ;
      private String Z1956ContagemResultadoNotificacao_Host ;
      private String Z1957ContagemResultadoNotificacao_User ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_111_idx="0001" ;
      private String Gx_mode ;
      private String GXKey ;
      private String A1420ContagemResultadoNotificacao_Midia ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoNotificacao_DataHora_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblContagemresultadonotificacaotitle_Internalname ;
      private String lblContagemresultadonotificacaotitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTableleaflevel_demanda_Internalname ;
      private String A801ContagemResultado_ServicoSigla ;
      private String sMode169 ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_Demanda_Internalname ;
      private String edtContagemResultado_DemandaFM_Internalname ;
      private String edtContagemResultado_Servico_Internalname ;
      private String edtContagemResultado_ServicoSigla_Internalname ;
      private String edtContagemResultado_PrazoInicialDias_Internalname ;
      private String tblTableleaflevel_destinatario_Internalname ;
      private String A1421ContagemResultadoNotificacao_DestNome ;
      private String sMode168 ;
      private String edtContagemResultadoNotificacao_DestCod_Internalname ;
      private String edtContagemResultadoNotificacao_DestPesCod_Internalname ;
      private String edtContagemResultadoNotificacao_DestNome_Internalname ;
      private String edtContagemResultadoNotificacao_DestEmail_Internalname ;
      private String imgprompt_1413_Link ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_codigo_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_codigo_Jsonclick ;
      private String edtContagemResultadoNotificacao_Codigo_Internalname ;
      private String edtContagemResultadoNotificacao_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_datahora_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_datahora_Jsonclick ;
      private String edtContagemResultadoNotificacao_DataHora_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_usucod_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_usucod_Jsonclick ;
      private String edtContagemResultadoNotificacao_UsuCod_Internalname ;
      private String edtContagemResultadoNotificacao_UsuCod_Jsonclick ;
      private String imgprompt_1413_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_usupescod_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_usupescod_Jsonclick ;
      private String edtContagemResultadoNotificacao_UsuPesCod_Internalname ;
      private String edtContagemResultadoNotificacao_UsuPesCod_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_usunom_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_usunom_Jsonclick ;
      private String edtContagemResultadoNotificacao_UsuNom_Internalname ;
      private String A1422ContagemResultadoNotificacao_UsuNom ;
      private String edtContagemResultadoNotificacao_UsuNom_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_assunto_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_assunto_Jsonclick ;
      private String edtContagemResultadoNotificacao_Assunto_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_corpoemail_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_corpoemail_Jsonclick ;
      private String edtContagemResultadoNotificacao_CorpoEmail_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_midia_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_midia_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Midia_Internalname ;
      private String cmbContagemResultadoNotificacao_Midia_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_observacao_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_observacao_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_host_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_host_Jsonclick ;
      private String edtContagemResultadoNotificacao_Host_Internalname ;
      private String A1956ContagemResultadoNotificacao_Host ;
      private String edtContagemResultadoNotificacao_Host_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_user_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_user_Jsonclick ;
      private String edtContagemResultadoNotificacao_User_Internalname ;
      private String A1957ContagemResultadoNotificacao_User ;
      private String edtContagemResultadoNotificacao_User_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_port_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_port_Jsonclick ;
      private String edtContagemResultadoNotificacao_Port_Internalname ;
      private String edtContagemResultadoNotificacao_Port_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_aut_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_aut_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Aut_Internalname ;
      private String cmbContagemResultadoNotificacao_Aut_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_sec_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_sec_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Sec_Internalname ;
      private String cmbContagemResultadoNotificacao_Sec_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_logged_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_logged_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Logged_Internalname ;
      private String cmbContagemResultadoNotificacao_Logged_Jsonclick ;
      private String AV16Pgmname ;
      private String Contagemresultadonotificacao_observacao_Width ;
      private String Contagemresultadonotificacao_observacao_Height ;
      private String Contagemresultadonotificacao_observacao_Skin ;
      private String Contagemresultadonotificacao_observacao_Toolbar ;
      private String Contagemresultadonotificacao_observacao_Class ;
      private String Contagemresultadonotificacao_observacao_Customtoolbar ;
      private String Contagemresultadonotificacao_observacao_Customconfiguration ;
      private String Contagemresultadonotificacao_observacao_Buttonpressedid ;
      private String Contagemresultadonotificacao_observacao_Captionvalue ;
      private String Contagemresultadonotificacao_observacao_Captionclass ;
      private String Contagemresultadonotificacao_observacao_Captionposition ;
      private String Contagemresultadonotificacao_observacao_Coltitle ;
      private String Contagemresultadonotificacao_observacao_Coltitlefont ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String Dvpanel_tableleaflevel_destinatario_Width ;
      private String Dvpanel_tableleaflevel_destinatario_Height ;
      private String Dvpanel_tableleaflevel_destinatario_Cls ;
      private String Dvpanel_tableleaflevel_destinatario_Title ;
      private String Dvpanel_tableleaflevel_destinatario_Class ;
      private String Dvpanel_tableleaflevel_destinatario_Iconposition ;
      private String Dvpanel_tableleaflevel_demanda_Width ;
      private String Dvpanel_tableleaflevel_demanda_Height ;
      private String Dvpanel_tableleaflevel_demanda_Cls ;
      private String Dvpanel_tableleaflevel_demanda_Title ;
      private String Dvpanel_tableleaflevel_demanda_Class ;
      private String Dvpanel_tableleaflevel_demanda_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode167 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String Z1422ContagemResultadoNotificacao_UsuNom ;
      private String Contagemresultadonotificacao_observacao_Internalname ;
      private String Z1421ContagemResultadoNotificacao_DestNome ;
      private String Z801ContagemResultado_ServicoSigla ;
      private String imgprompt_1414_Internalname ;
      private String sGXsfl_99_fel_idx="0001" ;
      private String subGridlevel_destinatario_Class ;
      private String subGridlevel_destinatario_Linesclass ;
      private String imgprompt_1414_Link ;
      private String ROClassString ;
      private String edtContagemResultadoNotificacao_DestCod_Jsonclick ;
      private String edtContagemResultadoNotificacao_DestPesCod_Jsonclick ;
      private String edtContagemResultadoNotificacao_DestNome_Jsonclick ;
      private String edtContagemResultadoNotificacao_DestEmail_Jsonclick ;
      private String sGXsfl_111_fel_idx="0001" ;
      private String subGridlevel_demanda_Class ;
      private String subGridlevel_demanda_Linesclass ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String edtContagemResultado_Demanda_Jsonclick ;
      private String edtContagemResultado_DemandaFM_Jsonclick ;
      private String edtContagemResultado_Servico_Jsonclick ;
      private String edtContagemResultado_ServicoSigla_Jsonclick ;
      private String edtContagemResultado_PrazoInicialDias_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String Dvpanel_tableleaflevel_destinatario_Internalname ;
      private String Dvpanel_tableleaflevel_demanda_Internalname ;
      private String subGridlevel_destinatario_Internalname ;
      private String subGridlevel_demanda_Internalname ;
      private DateTime Z1416ContagemResultadoNotificacao_DataHora ;
      private DateTime A1416ContagemResultadoNotificacao_DataHora ;
      private bool Z1959ContagemResultadoNotificacao_Aut ;
      private bool entryPointCalled ;
      private bool n1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool n1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool n1426ContagemResultadoNotificacao_DestPesCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool toggleJsOutput ;
      private bool n1420ContagemResultadoNotificacao_Midia ;
      private bool A1959ContagemResultadoNotificacao_Aut ;
      private bool n1959ContagemResultadoNotificacao_Aut ;
      private bool n1960ContagemResultadoNotificacao_Sec ;
      private bool n1961ContagemResultadoNotificacao_Logged ;
      private bool wbErr ;
      private bool n1962ContagemResultadoNotificacao_Destinatarios ;
      private bool n1416ContagemResultadoNotificacao_DataHora ;
      private bool n1422ContagemResultadoNotificacao_UsuNom ;
      private bool n1417ContagemResultadoNotificacao_Assunto ;
      private bool n1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool n1956ContagemResultadoNotificacao_Host ;
      private bool n1957ContagemResultadoNotificacao_User ;
      private bool n1958ContagemResultadoNotificacao_Port ;
      private bool n1415ContagemResultadoNotificacao_Observacao ;
      private bool n1963ContagemResultadoNotificacao_Demandas ;
      private bool Contagemresultadonotificacao_observacao_Enabled ;
      private bool Contagemresultadonotificacao_observacao_Toolbarcancollapse ;
      private bool Contagemresultadonotificacao_observacao_Toolbarexpanded ;
      private bool Contagemresultadonotificacao_observacao_Usercontroliscolumn ;
      private bool Contagemresultadonotificacao_observacao_Visible ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool Dvpanel_tableleaflevel_destinatario_Collapsible ;
      private bool Dvpanel_tableleaflevel_destinatario_Collapsed ;
      private bool Dvpanel_tableleaflevel_destinatario_Enabled ;
      private bool Dvpanel_tableleaflevel_destinatario_Autowidth ;
      private bool Dvpanel_tableleaflevel_destinatario_Autoheight ;
      private bool Dvpanel_tableleaflevel_destinatario_Showheader ;
      private bool Dvpanel_tableleaflevel_destinatario_Showcollapseicon ;
      private bool Dvpanel_tableleaflevel_destinatario_Autoscroll ;
      private bool Dvpanel_tableleaflevel_destinatario_Visible ;
      private bool Dvpanel_tableleaflevel_demanda_Collapsible ;
      private bool Dvpanel_tableleaflevel_demanda_Collapsed ;
      private bool Dvpanel_tableleaflevel_demanda_Enabled ;
      private bool Dvpanel_tableleaflevel_demanda_Autowidth ;
      private bool Dvpanel_tableleaflevel_demanda_Autoheight ;
      private bool Dvpanel_tableleaflevel_demanda_Showheader ;
      private bool Dvpanel_tableleaflevel_demanda_Showcollapseicon ;
      private bool Dvpanel_tableleaflevel_demanda_Autoscroll ;
      private bool Dvpanel_tableleaflevel_demanda_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool n1421ContagemResultadoNotificacao_DestNome ;
      private bool n1419ContagemResultadoNotificacao_DestEmail ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private String A1417ContagemResultadoNotificacao_Assunto ;
      private String A1415ContagemResultadoNotificacao_Observacao ;
      private String Z1417ContagemResultadoNotificacao_Assunto ;
      private String Z1415ContagemResultadoNotificacao_Observacao ;
      private String Z1418ContagemResultadoNotificacao_CorpoEmail ;
      private String Z1419ContagemResultadoNotificacao_DestEmail ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A1419ContagemResultadoNotificacao_DestEmail ;
      private String A1418ContagemResultadoNotificacao_CorpoEmail ;
      private String Z457ContagemResultado_Demanda ;
      private String Z493ContagemResultado_DemandaFM ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid Gridlevel_demandaContainer ;
      private GXWebGrid Gridlevel_destinatarioContainer ;
      private GXWebRow Gridlevel_destinatarioRow ;
      private GXWebRow Gridlevel_demandaRow ;
      private GXWebColumn Gridlevel_demandaColumn ;
      private GXWebColumn Gridlevel_destinatarioColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContagemResultadoNotificacao_Midia ;
      private GXCombobox cmbContagemResultadoNotificacao_Aut ;
      private GXCombobox cmbContagemResultadoNotificacao_Sec ;
      private GXCombobox cmbContagemResultadoNotificacao_Logged ;
      private IDataStoreProvider pr_default ;
      private int[] T003O8_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] T003O8_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] T003O20_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] T003O20_n1962ContagemResultadoNotificacao_Destinatarios ;
      private int[] T003O15_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] T003O15_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private String[] T003O18_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] T003O18_n1422ContagemResultadoNotificacao_UsuNom ;
      private long[] T003O23_A1412ContagemResultadoNotificacao_Codigo ;
      private DateTime[] T003O23_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] T003O23_n1416ContagemResultadoNotificacao_DataHora ;
      private String[] T003O23_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] T003O23_n1422ContagemResultadoNotificacao_UsuNom ;
      private String[] T003O23_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] T003O23_n1417ContagemResultadoNotificacao_Assunto ;
      private String[] T003O23_A1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool[] T003O23_n1418ContagemResultadoNotificacao_CorpoEmail ;
      private String[] T003O23_A1420ContagemResultadoNotificacao_Midia ;
      private bool[] T003O23_n1420ContagemResultadoNotificacao_Midia ;
      private String[] T003O23_A1415ContagemResultadoNotificacao_Observacao ;
      private bool[] T003O23_n1415ContagemResultadoNotificacao_Observacao ;
      private String[] T003O23_A1956ContagemResultadoNotificacao_Host ;
      private bool[] T003O23_n1956ContagemResultadoNotificacao_Host ;
      private String[] T003O23_A1957ContagemResultadoNotificacao_User ;
      private bool[] T003O23_n1957ContagemResultadoNotificacao_User ;
      private short[] T003O23_A1958ContagemResultadoNotificacao_Port ;
      private bool[] T003O23_n1958ContagemResultadoNotificacao_Port ;
      private bool[] T003O23_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] T003O23_n1959ContagemResultadoNotificacao_Aut ;
      private short[] T003O23_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] T003O23_n1960ContagemResultadoNotificacao_Sec ;
      private short[] T003O23_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] T003O23_n1961ContagemResultadoNotificacao_Logged ;
      private int[] T003O23_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] T003O23_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] T003O23_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private long[] T003O23_A1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool[] T003O23_n1965ContagemResultadoNotificacao_ReenvioCod ;
      private int[] T003O23_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] T003O23_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private int[] T003O23_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] T003O23_n1962ContagemResultadoNotificacao_Destinatarios ;
      private int[] T003O23_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] T003O23_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] T003O16_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] T003O16_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private long[] T003O17_A1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool[] T003O17_n1965ContagemResultadoNotificacao_ReenvioCod ;
      private int[] T003O25_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] T003O25_n1962ContagemResultadoNotificacao_Destinatarios ;
      private int[] T003O27_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] T003O27_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] T003O28_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] T003O28_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private String[] T003O29_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] T003O29_n1422ContagemResultadoNotificacao_UsuNom ;
      private int[] T003O30_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] T003O30_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private long[] T003O31_A1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool[] T003O31_n1965ContagemResultadoNotificacao_ReenvioCod ;
      private long[] T003O32_A1412ContagemResultadoNotificacao_Codigo ;
      private long[] T003O14_A1412ContagemResultadoNotificacao_Codigo ;
      private DateTime[] T003O14_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] T003O14_n1416ContagemResultadoNotificacao_DataHora ;
      private String[] T003O14_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] T003O14_n1417ContagemResultadoNotificacao_Assunto ;
      private String[] T003O14_A1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool[] T003O14_n1418ContagemResultadoNotificacao_CorpoEmail ;
      private String[] T003O14_A1420ContagemResultadoNotificacao_Midia ;
      private bool[] T003O14_n1420ContagemResultadoNotificacao_Midia ;
      private String[] T003O14_A1415ContagemResultadoNotificacao_Observacao ;
      private bool[] T003O14_n1415ContagemResultadoNotificacao_Observacao ;
      private String[] T003O14_A1956ContagemResultadoNotificacao_Host ;
      private bool[] T003O14_n1956ContagemResultadoNotificacao_Host ;
      private String[] T003O14_A1957ContagemResultadoNotificacao_User ;
      private bool[] T003O14_n1957ContagemResultadoNotificacao_User ;
      private short[] T003O14_A1958ContagemResultadoNotificacao_Port ;
      private bool[] T003O14_n1958ContagemResultadoNotificacao_Port ;
      private bool[] T003O14_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] T003O14_n1959ContagemResultadoNotificacao_Aut ;
      private short[] T003O14_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] T003O14_n1960ContagemResultadoNotificacao_Sec ;
      private short[] T003O14_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] T003O14_n1961ContagemResultadoNotificacao_Logged ;
      private int[] T003O14_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] T003O14_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] T003O14_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private long[] T003O14_A1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool[] T003O14_n1965ContagemResultadoNotificacao_ReenvioCod ;
      private long[] T003O33_A1412ContagemResultadoNotificacao_Codigo ;
      private long[] T003O34_A1412ContagemResultadoNotificacao_Codigo ;
      private long[] T003O13_A1412ContagemResultadoNotificacao_Codigo ;
      private DateTime[] T003O13_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] T003O13_n1416ContagemResultadoNotificacao_DataHora ;
      private String[] T003O13_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] T003O13_n1417ContagemResultadoNotificacao_Assunto ;
      private String[] T003O13_A1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool[] T003O13_n1418ContagemResultadoNotificacao_CorpoEmail ;
      private String[] T003O13_A1420ContagemResultadoNotificacao_Midia ;
      private bool[] T003O13_n1420ContagemResultadoNotificacao_Midia ;
      private String[] T003O13_A1415ContagemResultadoNotificacao_Observacao ;
      private bool[] T003O13_n1415ContagemResultadoNotificacao_Observacao ;
      private String[] T003O13_A1956ContagemResultadoNotificacao_Host ;
      private bool[] T003O13_n1956ContagemResultadoNotificacao_Host ;
      private String[] T003O13_A1957ContagemResultadoNotificacao_User ;
      private bool[] T003O13_n1957ContagemResultadoNotificacao_User ;
      private short[] T003O13_A1958ContagemResultadoNotificacao_Port ;
      private bool[] T003O13_n1958ContagemResultadoNotificacao_Port ;
      private bool[] T003O13_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] T003O13_n1959ContagemResultadoNotificacao_Aut ;
      private short[] T003O13_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] T003O13_n1960ContagemResultadoNotificacao_Sec ;
      private short[] T003O13_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] T003O13_n1961ContagemResultadoNotificacao_Logged ;
      private int[] T003O13_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] T003O13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] T003O13_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private long[] T003O13_A1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool[] T003O13_n1965ContagemResultadoNotificacao_ReenvioCod ;
      private long[] T003O35_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] T003O39_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] T003O39_n1962ContagemResultadoNotificacao_Destinatarios ;
      private int[] T003O41_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] T003O41_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] T003O42_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] T003O42_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private String[] T003O43_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] T003O43_n1422ContagemResultadoNotificacao_UsuNom ;
      private long[] T003O44_A1965ContagemResultadoNotificacao_ReenvioCod ;
      private bool[] T003O44_n1965ContagemResultadoNotificacao_ReenvioCod ;
      private long[] T003O45_A1412ContagemResultadoNotificacao_Codigo ;
      private long[] T003O46_A1412ContagemResultadoNotificacao_Codigo ;
      private String[] T003O46_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] T003O46_n1421ContagemResultadoNotificacao_DestNome ;
      private String[] T003O46_A1419ContagemResultadoNotificacao_DestEmail ;
      private bool[] T003O46_n1419ContagemResultadoNotificacao_DestEmail ;
      private int[] T003O46_A1414ContagemResultadoNotificacao_DestCod ;
      private int[] T003O46_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] T003O46_n1426ContagemResultadoNotificacao_DestPesCod ;
      private int[] T003O11_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] T003O11_n1426ContagemResultadoNotificacao_DestPesCod ;
      private String[] T003O12_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] T003O12_n1421ContagemResultadoNotificacao_DestNome ;
      private int[] T003O47_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] T003O47_n1426ContagemResultadoNotificacao_DestPesCod ;
      private String[] T003O48_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] T003O48_n1421ContagemResultadoNotificacao_DestNome ;
      private long[] T003O49_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] T003O49_A1414ContagemResultadoNotificacao_DestCod ;
      private long[] T003O10_A1412ContagemResultadoNotificacao_Codigo ;
      private String[] T003O10_A1419ContagemResultadoNotificacao_DestEmail ;
      private bool[] T003O10_n1419ContagemResultadoNotificacao_DestEmail ;
      private int[] T003O10_A1414ContagemResultadoNotificacao_DestCod ;
      private long[] T003O9_A1412ContagemResultadoNotificacao_Codigo ;
      private String[] T003O9_A1419ContagemResultadoNotificacao_DestEmail ;
      private bool[] T003O9_n1419ContagemResultadoNotificacao_DestEmail ;
      private int[] T003O9_A1414ContagemResultadoNotificacao_DestCod ;
      private int[] T003O53_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] T003O53_n1426ContagemResultadoNotificacao_DestPesCod ;
      private String[] T003O54_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] T003O54_n1421ContagemResultadoNotificacao_DestNome ;
      private long[] T003O55_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] T003O55_A1414ContagemResultadoNotificacao_DestCod ;
      private int[] T003O56_A1553ContagemResultado_CntSrvCod ;
      private bool[] T003O56_n1553ContagemResultado_CntSrvCod ;
      private long[] T003O56_A1412ContagemResultadoNotificacao_Codigo ;
      private String[] T003O56_A457ContagemResultado_Demanda ;
      private bool[] T003O56_n457ContagemResultado_Demanda ;
      private String[] T003O56_A493ContagemResultado_DemandaFM ;
      private bool[] T003O56_n493ContagemResultado_DemandaFM ;
      private String[] T003O56_A801ContagemResultado_ServicoSigla ;
      private bool[] T003O56_n801ContagemResultado_ServicoSigla ;
      private short[] T003O56_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] T003O56_n1227ContagemResultado_PrazoInicialDias ;
      private int[] T003O56_A456ContagemResultado_Codigo ;
      private int[] T003O56_A601ContagemResultado_Servico ;
      private bool[] T003O56_n601ContagemResultado_Servico ;
      private int[] T003O4_A1553ContagemResultado_CntSrvCod ;
      private bool[] T003O4_n1553ContagemResultado_CntSrvCod ;
      private String[] T003O4_A457ContagemResultado_Demanda ;
      private bool[] T003O4_n457ContagemResultado_Demanda ;
      private String[] T003O4_A493ContagemResultado_DemandaFM ;
      private bool[] T003O4_n493ContagemResultado_DemandaFM ;
      private short[] T003O4_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] T003O4_n1227ContagemResultado_PrazoInicialDias ;
      private int[] T003O5_A601ContagemResultado_Servico ;
      private bool[] T003O5_n601ContagemResultado_Servico ;
      private String[] T003O6_A801ContagemResultado_ServicoSigla ;
      private bool[] T003O6_n801ContagemResultado_ServicoSigla ;
      private int[] T003O57_A1553ContagemResultado_CntSrvCod ;
      private bool[] T003O57_n1553ContagemResultado_CntSrvCod ;
      private String[] T003O57_A457ContagemResultado_Demanda ;
      private bool[] T003O57_n457ContagemResultado_Demanda ;
      private String[] T003O57_A493ContagemResultado_DemandaFM ;
      private bool[] T003O57_n493ContagemResultado_DemandaFM ;
      private short[] T003O57_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] T003O57_n1227ContagemResultado_PrazoInicialDias ;
      private int[] T003O58_A601ContagemResultado_Servico ;
      private bool[] T003O58_n601ContagemResultado_Servico ;
      private String[] T003O59_A801ContagemResultado_ServicoSigla ;
      private bool[] T003O59_n801ContagemResultado_ServicoSigla ;
      private long[] T003O60_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] T003O60_A456ContagemResultado_Codigo ;
      private long[] T003O3_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] T003O3_A456ContagemResultado_Codigo ;
      private long[] T003O2_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] T003O2_A456ContagemResultado_Codigo ;
      private int[] T003O63_A1553ContagemResultado_CntSrvCod ;
      private bool[] T003O63_n1553ContagemResultado_CntSrvCod ;
      private String[] T003O63_A457ContagemResultado_Demanda ;
      private bool[] T003O63_n457ContagemResultado_Demanda ;
      private String[] T003O63_A493ContagemResultado_DemandaFM ;
      private bool[] T003O63_n493ContagemResultado_DemandaFM ;
      private short[] T003O63_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] T003O63_n1227ContagemResultado_PrazoInicialDias ;
      private int[] T003O64_A601ContagemResultado_Servico ;
      private bool[] T003O64_n601ContagemResultado_Servico ;
      private String[] T003O65_A801ContagemResultado_ServicoSigla ;
      private bool[] T003O65_n801ContagemResultado_ServicoSigla ;
      private long[] T003O66_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] T003O66_A456ContagemResultado_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class contagemresultadonotificacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new UpdateCursor(def[28])
         ,new UpdateCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new UpdateCursor(def[40])
         ,new UpdateCursor(def[41])
         ,new UpdateCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new ForEachCursor(def[45])
         ,new ForEachCursor(def[46])
         ,new ForEachCursor(def[47])
         ,new ForEachCursor(def[48])
         ,new ForEachCursor(def[49])
         ,new ForEachCursor(def[50])
         ,new UpdateCursor(def[51])
         ,new UpdateCursor(def[52])
         ,new ForEachCursor(def[53])
         ,new ForEachCursor(def[54])
         ,new ForEachCursor(def[55])
         ,new ForEachCursor(def[56])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003O23 ;
          prmT003O23 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          String cmdBufferT003O23 ;
          cmdBufferT003O23=" SELECT TM1.[ContagemResultadoNotificacao_Codigo], TM1.[ContagemResultadoNotificacao_DataHora], T5.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, TM1.[ContagemResultadoNotificacao_Assunto], TM1.[ContagemResultadoNotificacao_CorpoEmail], TM1.[ContagemResultadoNotificacao_Midia], TM1.[ContagemResultadoNotificacao_Observacao], TM1.[ContagemResultadoNotificacao_Host], TM1.[ContagemResultadoNotificacao_User], TM1.[ContagemResultadoNotificacao_Port], TM1.[ContagemResultadoNotificacao_Aut], TM1.[ContagemResultadoNotificacao_Sec], TM1.[ContagemResultadoNotificacao_Logged], TM1.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, TM1.[ContagemResultadoNotificacao_AreaTrabalhoCod] AS ContagemResultadoNotificacao_AreaTrabalhoCod, TM1.[ContagemResultadoNotificacao_ReenvioCod] AS ContagemResultadoNotificacao_ReenvioCod, T4.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, COALESCE( T2.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios, COALESCE( T3.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas FROM (((([ContagemResultadoNotificacao] TM1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T2 ON T2.[ContagemResultadoNotificacao_Codigo] = TM1.[ContagemResultadoNotificacao_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T3 ON T3.[ContagemResultadoNotificacao_Codigo] = TM1.[ContagemResultadoNotificacao_Codigo]) "
          + " INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = TM1.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE TM1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ORDER BY TM1.[ContagemResultadoNotificacao_Codigo]  OPTION (FAST 100)" ;
          Object[] prmT003O20 ;
          prmT003O20 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O8 ;
          prmT003O8 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O15 ;
          prmT003O15 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_UsuCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O18 ;
          prmT003O18 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_UsuPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O16 ;
          prmT003O16 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O17 ;
          prmT003O17 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_ReenvioCod",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O25 ;
          prmT003O25 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O27 ;
          prmT003O27 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O28 ;
          prmT003O28 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_UsuCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O29 ;
          prmT003O29 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_UsuPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O30 ;
          prmT003O30 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O31 ;
          prmT003O31 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_ReenvioCod",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O32 ;
          prmT003O32 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O14 ;
          prmT003O14 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O33 ;
          prmT003O33 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O34 ;
          prmT003O34 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O13 ;
          prmT003O13 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O35 ;
          prmT003O35 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoNotificacao_Assunto",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContagemResultadoNotificacao_CorpoEmail",SqlDbType.VarChar,8000,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Midia",SqlDbType.Char,3,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Host",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoNotificacao_User",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Aut",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Sec",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Logged",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_UsuCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNotificacao_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNotificacao_ReenvioCod",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O36 ;
          prmT003O36 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoNotificacao_Assunto",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContagemResultadoNotificacao_CorpoEmail",SqlDbType.VarChar,8000,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Midia",SqlDbType.Char,3,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Host",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoNotificacao_User",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Aut",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Sec",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Logged",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoNotificacao_UsuCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNotificacao_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNotificacao_ReenvioCod",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O37 ;
          prmT003O37 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O44 ;
          prmT003O44 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O45 ;
          prmT003O45 = new Object[] {
          } ;
          Object[] prmT003O46 ;
          prmT003O46 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O11 ;
          prmT003O11 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O12 ;
          prmT003O12 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DestPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O47 ;
          prmT003O47 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O48 ;
          prmT003O48 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DestPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O49 ;
          prmT003O49 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O10 ;
          prmT003O10 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O9 ;
          prmT003O9 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O50 ;
          prmT003O50 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestEmail",SqlDbType.VarChar,100,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O51 ;
          prmT003O51 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DestEmail",SqlDbType.VarChar,100,0} ,
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O52 ;
          prmT003O52 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O55 ;
          prmT003O55 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O56 ;
          prmT003O56 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O4 ;
          prmT003O4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O5 ;
          prmT003O5 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O6 ;
          prmT003O6 = new Object[] {
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O57 ;
          prmT003O57 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O58 ;
          prmT003O58 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O59 ;
          prmT003O59 = new Object[] {
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O60 ;
          prmT003O60 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O3 ;
          prmT003O3 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O2 ;
          prmT003O2 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O61 ;
          prmT003O61 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O62 ;
          prmT003O62 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O66 ;
          prmT003O66 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O39 ;
          prmT003O39 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O41 ;
          prmT003O41 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT003O42 ;
          prmT003O42 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_UsuCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O43 ;
          prmT003O43 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_UsuPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O53 ;
          prmT003O53 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O54 ;
          prmT003O54 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_DestPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O63 ;
          prmT003O63 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O64 ;
          prmT003O64 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003O65 ;
          prmT003O65 = new Object[] {
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003O2", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (UPDLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O2,1,0,true,false )
             ,new CursorDef("T003O3", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O3,1,0,true,false )
             ,new CursorDef("T003O4", "SELECT [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_Demanda], [ContagemResultado_DemandaFM], [ContagemResultado_PrazoInicialDias] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O4,1,0,true,false )
             ,new CursorDef("T003O5", "SELECT [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O5,1,0,true,false )
             ,new CursorDef("T003O6", "SELECT [Servico_Sigla] AS ContagemResultado_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_Servico ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O6,1,0,true,false )
             ,new CursorDef("T003O8", "SELECT COALESCE( T1.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas FROM (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (UPDLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T1 WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O8,1,0,true,false )
             ,new CursorDef("T003O9", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DestEmail], [ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D FROM [ContagemResultadoNotificacaoDestinatario] WITH (UPDLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultadoNotificacao_DestCod] = @ContagemResultadoNotificacao_DestCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O9,1,0,true,false )
             ,new CursorDef("T003O10", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DestEmail], [ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultadoNotificacao_DestCod] = @ContagemResultadoNotificacao_DestCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O10,1,0,true,false )
             ,new CursorDef("T003O11", "SELECT [Usuario_PessoaCod] AS ContagemResultadoNotificacao_D FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoNotificacao_DestCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O11,1,0,true,false )
             ,new CursorDef("T003O12", "SELECT [Pessoa_Nome] AS ContagemResultadoNotificacao_D FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoNotificacao_DestPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O12,1,0,true,false )
             ,new CursorDef("T003O13", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DataHora], [ContagemResultadoNotificacao_Assunto], [ContagemResultadoNotificacao_CorpoEmail], [ContagemResultadoNotificacao_Midia], [ContagemResultadoNotificacao_Observacao], [ContagemResultadoNotificacao_Host], [ContagemResultadoNotificacao_User], [ContagemResultadoNotificacao_Port], [ContagemResultadoNotificacao_Aut], [ContagemResultadoNotificacao_Sec], [ContagemResultadoNotificacao_Logged], [ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, [ContagemResultadoNotificacao_AreaTrabalhoCod] AS ContagemResultadoNotificacao_AreaTrabalhoCod, [ContagemResultadoNotificacao_ReenvioCod] AS ContagemResultadoNotificacao_ReenvioCod FROM [ContagemResultadoNotificacao] WITH (UPDLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O13,1,0,true,false )
             ,new CursorDef("T003O14", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DataHora], [ContagemResultadoNotificacao_Assunto], [ContagemResultadoNotificacao_CorpoEmail], [ContagemResultadoNotificacao_Midia], [ContagemResultadoNotificacao_Observacao], [ContagemResultadoNotificacao_Host], [ContagemResultadoNotificacao_User], [ContagemResultadoNotificacao_Port], [ContagemResultadoNotificacao_Aut], [ContagemResultadoNotificacao_Sec], [ContagemResultadoNotificacao_Logged], [ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, [ContagemResultadoNotificacao_AreaTrabalhoCod] AS ContagemResultadoNotificacao_AreaTrabalhoCod, [ContagemResultadoNotificacao_ReenvioCod] AS ContagemResultadoNotificacao_ReenvioCod FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O14,1,0,true,false )
             ,new CursorDef("T003O15", "SELECT [Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoNotificacao_UsuCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O15,1,0,true,false )
             ,new CursorDef("T003O16", "SELECT [AreaTrabalho_Codigo] AS ContagemResultadoNotificacao_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ContagemResultadoNotificacao_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O16,1,0,true,false )
             ,new CursorDef("T003O17", "SELECT [ContagemResultadoNotificacao_Codigo] AS ContagemResultadoNotificacao_ReenvioCod FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_ReenvioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O17,1,0,true,false )
             ,new CursorDef("T003O18", "SELECT [Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoNotificacao_UsuPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O18,1,0,true,false )
             ,new CursorDef("T003O20", "SELECT COALESCE( T1.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios FROM (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (UPDLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T1 WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O20,1,0,true,false )
             ,new CursorDef("T003O23", cmdBufferT003O23,true, GxErrorMask.GX_NOMASK, false, this,prmT003O23,100,0,true,false )
             ,new CursorDef("T003O25", "SELECT COALESCE( T1.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios FROM (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (UPDLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T1 WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O25,1,0,true,false )
             ,new CursorDef("T003O27", "SELECT COALESCE( T1.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas FROM (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (UPDLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T1 WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O27,1,0,true,false )
             ,new CursorDef("T003O28", "SELECT [Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoNotificacao_UsuCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O28,1,0,true,false )
             ,new CursorDef("T003O29", "SELECT [Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoNotificacao_UsuPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O29,1,0,true,false )
             ,new CursorDef("T003O30", "SELECT [AreaTrabalho_Codigo] AS ContagemResultadoNotificacao_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ContagemResultadoNotificacao_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O30,1,0,true,false )
             ,new CursorDef("T003O31", "SELECT [ContagemResultadoNotificacao_Codigo] AS ContagemResultadoNotificacao_ReenvioCod FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_ReenvioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O31,1,0,true,false )
             ,new CursorDef("T003O32", "SELECT [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003O32,1,0,true,false )
             ,new CursorDef("T003O33", "SELECT TOP 1 [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE ( [ContagemResultadoNotificacao_Codigo] > @ContagemResultadoNotificacao_Codigo) ORDER BY [ContagemResultadoNotificacao_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003O33,1,0,true,true )
             ,new CursorDef("T003O34", "SELECT TOP 1 [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE ( [ContagemResultadoNotificacao_Codigo] < @ContagemResultadoNotificacao_Codigo) ORDER BY [ContagemResultadoNotificacao_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003O34,1,0,true,true )
             ,new CursorDef("T003O35", "INSERT INTO [ContagemResultadoNotificacao]([ContagemResultadoNotificacao_DataHora], [ContagemResultadoNotificacao_Assunto], [ContagemResultadoNotificacao_CorpoEmail], [ContagemResultadoNotificacao_Midia], [ContagemResultadoNotificacao_Observacao], [ContagemResultadoNotificacao_Host], [ContagemResultadoNotificacao_User], [ContagemResultadoNotificacao_Port], [ContagemResultadoNotificacao_Aut], [ContagemResultadoNotificacao_Sec], [ContagemResultadoNotificacao_Logged], [ContagemResultadoNotificacao_UsuCod], [ContagemResultadoNotificacao_AreaTrabalhoCod], [ContagemResultadoNotificacao_ReenvioCod]) VALUES(@ContagemResultadoNotificacao_DataHora, @ContagemResultadoNotificacao_Assunto, @ContagemResultadoNotificacao_CorpoEmail, @ContagemResultadoNotificacao_Midia, @ContagemResultadoNotificacao_Observacao, @ContagemResultadoNotificacao_Host, @ContagemResultadoNotificacao_User, @ContagemResultadoNotificacao_Port, @ContagemResultadoNotificacao_Aut, @ContagemResultadoNotificacao_Sec, @ContagemResultadoNotificacao_Logged, @ContagemResultadoNotificacao_UsuCod, @ContagemResultadoNotificacao_AreaTrabalhoCod, @ContagemResultadoNotificacao_ReenvioCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003O35)
             ,new CursorDef("T003O36", "UPDATE [ContagemResultadoNotificacao] SET [ContagemResultadoNotificacao_DataHora]=@ContagemResultadoNotificacao_DataHora, [ContagemResultadoNotificacao_Assunto]=@ContagemResultadoNotificacao_Assunto, [ContagemResultadoNotificacao_CorpoEmail]=@ContagemResultadoNotificacao_CorpoEmail, [ContagemResultadoNotificacao_Midia]=@ContagemResultadoNotificacao_Midia, [ContagemResultadoNotificacao_Observacao]=@ContagemResultadoNotificacao_Observacao, [ContagemResultadoNotificacao_Host]=@ContagemResultadoNotificacao_Host, [ContagemResultadoNotificacao_User]=@ContagemResultadoNotificacao_User, [ContagemResultadoNotificacao_Port]=@ContagemResultadoNotificacao_Port, [ContagemResultadoNotificacao_Aut]=@ContagemResultadoNotificacao_Aut, [ContagemResultadoNotificacao_Sec]=@ContagemResultadoNotificacao_Sec, [ContagemResultadoNotificacao_Logged]=@ContagemResultadoNotificacao_Logged, [ContagemResultadoNotificacao_UsuCod]=@ContagemResultadoNotificacao_UsuCod, [ContagemResultadoNotificacao_AreaTrabalhoCod]=@ContagemResultadoNotificacao_AreaTrabalhoCod, [ContagemResultadoNotificacao_ReenvioCod]=@ContagemResultadoNotificacao_ReenvioCod  WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo", GxErrorMask.GX_NOMASK,prmT003O36)
             ,new CursorDef("T003O37", "DELETE FROM [ContagemResultadoNotificacao]  WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo", GxErrorMask.GX_NOMASK,prmT003O37)
             ,new CursorDef("T003O39", "SELECT COALESCE( T1.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios FROM (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (UPDLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T1 WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O39,1,0,true,false )
             ,new CursorDef("T003O41", "SELECT COALESCE( T1.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas FROM (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (UPDLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T1 WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O41,1,0,true,false )
             ,new CursorDef("T003O42", "SELECT [Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoNotificacao_UsuCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O42,1,0,true,false )
             ,new CursorDef("T003O43", "SELECT [Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoNotificacao_UsuPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O43,1,0,true,false )
             ,new CursorDef("T003O44", "SELECT TOP 1 [ContagemResultadoNotificacao_Codigo] AS ContagemResultadoNotificacao_ReenvioCod FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_ReenvioCod] = @ContagemResultadoNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O44,1,0,true,true )
             ,new CursorDef("T003O45", "SELECT [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacao] WITH (NOLOCK) ORDER BY [ContagemResultadoNotificacao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003O45,100,0,true,false )
             ,new CursorDef("T003O46", "SELECT T1.[ContagemResultadoNotificacao_Codigo], T3.[Pessoa_Nome] AS ContagemResultadoNotificacao_D, T1.[ContagemResultadoNotificacao_DestEmail], T1.[ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D, T2.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_D FROM (([ContagemResultadoNotificacaoDestinatario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_DestCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo and T1.[ContagemResultadoNotificacao_DestCod] = @ContagemResultadoNotificacao_DestCod ORDER BY T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultadoNotificacao_DestCod] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O46,11,0,true,false )
             ,new CursorDef("T003O47", "SELECT [Usuario_PessoaCod] AS ContagemResultadoNotificacao_D FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoNotificacao_DestCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O47,1,0,true,false )
             ,new CursorDef("T003O48", "SELECT [Pessoa_Nome] AS ContagemResultadoNotificacao_D FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoNotificacao_DestPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O48,1,0,true,false )
             ,new CursorDef("T003O49", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultadoNotificacao_DestCod] = @ContagemResultadoNotificacao_DestCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O49,1,0,true,false )
             ,new CursorDef("T003O50", "INSERT INTO [ContagemResultadoNotificacaoDestinatario]([ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DestEmail], [ContagemResultadoNotificacao_DestCod]) VALUES(@ContagemResultadoNotificacao_Codigo, @ContagemResultadoNotificacao_DestEmail, @ContagemResultadoNotificacao_DestCod)", GxErrorMask.GX_NOMASK,prmT003O50)
             ,new CursorDef("T003O51", "UPDATE [ContagemResultadoNotificacaoDestinatario] SET [ContagemResultadoNotificacao_DestEmail]=@ContagemResultadoNotificacao_DestEmail  WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultadoNotificacao_DestCod] = @ContagemResultadoNotificacao_DestCod", GxErrorMask.GX_NOMASK,prmT003O51)
             ,new CursorDef("T003O52", "DELETE FROM [ContagemResultadoNotificacaoDestinatario]  WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultadoNotificacao_DestCod] = @ContagemResultadoNotificacao_DestCod", GxErrorMask.GX_NOMASK,prmT003O52)
             ,new CursorDef("T003O53", "SELECT [Usuario_PessoaCod] AS ContagemResultadoNotificacao_D FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoNotificacao_DestCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O53,1,0,true,false )
             ,new CursorDef("T003O54", "SELECT [Pessoa_Nome] AS ContagemResultadoNotificacao_D FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoNotificacao_DestPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O54,1,0,true,false )
             ,new CursorDef("T003O55", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ORDER BY [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DestCod] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O55,11,0,true,false )
             ,new CursorDef("T003O56", "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultadoNotificacao_Codigo], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_DemandaFM], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T2.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_Codigo], T3.[Servico_Codigo] AS ContagemResultado_Servico FROM ((([ContagemResultadoNotificacaoDemanda] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo and T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O56,11,0,true,false )
             ,new CursorDef("T003O57", "SELECT [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_Demanda], [ContagemResultado_DemandaFM], [ContagemResultado_PrazoInicialDias] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O57,1,0,true,false )
             ,new CursorDef("T003O58", "SELECT [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O58,1,0,true,false )
             ,new CursorDef("T003O59", "SELECT [Servico_Sigla] AS ContagemResultado_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_Servico ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O59,1,0,true,false )
             ,new CursorDef("T003O60", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O60,1,0,true,false )
             ,new CursorDef("T003O61", "INSERT INTO [ContagemResultadoNotificacaoDemanda]([ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo]) VALUES(@ContagemResultadoNotificacao_Codigo, @ContagemResultado_Codigo)", GxErrorMask.GX_NOMASK,prmT003O61)
             ,new CursorDef("T003O62", "DELETE FROM [ContagemResultadoNotificacaoDemanda]  WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo AND [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK,prmT003O62)
             ,new CursorDef("T003O63", "SELECT [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_Demanda], [ContagemResultado_DemandaFM], [ContagemResultado_PrazoInicialDias] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O63,1,0,true,false )
             ,new CursorDef("T003O64", "SELECT [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O64,1,0,true,false )
             ,new CursorDef("T003O65", "SELECT [Servico_Sigla] AS ContagemResultado_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_Servico ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O65,1,0,true,false )
             ,new CursorDef("T003O66", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ORDER BY [ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003O66,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 7 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 3) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((bool[]) buf[17])[0] = rslt.getBool(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((short[]) buf[19])[0] = rslt.getShort(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((short[]) buf[21])[0] = rslt.getShort(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((long[]) buf[26])[0] = rslt.getLong(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                return;
             case 11 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 3) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((bool[]) buf[17])[0] = rslt.getBool(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((short[]) buf[19])[0] = rslt.getShort(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((short[]) buf[21])[0] = rslt.getShort(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((long[]) buf[26])[0] = rslt.getLong(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 3) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((short[]) buf[21])[0] = rslt.getShort(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((short[]) buf[23])[0] = rslt.getShort(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((long[]) buf[28])[0] = rslt.getLong(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 24 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 25 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 26 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 27 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 33 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 34 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 35 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 36 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 38 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 39 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 44 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 45 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 46 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((long[]) buf[2])[0] = rslt.getLong(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 47 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 48 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 49 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 50 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 53 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 54 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 55 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 56 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (long)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (long)parms[1]);
                }
                return;
             case 24 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 9 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(9, (bool)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[21]);
                }
                stmt.SetParameter(12, (int)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (long)parms[26]);
                }
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 9 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(9, (bool)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[21]);
                }
                stmt.SetParameter(12, (int)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (long)parms[26]);
                }
                stmt.SetParameter(15, (long)parms[27]);
                return;
             case 29 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 31 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 34 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 36 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 38 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 39 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 40 :
                stmt.SetParameter(1, (long)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 41 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (long)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 42 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 43 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 44 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 45 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 46 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 47 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 48 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 49 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 50 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 51 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 52 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 53 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 54 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 55 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 56 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
       }
    }

 }

}
