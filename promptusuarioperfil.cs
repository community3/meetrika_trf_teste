/*
               File: PromptUsuarioPerfil
        Description: Selecione Usuario x Perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:35:31.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptusuarioperfil : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptusuarioperfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptusuarioperfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutUsuario_Codigo ,
                           ref int aP1_InOutPerfil_Codigo ,
                           ref String aP2_InOutPerfil_Nome )
      {
         this.AV7InOutUsuario_Codigo = aP0_InOutUsuario_Codigo;
         this.AV8InOutPerfil_Codigo = aP1_InOutPerfil_Codigo;
         this.AV9InOutPerfil_Nome = aP2_InOutPerfil_Nome;
         executePrivate();
         aP0_InOutUsuario_Codigo=this.AV7InOutUsuario_Codigo;
         aP1_InOutPerfil_Codigo=this.AV8InOutPerfil_Codigo;
         aP2_InOutPerfil_Nome=this.AV9InOutPerfil_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbPerfil_Tipo = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_68 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_68_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_68_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
               AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV18Perfil_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Perfil_Nome1", AV18Perfil_Nome1);
               AV19Usuario_PessoaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Usuario_PessoaNom1", AV19Usuario_PessoaNom1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV23Perfil_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Perfil_Nome2", AV23Perfil_Nome2);
               AV24Usuario_PessoaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Usuario_PessoaNom2", AV24Usuario_PessoaNom2);
               AV26DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
               AV28Perfil_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Perfil_Nome3", AV28Perfil_Nome3);
               AV29Usuario_PessoaNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Usuario_PessoaNom3", AV29Usuario_PessoaNom3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV25DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
               AV38TFUsuario_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFUsuario_Nome", AV38TFUsuario_Nome);
               AV39TFUsuario_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFUsuario_Nome_Sel", AV39TFUsuario_Nome_Sel);
               AV42TFUsuario_PessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFUsuario_PessoaNom", AV42TFUsuario_PessoaNom);
               AV43TFUsuario_PessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFUsuario_PessoaNom_Sel", AV43TFUsuario_PessoaNom_Sel);
               AV46TFPerfil_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFPerfil_Nome", AV46TFPerfil_Nome);
               AV47TFPerfil_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFPerfil_Nome_Sel", AV47TFPerfil_Nome_Sel);
               AV40ddo_Usuario_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Usuario_NomeTitleControlIdToReplace", AV40ddo_Usuario_NomeTitleControlIdToReplace);
               AV44ddo_Usuario_PessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_Usuario_PessoaNomTitleControlIdToReplace", AV44ddo_Usuario_PessoaNomTitleControlIdToReplace);
               AV48ddo_Perfil_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Perfil_NomeTitleControlIdToReplace", AV48ddo_Perfil_NomeTitleControlIdToReplace);
               AV52ddo_Perfil_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_Perfil_TipoTitleControlIdToReplace", AV52ddo_Perfil_TipoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV51TFPerfil_Tipo_Sels);
               AV60Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
               AV31DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
               AV30DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV26DynamicFiltersSelector3, AV28Perfil_Nome3, AV29Usuario_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV38TFUsuario_Nome, AV39TFUsuario_Nome_Sel, AV42TFUsuario_PessoaNom, AV43TFUsuario_PessoaNom_Sel, AV46TFPerfil_Nome, AV47TFPerfil_Nome_Sel, AV40ddo_Usuario_NomeTitleControlIdToReplace, AV44ddo_Usuario_PessoaNomTitleControlIdToReplace, AV48ddo_Perfil_NomeTitleControlIdToReplace, AV52ddo_Perfil_TipoTitleControlIdToReplace, AV51TFPerfil_Tipo_Sels, AV60Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutUsuario_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutUsuario_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutPerfil_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutPerfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutPerfil_Codigo), 6, 0)));
                  AV9InOutPerfil_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutPerfil_Nome", AV9InOutPerfil_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA8Z2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV60Pgmname = "PromptUsuarioPerfil";
               context.Gx_err = 0;
               WS8Z2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE8Z2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299353127");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptusuarioperfil.aspx") + "?" + UrlEncode("" +AV7InOutUsuario_Codigo) + "," + UrlEncode("" +AV8InOutPerfil_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV9InOutPerfil_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vPERFIL_NOME1", StringUtil.RTrim( AV18Perfil_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOANOM1", StringUtil.RTrim( AV19Usuario_PessoaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vPERFIL_NOME2", StringUtil.RTrim( AV23Perfil_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOANOM2", StringUtil.RTrim( AV24Usuario_PessoaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV26DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vPERFIL_NOME3", StringUtil.RTrim( AV28Perfil_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOANOM3", StringUtil.RTrim( AV29Usuario_PessoaNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV25DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_NOME", StringUtil.RTrim( AV38TFUsuario_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_NOME_SEL", StringUtil.RTrim( AV39TFUsuario_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_PESSOANOM", StringUtil.RTrim( AV42TFUsuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_PESSOANOM_SEL", StringUtil.RTrim( AV43TFUsuario_PessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPERFIL_NOME", StringUtil.RTrim( AV46TFPerfil_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPERFIL_NOME_SEL", StringUtil.RTrim( AV47TFPerfil_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_68", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_68), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV53DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV53DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIO_NOMETITLEFILTERDATA", AV37Usuario_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIO_NOMETITLEFILTERDATA", AV37Usuario_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIO_PESSOANOMTITLEFILTERDATA", AV41Usuario_PessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIO_PESSOANOMTITLEFILTERDATA", AV41Usuario_PessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPERFIL_NOMETITLEFILTERDATA", AV45Perfil_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPERFIL_NOMETITLEFILTERDATA", AV45Perfil_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPERFIL_TIPOTITLEFILTERDATA", AV49Perfil_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPERFIL_TIPOTITLEFILTERDATA", AV49Perfil_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFPERFIL_TIPO_SELS", AV51TFPerfil_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFPERFIL_TIPO_SELS", AV51TFPerfil_Tipo_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV60Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV31DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV30DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutUsuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTPERFIL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8InOutPerfil_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTPERFIL_NOME", StringUtil.RTrim( AV9InOutPerfil_Nome));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Caption", StringUtil.RTrim( Ddo_usuario_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Tooltip", StringUtil.RTrim( Ddo_usuario_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Cls", StringUtil.RTrim( Ddo_usuario_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_usuario_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_usuario_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuario_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuario_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_usuario_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_usuario_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_usuario_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_usuario_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filtertype", StringUtil.RTrim( Ddo_usuario_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_usuario_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_usuario_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Datalisttype", StringUtil.RTrim( Ddo_usuario_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Datalistproc", StringUtil.RTrim( Ddo_usuario_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_usuario_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Sortasc", StringUtil.RTrim( Ddo_usuario_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Sortdsc", StringUtil.RTrim( Ddo_usuario_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Loadingdata", StringUtil.RTrim( Ddo_usuario_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_usuario_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_usuario_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_usuario_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Caption", StringUtil.RTrim( Ddo_usuario_pessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Tooltip", StringUtil.RTrim( Ddo_usuario_pessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Cls", StringUtil.RTrim( Ddo_usuario_pessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_usuario_pessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_usuario_pessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuario_pessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuario_pessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_usuario_pessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filtertype", StringUtil.RTrim( Ddo_usuario_pessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_usuario_pessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_usuario_pessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_usuario_pessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Sortasc", StringUtil.RTrim( Ddo_usuario_pessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_usuario_pessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_usuario_pessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_usuario_pessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_usuario_pessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_usuario_pessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Caption", StringUtil.RTrim( Ddo_perfil_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Tooltip", StringUtil.RTrim( Ddo_perfil_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Cls", StringUtil.RTrim( Ddo_perfil_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_perfil_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_perfil_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_perfil_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_perfil_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_perfil_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_perfil_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Sortedstatus", StringUtil.RTrim( Ddo_perfil_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Includefilter", StringUtil.BoolToStr( Ddo_perfil_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Filtertype", StringUtil.RTrim( Ddo_perfil_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_perfil_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_perfil_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Datalisttype", StringUtil.RTrim( Ddo_perfil_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Datalistproc", StringUtil.RTrim( Ddo_perfil_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_perfil_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Sortasc", StringUtil.RTrim( Ddo_perfil_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Sortdsc", StringUtil.RTrim( Ddo_perfil_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Loadingdata", StringUtil.RTrim( Ddo_perfil_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Cleanfilter", StringUtil.RTrim( Ddo_perfil_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Noresultsfound", StringUtil.RTrim( Ddo_perfil_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_perfil_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Caption", StringUtil.RTrim( Ddo_perfil_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Tooltip", StringUtil.RTrim( Ddo_perfil_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Cls", StringUtil.RTrim( Ddo_perfil_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_perfil_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_perfil_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_perfil_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_perfil_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_perfil_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_perfil_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_perfil_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_perfil_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Datalisttype", StringUtil.RTrim( Ddo_perfil_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_perfil_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_perfil_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Sortasc", StringUtil.RTrim( Ddo_perfil_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Sortdsc", StringUtil.RTrim( Ddo_perfil_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_perfil_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_perfil_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_usuario_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_usuario_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_usuario_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_usuario_pessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_usuario_pessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_usuario_pessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Activeeventkey", StringUtil.RTrim( Ddo_perfil_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_perfil_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_perfil_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_perfil_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_perfil_tipo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm8Z2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptUsuarioPerfil" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Usuario x Perfil" ;
      }

      protected void WB8Z0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_8Z2( true) ;
         }
         else
         {
            wb_table1_2_8Z2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_8Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_68_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(81, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_68_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV25DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(82, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_nome_Internalname, StringUtil.RTrim( AV38TFUsuario_Nome), StringUtil.RTrim( context.localUtil.Format( AV38TFUsuario_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuario_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_nome_sel_Internalname, StringUtil.RTrim( AV39TFUsuario_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV39TFUsuario_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuario_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_pessoanom_Internalname, StringUtil.RTrim( AV42TFUsuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV42TFUsuario_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_pessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfusuario_pessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_pessoanom_sel_Internalname, StringUtil.RTrim( AV43TFUsuario_PessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV43TFUsuario_PessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_pessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfusuario_pessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfperfil_nome_Internalname, StringUtil.RTrim( AV46TFPerfil_Nome), StringUtil.RTrim( context.localUtil.Format( AV46TFPerfil_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfperfil_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfperfil_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfperfil_nome_sel_Internalname, StringUtil.RTrim( AV47TFPerfil_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV47TFPerfil_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfperfil_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfperfil_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuarioPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_68_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname, AV40ddo_Usuario_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", 0, edtavDdo_usuario_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptUsuarioPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIO_PESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_68_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname, AV44ddo_Usuario_PessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", 0, edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptUsuarioPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PERFIL_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_68_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname, AV48ddo_Perfil_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", 0, edtavDdo_perfil_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptUsuarioPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PERFIL_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_68_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_perfil_tipotitlecontrolidtoreplace_Internalname, AV52ddo_Perfil_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", 0, edtavDdo_perfil_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptUsuarioPerfil.htm");
         }
         wbLoad = true;
      }

      protected void START8Z2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Usuario x Perfil", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP8Z0( ) ;
      }

      protected void WS8Z2( )
      {
         START8Z2( ) ;
         EVT8Z2( ) ;
      }

      protected void EVT8Z2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E118Z2 */
                           E118Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIO_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E128Z2 */
                           E128Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIO_PESSOANOM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E138Z2 */
                           E138Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_PERFIL_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E148Z2 */
                           E148Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_PERFIL_TIPO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E158Z2 */
                           E158Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E168Z2 */
                           E168Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E178Z2 */
                           E178Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E188Z2 */
                           E188Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E198Z2 */
                           E198Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E208Z2 */
                           E208Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E218Z2 */
                           E218Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E228Z2 */
                           E228Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E238Z2 */
                           E238Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E248Z2 */
                           E248Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E258Z2 */
                           E258Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_68_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
                           SubsflControlProps_682( ) ;
                           AV32Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Select)) ? AV59Select_GXI : context.convertURL( context.PathToRelativeUrl( AV32Select))));
                           A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", "."));
                           A3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPerfil_Codigo_Internalname), ",", "."));
                           A57Usuario_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtUsuario_PessoaCod_Internalname), ",", "."));
                           A2Usuario_Nome = StringUtil.Upper( cgiGet( edtUsuario_Nome_Internalname));
                           n2Usuario_Nome = false;
                           A58Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtUsuario_PessoaNom_Internalname));
                           n58Usuario_PessoaNom = false;
                           A4Perfil_Nome = StringUtil.Upper( cgiGet( edtPerfil_Nome_Internalname));
                           cmbPerfil_Tipo.Name = cmbPerfil_Tipo_Internalname;
                           cmbPerfil_Tipo.CurrentValue = cgiGet( cmbPerfil_Tipo_Internalname);
                           A275Perfil_Tipo = (short)(NumberUtil.Val( cgiGet( cmbPerfil_Tipo_Internalname), "."));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E268Z2 */
                                 E268Z2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E278Z2 */
                                 E278Z2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E288Z2 */
                                 E288Z2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Perfil_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPERFIL_NOME1"), AV18Perfil_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_pessoanom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM1"), AV19Usuario_PessoaNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Perfil_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPERFIL_NOME2"), AV23Perfil_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_pessoanom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM2"), AV24Usuario_PessoaNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Perfil_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPERFIL_NOME3"), AV28Perfil_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_pessoanom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM3"), AV29Usuario_PessoaNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfusuario_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME"), AV38TFUsuario_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfusuario_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME_SEL"), AV39TFUsuario_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfusuario_pessoanom Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM"), AV42TFUsuario_PessoaNom) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfusuario_pessoanom_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM_SEL"), AV43TFUsuario_PessoaNom_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfperfil_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPERFIL_NOME"), AV46TFPerfil_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfperfil_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPERFIL_NOME_SEL"), AV47TFPerfil_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E298Z2 */
                                       E298Z2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE8Z2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm8Z2( ) ;
            }
         }
      }

      protected void PA8Z2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("PERFIL_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("USUARIO_PESSOANOM", "Pessoa", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("PERFIL_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("USUARIO_PESSOANOM", "Pessoa", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("PERFIL_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("USUARIO_PESSOANOM", "Pessoa", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            }
            GXCCtl = "PERFIL_TIPO_" + sGXsfl_68_idx;
            cmbPerfil_Tipo.Name = GXCCtl;
            cmbPerfil_Tipo.WebTags = "";
            cmbPerfil_Tipo.addItem("0", "Desconhecido", 0);
            cmbPerfil_Tipo.addItem("1", "Auditor", 0);
            cmbPerfil_Tipo.addItem("2", "Contador", 0);
            cmbPerfil_Tipo.addItem("3", "Contratada", 0);
            cmbPerfil_Tipo.addItem("4", "Contratante", 0);
            cmbPerfil_Tipo.addItem("5", "Financeiro", 0);
            cmbPerfil_Tipo.addItem("6", "Usuario", 0);
            cmbPerfil_Tipo.addItem("10", "Licensiado", 0);
            cmbPerfil_Tipo.addItem("99", "Administrador GAM", 0);
            if ( cmbPerfil_Tipo.ItemCount > 0 )
            {
               A275Perfil_Tipo = (short)(NumberUtil.Val( cmbPerfil_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0))), "."));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_682( ) ;
         while ( nGXsfl_68_idx <= nRC_GXsfl_68 )
         {
            sendrow_682( ) ;
            nGXsfl_68_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_68_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_68_idx+1));
            sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
            SubsflControlProps_682( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV18Perfil_Nome1 ,
                                       String AV19Usuario_PessoaNom1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       String AV23Perfil_Nome2 ,
                                       String AV24Usuario_PessoaNom2 ,
                                       String AV26DynamicFiltersSelector3 ,
                                       String AV28Perfil_Nome3 ,
                                       String AV29Usuario_PessoaNom3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV25DynamicFiltersEnabled3 ,
                                       String AV38TFUsuario_Nome ,
                                       String AV39TFUsuario_Nome_Sel ,
                                       String AV42TFUsuario_PessoaNom ,
                                       String AV43TFUsuario_PessoaNom_Sel ,
                                       String AV46TFPerfil_Nome ,
                                       String AV47TFPerfil_Nome_Sel ,
                                       String AV40ddo_Usuario_NomeTitleControlIdToReplace ,
                                       String AV44ddo_Usuario_PessoaNomTitleControlIdToReplace ,
                                       String AV48ddo_Perfil_NomeTitleControlIdToReplace ,
                                       String AV52ddo_Perfil_TipoTitleControlIdToReplace ,
                                       IGxCollection AV51TFPerfil_Tipo_Sels ,
                                       String AV60Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV31DynamicFiltersIgnoreFirst ,
                                       bool AV30DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF8Z2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PERFIL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF8Z2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV60Pgmname = "PromptUsuarioPerfil";
         context.Gx_err = 0;
      }

      protected void RF8Z2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 68;
         /* Execute user event: E278Z2 */
         E278Z2 ();
         nGXsfl_68_idx = 1;
         sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
         SubsflControlProps_682( ) ;
         nGXsfl_68_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_682( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A275Perfil_Tipo ,
                                                 AV51TFPerfil_Tipo_Sels ,
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV18Perfil_Nome1 ,
                                                 AV19Usuario_PessoaNom1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV23Perfil_Nome2 ,
                                                 AV24Usuario_PessoaNom2 ,
                                                 AV25DynamicFiltersEnabled3 ,
                                                 AV26DynamicFiltersSelector3 ,
                                                 AV28Perfil_Nome3 ,
                                                 AV29Usuario_PessoaNom3 ,
                                                 AV39TFUsuario_Nome_Sel ,
                                                 AV38TFUsuario_Nome ,
                                                 AV43TFUsuario_PessoaNom_Sel ,
                                                 AV42TFUsuario_PessoaNom ,
                                                 AV47TFPerfil_Nome_Sel ,
                                                 AV46TFPerfil_Nome ,
                                                 AV51TFPerfil_Tipo_Sels.Count ,
                                                 A4Perfil_Nome ,
                                                 A58Usuario_PessoaNom ,
                                                 A2Usuario_Nome ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV18Perfil_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Perfil_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Perfil_Nome1", AV18Perfil_Nome1);
            lV19Usuario_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19Usuario_PessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Usuario_PessoaNom1", AV19Usuario_PessoaNom1);
            lV23Perfil_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23Perfil_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Perfil_Nome2", AV23Perfil_Nome2);
            lV24Usuario_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV24Usuario_PessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Usuario_PessoaNom2", AV24Usuario_PessoaNom2);
            lV28Perfil_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV28Perfil_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Perfil_Nome3", AV28Perfil_Nome3);
            lV29Usuario_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV29Usuario_PessoaNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Usuario_PessoaNom3", AV29Usuario_PessoaNom3);
            lV38TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV38TFUsuario_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFUsuario_Nome", AV38TFUsuario_Nome);
            lV42TFUsuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV42TFUsuario_PessoaNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFUsuario_PessoaNom", AV42TFUsuario_PessoaNom);
            lV46TFPerfil_Nome = StringUtil.PadR( StringUtil.RTrim( AV46TFPerfil_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFPerfil_Nome", AV46TFPerfil_Nome);
            /* Using cursor H008Z2 */
            pr_default.execute(0, new Object[] {lV18Perfil_Nome1, lV19Usuario_PessoaNom1, lV23Perfil_Nome2, lV24Usuario_PessoaNom2, lV28Perfil_Nome3, lV29Usuario_PessoaNom3, lV38TFUsuario_Nome, AV39TFUsuario_Nome_Sel, lV42TFUsuario_PessoaNom, AV43TFUsuario_PessoaNom_Sel, lV46TFPerfil_Nome, AV47TFPerfil_Nome_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_68_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A275Perfil_Tipo = H008Z2_A275Perfil_Tipo[0];
               A4Perfil_Nome = H008Z2_A4Perfil_Nome[0];
               A58Usuario_PessoaNom = H008Z2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H008Z2_n58Usuario_PessoaNom[0];
               A2Usuario_Nome = H008Z2_A2Usuario_Nome[0];
               n2Usuario_Nome = H008Z2_n2Usuario_Nome[0];
               A57Usuario_PessoaCod = H008Z2_A57Usuario_PessoaCod[0];
               A3Perfil_Codigo = H008Z2_A3Perfil_Codigo[0];
               A1Usuario_Codigo = H008Z2_A1Usuario_Codigo[0];
               A275Perfil_Tipo = H008Z2_A275Perfil_Tipo[0];
               A4Perfil_Nome = H008Z2_A4Perfil_Nome[0];
               A2Usuario_Nome = H008Z2_A2Usuario_Nome[0];
               n2Usuario_Nome = H008Z2_n2Usuario_Nome[0];
               A57Usuario_PessoaCod = H008Z2_A57Usuario_PessoaCod[0];
               A58Usuario_PessoaNom = H008Z2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H008Z2_n58Usuario_PessoaNom[0];
               /* Execute user event: E288Z2 */
               E288Z2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 68;
            WB8Z0( ) ;
         }
         nGXsfl_68_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A275Perfil_Tipo ,
                                              AV51TFPerfil_Tipo_Sels ,
                                              AV16DynamicFiltersSelector1 ,
                                              AV18Perfil_Nome1 ,
                                              AV19Usuario_PessoaNom1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV23Perfil_Nome2 ,
                                              AV24Usuario_PessoaNom2 ,
                                              AV25DynamicFiltersEnabled3 ,
                                              AV26DynamicFiltersSelector3 ,
                                              AV28Perfil_Nome3 ,
                                              AV29Usuario_PessoaNom3 ,
                                              AV39TFUsuario_Nome_Sel ,
                                              AV38TFUsuario_Nome ,
                                              AV43TFUsuario_PessoaNom_Sel ,
                                              AV42TFUsuario_PessoaNom ,
                                              AV47TFPerfil_Nome_Sel ,
                                              AV46TFPerfil_Nome ,
                                              AV51TFPerfil_Tipo_Sels.Count ,
                                              A4Perfil_Nome ,
                                              A58Usuario_PessoaNom ,
                                              A2Usuario_Nome ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV18Perfil_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Perfil_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Perfil_Nome1", AV18Perfil_Nome1);
         lV19Usuario_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19Usuario_PessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Usuario_PessoaNom1", AV19Usuario_PessoaNom1);
         lV23Perfil_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23Perfil_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Perfil_Nome2", AV23Perfil_Nome2);
         lV24Usuario_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV24Usuario_PessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Usuario_PessoaNom2", AV24Usuario_PessoaNom2);
         lV28Perfil_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV28Perfil_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Perfil_Nome3", AV28Perfil_Nome3);
         lV29Usuario_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV29Usuario_PessoaNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Usuario_PessoaNom3", AV29Usuario_PessoaNom3);
         lV38TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV38TFUsuario_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFUsuario_Nome", AV38TFUsuario_Nome);
         lV42TFUsuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV42TFUsuario_PessoaNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFUsuario_PessoaNom", AV42TFUsuario_PessoaNom);
         lV46TFPerfil_Nome = StringUtil.PadR( StringUtil.RTrim( AV46TFPerfil_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFPerfil_Nome", AV46TFPerfil_Nome);
         /* Using cursor H008Z3 */
         pr_default.execute(1, new Object[] {lV18Perfil_Nome1, lV19Usuario_PessoaNom1, lV23Perfil_Nome2, lV24Usuario_PessoaNom2, lV28Perfil_Nome3, lV29Usuario_PessoaNom3, lV38TFUsuario_Nome, AV39TFUsuario_Nome_Sel, lV42TFUsuario_PessoaNom, AV43TFUsuario_PessoaNom_Sel, lV46TFPerfil_Nome, AV47TFPerfil_Nome_Sel});
         GRID_nRecordCount = H008Z3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV26DynamicFiltersSelector3, AV28Perfil_Nome3, AV29Usuario_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV38TFUsuario_Nome, AV39TFUsuario_Nome_Sel, AV42TFUsuario_PessoaNom, AV43TFUsuario_PessoaNom_Sel, AV46TFPerfil_Nome, AV47TFPerfil_Nome_Sel, AV40ddo_Usuario_NomeTitleControlIdToReplace, AV44ddo_Usuario_PessoaNomTitleControlIdToReplace, AV48ddo_Perfil_NomeTitleControlIdToReplace, AV52ddo_Perfil_TipoTitleControlIdToReplace, AV51TFPerfil_Tipo_Sels, AV60Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV26DynamicFiltersSelector3, AV28Perfil_Nome3, AV29Usuario_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV38TFUsuario_Nome, AV39TFUsuario_Nome_Sel, AV42TFUsuario_PessoaNom, AV43TFUsuario_PessoaNom_Sel, AV46TFPerfil_Nome, AV47TFPerfil_Nome_Sel, AV40ddo_Usuario_NomeTitleControlIdToReplace, AV44ddo_Usuario_PessoaNomTitleControlIdToReplace, AV48ddo_Perfil_NomeTitleControlIdToReplace, AV52ddo_Perfil_TipoTitleControlIdToReplace, AV51TFPerfil_Tipo_Sels, AV60Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV26DynamicFiltersSelector3, AV28Perfil_Nome3, AV29Usuario_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV38TFUsuario_Nome, AV39TFUsuario_Nome_Sel, AV42TFUsuario_PessoaNom, AV43TFUsuario_PessoaNom_Sel, AV46TFPerfil_Nome, AV47TFPerfil_Nome_Sel, AV40ddo_Usuario_NomeTitleControlIdToReplace, AV44ddo_Usuario_PessoaNomTitleControlIdToReplace, AV48ddo_Perfil_NomeTitleControlIdToReplace, AV52ddo_Perfil_TipoTitleControlIdToReplace, AV51TFPerfil_Tipo_Sels, AV60Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV26DynamicFiltersSelector3, AV28Perfil_Nome3, AV29Usuario_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV38TFUsuario_Nome, AV39TFUsuario_Nome_Sel, AV42TFUsuario_PessoaNom, AV43TFUsuario_PessoaNom_Sel, AV46TFPerfil_Nome, AV47TFPerfil_Nome_Sel, AV40ddo_Usuario_NomeTitleControlIdToReplace, AV44ddo_Usuario_PessoaNomTitleControlIdToReplace, AV48ddo_Perfil_NomeTitleControlIdToReplace, AV52ddo_Perfil_TipoTitleControlIdToReplace, AV51TFPerfil_Tipo_Sels, AV60Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV26DynamicFiltersSelector3, AV28Perfil_Nome3, AV29Usuario_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV38TFUsuario_Nome, AV39TFUsuario_Nome_Sel, AV42TFUsuario_PessoaNom, AV43TFUsuario_PessoaNom_Sel, AV46TFPerfil_Nome, AV47TFPerfil_Nome_Sel, AV40ddo_Usuario_NomeTitleControlIdToReplace, AV44ddo_Usuario_PessoaNomTitleControlIdToReplace, AV48ddo_Perfil_NomeTitleControlIdToReplace, AV52ddo_Perfil_TipoTitleControlIdToReplace, AV51TFPerfil_Tipo_Sels, AV60Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP8Z0( )
      {
         /* Before Start, stand alone formulas. */
         AV60Pgmname = "PromptUsuarioPerfil";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E268Z2 */
         E268Z2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV53DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIO_NOMETITLEFILTERDATA"), AV37Usuario_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIO_PESSOANOMTITLEFILTERDATA"), AV41Usuario_PessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPERFIL_NOMETITLEFILTERDATA"), AV45Perfil_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPERFIL_TIPOTITLEFILTERDATA"), AV49Perfil_TipoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV18Perfil_Nome1 = StringUtil.Upper( cgiGet( edtavPerfil_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Perfil_Nome1", AV18Perfil_Nome1);
            AV19Usuario_PessoaNom1 = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Usuario_PessoaNom1", AV19Usuario_PessoaNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            AV23Perfil_Nome2 = StringUtil.Upper( cgiGet( edtavPerfil_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Perfil_Nome2", AV23Perfil_Nome2);
            AV24Usuario_PessoaNom2 = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Usuario_PessoaNom2", AV24Usuario_PessoaNom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV26DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            AV28Perfil_Nome3 = StringUtil.Upper( cgiGet( edtavPerfil_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Perfil_Nome3", AV28Perfil_Nome3);
            AV29Usuario_PessoaNom3 = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Usuario_PessoaNom3", AV29Usuario_PessoaNom3);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV25DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
            AV38TFUsuario_Nome = StringUtil.Upper( cgiGet( edtavTfusuario_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFUsuario_Nome", AV38TFUsuario_Nome);
            AV39TFUsuario_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfusuario_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFUsuario_Nome_Sel", AV39TFUsuario_Nome_Sel);
            AV42TFUsuario_PessoaNom = StringUtil.Upper( cgiGet( edtavTfusuario_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFUsuario_PessoaNom", AV42TFUsuario_PessoaNom);
            AV43TFUsuario_PessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfusuario_pessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFUsuario_PessoaNom_Sel", AV43TFUsuario_PessoaNom_Sel);
            AV46TFPerfil_Nome = StringUtil.Upper( cgiGet( edtavTfperfil_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFPerfil_Nome", AV46TFPerfil_Nome);
            AV47TFPerfil_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfperfil_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFPerfil_Nome_Sel", AV47TFPerfil_Nome_Sel);
            AV40ddo_Usuario_NomeTitleControlIdToReplace = cgiGet( edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Usuario_NomeTitleControlIdToReplace", AV40ddo_Usuario_NomeTitleControlIdToReplace);
            AV44ddo_Usuario_PessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_Usuario_PessoaNomTitleControlIdToReplace", AV44ddo_Usuario_PessoaNomTitleControlIdToReplace);
            AV48ddo_Perfil_NomeTitleControlIdToReplace = cgiGet( edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Perfil_NomeTitleControlIdToReplace", AV48ddo_Perfil_NomeTitleControlIdToReplace);
            AV52ddo_Perfil_TipoTitleControlIdToReplace = cgiGet( edtavDdo_perfil_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_Perfil_TipoTitleControlIdToReplace", AV52ddo_Perfil_TipoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_68 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_68"), ",", "."));
            AV55GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV56GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_usuario_nome_Caption = cgiGet( "DDO_USUARIO_NOME_Caption");
            Ddo_usuario_nome_Tooltip = cgiGet( "DDO_USUARIO_NOME_Tooltip");
            Ddo_usuario_nome_Cls = cgiGet( "DDO_USUARIO_NOME_Cls");
            Ddo_usuario_nome_Filteredtext_set = cgiGet( "DDO_USUARIO_NOME_Filteredtext_set");
            Ddo_usuario_nome_Selectedvalue_set = cgiGet( "DDO_USUARIO_NOME_Selectedvalue_set");
            Ddo_usuario_nome_Dropdownoptionstype = cgiGet( "DDO_USUARIO_NOME_Dropdownoptionstype");
            Ddo_usuario_nome_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIO_NOME_Titlecontrolidtoreplace");
            Ddo_usuario_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includesortasc"));
            Ddo_usuario_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includesortdsc"));
            Ddo_usuario_nome_Sortedstatus = cgiGet( "DDO_USUARIO_NOME_Sortedstatus");
            Ddo_usuario_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includefilter"));
            Ddo_usuario_nome_Filtertype = cgiGet( "DDO_USUARIO_NOME_Filtertype");
            Ddo_usuario_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Filterisrange"));
            Ddo_usuario_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includedatalist"));
            Ddo_usuario_nome_Datalisttype = cgiGet( "DDO_USUARIO_NOME_Datalisttype");
            Ddo_usuario_nome_Datalistproc = cgiGet( "DDO_USUARIO_NOME_Datalistproc");
            Ddo_usuario_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_USUARIO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_usuario_nome_Sortasc = cgiGet( "DDO_USUARIO_NOME_Sortasc");
            Ddo_usuario_nome_Sortdsc = cgiGet( "DDO_USUARIO_NOME_Sortdsc");
            Ddo_usuario_nome_Loadingdata = cgiGet( "DDO_USUARIO_NOME_Loadingdata");
            Ddo_usuario_nome_Cleanfilter = cgiGet( "DDO_USUARIO_NOME_Cleanfilter");
            Ddo_usuario_nome_Noresultsfound = cgiGet( "DDO_USUARIO_NOME_Noresultsfound");
            Ddo_usuario_nome_Searchbuttontext = cgiGet( "DDO_USUARIO_NOME_Searchbuttontext");
            Ddo_usuario_pessoanom_Caption = cgiGet( "DDO_USUARIO_PESSOANOM_Caption");
            Ddo_usuario_pessoanom_Tooltip = cgiGet( "DDO_USUARIO_PESSOANOM_Tooltip");
            Ddo_usuario_pessoanom_Cls = cgiGet( "DDO_USUARIO_PESSOANOM_Cls");
            Ddo_usuario_pessoanom_Filteredtext_set = cgiGet( "DDO_USUARIO_PESSOANOM_Filteredtext_set");
            Ddo_usuario_pessoanom_Selectedvalue_set = cgiGet( "DDO_USUARIO_PESSOANOM_Selectedvalue_set");
            Ddo_usuario_pessoanom_Dropdownoptionstype = cgiGet( "DDO_USUARIO_PESSOANOM_Dropdownoptionstype");
            Ddo_usuario_pessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIO_PESSOANOM_Titlecontrolidtoreplace");
            Ddo_usuario_pessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includesortasc"));
            Ddo_usuario_pessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includesortdsc"));
            Ddo_usuario_pessoanom_Sortedstatus = cgiGet( "DDO_USUARIO_PESSOANOM_Sortedstatus");
            Ddo_usuario_pessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includefilter"));
            Ddo_usuario_pessoanom_Filtertype = cgiGet( "DDO_USUARIO_PESSOANOM_Filtertype");
            Ddo_usuario_pessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Filterisrange"));
            Ddo_usuario_pessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includedatalist"));
            Ddo_usuario_pessoanom_Datalisttype = cgiGet( "DDO_USUARIO_PESSOANOM_Datalisttype");
            Ddo_usuario_pessoanom_Datalistproc = cgiGet( "DDO_USUARIO_PESSOANOM_Datalistproc");
            Ddo_usuario_pessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_USUARIO_PESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_usuario_pessoanom_Sortasc = cgiGet( "DDO_USUARIO_PESSOANOM_Sortasc");
            Ddo_usuario_pessoanom_Sortdsc = cgiGet( "DDO_USUARIO_PESSOANOM_Sortdsc");
            Ddo_usuario_pessoanom_Loadingdata = cgiGet( "DDO_USUARIO_PESSOANOM_Loadingdata");
            Ddo_usuario_pessoanom_Cleanfilter = cgiGet( "DDO_USUARIO_PESSOANOM_Cleanfilter");
            Ddo_usuario_pessoanom_Noresultsfound = cgiGet( "DDO_USUARIO_PESSOANOM_Noresultsfound");
            Ddo_usuario_pessoanom_Searchbuttontext = cgiGet( "DDO_USUARIO_PESSOANOM_Searchbuttontext");
            Ddo_perfil_nome_Caption = cgiGet( "DDO_PERFIL_NOME_Caption");
            Ddo_perfil_nome_Tooltip = cgiGet( "DDO_PERFIL_NOME_Tooltip");
            Ddo_perfil_nome_Cls = cgiGet( "DDO_PERFIL_NOME_Cls");
            Ddo_perfil_nome_Filteredtext_set = cgiGet( "DDO_PERFIL_NOME_Filteredtext_set");
            Ddo_perfil_nome_Selectedvalue_set = cgiGet( "DDO_PERFIL_NOME_Selectedvalue_set");
            Ddo_perfil_nome_Dropdownoptionstype = cgiGet( "DDO_PERFIL_NOME_Dropdownoptionstype");
            Ddo_perfil_nome_Titlecontrolidtoreplace = cgiGet( "DDO_PERFIL_NOME_Titlecontrolidtoreplace");
            Ddo_perfil_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Includesortasc"));
            Ddo_perfil_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Includesortdsc"));
            Ddo_perfil_nome_Sortedstatus = cgiGet( "DDO_PERFIL_NOME_Sortedstatus");
            Ddo_perfil_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Includefilter"));
            Ddo_perfil_nome_Filtertype = cgiGet( "DDO_PERFIL_NOME_Filtertype");
            Ddo_perfil_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Filterisrange"));
            Ddo_perfil_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Includedatalist"));
            Ddo_perfil_nome_Datalisttype = cgiGet( "DDO_PERFIL_NOME_Datalisttype");
            Ddo_perfil_nome_Datalistproc = cgiGet( "DDO_PERFIL_NOME_Datalistproc");
            Ddo_perfil_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PERFIL_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_perfil_nome_Sortasc = cgiGet( "DDO_PERFIL_NOME_Sortasc");
            Ddo_perfil_nome_Sortdsc = cgiGet( "DDO_PERFIL_NOME_Sortdsc");
            Ddo_perfil_nome_Loadingdata = cgiGet( "DDO_PERFIL_NOME_Loadingdata");
            Ddo_perfil_nome_Cleanfilter = cgiGet( "DDO_PERFIL_NOME_Cleanfilter");
            Ddo_perfil_nome_Noresultsfound = cgiGet( "DDO_PERFIL_NOME_Noresultsfound");
            Ddo_perfil_nome_Searchbuttontext = cgiGet( "DDO_PERFIL_NOME_Searchbuttontext");
            Ddo_perfil_tipo_Caption = cgiGet( "DDO_PERFIL_TIPO_Caption");
            Ddo_perfil_tipo_Tooltip = cgiGet( "DDO_PERFIL_TIPO_Tooltip");
            Ddo_perfil_tipo_Cls = cgiGet( "DDO_PERFIL_TIPO_Cls");
            Ddo_perfil_tipo_Selectedvalue_set = cgiGet( "DDO_PERFIL_TIPO_Selectedvalue_set");
            Ddo_perfil_tipo_Dropdownoptionstype = cgiGet( "DDO_PERFIL_TIPO_Dropdownoptionstype");
            Ddo_perfil_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_PERFIL_TIPO_Titlecontrolidtoreplace");
            Ddo_perfil_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_TIPO_Includesortasc"));
            Ddo_perfil_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_TIPO_Includesortdsc"));
            Ddo_perfil_tipo_Sortedstatus = cgiGet( "DDO_PERFIL_TIPO_Sortedstatus");
            Ddo_perfil_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_TIPO_Includefilter"));
            Ddo_perfil_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_TIPO_Includedatalist"));
            Ddo_perfil_tipo_Datalisttype = cgiGet( "DDO_PERFIL_TIPO_Datalisttype");
            Ddo_perfil_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_TIPO_Allowmultipleselection"));
            Ddo_perfil_tipo_Datalistfixedvalues = cgiGet( "DDO_PERFIL_TIPO_Datalistfixedvalues");
            Ddo_perfil_tipo_Sortasc = cgiGet( "DDO_PERFIL_TIPO_Sortasc");
            Ddo_perfil_tipo_Sortdsc = cgiGet( "DDO_PERFIL_TIPO_Sortdsc");
            Ddo_perfil_tipo_Cleanfilter = cgiGet( "DDO_PERFIL_TIPO_Cleanfilter");
            Ddo_perfil_tipo_Searchbuttontext = cgiGet( "DDO_PERFIL_TIPO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_usuario_nome_Activeeventkey = cgiGet( "DDO_USUARIO_NOME_Activeeventkey");
            Ddo_usuario_nome_Filteredtext_get = cgiGet( "DDO_USUARIO_NOME_Filteredtext_get");
            Ddo_usuario_nome_Selectedvalue_get = cgiGet( "DDO_USUARIO_NOME_Selectedvalue_get");
            Ddo_usuario_pessoanom_Activeeventkey = cgiGet( "DDO_USUARIO_PESSOANOM_Activeeventkey");
            Ddo_usuario_pessoanom_Filteredtext_get = cgiGet( "DDO_USUARIO_PESSOANOM_Filteredtext_get");
            Ddo_usuario_pessoanom_Selectedvalue_get = cgiGet( "DDO_USUARIO_PESSOANOM_Selectedvalue_get");
            Ddo_perfil_nome_Activeeventkey = cgiGet( "DDO_PERFIL_NOME_Activeeventkey");
            Ddo_perfil_nome_Filteredtext_get = cgiGet( "DDO_PERFIL_NOME_Filteredtext_get");
            Ddo_perfil_nome_Selectedvalue_get = cgiGet( "DDO_PERFIL_NOME_Selectedvalue_get");
            Ddo_perfil_tipo_Activeeventkey = cgiGet( "DDO_PERFIL_TIPO_Activeeventkey");
            Ddo_perfil_tipo_Selectedvalue_get = cgiGet( "DDO_PERFIL_TIPO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPERFIL_NOME1"), AV18Perfil_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM1"), AV19Usuario_PessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPERFIL_NOME2"), AV23Perfil_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM2"), AV24Usuario_PessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPERFIL_NOME3"), AV28Perfil_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM3"), AV29Usuario_PessoaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME"), AV38TFUsuario_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME_SEL"), AV39TFUsuario_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM"), AV42TFUsuario_PessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM_SEL"), AV43TFUsuario_PessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPERFIL_NOME"), AV46TFPerfil_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPERFIL_NOME_SEL"), AV47TFPerfil_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E268Z2 */
         E268Z2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E268Z2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "PERFIL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "PERFIL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersSelector3 = "PERFIL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfusuario_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_nome_Visible), 5, 0)));
         edtavTfusuario_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_nome_sel_Visible), 5, 0)));
         edtavTfusuario_pessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_pessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_pessoanom_Visible), 5, 0)));
         edtavTfusuario_pessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_pessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_pessoanom_sel_Visible), 5, 0)));
         edtavTfperfil_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfperfil_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfperfil_nome_Visible), 5, 0)));
         edtavTfperfil_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfperfil_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfperfil_nome_sel_Visible), 5, 0)));
         Ddo_usuario_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Usuario_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "TitleControlIdToReplace", Ddo_usuario_nome_Titlecontrolidtoreplace);
         AV40ddo_Usuario_NomeTitleControlIdToReplace = Ddo_usuario_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Usuario_NomeTitleControlIdToReplace", AV40ddo_Usuario_NomeTitleControlIdToReplace);
         edtavDdo_usuario_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuario_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_usuario_pessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_Usuario_PessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "TitleControlIdToReplace", Ddo_usuario_pessoanom_Titlecontrolidtoreplace);
         AV44ddo_Usuario_PessoaNomTitleControlIdToReplace = Ddo_usuario_pessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_Usuario_PessoaNomTitleControlIdToReplace", AV44ddo_Usuario_PessoaNomTitleControlIdToReplace);
         edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_perfil_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Perfil_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "TitleControlIdToReplace", Ddo_perfil_nome_Titlecontrolidtoreplace);
         AV48ddo_Perfil_NomeTitleControlIdToReplace = Ddo_perfil_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Perfil_NomeTitleControlIdToReplace", AV48ddo_Perfil_NomeTitleControlIdToReplace);
         edtavDdo_perfil_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_perfil_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_perfil_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_Perfil_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_tipo_Internalname, "TitleControlIdToReplace", Ddo_perfil_tipo_Titlecontrolidtoreplace);
         AV52ddo_Perfil_TipoTitleControlIdToReplace = Ddo_perfil_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_Perfil_TipoTitleControlIdToReplace", AV52ddo_Perfil_TipoTitleControlIdToReplace);
         edtavDdo_perfil_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_perfil_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_perfil_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Usuario x Perfil";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Usu�rio", 0);
         cmbavOrderedby.addItem("3", "Pessoa", 0);
         cmbavOrderedby.addItem("4", "Tipo do Perfil", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV53DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV53DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E278Z2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV37Usuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41Usuario_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45Perfil_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49Perfil_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtUsuario_Nome_Titleformat = 2;
         edtUsuario_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usu�rio", AV40ddo_Usuario_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Nome_Internalname, "Title", edtUsuario_Nome_Title);
         edtUsuario_PessoaNom_Titleformat = 2;
         edtUsuario_PessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Pessoa", AV44ddo_Usuario_PessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaNom_Internalname, "Title", edtUsuario_PessoaNom_Title);
         edtPerfil_Nome_Titleformat = 2;
         edtPerfil_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Perfil", AV48ddo_Perfil_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Nome_Internalname, "Title", edtPerfil_Nome_Title);
         cmbPerfil_Tipo_Titleformat = 2;
         cmbPerfil_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo do Perfil", AV52ddo_Perfil_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPerfil_Tipo_Internalname, "Title", cmbPerfil_Tipo.Title.Text);
         AV55GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55GridCurrentPage), 10, 0)));
         AV56GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37Usuario_NomeTitleFilterData", AV37Usuario_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV41Usuario_PessoaNomTitleFilterData", AV41Usuario_PessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45Perfil_NomeTitleFilterData", AV45Perfil_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49Perfil_TipoTitleFilterData", AV49Perfil_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
      }

      protected void E118Z2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV54PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV54PageToGo) ;
         }
      }

      protected void E128Z2( )
      {
         /* Ddo_usuario_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_usuario_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_usuario_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFUsuario_Nome = Ddo_usuario_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFUsuario_Nome", AV38TFUsuario_Nome);
            AV39TFUsuario_Nome_Sel = Ddo_usuario_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFUsuario_Nome_Sel", AV39TFUsuario_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E138Z2( )
      {
         /* Ddo_usuario_pessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuario_pessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_usuario_pessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_pessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_usuario_pessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_pessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV42TFUsuario_PessoaNom = Ddo_usuario_pessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFUsuario_PessoaNom", AV42TFUsuario_PessoaNom);
            AV43TFUsuario_PessoaNom_Sel = Ddo_usuario_pessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFUsuario_PessoaNom_Sel", AV43TFUsuario_PessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E148Z2( )
      {
         /* Ddo_perfil_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_perfil_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_perfil_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_perfil_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFPerfil_Nome = Ddo_perfil_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFPerfil_Nome", AV46TFPerfil_Nome);
            AV47TFPerfil_Nome_Sel = Ddo_perfil_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFPerfil_Nome_Sel", AV47TFPerfil_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E158Z2( )
      {
         /* Ddo_perfil_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_perfil_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_perfil_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_tipo_Internalname, "SortedStatus", Ddo_perfil_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_perfil_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_tipo_Internalname, "SortedStatus", Ddo_perfil_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFPerfil_Tipo_SelsJson = Ddo_perfil_tipo_Selectedvalue_get;
            AV51TFPerfil_Tipo_Sels.FromJSonString(StringUtil.StringReplace( AV50TFPerfil_Tipo_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51TFPerfil_Tipo_Sels", AV51TFPerfil_Tipo_Sels);
      }

      private void E288Z2( )
      {
         /* Grid_Load Routine */
         AV32Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV32Select);
         AV59Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 68;
         }
         sendrow_682( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_68_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(68, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E298Z2 */
         E298Z2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E298Z2( )
      {
         /* Enter Routine */
         AV7InOutUsuario_Codigo = A1Usuario_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutUsuario_Codigo), 6, 0)));
         AV8InOutPerfil_Codigo = A3Perfil_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutPerfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutPerfil_Codigo), 6, 0)));
         AV9InOutPerfil_Nome = A4Perfil_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutPerfil_Nome", AV9InOutPerfil_Nome);
         context.setWebReturnParms(new Object[] {(int)AV7InOutUsuario_Codigo,(int)AV8InOutPerfil_Codigo,(String)AV9InOutPerfil_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E168Z2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E218Z2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E178Z2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV26DynamicFiltersSelector3, AV28Perfil_Nome3, AV29Usuario_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV38TFUsuario_Nome, AV39TFUsuario_Nome_Sel, AV42TFUsuario_PessoaNom, AV43TFUsuario_PessoaNom_Sel, AV46TFPerfil_Nome, AV47TFPerfil_Nome_Sel, AV40ddo_Usuario_NomeTitleControlIdToReplace, AV44ddo_Usuario_PessoaNomTitleControlIdToReplace, AV48ddo_Perfil_NomeTitleControlIdToReplace, AV52ddo_Perfil_TipoTitleControlIdToReplace, AV51TFPerfil_Tipo_Sels, AV60Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E228Z2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E238Z2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV25DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E188Z2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV26DynamicFiltersSelector3, AV28Perfil_Nome3, AV29Usuario_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV38TFUsuario_Nome, AV39TFUsuario_Nome_Sel, AV42TFUsuario_PessoaNom, AV43TFUsuario_PessoaNom_Sel, AV46TFPerfil_Nome, AV47TFPerfil_Nome_Sel, AV40ddo_Usuario_NomeTitleControlIdToReplace, AV44ddo_Usuario_PessoaNomTitleControlIdToReplace, AV48ddo_Perfil_NomeTitleControlIdToReplace, AV52ddo_Perfil_TipoTitleControlIdToReplace, AV51TFPerfil_Tipo_Sels, AV60Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E248Z2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E198Z2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV26DynamicFiltersSelector3, AV28Perfil_Nome3, AV29Usuario_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV38TFUsuario_Nome, AV39TFUsuario_Nome_Sel, AV42TFUsuario_PessoaNom, AV43TFUsuario_PessoaNom_Sel, AV46TFPerfil_Nome, AV47TFPerfil_Nome_Sel, AV40ddo_Usuario_NomeTitleControlIdToReplace, AV44ddo_Usuario_PessoaNomTitleControlIdToReplace, AV48ddo_Perfil_NomeTitleControlIdToReplace, AV52ddo_Perfil_TipoTitleControlIdToReplace, AV51TFPerfil_Tipo_Sels, AV60Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E258Z2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E208Z2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51TFPerfil_Tipo_Sels", AV51TFPerfil_Tipo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_usuario_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
         Ddo_usuario_pessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
         Ddo_perfil_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
         Ddo_perfil_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_tipo_Internalname, "SortedStatus", Ddo_perfil_tipo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 2 )
         {
            Ddo_usuario_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_usuario_pessoanom_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
         }
         else if ( AV14OrderedBy == 1 )
         {
            Ddo_perfil_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
         }
         else if ( AV14OrderedBy == 4 )
         {
            Ddo_perfil_tipo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_tipo_Internalname, "SortedStatus", Ddo_perfil_tipo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavPerfil_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome1_Visible), 5, 0)));
         edtavUsuario_pessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "PERFIL_NOME") == 0 )
         {
            edtavPerfil_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 )
         {
            edtavUsuario_pessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavPerfil_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome2_Visible), 5, 0)));
         edtavUsuario_pessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "PERFIL_NOME") == 0 )
         {
            edtavPerfil_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 )
         {
            edtavUsuario_pessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavPerfil_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome3_Visible), 5, 0)));
         edtavUsuario_pessoanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "PERFIL_NOME") == 0 )
         {
            edtavPerfil_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 )
         {
            edtavUsuario_pessoanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom3_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "PERFIL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV23Perfil_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Perfil_Nome2", AV23Perfil_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         AV26DynamicFiltersSelector3 = "PERFIL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         AV28Perfil_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Perfil_Nome3", AV28Perfil_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV38TFUsuario_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFUsuario_Nome", AV38TFUsuario_Nome);
         Ddo_usuario_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "FilteredText_set", Ddo_usuario_nome_Filteredtext_set);
         AV39TFUsuario_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFUsuario_Nome_Sel", AV39TFUsuario_Nome_Sel);
         Ddo_usuario_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SelectedValue_set", Ddo_usuario_nome_Selectedvalue_set);
         AV42TFUsuario_PessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFUsuario_PessoaNom", AV42TFUsuario_PessoaNom);
         Ddo_usuario_pessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "FilteredText_set", Ddo_usuario_pessoanom_Filteredtext_set);
         AV43TFUsuario_PessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFUsuario_PessoaNom_Sel", AV43TFUsuario_PessoaNom_Sel);
         Ddo_usuario_pessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SelectedValue_set", Ddo_usuario_pessoanom_Selectedvalue_set);
         AV46TFPerfil_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFPerfil_Nome", AV46TFPerfil_Nome);
         Ddo_perfil_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "FilteredText_set", Ddo_perfil_nome_Filteredtext_set);
         AV47TFPerfil_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFPerfil_Nome_Sel", AV47TFPerfil_Nome_Sel);
         Ddo_perfil_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SelectedValue_set", Ddo_perfil_nome_Selectedvalue_set);
         AV51TFPerfil_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_perfil_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_tipo_Internalname, "SelectedValue_set", Ddo_perfil_tipo_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "PERFIL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV18Perfil_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Perfil_Nome1", AV18Perfil_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "PERFIL_NOME") == 0 )
            {
               AV18Perfil_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Perfil_Nome1", AV18Perfil_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 )
            {
               AV19Usuario_PessoaNom1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Usuario_PessoaNom1", AV19Usuario_PessoaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "PERFIL_NOME") == 0 )
               {
                  AV23Perfil_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Perfil_Nome2", AV23Perfil_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 )
               {
                  AV24Usuario_PessoaNom2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Usuario_PessoaNom2", AV24Usuario_PessoaNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV25DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV26DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "PERFIL_NOME") == 0 )
                  {
                     AV28Perfil_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Perfil_Nome3", AV28Perfil_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 )
                  {
                     AV29Usuario_PessoaNom3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Usuario_PessoaNom3", AV29Usuario_PessoaNom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV30DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFUsuario_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUSUARIO_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV38TFUsuario_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFUsuario_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUSUARIO_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV39TFUsuario_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFUsuario_PessoaNom)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUSUARIO_PESSOANOM";
            AV12GridStateFilterValue.gxTpr_Value = AV42TFUsuario_PessoaNom;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFUsuario_PessoaNom_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUSUARIO_PESSOANOM_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV43TFUsuario_PessoaNom_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFPerfil_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFPERFIL_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV46TFPerfil_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFPerfil_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFPERFIL_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV47TFPerfil_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV51TFPerfil_Tipo_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFPERFIL_TIPO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV51TFPerfil_Tipo_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV60Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV31DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "PERFIL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Perfil_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18Perfil_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Usuario_PessoaNom1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19Usuario_PessoaNom1;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "PERFIL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Perfil_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV23Perfil_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Usuario_PessoaNom2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV24Usuario_PessoaNom2;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV25DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV26DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "PERFIL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Perfil_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV28Perfil_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Usuario_PessoaNom3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV29Usuario_PessoaNom3;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_8Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_8Z2( true) ;
         }
         else
         {
            wb_table2_5_8Z2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_8Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_62_8Z2( true) ;
         }
         else
         {
            wb_table3_62_8Z2( false) ;
         }
         return  ;
      }

      protected void wb_table3_62_8Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_8Z2e( true) ;
         }
         else
         {
            wb_table1_2_8Z2e( false) ;
         }
      }

      protected void wb_table3_62_8Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_65_8Z2( true) ;
         }
         else
         {
            wb_table4_65_8Z2( false) ;
         }
         return  ;
      }

      protected void wb_table4_65_8Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_62_8Z2e( true) ;
         }
         else
         {
            wb_table3_62_8Z2e( false) ;
         }
      }

      protected void wb_table4_65_8Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"68\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�d. da Pessoa") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPerfil_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtPerfil_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPerfil_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbPerfil_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbPerfil_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbPerfil_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2Usuario_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A58Usuario_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A4Perfil_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPerfil_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPerfil_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A275Perfil_Tipo), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbPerfil_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbPerfil_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 68 )
         {
            wbEnd = 0;
            nRC_GXsfl_68 = (short)(nGXsfl_68_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_65_8Z2e( true) ;
         }
         else
         {
            wb_table4_65_8Z2e( false) ;
         }
      }

      protected void wb_table2_5_8Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptUsuarioPerfil.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_8Z2( true) ;
         }
         else
         {
            wb_table5_14_8Z2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_8Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_8Z2e( true) ;
         }
         else
         {
            wb_table2_5_8Z2e( false) ;
         }
      }

      protected void wb_table5_14_8Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_8Z2( true) ;
         }
         else
         {
            wb_table6_19_8Z2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_8Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_8Z2e( true) ;
         }
         else
         {
            wb_table5_14_8Z2e( false) ;
         }
      }

      protected void wb_table6_19_8Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptUsuarioPerfil.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPerfil_nome1_Internalname, StringUtil.RTrim( AV18Perfil_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18Perfil_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPerfil_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPerfil_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom1_Internalname, StringUtil.RTrim( AV19Usuario_PessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV19Usuario_PessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUsuarioPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_PromptUsuarioPerfil.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPerfil_nome2_Internalname, StringUtil.RTrim( AV23Perfil_Nome2), StringUtil.RTrim( context.localUtil.Format( AV23Perfil_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPerfil_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPerfil_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom2_Internalname, StringUtil.RTrim( AV24Usuario_PessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV24Usuario_PessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUsuarioPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV26DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_PromptUsuarioPerfil.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPerfil_nome3_Internalname, StringUtil.RTrim( AV28Perfil_Nome3), StringUtil.RTrim( context.localUtil.Format( AV28Perfil_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPerfil_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPerfil_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom3_Internalname, StringUtil.RTrim( AV29Usuario_PessoaNom3), StringUtil.RTrim( context.localUtil.Format( AV29Usuario_PessoaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoanom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_8Z2e( true) ;
         }
         else
         {
            wb_table6_19_8Z2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutUsuario_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutUsuario_Codigo), 6, 0)));
         AV8InOutPerfil_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutPerfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutPerfil_Codigo), 6, 0)));
         AV9InOutPerfil_Nome = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutPerfil_Nome", AV9InOutPerfil_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA8Z2( ) ;
         WS8Z2( ) ;
         WE8Z2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299353637");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptusuarioperfil.js", "?20205299353637");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_682( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_68_idx;
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO_"+sGXsfl_68_idx;
         edtPerfil_Codigo_Internalname = "PERFIL_CODIGO_"+sGXsfl_68_idx;
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD_"+sGXsfl_68_idx;
         edtUsuario_Nome_Internalname = "USUARIO_NOME_"+sGXsfl_68_idx;
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM_"+sGXsfl_68_idx;
         edtPerfil_Nome_Internalname = "PERFIL_NOME_"+sGXsfl_68_idx;
         cmbPerfil_Tipo_Internalname = "PERFIL_TIPO_"+sGXsfl_68_idx;
      }

      protected void SubsflControlProps_fel_682( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_68_fel_idx;
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO_"+sGXsfl_68_fel_idx;
         edtPerfil_Codigo_Internalname = "PERFIL_CODIGO_"+sGXsfl_68_fel_idx;
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD_"+sGXsfl_68_fel_idx;
         edtUsuario_Nome_Internalname = "USUARIO_NOME_"+sGXsfl_68_fel_idx;
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM_"+sGXsfl_68_fel_idx;
         edtPerfil_Nome_Internalname = "PERFIL_NOME_"+sGXsfl_68_fel_idx;
         cmbPerfil_Tipo_Internalname = "PERFIL_TIPO_"+sGXsfl_68_fel_idx;
      }

      protected void sendrow_682( )
      {
         SubsflControlProps_682( ) ;
         WB8Z0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_68_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_68_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_68_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 69,'',false,'',68)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV32Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV59Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Select)) ? AV59Select_GXI : context.PathToRelativeUrl( AV32Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_68_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV32Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPerfil_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPerfil_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Nome_Internalname,StringUtil.RTrim( A2Usuario_Nome),StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_PessoaNom_Internalname,StringUtil.RTrim( A58Usuario_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A58Usuario_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPerfil_Nome_Internalname,StringUtil.RTrim( A4Perfil_Nome),StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPerfil_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "PERFIL_TIPO_" + sGXsfl_68_idx;
            cmbPerfil_Tipo.Name = GXCCtl;
            cmbPerfil_Tipo.WebTags = "";
            cmbPerfil_Tipo.addItem("0", "Desconhecido", 0);
            cmbPerfil_Tipo.addItem("1", "Auditor", 0);
            cmbPerfil_Tipo.addItem("2", "Contador", 0);
            cmbPerfil_Tipo.addItem("3", "Contratada", 0);
            cmbPerfil_Tipo.addItem("4", "Contratante", 0);
            cmbPerfil_Tipo.addItem("5", "Financeiro", 0);
            cmbPerfil_Tipo.addItem("6", "Usuario", 0);
            cmbPerfil_Tipo.addItem("10", "Licensiado", 0);
            cmbPerfil_Tipo.addItem("99", "Administrador GAM", 0);
            if ( cmbPerfil_Tipo.ItemCount > 0 )
            {
               A275Perfil_Tipo = (short)(NumberUtil.Val( cmbPerfil_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0))), "."));
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbPerfil_Tipo,(String)cmbPerfil_Tipo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)),(short)1,(String)cmbPerfil_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbPerfil_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPerfil_Tipo_Internalname, "Values", (String)(cmbPerfil_Tipo.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CODIGO"+"_"+sGXsfl_68_idx, GetSecureSignedToken( sGXsfl_68_idx, context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_CODIGO"+"_"+sGXsfl_68_idx, GetSecureSignedToken( sGXsfl_68_idx, context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_68_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_68_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_68_idx+1));
            sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
            SubsflControlProps_682( ) ;
         }
         /* End function sendrow_682 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavPerfil_nome1_Internalname = "vPERFIL_NOME1";
         edtavUsuario_pessoanom1_Internalname = "vUSUARIO_PESSOANOM1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavPerfil_nome2_Internalname = "vPERFIL_NOME2";
         edtavUsuario_pessoanom2_Internalname = "vUSUARIO_PESSOANOM2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavPerfil_nome3_Internalname = "vPERFIL_NOME3";
         edtavUsuario_pessoanom3_Internalname = "vUSUARIO_PESSOANOM3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO";
         edtPerfil_Codigo_Internalname = "PERFIL_CODIGO";
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD";
         edtUsuario_Nome_Internalname = "USUARIO_NOME";
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM";
         edtPerfil_Nome_Internalname = "PERFIL_NOME";
         cmbPerfil_Tipo_Internalname = "PERFIL_TIPO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfusuario_nome_Internalname = "vTFUSUARIO_NOME";
         edtavTfusuario_nome_sel_Internalname = "vTFUSUARIO_NOME_SEL";
         edtavTfusuario_pessoanom_Internalname = "vTFUSUARIO_PESSOANOM";
         edtavTfusuario_pessoanom_sel_Internalname = "vTFUSUARIO_PESSOANOM_SEL";
         edtavTfperfil_nome_Internalname = "vTFPERFIL_NOME";
         edtavTfperfil_nome_sel_Internalname = "vTFPERFIL_NOME_SEL";
         Ddo_usuario_nome_Internalname = "DDO_USUARIO_NOME";
         edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname = "vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_usuario_pessoanom_Internalname = "DDO_USUARIO_PESSOANOM";
         edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname = "vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_perfil_nome_Internalname = "DDO_PERFIL_NOME";
         edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname = "vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE";
         Ddo_perfil_tipo_Internalname = "DDO_PERFIL_TIPO";
         edtavDdo_perfil_tipotitlecontrolidtoreplace_Internalname = "vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbPerfil_Tipo_Jsonclick = "";
         edtPerfil_Nome_Jsonclick = "";
         edtUsuario_PessoaNom_Jsonclick = "";
         edtUsuario_Nome_Jsonclick = "";
         edtUsuario_PessoaCod_Jsonclick = "";
         edtPerfil_Codigo_Jsonclick = "";
         edtUsuario_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavUsuario_pessoanom3_Jsonclick = "";
         edtavPerfil_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavUsuario_pessoanom2_Jsonclick = "";
         edtavPerfil_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavUsuario_pessoanom1_Jsonclick = "";
         edtavPerfil_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         cmbPerfil_Tipo_Titleformat = 0;
         edtPerfil_Nome_Titleformat = 0;
         edtUsuario_PessoaNom_Titleformat = 0;
         edtUsuario_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavUsuario_pessoanom3_Visible = 1;
         edtavPerfil_nome3_Visible = 1;
         edtavUsuario_pessoanom2_Visible = 1;
         edtavPerfil_nome2_Visible = 1;
         edtavUsuario_pessoanom1_Visible = 1;
         edtavPerfil_nome1_Visible = 1;
         cmbPerfil_Tipo.Title.Text = "Tipo do Perfil";
         edtPerfil_Nome_Title = "Perfil";
         edtUsuario_PessoaNom_Title = "Pessoa";
         edtUsuario_Nome_Title = "Usu�rio";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_perfil_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_perfil_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_usuario_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfperfil_nome_sel_Jsonclick = "";
         edtavTfperfil_nome_sel_Visible = 1;
         edtavTfperfil_nome_Jsonclick = "";
         edtavTfperfil_nome_Visible = 1;
         edtavTfusuario_pessoanom_sel_Jsonclick = "";
         edtavTfusuario_pessoanom_sel_Visible = 1;
         edtavTfusuario_pessoanom_Jsonclick = "";
         edtavTfusuario_pessoanom_Visible = 1;
         edtavTfusuario_nome_sel_Jsonclick = "";
         edtavTfusuario_nome_sel_Visible = 1;
         edtavTfusuario_nome_Jsonclick = "";
         edtavTfusuario_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_perfil_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_perfil_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_perfil_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_perfil_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_perfil_tipo_Datalistfixedvalues = "0:Desconhecido,1:Auditor,2:Contador,3:Contratada,4:Contratante,5:Financeiro,6:Usuario,10:Licensiado,99:Administrador GAM";
         Ddo_perfil_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_perfil_tipo_Datalisttype = "FixedValues";
         Ddo_perfil_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_perfil_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_perfil_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_perfil_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_perfil_tipo_Titlecontrolidtoreplace = "";
         Ddo_perfil_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_perfil_tipo_Cls = "ColumnSettings";
         Ddo_perfil_tipo_Tooltip = "Op��es";
         Ddo_perfil_tipo_Caption = "";
         Ddo_perfil_nome_Searchbuttontext = "Pesquisar";
         Ddo_perfil_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_perfil_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_perfil_nome_Loadingdata = "Carregando dados...";
         Ddo_perfil_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_perfil_nome_Sortasc = "Ordenar de A � Z";
         Ddo_perfil_nome_Datalistupdateminimumcharacters = 0;
         Ddo_perfil_nome_Datalistproc = "GetPromptUsuarioPerfilFilterData";
         Ddo_perfil_nome_Datalisttype = "Dynamic";
         Ddo_perfil_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_perfil_nome_Filtertype = "Character";
         Ddo_perfil_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Titlecontrolidtoreplace = "";
         Ddo_perfil_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_perfil_nome_Cls = "ColumnSettings";
         Ddo_perfil_nome_Tooltip = "Op��es";
         Ddo_perfil_nome_Caption = "";
         Ddo_usuario_pessoanom_Searchbuttontext = "Pesquisar";
         Ddo_usuario_pessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_usuario_pessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_usuario_pessoanom_Loadingdata = "Carregando dados...";
         Ddo_usuario_pessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_usuario_pessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_usuario_pessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_usuario_pessoanom_Datalistproc = "GetPromptUsuarioPerfilFilterData";
         Ddo_usuario_pessoanom_Datalisttype = "Dynamic";
         Ddo_usuario_pessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_usuario_pessoanom_Filtertype = "Character";
         Ddo_usuario_pessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Titlecontrolidtoreplace = "";
         Ddo_usuario_pessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuario_pessoanom_Cls = "ColumnSettings";
         Ddo_usuario_pessoanom_Tooltip = "Op��es";
         Ddo_usuario_pessoanom_Caption = "";
         Ddo_usuario_nome_Searchbuttontext = "Pesquisar";
         Ddo_usuario_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_usuario_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_usuario_nome_Loadingdata = "Carregando dados...";
         Ddo_usuario_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_usuario_nome_Sortasc = "Ordenar de A � Z";
         Ddo_usuario_nome_Datalistupdateminimumcharacters = 0;
         Ddo_usuario_nome_Datalistproc = "GetPromptUsuarioPerfilFilterData";
         Ddo_usuario_nome_Datalisttype = "Dynamic";
         Ddo_usuario_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_usuario_nome_Filtertype = "Character";
         Ddo_usuario_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Titlecontrolidtoreplace = "";
         Ddo_usuario_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuario_nome_Cls = "ColumnSettings";
         Ddo_usuario_nome_Tooltip = "Op��es";
         Ddo_usuario_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Usuario x Perfil";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV40ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV38TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV39TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV42TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV43TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV46TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV47TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''}],oparms:[{av:'AV37Usuario_NomeTitleFilterData',fld:'vUSUARIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV41Usuario_PessoaNomTitleFilterData',fld:'vUSUARIO_PESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV45Perfil_NomeTitleFilterData',fld:'vPERFIL_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV49Perfil_TipoTitleFilterData',fld:'vPERFIL_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'edtUsuario_PessoaNom_Titleformat',ctrl:'USUARIO_PESSOANOM',prop:'Titleformat'},{av:'edtUsuario_PessoaNom_Title',ctrl:'USUARIO_PESSOANOM',prop:'Title'},{av:'edtPerfil_Nome_Titleformat',ctrl:'PERFIL_NOME',prop:'Titleformat'},{av:'edtPerfil_Nome_Title',ctrl:'PERFIL_NOME',prop:'Title'},{av:'cmbPerfil_Tipo'},{av:'AV55GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV56GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E118Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV39TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV42TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV43TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV46TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV47TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV40ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_USUARIO_NOME.ONOPTIONCLICKED","{handler:'E128Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV39TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV42TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV43TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV46TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV47TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV40ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_usuario_nome_Activeeventkey',ctrl:'DDO_USUARIO_NOME',prop:'ActiveEventKey'},{av:'Ddo_usuario_nome_Filteredtext_get',ctrl:'DDO_USUARIO_NOME',prop:'FilteredText_get'},{av:'Ddo_usuario_nome_Selectedvalue_get',ctrl:'DDO_USUARIO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'AV38TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV39TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'},{av:'Ddo_perfil_tipo_Sortedstatus',ctrl:'DDO_PERFIL_TIPO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_USUARIO_PESSOANOM.ONOPTIONCLICKED","{handler:'E138Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV39TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV42TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV43TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV46TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV47TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV40ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_usuario_pessoanom_Activeeventkey',ctrl:'DDO_USUARIO_PESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_usuario_pessoanom_Filteredtext_get',ctrl:'DDO_USUARIO_PESSOANOM',prop:'FilteredText_get'},{av:'Ddo_usuario_pessoanom_Selectedvalue_get',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'AV42TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV43TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'},{av:'Ddo_perfil_tipo_Sortedstatus',ctrl:'DDO_PERFIL_TIPO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PERFIL_NOME.ONOPTIONCLICKED","{handler:'E148Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV39TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV42TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV43TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV46TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV47TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV40ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_perfil_nome_Activeeventkey',ctrl:'DDO_PERFIL_NOME',prop:'ActiveEventKey'},{av:'Ddo_perfil_nome_Filteredtext_get',ctrl:'DDO_PERFIL_NOME',prop:'FilteredText_get'},{av:'Ddo_perfil_nome_Selectedvalue_get',ctrl:'DDO_PERFIL_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'},{av:'AV46TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV47TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_perfil_tipo_Sortedstatus',ctrl:'DDO_PERFIL_TIPO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PERFIL_TIPO.ONOPTIONCLICKED","{handler:'E158Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV39TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV42TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV43TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV46TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV47TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV40ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_perfil_tipo_Activeeventkey',ctrl:'DDO_PERFIL_TIPO',prop:'ActiveEventKey'},{av:'Ddo_perfil_tipo_Selectedvalue_get',ctrl:'DDO_PERFIL_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_perfil_tipo_Sortedstatus',ctrl:'DDO_PERFIL_TIPO',prop:'SortedStatus'},{av:'AV51TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E288Z2',iparms:[],oparms:[{av:'AV32Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E298Z2',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A4Perfil_Nome',fld:'PERFIL_NOME',pic:'@!',nv:''}],oparms:[{av:'AV7InOutUsuario_Codigo',fld:'vINOUTUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutPerfil_Codigo',fld:'vINOUTPERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9InOutPerfil_Nome',fld:'vINOUTPERFIL_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E168Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV39TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV42TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV43TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV46TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV47TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV40ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E218Z2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E178Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV39TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV42TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV43TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV46TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV47TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV40ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'edtavPerfil_nome2_Visible',ctrl:'vPERFIL_NOME2',prop:'Visible'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavPerfil_nome3_Visible',ctrl:'vPERFIL_NOME3',prop:'Visible'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'},{av:'edtavPerfil_nome1_Visible',ctrl:'vPERFIL_NOME1',prop:'Visible'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E228Z2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavPerfil_nome1_Visible',ctrl:'vPERFIL_NOME1',prop:'Visible'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E238Z2',iparms:[],oparms:[{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E188Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV39TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV42TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV43TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV46TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV47TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV40ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'edtavPerfil_nome2_Visible',ctrl:'vPERFIL_NOME2',prop:'Visible'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavPerfil_nome3_Visible',ctrl:'vPERFIL_NOME3',prop:'Visible'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'},{av:'edtavPerfil_nome1_Visible',ctrl:'vPERFIL_NOME1',prop:'Visible'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E248Z2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavPerfil_nome2_Visible',ctrl:'vPERFIL_NOME2',prop:'Visible'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E198Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV39TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV42TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV43TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV46TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV47TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV40ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'edtavPerfil_nome2_Visible',ctrl:'vPERFIL_NOME2',prop:'Visible'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavPerfil_nome3_Visible',ctrl:'vPERFIL_NOME3',prop:'Visible'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'},{av:'edtavPerfil_nome1_Visible',ctrl:'vPERFIL_NOME1',prop:'Visible'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E258Z2',iparms:[{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavPerfil_nome3_Visible',ctrl:'vPERFIL_NOME3',prop:'Visible'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E208Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV39TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV42TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV43TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV46TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV47TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV40ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV38TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Filteredtext_set',ctrl:'DDO_USUARIO_NOME',prop:'FilteredText_set'},{av:'AV39TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Selectedvalue_set',ctrl:'DDO_USUARIO_NOME',prop:'SelectedValue_set'},{av:'AV42TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_usuario_pessoanom_Filteredtext_set',ctrl:'DDO_USUARIO_PESSOANOM',prop:'FilteredText_set'},{av:'AV43TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_pessoanom_Selectedvalue_set',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SelectedValue_set'},{av:'AV46TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'Ddo_perfil_nome_Filteredtext_set',ctrl:'DDO_PERFIL_NOME',prop:'FilteredText_set'},{av:'AV47TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_perfil_nome_Selectedvalue_set',ctrl:'DDO_PERFIL_NOME',prop:'SelectedValue_set'},{av:'AV51TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'Ddo_perfil_tipo_Selectedvalue_set',ctrl:'DDO_PERFIL_TIPO',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavPerfil_nome1_Visible',ctrl:'vPERFIL_NOME1',prop:'Visible'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28Perfil_Nome3',fld:'vPERFIL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV29Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'edtavPerfil_nome2_Visible',ctrl:'vPERFIL_NOME2',prop:'Visible'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavPerfil_nome3_Visible',ctrl:'vPERFIL_NOME3',prop:'Visible'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV9InOutPerfil_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_usuario_nome_Activeeventkey = "";
         Ddo_usuario_nome_Filteredtext_get = "";
         Ddo_usuario_nome_Selectedvalue_get = "";
         Ddo_usuario_pessoanom_Activeeventkey = "";
         Ddo_usuario_pessoanom_Filteredtext_get = "";
         Ddo_usuario_pessoanom_Selectedvalue_get = "";
         Ddo_perfil_nome_Activeeventkey = "";
         Ddo_perfil_nome_Filteredtext_get = "";
         Ddo_perfil_nome_Selectedvalue_get = "";
         Ddo_perfil_tipo_Activeeventkey = "";
         Ddo_perfil_tipo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV18Perfil_Nome1 = "";
         AV19Usuario_PessoaNom1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23Perfil_Nome2 = "";
         AV24Usuario_PessoaNom2 = "";
         AV26DynamicFiltersSelector3 = "";
         AV28Perfil_Nome3 = "";
         AV29Usuario_PessoaNom3 = "";
         AV38TFUsuario_Nome = "";
         AV39TFUsuario_Nome_Sel = "";
         AV42TFUsuario_PessoaNom = "";
         AV43TFUsuario_PessoaNom_Sel = "";
         AV46TFPerfil_Nome = "";
         AV47TFPerfil_Nome_Sel = "";
         AV40ddo_Usuario_NomeTitleControlIdToReplace = "";
         AV44ddo_Usuario_PessoaNomTitleControlIdToReplace = "";
         AV48ddo_Perfil_NomeTitleControlIdToReplace = "";
         AV52ddo_Perfil_TipoTitleControlIdToReplace = "";
         AV51TFPerfil_Tipo_Sels = new GxSimpleCollection();
         AV60Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV53DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV37Usuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41Usuario_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45Perfil_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49Perfil_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_usuario_nome_Filteredtext_set = "";
         Ddo_usuario_nome_Selectedvalue_set = "";
         Ddo_usuario_nome_Sortedstatus = "";
         Ddo_usuario_pessoanom_Filteredtext_set = "";
         Ddo_usuario_pessoanom_Selectedvalue_set = "";
         Ddo_usuario_pessoanom_Sortedstatus = "";
         Ddo_perfil_nome_Filteredtext_set = "";
         Ddo_perfil_nome_Selectedvalue_set = "";
         Ddo_perfil_nome_Sortedstatus = "";
         Ddo_perfil_tipo_Selectedvalue_set = "";
         Ddo_perfil_tipo_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV32Select = "";
         AV59Select_GXI = "";
         A2Usuario_Nome = "";
         A58Usuario_PessoaNom = "";
         A4Perfil_Nome = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18Perfil_Nome1 = "";
         lV19Usuario_PessoaNom1 = "";
         lV23Perfil_Nome2 = "";
         lV24Usuario_PessoaNom2 = "";
         lV28Perfil_Nome3 = "";
         lV29Usuario_PessoaNom3 = "";
         lV38TFUsuario_Nome = "";
         lV42TFUsuario_PessoaNom = "";
         lV46TFPerfil_Nome = "";
         H008Z2_A275Perfil_Tipo = new short[1] ;
         H008Z2_A4Perfil_Nome = new String[] {""} ;
         H008Z2_A58Usuario_PessoaNom = new String[] {""} ;
         H008Z2_n58Usuario_PessoaNom = new bool[] {false} ;
         H008Z2_A2Usuario_Nome = new String[] {""} ;
         H008Z2_n2Usuario_Nome = new bool[] {false} ;
         H008Z2_A57Usuario_PessoaCod = new int[1] ;
         H008Z2_A3Perfil_Codigo = new int[1] ;
         H008Z2_A1Usuario_Codigo = new int[1] ;
         H008Z3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV50TFPerfil_Tipo_SelsJson = "";
         GridRow = new GXWebRow();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptusuarioperfil__default(),
            new Object[][] {
                new Object[] {
               H008Z2_A275Perfil_Tipo, H008Z2_A4Perfil_Nome, H008Z2_A58Usuario_PessoaNom, H008Z2_n58Usuario_PessoaNom, H008Z2_A2Usuario_Nome, H008Z2_n2Usuario_Nome, H008Z2_A57Usuario_PessoaCod, H008Z2_A3Perfil_Codigo, H008Z2_A1Usuario_Codigo
               }
               , new Object[] {
               H008Z3_AGRID_nRecordCount
               }
            }
         );
         AV60Pgmname = "PromptUsuarioPerfil";
         /* GeneXus formulas. */
         AV60Pgmname = "PromptUsuarioPerfil";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_68 ;
      private short nGXsfl_68_idx=1 ;
      private short AV14OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A275Perfil_Tipo ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_68_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtUsuario_Nome_Titleformat ;
      private short edtUsuario_PessoaNom_Titleformat ;
      private short edtPerfil_Nome_Titleformat ;
      private short cmbPerfil_Tipo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutUsuario_Codigo ;
      private int AV8InOutPerfil_Codigo ;
      private int wcpOAV7InOutUsuario_Codigo ;
      private int wcpOAV8InOutPerfil_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_usuario_nome_Datalistupdateminimumcharacters ;
      private int Ddo_usuario_pessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_perfil_nome_Datalistupdateminimumcharacters ;
      private int edtavTfusuario_nome_Visible ;
      private int edtavTfusuario_nome_sel_Visible ;
      private int edtavTfusuario_pessoanom_Visible ;
      private int edtavTfusuario_pessoanom_sel_Visible ;
      private int edtavTfperfil_nome_Visible ;
      private int edtavTfperfil_nome_sel_Visible ;
      private int edtavDdo_usuario_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_perfil_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_perfil_tipotitlecontrolidtoreplace_Visible ;
      private int A1Usuario_Codigo ;
      private int A3Perfil_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV51TFPerfil_Tipo_Sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int AV54PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavPerfil_nome1_Visible ;
      private int edtavUsuario_pessoanom1_Visible ;
      private int edtavPerfil_nome2_Visible ;
      private int edtavUsuario_pessoanom2_Visible ;
      private int edtavPerfil_nome3_Visible ;
      private int edtavUsuario_pessoanom3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV55GridCurrentPage ;
      private long AV56GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV9InOutPerfil_Nome ;
      private String wcpOAV9InOutPerfil_Nome ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_usuario_nome_Activeeventkey ;
      private String Ddo_usuario_nome_Filteredtext_get ;
      private String Ddo_usuario_nome_Selectedvalue_get ;
      private String Ddo_usuario_pessoanom_Activeeventkey ;
      private String Ddo_usuario_pessoanom_Filteredtext_get ;
      private String Ddo_usuario_pessoanom_Selectedvalue_get ;
      private String Ddo_perfil_nome_Activeeventkey ;
      private String Ddo_perfil_nome_Filteredtext_get ;
      private String Ddo_perfil_nome_Selectedvalue_get ;
      private String Ddo_perfil_tipo_Activeeventkey ;
      private String Ddo_perfil_tipo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_68_idx="0001" ;
      private String AV18Perfil_Nome1 ;
      private String AV19Usuario_PessoaNom1 ;
      private String AV23Perfil_Nome2 ;
      private String AV24Usuario_PessoaNom2 ;
      private String AV28Perfil_Nome3 ;
      private String AV29Usuario_PessoaNom3 ;
      private String AV38TFUsuario_Nome ;
      private String AV39TFUsuario_Nome_Sel ;
      private String AV42TFUsuario_PessoaNom ;
      private String AV43TFUsuario_PessoaNom_Sel ;
      private String AV46TFPerfil_Nome ;
      private String AV47TFPerfil_Nome_Sel ;
      private String AV60Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_usuario_nome_Caption ;
      private String Ddo_usuario_nome_Tooltip ;
      private String Ddo_usuario_nome_Cls ;
      private String Ddo_usuario_nome_Filteredtext_set ;
      private String Ddo_usuario_nome_Selectedvalue_set ;
      private String Ddo_usuario_nome_Dropdownoptionstype ;
      private String Ddo_usuario_nome_Titlecontrolidtoreplace ;
      private String Ddo_usuario_nome_Sortedstatus ;
      private String Ddo_usuario_nome_Filtertype ;
      private String Ddo_usuario_nome_Datalisttype ;
      private String Ddo_usuario_nome_Datalistproc ;
      private String Ddo_usuario_nome_Sortasc ;
      private String Ddo_usuario_nome_Sortdsc ;
      private String Ddo_usuario_nome_Loadingdata ;
      private String Ddo_usuario_nome_Cleanfilter ;
      private String Ddo_usuario_nome_Noresultsfound ;
      private String Ddo_usuario_nome_Searchbuttontext ;
      private String Ddo_usuario_pessoanom_Caption ;
      private String Ddo_usuario_pessoanom_Tooltip ;
      private String Ddo_usuario_pessoanom_Cls ;
      private String Ddo_usuario_pessoanom_Filteredtext_set ;
      private String Ddo_usuario_pessoanom_Selectedvalue_set ;
      private String Ddo_usuario_pessoanom_Dropdownoptionstype ;
      private String Ddo_usuario_pessoanom_Titlecontrolidtoreplace ;
      private String Ddo_usuario_pessoanom_Sortedstatus ;
      private String Ddo_usuario_pessoanom_Filtertype ;
      private String Ddo_usuario_pessoanom_Datalisttype ;
      private String Ddo_usuario_pessoanom_Datalistproc ;
      private String Ddo_usuario_pessoanom_Sortasc ;
      private String Ddo_usuario_pessoanom_Sortdsc ;
      private String Ddo_usuario_pessoanom_Loadingdata ;
      private String Ddo_usuario_pessoanom_Cleanfilter ;
      private String Ddo_usuario_pessoanom_Noresultsfound ;
      private String Ddo_usuario_pessoanom_Searchbuttontext ;
      private String Ddo_perfil_nome_Caption ;
      private String Ddo_perfil_nome_Tooltip ;
      private String Ddo_perfil_nome_Cls ;
      private String Ddo_perfil_nome_Filteredtext_set ;
      private String Ddo_perfil_nome_Selectedvalue_set ;
      private String Ddo_perfil_nome_Dropdownoptionstype ;
      private String Ddo_perfil_nome_Titlecontrolidtoreplace ;
      private String Ddo_perfil_nome_Sortedstatus ;
      private String Ddo_perfil_nome_Filtertype ;
      private String Ddo_perfil_nome_Datalisttype ;
      private String Ddo_perfil_nome_Datalistproc ;
      private String Ddo_perfil_nome_Sortasc ;
      private String Ddo_perfil_nome_Sortdsc ;
      private String Ddo_perfil_nome_Loadingdata ;
      private String Ddo_perfil_nome_Cleanfilter ;
      private String Ddo_perfil_nome_Noresultsfound ;
      private String Ddo_perfil_nome_Searchbuttontext ;
      private String Ddo_perfil_tipo_Caption ;
      private String Ddo_perfil_tipo_Tooltip ;
      private String Ddo_perfil_tipo_Cls ;
      private String Ddo_perfil_tipo_Selectedvalue_set ;
      private String Ddo_perfil_tipo_Dropdownoptionstype ;
      private String Ddo_perfil_tipo_Titlecontrolidtoreplace ;
      private String Ddo_perfil_tipo_Sortedstatus ;
      private String Ddo_perfil_tipo_Datalisttype ;
      private String Ddo_perfil_tipo_Datalistfixedvalues ;
      private String Ddo_perfil_tipo_Sortasc ;
      private String Ddo_perfil_tipo_Sortdsc ;
      private String Ddo_perfil_tipo_Cleanfilter ;
      private String Ddo_perfil_tipo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfusuario_nome_Internalname ;
      private String edtavTfusuario_nome_Jsonclick ;
      private String edtavTfusuario_nome_sel_Internalname ;
      private String edtavTfusuario_nome_sel_Jsonclick ;
      private String edtavTfusuario_pessoanom_Internalname ;
      private String edtavTfusuario_pessoanom_Jsonclick ;
      private String edtavTfusuario_pessoanom_sel_Internalname ;
      private String edtavTfusuario_pessoanom_sel_Jsonclick ;
      private String edtavTfperfil_nome_Internalname ;
      private String edtavTfperfil_nome_Jsonclick ;
      private String edtavTfperfil_nome_sel_Internalname ;
      private String edtavTfperfil_nome_sel_Jsonclick ;
      private String edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_perfil_tipotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtUsuario_Codigo_Internalname ;
      private String edtPerfil_Codigo_Internalname ;
      private String edtUsuario_PessoaCod_Internalname ;
      private String A2Usuario_Nome ;
      private String edtUsuario_Nome_Internalname ;
      private String A58Usuario_PessoaNom ;
      private String edtUsuario_PessoaNom_Internalname ;
      private String A4Perfil_Nome ;
      private String edtPerfil_Nome_Internalname ;
      private String cmbPerfil_Tipo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV18Perfil_Nome1 ;
      private String lV19Usuario_PessoaNom1 ;
      private String lV23Perfil_Nome2 ;
      private String lV24Usuario_PessoaNom2 ;
      private String lV28Perfil_Nome3 ;
      private String lV29Usuario_PessoaNom3 ;
      private String lV38TFUsuario_Nome ;
      private String lV42TFUsuario_PessoaNom ;
      private String lV46TFPerfil_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavPerfil_nome1_Internalname ;
      private String edtavUsuario_pessoanom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavPerfil_nome2_Internalname ;
      private String edtavUsuario_pessoanom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavPerfil_nome3_Internalname ;
      private String edtavUsuario_pessoanom3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_usuario_nome_Internalname ;
      private String Ddo_usuario_pessoanom_Internalname ;
      private String Ddo_perfil_nome_Internalname ;
      private String Ddo_perfil_tipo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtUsuario_Nome_Title ;
      private String edtUsuario_PessoaNom_Title ;
      private String edtPerfil_Nome_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavPerfil_nome1_Jsonclick ;
      private String edtavUsuario_pessoanom1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavPerfil_nome2_Jsonclick ;
      private String edtavUsuario_pessoanom2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavPerfil_nome3_Jsonclick ;
      private String edtavUsuario_pessoanom3_Jsonclick ;
      private String sGXsfl_68_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtUsuario_Codigo_Jsonclick ;
      private String edtPerfil_Codigo_Jsonclick ;
      private String edtUsuario_PessoaCod_Jsonclick ;
      private String edtUsuario_Nome_Jsonclick ;
      private String edtUsuario_PessoaNom_Jsonclick ;
      private String edtPerfil_Nome_Jsonclick ;
      private String cmbPerfil_Tipo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV25DynamicFiltersEnabled3 ;
      private bool AV31DynamicFiltersIgnoreFirst ;
      private bool AV30DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_usuario_nome_Includesortasc ;
      private bool Ddo_usuario_nome_Includesortdsc ;
      private bool Ddo_usuario_nome_Includefilter ;
      private bool Ddo_usuario_nome_Filterisrange ;
      private bool Ddo_usuario_nome_Includedatalist ;
      private bool Ddo_usuario_pessoanom_Includesortasc ;
      private bool Ddo_usuario_pessoanom_Includesortdsc ;
      private bool Ddo_usuario_pessoanom_Includefilter ;
      private bool Ddo_usuario_pessoanom_Filterisrange ;
      private bool Ddo_usuario_pessoanom_Includedatalist ;
      private bool Ddo_perfil_nome_Includesortasc ;
      private bool Ddo_perfil_nome_Includesortdsc ;
      private bool Ddo_perfil_nome_Includefilter ;
      private bool Ddo_perfil_nome_Filterisrange ;
      private bool Ddo_perfil_nome_Includedatalist ;
      private bool Ddo_perfil_tipo_Includesortasc ;
      private bool Ddo_perfil_tipo_Includesortdsc ;
      private bool Ddo_perfil_tipo_Includefilter ;
      private bool Ddo_perfil_tipo_Includedatalist ;
      private bool Ddo_perfil_tipo_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2Usuario_Nome ;
      private bool n58Usuario_PessoaNom ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV32Select_IsBlob ;
      private String AV50TFPerfil_Tipo_SelsJson ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV26DynamicFiltersSelector3 ;
      private String AV40ddo_Usuario_NomeTitleControlIdToReplace ;
      private String AV44ddo_Usuario_PessoaNomTitleControlIdToReplace ;
      private String AV48ddo_Perfil_NomeTitleControlIdToReplace ;
      private String AV52ddo_Perfil_TipoTitleControlIdToReplace ;
      private String AV59Select_GXI ;
      private String AV32Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutUsuario_Codigo ;
      private int aP1_InOutPerfil_Codigo ;
      private String aP2_InOutPerfil_Nome ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbPerfil_Tipo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private short[] H008Z2_A275Perfil_Tipo ;
      private String[] H008Z2_A4Perfil_Nome ;
      private String[] H008Z2_A58Usuario_PessoaNom ;
      private bool[] H008Z2_n58Usuario_PessoaNom ;
      private String[] H008Z2_A2Usuario_Nome ;
      private bool[] H008Z2_n2Usuario_Nome ;
      private int[] H008Z2_A57Usuario_PessoaCod ;
      private int[] H008Z2_A3Perfil_Codigo ;
      private int[] H008Z2_A1Usuario_Codigo ;
      private long[] H008Z3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV51TFPerfil_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37Usuario_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV41Usuario_PessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45Perfil_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49Perfil_TipoTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV53DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptusuarioperfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H008Z2( IGxContext context ,
                                             short A275Perfil_Tipo ,
                                             IGxCollection AV51TFPerfil_Tipo_Sels ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV18Perfil_Nome1 ,
                                             String AV19Usuario_PessoaNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             String AV23Perfil_Nome2 ,
                                             String AV24Usuario_PessoaNom2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             String AV28Perfil_Nome3 ,
                                             String AV29Usuario_PessoaNom3 ,
                                             String AV39TFUsuario_Nome_Sel ,
                                             String AV38TFUsuario_Nome ,
                                             String AV43TFUsuario_PessoaNom_Sel ,
                                             String AV42TFUsuario_PessoaNom ,
                                             String AV47TFPerfil_Nome_Sel ,
                                             String AV46TFPerfil_Nome ,
                                             int AV51TFPerfil_Tipo_Sels_Count ,
                                             String A4Perfil_Nome ,
                                             String A58Usuario_PessoaNom ,
                                             String A2Usuario_Nome ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [17] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Perfil_Tipo], T2.[Perfil_Nome], T4.[Pessoa_Nome] AS Usuario_PessoaNom, T3.[Usuario_Nome], T3.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Perfil_Codigo], T1.[Usuario_Codigo]";
         sFromString = " FROM ((([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Usuario_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Perfil_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Perfil_Nome] like '%' + @lV18Perfil_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Perfil_Nome] like '%' + @lV18Perfil_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Usuario_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV19Usuario_PessoaNom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV19Usuario_PessoaNom1 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Perfil_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Perfil_Nome] like '%' + @lV23Perfil_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Perfil_Nome] like '%' + @lV23Perfil_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Usuario_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV24Usuario_PessoaNom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV24Usuario_PessoaNom2 + '%')";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Perfil_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Perfil_Nome] like '%' + @lV28Perfil_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Perfil_Nome] like '%' + @lV28Perfil_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Usuario_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV29Usuario_PessoaNom3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV29Usuario_PessoaNom3 + '%')";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV39TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFUsuario_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_Nome] like @lV38TFUsuario_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_Nome] like @lV38TFUsuario_Nome)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFUsuario_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_Nome] = @AV39TFUsuario_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_Nome] = @AV39TFUsuario_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV43TFUsuario_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFUsuario_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV42TFUsuario_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV42TFUsuario_PessoaNom)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFUsuario_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV43TFUsuario_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV43TFUsuario_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV47TFPerfil_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFPerfil_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Perfil_Nome] like @lV46TFPerfil_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Perfil_Nome] like @lV46TFPerfil_Nome)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFPerfil_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Perfil_Nome] = @AV47TFPerfil_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Perfil_Nome] = @AV47TFPerfil_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV51TFPerfil_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV51TFPerfil_Tipo_Sels, "T2.[Perfil_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV51TFPerfil_Tipo_Sels, "T2.[Perfil_Tipo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Perfil_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Perfil_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Usuario_Nome]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Usuario_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Perfil_Tipo]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Perfil_Tipo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_Codigo], T1.[Perfil_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H008Z3( IGxContext context ,
                                             short A275Perfil_Tipo ,
                                             IGxCollection AV51TFPerfil_Tipo_Sels ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV18Perfil_Nome1 ,
                                             String AV19Usuario_PessoaNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             String AV23Perfil_Nome2 ,
                                             String AV24Usuario_PessoaNom2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             String AV28Perfil_Nome3 ,
                                             String AV29Usuario_PessoaNom3 ,
                                             String AV39TFUsuario_Nome_Sel ,
                                             String AV38TFUsuario_Nome ,
                                             String AV43TFUsuario_PessoaNom_Sel ,
                                             String AV42TFUsuario_PessoaNom ,
                                             String AV47TFPerfil_Nome_Sel ,
                                             String AV46TFPerfil_Nome ,
                                             int AV51TFPerfil_Tipo_Sels_Count ,
                                             String A4Perfil_Nome ,
                                             String A58Usuario_PessoaNom ,
                                             String A2Usuario_Nome ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [12] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T4 WITH (NOLOCK) ON T4.[Perfil_Codigo] = T1.[Perfil_Codigo]) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Perfil_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV18Perfil_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV18Perfil_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Usuario_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV19Usuario_PessoaNom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV19Usuario_PessoaNom1 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Perfil_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV23Perfil_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV23Perfil_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Usuario_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV24Usuario_PessoaNom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV24Usuario_PessoaNom2 + '%')";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Perfil_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV28Perfil_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV28Perfil_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Usuario_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV29Usuario_PessoaNom3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV29Usuario_PessoaNom3 + '%')";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV39TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFUsuario_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV38TFUsuario_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV38TFUsuario_Nome)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFUsuario_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] = @AV39TFUsuario_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] = @AV39TFUsuario_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV43TFUsuario_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFUsuario_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV42TFUsuario_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV42TFUsuario_PessoaNom)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFUsuario_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV43TFUsuario_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV43TFUsuario_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV47TFPerfil_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFPerfil_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like @lV46TFPerfil_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like @lV46TFPerfil_Nome)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFPerfil_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] = @AV47TFPerfil_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] = @AV47TFPerfil_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV51TFPerfil_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV51TFPerfil_Tipo_Sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV51TFPerfil_Tipo_Sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H008Z2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
               case 1 :
                     return conditional_H008Z3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH008Z2 ;
          prmH008Z2 = new Object[] {
          new Object[] {"@lV18Perfil_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19Usuario_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23Perfil_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24Usuario_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV28Perfil_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV29Usuario_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV38TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV39TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42TFUsuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV43TFUsuario_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV46TFPerfil_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV47TFPerfil_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH008Z3 ;
          prmH008Z3 = new Object[] {
          new Object[] {"@lV18Perfil_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19Usuario_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23Perfil_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24Usuario_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV28Perfil_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV29Usuario_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV38TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV39TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42TFUsuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV43TFUsuario_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV46TFPerfil_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV47TFPerfil_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H008Z2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008Z2,11,0,true,false )
             ,new CursorDef("H008Z3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008Z3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

}
