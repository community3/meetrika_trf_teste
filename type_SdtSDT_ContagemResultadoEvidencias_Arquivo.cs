/*
               File: type_SdtSDT_ContagemResultadoEvidencias_Arquivo
        Description: SDT_ContagemResultadoEvidencias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:59.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_ContagemResultadoEvidencias.Arquivo" )]
   [XmlType(TypeName =  "SDT_ContagemResultadoEvidencias.Arquivo" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_ContagemResultadoEvidencias_Arquivo : GxUserType
   {
      public SdtSDT_ContagemResultadoEvidencias_Arquivo( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_arquivo = "";
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_nomearq = "";
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_tipoarq = "";
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_descricao = "";
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_nome = "";
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_link = "";
      }

      public SdtSDT_ContagemResultadoEvidencias_Arquivo( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_ContagemResultadoEvidencias_Arquivo deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_ContagemResultadoEvidencias_Arquivo)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_ContagemResultadoEvidencias_Arquivo obj ;
         obj = this;
         obj.gxTpr_Contagemresultadoevidencia_codigo = deserialized.gxTpr_Contagemresultadoevidencia_codigo;
         obj.gxTpr_Contagemresultado_codigo = deserialized.gxTpr_Contagemresultado_codigo;
         obj.gxTpr_Contagemresultadoevidencia_arquivo = deserialized.gxTpr_Contagemresultadoevidencia_arquivo;
         obj.gxTpr_Contagemresultadoevidencia_nomearq = deserialized.gxTpr_Contagemresultadoevidencia_nomearq;
         obj.gxTpr_Contagemresultadoevidencia_tipoarq = deserialized.gxTpr_Contagemresultadoevidencia_tipoarq;
         obj.gxTpr_Contagemresultadoevidencia_data = deserialized.gxTpr_Contagemresultadoevidencia_data;
         obj.gxTpr_Contagemresultadoevidencia_descricao = deserialized.gxTpr_Contagemresultadoevidencia_descricao;
         obj.gxTpr_Contagemresultadoevidencia_rdmncreated = deserialized.gxTpr_Contagemresultadoevidencia_rdmncreated;
         obj.gxTpr_Tipodocumento_codigo = deserialized.gxTpr_Tipodocumento_codigo;
         obj.gxTpr_Tipodocumento_nome = deserialized.gxTpr_Tipodocumento_nome;
         obj.gxTpr_Contagemresultadoevidencia_link = deserialized.gxTpr_Contagemresultadoevidencia_link;
         obj.gxTpr_Contagemresultadoevidencia_owner = deserialized.gxTpr_Contagemresultadoevidencia_owner;
         obj.gxTpr_Artefatos_codigo = deserialized.gxTpr_Artefatos_codigo;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Codigo") )
               {
                  gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Codigo") )
               {
                  gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultado_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Arquivo") )
               {
                  gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_arquivo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_NomeArq") )
               {
                  gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_nomearq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_TipoArq") )
               {
                  gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_tipoarq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Data") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Descricao") )
               {
                  gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_RdmnCreated") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo") )
               {
                  gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Nome") )
               {
                  gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Link") )
               {
                  gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Owner") )
               {
                  gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_owner = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_Codigo") )
               {
                  gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Artefatos_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_ContagemResultadoEvidencias.Arquivo";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultado_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_Arquivo", StringUtil.RTrim( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_arquivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_NomeArq", StringUtil.RTrim( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_nomearq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_TipoArq", StringUtil.RTrim( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_tipoarq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data) )
         {
            oWriter.WriteStartElement("ContagemResultadoEvidencia_Data");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultadoEvidencia_Data", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_Descricao", StringUtil.RTrim( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated) )
         {
            oWriter.WriteStartElement("ContagemResultadoEvidencia_RdmnCreated");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultadoEvidencia_RdmnCreated", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("TipoDocumento_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("TipoDocumento_Nome", StringUtil.RTrim( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_Link", StringUtil.RTrim( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_Owner", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_owner), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Artefatos_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Artefatos_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultadoEvidencia_Codigo", gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_codigo, false);
         AddObjectProperty("ContagemResultado_Codigo", gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultado_codigo, false);
         AddObjectProperty("ContagemResultadoEvidencia_Arquivo", gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_arquivo, false);
         AddObjectProperty("ContagemResultadoEvidencia_NomeArq", gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_nomearq, false);
         AddObjectProperty("ContagemResultadoEvidencia_TipoArq", gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_tipoarq, false);
         datetime_STZ = gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultadoEvidencia_Data", sDateCnv, false);
         AddObjectProperty("ContagemResultadoEvidencia_Descricao", gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_descricao, false);
         datetime_STZ = gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultadoEvidencia_RdmnCreated", sDateCnv, false);
         AddObjectProperty("TipoDocumento_Codigo", gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_codigo, false);
         AddObjectProperty("TipoDocumento_Nome", gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_nome, false);
         AddObjectProperty("ContagemResultadoEvidencia_Link", gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_link, false);
         AddObjectProperty("ContagemResultadoEvidencia_Owner", gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_owner, false);
         AddObjectProperty("Artefatos_Codigo", gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Artefatos_codigo, false);
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Codigo"   )]
      public int gxTpr_Contagemresultadoevidencia_codigo
      {
         get {
            return gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_codigo ;
         }

         set {
            gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultado_Codigo"   )]
      public int gxTpr_Contagemresultado_codigo
      {
         get {
            return gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultado_codigo ;
         }

         set {
            gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultado_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Arquivo" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Arquivo"   )]
      public String gxTpr_Contagemresultadoevidencia_arquivo
      {
         get {
            return gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_arquivo ;
         }

         set {
            gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_arquivo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_NomeArq" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_NomeArq"   )]
      public String gxTpr_Contagemresultadoevidencia_nomearq
      {
         get {
            return gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_nomearq ;
         }

         set {
            gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_nomearq = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_TipoArq" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_TipoArq"   )]
      public String gxTpr_Contagemresultadoevidencia_tipoarq
      {
         get {
            return gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_tipoarq ;
         }

         set {
            gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_tipoarq = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Data" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Data"  , IsNullable=true )]
      public string gxTpr_Contagemresultadoevidencia_data_Nullable
      {
         get {
            if ( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data = DateTime.MinValue;
            else
               gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadoevidencia_data
      {
         get {
            return gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data ;
         }

         set {
            gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Descricao" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Descricao"   )]
      public String gxTpr_Contagemresultadoevidencia_descricao
      {
         get {
            return gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_descricao ;
         }

         set {
            gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_RdmnCreated" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_RdmnCreated"  , IsNullable=true )]
      public string gxTpr_Contagemresultadoevidencia_rdmncreated_Nullable
      {
         get {
            if ( gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated = DateTime.MinValue;
            else
               gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadoevidencia_rdmncreated
      {
         get {
            return gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated ;
         }

         set {
            gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo"   )]
      public int gxTpr_Tipodocumento_codigo
      {
         get {
            return gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_codigo ;
         }

         set {
            gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "TipoDocumento_Nome" )]
      [  XmlElement( ElementName = "TipoDocumento_Nome"   )]
      public String gxTpr_Tipodocumento_nome
      {
         get {
            return gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_nome ;
         }

         set {
            gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Link" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Link"   )]
      public String gxTpr_Contagemresultadoevidencia_link
      {
         get {
            return gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_link ;
         }

         set {
            gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_link = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Owner" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Owner"   )]
      public int gxTpr_Contagemresultadoevidencia_owner
      {
         get {
            return gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_owner ;
         }

         set {
            gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_owner = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Artefatos_Codigo" )]
      [  XmlElement( ElementName = "Artefatos_Codigo"   )]
      public int gxTpr_Artefatos_codigo
      {
         get {
            return gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Artefatos_codigo ;
         }

         set {
            gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Artefatos_codigo = (int)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_arquivo = "";
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_nomearq = "";
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_tipoarq = "";
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data = DateTimeUtil.ServerNow( (IGxContext)(context), "DEFAULT");
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_descricao = "";
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_nome = "";
         gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_link = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_codigo ;
      protected int gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultado_codigo ;
      protected int gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_codigo ;
      protected int gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_owner ;
      protected int gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Artefatos_codigo ;
      protected String gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_arquivo ;
      protected String gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_nomearq ;
      protected String gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_tipoarq ;
      protected String gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Tipodocumento_nome ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_data ;
      protected DateTime gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_rdmncreated ;
      protected DateTime datetime_STZ ;
      protected String gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_descricao ;
      protected String gxTv_SdtSDT_ContagemResultadoEvidencias_Arquivo_Contagemresultadoevidencia_link ;
   }

   [DataContract(Name = @"SDT_ContagemResultadoEvidencias.Arquivo", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_ContagemResultadoEvidencias_Arquivo_RESTInterface : GxGenericCollectionItem<SdtSDT_ContagemResultadoEvidencias_Arquivo>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_ContagemResultadoEvidencias_Arquivo_RESTInterface( ) : base()
      {
      }

      public SdtSDT_ContagemResultadoEvidencias_Arquivo_RESTInterface( SdtSDT_ContagemResultadoEvidencias_Arquivo psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultadoEvidencia_Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Contagemresultadoevidencia_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultadoevidencia_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Codigo" , Order = 1 )]
      public Nullable<int> gxTpr_Contagemresultado_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultado_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultado_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_Arquivo" , Order = 2 )]
      public String gxTpr_Contagemresultadoevidencia_arquivo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadoevidencia_arquivo) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_arquivo = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_NomeArq" , Order = 3 )]
      public String gxTpr_Contagemresultadoevidencia_nomearq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadoevidencia_nomearq) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_nomearq = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_TipoArq" , Order = 4 )]
      public String gxTpr_Contagemresultadoevidencia_tipoarq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadoevidencia_tipoarq) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_tipoarq = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_Data" , Order = 5 )]
      public String gxTpr_Contagemresultadoevidencia_data
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultadoevidencia_data) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_data = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_Descricao" , Order = 6 )]
      public String gxTpr_Contagemresultadoevidencia_descricao
      {
         get {
            return sdt.gxTpr_Contagemresultadoevidencia_descricao ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_descricao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_RdmnCreated" , Order = 7 )]
      public String gxTpr_Contagemresultadoevidencia_rdmncreated
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultadoevidencia_rdmncreated) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_rdmncreated = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "TipoDocumento_Codigo" , Order = 8 )]
      public Nullable<int> gxTpr_Tipodocumento_codigo
      {
         get {
            return sdt.gxTpr_Tipodocumento_codigo ;
         }

         set {
            sdt.gxTpr_Tipodocumento_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "TipoDocumento_Nome" , Order = 9 )]
      public String gxTpr_Tipodocumento_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tipodocumento_nome) ;
         }

         set {
            sdt.gxTpr_Tipodocumento_nome = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_Link" , Order = 10 )]
      public String gxTpr_Contagemresultadoevidencia_link
      {
         get {
            return sdt.gxTpr_Contagemresultadoevidencia_link ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_link = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_Owner" , Order = 11 )]
      public Nullable<int> gxTpr_Contagemresultadoevidencia_owner
      {
         get {
            return sdt.gxTpr_Contagemresultadoevidencia_owner ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_owner = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Artefatos_Codigo" , Order = 12 )]
      public Nullable<int> gxTpr_Artefatos_codigo
      {
         get {
            return sdt.gxTpr_Artefatos_codigo ;
         }

         set {
            sdt.gxTpr_Artefatos_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtSDT_ContagemResultadoEvidencias_Arquivo sdt
      {
         get {
            return (SdtSDT_ContagemResultadoEvidencias_Arquivo)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_ContagemResultadoEvidencias_Arquivo() ;
         }
      }

   }

}
