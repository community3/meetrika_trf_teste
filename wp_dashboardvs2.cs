/*
               File: WP_DashboardVs2
        Description: Dashboard
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:45:44.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_dashboardvs2 : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_dashboardvs2( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_dashboardvs2( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavAreatrabalho_codigo = new GXCombobox();
         dynavCodigocontrato = new GXCombobox();
         cmbavFiltrostatusdemanda1 = new GXCombobox();
         cmbavFiltrotipodata = new GXCombobox();
         cmbavFiltroperiodo = new GXCombobox();
         cmbavFiltrostatusdemanda2 = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vAREATRABALHO_CODIGO") == 0 )
            {
               AV40Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Contratada_Codigo), 6, 0)));
               AV42UserId = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42UserId), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDSVvAREATRABALHO_CODIGOSS2( AV40Contratada_Codigo, AV42UserId) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCODIGOCONTRATO") == 0 )
            {
               AV43AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43AreaTrabalho_Codigo), 6, 0)));
               AV42UserId = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42UserId), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDSVvCODIGOCONTRATOSS2( AV43AreaTrabalho_Codigo, AV42UserId) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PASS2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTSS2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051813454484");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("QueryViewer/xpertPivot/swf.js", "");
         context.AddJavascriptSource("QueryViewer/FusionCharts/FusionCharts.js", "");
         context.AddJavascriptSource("QueryViewer/Flash/AC_OETags.js", "");
         context.AddJavascriptSource("QueryViewer/js/highcharts.js", "");
         context.AddJavascriptSource("QueryViewer/js/modules/funnel.js", "");
         context.AddJavascriptSource("QueryViewer/oatPivot/gxpivotjs.js", "");
         context.AddJavascriptSource("QueryViewer/QueryViewerRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("QueryViewer/xpertPivot/swf.js", "");
         context.AddJavascriptSource("QueryViewer/FusionCharts/FusionCharts.js", "");
         context.AddJavascriptSource("QueryViewer/Flash/AC_OETags.js", "");
         context.AddJavascriptSource("QueryViewer/js/highcharts.js", "");
         context.AddJavascriptSource("QueryViewer/js/modules/funnel.js", "");
         context.AddJavascriptSource("QueryViewer/oatPivot/gxpivotjs.js", "");
         context.AddJavascriptSource("QueryViewer/QueryViewerRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_dashboardvs2.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDRAGANDDROPDATA", AV19DragAndDropData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDRAGANDDROPDATA", AV19DragAndDropData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vITEMEXPANDDATA", AV29ItemExpandData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vITEMEXPANDDATA", AV29ItemExpandData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vITEMCOLLAPSEDATA", AV27ItemCollapseData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vITEMCOLLAPSEDATA", AV27ItemCollapseData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFILTERCHANGEDDATA", AV20FilterChangedData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFILTERCHANGEDDATA", AV20FilterChangedData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAGGREGATIONCHANGEDDATA", AV6AggregationChangedData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAGGREGATIONCHANGEDDATA", AV6AggregationChangedData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vITEMCLICKDATA", AV26ItemClickData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vITEMCLICKDATA", AV26ItemClickData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vITEMDOUBLECLICKDATA", AV28ItemDoubleClickData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vITEMDOUBLECLICKDATA", AV28ItemDoubleClickData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAXES", AV7Axes);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAXES", AV7Axes);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPARAMETERS", AV33Parameters);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPARAMETERS", AV33Parameters);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPARAMETERS2", AV57Parameters2);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPARAMETERS2", AV57Parameters2);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vISFILTROSVALIDOS", AV25IsFiltrosValidos);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV35WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV35WWPContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFILTROSTATUSDEMANDACOLLECTION", AV56FiltroStatusDemandaCollection);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFILTROSTATUSDEMANDACOLLECTION", AV56FiltroStatusDemandaCollection);
         }
         GxWebStd.gx_hidden_field( context, "vDATA_AUX", context.localUtil.DToC( AV51Data_Aux, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42UserId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEHEADER_Width", StringUtil.RTrim( Dvpanel_tableheader_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEHEADER_Cls", StringUtil.RTrim( Dvpanel_tableheader_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEHEADER_Title", StringUtil.RTrim( Dvpanel_tableheader_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEHEADER_Collapsible", StringUtil.BoolToStr( Dvpanel_tableheader_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEHEADER_Collapsed", StringUtil.BoolToStr( Dvpanel_tableheader_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEHEADER_Autowidth", StringUtil.BoolToStr( Dvpanel_tableheader_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEHEADER_Autoheight", StringUtil.BoolToStr( Dvpanel_tableheader_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEHEADER_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableheader_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEHEADER_Iconposition", StringUtil.RTrim( Dvpanel_tableheader_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEHEADER_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableheader_Autoscroll));
         GxWebStd.gx_hidden_field( context, "USQUERYRESULTADO_Type", StringUtil.RTrim( Usqueryresultado_Type));
         GxWebStd.gx_hidden_field( context, "USQUERYRESULTADO_Objectname", StringUtil.RTrim( Usqueryresultado_Objectname));
         GxWebStd.gx_hidden_field( context, "USQUERYRESULTADO_Fontsize", StringUtil.LTrim( StringUtil.NToC( (decimal)(Usqueryresultado_Fontsize), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "USQUERYRESULTADO_Charttype", StringUtil.RTrim( Usqueryresultado_Charttype));
         GxWebStd.gx_hidden_field( context, "USQUERYRESULTADO_Preferredrendererforqueryviewercharts", StringUtil.RTrim( Usqueryresultado_Preferredrendererforqueryviewercharts));
         GxWebStd.gx_hidden_field( context, "USQUERYRESULTADO_Rememberlayout", StringUtil.BoolToStr( Usqueryresultado_Rememberlayout));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO1_Width", StringUtil.RTrim( Dvpanel_tablegrafico1_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO1_Cls", StringUtil.RTrim( Dvpanel_tablegrafico1_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO1_Title", StringUtil.RTrim( Dvpanel_tablegrafico1_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO1_Collapsible", StringUtil.BoolToStr( Dvpanel_tablegrafico1_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO1_Collapsed", StringUtil.BoolToStr( Dvpanel_tablegrafico1_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO1_Autowidth", StringUtil.BoolToStr( Dvpanel_tablegrafico1_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO1_Autoheight", StringUtil.BoolToStr( Dvpanel_tablegrafico1_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO1_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tablegrafico1_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO1_Iconposition", StringUtil.RTrim( Dvpanel_tablegrafico1_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO1_Autoscroll", StringUtil.BoolToStr( Dvpanel_tablegrafico1_Autoscroll));
         GxWebStd.gx_hidden_field( context, "USQUERYRESPONSABILIDADE_Type", StringUtil.RTrim( Usqueryresponsabilidade_Type));
         GxWebStd.gx_hidden_field( context, "USQUERYRESPONSABILIDADE_Objectname", StringUtil.RTrim( Usqueryresponsabilidade_Objectname));
         GxWebStd.gx_hidden_field( context, "USQUERYRESPONSABILIDADE_Fontsize", StringUtil.LTrim( StringUtil.NToC( (decimal)(Usqueryresponsabilidade_Fontsize), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "USQUERYRESPONSABILIDADE_Charttype", StringUtil.RTrim( Usqueryresponsabilidade_Charttype));
         GxWebStd.gx_hidden_field( context, "USQUERYRESPONSABILIDADE_Preferredrendererforqueryviewercharts", StringUtil.RTrim( Usqueryresponsabilidade_Preferredrendererforqueryviewercharts));
         GxWebStd.gx_hidden_field( context, "USQUERYRESPONSABILIDADE_Rememberlayout", StringUtil.BoolToStr( Usqueryresponsabilidade_Rememberlayout));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO2_Width", StringUtil.RTrim( Dvpanel_tablegrafico2_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO2_Cls", StringUtil.RTrim( Dvpanel_tablegrafico2_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO2_Title", StringUtil.RTrim( Dvpanel_tablegrafico2_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO2_Collapsible", StringUtil.BoolToStr( Dvpanel_tablegrafico2_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO2_Collapsed", StringUtil.BoolToStr( Dvpanel_tablegrafico2_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO2_Autowidth", StringUtil.BoolToStr( Dvpanel_tablegrafico2_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO2_Autoheight", StringUtil.BoolToStr( Dvpanel_tablegrafico2_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO2_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tablegrafico2_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO2_Iconposition", StringUtil.RTrim( Dvpanel_tablegrafico2_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEGRAFICO2_Autoscroll", StringUtil.BoolToStr( Dvpanel_tablegrafico2_Autoscroll));
         GxWebStd.gx_hidden_field( context, "UCMENSAGEM_Title", StringUtil.RTrim( Ucmensagem_Title));
         GxWebStd.gx_hidden_field( context, "UCMENSAGEM_Confirmationtext", StringUtil.RTrim( Ucmensagem_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "UCMENSAGEM_Yesbuttoncaption", StringUtil.RTrim( Ucmensagem_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "UCMENSAGEM_Confirmtype", StringUtil.RTrim( Ucmensagem_Confirmtype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WESS2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTSS2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_dashboardvs2.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_DashboardVs2" ;
      }

      public override String GetPgmdesc( )
      {
         return "Dashboard" ;
      }

      protected void WBSS0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_SS2( true) ;
         }
         else
         {
            wb_table1_2_SS2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_SS2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTSS2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Dashboard", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPSS0( ) ;
      }

      protected void WSSS2( )
      {
         STARTSS2( ) ;
         EVTSS2( ) ;
      }

      protected void EVTSS2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "UCMENSAGEM.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11SS2 */
                              E11SS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12SS2 */
                              E12SS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOBNTCONSULTARPRINCIPAL'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13SS2 */
                              E13SS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14SS2 */
                              E14SS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VFILTROSTATUSDEMANDA2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15SS2 */
                              E15SS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16SS2 */
                              E16SS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WESS2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PASS2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavAreatrabalho_codigo.Name = "vAREATRABALHO_CODIGO";
            dynavAreatrabalho_codigo.WebTags = "";
            dynavCodigocontrato.Name = "vCODIGOCONTRATO";
            dynavCodigocontrato.WebTags = "";
            cmbavFiltrostatusdemanda1.Name = "vFILTROSTATUSDEMANDA1";
            cmbavFiltrostatusdemanda1.WebTags = "";
            cmbavFiltrostatusdemanda1.addItem("", "Selecione", 0);
            cmbavFiltrostatusdemanda1.addItem("Y", "Demandas em Aguardo", 0);
            cmbavFiltrostatusdemanda1.addItem("F", "Demandas em Aberto", 0);
            cmbavFiltrostatusdemanda1.addItem("Z", "Demandas Finalizadas", 0);
            cmbavFiltrostatusdemanda1.addItem("P", "A Pagar", 0);
            cmbavFiltrostatusdemanda1.addItem("O", "Aceite", 0);
            cmbavFiltrostatusdemanda1.addItem("I", "An�lise Planejamento", 0);
            cmbavFiltrostatusdemanda1.addItem("X", "Cancelada", 0);
            cmbavFiltrostatusdemanda1.addItem("C", "Conferida", 0);
            cmbavFiltrostatusdemanda1.addItem("E", "Em An�lise", 0);
            cmbavFiltrostatusdemanda1.addItem("A", "Em Execu��o", 0);
            cmbavFiltrostatusdemanda1.addItem("H", "Homologada", 0);
            cmbavFiltrostatusdemanda1.addItem("L", "Liquidada", 0);
            cmbavFiltrostatusdemanda1.addItem("N", "N�o Faturada", 0);
            cmbavFiltrostatusdemanda1.addItem("J", "Planejamento", 0);
            cmbavFiltrostatusdemanda1.addItem("U", "Rascunho", 0);
            cmbavFiltrostatusdemanda1.addItem("D", "Rejeitada", 0);
            cmbavFiltrostatusdemanda1.addItem("R", "Resolvida", 0);
            cmbavFiltrostatusdemanda1.addItem("S", "Solicitada", 0);
            cmbavFiltrostatusdemanda1.addItem("B", "Stand by", 0);
            cmbavFiltrostatusdemanda1.addItem("M", "Valida��o Mensura��o", 0);
            cmbavFiltrostatusdemanda1.addItem("Q", "Validacao Qualidade", 0);
            cmbavFiltrostatusdemanda1.addItem("T", "Validacao T�cnica", 0);
            cmbavFiltrostatusdemanda1.addItem("G", "Em Homologa��o", 0);
            if ( cmbavFiltrostatusdemanda1.ItemCount > 0 )
            {
               AV46FiltroStatusDemanda1 = cmbavFiltrostatusdemanda1.getValidValue(AV46FiltroStatusDemanda1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46FiltroStatusDemanda1", AV46FiltroStatusDemanda1);
            }
            cmbavFiltrotipodata.Name = "vFILTROTIPODATA";
            cmbavFiltrotipodata.WebTags = "";
            cmbavFiltrotipodata.addItem("1", "Data da Demanda", 0);
            cmbavFiltrotipodata.addItem("2", "Data da Contagem", 0);
            if ( cmbavFiltrotipodata.ItemCount > 0 )
            {
               AV45FiltroTipoData = (short)(NumberUtil.Val( cmbavFiltrotipodata.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV45FiltroTipoData), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45FiltroTipoData", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45FiltroTipoData), 4, 0)));
            }
            cmbavFiltroperiodo.Name = "vFILTROPERIODO";
            cmbavFiltroperiodo.WebTags = "";
            cmbavFiltroperiodo.addItem("1", "Hoje", 0);
            cmbavFiltroperiodo.addItem("2", "Nesta Semana", 0);
            cmbavFiltroperiodo.addItem("3", "Nesta Pr�xima Semana", 0);
            cmbavFiltroperiodo.addItem("4", "Nesta Quinzena", 0);
            cmbavFiltroperiodo.addItem("5", "Neste M�s", 0);
            cmbavFiltroperiodo.addItem("6", "Neste Trimestre", 0);
            cmbavFiltroperiodo.addItem("7", "Neste Semestre", 0);
            cmbavFiltroperiodo.addItem("8", "Neste Ano", 0);
            cmbavFiltroperiodo.addItem("9", "No �ltimo M�s", 0);
            cmbavFiltroperiodo.addItem("10", "No �ltimo Trimestre", 0);
            cmbavFiltroperiodo.addItem("11", "No �ltimo Semestre", 0);
            cmbavFiltroperiodo.addItem("12", "No �ltimo Ano", 0);
            cmbavFiltroperiodo.addItem("13", "Per�odo Personalizado", 0);
            if ( cmbavFiltroperiodo.ItemCount > 0 )
            {
               AV47FiltroPeriodo = (short)(NumberUtil.Val( cmbavFiltroperiodo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV47FiltroPeriodo), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47FiltroPeriodo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47FiltroPeriodo), 4, 0)));
            }
            cmbavFiltrostatusdemanda2.Name = "vFILTROSTATUSDEMANDA2";
            cmbavFiltrostatusdemanda2.WebTags = "";
            cmbavFiltrostatusdemanda2.addItem("", "Selecione", 0);
            cmbavFiltrostatusdemanda2.addItem("Y", "Demandas em Aguardo", 0);
            cmbavFiltrostatusdemanda2.addItem("F", "Demandas em Aberto", 0);
            cmbavFiltrostatusdemanda2.addItem("Z", "Demandas Finalizadas", 0);
            cmbavFiltrostatusdemanda2.addItem("P", "A Pagar", 0);
            cmbavFiltrostatusdemanda2.addItem("O", "Aceite", 0);
            cmbavFiltrostatusdemanda2.addItem("I", "An�lise Planejamento", 0);
            cmbavFiltrostatusdemanda2.addItem("X", "Cancelada", 0);
            cmbavFiltrostatusdemanda2.addItem("C", "Conferida", 0);
            cmbavFiltrostatusdemanda2.addItem("E", "Em An�lise", 0);
            cmbavFiltrostatusdemanda2.addItem("A", "Em Execu��o", 0);
            cmbavFiltrostatusdemanda2.addItem("H", "Homologada", 0);
            cmbavFiltrostatusdemanda2.addItem("L", "Liquidada", 0);
            cmbavFiltrostatusdemanda2.addItem("N", "N�o Faturada", 0);
            cmbavFiltrostatusdemanda2.addItem("J", "Planejamento", 0);
            cmbavFiltrostatusdemanda2.addItem("U", "Rascunho", 0);
            cmbavFiltrostatusdemanda2.addItem("D", "Rejeitada", 0);
            cmbavFiltrostatusdemanda2.addItem("R", "Resolvida", 0);
            cmbavFiltrostatusdemanda2.addItem("S", "Solicitada", 0);
            cmbavFiltrostatusdemanda2.addItem("B", "Stand by", 0);
            cmbavFiltrostatusdemanda2.addItem("M", "Valida��o Mensura��o", 0);
            cmbavFiltrostatusdemanda2.addItem("Q", "Validacao Qualidade", 0);
            cmbavFiltrostatusdemanda2.addItem("T", "Validacao T�cnica", 0);
            cmbavFiltrostatusdemanda2.addItem("G", "Em Homologa��o", 0);
            if ( cmbavFiltrostatusdemanda2.ItemCount > 0 )
            {
               AV50FiltroStatusDemanda2 = cmbavFiltrostatusdemanda2.getValidValue(AV50FiltroStatusDemanda2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50FiltroStatusDemanda2", AV50FiltroStatusDemanda2);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvCODIGOCONTRATO_htmlSS2( AV43AreaTrabalho_Codigo, AV42UserId) ;
         GXVvCODIGOCONTRATO_htmlSS2( AV43AreaTrabalho_Codigo, AV42UserId) ;
         /* End function dynload_actions */
      }

      protected void GXDSVvAREATRABALHO_CODIGOSS2( int AV40Contratada_Codigo ,
                                                   int AV42UserId )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDSVvAREATRABALHO_CODIGO_dataSS2( AV40Contratada_Codigo, AV42UserId) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvAREATRABALHO_CODIGO_htmlSS2( int AV40Contratada_Codigo ,
                                                      int AV42UserId )
      {
         int gxdynajaxvalue ;
         GXDSVvAREATRABALHO_CODIGO_dataSS2( AV40Contratada_Codigo, AV42UserId) ;
         gxdynajaxindex = 1;
         dynavAreatrabalho_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavAreatrabalho_codigo.ItemCount > 0 )
         {
            AV43AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV43AreaTrabalho_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43AreaTrabalho_Codigo), 6, 0)));
         }
      }

      protected void GXDSVvAREATRABALHO_CODIGO_dataSS2( int AV40Contratada_Codigo ,
                                                        int AV42UserId )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Todas");
         IGxCollection gxcolvAREATRABALHO_CODIGO ;
         SdtSDT_Codigos gxcolitemvAREATRABALHO_CODIGO ;
         new dp_areatrabalhofiltro(context ).execute(  AV40Contratada_Codigo,  AV42UserId, out  gxcolvAREATRABALHO_CODIGO) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42UserId), 6, 0)));
         gxcolvAREATRABALHO_CODIGO.Sort("Descricao");
         int gxindex = 1 ;
         while ( gxindex <= gxcolvAREATRABALHO_CODIGO.Count )
         {
            gxcolitemvAREATRABALHO_CODIGO = ((SdtSDT_Codigos)gxcolvAREATRABALHO_CODIGO.Item(gxindex));
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.Str( (decimal)(gxcolitemvAREATRABALHO_CODIGO.gxTpr_Codigo), 6, 0)));
            gxdynajaxctrldescr.Add(gxcolitemvAREATRABALHO_CODIGO.gxTpr_Descricao);
            gxindex = (int)(gxindex+1);
         }
      }

      protected void GXDSVvCODIGOCONTRATOSS2( int AV43AreaTrabalho_Codigo ,
                                              int AV42UserId )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDSVvCODIGOCONTRATO_dataSS2( AV43AreaTrabalho_Codigo, AV42UserId) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCODIGOCONTRATO_htmlSS2( int AV43AreaTrabalho_Codigo ,
                                                 int AV42UserId )
      {
         int gxdynajaxvalue ;
         GXDSVvCODIGOCONTRATO_dataSS2( AV43AreaTrabalho_Codigo, AV42UserId) ;
         gxdynajaxindex = 1;
         dynavCodigocontrato.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavCodigocontrato.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavCodigocontrato.ItemCount > 0 )
         {
            AV39CodigoContrato = (int)(NumberUtil.Val( dynavCodigocontrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV39CodigoContrato), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39CodigoContrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39CodigoContrato), 6, 0)));
         }
      }

      protected void GXDSVvCODIGOCONTRATO_dataSS2( int AV43AreaTrabalho_Codigo ,
                                                   int AV42UserId )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         IGxCollection gxcolvCODIGOCONTRATO ;
         SdtSDT_Codigos gxcolitemvCODIGOCONTRATO ;
         new dp_wp_dashboard_contrato(context ).execute(  AV43AreaTrabalho_Codigo,  AV42UserId, out  gxcolvCODIGOCONTRATO) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42UserId), 6, 0)));
         gxcolvCODIGOCONTRATO.Sort("Descricao");
         int gxindex = 1 ;
         while ( gxindex <= gxcolvCODIGOCONTRATO.Count )
         {
            gxcolitemvCODIGOCONTRATO = ((SdtSDT_Codigos)gxcolvCODIGOCONTRATO.Item(gxindex));
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.Str( (decimal)(gxcolitemvCODIGOCONTRATO.gxTpr_Codigo), 6, 0)));
            gxdynajaxctrldescr.Add(gxcolitemvCODIGOCONTRATO.gxTpr_Descricao);
            gxindex = (int)(gxindex+1);
         }
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavAreatrabalho_codigo.ItemCount > 0 )
         {
            AV43AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV43AreaTrabalho_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43AreaTrabalho_Codigo), 6, 0)));
         }
         if ( dynavCodigocontrato.ItemCount > 0 )
         {
            AV39CodigoContrato = (int)(NumberUtil.Val( dynavCodigocontrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV39CodigoContrato), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39CodigoContrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39CodigoContrato), 6, 0)));
         }
         if ( cmbavFiltrostatusdemanda1.ItemCount > 0 )
         {
            AV46FiltroStatusDemanda1 = cmbavFiltrostatusdemanda1.getValidValue(AV46FiltroStatusDemanda1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46FiltroStatusDemanda1", AV46FiltroStatusDemanda1);
         }
         if ( cmbavFiltrotipodata.ItemCount > 0 )
         {
            AV45FiltroTipoData = (short)(NumberUtil.Val( cmbavFiltrotipodata.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV45FiltroTipoData), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45FiltroTipoData", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45FiltroTipoData), 4, 0)));
         }
         if ( cmbavFiltroperiodo.ItemCount > 0 )
         {
            AV47FiltroPeriodo = (short)(NumberUtil.Val( cmbavFiltroperiodo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV47FiltroPeriodo), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47FiltroPeriodo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47FiltroPeriodo), 4, 0)));
         }
         if ( cmbavFiltrostatusdemanda2.ItemCount > 0 )
         {
            AV50FiltroStatusDemanda2 = cmbavFiltrostatusdemanda2.getValidValue(AV50FiltroStatusDemanda2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50FiltroStatusDemanda2", AV50FiltroStatusDemanda2);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFSS2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFSS2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E14SS2 */
         E14SS2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E16SS2 */
            E16SS2 ();
            WBSS0( ) ;
         }
      }

      protected void STRUPSS0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12SS2 */
         E12SS2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvAREATRABALHO_CODIGO_htmlSS2( AV40Contratada_Codigo, AV42UserId) ;
         GXVvCODIGOCONTRATO_htmlSS2( AV43AreaTrabalho_Codigo, AV42UserId) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDRAGANDDROPDATA"), AV19DragAndDropData);
            ajax_req_read_hidden_sdt(cgiGet( "vITEMEXPANDDATA"), AV29ItemExpandData);
            ajax_req_read_hidden_sdt(cgiGet( "vITEMCOLLAPSEDATA"), AV27ItemCollapseData);
            ajax_req_read_hidden_sdt(cgiGet( "vFILTERCHANGEDDATA"), AV20FilterChangedData);
            ajax_req_read_hidden_sdt(cgiGet( "vAGGREGATIONCHANGEDDATA"), AV6AggregationChangedData);
            ajax_req_read_hidden_sdt(cgiGet( "vITEMCLICKDATA"), AV26ItemClickData);
            ajax_req_read_hidden_sdt(cgiGet( "vITEMDOUBLECLICKDATA"), AV28ItemDoubleClickData);
            ajax_req_read_hidden_sdt(cgiGet( "vAXES"), AV7Axes);
            ajax_req_read_hidden_sdt(cgiGet( "vPARAMETERS"), AV33Parameters);
            ajax_req_read_hidden_sdt(cgiGet( "vPARAMETERS2"), AV57Parameters2);
            /* Read variables values. */
            dynavAreatrabalho_codigo.CurrentValue = cgiGet( dynavAreatrabalho_codigo_Internalname);
            AV43AreaTrabalho_Codigo = (int)(NumberUtil.Val( cgiGet( dynavAreatrabalho_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43AreaTrabalho_Codigo), 6, 0)));
            dynavCodigocontrato.CurrentValue = cgiGet( dynavCodigocontrato_Internalname);
            AV39CodigoContrato = (int)(NumberUtil.Val( cgiGet( dynavCodigocontrato_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39CodigoContrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39CodigoContrato), 6, 0)));
            cmbavFiltrostatusdemanda1.CurrentValue = cgiGet( cmbavFiltrostatusdemanda1_Internalname);
            AV46FiltroStatusDemanda1 = cgiGet( cmbavFiltrostatusdemanda1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46FiltroStatusDemanda1", AV46FiltroStatusDemanda1);
            cmbavFiltrotipodata.CurrentValue = cgiGet( cmbavFiltrotipodata_Internalname);
            AV45FiltroTipoData = (short)(NumberUtil.Val( cgiGet( cmbavFiltrotipodata_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45FiltroTipoData", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45FiltroTipoData), 4, 0)));
            cmbavFiltroperiodo.CurrentValue = cgiGet( cmbavFiltroperiodo_Internalname);
            AV47FiltroPeriodo = (short)(NumberUtil.Val( cgiGet( cmbavFiltroperiodo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47FiltroPeriodo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47FiltroPeriodo), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavFiltroperiododata_in_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Filtro Periodo Data_In"}), 1, "vFILTROPERIODODATA_IN");
               GX_FocusControl = edtavFiltroperiododata_in_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48FiltroPeriodoData_In = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            }
            else
            {
               AV48FiltroPeriodoData_In = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFiltroperiododata_in_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavFiltroperiododata_fim_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Filtro Periodo Data_Fim"}), 1, "vFILTROPERIODODATA_FIM");
               GX_FocusControl = edtavFiltroperiododata_fim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49FiltroPeriodoData_Fim = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
            }
            else
            {
               AV49FiltroPeriodoData_Fim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFiltroperiododata_fim_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
            }
            cmbavFiltrostatusdemanda2.CurrentValue = cgiGet( cmbavFiltrostatusdemanda2_Internalname);
            AV50FiltroStatusDemanda2 = cgiGet( cmbavFiltrostatusdemanda2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50FiltroStatusDemanda2", AV50FiltroStatusDemanda2);
            /* Read saved values. */
            Dvpanel_tableheader_Width = cgiGet( "DVPANEL_TABLEHEADER_Width");
            Dvpanel_tableheader_Cls = cgiGet( "DVPANEL_TABLEHEADER_Cls");
            Dvpanel_tableheader_Title = cgiGet( "DVPANEL_TABLEHEADER_Title");
            Dvpanel_tableheader_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEHEADER_Collapsible"));
            Dvpanel_tableheader_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEHEADER_Collapsed"));
            Dvpanel_tableheader_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEHEADER_Autowidth"));
            Dvpanel_tableheader_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEHEADER_Autoheight"));
            Dvpanel_tableheader_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEHEADER_Showcollapseicon"));
            Dvpanel_tableheader_Iconposition = cgiGet( "DVPANEL_TABLEHEADER_Iconposition");
            Dvpanel_tableheader_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEHEADER_Autoscroll"));
            Usqueryresultado_Type = cgiGet( "USQUERYRESULTADO_Type");
            Usqueryresultado_Objectname = cgiGet( "USQUERYRESULTADO_Objectname");
            Usqueryresultado_Fontsize = (int)(context.localUtil.CToN( cgiGet( "USQUERYRESULTADO_Fontsize"), ",", "."));
            Usqueryresultado_Charttype = cgiGet( "USQUERYRESULTADO_Charttype");
            Usqueryresultado_Preferredrendererforqueryviewercharts = cgiGet( "USQUERYRESULTADO_Preferredrendererforqueryviewercharts");
            Usqueryresultado_Rememberlayout = StringUtil.StrToBool( cgiGet( "USQUERYRESULTADO_Rememberlayout"));
            Dvpanel_tablegrafico1_Width = cgiGet( "DVPANEL_TABLEGRAFICO1_Width");
            Dvpanel_tablegrafico1_Cls = cgiGet( "DVPANEL_TABLEGRAFICO1_Cls");
            Dvpanel_tablegrafico1_Title = cgiGet( "DVPANEL_TABLEGRAFICO1_Title");
            Dvpanel_tablegrafico1_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEGRAFICO1_Collapsible"));
            Dvpanel_tablegrafico1_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEGRAFICO1_Collapsed"));
            Dvpanel_tablegrafico1_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEGRAFICO1_Autowidth"));
            Dvpanel_tablegrafico1_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEGRAFICO1_Autoheight"));
            Dvpanel_tablegrafico1_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEGRAFICO1_Showcollapseicon"));
            Dvpanel_tablegrafico1_Iconposition = cgiGet( "DVPANEL_TABLEGRAFICO1_Iconposition");
            Dvpanel_tablegrafico1_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEGRAFICO1_Autoscroll"));
            Usqueryresponsabilidade_Type = cgiGet( "USQUERYRESPONSABILIDADE_Type");
            Usqueryresponsabilidade_Objectname = cgiGet( "USQUERYRESPONSABILIDADE_Objectname");
            Usqueryresponsabilidade_Fontsize = (int)(context.localUtil.CToN( cgiGet( "USQUERYRESPONSABILIDADE_Fontsize"), ",", "."));
            Usqueryresponsabilidade_Charttype = cgiGet( "USQUERYRESPONSABILIDADE_Charttype");
            Usqueryresponsabilidade_Preferredrendererforqueryviewercharts = cgiGet( "USQUERYRESPONSABILIDADE_Preferredrendererforqueryviewercharts");
            Usqueryresponsabilidade_Rememberlayout = StringUtil.StrToBool( cgiGet( "USQUERYRESPONSABILIDADE_Rememberlayout"));
            Dvpanel_tablegrafico2_Width = cgiGet( "DVPANEL_TABLEGRAFICO2_Width");
            Dvpanel_tablegrafico2_Cls = cgiGet( "DVPANEL_TABLEGRAFICO2_Cls");
            Dvpanel_tablegrafico2_Title = cgiGet( "DVPANEL_TABLEGRAFICO2_Title");
            Dvpanel_tablegrafico2_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEGRAFICO2_Collapsible"));
            Dvpanel_tablegrafico2_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEGRAFICO2_Collapsed"));
            Dvpanel_tablegrafico2_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEGRAFICO2_Autowidth"));
            Dvpanel_tablegrafico2_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEGRAFICO2_Autoheight"));
            Dvpanel_tablegrafico2_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEGRAFICO2_Showcollapseicon"));
            Dvpanel_tablegrafico2_Iconposition = cgiGet( "DVPANEL_TABLEGRAFICO2_Iconposition");
            Dvpanel_tablegrafico2_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEGRAFICO2_Autoscroll"));
            Ucmensagem_Title = cgiGet( "UCMENSAGEM_Title");
            Ucmensagem_Confirmationtext = cgiGet( "UCMENSAGEM_Confirmationtext");
            Ucmensagem_Yesbuttoncaption = cgiGet( "UCMENSAGEM_Yesbuttoncaption");
            Ucmensagem_Confirmtype = cgiGet( "UCMENSAGEM_Confirmtype");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvAREATRABALHO_CODIGO_htmlSS2( AV40Contratada_Codigo, AV42UserId) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12SS2 */
         E12SS2 ();
         if (returnInSub) return;
      }

      protected void E12SS2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV35WWPContext) ;
         AV40Contratada_Codigo = AV35WWPContext.gxTpr_Contratada_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Contratada_Codigo), 6, 0)));
         AV41Contratante_Codigo = AV35WWPContext.gxTpr_Contratante_codigo;
         AV42UserId = AV35WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42UserId), 6, 0)));
         AV38ContratoGestor_ContratadaAreaCod = AV35WWPContext.gxTpr_Areatrabalho_codigo;
         AV37ContratoGestor_UsuarioCod = AV35WWPContext.gxTpr_Userid;
         AV25IsFiltrosValidos = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25IsFiltrosValidos", AV25IsFiltrosValidos);
         if ( ! AV35WWPContext.gxTpr_Userehgestor )
         {
            Ucmensagem_Confirmationtext = "Usu�rio n�o possui permiss�o de acesso.";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ucmensagem_Internalname, "ConfirmationText", Ucmensagem_Confirmationtext);
            this.executeUsercontrolMethod("", false, "UCMENSAGEMContainer", "Confirm", "", new Object[] {});
         }
         /* Execute user subroutine: 'CARREGRA.FILTRO.INICIAL' */
         S112 ();
         if (returnInSub) return;
      }

      protected void E13SS2( )
      {
         /* 'DoBntConsultarPrincipal' Routine */
         /* Execute user subroutine: 'VALIDA.FILTROS' */
         S122 ();
         if (returnInSub) return;
         if ( AV25IsFiltrosValidos )
         {
            AV33Parameters = new GxObjectCollection( context, "QueryViewerParameters.Parameter", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerParameters_Parameter", "GeneXus.Programs");
            AV57Parameters2 = new GxObjectCollection( context, "QueryViewerParameters.Parameter", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerParameters_Parameter", "GeneXus.Programs");
            AV54AreaTrabalho_CodigoCollection.Clear();
            if ( ! (0==AV43AreaTrabalho_Codigo) )
            {
               AV54AreaTrabalho_CodigoCollection.Add(AV43AreaTrabalho_Codigo, 0);
               AV32Parameter = new SdtQueryViewerParameters_Parameter(context);
               AV32Parameter.gxTpr_Name = "FiltroAreaTrabalho";
               AV32Parameter.gxTpr_Value = AV54AreaTrabalho_CodigoCollection.ToJSonString(false);
               AV33Parameters.Add(AV32Parameter, 0);
               AV57Parameters2.Add(AV32Parameter, 0);
            }
            else
            {
               GXt_objcol_SdtSDT_Codigos1 = AV53AreaTrabalhoCollection;
               new dp_areatrabalhofiltro(context ).execute(  AV35WWPContext.gxTpr_Contratada_codigo,  AV35WWPContext.gxTpr_Userid, out  GXt_objcol_SdtSDT_Codigos1) ;
               AV53AreaTrabalhoCollection = GXt_objcol_SdtSDT_Codigos1;
               AV61GXV1 = 1;
               while ( AV61GXV1 <= AV53AreaTrabalhoCollection.Count )
               {
                  AV55AreaTrabalhoItens = ((SdtSDT_Codigos)AV53AreaTrabalhoCollection.Item(AV61GXV1));
                  AV54AreaTrabalho_CodigoCollection.Add(AV55AreaTrabalhoItens.gxTpr_Codigo, 0);
                  AV61GXV1 = (int)(AV61GXV1+1);
               }
               AV32Parameter = new SdtQueryViewerParameters_Parameter(context);
               AV32Parameter.gxTpr_Name = "FiltroAreaTrabalho";
               AV32Parameter.gxTpr_Value = AV54AreaTrabalho_CodigoCollection.ToJSonString(false);
               AV33Parameters.Add(AV32Parameter, 0);
               AV57Parameters2.Add(AV32Parameter, 0);
            }
            if ( ! (0==AV39CodigoContrato) )
            {
               AV32Parameter = new SdtQueryViewerParameters_Parameter(context);
               AV32Parameter.gxTpr_Name = "FiltroContrato";
               AV32Parameter.gxTpr_Value = StringUtil.Trim( StringUtil.Str( (decimal)(AV39CodigoContrato), 6, 0));
               AV33Parameters.Add(AV32Parameter, 0);
               AV57Parameters2.Add(AV32Parameter, 0);
            }
            /* Execute user subroutine: 'CARREGA.FILTRO.STATUS.GRAFICO1' */
            S132 ();
            if (returnInSub) return;
            AV32Parameter = new SdtQueryViewerParameters_Parameter(context);
            AV32Parameter.gxTpr_Name = "FiltroDemandaSituacao";
            AV32Parameter.gxTpr_Value = AV56FiltroStatusDemandaCollection.ToJSonString(false);
            AV33Parameters.Add(AV32Parameter, 0);
            /* Execute user subroutine: 'CARREGA.FILTRO.STATUS.GRAFICO2' */
            S142 ();
            if (returnInSub) return;
            AV32Parameter = new SdtQueryViewerParameters_Parameter(context);
            AV32Parameter.gxTpr_Name = "FiltroDemandaSituacao";
            AV32Parameter.gxTpr_Value = AV56FiltroStatusDemandaCollection.ToJSonString(false);
            AV57Parameters2.Add(AV32Parameter, 0);
            AV32Parameter = new SdtQueryViewerParameters_Parameter(context);
            AV32Parameter.gxTpr_Name = "FiltroTipoData";
            AV32Parameter.gxTpr_Value = StringUtil.Trim( StringUtil.Str( (decimal)(AV45FiltroTipoData), 4, 0));
            AV33Parameters.Add(AV32Parameter, 0);
            AV57Parameters2.Add(AV32Parameter, 0);
            /* Execute user subroutine: 'GERA.FILTRO.PERIODO' */
            S152 ();
            if (returnInSub) return;
            AV32Parameter = new SdtQueryViewerParameters_Parameter(context);
            AV32Parameter.gxTpr_Name = "FiltroPeriodoDataIn";
            AV32Parameter.gxTpr_Value = StringUtil.Trim( context.localUtil.DToC( AV48FiltroPeriodoData_In, 2, "/"));
            AV33Parameters.Add(AV32Parameter, 0);
            AV57Parameters2.Add(AV32Parameter, 0);
            AV32Parameter = new SdtQueryViewerParameters_Parameter(context);
            AV32Parameter.gxTpr_Name = "FiltroPeriodoDataFim";
            AV32Parameter.gxTpr_Value = StringUtil.Trim( context.localUtil.DToC( AV49FiltroPeriodoData_Fim, 2, "/"));
            AV33Parameters.Add(AV32Parameter, 0);
            AV33Parameters.Add(AV32Parameter, 0);
            AV57Parameters2.Add(AV32Parameter, 0);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33Parameters", AV33Parameters);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57Parameters2", AV57Parameters2);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV56FiltroStatusDemandaCollection", AV56FiltroStatusDemandaCollection);
      }

      protected void E14SS2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV18Context) ;
      }

      protected void E11SS2( )
      {
         /* Ucmensagem_Close Routine */
         context.wjLoc = formatLink("wp_gestao.aspx") ;
         context.wjLocDisableFrm = 1;
         context.DoAjaxRefresh();
      }

      protected void E15SS2( )
      {
         /* Filtrostatusdemanda2_Click Routine */
         AV58item = 0;
         AV62GXV2 = 1;
         while ( AV62GXV2 <= AV57Parameters2.Count )
         {
            AV32Parameter = ((SdtQueryViewerParameters_Parameter)AV57Parameters2.Item(AV62GXV2));
            AV58item = (short)(AV58item+1);
            if ( StringUtil.StrCmp(AV32Parameter.gxTpr_Name, "FiltroDemandaSituacao") == 0 )
            {
               AV57Parameters2.RemoveItem(AV58item);
               if (true) break;
            }
            AV62GXV2 = (int)(AV62GXV2+1);
         }
         /* Execute user subroutine: 'CARREGA.FILTRO.STATUS.GRAFICO2' */
         S142 ();
         if (returnInSub) return;
         AV32Parameter = new SdtQueryViewerParameters_Parameter(context);
         AV32Parameter.gxTpr_Name = "FiltroDemandaSituacao";
         AV32Parameter.gxTpr_Value = AV56FiltroStatusDemandaCollection.ToJSonString(false);
         AV57Parameters2.Add(AV32Parameter, 0);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57Parameters2", AV57Parameters2);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV56FiltroStatusDemandaCollection", AV56FiltroStatusDemandaCollection);
      }

      protected void S112( )
      {
         /* 'CARREGRA.FILTRO.INICIAL' Routine */
         AV43AreaTrabalho_Codigo = AV35WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43AreaTrabalho_Codigo), 6, 0)));
         AV45FiltroTipoData = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45FiltroTipoData", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45FiltroTipoData), 4, 0)));
         AV47FiltroPeriodo = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47FiltroPeriodo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47FiltroPeriodo), 4, 0)));
         /* Execute user subroutine: 'GERA.FILTRO.PERIODO' */
         S152 ();
         if (returnInSub) return;
      }

      protected void S122( )
      {
         /* 'VALIDA.FILTROS' Routine */
         AV25IsFiltrosValidos = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25IsFiltrosValidos", AV25IsFiltrosValidos);
         if ( ( AV47FiltroPeriodo == 13 ) && ( (DateTime.MinValue==AV48FiltroPeriodoData_In) || (DateTime.MinValue==AV49FiltroPeriodoData_Fim) ) || ( ! (DateTime.MinValue==AV48FiltroPeriodoData_In) && ! (DateTime.MinValue==AV49FiltroPeriodoData_Fim) && ( AV48FiltroPeriodoData_In > AV49FiltroPeriodoData_Fim ) ) )
         {
            GX_msglist.addItem("O Filtro Data n�o � v�lida. Verifique!");
            AV25IsFiltrosValidos = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25IsFiltrosValidos", AV25IsFiltrosValidos);
         }
      }

      protected void S152( )
      {
         /* 'GERA.FILTRO.PERIODO' Routine */
         AV48FiltroPeriodoData_In = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
         AV49FiltroPeriodoData_Fim = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
         lblTextblockfiltroperiododata_fim_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockfiltroperiododata_fim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockfiltroperiododata_fim_Visible), 5, 0)));
         edtavFiltroperiododata_in_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFiltroperiododata_in_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFiltroperiododata_in_Visible), 5, 0)));
         edtavFiltroperiododata_fim_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFiltroperiododata_fim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFiltroperiododata_fim_Visible), 5, 0)));
         if ( AV47FiltroPeriodo == 1 )
         {
            AV48FiltroPeriodoData_In = DateTimeUtil.ServerDate( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            AV49FiltroPeriodoData_Fim = AV48FiltroPeriodoData_In;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
         }
         else if ( AV47FiltroPeriodo == 2 )
         {
            AV48FiltroPeriodoData_In = DateTimeUtil.ServerDate( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            AV51Data_Aux = AV48FiltroPeriodoData_In;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Data_Aux", context.localUtil.Format(AV51Data_Aux, "99/99/9999"));
            while ( DateTimeUtil.Dow( AV51Data_Aux) != 1 )
            {
               GXt_dtime2 = DateTimeUtil.ResetTime( AV51Data_Aux ) ;
               AV51Data_Aux = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime2, 86400*(1)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Data_Aux", context.localUtil.Format(AV51Data_Aux, "99/99/9999"));
            }
            AV49FiltroPeriodoData_Fim = AV51Data_Aux;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
         }
         else if ( AV47FiltroPeriodo == 3 )
         {
            AV48FiltroPeriodoData_In = DateTimeUtil.ServerDate( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            if ( DateTimeUtil.Dow( AV51Data_Aux) == 1 )
            {
               GXt_dtime2 = DateTimeUtil.ResetTime( AV48FiltroPeriodoData_In ) ;
               AV48FiltroPeriodoData_In = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime2, 86400*(1)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            }
            AV51Data_Aux = AV48FiltroPeriodoData_In;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Data_Aux", context.localUtil.Format(AV51Data_Aux, "99/99/9999"));
            while ( DateTimeUtil.Dow( AV51Data_Aux) != 2 )
            {
               GXt_dtime2 = DateTimeUtil.ResetTime( AV51Data_Aux ) ;
               AV51Data_Aux = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime2, 86400*(1)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Data_Aux", context.localUtil.Format(AV51Data_Aux, "99/99/9999"));
            }
            AV48FiltroPeriodoData_In = AV51Data_Aux;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            while ( DateTimeUtil.Dow( AV51Data_Aux) != 1 )
            {
               GXt_dtime2 = DateTimeUtil.ResetTime( AV51Data_Aux ) ;
               AV51Data_Aux = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime2, 86400*(1)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Data_Aux", context.localUtil.Format(AV51Data_Aux, "99/99/9999"));
            }
            AV49FiltroPeriodoData_Fim = AV51Data_Aux;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
         }
         else if ( AV47FiltroPeriodo == 4 )
         {
            AV48FiltroPeriodoData_In = DateTimeUtil.ServerDate( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            GXt_dtime2 = DateTimeUtil.ResetTime( AV48FiltroPeriodoData_In ) ;
            AV49FiltroPeriodoData_Fim = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime2, 86400*(14)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
         }
         else if ( AV47FiltroPeriodo == 5 )
         {
            AV48FiltroPeriodoData_In = DateTimeUtil.ServerDate( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            AV48FiltroPeriodoData_In = context.localUtil.YMDToD( DateTimeUtil.Year( AV48FiltroPeriodoData_In), DateTimeUtil.Month( AV48FiltroPeriodoData_In), 1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            AV49FiltroPeriodoData_Fim = DateTimeUtil.DateEndOfMonth( AV48FiltroPeriodoData_In);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
         }
         else if ( AV47FiltroPeriodo == 6 )
         {
            AV48FiltroPeriodoData_In = DateTimeUtil.ServerDate( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            GXt_dtime2 = DateTimeUtil.ResetTime( AV48FiltroPeriodoData_In ) ;
            AV49FiltroPeriodoData_Fim = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime2, 86400*(89)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
         }
         else if ( AV47FiltroPeriodo == 7 )
         {
            AV48FiltroPeriodoData_In = DateTimeUtil.ServerDate( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            GXt_dtime2 = DateTimeUtil.ResetTime( AV48FiltroPeriodoData_In ) ;
            AV49FiltroPeriodoData_Fim = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime2, 86400*(179)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
         }
         else if ( AV47FiltroPeriodo == 8 )
         {
            AV48FiltroPeriodoData_In = DateTimeUtil.ServerDate( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            AV48FiltroPeriodoData_In = context.localUtil.YMDToD( DateTimeUtil.Year( AV48FiltroPeriodoData_In), 1, 1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            AV49FiltroPeriodoData_Fim = context.localUtil.YMDToD( DateTimeUtil.Year( AV48FiltroPeriodoData_In), 12, 31);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
         }
         else if ( AV47FiltroPeriodo == 9 )
         {
            AV48FiltroPeriodoData_In = DateTimeUtil.ServerDate( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            AV48FiltroPeriodoData_In = context.localUtil.YMDToD( DateTimeUtil.Year( AV48FiltroPeriodoData_In), DateTimeUtil.Month( AV48FiltroPeriodoData_In)-1, 1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            AV49FiltroPeriodoData_Fim = DateTimeUtil.DateEndOfMonth( AV48FiltroPeriodoData_In);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
         }
         else if ( AV47FiltroPeriodo == 10 )
         {
            AV49FiltroPeriodoData_Fim = DateTimeUtil.ServerDate( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
            GXt_dtime2 = DateTimeUtil.ResetTime( AV49FiltroPeriodoData_Fim ) ;
            AV48FiltroPeriodoData_In = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime2, 86400*(-89)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
         }
         else if ( AV47FiltroPeriodo == 11 )
         {
            AV49FiltroPeriodoData_Fim = DateTimeUtil.ServerDate( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
            GXt_dtime2 = DateTimeUtil.ResetTime( AV49FiltroPeriodoData_Fim ) ;
            AV48FiltroPeriodoData_In = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime2, 86400*(-179)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
         }
         else if ( AV47FiltroPeriodo == 12 )
         {
            AV48FiltroPeriodoData_In = DateTimeUtil.ServerDate( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            AV48FiltroPeriodoData_In = context.localUtil.YMDToD( DateTimeUtil.Year( AV48FiltroPeriodoData_In)-1, 1, 1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48FiltroPeriodoData_In", context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"));
            AV49FiltroPeriodoData_Fim = context.localUtil.YMDToD( DateTimeUtil.Year( AV48FiltroPeriodoData_In), 12, 31);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49FiltroPeriodoData_Fim", context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"));
         }
         else if ( AV47FiltroPeriodo == 13 )
         {
            lblTextblockfiltroperiododata_fim_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockfiltroperiododata_fim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockfiltroperiododata_fim_Visible), 5, 0)));
            edtavFiltroperiododata_in_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFiltroperiododata_in_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFiltroperiododata_in_Visible), 5, 0)));
            edtavFiltroperiododata_fim_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFiltroperiododata_fim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFiltroperiododata_fim_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'CARREGA.FILTRO.STATUS.GRAFICO1' Routine */
         AV56FiltroStatusDemandaCollection.Clear();
         if ( StringUtil.StrCmp(AV46FiltroStatusDemanda1, "Y") == 0 )
         {
            AV56FiltroStatusDemandaCollection.Add("B", 0);
            AV56FiltroStatusDemandaCollection.Add("J", 0);
            AV56FiltroStatusDemandaCollection.Add("T", 0);
            AV56FiltroStatusDemandaCollection.Add("Q", 0);
            AV56FiltroStatusDemandaCollection.Add("M", 0);
            AV56FiltroStatusDemandaCollection.Add("U", 0);
         }
         else if ( StringUtil.StrCmp(AV46FiltroStatusDemanda1, "F") == 0 )
         {
            AV56FiltroStatusDemandaCollection.Add("S", 0);
            AV56FiltroStatusDemandaCollection.Add("E", 0);
            AV56FiltroStatusDemandaCollection.Add("A", 0);
            AV56FiltroStatusDemandaCollection.Add("C", 0);
            AV56FiltroStatusDemandaCollection.Add("D", 0);
         }
         else if ( StringUtil.StrCmp(AV46FiltroStatusDemanda1, "Z") == 0 )
         {
            AV56FiltroStatusDemandaCollection.Add("R", 0);
            AV56FiltroStatusDemandaCollection.Add("H", 0);
            AV56FiltroStatusDemandaCollection.Add("O", 0);
            AV56FiltroStatusDemandaCollection.Add("P", 0);
            AV56FiltroStatusDemandaCollection.Add("L", 0);
            AV56FiltroStatusDemandaCollection.Add("X", 0);
            AV56FiltroStatusDemandaCollection.Add("N", 0);
         }
         else if ( ( StringUtil.StrCmp(AV46FiltroStatusDemanda1, "Y") != 0 ) && ( StringUtil.StrCmp(AV46FiltroStatusDemanda1, "F") != 0 ) && ( StringUtil.StrCmp(AV46FiltroStatusDemanda1, "Z") != 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV46FiltroStatusDemanda1)) )
         {
            AV56FiltroStatusDemandaCollection.Add(AV46FiltroStatusDemanda1, 0);
         }
         else
         {
            AV56FiltroStatusDemandaCollection.Add("P", 0);
            AV56FiltroStatusDemandaCollection.Add("O", 0);
            AV56FiltroStatusDemandaCollection.Add("I", 0);
            AV56FiltroStatusDemandaCollection.Add("X", 0);
            AV56FiltroStatusDemandaCollection.Add("C", 0);
            AV56FiltroStatusDemandaCollection.Add("E", 0);
            AV56FiltroStatusDemandaCollection.Add("A", 0);
            AV56FiltroStatusDemandaCollection.Add("H", 0);
            AV56FiltroStatusDemandaCollection.Add("L", 0);
            AV56FiltroStatusDemandaCollection.Add("N", 0);
            AV56FiltroStatusDemandaCollection.Add("J", 0);
            AV56FiltroStatusDemandaCollection.Add("U", 0);
            AV56FiltroStatusDemandaCollection.Add("D", 0);
            AV56FiltroStatusDemandaCollection.Add("R", 0);
            AV56FiltroStatusDemandaCollection.Add("S", 0);
            AV56FiltroStatusDemandaCollection.Add("B", 0);
            AV56FiltroStatusDemandaCollection.Add("T", 0);
            AV56FiltroStatusDemandaCollection.Add("Q", 0);
            AV56FiltroStatusDemandaCollection.Add("M", 0);
         }
      }

      protected void S142( )
      {
         /* 'CARREGA.FILTRO.STATUS.GRAFICO2' Routine */
         AV56FiltroStatusDemandaCollection.Clear();
         if ( StringUtil.StrCmp(AV50FiltroStatusDemanda2, "Y") == 0 )
         {
            AV56FiltroStatusDemandaCollection.Add("B", 0);
            AV56FiltroStatusDemandaCollection.Add("J", 0);
            AV56FiltroStatusDemandaCollection.Add("T", 0);
            AV56FiltroStatusDemandaCollection.Add("Q", 0);
            AV56FiltroStatusDemandaCollection.Add("M", 0);
            AV56FiltroStatusDemandaCollection.Add("U", 0);
         }
         else if ( StringUtil.StrCmp(AV50FiltroStatusDemanda2, "F") == 0 )
         {
            AV56FiltroStatusDemandaCollection.Add("S", 0);
            AV56FiltroStatusDemandaCollection.Add("E", 0);
            AV56FiltroStatusDemandaCollection.Add("A", 0);
            AV56FiltroStatusDemandaCollection.Add("C", 0);
            AV56FiltroStatusDemandaCollection.Add("D", 0);
         }
         else if ( StringUtil.StrCmp(AV50FiltroStatusDemanda2, "Z") == 0 )
         {
            AV56FiltroStatusDemandaCollection.Add("R", 0);
            AV56FiltroStatusDemandaCollection.Add("H", 0);
            AV56FiltroStatusDemandaCollection.Add("O", 0);
            AV56FiltroStatusDemandaCollection.Add("P", 0);
            AV56FiltroStatusDemandaCollection.Add("L", 0);
            AV56FiltroStatusDemandaCollection.Add("X", 0);
            AV56FiltroStatusDemandaCollection.Add("N", 0);
         }
         else if ( ( StringUtil.StrCmp(AV50FiltroStatusDemanda2, "Y") != 0 ) && ( StringUtil.StrCmp(AV50FiltroStatusDemanda2, "F") != 0 ) && ( StringUtil.StrCmp(AV50FiltroStatusDemanda2, "Z") != 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV50FiltroStatusDemanda2)) )
         {
            AV56FiltroStatusDemandaCollection.Add(AV50FiltroStatusDemanda2, 0);
         }
         else
         {
            AV56FiltroStatusDemandaCollection.Add("P", 0);
            AV56FiltroStatusDemandaCollection.Add("O", 0);
            AV56FiltroStatusDemandaCollection.Add("I", 0);
            AV56FiltroStatusDemandaCollection.Add("X", 0);
            AV56FiltroStatusDemandaCollection.Add("C", 0);
            AV56FiltroStatusDemandaCollection.Add("E", 0);
            AV56FiltroStatusDemandaCollection.Add("A", 0);
            AV56FiltroStatusDemandaCollection.Add("H", 0);
            AV56FiltroStatusDemandaCollection.Add("L", 0);
            AV56FiltroStatusDemandaCollection.Add("N", 0);
            AV56FiltroStatusDemandaCollection.Add("J", 0);
            AV56FiltroStatusDemandaCollection.Add("U", 0);
            AV56FiltroStatusDemandaCollection.Add("D", 0);
            AV56FiltroStatusDemandaCollection.Add("R", 0);
            AV56FiltroStatusDemandaCollection.Add("S", 0);
            AV56FiltroStatusDemandaCollection.Add("B", 0);
            AV56FiltroStatusDemandaCollection.Add("T", 0);
            AV56FiltroStatusDemandaCollection.Add("Q", 0);
            AV56FiltroStatusDemandaCollection.Add("M", 0);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E16SS2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_SS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEHEADERContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEHEADERContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table2_10_SS2( true) ;
         }
         else
         {
            wb_table2_10_SS2( false) ;
         }
         return  ;
      }

      protected void wb_table2_10_SS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_64_SS2( true) ;
         }
         else
         {
            wb_table3_64_SS2( false) ;
         }
         return  ;
      }

      protected void wb_table3_64_SS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayout_tablegraficos_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTablegraficos_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-6", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divHtml_dvpanel_tablegrafico1_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEGRAFICO1Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEGRAFICO1Container"+"Body"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayout_tablegrafico1_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            wb_table4_79_SS2( true) ;
         }
         else
         {
            wb_table4_79_SS2( false) ;
         }
         return  ;
      }

      protected void wb_table4_79_SS2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-6", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divHtml_dvpanel_tablegrafico2_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEGRAFICO2Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEGRAFICO2Container"+"Body"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayout_tablegrafico2_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            wb_table5_89_SS2( true) ;
         }
         else
         {
            wb_table5_89_SS2( false) ;
         }
         return  ;
      }

      protected void wb_table5_89_SS2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"UCMENSAGEMContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_SS2e( true) ;
         }
         else
         {
            wb_table1_2_SS2e( false) ;
         }
      }

      protected void wb_table5_89_SS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegrafico2_Internalname, tblTablegrafico2_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"Center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-Center;text-align:-moz-Center;text-align:-webkit-Center")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"USQUERYRESPONSABILIDADEContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_89_SS2e( true) ;
         }
         else
         {
            wb_table5_89_SS2e( false) ;
         }
      }

      protected void wb_table4_79_SS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegrafico1_Internalname, tblTablegrafico1_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"Center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-Center;text-align:-moz-Center;text-align:-webkit-Center")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"USQUERYRESULTADOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_79_SS2e( true) ;
         }
         else
         {
            wb_table4_79_SS2e( false) ;
         }
      }

      protected void wb_table3_64_SS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_64_SS2e( true) ;
         }
         else
         {
            wb_table3_64_SS2e( false) ;
         }
      }

      protected void wb_table2_10_SS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:80%")+"\">") ;
            wb_table6_13_SS2( true) ;
         }
         else
         {
            wb_table6_13_SS2( false) ;
         }
         return  ;
      }

      protected void wb_table6_13_SS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:20%")+"\">") ;
            wb_table7_56_SS2( true) ;
         }
         else
         {
            wb_table7_56_SS2( false) ;
         }
         return  ;
      }

      protected void wb_table7_56_SS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_10_SS2e( true) ;
         }
         else
         {
            wb_table2_10_SS2e( false) ;
         }
      }

      protected void wb_table7_56_SS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters2_Internalname, tblTablefilters2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfiltrostatusdemanda2_Internalname, "Status", "", "", lblTextblockfiltrostatusdemanda2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DashboardVs2.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltrostatusdemanda2, cmbavFiltrostatusdemanda2_Internalname, StringUtil.RTrim( AV50FiltroStatusDemanda2), 1, cmbavFiltrostatusdemanda2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVFILTROSTATUSDEMANDA2.CLICK."+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_WP_DashboardVs2.htm");
            cmbavFiltrostatusdemanda2.CurrentValue = StringUtil.RTrim( AV50FiltroStatusDemanda2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltrostatusdemanda2_Internalname, "Values", (String)(cmbavFiltrostatusdemanda2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_56_SS2e( true) ;
         }
         else
         {
            wb_table7_56_SS2e( false) ;
         }
      }

      protected void wb_table6_13_SS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions1_Internalname, tblTableactions1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_codigo_Internalname, "�rea de Trabalho", "", "", lblTextblockareatrabalho_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DashboardVs2.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavAreatrabalho_codigo, dynavAreatrabalho_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV43AreaTrabalho_Codigo), 6, 0)), 1, dynavAreatrabalho_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WP_DashboardVs2.htm");
            dynavAreatrabalho_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV43AreaTrabalho_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo_Internalname, "Values", (String)(dynavAreatrabalho_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcodigocontrato_Internalname, "Contrato", "", "", lblTextblockcodigocontrato_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DashboardVs2.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavCodigocontrato, dynavCodigocontrato_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV39CodigoContrato), 6, 0)), 1, dynavCodigocontrato_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_WP_DashboardVs2.htm");
            dynavCodigocontrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39CodigoContrato), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCodigocontrato_Internalname, "Values", (String)(dynavCodigocontrato.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfiltrostatusdemanda1_Internalname, "Status", "", "", lblTextblockfiltrostatusdemanda1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DashboardVs2.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltrostatusdemanda1, cmbavFiltrostatusdemanda1_Internalname, StringUtil.RTrim( AV46FiltroStatusDemanda1), 1, cmbavFiltrostatusdemanda1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_WP_DashboardVs2.htm");
            cmbavFiltrostatusdemanda1.CurrentValue = StringUtil.RTrim( AV46FiltroStatusDemanda1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltrostatusdemanda1_Internalname, "Values", (String)(cmbavFiltrostatusdemanda1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfiltrotipodata_Internalname, "Tipo de Data", "", "", lblTextblockfiltrotipodata_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DashboardVs2.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltrotipodata, cmbavFiltrotipodata_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV45FiltroTipoData), 4, 0)), 1, cmbavFiltrotipodata_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WP_DashboardVs2.htm");
            cmbavFiltrotipodata.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV45FiltroTipoData), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltrotipodata_Internalname, "Values", (String)(cmbavFiltrotipodata.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfiltroperiodo_Internalname, "Per�odo", "", "", lblTextblockfiltroperiodo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DashboardVs2.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltroperiodo, cmbavFiltroperiodo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV47FiltroPeriodo), 4, 0)), 1, cmbavFiltroperiodo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_WP_DashboardVs2.htm");
            cmbavFiltroperiodo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV47FiltroPeriodo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltroperiodo_Internalname, "Values", (String)(cmbavFiltroperiodo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfiltroperiododata_in_Internalname, "", "", "", lblTextblockfiltroperiododata_in_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DashboardVs2.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table8_43_SS2( true) ;
         }
         else
         {
            wb_table8_43_SS2( false) ;
         }
         return  ;
      }

      protected void wb_table8_43_SS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbntconsultarprincipal_Internalname, "", "Consultar", bttBtnbntconsultarprincipal_Jsonclick, 5, "Consultar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOBNTCONSULTARPRINCIPAL\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_DashboardVs2.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_13_SS2e( true) ;
         }
         else
         {
            wb_table6_13_SS2e( false) ;
         }
      }

      protected void wb_table8_43_SS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedfiltroperiododata_in_Internalname, tblTablemergedfiltroperiododata_in_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFiltroperiododata_in_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFiltroperiododata_in_Internalname, context.localUtil.Format(AV48FiltroPeriodoData_In, "99/99/9999"), context.localUtil.Format( AV48FiltroPeriodoData_In, "99/99/9999"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltroperiododata_in_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", edtavFiltroperiododata_in_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_DashboardVs2.htm");
            GxWebStd.gx_bitmap( context, edtavFiltroperiododata_in_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavFiltroperiododata_in_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_DashboardVs2.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfiltroperiododata_fim_Internalname, "At�", "", "", lblTextblockfiltroperiododata_fim_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockfiltroperiododata_fim_Visible, 1, 0, "HLP_WP_DashboardVs2.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFiltroperiododata_fim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFiltroperiododata_fim_Internalname, context.localUtil.Format(AV49FiltroPeriodoData_Fim, "99/99/9999"), context.localUtil.Format( AV49FiltroPeriodoData_Fim, "99/99/9999"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltroperiododata_fim_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", edtavFiltroperiododata_fim_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_DashboardVs2.htm");
            GxWebStd.gx_bitmap( context, edtavFiltroperiododata_fim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavFiltroperiododata_fim_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_DashboardVs2.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_43_SS2e( true) ;
         }
         else
         {
            wb_table8_43_SS2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PASS2( ) ;
         WSSS2( ) ;
         WESS2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("QueryViewer/QueryViewer.css", "?10720");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("QueryViewer/QueryViewer.css", "?10720");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051813454947");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_dashboardvs2.js", "?202051813454947");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("QueryViewer/xpertPivot/swf.js", "");
         context.AddJavascriptSource("QueryViewer/FusionCharts/FusionCharts.js", "");
         context.AddJavascriptSource("QueryViewer/Flash/AC_OETags.js", "");
         context.AddJavascriptSource("QueryViewer/js/highcharts.js", "");
         context.AddJavascriptSource("QueryViewer/js/modules/funnel.js", "");
         context.AddJavascriptSource("QueryViewer/oatPivot/gxpivotjs.js", "");
         context.AddJavascriptSource("QueryViewer/QueryViewerRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("QueryViewer/xpertPivot/swf.js", "");
         context.AddJavascriptSource("QueryViewer/FusionCharts/FusionCharts.js", "");
         context.AddJavascriptSource("QueryViewer/Flash/AC_OETags.js", "");
         context.AddJavascriptSource("QueryViewer/js/highcharts.js", "");
         context.AddJavascriptSource("QueryViewer/js/modules/funnel.js", "");
         context.AddJavascriptSource("QueryViewer/oatPivot/gxpivotjs.js", "");
         context.AddJavascriptSource("QueryViewer/QueryViewerRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockareatrabalho_codigo_Internalname = "TEXTBLOCKAREATRABALHO_CODIGO";
         dynavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO";
         lblTextblockcodigocontrato_Internalname = "TEXTBLOCKCODIGOCONTRATO";
         dynavCodigocontrato_Internalname = "vCODIGOCONTRATO";
         lblTextblockfiltrostatusdemanda1_Internalname = "TEXTBLOCKFILTROSTATUSDEMANDA1";
         cmbavFiltrostatusdemanda1_Internalname = "vFILTROSTATUSDEMANDA1";
         lblTextblockfiltrotipodata_Internalname = "TEXTBLOCKFILTROTIPODATA";
         cmbavFiltrotipodata_Internalname = "vFILTROTIPODATA";
         lblTextblockfiltroperiodo_Internalname = "TEXTBLOCKFILTROPERIODO";
         cmbavFiltroperiodo_Internalname = "vFILTROPERIODO";
         lblTextblockfiltroperiododata_in_Internalname = "TEXTBLOCKFILTROPERIODODATA_IN";
         edtavFiltroperiododata_in_Internalname = "vFILTROPERIODODATA_IN";
         lblTextblockfiltroperiododata_fim_Internalname = "TEXTBLOCKFILTROPERIODODATA_FIM";
         edtavFiltroperiododata_fim_Internalname = "vFILTROPERIODODATA_FIM";
         tblTablemergedfiltroperiododata_in_Internalname = "TABLEMERGEDFILTROPERIODODATA_IN";
         bttBtnbntconsultarprincipal_Internalname = "BTNBNTCONSULTARPRINCIPAL";
         tblTableactions1_Internalname = "TABLEACTIONS1";
         lblTextblockfiltrostatusdemanda2_Internalname = "TEXTBLOCKFILTROSTATUSDEMANDA2";
         cmbavFiltrostatusdemanda2_Internalname = "vFILTROSTATUSDEMANDA2";
         tblTablefilters2_Internalname = "TABLEFILTERS2";
         tblTableheader_Internalname = "TABLEHEADER";
         Dvpanel_tableheader_Internalname = "DVPANEL_TABLEHEADER";
         tblTablecontent_Internalname = "TABLECONTENT";
         Usqueryresultado_Internalname = "USQUERYRESULTADO";
         tblTablegrafico1_Internalname = "TABLEGRAFICO1";
         divTextblockfiltroperiododata_in_Internalname = "";
         divLayout_tablegrafico1_Internalname = "LAYOUT_TABLEGRAFICO1";
         Dvpanel_tablegrafico1_Internalname = "DVPANEL_TABLEGRAFICO1";
         divHtml_dvpanel_tablegrafico1_Internalname = "HTML_DVPANEL_TABLEGRAFICO1";
         divTextblockfiltroperiododata_in_Internalname = "";
         Usqueryresponsabilidade_Internalname = "USQUERYRESPONSABILIDADE";
         tblTablegrafico2_Internalname = "TABLEGRAFICO2";
         divTextblockfiltroperiododata_in_Internalname = "";
         divLayout_tablegrafico2_Internalname = "LAYOUT_TABLEGRAFICO2";
         Dvpanel_tablegrafico2_Internalname = "DVPANEL_TABLEGRAFICO2";
         divHtml_dvpanel_tablegrafico2_Internalname = "HTML_DVPANEL_TABLEGRAFICO2";
         divTextblockfiltroperiododata_in_Internalname = "";
         divTextblockfiltroperiododata_in_Internalname = "";
         divTablegraficos_Internalname = "TABLEGRAFICOS";
         divTextblockfiltroperiododata_in_Internalname = "";
         divLayout_tablegraficos_Internalname = "LAYOUT_TABLEGRAFICOS";
         Ucmensagem_Internalname = "UCMENSAGEM";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavFiltroperiododata_fim_Jsonclick = "";
         lblTextblockfiltroperiododata_fim_Visible = 1;
         edtavFiltroperiododata_in_Jsonclick = "";
         cmbavFiltroperiodo_Jsonclick = "";
         cmbavFiltrotipodata_Jsonclick = "";
         cmbavFiltrostatusdemanda1_Jsonclick = "";
         dynavCodigocontrato_Jsonclick = "";
         dynavAreatrabalho_codigo_Jsonclick = "";
         cmbavFiltrostatusdemanda2_Jsonclick = "";
         edtavFiltroperiododata_fim_Visible = 1;
         edtavFiltroperiododata_in_Visible = 1;
         Ucmensagem_Confirmtype = "";
         Ucmensagem_Yesbuttoncaption = "Fechar";
         Ucmensagem_Confirmationtext = "";
         Ucmensagem_Title = "Aten��o";
         Dvpanel_tablegrafico2_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tablegrafico2_Iconposition = "left";
         Dvpanel_tablegrafico2_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tablegrafico2_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tablegrafico2_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tablegrafico2_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tablegrafico2_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_tablegrafico2_Title = "Rsponsabilidade: Situa��o das demandas";
         Dvpanel_tablegrafico2_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tablegrafico2_Width = "100%";
         Usqueryresponsabilidade_Rememberlayout = Convert.ToBoolean( 0);
         Usqueryresponsabilidade_Preferredrendererforqueryviewercharts = "htmlandjs";
         Usqueryresponsabilidade_Charttype = "Pie";
         Usqueryresponsabilidade_Fontsize = 12;
         Usqueryresponsabilidade_Objectname = "QR_WP_DashboardResposabilidade";
         Usqueryresponsabilidade_Type = "Chart";
         Dvpanel_tablegrafico1_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tablegrafico1_Iconposition = "left";
         Dvpanel_tablegrafico1_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tablegrafico1_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tablegrafico1_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tablegrafico1_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tablegrafico1_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_tablegrafico1_Title = "Resultado";
         Dvpanel_tablegrafico1_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tablegrafico1_Width = "100%";
         Usqueryresultado_Rememberlayout = Convert.ToBoolean( 0);
         Usqueryresultado_Preferredrendererforqueryviewercharts = "htmlandjs";
         Usqueryresultado_Charttype = "Pie";
         Usqueryresultado_Fontsize = 12;
         Usqueryresultado_Objectname = "QR_WP_DashboardResutado";
         Usqueryresultado_Type = "Chart";
         Dvpanel_tableheader_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableheader_Iconposition = "left";
         Dvpanel_tableheader_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableheader_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableheader_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableheader_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableheader_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_tableheader_Title = "Filtros";
         Dvpanel_tableheader_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableheader_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Dashboard";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Areatrabalho_codigo( GXCombobox dynGX_Parm1 ,
                                              int GX_Parm2 ,
                                              GXCombobox dynGX_Parm3 )
      {
         dynavAreatrabalho_codigo = dynGX_Parm1;
         AV43AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.CurrentValue, "."));
         AV42UserId = GX_Parm2;
         dynavCodigocontrato = dynGX_Parm3;
         AV39CodigoContrato = (int)(NumberUtil.Val( dynavCodigocontrato.CurrentValue, "."));
         GXVvCODIGOCONTRATO_htmlSS2( AV43AreaTrabalho_Codigo, AV42UserId) ;
         dynload_actions( ) ;
         if ( dynavCodigocontrato.ItemCount > 0 )
         {
            AV39CodigoContrato = (int)(NumberUtil.Val( dynavCodigocontrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV39CodigoContrato), 6, 0))), "."));
         }
         dynavCodigocontrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39CodigoContrato), 6, 0));
         isValidOutput.Add(dynavCodigocontrato);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOBNTCONSULTARPRINCIPAL'","{handler:'E13SS2',iparms:[{av:'AV25IsFiltrosValidos',fld:'vISFILTROSVALIDOS',pic:'',nv:false},{av:'AV43AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV39CodigoContrato',fld:'vCODIGOCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV56FiltroStatusDemandaCollection',fld:'vFILTROSTATUSDEMANDACOLLECTION',pic:'',nv:null},{av:'AV45FiltroTipoData',fld:'vFILTROTIPODATA',pic:'ZZZ9',nv:0},{av:'AV48FiltroPeriodoData_In',fld:'vFILTROPERIODODATA_IN',pic:'',nv:''},{av:'AV49FiltroPeriodoData_Fim',fld:'vFILTROPERIODODATA_FIM',pic:'',nv:''},{av:'AV47FiltroPeriodo',fld:'vFILTROPERIODO',pic:'ZZZ9',nv:0},{av:'AV46FiltroStatusDemanda1',fld:'vFILTROSTATUSDEMANDA1',pic:'',nv:''},{av:'AV50FiltroStatusDemanda2',fld:'vFILTROSTATUSDEMANDA2',pic:'',nv:''},{av:'AV51Data_Aux',fld:'vDATA_AUX',pic:'',nv:''}],oparms:[{av:'AV33Parameters',fld:'vPARAMETERS',pic:'',nv:null},{av:'AV57Parameters2',fld:'vPARAMETERS2',pic:'',nv:null},{av:'AV25IsFiltrosValidos',fld:'vISFILTROSVALIDOS',pic:'',nv:false},{av:'AV56FiltroStatusDemandaCollection',fld:'vFILTROSTATUSDEMANDACOLLECTION',pic:'',nv:null},{av:'AV48FiltroPeriodoData_In',fld:'vFILTROPERIODODATA_IN',pic:'',nv:''},{av:'AV49FiltroPeriodoData_Fim',fld:'vFILTROPERIODODATA_FIM',pic:'',nv:''},{av:'lblTextblockfiltroperiododata_fim_Visible',ctrl:'TEXTBLOCKFILTROPERIODODATA_FIM',prop:'Visible'},{av:'edtavFiltroperiododata_in_Visible',ctrl:'vFILTROPERIODODATA_IN',prop:'Visible'},{av:'edtavFiltroperiododata_fim_Visible',ctrl:'vFILTROPERIODODATA_FIM',prop:'Visible'},{av:'AV51Data_Aux',fld:'vDATA_AUX',pic:'',nv:''}]}");
         setEventMetadata("UCMENSAGEM.CLOSE","{handler:'E11SS2',iparms:[],oparms:[]}");
         setEventMetadata("VFILTROSTATUSDEMANDA2.CLICK","{handler:'E15SS2',iparms:[{av:'AV57Parameters2',fld:'vPARAMETERS2',pic:'',nv:null},{av:'AV56FiltroStatusDemandaCollection',fld:'vFILTROSTATUSDEMANDACOLLECTION',pic:'',nv:null},{av:'AV50FiltroStatusDemanda2',fld:'vFILTROSTATUSDEMANDA2',pic:'',nv:''}],oparms:[{av:'AV57Parameters2',fld:'vPARAMETERS2',pic:'',nv:null},{av:'AV56FiltroStatusDemandaCollection',fld:'vFILTROSTATUSDEMANDACOLLECTION',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV19DragAndDropData = new SdtQueryViewerDragAndDropData(context);
         AV29ItemExpandData = new SdtQueryViewerItemExpandData(context);
         AV27ItemCollapseData = new SdtQueryViewerItemCollapseData(context);
         AV20FilterChangedData = new SdtQueryViewerFilterChangedData(context);
         AV6AggregationChangedData = new SdtQueryViewerAggregationChangedData(context);
         AV26ItemClickData = new SdtQueryViewerItemClickData(context);
         AV28ItemDoubleClickData = new SdtQueryViewerItemDoubleClickData(context);
         AV7Axes = new GxObjectCollection( context, "QueryViewerAxes.Axis", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerAxes_Axis", "GeneXus.Programs");
         AV33Parameters = new GxObjectCollection( context, "QueryViewerParameters.Parameter", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerParameters_Parameter", "GeneXus.Programs");
         AV57Parameters2 = new GxObjectCollection( context, "QueryViewerParameters.Parameter", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerParameters_Parameter", "GeneXus.Programs");
         AV35WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV56FiltroStatusDemandaCollection = new GxSimpleCollection();
         AV51Data_Aux = DateTime.MinValue;
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV46FiltroStatusDemanda1 = "";
         AV45FiltroTipoData = 1;
         AV50FiltroStatusDemanda2 = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         AV48FiltroPeriodoData_In = DateTime.MinValue;
         AV49FiltroPeriodoData_Fim = DateTime.MinValue;
         AV54AreaTrabalho_CodigoCollection = new GxSimpleCollection();
         AV32Parameter = new SdtQueryViewerParameters_Parameter(context);
         AV53AreaTrabalhoCollection = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs");
         GXt_objcol_SdtSDT_Codigos1 = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs");
         AV55AreaTrabalhoItens = new SdtSDT_Codigos(context);
         AV18Context = new wwpbaseobjects.SdtWWPContext(context);
         GXt_dtime2 = (DateTime)(DateTime.MinValue);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblockfiltrostatusdemanda2_Jsonclick = "";
         TempTags = "";
         lblTextblockareatrabalho_codigo_Jsonclick = "";
         lblTextblockcodigocontrato_Jsonclick = "";
         lblTextblockfiltrostatusdemanda1_Jsonclick = "";
         lblTextblockfiltrotipodata_Jsonclick = "";
         lblTextblockfiltroperiodo_Jsonclick = "";
         lblTextblockfiltroperiododata_in_Jsonclick = "";
         bttBtnbntconsultarprincipal_Jsonclick = "";
         lblTextblockfiltroperiododata_fim_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         isValidOutput = new GxUnknownObjectCollection();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV45FiltroTipoData ;
      private short AV47FiltroPeriodo ;
      private short AV58item ;
      private short nGXWrapped ;
      private short wbTemp ;
      private int AV40Contratada_Codigo ;
      private int AV42UserId ;
      private int AV43AreaTrabalho_Codigo ;
      private int Usqueryresultado_Fontsize ;
      private int Usqueryresponsabilidade_Fontsize ;
      private int gxdynajaxindex ;
      private int AV39CodigoContrato ;
      private int AV41Contratante_Codigo ;
      private int AV38ContratoGestor_ContratadaAreaCod ;
      private int AV37ContratoGestor_UsuarioCod ;
      private int AV61GXV1 ;
      private int AV62GXV2 ;
      private int lblTextblockfiltroperiododata_fim_Visible ;
      private int edtavFiltroperiododata_in_Visible ;
      private int edtavFiltroperiododata_fim_Visible ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableheader_Width ;
      private String Dvpanel_tableheader_Cls ;
      private String Dvpanel_tableheader_Title ;
      private String Dvpanel_tableheader_Iconposition ;
      private String Usqueryresultado_Type ;
      private String Usqueryresultado_Objectname ;
      private String Usqueryresultado_Charttype ;
      private String Usqueryresultado_Preferredrendererforqueryviewercharts ;
      private String Dvpanel_tablegrafico1_Width ;
      private String Dvpanel_tablegrafico1_Cls ;
      private String Dvpanel_tablegrafico1_Title ;
      private String Dvpanel_tablegrafico1_Iconposition ;
      private String Usqueryresponsabilidade_Type ;
      private String Usqueryresponsabilidade_Objectname ;
      private String Usqueryresponsabilidade_Charttype ;
      private String Usqueryresponsabilidade_Preferredrendererforqueryviewercharts ;
      private String Dvpanel_tablegrafico2_Width ;
      private String Dvpanel_tablegrafico2_Cls ;
      private String Dvpanel_tablegrafico2_Title ;
      private String Dvpanel_tablegrafico2_Iconposition ;
      private String Ucmensagem_Title ;
      private String Ucmensagem_Confirmationtext ;
      private String Ucmensagem_Yesbuttoncaption ;
      private String Ucmensagem_Confirmtype ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV46FiltroStatusDemanda1 ;
      private String AV50FiltroStatusDemanda2 ;
      private String dynavAreatrabalho_codigo_Internalname ;
      private String gxwrpcisep ;
      private String dynavCodigocontrato_Internalname ;
      private String cmbavFiltrostatusdemanda1_Internalname ;
      private String cmbavFiltrotipodata_Internalname ;
      private String cmbavFiltroperiodo_Internalname ;
      private String edtavFiltroperiododata_in_Internalname ;
      private String edtavFiltroperiododata_fim_Internalname ;
      private String cmbavFiltrostatusdemanda2_Internalname ;
      private String Ucmensagem_Internalname ;
      private String lblTextblockfiltroperiododata_fim_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String divLayout_tablegraficos_Internalname ;
      private String divTablegraficos_Internalname ;
      private String divHtml_dvpanel_tablegrafico1_Internalname ;
      private String divLayout_tablegrafico1_Internalname ;
      private String divHtml_dvpanel_tablegrafico2_Internalname ;
      private String divLayout_tablegrafico2_Internalname ;
      private String tblTablegrafico2_Internalname ;
      private String tblTablegrafico1_Internalname ;
      private String tblTablecontent_Internalname ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters2_Internalname ;
      private String lblTextblockfiltrostatusdemanda2_Internalname ;
      private String lblTextblockfiltrostatusdemanda2_Jsonclick ;
      private String TempTags ;
      private String cmbavFiltrostatusdemanda2_Jsonclick ;
      private String tblTableactions1_Internalname ;
      private String lblTextblockareatrabalho_codigo_Internalname ;
      private String lblTextblockareatrabalho_codigo_Jsonclick ;
      private String dynavAreatrabalho_codigo_Jsonclick ;
      private String lblTextblockcodigocontrato_Internalname ;
      private String lblTextblockcodigocontrato_Jsonclick ;
      private String dynavCodigocontrato_Jsonclick ;
      private String lblTextblockfiltrostatusdemanda1_Internalname ;
      private String lblTextblockfiltrostatusdemanda1_Jsonclick ;
      private String cmbavFiltrostatusdemanda1_Jsonclick ;
      private String lblTextblockfiltrotipodata_Internalname ;
      private String lblTextblockfiltrotipodata_Jsonclick ;
      private String cmbavFiltrotipodata_Jsonclick ;
      private String lblTextblockfiltroperiodo_Internalname ;
      private String lblTextblockfiltroperiodo_Jsonclick ;
      private String cmbavFiltroperiodo_Jsonclick ;
      private String lblTextblockfiltroperiododata_in_Internalname ;
      private String lblTextblockfiltroperiododata_in_Jsonclick ;
      private String bttBtnbntconsultarprincipal_Internalname ;
      private String bttBtnbntconsultarprincipal_Jsonclick ;
      private String tblTablemergedfiltroperiododata_in_Internalname ;
      private String edtavFiltroperiododata_in_Jsonclick ;
      private String lblTextblockfiltroperiododata_fim_Jsonclick ;
      private String edtavFiltroperiododata_fim_Jsonclick ;
      private String Dvpanel_tableheader_Internalname ;
      private String Usqueryresultado_Internalname ;
      private String divTextblockfiltroperiododata_in_Internalname ;
      private String Dvpanel_tablegrafico1_Internalname ;
      private String Usqueryresponsabilidade_Internalname ;
      private String Dvpanel_tablegrafico2_Internalname ;
      private DateTime GXt_dtime2 ;
      private DateTime AV51Data_Aux ;
      private DateTime AV48FiltroPeriodoData_In ;
      private DateTime AV49FiltroPeriodoData_Fim ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV25IsFiltrosValidos ;
      private bool Dvpanel_tableheader_Collapsible ;
      private bool Dvpanel_tableheader_Collapsed ;
      private bool Dvpanel_tableheader_Autowidth ;
      private bool Dvpanel_tableheader_Autoheight ;
      private bool Dvpanel_tableheader_Showcollapseicon ;
      private bool Dvpanel_tableheader_Autoscroll ;
      private bool Usqueryresultado_Rememberlayout ;
      private bool Dvpanel_tablegrafico1_Collapsible ;
      private bool Dvpanel_tablegrafico1_Collapsed ;
      private bool Dvpanel_tablegrafico1_Autowidth ;
      private bool Dvpanel_tablegrafico1_Autoheight ;
      private bool Dvpanel_tablegrafico1_Showcollapseicon ;
      private bool Dvpanel_tablegrafico1_Autoscroll ;
      private bool Usqueryresponsabilidade_Rememberlayout ;
      private bool Dvpanel_tablegrafico2_Collapsible ;
      private bool Dvpanel_tablegrafico2_Collapsed ;
      private bool Dvpanel_tablegrafico2_Autowidth ;
      private bool Dvpanel_tablegrafico2_Autoheight ;
      private bool Dvpanel_tablegrafico2_Showcollapseicon ;
      private bool Dvpanel_tablegrafico2_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavAreatrabalho_codigo ;
      private GXCombobox dynavCodigocontrato ;
      private GXCombobox cmbavFiltrostatusdemanda1 ;
      private GXCombobox cmbavFiltrotipodata ;
      private GXCombobox cmbavFiltroperiodo ;
      private GXCombobox cmbavFiltrostatusdemanda2 ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV54AreaTrabalho_CodigoCollection ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV56FiltroStatusDemandaCollection ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection AV53AreaTrabalhoCollection ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection GXt_objcol_SdtSDT_Codigos1 ;
      [ObjectCollection(ItemType=typeof( SdtQueryViewerAxes_Axis ))]
      private IGxCollection AV7Axes ;
      [ObjectCollection(ItemType=typeof( SdtQueryViewerParameters_Parameter ))]
      private IGxCollection AV33Parameters ;
      [ObjectCollection(ItemType=typeof( SdtQueryViewerParameters_Parameter ))]
      private IGxCollection AV57Parameters2 ;
      private GXWebForm Form ;
      private SdtQueryViewerAggregationChangedData AV6AggregationChangedData ;
      private SdtSDT_Codigos AV55AreaTrabalhoItens ;
      private wwpbaseobjects.SdtWWPContext AV35WWPContext ;
      private wwpbaseobjects.SdtWWPContext AV18Context ;
      private SdtQueryViewerDragAndDropData AV19DragAndDropData ;
      private SdtQueryViewerFilterChangedData AV20FilterChangedData ;
      private SdtQueryViewerItemClickData AV26ItemClickData ;
      private SdtQueryViewerItemCollapseData AV27ItemCollapseData ;
      private SdtQueryViewerItemDoubleClickData AV28ItemDoubleClickData ;
      private SdtQueryViewerItemExpandData AV29ItemExpandData ;
      private SdtQueryViewerParameters_Parameter AV32Parameter ;
   }

}
