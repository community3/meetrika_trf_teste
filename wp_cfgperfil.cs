/*
               File: WP_CfgPerfil
        Description:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:24:17.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_cfgperfil : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_cfgperfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_cfgperfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo ,
                           String aP1_ContratadaUsuario_UsuarioPessoaNom )
      {
         this.AV13Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV15ContratadaUsuario_UsuarioPessoaNom = aP1_ContratadaUsuario_UsuarioPessoaNom;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavUsuarioperfil_insert = new GXCheckbox();
         chkavUsuarioperfil_update = new GXCheckbox();
         chkavUsuarioperfil_delete = new GXCheckbox();
         chkavUsuarioperfil_display = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_12 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_12_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_12_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A7Perfil_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A7Perfil_AreaTrabalhoCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV11WWPContext);
               A3Perfil_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
               A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               AV13Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Usuario_Codigo), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV14Perfis);
               A659UsuarioPerfil_Update = (bool)(BooleanUtil.Val(GetNextPar( )));
               n659UsuarioPerfil_Update = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A659UsuarioPerfil_Update", A659UsuarioPerfil_Update);
               A544UsuarioPerfil_Insert = (bool)(BooleanUtil.Val(GetNextPar( )));
               n544UsuarioPerfil_Insert = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A544UsuarioPerfil_Insert", A544UsuarioPerfil_Insert);
               A546UsuarioPerfil_Delete = (bool)(BooleanUtil.Val(GetNextPar( )));
               n546UsuarioPerfil_Delete = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A546UsuarioPerfil_Delete", A546UsuarioPerfil_Delete);
               A543UsuarioPerfil_Display = (bool)(BooleanUtil.Val(GetNextPar( )));
               n543UsuarioPerfil_Display = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A543UsuarioPerfil_Display", A543UsuarioPerfil_Display);
               A4Perfil_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, A7Perfil_AreaTrabalhoCod, AV11WWPContext, A3Perfil_Codigo, A1Usuario_Codigo, AV13Usuario_Codigo, AV14Perfis, A659UsuarioPerfil_Update, A544UsuarioPerfil_Insert, A546UsuarioPerfil_Delete, A543UsuarioPerfil_Display, A4Perfil_Nome) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV13Usuario_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Usuario_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV15ContratadaUsuario_UsuarioPessoaNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContratadaUsuario_UsuarioPessoaNom", AV15ContratadaUsuario_UsuarioPessoaNom);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PADK2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTDK2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823241735");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_cfgperfil.aspx") + "?" + UrlEncode("" +AV13Usuario_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV15ContratadaUsuario_UsuarioPessoaNom))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_12", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_12), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PERFIL_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A7Perfil_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV11WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV11WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "PERFIL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Usuario_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPERFIS", AV14Perfis);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPERFIS", AV14Perfis);
         }
         GxWebStd.gx_boolean_hidden_field( context, "USUARIOPERFIL_UPDATE", A659UsuarioPerfil_Update);
         GxWebStd.gx_boolean_hidden_field( context, "USUARIOPERFIL_INSERT", A544UsuarioPerfil_Insert);
         GxWebStd.gx_boolean_hidden_field( context, "USUARIOPERFIL_DELETE", A546UsuarioPerfil_Delete);
         GxWebStd.gx_boolean_hidden_field( context, "USUARIOPERFIL_DISPLAY", A543UsuarioPerfil_Display);
         GxWebStd.gx_hidden_field( context, "PERFIL_NOME", StringUtil.RTrim( A4Perfil_Nome));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEDK2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTDK2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_cfgperfil.aspx") + "?" + UrlEncode("" +AV13Usuario_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV15ContratadaUsuario_UsuarioPessoaNom)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_CfgPerfil" ;
      }

      public override String GetPgmdesc( )
      {
         return "" ;
      }

      protected void WBDK0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_DK2( true) ;
         }
         else
         {
            wb_table1_2_DK2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DK2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTDK2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPDK0( ) ;
      }

      protected void WSDK2( )
      {
         STARTDK2( ) ;
         EVTDK2( ) ;
      }

      protected void EVTDK2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 27), "VUSUARIOPERFIL_INSERT.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 27), "VUSUARIOPERFIL_UPDATE.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 27), "VUSUARIOPERFIL_DELETE.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 28), "VUSUARIOPERFIL_DISPLAY.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 27), "VUSUARIOPERFIL_INSERT.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 27), "VUSUARIOPERFIL_UPDATE.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 27), "VUSUARIOPERFIL_DELETE.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 28), "VUSUARIOPERFIL_DISPLAY.CLICK") == 0 ) )
                           {
                              nGXsfl_12_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_12_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_12_idx), 4, 0)), 4, "0");
                              SubsflControlProps_122( ) ;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavPerfil_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPerfil_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPERFIL_CODIGO");
                                 GX_FocusControl = edtavPerfil_codigo_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV9Perfil_Codigo = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPerfil_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Perfil_Codigo), 6, 0)));
                              }
                              else
                              {
                                 AV9Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavPerfil_codigo_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPerfil_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Perfil_Codigo), 6, 0)));
                              }
                              AV12Perfil_Nome = StringUtil.Upper( cgiGet( edtavPerfil_nome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPerfil_nome_Internalname, AV12Perfil_Nome);
                              AV5UsuarioPerfil_Insert = StringUtil.StrToBool( cgiGet( chkavUsuarioperfil_insert_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavUsuarioperfil_insert_Internalname, AV5UsuarioPerfil_Insert);
                              AV8UsuarioPerfil_Update = StringUtil.StrToBool( cgiGet( chkavUsuarioperfil_update_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavUsuarioperfil_update_Internalname, AV8UsuarioPerfil_Update);
                              AV6UsuarioPerfil_Delete = StringUtil.StrToBool( cgiGet( chkavUsuarioperfil_delete_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavUsuarioperfil_delete_Internalname, AV6UsuarioPerfil_Delete);
                              AV7UsuarioPerfil_Display = StringUtil.StrToBool( cgiGet( chkavUsuarioperfil_display_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavUsuarioperfil_display_Internalname, AV7UsuarioPerfil_Display);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E11DK2 */
                                    E11DK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VUSUARIOPERFIL_INSERT.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12DK2 */
                                    E12DK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VUSUARIOPERFIL_UPDATE.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13DK2 */
                                    E13DK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VUSUARIOPERFIL_DELETE.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14DK2 */
                                    E14DK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VUSUARIOPERFIL_DISPLAY.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15DK2 */
                                    E15DK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E16DK2 */
                                    E16DK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E17DK2 */
                                    E17DK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDK2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PADK2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            GXCCtl = "vUSUARIOPERFIL_INSERT_" + sGXsfl_12_idx;
            chkavUsuarioperfil_insert.Name = GXCCtl;
            chkavUsuarioperfil_insert.WebTags = "";
            chkavUsuarioperfil_insert.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUsuarioperfil_insert_Internalname, "TitleCaption", chkavUsuarioperfil_insert.Caption);
            chkavUsuarioperfil_insert.CheckedValue = "false";
            GXCCtl = "vUSUARIOPERFIL_UPDATE_" + sGXsfl_12_idx;
            chkavUsuarioperfil_update.Name = GXCCtl;
            chkavUsuarioperfil_update.WebTags = "";
            chkavUsuarioperfil_update.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUsuarioperfil_update_Internalname, "TitleCaption", chkavUsuarioperfil_update.Caption);
            chkavUsuarioperfil_update.CheckedValue = "false";
            GXCCtl = "vUSUARIOPERFIL_DELETE_" + sGXsfl_12_idx;
            chkavUsuarioperfil_delete.Name = GXCCtl;
            chkavUsuarioperfil_delete.WebTags = "";
            chkavUsuarioperfil_delete.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUsuarioperfil_delete_Internalname, "TitleCaption", chkavUsuarioperfil_delete.Caption);
            chkavUsuarioperfil_delete.CheckedValue = "false";
            GXCCtl = "vUSUARIOPERFIL_DISPLAY_" + sGXsfl_12_idx;
            chkavUsuarioperfil_display.Name = GXCCtl;
            chkavUsuarioperfil_display.WebTags = "";
            chkavUsuarioperfil_display.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUsuarioperfil_display_Internalname, "TitleCaption", chkavUsuarioperfil_display.Caption);
            chkavUsuarioperfil_display.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_122( ) ;
         while ( nGXsfl_12_idx <= nRC_GXsfl_12 )
         {
            sendrow_122( ) ;
            nGXsfl_12_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_12_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_12_idx+1));
            sGXsfl_12_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_12_idx), 4, 0)), 4, "0");
            SubsflControlProps_122( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int A7Perfil_AreaTrabalhoCod ,
                                       wwpbaseobjects.SdtWWPContext AV11WWPContext ,
                                       int A3Perfil_Codigo ,
                                       int A1Usuario_Codigo ,
                                       int AV13Usuario_Codigo ,
                                       IGxCollection AV14Perfis ,
                                       bool A659UsuarioPerfil_Update ,
                                       bool A544UsuarioPerfil_Insert ,
                                       bool A546UsuarioPerfil_Delete ,
                                       bool A543UsuarioPerfil_Display ,
                                       String A4Perfil_Nome )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFDK2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDK2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavPerfil_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_codigo_Enabled), 5, 0)));
         edtavPerfil_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome_Enabled), 5, 0)));
      }

      protected void RFDK2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 12;
         /* Execute user event: E16DK2 */
         E16DK2 ();
         nGXsfl_12_idx = 1;
         sGXsfl_12_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_12_idx), 4, 0)), 4, "0");
         SubsflControlProps_122( ) ;
         nGXsfl_12_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_122( ) ;
            /* Using cursor H00DK2 */
            pr_default.execute(0, new Object[] {AV11WWPContext.gxTpr_Areatrabalho_codigo});
            nGXsfl_12_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A3Perfil_Codigo = H00DK2_A3Perfil_Codigo[0];
               A4Perfil_Nome = H00DK2_A4Perfil_Nome[0];
               A276Perfil_Ativo = H00DK2_A276Perfil_Ativo[0];
               A7Perfil_AreaTrabalhoCod = H00DK2_A7Perfil_AreaTrabalhoCod[0];
               /* Execute user event: E17DK2 */
               E17DK2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 12;
            WBDK0( ) ;
         }
         nGXsfl_12_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A7Perfil_AreaTrabalhoCod, AV11WWPContext, A3Perfil_Codigo, A1Usuario_Codigo, AV13Usuario_Codigo, AV14Perfis, A659UsuarioPerfil_Update, A544UsuarioPerfil_Insert, A546UsuarioPerfil_Delete, A543UsuarioPerfil_Display, A4Perfil_Nome) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A7Perfil_AreaTrabalhoCod, AV11WWPContext, A3Perfil_Codigo, A1Usuario_Codigo, AV13Usuario_Codigo, AV14Perfis, A659UsuarioPerfil_Update, A544UsuarioPerfil_Insert, A546UsuarioPerfil_Delete, A543UsuarioPerfil_Display, A4Perfil_Nome) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A7Perfil_AreaTrabalhoCod, AV11WWPContext, A3Perfil_Codigo, A1Usuario_Codigo, AV13Usuario_Codigo, AV14Perfis, A659UsuarioPerfil_Update, A544UsuarioPerfil_Insert, A546UsuarioPerfil_Delete, A543UsuarioPerfil_Display, A4Perfil_Nome) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A7Perfil_AreaTrabalhoCod, AV11WWPContext, A3Perfil_Codigo, A1Usuario_Codigo, AV13Usuario_Codigo, AV14Perfis, A659UsuarioPerfil_Update, A544UsuarioPerfil_Insert, A546UsuarioPerfil_Delete, A543UsuarioPerfil_Display, A4Perfil_Nome) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A7Perfil_AreaTrabalhoCod, AV11WWPContext, A3Perfil_Codigo, A1Usuario_Codigo, AV13Usuario_Codigo, AV14Perfis, A659UsuarioPerfil_Update, A544UsuarioPerfil_Insert, A546UsuarioPerfil_Delete, A543UsuarioPerfil_Display, A4Perfil_Nome) ;
         }
         return (int)(0) ;
      }

      protected void STRUPDK0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavPerfil_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_codigo_Enabled), 5, 0)));
         edtavPerfil_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11DK2 */
         E11DK2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_12 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_12"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11DK2 */
         E11DK2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11DK2( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
      }

      protected void E12DK2( )
      {
         /* Usuarioperfil_insert_Click Routine */
         new prc_updusuarioperfil_ins(context ).execute( ref  AV13Usuario_Codigo, ref  AV9Perfil_Codigo,  AV5UsuarioPerfil_Insert) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Usuario_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPerfil_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Perfil_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavUsuarioperfil_insert_Internalname, AV5UsuarioPerfil_Insert);
         context.DoAjaxRefresh();
      }

      protected void E13DK2( )
      {
         /* Usuarioperfil_update_Click Routine */
         new prc_updusuarioperfil_upd(context ).execute( ref  AV13Usuario_Codigo, ref  AV9Perfil_Codigo,  AV8UsuarioPerfil_Update) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Usuario_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPerfil_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Perfil_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavUsuarioperfil_update_Internalname, AV8UsuarioPerfil_Update);
         context.DoAjaxRefresh();
      }

      protected void E14DK2( )
      {
         /* Usuarioperfil_delete_Click Routine */
         new prc_updusuarioperfil_dlt(context ).execute( ref  AV13Usuario_Codigo, ref  AV9Perfil_Codigo,  AV6UsuarioPerfil_Delete) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Usuario_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPerfil_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Perfil_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavUsuarioperfil_delete_Internalname, AV6UsuarioPerfil_Delete);
         context.DoAjaxRefresh();
      }

      protected void E15DK2( )
      {
         /* Usuarioperfil_display_Click Routine */
         new prc_updusuarioperfil_dsp(context ).execute( ref  AV13Usuario_Codigo, ref  AV9Perfil_Codigo,  AV7UsuarioPerfil_Display) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Usuario_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPerfil_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Perfil_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavUsuarioperfil_display_Internalname, AV7UsuarioPerfil_Display);
         context.DoAjaxRefresh();
      }

      protected void E16DK2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV11WWPContext) ;
         /* Execute user subroutine: 'PERFIS' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11WWPContext", AV11WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14Perfis", AV14Perfis);
      }

      private void E17DK2( )
      {
         /* Grid_Load Routine */
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A3Perfil_Codigo ,
                                              AV14Perfis ,
                                              AV13Usuario_Codigo ,
                                              A1Usuario_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00DK3 */
         pr_default.execute(1, new Object[] {AV13Usuario_Codigo, A3Perfil_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1Usuario_Codigo = H00DK3_A1Usuario_Codigo[0];
            A659UsuarioPerfil_Update = H00DK3_A659UsuarioPerfil_Update[0];
            n659UsuarioPerfil_Update = H00DK3_n659UsuarioPerfil_Update[0];
            A544UsuarioPerfil_Insert = H00DK3_A544UsuarioPerfil_Insert[0];
            n544UsuarioPerfil_Insert = H00DK3_n544UsuarioPerfil_Insert[0];
            A546UsuarioPerfil_Delete = H00DK3_A546UsuarioPerfil_Delete[0];
            n546UsuarioPerfil_Delete = H00DK3_n546UsuarioPerfil_Delete[0];
            A543UsuarioPerfil_Display = H00DK3_A543UsuarioPerfil_Display[0];
            n543UsuarioPerfil_Display = H00DK3_n543UsuarioPerfil_Display[0];
            AV8UsuarioPerfil_Update = A659UsuarioPerfil_Update;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavUsuarioperfil_update_Internalname, AV8UsuarioPerfil_Update);
            AV5UsuarioPerfil_Insert = A544UsuarioPerfil_Insert;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavUsuarioperfil_insert_Internalname, AV5UsuarioPerfil_Insert);
            AV6UsuarioPerfil_Delete = A546UsuarioPerfil_Delete;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavUsuarioperfil_delete_Internalname, AV6UsuarioPerfil_Delete);
            AV7UsuarioPerfil_Display = A543UsuarioPerfil_Display;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavUsuarioperfil_display_Internalname, AV7UsuarioPerfil_Display);
            AV9Perfil_Codigo = A3Perfil_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPerfil_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Perfil_Codigo), 6, 0)));
            AV12Perfil_Nome = A4Perfil_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPerfil_nome_Internalname, AV12Perfil_Nome);
            chkavUsuarioperfil_update.Enabled = ((AV8UsuarioPerfil_Update&&(AV11WWPContext.gxTpr_Update||AV11WWPContext.gxTpr_Delete))||(!AV8UsuarioPerfil_Update&&(AV11WWPContext.gxTpr_Insert||AV11WWPContext.gxTpr_Update)) ? 1 : 0);
            chkavUsuarioperfil_insert.Enabled = ((AV5UsuarioPerfil_Insert&&(AV11WWPContext.gxTpr_Update||AV11WWPContext.gxTpr_Delete))||(!AV5UsuarioPerfil_Insert&&(AV11WWPContext.gxTpr_Insert||AV11WWPContext.gxTpr_Update)) ? 1 : 0);
            chkavUsuarioperfil_delete.Enabled = ((AV6UsuarioPerfil_Delete&&(AV11WWPContext.gxTpr_Update||AV11WWPContext.gxTpr_Delete))||(!AV6UsuarioPerfil_Delete&&(AV11WWPContext.gxTpr_Insert||AV11WWPContext.gxTpr_Update)) ? 1 : 0);
            chkavUsuarioperfil_display.Enabled = ((AV7UsuarioPerfil_Display&&(AV11WWPContext.gxTpr_Update||AV11WWPContext.gxTpr_Delete))||(!AV7UsuarioPerfil_Display&&(AV11WWPContext.gxTpr_Insert||AV11WWPContext.gxTpr_Update)) ? 1 : 0);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 12;
            }
            if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
            {
               sendrow_122( ) ;
               GRID_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
               if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
               {
                  GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
               }
            }
            if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
            {
               GRID_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            }
            GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_12_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(12, GridRow);
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      protected void S112( )
      {
         /* 'PERFIS' Routine */
         AV14Perfis.Clear();
         /* Using cursor H00DK4 */
         pr_default.execute(2, new Object[] {AV11WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A7Perfil_AreaTrabalhoCod = H00DK4_A7Perfil_AreaTrabalhoCod[0];
            A3Perfil_Codigo = H00DK4_A3Perfil_Codigo[0];
            AV14Perfis.Add(A3Perfil_Codigo, 0);
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void wb_table1_2_DK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "Configura��o de Acesso para", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_CfgPerfil.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavContratadausuario_usuariopessoanom_Internalname, StringUtil.RTrim( AV15ContratadaUsuario_UsuarioPessoaNom), StringUtil.RTrim( context.localUtil.Format( AV15ContratadaUsuario_UsuarioPessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadausuario_usuariopessoanom_Jsonclick, 0, "TextBlockTitleWWP", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_CfgPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"12\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(360), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Perfil") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Inserir") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Alterar") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Apagar") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Visualizar") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Perfil_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPerfil_codigo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV12Perfil_Nome));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPerfil_nome_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV5UsuarioPerfil_Insert));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkavUsuarioperfil_insert.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV8UsuarioPerfil_Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkavUsuarioperfil_update.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV6UsuarioPerfil_Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkavUsuarioperfil_delete.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV7UsuarioPerfil_Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkavUsuarioperfil_display.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 12 )
         {
            wbEnd = 0;
            nRC_GXsfl_12 = (short)(nGXsfl_12_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(12), 2, 0)+","+"null"+");", "Fechar", bttButton2_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_CfgPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DK2e( true) ;
         }
         else
         {
            wb_table1_2_DK2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV13Usuario_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Usuario_Codigo), 6, 0)));
         AV15ContratadaUsuario_UsuarioPessoaNom = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContratadaUsuario_UsuarioPessoaNom", AV15ContratadaUsuario_UsuarioPessoaNom);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADK2( ) ;
         WSDK2( ) ;
         WEDK2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823241767");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_cfgperfil.js", "?202042823241767");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_122( )
      {
         edtavPerfil_codigo_Internalname = "vPERFIL_CODIGO_"+sGXsfl_12_idx;
         edtavPerfil_nome_Internalname = "vPERFIL_NOME_"+sGXsfl_12_idx;
         chkavUsuarioperfil_insert_Internalname = "vUSUARIOPERFIL_INSERT_"+sGXsfl_12_idx;
         chkavUsuarioperfil_update_Internalname = "vUSUARIOPERFIL_UPDATE_"+sGXsfl_12_idx;
         chkavUsuarioperfil_delete_Internalname = "vUSUARIOPERFIL_DELETE_"+sGXsfl_12_idx;
         chkavUsuarioperfil_display_Internalname = "vUSUARIOPERFIL_DISPLAY_"+sGXsfl_12_idx;
      }

      protected void SubsflControlProps_fel_122( )
      {
         edtavPerfil_codigo_Internalname = "vPERFIL_CODIGO_"+sGXsfl_12_fel_idx;
         edtavPerfil_nome_Internalname = "vPERFIL_NOME_"+sGXsfl_12_fel_idx;
         chkavUsuarioperfil_insert_Internalname = "vUSUARIOPERFIL_INSERT_"+sGXsfl_12_fel_idx;
         chkavUsuarioperfil_update_Internalname = "vUSUARIOPERFIL_UPDATE_"+sGXsfl_12_fel_idx;
         chkavUsuarioperfil_delete_Internalname = "vUSUARIOPERFIL_DELETE_"+sGXsfl_12_fel_idx;
         chkavUsuarioperfil_display_Internalname = "vUSUARIOPERFIL_DISPLAY_"+sGXsfl_12_fel_idx;
      }

      protected void sendrow_122( )
      {
         SubsflControlProps_122( ) ;
         WBDK0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_12_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_12_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_12_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavPerfil_codigo_Enabled!=0)&&(edtavPerfil_codigo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 13,'',false,'"+sGXsfl_12_idx+"',12)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPerfil_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Perfil_Codigo), 6, 0, ",", "")),((edtavPerfil_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9Perfil_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV9Perfil_Codigo), "ZZZZZ9")),TempTags+((edtavPerfil_codigo_Enabled!=0)&&(edtavPerfil_codigo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPerfil_codigo_Enabled!=0)&&(edtavPerfil_codigo_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,13);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPerfil_codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavPerfil_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)12,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavPerfil_nome_Enabled!=0)&&(edtavPerfil_nome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 14,'',false,'"+sGXsfl_12_idx+"',12)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPerfil_nome_Internalname,StringUtil.RTrim( AV12Perfil_Nome),StringUtil.RTrim( context.localUtil.Format( AV12Perfil_Nome, "@!")),TempTags+((edtavPerfil_nome_Enabled!=0)&&(edtavPerfil_nome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPerfil_nome_Enabled!=0)&&(edtavPerfil_nome_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,14);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPerfil_nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPerfil_nome_Enabled,(short)0,(String)"text",(String)"",(short)360,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)12,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavUsuarioperfil_insert.Enabled!=0)&&(chkavUsuarioperfil_insert.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 15,'',false,'"+sGXsfl_12_idx+"',12)\"" : " ");
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavUsuarioperfil_insert_Internalname,StringUtil.BoolToStr( AV5UsuarioPerfil_Insert),(String)"",(String)"",(short)-1,chkavUsuarioperfil_insert.Enabled,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavUsuarioperfil_insert.Enabled!=0)&&(chkavUsuarioperfil_insert.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(15, this, 'true', 'false');gx.ajax.executeCliEvent('e12dk2_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavUsuarioperfil_insert.Enabled!=0)&&(chkavUsuarioperfil_insert.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,15);\"" : " ")});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavUsuarioperfil_update.Enabled!=0)&&(chkavUsuarioperfil_update.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 16,'',false,'"+sGXsfl_12_idx+"',12)\"" : " ");
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavUsuarioperfil_update_Internalname,StringUtil.BoolToStr( AV8UsuarioPerfil_Update),(String)"",(String)"",(short)-1,chkavUsuarioperfil_update.Enabled,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavUsuarioperfil_update.Enabled!=0)&&(chkavUsuarioperfil_update.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(16, this, 'true', 'false');gx.ajax.executeCliEvent('e13dk2_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavUsuarioperfil_update.Enabled!=0)&&(chkavUsuarioperfil_update.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,16);\"" : " ")});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavUsuarioperfil_delete.Enabled!=0)&&(chkavUsuarioperfil_delete.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 17,'',false,'"+sGXsfl_12_idx+"',12)\"" : " ");
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavUsuarioperfil_delete_Internalname,StringUtil.BoolToStr( AV6UsuarioPerfil_Delete),(String)"",(String)"",(short)-1,chkavUsuarioperfil_delete.Enabled,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavUsuarioperfil_delete.Enabled!=0)&&(chkavUsuarioperfil_delete.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(17, this, 'true', 'false');gx.ajax.executeCliEvent('e14dk2_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavUsuarioperfil_delete.Enabled!=0)&&(chkavUsuarioperfil_delete.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,17);\"" : " ")});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavUsuarioperfil_display.Enabled!=0)&&(chkavUsuarioperfil_display.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 18,'',false,'"+sGXsfl_12_idx+"',12)\"" : " ");
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavUsuarioperfil_display_Internalname,StringUtil.BoolToStr( AV7UsuarioPerfil_Display),(String)"",(String)"",(short)-1,chkavUsuarioperfil_display.Enabled,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavUsuarioperfil_display.Enabled!=0)&&(chkavUsuarioperfil_display.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(18, this, 'true', 'false');gx.ajax.executeCliEvent('e15dk2_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavUsuarioperfil_display.Enabled!=0)&&(chkavUsuarioperfil_display.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,18);\"" : " ")});
            GridContainer.AddRow(GridRow);
            nGXsfl_12_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_12_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_12_idx+1));
            sGXsfl_12_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_12_idx), 4, 0)), 4, "0");
            SubsflControlProps_122( ) ;
         }
         /* End function sendrow_122 */
      }

      protected void init_default_properties( )
      {
         lblViewtitle_Internalname = "VIEWTITLE";
         edtavContratadausuario_usuariopessoanom_Internalname = "vCONTRATADAUSUARIO_USUARIOPESSOANOM";
         edtavPerfil_codigo_Internalname = "vPERFIL_CODIGO";
         edtavPerfil_nome_Internalname = "vPERFIL_NOME";
         chkavUsuarioperfil_insert_Internalname = "vUSUARIOPERFIL_INSERT";
         chkavUsuarioperfil_update_Internalname = "vUSUARIOPERFIL_UPDATE";
         chkavUsuarioperfil_delete_Internalname = "vUSUARIOPERFIL_DELETE";
         chkavUsuarioperfil_display_Internalname = "vUSUARIOPERFIL_DISPLAY";
         bttButton2_Internalname = "BUTTON2";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         chkavUsuarioperfil_display.Visible = -1;
         chkavUsuarioperfil_delete.Visible = -1;
         chkavUsuarioperfil_update.Visible = -1;
         chkavUsuarioperfil_insert.Visible = -1;
         edtavPerfil_nome_Jsonclick = "";
         edtavPerfil_nome_Visible = -1;
         edtavPerfil_codigo_Jsonclick = "";
         edtavPerfil_codigo_Visible = 0;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         chkavUsuarioperfil_display.Enabled = 1;
         chkavUsuarioperfil_delete.Enabled = 1;
         chkavUsuarioperfil_update.Enabled = 1;
         chkavUsuarioperfil_insert.Enabled = 1;
         edtavPerfil_nome_Enabled = 1;
         edtavPerfil_codigo_Enabled = 1;
         subGrid_Class = "WorkWith";
         edtavContratadausuario_usuariopessoanom_Jsonclick = "";
         subGrid_Backcolorstyle = 3;
         chkavUsuarioperfil_display.Caption = "";
         chkavUsuarioperfil_delete.Caption = "";
         chkavUsuarioperfil_update.Caption = "";
         chkavUsuarioperfil_insert.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null},{av:'A659UsuarioPerfil_Update',fld:'USUARIOPERFIL_UPDATE',pic:'',nv:false},{av:'A544UsuarioPerfil_Insert',fld:'USUARIOPERFIL_INSERT',pic:'',nv:false},{av:'A546UsuarioPerfil_Delete',fld:'USUARIOPERFIL_DELETE',pic:'',nv:false},{av:'A543UsuarioPerfil_Display',fld:'USUARIOPERFIL_DISPLAY',pic:'',nv:false},{av:'A4Perfil_Nome',fld:'PERFIL_NOME',pic:'@!',nv:''},{av:'A7Perfil_AreaTrabalhoCod',fld:'PERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null}]}");
         setEventMetadata("VUSUARIOPERFIL_INSERT.CLICK","{handler:'E12DK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A7Perfil_AreaTrabalhoCod',fld:'PERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null},{av:'A659UsuarioPerfil_Update',fld:'USUARIOPERFIL_UPDATE',pic:'',nv:false},{av:'A544UsuarioPerfil_Insert',fld:'USUARIOPERFIL_INSERT',pic:'',nv:false},{av:'A546UsuarioPerfil_Delete',fld:'USUARIOPERFIL_DELETE',pic:'',nv:false},{av:'A543UsuarioPerfil_Display',fld:'USUARIOPERFIL_DISPLAY',pic:'',nv:false},{av:'A4Perfil_Nome',fld:'PERFIL_NOME',pic:'@!',nv:''},{av:'AV9Perfil_Codigo',fld:'vPERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5UsuarioPerfil_Insert',fld:'vUSUARIOPERFIL_INSERT',pic:'',nv:false}],oparms:[{av:'AV9Perfil_Codigo',fld:'vPERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VUSUARIOPERFIL_UPDATE.CLICK","{handler:'E13DK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A7Perfil_AreaTrabalhoCod',fld:'PERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null},{av:'A659UsuarioPerfil_Update',fld:'USUARIOPERFIL_UPDATE',pic:'',nv:false},{av:'A544UsuarioPerfil_Insert',fld:'USUARIOPERFIL_INSERT',pic:'',nv:false},{av:'A546UsuarioPerfil_Delete',fld:'USUARIOPERFIL_DELETE',pic:'',nv:false},{av:'A543UsuarioPerfil_Display',fld:'USUARIOPERFIL_DISPLAY',pic:'',nv:false},{av:'A4Perfil_Nome',fld:'PERFIL_NOME',pic:'@!',nv:''},{av:'AV9Perfil_Codigo',fld:'vPERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8UsuarioPerfil_Update',fld:'vUSUARIOPERFIL_UPDATE',pic:'',nv:false}],oparms:[{av:'AV9Perfil_Codigo',fld:'vPERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VUSUARIOPERFIL_DELETE.CLICK","{handler:'E14DK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A7Perfil_AreaTrabalhoCod',fld:'PERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null},{av:'A659UsuarioPerfil_Update',fld:'USUARIOPERFIL_UPDATE',pic:'',nv:false},{av:'A544UsuarioPerfil_Insert',fld:'USUARIOPERFIL_INSERT',pic:'',nv:false},{av:'A546UsuarioPerfil_Delete',fld:'USUARIOPERFIL_DELETE',pic:'',nv:false},{av:'A543UsuarioPerfil_Display',fld:'USUARIOPERFIL_DISPLAY',pic:'',nv:false},{av:'A4Perfil_Nome',fld:'PERFIL_NOME',pic:'@!',nv:''},{av:'AV9Perfil_Codigo',fld:'vPERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6UsuarioPerfil_Delete',fld:'vUSUARIOPERFIL_DELETE',pic:'',nv:false}],oparms:[{av:'AV9Perfil_Codigo',fld:'vPERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VUSUARIOPERFIL_DISPLAY.CLICK","{handler:'E15DK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A7Perfil_AreaTrabalhoCod',fld:'PERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null},{av:'A659UsuarioPerfil_Update',fld:'USUARIOPERFIL_UPDATE',pic:'',nv:false},{av:'A544UsuarioPerfil_Insert',fld:'USUARIOPERFIL_INSERT',pic:'',nv:false},{av:'A546UsuarioPerfil_Delete',fld:'USUARIOPERFIL_DELETE',pic:'',nv:false},{av:'A543UsuarioPerfil_Display',fld:'USUARIOPERFIL_DISPLAY',pic:'',nv:false},{av:'A4Perfil_Nome',fld:'PERFIL_NOME',pic:'@!',nv:''},{av:'AV9Perfil_Codigo',fld:'vPERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7UsuarioPerfil_Display',fld:'vUSUARIOPERFIL_DISPLAY',pic:'',nv:false}],oparms:[{av:'AV9Perfil_Codigo',fld:'vPERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E17DK2',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null},{av:'A659UsuarioPerfil_Update',fld:'USUARIOPERFIL_UPDATE',pic:'',nv:false},{av:'A544UsuarioPerfil_Insert',fld:'USUARIOPERFIL_INSERT',pic:'',nv:false},{av:'A546UsuarioPerfil_Delete',fld:'USUARIOPERFIL_DELETE',pic:'',nv:false},{av:'A543UsuarioPerfil_Display',fld:'USUARIOPERFIL_DISPLAY',pic:'',nv:false},{av:'A4Perfil_Nome',fld:'PERFIL_NOME',pic:'@!',nv:''},{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV8UsuarioPerfil_Update',fld:'vUSUARIOPERFIL_UPDATE',pic:'',nv:false},{av:'AV5UsuarioPerfil_Insert',fld:'vUSUARIOPERFIL_INSERT',pic:'',nv:false},{av:'AV6UsuarioPerfil_Delete',fld:'vUSUARIOPERFIL_DELETE',pic:'',nv:false},{av:'AV7UsuarioPerfil_Display',fld:'vUSUARIOPERFIL_DISPLAY',pic:'',nv:false},{av:'AV9Perfil_Codigo',fld:'vPERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12Perfil_Nome',fld:'vPERFIL_NOME',pic:'@!',nv:''},{av:'chkavUsuarioperfil_update.Enabled',ctrl:'vUSUARIOPERFIL_UPDATE',prop:'Enabled'},{av:'chkavUsuarioperfil_insert.Enabled',ctrl:'vUSUARIOPERFIL_INSERT',prop:'Enabled'},{av:'chkavUsuarioperfil_delete.Enabled',ctrl:'vUSUARIOPERFIL_DELETE',prop:'Enabled'},{av:'chkavUsuarioperfil_display.Enabled',ctrl:'vUSUARIOPERFIL_DISPLAY',prop:'Enabled'}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null},{av:'A659UsuarioPerfil_Update',fld:'USUARIOPERFIL_UPDATE',pic:'',nv:false},{av:'A544UsuarioPerfil_Insert',fld:'USUARIOPERFIL_INSERT',pic:'',nv:false},{av:'A546UsuarioPerfil_Delete',fld:'USUARIOPERFIL_DELETE',pic:'',nv:false},{av:'A543UsuarioPerfil_Display',fld:'USUARIOPERFIL_DISPLAY',pic:'',nv:false},{av:'A4Perfil_Nome',fld:'PERFIL_NOME',pic:'@!',nv:''},{av:'A7Perfil_AreaTrabalhoCod',fld:'PERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null},{av:'A659UsuarioPerfil_Update',fld:'USUARIOPERFIL_UPDATE',pic:'',nv:false},{av:'A544UsuarioPerfil_Insert',fld:'USUARIOPERFIL_INSERT',pic:'',nv:false},{av:'A546UsuarioPerfil_Delete',fld:'USUARIOPERFIL_DELETE',pic:'',nv:false},{av:'A543UsuarioPerfil_Display',fld:'USUARIOPERFIL_DISPLAY',pic:'',nv:false},{av:'A4Perfil_Nome',fld:'PERFIL_NOME',pic:'@!',nv:''},{av:'A7Perfil_AreaTrabalhoCod',fld:'PERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null},{av:'A659UsuarioPerfil_Update',fld:'USUARIOPERFIL_UPDATE',pic:'',nv:false},{av:'A544UsuarioPerfil_Insert',fld:'USUARIOPERFIL_INSERT',pic:'',nv:false},{av:'A546UsuarioPerfil_Delete',fld:'USUARIOPERFIL_DELETE',pic:'',nv:false},{av:'A543UsuarioPerfil_Display',fld:'USUARIOPERFIL_DISPLAY',pic:'',nv:false},{av:'A4Perfil_Nome',fld:'PERFIL_NOME',pic:'@!',nv:''},{av:'A7Perfil_AreaTrabalhoCod',fld:'PERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null},{av:'A659UsuarioPerfil_Update',fld:'USUARIOPERFIL_UPDATE',pic:'',nv:false},{av:'A544UsuarioPerfil_Insert',fld:'USUARIOPERFIL_INSERT',pic:'',nv:false},{av:'A546UsuarioPerfil_Delete',fld:'USUARIOPERFIL_DELETE',pic:'',nv:false},{av:'A543UsuarioPerfil_Display',fld:'USUARIOPERFIL_DISPLAY',pic:'',nv:false},{av:'A4Perfil_Nome',fld:'PERFIL_NOME',pic:'@!',nv:''},{av:'A7Perfil_AreaTrabalhoCod',fld:'PERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV14Perfis',fld:'vPERFIS',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV15ContratadaUsuario_UsuarioPessoaNom = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV11WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV14Perfis = new GxSimpleCollection();
         A4Perfil_Nome = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV12Perfil_Nome = "";
         AV7UsuarioPerfil_Display = true;
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00DK2_A3Perfil_Codigo = new int[1] ;
         H00DK2_A4Perfil_Nome = new String[] {""} ;
         H00DK2_A276Perfil_Ativo = new bool[] {false} ;
         H00DK2_A7Perfil_AreaTrabalhoCod = new int[1] ;
         H00DK3_A3Perfil_Codigo = new int[1] ;
         H00DK3_A1Usuario_Codigo = new int[1] ;
         H00DK3_A659UsuarioPerfil_Update = new bool[] {false} ;
         H00DK3_n659UsuarioPerfil_Update = new bool[] {false} ;
         H00DK3_A544UsuarioPerfil_Insert = new bool[] {false} ;
         H00DK3_n544UsuarioPerfil_Insert = new bool[] {false} ;
         H00DK3_A546UsuarioPerfil_Delete = new bool[] {false} ;
         H00DK3_n546UsuarioPerfil_Delete = new bool[] {false} ;
         H00DK3_A543UsuarioPerfil_Display = new bool[] {false} ;
         H00DK3_n543UsuarioPerfil_Display = new bool[] {false} ;
         GridRow = new GXWebRow();
         H00DK4_A7Perfil_AreaTrabalhoCod = new int[1] ;
         H00DK4_A3Perfil_Codigo = new int[1] ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblViewtitle_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         TempTags = "";
         bttButton2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_cfgperfil__default(),
            new Object[][] {
                new Object[] {
               H00DK2_A3Perfil_Codigo, H00DK2_A4Perfil_Nome, H00DK2_A276Perfil_Ativo, H00DK2_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               H00DK3_A3Perfil_Codigo, H00DK3_A1Usuario_Codigo, H00DK3_A659UsuarioPerfil_Update, H00DK3_n659UsuarioPerfil_Update, H00DK3_A544UsuarioPerfil_Insert, H00DK3_n544UsuarioPerfil_Insert, H00DK3_A546UsuarioPerfil_Delete, H00DK3_n546UsuarioPerfil_Delete, H00DK3_A543UsuarioPerfil_Display, H00DK3_n543UsuarioPerfil_Display
               }
               , new Object[] {
               H00DK4_A7Perfil_AreaTrabalhoCod, H00DK4_A3Perfil_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavPerfil_codigo_Enabled = 0;
         edtavPerfil_nome_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_12 ;
      private short nGXsfl_12_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_12_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV13Usuario_Codigo ;
      private int wcpOAV13Usuario_Codigo ;
      private int subGrid_Rows ;
      private int A7Perfil_AreaTrabalhoCod ;
      private int A3Perfil_Codigo ;
      private int A1Usuario_Codigo ;
      private int AV9Perfil_Codigo ;
      private int subGrid_Islastpage ;
      private int edtavPerfil_codigo_Enabled ;
      private int edtavPerfil_nome_Enabled ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavPerfil_codigo_Visible ;
      private int edtavPerfil_nome_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV15ContratadaUsuario_UsuarioPessoaNom ;
      private String wcpOAV15ContratadaUsuario_UsuarioPessoaNom ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_12_idx="0001" ;
      private String A4Perfil_Nome ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavPerfil_codigo_Internalname ;
      private String AV12Perfil_Nome ;
      private String edtavPerfil_nome_Internalname ;
      private String chkavUsuarioperfil_insert_Internalname ;
      private String chkavUsuarioperfil_update_Internalname ;
      private String chkavUsuarioperfil_delete_Internalname ;
      private String chkavUsuarioperfil_display_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private String edtavContratadausuario_usuariopessoanom_Internalname ;
      private String edtavContratadausuario_usuariopessoanom_Jsonclick ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String TempTags ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private String sGXsfl_12_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavPerfil_codigo_Jsonclick ;
      private String edtavPerfil_nome_Jsonclick ;
      private bool entryPointCalled ;
      private bool A659UsuarioPerfil_Update ;
      private bool n659UsuarioPerfil_Update ;
      private bool A544UsuarioPerfil_Insert ;
      private bool n544UsuarioPerfil_Insert ;
      private bool A546UsuarioPerfil_Delete ;
      private bool n546UsuarioPerfil_Delete ;
      private bool A543UsuarioPerfil_Display ;
      private bool n543UsuarioPerfil_Display ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV5UsuarioPerfil_Insert ;
      private bool AV8UsuarioPerfil_Update ;
      private bool AV6UsuarioPerfil_Delete ;
      private bool AV7UsuarioPerfil_Display ;
      private bool A276Perfil_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavUsuarioperfil_insert ;
      private GXCheckbox chkavUsuarioperfil_update ;
      private GXCheckbox chkavUsuarioperfil_delete ;
      private GXCheckbox chkavUsuarioperfil_display ;
      private IDataStoreProvider pr_default ;
      private int[] H00DK2_A3Perfil_Codigo ;
      private String[] H00DK2_A4Perfil_Nome ;
      private bool[] H00DK2_A276Perfil_Ativo ;
      private int[] H00DK2_A7Perfil_AreaTrabalhoCod ;
      private int[] H00DK3_A3Perfil_Codigo ;
      private int[] H00DK3_A1Usuario_Codigo ;
      private bool[] H00DK3_A659UsuarioPerfil_Update ;
      private bool[] H00DK3_n659UsuarioPerfil_Update ;
      private bool[] H00DK3_A544UsuarioPerfil_Insert ;
      private bool[] H00DK3_n544UsuarioPerfil_Insert ;
      private bool[] H00DK3_A546UsuarioPerfil_Delete ;
      private bool[] H00DK3_n546UsuarioPerfil_Delete ;
      private bool[] H00DK3_A543UsuarioPerfil_Display ;
      private bool[] H00DK3_n543UsuarioPerfil_Display ;
      private int[] H00DK4_A7Perfil_AreaTrabalhoCod ;
      private int[] H00DK4_A3Perfil_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV14Perfis ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV11WWPContext ;
   }

   public class wp_cfgperfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00DK3( IGxContext context ,
                                             int A3Perfil_Codigo ,
                                             IGxCollection AV14Perfis ,
                                             int AV13Usuario_Codigo ,
                                             int A1Usuario_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [2] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Perfil_Codigo], [Usuario_Codigo], [UsuarioPerfil_Update], [UsuarioPerfil_Insert], [UsuarioPerfil_Delete], [UsuarioPerfil_Display] FROM [UsuarioPerfil] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Usuario_Codigo] = @AV13Usuario_Codigo and [Perfil_Codigo] = @Perfil_Codigo)";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV14Perfis, "[Perfil_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Usuario_Codigo], [Perfil_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00DK3(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DK2 ;
          prmH00DK2 = new Object[] {
          new Object[] {"@AV11WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DK4 ;
          prmH00DK4 = new Object[] {
          new Object[] {"@AV11WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DK3 ;
          prmH00DK3 = new Object[] {
          new Object[] {"@AV13Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DK2", "SELECT [Perfil_Codigo], [Perfil_Nome], [Perfil_Ativo], [Perfil_AreaTrabalhoCod] FROM [Perfil] WITH (NOLOCK) WHERE ([Perfil_AreaTrabalhoCod] = @AV11WWPC_1Areatrabalho_codigo) AND ([Perfil_Ativo] = 1) ORDER BY [Perfil_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DK2,11,0,true,false )
             ,new CursorDef("H00DK3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DK3,1,0,false,true )
             ,new CursorDef("H00DK4", "SELECT [Perfil_AreaTrabalhoCod], [Perfil_Codigo] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_AreaTrabalhoCod] = @AV11WWPC_1Areatrabalho_codigo ORDER BY [Perfil_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DK4,11,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
