/*
               File: WC_FuncaoAPFAtributosIns
        Description: Funcao APF Atrobutos Ins
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:55:1.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_funcaoapfatributosins : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_funcaoapfatributosins( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_funcaoapfatributosins( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPF_Codigo ,
                           int aP1_Tabela_SistemaCod )
      {
         this.AV12FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV17Tabela_SistemaCod = aP1_Tabela_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynavFuncaodados_codigo = new GXCombobox();
         cmbavAtributos_tipodados = new GXCombobox();
         chkavAtributos_pk = new GXCheckbox();
         chkavAtributos_fk = new GXCheckbox();
         cmbavFuncaodados_tipo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV12FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12FuncaoAPF_Codigo), 6, 0)));
                  AV17Tabela_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Tabela_SistemaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV12FuncaoAPF_Codigo,(int)AV17Tabela_SistemaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vFUNCAODADOS_CODIGO") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvFUNCAODADOS_CODIGOAK2( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridatributos") == 0 )
               {
                  nRC_GXsfl_16 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_16_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_16_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  AV16InsertAtributo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavInsertatributo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16InsertAtributo)) ? AV24Insertatributo_GXI : context.convertURL( context.PathToRelativeUrl( AV16InsertAtributo))));
                  edtavInsertatributo_Tooltiptext = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavInsertatributo_Internalname, "Tooltiptext", edtavInsertatributo_Tooltiptext);
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGridatributos_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridatributos") == 0 )
               {
                  subGridatributos_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV16InsertAtributo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavInsertatributo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16InsertAtributo)) ? AV24Insertatributo_GXI : context.convertURL( context.PathToRelativeUrl( AV16InsertAtributo))));
                  edtavInsertatributo_Tooltiptext = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavInsertatributo_Internalname, "Tooltiptext", edtavInsertatributo_Tooltiptext);
                  A370FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
                  AV17Tabela_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Tabela_SistemaCod), 6, 0)));
                  A391FuncaoDados_FuncaoDadosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n391FuncaoDados_FuncaoDadosCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV19FuncaDadosAIE);
                  A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                  A373FuncaoDados_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
                  A180Atributos_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A180Atributos_Ativo", A180Atributos_Ativo);
                  A356Atributos_TabelaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0)));
                  A172Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
                  A177Atributos_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A177Atributos_Nome", A177Atributos_Nome);
                  AV8FiltroAtributos_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FiltroAtributos_Nome", AV8FiltroAtributos_Nome);
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
                  AV12FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12FuncaoAPF_Codigo), 6, 0)));
                  A364FuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
                  A176Atributos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
                  A178Atributos_TipoDados = GetNextPar( );
                  n178Atributos_TipoDados = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A178Atributos_TipoDados", A178Atributos_TipoDados);
                  A390Atributos_Detalhes = GetNextPar( );
                  n390Atributos_Detalhes = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A390Atributos_Detalhes", A390Atributos_Detalhes);
                  A400Atributos_PK = (bool)(BooleanUtil.Val(GetNextPar( )));
                  n400Atributos_PK = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A400Atributos_PK", A400Atributos_PK);
                  A401Atributos_FK = (bool)(BooleanUtil.Val(GetNextPar( )));
                  n401Atributos_FK = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A401Atributos_FK", A401Atributos_FK);
                  A357Atributos_TabelaNom = GetNextPar( );
                  n357Atributos_TabelaNom = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A357Atributos_TabelaNom", A357Atributos_TabelaNom);
                  AV20FuncaoDados_FuncaoDadosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20FuncaoDados_FuncaoDadosCod), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGridatributos_refresh( subGridatributos_Rows, AV16InsertAtributo, A370FuncaoDados_SistemaCod, AV17Tabela_SistemaCod, A391FuncaoDados_FuncaoDadosCod, AV19FuncaDadosAIE, A368FuncaoDados_Codigo, A373FuncaoDados_Tipo, A180Atributos_Ativo, A356Atributos_TabelaCod, A172Tabela_Codigo, A177Atributos_Nome, AV8FiltroAtributos_Nome, A165FuncaoAPF_Codigo, AV12FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, A176Atributos_Codigo, A178Atributos_TipoDados, A390Atributos_Detalhes, A400Atributos_PK, A401Atributos_FK, A357Atributos_TabelaNom, AV20FuncaoDados_FuncaoDadosCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAAK2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               dynavFuncaodados_codigo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavFuncaodados_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaodados_codigo.Enabled), 5, 0)));
               edtavFuncaoapf_codigo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_codigo_Enabled), 5, 0)));
               edtavAtributos_codigo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_codigo_Enabled), 5, 0)));
               edtavAtributos_nome_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_nome_Enabled), 5, 0)));
               cmbavAtributos_tipodados.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAtributos_tipodados.Enabled), 5, 0)));
               edtavAtributos_detalhes_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_detalhes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_detalhes_Enabled), 5, 0)));
               chkavAtributos_pk.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_pk_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAtributos_pk.Enabled), 5, 0)));
               chkavAtributos_fk.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_fk_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAtributos_fk.Enabled), 5, 0)));
               edtavAtributos_tabelanom_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_tabelanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_tabelanom_Enabled), 5, 0)));
               cmbavFuncaodados_tipo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaodados_tipo.Enabled), 5, 0)));
               GXVvFUNCAODADOS_CODIGO_htmlAK2( ) ;
               WSAK2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Funcao APF Atrobutos Ins") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020562355122");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_funcaoapfatributosins.aspx") + "?" + UrlEncode("" +AV12FuncaoAPF_Codigo) + "," + UrlEncode("" +AV17Tabela_SistemaCod)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_16", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_16), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV12FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV12FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV17Tabela_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV17Tabela_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTABELA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Tabela_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCADADOSAIE", AV19FuncaDadosAIE);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCADADOSAIE", AV19FuncaDadosAIE);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_TIPO", StringUtil.RTrim( A373FuncaoDados_Tipo));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"ATRIBUTOS_ATIVO", A180Atributos_Ativo);
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_TABELACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_NOME", StringUtil.RTrim( A177Atributos_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_TIPODADOS", StringUtil.RTrim( A178Atributos_TipoDados));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_DETALHES", StringUtil.RTrim( A390Atributos_Detalhes));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"ATRIBUTOS_PK", A400Atributos_PK);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"ATRIBUTOS_FK", A401Atributos_FK);
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_TABELANOM", StringUtil.RTrim( A357Atributos_TabelaNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAODADOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20FuncaoDados_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vINSERTATRIBUTO_Tooltiptext", StringUtil.RTrim( edtavInsertatributo_Tooltiptext));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormAK2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_funcaoapfatributosins.js", "?2020562355126");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_FuncaoAPFAtributosIns" ;
      }

      public override String GetPgmdesc( )
      {
         return "Funcao APF Atrobutos Ins" ;
      }

      protected void WBAK0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_funcaoapfatributosins.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_AK2( true) ;
         }
         else
         {
            wb_table1_2_AK2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_AK2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTAK2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Funcao APF Atrobutos Ins", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPAK0( ) ;
            }
         }
      }

      protected void WSAK2( )
      {
         STARTAK2( ) ;
         EVTAK2( ) ;
      }

      protected void EVTAK2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = dynavFuncaodados_codigo_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDATRIBUTOSPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAK0( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDATRIBUTOSPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgridatributos_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgridatributos_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgridatributos_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgridatributos_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "GRIDATRIBUTOS.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "'INSERTATRIBUTO'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "'INSERTATRIBUTO'") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAK0( ) ;
                              }
                              nGXsfl_16_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_16_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_16_idx), 4, 0)), 4, "0");
                              SubsflControlProps_162( ) ;
                              dynavFuncaodados_codigo.Name = dynavFuncaodados_codigo_Internalname;
                              dynavFuncaodados_codigo.CurrentValue = cgiGet( dynavFuncaodados_codigo_Internalname);
                              AV18FuncaoDados_Codigo = (int)(NumberUtil.Val( cgiGet( dynavFuncaodados_codigo_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, dynavFuncaodados_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV18FuncaoDados_Codigo), 6, 0)));
                              if ( StringUtil.Len( sPrefix) == 0 )
                              {
                                 AV12FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_codigo_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12FuncaoAPF_Codigo), 6, 0)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavAtributos_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAtributos_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vATRIBUTOS_CODIGO");
                                 GX_FocusControl = edtavAtributos_codigo_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV5Atributos_Codigo = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtributos_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Atributos_Codigo), 6, 0)));
                              }
                              else
                              {
                                 AV5Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavAtributos_codigo_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtributos_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Atributos_Codigo), 6, 0)));
                              }
                              AV16InsertAtributo = cgiGet( edtavInsertatributo_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavInsertatributo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16InsertAtributo)) ? AV24Insertatributo_GXI : context.convertURL( context.PathToRelativeUrl( AV16InsertAtributo))));
                              AV15Atributos_Nome = StringUtil.Upper( cgiGet( edtavAtributos_nome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtributos_nome_Internalname, AV15Atributos_Nome);
                              cmbavAtributos_tipodados.Name = cmbavAtributos_tipodados_Internalname;
                              cmbavAtributos_tipodados.CurrentValue = cgiGet( cmbavAtributos_tipodados_Internalname);
                              AV11Atributos_TipoDados = cgiGet( cmbavAtributos_tipodados_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavAtributos_tipodados_Internalname, AV11Atributos_TipoDados);
                              AV6Atributos_Detalhes = cgiGet( edtavAtributos_detalhes_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtributos_detalhes_Internalname, AV6Atributos_Detalhes);
                              AV9Atributos_PK = StringUtil.StrToBool( cgiGet( chkavAtributos_pk_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavAtributos_pk_Internalname, AV9Atributos_PK);
                              AV7Atributos_FK = StringUtil.StrToBool( cgiGet( chkavAtributos_fk_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavAtributos_fk_Internalname, AV7Atributos_FK);
                              AV10Atributos_TabelaNom = StringUtil.Upper( cgiGet( edtavAtributos_tabelanom_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtributos_tabelanom_Internalname, AV10Atributos_TabelaNom);
                              cmbavFuncaodados_tipo.Name = cmbavFuncaodados_tipo_Internalname;
                              cmbavFuncaodados_tipo.CurrentValue = cgiGet( cmbavFuncaodados_tipo_Internalname);
                              AV21FuncaoDados_Tipo = cgiGet( cmbavFuncaodados_tipo_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaodados_tipo_Internalname, AV21FuncaoDados_Tipo);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = dynavFuncaodados_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E11AK2 */
                                          E11AK2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = dynavFuncaodados_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12AK2 */
                                          E12AK2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDATRIBUTOS.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = dynavFuncaodados_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13AK2 */
                                          E13AK2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'INSERTATRIBUTO'") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = dynavFuncaodados_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14AK2 */
                                          E14AK2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = dynavFuncaodados_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPAK0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = dynavFuncaodados_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEAK2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormAK2( ) ;
            }
         }
      }

      protected void PAAK2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "vFUNCAODADOS_CODIGO_" + sGXsfl_16_idx;
            dynavFuncaodados_codigo.Name = GXCCtl;
            dynavFuncaodados_codigo.WebTags = "";
            GXCCtl = "vATRIBUTOS_TIPODADOS_" + sGXsfl_16_idx;
            cmbavAtributos_tipodados.Name = GXCCtl;
            cmbavAtributos_tipodados.WebTags = "";
            cmbavAtributos_tipodados.addItem("", "Desconhecido", 0);
            cmbavAtributos_tipodados.addItem("N", "Numeric", 0);
            cmbavAtributos_tipodados.addItem("C", "Character", 0);
            cmbavAtributos_tipodados.addItem("VC", "Varchar", 0);
            cmbavAtributos_tipodados.addItem("D", "Date", 0);
            cmbavAtributos_tipodados.addItem("DT", "Date Time", 0);
            cmbavAtributos_tipodados.addItem("Bool", "Boolean", 0);
            cmbavAtributos_tipodados.addItem("Blob", "Blob", 0);
            cmbavAtributos_tipodados.addItem("Outr", "Outros", 0);
            if ( cmbavAtributos_tipodados.ItemCount > 0 )
            {
               AV11Atributos_TipoDados = cmbavAtributos_tipodados.getValidValue(AV11Atributos_TipoDados);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavAtributos_tipodados_Internalname, AV11Atributos_TipoDados);
            }
            GXCCtl = "vATRIBUTOS_PK_" + sGXsfl_16_idx;
            chkavAtributos_pk.Name = GXCCtl;
            chkavAtributos_pk.WebTags = "";
            chkavAtributos_pk.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_pk_Internalname, "TitleCaption", chkavAtributos_pk.Caption);
            chkavAtributos_pk.CheckedValue = "false";
            GXCCtl = "vATRIBUTOS_FK_" + sGXsfl_16_idx;
            chkavAtributos_fk.Name = GXCCtl;
            chkavAtributos_fk.WebTags = "";
            chkavAtributos_fk.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_fk_Internalname, "TitleCaption", chkavAtributos_fk.Caption);
            chkavAtributos_fk.CheckedValue = "false";
            GXCCtl = "vFUNCAODADOS_TIPO_" + sGXsfl_16_idx;
            cmbavFuncaodados_tipo.Name = GXCCtl;
            cmbavFuncaodados_tipo.WebTags = "";
            cmbavFuncaodados_tipo.addItem("", "(Nenhum)", 0);
            cmbavFuncaodados_tipo.addItem("ALI", "ALI", 0);
            cmbavFuncaodados_tipo.addItem("AIE", "AIE", 0);
            cmbavFuncaodados_tipo.addItem("DC", "DC", 0);
            if ( cmbavFuncaodados_tipo.ItemCount > 0 )
            {
               AV21FuncaoDados_Tipo = cmbavFuncaodados_tipo.getValidValue(AV21FuncaoDados_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaodados_tipo_Internalname, AV21FuncaoDados_Tipo);
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavFiltroatributos_nome_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvFUNCAODADOS_CODIGOAK2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvFUNCAODADOS_CODIGO_dataAK2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvFUNCAODADOS_CODIGO_htmlAK2( )
      {
         int gxdynajaxvalue ;
         GXDLVvFUNCAODADOS_CODIGO_dataAK2( ) ;
         gxdynajaxindex = 1;
         dynavFuncaodados_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavFuncaodados_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvFUNCAODADOS_CODIGO_dataAK2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00AK2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00AK2_A368FuncaoDados_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00AK2_A369FuncaoDados_Nome[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGridatributos_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_162( ) ;
         while ( nGXsfl_16_idx <= nRC_GXsfl_16 )
         {
            sendrow_162( ) ;
            nGXsfl_16_idx = (short)(((subGridatributos_Islastpage==1)&&(nGXsfl_16_idx+1>subGridatributos_Recordsperpage( )) ? 1 : nGXsfl_16_idx+1));
            sGXsfl_16_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_16_idx), 4, 0)), 4, "0");
            SubsflControlProps_162( ) ;
         }
         context.GX_webresponse.AddString(GridatributosContainer.ToJavascriptSource());
         /* End function gxnrGridatributos_newrow */
      }

      protected void gxgrGridatributos_refresh( int subGridatributos_Rows ,
                                                String AV16InsertAtributo ,
                                                int A370FuncaoDados_SistemaCod ,
                                                int AV17Tabela_SistemaCod ,
                                                int A391FuncaoDados_FuncaoDadosCod ,
                                                IGxCollection AV19FuncaDadosAIE ,
                                                int A368FuncaoDados_Codigo ,
                                                String A373FuncaoDados_Tipo ,
                                                bool A180Atributos_Ativo ,
                                                int A356Atributos_TabelaCod ,
                                                int A172Tabela_Codigo ,
                                                String A177Atributos_Nome ,
                                                String AV8FiltroAtributos_Nome ,
                                                int A165FuncaoAPF_Codigo ,
                                                int AV12FuncaoAPF_Codigo ,
                                                int A364FuncaoAPFAtributos_AtributosCod ,
                                                int A176Atributos_Codigo ,
                                                String A178Atributos_TipoDados ,
                                                String A390Atributos_Detalhes ,
                                                bool A400Atributos_PK ,
                                                bool A401Atributos_FK ,
                                                String A357Atributos_TabelaNom ,
                                                int AV20FuncaoDados_FuncaoDadosCod ,
                                                String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         GRIDATRIBUTOS_nCurrentRecord = 0;
         RFAK2( ) ;
         /* End function gxgrGridatributos_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFAK2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         dynavFuncaodados_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavFuncaodados_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaodados_codigo.Enabled), 5, 0)));
         edtavFuncaoapf_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_codigo_Enabled), 5, 0)));
         edtavAtributos_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_codigo_Enabled), 5, 0)));
         edtavAtributos_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_nome_Enabled), 5, 0)));
         cmbavAtributos_tipodados.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAtributos_tipodados.Enabled), 5, 0)));
         edtavAtributos_detalhes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_detalhes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_detalhes_Enabled), 5, 0)));
         chkavAtributos_pk.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_pk_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAtributos_pk.Enabled), 5, 0)));
         chkavAtributos_fk.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_fk_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAtributos_fk.Enabled), 5, 0)));
         edtavAtributos_tabelanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_tabelanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_tabelanom_Enabled), 5, 0)));
         cmbavFuncaodados_tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaodados_tipo.Enabled), 5, 0)));
      }

      protected void RFAK2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridatributosContainer.ClearRows();
         }
         wbStart = 16;
         /* Execute user event: E12AK2 */
         E12AK2 ();
         nGXsfl_16_idx = 1;
         sGXsfl_16_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_16_idx), 4, 0)), 4, "0");
         SubsflControlProps_162( ) ;
         nGXsfl_16_Refreshing = 1;
         GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
         GridatributosContainer.AddObjectProperty("CmpContext", sPrefix);
         GridatributosContainer.AddObjectProperty("InMasterPage", "false");
         GridatributosContainer.AddObjectProperty("Class", "WorkWith");
         GridatributosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridatributosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridatributosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Backcolorstyle), 1, 0, ".", "")));
         GridatributosContainer.PageSize = subGridatributos_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_162( ) ;
            /* Execute user event: E13AK2 */
            E13AK2 ();
            if ( ( GRIDATRIBUTOS_nCurrentRecord > 0 ) && ( GRIDATRIBUTOS_nGridOutOfScope == 0 ) && ( nGXsfl_16_idx == 1 ) )
            {
               GRIDATRIBUTOS_nCurrentRecord = 0;
               GRIDATRIBUTOS_nGridOutOfScope = 1;
               subgridatributos_firstpage( ) ;
               /* Execute user event: E13AK2 */
               E13AK2 ();
            }
            wbEnd = 16;
            WBAK0( ) ;
         }
         nGXsfl_16_Refreshing = 0;
      }

      protected int subGridatributos_Pagecount( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( ((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nRecordCount/ (decimal)(subGridatributos_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nRecordCount/ (decimal)(subGridatributos_Recordsperpage( ))))+1) ;
      }

      protected int subGridatributos_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridatributos_Recordsperpage( )
      {
         if ( subGridatributos_Rows > 0 )
         {
            return subGridatributos_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridatributos_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgridatributos_firstpage( )
      {
         GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV16InsertAtributo, A370FuncaoDados_SistemaCod, AV17Tabela_SistemaCod, A391FuncaoDados_FuncaoDadosCod, AV19FuncaDadosAIE, A368FuncaoDados_Codigo, A373FuncaoDados_Tipo, A180Atributos_Ativo, A356Atributos_TabelaCod, A172Tabela_Codigo, A177Atributos_Nome, AV8FiltroAtributos_Nome, A165FuncaoAPF_Codigo, AV12FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, A176Atributos_Codigo, A178Atributos_TipoDados, A390Atributos_Detalhes, A400Atributos_PK, A401Atributos_FK, A357Atributos_TabelaNom, AV20FuncaoDados_FuncaoDadosCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridatributos_nextpage( )
      {
         if ( GRIDATRIBUTOS_nEOF == 0 )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nFirstRecordOnPage+subGridatributos_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV16InsertAtributo, A370FuncaoDados_SistemaCod, AV17Tabela_SistemaCod, A391FuncaoDados_FuncaoDadosCod, AV19FuncaDadosAIE, A368FuncaoDados_Codigo, A373FuncaoDados_Tipo, A180Atributos_Ativo, A356Atributos_TabelaCod, A172Tabela_Codigo, A177Atributos_Nome, AV8FiltroAtributos_Nome, A165FuncaoAPF_Codigo, AV12FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, A176Atributos_Codigo, A178Atributos_TipoDados, A390Atributos_Detalhes, A400Atributos_PK, A401Atributos_FK, A357Atributos_TabelaNom, AV20FuncaoDados_FuncaoDadosCod, sPrefix) ;
         }
         return (short)(((GRIDATRIBUTOS_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridatributos_previouspage( )
      {
         if ( GRIDATRIBUTOS_nFirstRecordOnPage >= subGridatributos_Recordsperpage( ) )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nFirstRecordOnPage-subGridatributos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV16InsertAtributo, A370FuncaoDados_SistemaCod, AV17Tabela_SistemaCod, A391FuncaoDados_FuncaoDadosCod, AV19FuncaDadosAIE, A368FuncaoDados_Codigo, A373FuncaoDados_Tipo, A180Atributos_Ativo, A356Atributos_TabelaCod, A172Tabela_Codigo, A177Atributos_Nome, AV8FiltroAtributos_Nome, A165FuncaoAPF_Codigo, AV12FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, A176Atributos_Codigo, A178Atributos_TipoDados, A390Atributos_Detalhes, A400Atributos_PK, A401Atributos_FK, A357Atributos_TabelaNom, AV20FuncaoDados_FuncaoDadosCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridatributos_lastpage( )
      {
         subGridatributos_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV16InsertAtributo, A370FuncaoDados_SistemaCod, AV17Tabela_SistemaCod, A391FuncaoDados_FuncaoDadosCod, AV19FuncaDadosAIE, A368FuncaoDados_Codigo, A373FuncaoDados_Tipo, A180Atributos_Ativo, A356Atributos_TabelaCod, A172Tabela_Codigo, A177Atributos_Nome, AV8FiltroAtributos_Nome, A165FuncaoAPF_Codigo, AV12FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, A176Atributos_Codigo, A178Atributos_TipoDados, A390Atributos_Detalhes, A400Atributos_PK, A401Atributos_FK, A357Atributos_TabelaNom, AV20FuncaoDados_FuncaoDadosCod, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgridatributos_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(subGridatributos_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV16InsertAtributo, A370FuncaoDados_SistemaCod, AV17Tabela_SistemaCod, A391FuncaoDados_FuncaoDadosCod, AV19FuncaDadosAIE, A368FuncaoDados_Codigo, A373FuncaoDados_Tipo, A180Atributos_Ativo, A356Atributos_TabelaCod, A172Tabela_Codigo, A177Atributos_Nome, AV8FiltroAtributos_Nome, A165FuncaoAPF_Codigo, AV12FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, A176Atributos_Codigo, A178Atributos_TipoDados, A390Atributos_Detalhes, A400Atributos_PK, A401Atributos_FK, A357Atributos_TabelaNom, AV20FuncaoDados_FuncaoDadosCod, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPAK0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         dynavFuncaodados_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavFuncaodados_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaodados_codigo.Enabled), 5, 0)));
         edtavFuncaoapf_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_codigo_Enabled), 5, 0)));
         edtavAtributos_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_codigo_Enabled), 5, 0)));
         edtavAtributos_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_nome_Enabled), 5, 0)));
         cmbavAtributos_tipodados.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAtributos_tipodados.Enabled), 5, 0)));
         edtavAtributos_detalhes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_detalhes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_detalhes_Enabled), 5, 0)));
         chkavAtributos_pk.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_pk_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAtributos_pk.Enabled), 5, 0)));
         chkavAtributos_fk.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_fk_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAtributos_fk.Enabled), 5, 0)));
         edtavAtributos_tabelanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_tabelanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_tabelanom_Enabled), 5, 0)));
         cmbavFuncaodados_tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaodados_tipo.Enabled), 5, 0)));
         GXVvFUNCAODADOS_CODIGO_htmlAK2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11AK2 */
         E11AK2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV8FiltroAtributos_Nome = StringUtil.Upper( cgiGet( edtavFiltroatributos_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FiltroAtributos_Nome", AV8FiltroAtributos_Nome);
            /* Read saved values. */
            nRC_GXsfl_16 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_16"), ",", "."));
            wcpOAV12FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV12FuncaoAPF_Codigo"), ",", "."));
            wcpOAV17Tabela_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV17Tabela_SistemaCod"), ",", "."));
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage"), ",", "."));
            GRIDATRIBUTOS_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_nEOF"), ",", "."));
            subGridatributos_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11AK2 */
         E11AK2 ();
         if (returnInSub) return;
      }

      protected void E11AK2( )
      {
         /* Start Routine */
         subGridatributos_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         AV16InsertAtributo = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavInsertatributo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16InsertAtributo)) ? AV24Insertatributo_GXI : context.convertURL( context.PathToRelativeUrl( AV16InsertAtributo))));
         AV24Insertatributo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavInsertatributo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16InsertAtributo)) ? AV24Insertatributo_GXI : context.convertURL( context.PathToRelativeUrl( AV16InsertAtributo))));
         edtavInsertatributo_Tooltiptext = "Vincular atributo";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavInsertatributo_Internalname, "Tooltiptext", edtavInsertatributo_Tooltiptext);
      }

      protected void E12AK2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         /* Using cursor H00AK3 */
         pr_default.execute(1, new Object[] {AV17Tabela_SistemaCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A391FuncaoDados_FuncaoDadosCod = H00AK3_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = H00AK3_n391FuncaoDados_FuncaoDadosCod[0];
            A370FuncaoDados_SistemaCod = H00AK3_A370FuncaoDados_SistemaCod[0];
            AV19FuncaDadosAIE.Add(A391FuncaoDados_FuncaoDadosCod, 0);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV19FuncaDadosAIE", AV19FuncaDadosAIE);
      }

      private void E13AK2( )
      {
         /* Gridatributos_Load Routine */
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A368FuncaoDados_Codigo ,
                                              AV19FuncaDadosAIE ,
                                              A370FuncaoDados_SistemaCod ,
                                              AV17Tabela_SistemaCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00AK4 */
         pr_default.execute(2, new Object[] {AV17Tabela_SistemaCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A172Tabela_Codigo = H00AK4_A172Tabela_Codigo[0];
            A368FuncaoDados_Codigo = H00AK4_A368FuncaoDados_Codigo[0];
            A370FuncaoDados_SistemaCod = H00AK4_A370FuncaoDados_SistemaCod[0];
            A391FuncaoDados_FuncaoDadosCod = H00AK4_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = H00AK4_n391FuncaoDados_FuncaoDadosCod[0];
            A373FuncaoDados_Tipo = H00AK4_A373FuncaoDados_Tipo[0];
            A370FuncaoDados_SistemaCod = H00AK4_A370FuncaoDados_SistemaCod[0];
            A391FuncaoDados_FuncaoDadosCod = H00AK4_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = H00AK4_n391FuncaoDados_FuncaoDadosCod[0];
            A373FuncaoDados_Tipo = H00AK4_A373FuncaoDados_Tipo[0];
            if ( H00AK4_n391FuncaoDados_FuncaoDadosCod[0] )
            {
               AV18FuncaoDados_Codigo = A368FuncaoDados_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, dynavFuncaodados_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV18FuncaoDados_Codigo), 6, 0)));
               AV21FuncaoDados_Tipo = A373FuncaoDados_Tipo;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaodados_tipo_Internalname, AV21FuncaoDados_Tipo);
            }
            else
            {
               AV20FuncaoDados_FuncaoDadosCod = A368FuncaoDados_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20FuncaoDados_FuncaoDadosCod), 6, 0)));
               AV21FuncaoDados_Tipo = "AIE";
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaodados_tipo_Internalname, AV21FuncaoDados_Tipo);
               /* Execute user subroutine: 'FUNCAOAIE' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  returnInSub = true;
                  if (true) return;
               }
            }
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 AV8FiltroAtributos_Nome ,
                                                 A177Atributos_Nome ,
                                                 A180Atributos_Ativo ,
                                                 A172Tabela_Codigo ,
                                                 A356Atributos_TabelaCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV8FiltroAtributos_Nome = StringUtil.PadR( StringUtil.RTrim( AV8FiltroAtributos_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FiltroAtributos_Nome", AV8FiltroAtributos_Nome);
            /* Using cursor H00AK5 */
            pr_default.execute(3, new Object[] {A172Tabela_Codigo, lV8FiltroAtributos_Nome});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A356Atributos_TabelaCod = H00AK5_A356Atributos_TabelaCod[0];
               A176Atributos_Codigo = H00AK5_A176Atributos_Codigo[0];
               A177Atributos_Nome = H00AK5_A177Atributos_Nome[0];
               A180Atributos_Ativo = H00AK5_A180Atributos_Ativo[0];
               A178Atributos_TipoDados = H00AK5_A178Atributos_TipoDados[0];
               n178Atributos_TipoDados = H00AK5_n178Atributos_TipoDados[0];
               A390Atributos_Detalhes = H00AK5_A390Atributos_Detalhes[0];
               n390Atributos_Detalhes = H00AK5_n390Atributos_Detalhes[0];
               A400Atributos_PK = H00AK5_A400Atributos_PK[0];
               n400Atributos_PK = H00AK5_n400Atributos_PK[0];
               A401Atributos_FK = H00AK5_A401Atributos_FK[0];
               n401Atributos_FK = H00AK5_n401Atributos_FK[0];
               A357Atributos_TabelaNom = H00AK5_A357Atributos_TabelaNom[0];
               n357Atributos_TabelaNom = H00AK5_n357Atributos_TabelaNom[0];
               A357Atributos_TabelaNom = H00AK5_A357Atributos_TabelaNom[0];
               n357Atributos_TabelaNom = H00AK5_n357Atributos_TabelaNom[0];
               AV28GXLvl47 = 0;
               /* Using cursor H00AK6 */
               pr_default.execute(4, new Object[] {AV12FuncaoAPF_Codigo, A176Atributos_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A364FuncaoAPFAtributos_AtributosCod = H00AK6_A364FuncaoAPFAtributos_AtributosCod[0];
                  A165FuncaoAPF_Codigo = H00AK6_A165FuncaoAPF_Codigo[0];
                  AV28GXLvl47 = 1;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(4);
               if ( AV28GXLvl47 == 0 )
               {
                  AV5Atributos_Codigo = A176Atributos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtributos_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Atributos_Codigo), 6, 0)));
                  AV15Atributos_Nome = A177Atributos_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtributos_nome_Internalname, AV15Atributos_Nome);
                  AV11Atributos_TipoDados = A178Atributos_TipoDados;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavAtributos_tipodados_Internalname, AV11Atributos_TipoDados);
                  AV6Atributos_Detalhes = A390Atributos_Detalhes;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtributos_detalhes_Internalname, AV6Atributos_Detalhes);
                  AV9Atributos_PK = A400Atributos_PK;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavAtributos_pk_Internalname, AV9Atributos_PK);
                  AV7Atributos_FK = A401Atributos_FK;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavAtributos_fk_Internalname, AV7Atributos_FK);
                  AV10Atributos_TabelaNom = A357Atributos_TabelaNom;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtributos_tabelanom_Internalname, AV10Atributos_TabelaNom);
                  AV14ImagemInsert = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
                  AV29Imageminsert_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 16;
                  }
                  if ( ( subGridatributos_Islastpage == 1 ) || ( subGridatributos_Rows == 0 ) || ( ( GRIDATRIBUTOS_nCurrentRecord >= GRIDATRIBUTOS_nFirstRecordOnPage ) && ( GRIDATRIBUTOS_nCurrentRecord < GRIDATRIBUTOS_nFirstRecordOnPage + subGridatributos_Recordsperpage( ) ) ) )
                  {
                     sendrow_162( ) ;
                     GRIDATRIBUTOS_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ".", "")));
                     if ( ( subGridatributos_Islastpage == 1 ) && ( ((int)((GRIDATRIBUTOS_nCurrentRecord) % (subGridatributos_Recordsperpage( )))) == 0 ) )
                     {
                        GRIDATRIBUTOS_nFirstRecordOnPage = GRIDATRIBUTOS_nCurrentRecord;
                     }
                  }
                  if ( GRIDATRIBUTOS_nCurrentRecord >= GRIDATRIBUTOS_nFirstRecordOnPage + subGridatributos_Recordsperpage( ) )
                  {
                     GRIDATRIBUTOS_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ".", "")));
                  }
                  GRIDATRIBUTOS_nCurrentRecord = (long)(GRIDATRIBUTOS_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_16_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(16, GridatributosRow);
                  }
               }
               pr_default.readNext(3);
            }
            pr_default.close(3);
            pr_default.readNext(2);
         }
         pr_default.close(2);
         dynavFuncaodados_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18FuncaoDados_Codigo), 6, 0));
         cmbavFuncaodados_tipo.CurrentValue = StringUtil.RTrim( AV21FuncaoDados_Tipo);
         cmbavAtributos_tipodados.CurrentValue = StringUtil.RTrim( AV11Atributos_TipoDados);
      }

      protected void E14AK2( )
      {
         /* 'InsertAtributo' Routine */
         AV13FuncoesAPFAtributos = new SdtFuncoesAPFAtributos(context);
         AV13FuncoesAPFAtributos.gxTpr_Funcaoapf_codigo = AV12FuncaoAPF_Codigo;
         AV13FuncoesAPFAtributos.gxTpr_Funcaoapfatributos_atributoscod = AV5Atributos_Codigo;
         AV13FuncoesAPFAtributos.gxTpr_Funcaoapfatributos_funcaodadoscod = AV18FuncaoDados_Codigo;
         AV13FuncoesAPFAtributos.Save();
         if ( AV13FuncoesAPFAtributos.Success() )
         {
            context.CommitDataStores( "WC_FuncaoAPFAtributosIns");
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else
         {
            context.RollbackDataStores( "WC_FuncaoAPFAtributosIns");
         }
      }

      protected void S114( )
      {
         /* 'FUNCAOAIE' Routine */
         /* Using cursor H00AK7 */
         pr_default.execute(5, new Object[] {AV20FuncaoDados_FuncaoDadosCod});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A391FuncaoDados_FuncaoDadosCod = H00AK7_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = H00AK7_n391FuncaoDados_FuncaoDadosCod[0];
            A368FuncaoDados_Codigo = H00AK7_A368FuncaoDados_Codigo[0];
            AV18FuncaoDados_Codigo = A368FuncaoDados_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, dynavFuncaodados_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV18FuncaoDados_Codigo), 6, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void wb_table1_2_AK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table2_5_AK2( true) ;
         }
         else
         {
            wb_table2_5_AK2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_AK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table3_13_AK2( true) ;
         }
         else
         {
            wb_table3_13_AK2( false) ;
         }
         return  ;
      }

      protected void wb_table3_13_AK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_AK2e( true) ;
         }
         else
         {
            wb_table1_2_AK2e( false) ;
         }
      }

      protected void wb_table3_13_AK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /*  Grid Control  */
            GridatributosContainer.SetWrapped(nGXWrapped);
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridatributosContainer"+"DivS\" data-gxgridid=\"16\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridatributos_Internalname, subGridatributos_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridatributos_Backcolorstyle == 0 )
               {
                  subGridatributos_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridatributos_Class) > 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Title";
                  }
               }
               else
               {
                  subGridatributos_Titlebackstyle = 1;
                  if ( subGridatributos_Backcolorstyle == 1 )
                  {
                     subGridatributos_Titlebackcolor = subGridatributos_Allbackcolor;
                     if ( StringUtil.Len( subGridatributos_Class) > 0 )
                     {
                        subGridatributos_Linesclass = subGridatributos_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridatributos_Class) > 0 )
                     {
                        subGridatributos_Linesclass = subGridatributos_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Fun��o de Transa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo Atr.") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tamanho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PK") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "FK") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tabela") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo Fun.") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
            }
            else
            {
               GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
               GridatributosContainer.AddObjectProperty("Class", "WorkWith");
               GridatributosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Backcolorstyle), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("CmpContext", sPrefix);
               GridatributosContainer.AddObjectProperty("InMasterPage", "false");
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18FuncaoDados_Codigo), 6, 0, ".", "")));
               GridatributosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynavFuncaodados_codigo.Enabled), 5, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12FuncaoAPF_Codigo), 6, 0, ".", "")));
               GridatributosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFuncaoapf_codigo_Enabled), 5, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5Atributos_Codigo), 6, 0, ".", "")));
               GridatributosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAtributos_codigo_Enabled), 5, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", context.convertURL( AV16InsertAtributo));
               GridatributosColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavInsertatributo_Tooltiptext));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( AV15Atributos_Nome));
               GridatributosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAtributos_nome_Enabled), 5, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( AV11Atributos_TipoDados));
               GridatributosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavAtributos_tipodados.Enabled), 5, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( AV6Atributos_Detalhes));
               GridatributosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAtributos_detalhes_Enabled), 5, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV9Atributos_PK));
               GridatributosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkavAtributos_pk.Enabled), 5, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV7Atributos_FK));
               GridatributosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkavAtributos_fk.Enabled), 5, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( AV10Atributos_TabelaNom));
               GridatributosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAtributos_tabelanom_Enabled), 5, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( AV21FuncaoDados_Tipo));
               GridatributosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavFuncaodados_tipo.Enabled), 5, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowselection), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Selectioncolor), 9, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowhovering), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Hoveringcolor), 9, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowcollapsing), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 16 )
         {
            wbEnd = 0;
            nRC_GXsfl_16 = (short)(nGXsfl_16_idx-1);
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridatributosContainer.AddObjectProperty("GRIDATRIBUTOS_nEOF", GRIDATRIBUTOS_nEOF);
               GridatributosContainer.AddObjectProperty("GRIDATRIBUTOS_nFirstRecordOnPage", GRIDATRIBUTOS_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridatributosContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Gridatributos", GridatributosContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridatributosContainerData", GridatributosContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridatributosContainerData"+"V", GridatributosContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridatributosContainerData"+"V"+"\" value='"+GridatributosContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_13_AK2e( true) ;
         }
         else
         {
            wb_table3_13_AK2e( false) ;
         }
      }

      protected void wb_table2_5_AK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfiltroatributos_nome_Internalname, "Filtrar Atributos contendo:", "", "", lblTextblockfiltroatributos_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WC_FuncaoAPFAtributosIns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltroatributos_nome_Internalname, StringUtil.RTrim( AV8FiltroAtributos_Nome), StringUtil.RTrim( context.localUtil.Format( AV8FiltroAtributos_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,10);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltroatributos_nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WC_FuncaoAPFAtributosIns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_AK2e( true) ;
         }
         else
         {
            wb_table2_5_AK2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV12FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12FuncaoAPF_Codigo), 6, 0)));
         AV17Tabela_SistemaCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Tabela_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAAK2( ) ;
         WSAK2( ) ;
         WEAK2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV12FuncaoAPF_Codigo = (String)((String)getParm(obj,0));
         sCtrlAV17Tabela_SistemaCod = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAAK2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_funcaoapfatributosins");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAAK2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV12FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12FuncaoAPF_Codigo), 6, 0)));
            AV17Tabela_SistemaCod = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Tabela_SistemaCod), 6, 0)));
         }
         wcpOAV12FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV12FuncaoAPF_Codigo"), ",", "."));
         wcpOAV17Tabela_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV17Tabela_SistemaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV12FuncaoAPF_Codigo != wcpOAV12FuncaoAPF_Codigo ) || ( AV17Tabela_SistemaCod != wcpOAV17Tabela_SistemaCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV12FuncaoAPF_Codigo = AV12FuncaoAPF_Codigo;
         wcpOAV17Tabela_SistemaCod = AV17Tabela_SistemaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV12FuncaoAPF_Codigo = cgiGet( sPrefix+"AV12FuncaoAPF_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV12FuncaoAPF_Codigo) > 0 )
         {
            AV12FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV12FuncaoAPF_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12FuncaoAPF_Codigo), 6, 0)));
         }
         else
         {
            AV12FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV12FuncaoAPF_Codigo_PARM"), ",", "."));
         }
         sCtrlAV17Tabela_SistemaCod = cgiGet( sPrefix+"AV17Tabela_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV17Tabela_SistemaCod) > 0 )
         {
            AV17Tabela_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV17Tabela_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Tabela_SistemaCod), 6, 0)));
         }
         else
         {
            AV17Tabela_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV17Tabela_SistemaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAAK2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSAK2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSAK2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV12FuncaoAPF_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12FuncaoAPF_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV12FuncaoAPF_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV12FuncaoAPF_Codigo_CTRL", StringUtil.RTrim( sCtrlAV12FuncaoAPF_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV17Tabela_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Tabela_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV17Tabela_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV17Tabela_SistemaCod_CTRL", StringUtil.RTrim( sCtrlAV17Tabela_SistemaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEAK2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020562355214");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("wc_funcaoapfatributosins.js", "?2020562355215");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_162( )
      {
         dynavFuncaodados_codigo_Internalname = sPrefix+"vFUNCAODADOS_CODIGO_"+sGXsfl_16_idx;
         edtavFuncaoapf_codigo_Internalname = sPrefix+"vFUNCAOAPF_CODIGO_"+sGXsfl_16_idx;
         edtavAtributos_codigo_Internalname = sPrefix+"vATRIBUTOS_CODIGO_"+sGXsfl_16_idx;
         edtavInsertatributo_Internalname = sPrefix+"vINSERTATRIBUTO_"+sGXsfl_16_idx;
         edtavAtributos_nome_Internalname = sPrefix+"vATRIBUTOS_NOME_"+sGXsfl_16_idx;
         cmbavAtributos_tipodados_Internalname = sPrefix+"vATRIBUTOS_TIPODADOS_"+sGXsfl_16_idx;
         edtavAtributos_detalhes_Internalname = sPrefix+"vATRIBUTOS_DETALHES_"+sGXsfl_16_idx;
         chkavAtributos_pk_Internalname = sPrefix+"vATRIBUTOS_PK_"+sGXsfl_16_idx;
         chkavAtributos_fk_Internalname = sPrefix+"vATRIBUTOS_FK_"+sGXsfl_16_idx;
         edtavAtributos_tabelanom_Internalname = sPrefix+"vATRIBUTOS_TABELANOM_"+sGXsfl_16_idx;
         cmbavFuncaodados_tipo_Internalname = sPrefix+"vFUNCAODADOS_TIPO_"+sGXsfl_16_idx;
      }

      protected void SubsflControlProps_fel_162( )
      {
         dynavFuncaodados_codigo_Internalname = sPrefix+"vFUNCAODADOS_CODIGO_"+sGXsfl_16_fel_idx;
         edtavFuncaoapf_codigo_Internalname = sPrefix+"vFUNCAOAPF_CODIGO_"+sGXsfl_16_fel_idx;
         edtavAtributos_codigo_Internalname = sPrefix+"vATRIBUTOS_CODIGO_"+sGXsfl_16_fel_idx;
         edtavInsertatributo_Internalname = sPrefix+"vINSERTATRIBUTO_"+sGXsfl_16_fel_idx;
         edtavAtributos_nome_Internalname = sPrefix+"vATRIBUTOS_NOME_"+sGXsfl_16_fel_idx;
         cmbavAtributos_tipodados_Internalname = sPrefix+"vATRIBUTOS_TIPODADOS_"+sGXsfl_16_fel_idx;
         edtavAtributos_detalhes_Internalname = sPrefix+"vATRIBUTOS_DETALHES_"+sGXsfl_16_fel_idx;
         chkavAtributos_pk_Internalname = sPrefix+"vATRIBUTOS_PK_"+sGXsfl_16_fel_idx;
         chkavAtributos_fk_Internalname = sPrefix+"vATRIBUTOS_FK_"+sGXsfl_16_fel_idx;
         edtavAtributos_tabelanom_Internalname = sPrefix+"vATRIBUTOS_TABELANOM_"+sGXsfl_16_fel_idx;
         cmbavFuncaodados_tipo_Internalname = sPrefix+"vFUNCAODADOS_TIPO_"+sGXsfl_16_fel_idx;
      }

      protected void sendrow_162( )
      {
         SubsflControlProps_162( ) ;
         WBAK0( ) ;
         if ( ( subGridatributos_Rows * 1 == 0 ) || ( nGXsfl_16_idx <= subGridatributos_Recordsperpage( ) * 1 ) )
         {
            GridatributosRow = GXWebRow.GetNew(context,GridatributosContainer);
            if ( subGridatributos_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridatributos_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Odd";
               }
            }
            else if ( subGridatributos_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridatributos_Backstyle = 0;
               subGridatributos_Backcolor = subGridatributos_Allbackcolor;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Uniform";
               }
            }
            else if ( subGridatributos_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridatributos_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Odd";
               }
               subGridatributos_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridatributos_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridatributos_Backstyle = 1;
               if ( ((int)((nGXsfl_16_idx) % (2))) == 0 )
               {
                  subGridatributos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Even";
                  }
               }
               else
               {
                  subGridatributos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Odd";
                  }
               }
            }
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridatributos_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_16_idx+"\">") ;
            }
            GXVvFUNCAODADOS_CODIGO_htmlAK2( ) ;
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            TempTags = " " + ((dynavFuncaodados_codigo.Enabled!=0)&&(dynavFuncaodados_codigo.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 17,'"+sPrefix+"',false,'"+sGXsfl_16_idx+"',16)\"" : " ");
            if ( ( nGXsfl_16_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vFUNCAODADOS_CODIGO_" + sGXsfl_16_idx;
               dynavFuncaodados_codigo.Name = GXCCtl;
               dynavFuncaodados_codigo.WebTags = "";
            }
            /* ComboBox */
            GridatributosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynavFuncaodados_codigo,(String)dynavFuncaodados_codigo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV18FuncaoDados_Codigo), 6, 0)),(short)1,(String)dynavFuncaodados_codigo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)0,dynavFuncaodados_codigo.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((dynavFuncaodados_codigo.Enabled!=0)&&(dynavFuncaodados_codigo.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((dynavFuncaodados_codigo.Enabled!=0)&&(dynavFuncaodados_codigo.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,17);\"" : " "),(String)"",(bool)true});
            dynavFuncaodados_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18FuncaoDados_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavFuncaodados_codigo_Internalname, "Values", (String)(dynavFuncaodados_codigo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFuncaoapf_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12FuncaoAPF_Codigo), 6, 0, ",", "")),((edtavFuncaoapf_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12FuncaoAPF_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV12FuncaoAPF_Codigo), "ZZZZZ9")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFuncaoapf_codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavFuncaoapf_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)16,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavAtributos_codigo_Enabled!=0)&&(edtavAtributos_codigo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 19,'"+sPrefix+"',false,'"+sGXsfl_16_idx+"',16)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAtributos_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5Atributos_Codigo), 6, 0, ",", "")),((edtavAtributos_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV5Atributos_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV5Atributos_Codigo), "ZZZZZ9")),TempTags+((edtavAtributos_codigo_Enabled!=0)&&(edtavAtributos_codigo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavAtributos_codigo_Enabled!=0)&&(edtavAtributos_codigo_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,19);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAtributos_codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavAtributos_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)16,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavInsertatributo_Enabled!=0)&&(edtavInsertatributo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 20,'"+sPrefix+"',false,'',16)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV16InsertAtributo_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16InsertAtributo))&&String.IsNullOrEmpty(StringUtil.RTrim( AV24Insertatributo_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16InsertAtributo)));
            GridatributosRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavInsertatributo_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16InsertAtributo)) ? AV24Insertatributo_GXI : context.PathToRelativeUrl( AV16InsertAtributo)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavInsertatributo_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavInsertatributo_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'INSERTATRIBUTO\\'."+sGXsfl_16_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV16InsertAtributo_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavAtributos_nome_Enabled!=0)&&(edtavAtributos_nome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 21,'"+sPrefix+"',false,'"+sGXsfl_16_idx+"',16)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAtributos_nome_Internalname,StringUtil.RTrim( AV15Atributos_Nome),StringUtil.RTrim( context.localUtil.Format( AV15Atributos_Nome, "@!")),TempTags+((edtavAtributos_nome_Enabled!=0)&&(edtavAtributos_nome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavAtributos_nome_Enabled!=0)&&(edtavAtributos_nome_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,21);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAtributos_nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavAtributos_nome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)16,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            TempTags = " " + ((cmbavAtributos_tipodados.Enabled!=0)&&(cmbavAtributos_tipodados.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 22,'"+sPrefix+"',false,'"+sGXsfl_16_idx+"',16)\"" : " ");
            if ( ( nGXsfl_16_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vATRIBUTOS_TIPODADOS_" + sGXsfl_16_idx;
               cmbavAtributos_tipodados.Name = GXCCtl;
               cmbavAtributos_tipodados.WebTags = "";
               cmbavAtributos_tipodados.addItem("", "Desconhecido", 0);
               cmbavAtributos_tipodados.addItem("N", "Numeric", 0);
               cmbavAtributos_tipodados.addItem("C", "Character", 0);
               cmbavAtributos_tipodados.addItem("VC", "Varchar", 0);
               cmbavAtributos_tipodados.addItem("D", "Date", 0);
               cmbavAtributos_tipodados.addItem("DT", "Date Time", 0);
               cmbavAtributos_tipodados.addItem("Bool", "Boolean", 0);
               cmbavAtributos_tipodados.addItem("Blob", "Blob", 0);
               cmbavAtributos_tipodados.addItem("Outr", "Outros", 0);
               if ( cmbavAtributos_tipodados.ItemCount > 0 )
               {
                  AV11Atributos_TipoDados = cmbavAtributos_tipodados.getValidValue(AV11Atributos_TipoDados);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavAtributos_tipodados_Internalname, AV11Atributos_TipoDados);
               }
            }
            /* ComboBox */
            GridatributosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavAtributos_tipodados,(String)cmbavAtributos_tipodados_Internalname,StringUtil.RTrim( AV11Atributos_TipoDados),(short)1,(String)cmbavAtributos_tipodados_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbavAtributos_tipodados.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",TempTags+((cmbavAtributos_tipodados.Enabled!=0)&&(cmbavAtributos_tipodados.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavAtributos_tipodados.Enabled!=0)&&(cmbavAtributos_tipodados.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,22);\"" : " "),(String)"",(bool)true});
            cmbavAtributos_tipodados.CurrentValue = StringUtil.RTrim( AV11Atributos_TipoDados);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados_Internalname, "Values", (String)(cmbavAtributos_tipodados.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavAtributos_detalhes_Enabled!=0)&&(edtavAtributos_detalhes_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 23,'"+sPrefix+"',false,'"+sGXsfl_16_idx+"',16)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAtributos_detalhes_Internalname,StringUtil.RTrim( AV6Atributos_Detalhes),(String)"",TempTags+((edtavAtributos_detalhes_Enabled!=0)&&(edtavAtributos_detalhes_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavAtributos_detalhes_Enabled!=0)&&(edtavAtributos_detalhes_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,23);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAtributos_detalhes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavAtributos_detalhes_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)16,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavAtributos_pk.Enabled!=0)&&(chkavAtributos_pk.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 24,'"+sPrefix+"',false,'"+sGXsfl_16_idx+"',16)\"" : " ");
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GridatributosRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavAtributos_pk_Internalname,StringUtil.BoolToStr( AV9Atributos_PK),(String)"",(String)"",(short)-1,chkavAtributos_pk.Enabled,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavAtributos_pk.Enabled!=0)&&(chkavAtributos_pk.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(24, this, 'true', 'false');gx.evt.onchange(this);\" " : " ")+((chkavAtributos_pk.Enabled!=0)&&(chkavAtributos_pk.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,24);\"" : " ")});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavAtributos_fk.Enabled!=0)&&(chkavAtributos_fk.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 25,'"+sPrefix+"',false,'"+sGXsfl_16_idx+"',16)\"" : " ");
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GridatributosRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavAtributos_fk_Internalname,StringUtil.BoolToStr( AV7Atributos_FK),(String)"",(String)"",(short)-1,chkavAtributos_fk.Enabled,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavAtributos_fk.Enabled!=0)&&(chkavAtributos_fk.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(25, this, 'true', 'false');gx.evt.onchange(this);\" " : " ")+((chkavAtributos_fk.Enabled!=0)&&(chkavAtributos_fk.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,25);\"" : " ")});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavAtributos_tabelanom_Enabled!=0)&&(edtavAtributos_tabelanom_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 26,'"+sPrefix+"',false,'"+sGXsfl_16_idx+"',16)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAtributos_tabelanom_Internalname,StringUtil.RTrim( AV10Atributos_TabelaNom),StringUtil.RTrim( context.localUtil.Format( AV10Atributos_TabelaNom, "@!")),TempTags+((edtavAtributos_tabelanom_Enabled!=0)&&(edtavAtributos_tabelanom_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavAtributos_tabelanom_Enabled!=0)&&(edtavAtributos_tabelanom_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,26);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAtributos_tabelanom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavAtributos_tabelanom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)16,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            TempTags = " " + ((cmbavFuncaodados_tipo.Enabled!=0)&&(cmbavFuncaodados_tipo.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 27,'"+sPrefix+"',false,'"+sGXsfl_16_idx+"',16)\"" : " ");
            if ( ( nGXsfl_16_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vFUNCAODADOS_TIPO_" + sGXsfl_16_idx;
               cmbavFuncaodados_tipo.Name = GXCCtl;
               cmbavFuncaodados_tipo.WebTags = "";
               cmbavFuncaodados_tipo.addItem("", "(Nenhum)", 0);
               cmbavFuncaodados_tipo.addItem("ALI", "ALI", 0);
               cmbavFuncaodados_tipo.addItem("AIE", "AIE", 0);
               cmbavFuncaodados_tipo.addItem("DC", "DC", 0);
               if ( cmbavFuncaodados_tipo.ItemCount > 0 )
               {
                  AV21FuncaoDados_Tipo = cmbavFuncaodados_tipo.getValidValue(AV21FuncaoDados_Tipo);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaodados_tipo_Internalname, AV21FuncaoDados_Tipo);
               }
            }
            /* ComboBox */
            GridatributosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavFuncaodados_tipo,(String)cmbavFuncaodados_tipo_Internalname,StringUtil.RTrim( AV21FuncaoDados_Tipo),(short)1,(String)cmbavFuncaodados_tipo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbavFuncaodados_tipo.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavFuncaodados_tipo.Enabled!=0)&&(cmbavFuncaodados_tipo.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavFuncaodados_tipo.Enabled!=0)&&(cmbavFuncaodados_tipo.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,27);\"" : " "),(String)"",(bool)true});
            cmbavFuncaodados_tipo.CurrentValue = StringUtil.RTrim( AV21FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo_Internalname, "Values", (String)(cmbavFuncaodados_tipo.ToJavascriptSource()));
            GridatributosContainer.AddRow(GridatributosRow);
            nGXsfl_16_idx = (short)(((subGridatributos_Islastpage==1)&&(nGXsfl_16_idx+1>subGridatributos_Recordsperpage( )) ? 1 : nGXsfl_16_idx+1));
            sGXsfl_16_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_16_idx), 4, 0)), 4, "0");
            SubsflControlProps_162( ) ;
         }
         /* End function sendrow_162 */
      }

      protected void init_default_properties( )
      {
         lblTextblockfiltroatributos_nome_Internalname = sPrefix+"TEXTBLOCKFILTROATRIBUTOS_NOME";
         edtavFiltroatributos_nome_Internalname = sPrefix+"vFILTROATRIBUTOS_NOME";
         tblTable1_Internalname = sPrefix+"TABLE1";
         dynavFuncaodados_codigo_Internalname = sPrefix+"vFUNCAODADOS_CODIGO";
         edtavFuncaoapf_codigo_Internalname = sPrefix+"vFUNCAOAPF_CODIGO";
         edtavAtributos_codigo_Internalname = sPrefix+"vATRIBUTOS_CODIGO";
         edtavInsertatributo_Internalname = sPrefix+"vINSERTATRIBUTO";
         edtavAtributos_nome_Internalname = sPrefix+"vATRIBUTOS_NOME";
         cmbavAtributos_tipodados_Internalname = sPrefix+"vATRIBUTOS_TIPODADOS";
         edtavAtributos_detalhes_Internalname = sPrefix+"vATRIBUTOS_DETALHES";
         chkavAtributos_pk_Internalname = sPrefix+"vATRIBUTOS_PK";
         chkavAtributos_fk_Internalname = sPrefix+"vATRIBUTOS_FK";
         edtavAtributos_tabelanom_Internalname = sPrefix+"vATRIBUTOS_TABELANOM";
         cmbavFuncaodados_tipo_Internalname = sPrefix+"vFUNCAODADOS_TIPO";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         tblTablemain_Internalname = sPrefix+"TABLEMAIN";
         Form.Internalname = sPrefix+"FORM";
         subGridatributos_Internalname = sPrefix+"GRIDATRIBUTOS";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbavFuncaodados_tipo_Jsonclick = "";
         cmbavFuncaodados_tipo.Visible = -1;
         edtavAtributos_tabelanom_Jsonclick = "";
         edtavAtributos_tabelanom_Visible = -1;
         chkavAtributos_fk.Visible = -1;
         chkavAtributos_pk.Visible = -1;
         edtavAtributos_detalhes_Jsonclick = "";
         edtavAtributos_detalhes_Visible = -1;
         cmbavAtributos_tipodados_Jsonclick = "";
         cmbavAtributos_tipodados.Visible = -1;
         edtavAtributos_nome_Jsonclick = "";
         edtavAtributos_nome_Visible = -1;
         edtavInsertatributo_Jsonclick = "";
         edtavInsertatributo_Visible = -1;
         edtavInsertatributo_Enabled = 1;
         edtavAtributos_codigo_Jsonclick = "";
         edtavAtributos_codigo_Visible = 0;
         edtavFuncaoapf_codigo_Jsonclick = "";
         dynavFuncaodados_codigo_Jsonclick = "";
         dynavFuncaodados_codigo.Visible = 0;
         edtavFiltroatributos_nome_Jsonclick = "";
         subGridatributos_Allowcollapsing = 0;
         subGridatributos_Allowselection = 0;
         cmbavFuncaodados_tipo.Enabled = 1;
         edtavAtributos_tabelanom_Enabled = 1;
         chkavAtributos_fk.Enabled = 1;
         chkavAtributos_pk.Enabled = 1;
         edtavAtributos_detalhes_Enabled = 1;
         cmbavAtributos_tipodados.Enabled = 1;
         edtavAtributos_nome_Enabled = 1;
         edtavAtributos_codigo_Enabled = 1;
         edtavFuncaoapf_codigo_Enabled = 0;
         dynavFuncaodados_codigo.Enabled = 1;
         subGridatributos_Class = "WorkWith";
         subGridatributos_Backcolorstyle = 3;
         chkavAtributos_fk.Caption = "";
         chkavAtributos_pk.Caption = "";
         subGridatributos_Rows = 0;
         edtavInsertatributo_Tooltiptext = "Vincular atributo";
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV16InsertAtributo',fld:'vINSERTATRIBUTO',pic:'',nv:''},{av:'edtavInsertatributo_Tooltiptext',ctrl:'vINSERTATRIBUTO',prop:'Tooltiptext'},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV8FiltroAtributos_Nome',fld:'vFILTROATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'A390Atributos_Detalhes',fld:'ATRIBUTOS_DETALHES',pic:'',nv:''},{av:'A400Atributos_PK',fld:'ATRIBUTOS_PK',pic:'',nv:false},{av:'A401Atributos_FK',fld:'ATRIBUTOS_FK',pic:'',nv:false},{av:'A357Atributos_TabelaNom',fld:'ATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV20FuncaoDados_FuncaoDadosCod',fld:'vFUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV17Tabela_SistemaCod',fld:'vTABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaDadosAIE',fld:'vFUNCADADOSAIE',pic:'',nv:null}],oparms:[{av:'AV19FuncaDadosAIE',fld:'vFUNCADADOSAIE',pic:'',nv:null}]}");
         setEventMetadata("GRIDATRIBUTOS.LOAD","{handler:'E13AK2',iparms:[{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV17Tabela_SistemaCod',fld:'vTABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaDadosAIE',fld:'vFUNCADADOSAIE',pic:'',nv:null},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV8FiltroAtributos_Nome',fld:'vFILTROATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'A390Atributos_Detalhes',fld:'ATRIBUTOS_DETALHES',pic:'',nv:''},{av:'A400Atributos_PK',fld:'ATRIBUTOS_PK',pic:'',nv:false},{av:'A401Atributos_FK',fld:'ATRIBUTOS_FK',pic:'',nv:false},{av:'A357Atributos_TabelaNom',fld:'ATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV20FuncaoDados_FuncaoDadosCod',fld:'vFUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV18FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20FuncaoDados_FuncaoDadosCod',fld:'vFUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV21FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15Atributos_Nome',fld:'vATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV11Atributos_TipoDados',fld:'vATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'AV6Atributos_Detalhes',fld:'vATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV9Atributos_PK',fld:'vATRIBUTOS_PK',pic:'',nv:false},{av:'AV7Atributos_FK',fld:'vATRIBUTOS_FK',pic:'',nv:false},{av:'AV10Atributos_TabelaNom',fld:'vATRIBUTOS_TABELANOM',pic:'@!',nv:''}]}");
         setEventMetadata("'INSERTATRIBUTO'","{handler:'E14AK2',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV16InsertAtributo',fld:'vINSERTATRIBUTO',pic:'',nv:''},{av:'edtavInsertatributo_Tooltiptext',ctrl:'vINSERTATRIBUTO',prop:'Tooltiptext'},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV17Tabela_SistemaCod',fld:'vTABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaDadosAIE',fld:'vFUNCADADOSAIE',pic:'',nv:null},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV8FiltroAtributos_Nome',fld:'vFILTROATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'A390Atributos_Detalhes',fld:'ATRIBUTOS_DETALHES',pic:'',nv:''},{av:'A400Atributos_PK',fld:'ATRIBUTOS_PK',pic:'',nv:false},{av:'A401Atributos_FK',fld:'ATRIBUTOS_FK',pic:'',nv:false},{av:'A357Atributos_TabelaNom',fld:'ATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV20FuncaoDados_FuncaoDadosCod',fld:'vFUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV18FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("GRIDATRIBUTOS_FIRSTPAGE","{handler:'subgridatributos_firstpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV16InsertAtributo',fld:'vINSERTATRIBUTO',pic:'',nv:''},{av:'edtavInsertatributo_Tooltiptext',ctrl:'vINSERTATRIBUTO',prop:'Tooltiptext'},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV8FiltroAtributos_Nome',fld:'vFILTROATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'A390Atributos_Detalhes',fld:'ATRIBUTOS_DETALHES',pic:'',nv:''},{av:'A400Atributos_PK',fld:'ATRIBUTOS_PK',pic:'',nv:false},{av:'A401Atributos_FK',fld:'ATRIBUTOS_FK',pic:'',nv:false},{av:'A357Atributos_TabelaNom',fld:'ATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV20FuncaoDados_FuncaoDadosCod',fld:'vFUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV17Tabela_SistemaCod',fld:'vTABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaDadosAIE',fld:'vFUNCADADOSAIE',pic:'',nv:null}],oparms:[{av:'AV19FuncaDadosAIE',fld:'vFUNCADADOSAIE',pic:'',nv:null}]}");
         setEventMetadata("GRIDATRIBUTOS_PREVPAGE","{handler:'subgridatributos_previouspage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV16InsertAtributo',fld:'vINSERTATRIBUTO',pic:'',nv:''},{av:'edtavInsertatributo_Tooltiptext',ctrl:'vINSERTATRIBUTO',prop:'Tooltiptext'},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV8FiltroAtributos_Nome',fld:'vFILTROATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'A390Atributos_Detalhes',fld:'ATRIBUTOS_DETALHES',pic:'',nv:''},{av:'A400Atributos_PK',fld:'ATRIBUTOS_PK',pic:'',nv:false},{av:'A401Atributos_FK',fld:'ATRIBUTOS_FK',pic:'',nv:false},{av:'A357Atributos_TabelaNom',fld:'ATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV20FuncaoDados_FuncaoDadosCod',fld:'vFUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV17Tabela_SistemaCod',fld:'vTABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaDadosAIE',fld:'vFUNCADADOSAIE',pic:'',nv:null}],oparms:[{av:'AV19FuncaDadosAIE',fld:'vFUNCADADOSAIE',pic:'',nv:null}]}");
         setEventMetadata("GRIDATRIBUTOS_NEXTPAGE","{handler:'subgridatributos_nextpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV16InsertAtributo',fld:'vINSERTATRIBUTO',pic:'',nv:''},{av:'edtavInsertatributo_Tooltiptext',ctrl:'vINSERTATRIBUTO',prop:'Tooltiptext'},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV8FiltroAtributos_Nome',fld:'vFILTROATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'A390Atributos_Detalhes',fld:'ATRIBUTOS_DETALHES',pic:'',nv:''},{av:'A400Atributos_PK',fld:'ATRIBUTOS_PK',pic:'',nv:false},{av:'A401Atributos_FK',fld:'ATRIBUTOS_FK',pic:'',nv:false},{av:'A357Atributos_TabelaNom',fld:'ATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV20FuncaoDados_FuncaoDadosCod',fld:'vFUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV17Tabela_SistemaCod',fld:'vTABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaDadosAIE',fld:'vFUNCADADOSAIE',pic:'',nv:null}],oparms:[{av:'AV19FuncaDadosAIE',fld:'vFUNCADADOSAIE',pic:'',nv:null}]}");
         setEventMetadata("GRIDATRIBUTOS_LASTPAGE","{handler:'subgridatributos_lastpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV16InsertAtributo',fld:'vINSERTATRIBUTO',pic:'',nv:''},{av:'edtavInsertatributo_Tooltiptext',ctrl:'vINSERTATRIBUTO',prop:'Tooltiptext'},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV8FiltroAtributos_Nome',fld:'vFILTROATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'A390Atributos_Detalhes',fld:'ATRIBUTOS_DETALHES',pic:'',nv:''},{av:'A400Atributos_PK',fld:'ATRIBUTOS_PK',pic:'',nv:false},{av:'A401Atributos_FK',fld:'ATRIBUTOS_FK',pic:'',nv:false},{av:'A357Atributos_TabelaNom',fld:'ATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV20FuncaoDados_FuncaoDadosCod',fld:'vFUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV17Tabela_SistemaCod',fld:'vTABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaDadosAIE',fld:'vFUNCADADOSAIE',pic:'',nv:null}],oparms:[{av:'AV19FuncaDadosAIE',fld:'vFUNCADADOSAIE',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16InsertAtributo = "";
         AV24Insertatributo_GXI = "";
         AV19FuncaDadosAIE = new GxSimpleCollection();
         A373FuncaoDados_Tipo = "";
         A177Atributos_Nome = "";
         AV8FiltroAtributos_Nome = "";
         A178Atributos_TipoDados = "";
         A390Atributos_Detalhes = "";
         A357Atributos_TabelaNom = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15Atributos_Nome = "";
         AV11Atributos_TipoDados = "";
         AV6Atributos_Detalhes = "";
         AV10Atributos_TabelaNom = "";
         AV21FuncaoDados_Tipo = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00AK2_A368FuncaoDados_Codigo = new int[1] ;
         H00AK2_A369FuncaoDados_Nome = new String[] {""} ;
         GridatributosContainer = new GXWebGrid( context);
         H00AK3_A368FuncaoDados_Codigo = new int[1] ;
         H00AK3_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         H00AK3_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         H00AK3_A370FuncaoDados_SistemaCod = new int[1] ;
         H00AK4_A172Tabela_Codigo = new int[1] ;
         H00AK4_A368FuncaoDados_Codigo = new int[1] ;
         H00AK4_A370FuncaoDados_SistemaCod = new int[1] ;
         H00AK4_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         H00AK4_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         H00AK4_A373FuncaoDados_Tipo = new String[] {""} ;
         lV8FiltroAtributos_Nome = "";
         H00AK5_A356Atributos_TabelaCod = new int[1] ;
         H00AK5_A176Atributos_Codigo = new int[1] ;
         H00AK5_A177Atributos_Nome = new String[] {""} ;
         H00AK5_A180Atributos_Ativo = new bool[] {false} ;
         H00AK5_A178Atributos_TipoDados = new String[] {""} ;
         H00AK5_n178Atributos_TipoDados = new bool[] {false} ;
         H00AK5_A390Atributos_Detalhes = new String[] {""} ;
         H00AK5_n390Atributos_Detalhes = new bool[] {false} ;
         H00AK5_A400Atributos_PK = new bool[] {false} ;
         H00AK5_n400Atributos_PK = new bool[] {false} ;
         H00AK5_A401Atributos_FK = new bool[] {false} ;
         H00AK5_n401Atributos_FK = new bool[] {false} ;
         H00AK5_A357Atributos_TabelaNom = new String[] {""} ;
         H00AK5_n357Atributos_TabelaNom = new bool[] {false} ;
         H00AK6_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         H00AK6_A165FuncaoAPF_Codigo = new int[1] ;
         AV14ImagemInsert = "";
         AV29Imageminsert_GXI = "";
         GridatributosRow = new GXWebRow();
         AV13FuncoesAPFAtributos = new SdtFuncoesAPFAtributos(context);
         H00AK7_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         H00AK7_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         H00AK7_A368FuncaoDados_Codigo = new int[1] ;
         sStyleString = "";
         subGridatributos_Linesclass = "";
         GridatributosColumn = new GXWebColumn();
         lblTextblockfiltroatributos_nome_Jsonclick = "";
         TempTags = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV12FuncaoAPF_Codigo = "";
         sCtrlAV17Tabela_SistemaCod = "";
         ROClassString = "";
         ClassString = "";
         StyleString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_funcaoapfatributosins__default(),
            new Object[][] {
                new Object[] {
               H00AK2_A368FuncaoDados_Codigo, H00AK2_A369FuncaoDados_Nome
               }
               , new Object[] {
               H00AK3_A368FuncaoDados_Codigo, H00AK3_A391FuncaoDados_FuncaoDadosCod, H00AK3_n391FuncaoDados_FuncaoDadosCod, H00AK3_A370FuncaoDados_SistemaCod
               }
               , new Object[] {
               H00AK4_A172Tabela_Codigo, H00AK4_A368FuncaoDados_Codigo, H00AK4_A370FuncaoDados_SistemaCod, H00AK4_A391FuncaoDados_FuncaoDadosCod, H00AK4_n391FuncaoDados_FuncaoDadosCod, H00AK4_A373FuncaoDados_Tipo
               }
               , new Object[] {
               H00AK5_A356Atributos_TabelaCod, H00AK5_A176Atributos_Codigo, H00AK5_A177Atributos_Nome, H00AK5_A180Atributos_Ativo, H00AK5_A178Atributos_TipoDados, H00AK5_n178Atributos_TipoDados, H00AK5_A390Atributos_Detalhes, H00AK5_n390Atributos_Detalhes, H00AK5_A400Atributos_PK, H00AK5_n400Atributos_PK,
               H00AK5_A401Atributos_FK, H00AK5_n401Atributos_FK, H00AK5_A357Atributos_TabelaNom, H00AK5_n357Atributos_TabelaNom
               }
               , new Object[] {
               H00AK6_A364FuncaoAPFAtributos_AtributosCod, H00AK6_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               H00AK7_A391FuncaoDados_FuncaoDadosCod, H00AK7_n391FuncaoDados_FuncaoDadosCod, H00AK7_A368FuncaoDados_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         dynavFuncaodados_codigo.Enabled = 0;
         edtavFuncaoapf_codigo_Enabled = 0;
         edtavAtributos_codigo_Enabled = 0;
         edtavAtributos_nome_Enabled = 0;
         cmbavAtributos_tipodados.Enabled = 0;
         edtavAtributos_detalhes_Enabled = 0;
         chkavAtributos_pk.Enabled = 0;
         chkavAtributos_fk.Enabled = 0;
         edtavAtributos_tabelanom_Enabled = 0;
         cmbavFuncaodados_tipo.Enabled = 0;
      }

      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_16 ;
      private short nGXsfl_16_idx=1 ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRIDATRIBUTOS_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_16_Refreshing=0 ;
      private short subGridatributos_Backcolorstyle ;
      private short AV28GXLvl47 ;
      private short subGridatributos_Titlebackstyle ;
      private short subGridatributos_Allowselection ;
      private short subGridatributos_Allowhovering ;
      private short subGridatributos_Allowcollapsing ;
      private short subGridatributos_Collapsed ;
      private short subGridatributos_Backstyle ;
      private int AV12FuncaoAPF_Codigo ;
      private int AV17Tabela_SistemaCod ;
      private int wcpOAV12FuncaoAPF_Codigo ;
      private int wcpOAV17Tabela_SistemaCod ;
      private int subGridatributos_Rows ;
      private int A370FuncaoDados_SistemaCod ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int A368FuncaoDados_Codigo ;
      private int A356Atributos_TabelaCod ;
      private int A172Tabela_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int A176Atributos_Codigo ;
      private int AV20FuncaoDados_FuncaoDadosCod ;
      private int edtavFuncaoapf_codigo_Enabled ;
      private int edtavAtributos_codigo_Enabled ;
      private int edtavAtributos_nome_Enabled ;
      private int edtavAtributos_detalhes_Enabled ;
      private int edtavAtributos_tabelanom_Enabled ;
      private int AV18FuncaoDados_Codigo ;
      private int AV5Atributos_Codigo ;
      private int gxdynajaxindex ;
      private int subGridatributos_Islastpage ;
      private int GRIDATRIBUTOS_nGridOutOfScope ;
      private int subGridatributos_Titlebackcolor ;
      private int subGridatributos_Allbackcolor ;
      private int subGridatributos_Selectioncolor ;
      private int subGridatributos_Hoveringcolor ;
      private int idxLst ;
      private int subGridatributos_Backcolor ;
      private int edtavAtributos_codigo_Visible ;
      private int edtavInsertatributo_Enabled ;
      private int edtavInsertatributo_Visible ;
      private int edtavAtributos_nome_Visible ;
      private int edtavAtributos_detalhes_Visible ;
      private int edtavAtributos_tabelanom_Visible ;
      private long GRIDATRIBUTOS_nFirstRecordOnPage ;
      private long GRIDATRIBUTOS_nCurrentRecord ;
      private long GRIDATRIBUTOS_nRecordCount ;
      private String edtavInsertatributo_Tooltiptext ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String edtavFuncaoapf_codigo_Internalname ;
      private String sGXsfl_16_idx="0001" ;
      private String edtavInsertatributo_Internalname ;
      private String A373FuncaoDados_Tipo ;
      private String A177Atributos_Nome ;
      private String AV8FiltroAtributos_Nome ;
      private String A178Atributos_TipoDados ;
      private String A390Atributos_Detalhes ;
      private String A357Atributos_TabelaNom ;
      private String GXKey ;
      private String dynavFuncaodados_codigo_Internalname ;
      private String edtavAtributos_codigo_Internalname ;
      private String edtavAtributos_nome_Internalname ;
      private String cmbavAtributos_tipodados_Internalname ;
      private String edtavAtributos_detalhes_Internalname ;
      private String chkavAtributos_pk_Internalname ;
      private String chkavAtributos_fk_Internalname ;
      private String edtavAtributos_tabelanom_Internalname ;
      private String cmbavFuncaodados_tipo_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV15Atributos_Nome ;
      private String AV11Atributos_TipoDados ;
      private String AV6Atributos_Detalhes ;
      private String AV10Atributos_TabelaNom ;
      private String AV21FuncaoDados_Tipo ;
      private String GXCCtl ;
      private String edtavFiltroatributos_nome_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV8FiltroAtributos_Nome ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablecontent_Internalname ;
      private String subGridatributos_Internalname ;
      private String subGridatributos_Class ;
      private String subGridatributos_Linesclass ;
      private String tblTable1_Internalname ;
      private String lblTextblockfiltroatributos_nome_Internalname ;
      private String lblTextblockfiltroatributos_nome_Jsonclick ;
      private String TempTags ;
      private String edtavFiltroatributos_nome_Jsonclick ;
      private String sCtrlAV12FuncaoAPF_Codigo ;
      private String sCtrlAV17Tabela_SistemaCod ;
      private String sGXsfl_16_fel_idx="0001" ;
      private String dynavFuncaodados_codigo_Jsonclick ;
      private String ROClassString ;
      private String edtavFuncaoapf_codigo_Jsonclick ;
      private String edtavAtributos_codigo_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavInsertatributo_Jsonclick ;
      private String edtavAtributos_nome_Jsonclick ;
      private String cmbavAtributos_tipodados_Jsonclick ;
      private String edtavAtributos_detalhes_Jsonclick ;
      private String edtavAtributos_tabelanom_Jsonclick ;
      private String cmbavFuncaodados_tipo_Jsonclick ;
      private bool entryPointCalled ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool A180Atributos_Ativo ;
      private bool n178Atributos_TipoDados ;
      private bool n390Atributos_Detalhes ;
      private bool A400Atributos_PK ;
      private bool n400Atributos_PK ;
      private bool A401Atributos_FK ;
      private bool n401Atributos_FK ;
      private bool n357Atributos_TabelaNom ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV9Atributos_PK ;
      private bool AV7Atributos_FK ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV16InsertAtributo_IsBlob ;
      private String AV24Insertatributo_GXI ;
      private String AV29Imageminsert_GXI ;
      private String AV16InsertAtributo ;
      private String AV14ImagemInsert ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridatributosContainer ;
      private GXWebRow GridatributosRow ;
      private GXWebColumn GridatributosColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavFuncaodados_codigo ;
      private GXCombobox cmbavAtributos_tipodados ;
      private GXCheckbox chkavAtributos_pk ;
      private GXCheckbox chkavAtributos_fk ;
      private GXCombobox cmbavFuncaodados_tipo ;
      private IDataStoreProvider pr_default ;
      private int[] H00AK2_A368FuncaoDados_Codigo ;
      private String[] H00AK2_A369FuncaoDados_Nome ;
      private int[] H00AK3_A368FuncaoDados_Codigo ;
      private int[] H00AK3_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] H00AK3_n391FuncaoDados_FuncaoDadosCod ;
      private int[] H00AK3_A370FuncaoDados_SistemaCod ;
      private int[] H00AK4_A172Tabela_Codigo ;
      private int[] H00AK4_A368FuncaoDados_Codigo ;
      private int[] H00AK4_A370FuncaoDados_SistemaCod ;
      private int[] H00AK4_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] H00AK4_n391FuncaoDados_FuncaoDadosCod ;
      private String[] H00AK4_A373FuncaoDados_Tipo ;
      private int[] H00AK5_A356Atributos_TabelaCod ;
      private int[] H00AK5_A176Atributos_Codigo ;
      private String[] H00AK5_A177Atributos_Nome ;
      private bool[] H00AK5_A180Atributos_Ativo ;
      private String[] H00AK5_A178Atributos_TipoDados ;
      private bool[] H00AK5_n178Atributos_TipoDados ;
      private String[] H00AK5_A390Atributos_Detalhes ;
      private bool[] H00AK5_n390Atributos_Detalhes ;
      private bool[] H00AK5_A400Atributos_PK ;
      private bool[] H00AK5_n400Atributos_PK ;
      private bool[] H00AK5_A401Atributos_FK ;
      private bool[] H00AK5_n401Atributos_FK ;
      private String[] H00AK5_A357Atributos_TabelaNom ;
      private bool[] H00AK5_n357Atributos_TabelaNom ;
      private int[] H00AK6_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] H00AK6_A165FuncaoAPF_Codigo ;
      private int[] H00AK7_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] H00AK7_n391FuncaoDados_FuncaoDadosCod ;
      private int[] H00AK7_A368FuncaoDados_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV19FuncaDadosAIE ;
      private SdtFuncoesAPFAtributos AV13FuncoesAPFAtributos ;
   }

   public class wc_funcaoapfatributosins__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00AK4( IGxContext context ,
                                             int A368FuncaoDados_Codigo ,
                                             IGxCollection AV19FuncaDadosAIE ,
                                             int A370FuncaoDados_SistemaCod ,
                                             int AV17Tabela_SistemaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [1] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Tabela_Codigo], T1.[FuncaoDados_Codigo], T2.[FuncaoDados_SistemaCod], T2.[FuncaoDados_FuncaoDadosCod], T2.[FuncaoDados_Tipo] FROM ([FuncaoDadosTabela] T1 WITH (NOLOCK) INNER JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoDados_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[FuncaoDados_SistemaCod] = @AV17Tabela_SistemaCod or " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV19FuncaDadosAIE, "T1.[FuncaoDados_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_Codigo], T1.[Tabela_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00AK5( IGxContext context ,
                                             String AV8FiltroAtributos_Nome ,
                                             String A177Atributos_Nome ,
                                             bool A180Atributos_Ativo ,
                                             int A172Tabela_Codigo ,
                                             int A356Atributos_TabelaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [2] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, T1.[Atributos_Codigo], T1.[Atributos_Nome], T1.[Atributos_Ativo], T1.[Atributos_TipoDados], T1.[Atributos_Detalhes], T1.[Atributos_PK], T1.[Atributos_FK], T2.[Tabela_Nome] AS Atributos_TabelaNom FROM ([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Atributos_TabelaCod] = @Tabela_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[Atributos_Ativo] = 1)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8FiltroAtributos_Nome)) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV8FiltroAtributos_Nome)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Atributos_TabelaCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00AK4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 3 :
                     return conditional_H00AK5(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00AK2 ;
          prmH00AK2 = new Object[] {
          } ;
          Object[] prmH00AK3 ;
          prmH00AK3 = new Object[] {
          new Object[] {"@AV17Tabela_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00AK6 ;
          prmH00AK6 = new Object[] {
          new Object[] {"@AV12FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00AK7 ;
          prmH00AK7 = new Object[] {
          new Object[] {"@AV20FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00AK4 ;
          prmH00AK4 = new Object[] {
          new Object[] {"@AV17Tabela_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00AK5 ;
          prmH00AK5 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV8FiltroAtributos_Nome",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00AK2", "SELECT [FuncaoDados_Codigo], [FuncaoDados_Nome] FROM [FuncaoDados] WITH (NOLOCK) ORDER BY [FuncaoDados_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AK2,0,0,true,false )
             ,new CursorDef("H00AK3", "SELECT [FuncaoDados_Codigo], [FuncaoDados_FuncaoDadosCod], [FuncaoDados_SistemaCod] FROM [FuncaoDados] WITH (NOLOCK) WHERE ([FuncaoDados_SistemaCod] = @AV17Tabela_SistemaCod) AND (Not [FuncaoDados_FuncaoDadosCod] IS NULL) ORDER BY [FuncaoDados_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AK3,100,0,false,false )
             ,new CursorDef("H00AK4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AK4,100,0,true,false )
             ,new CursorDef("H00AK5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AK5,100,0,true,false )
             ,new CursorDef("H00AK6", "SELECT [FuncaoAPFAtributos_AtributosCod], [FuncaoAPF_Codigo] FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @AV12FuncaoAPF_Codigo and [FuncaoAPFAtributos_AtributosCod] = @Atributos_Codigo ORDER BY [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AK6,1,0,true,true )
             ,new CursorDef("H00AK7", "SELECT TOP 1 [FuncaoDados_FuncaoDadosCod], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_FuncaoDadosCod] = @AV20FuncaoDados_FuncaoDadosCod ORDER BY [FuncaoDados_FuncaoDadosCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AK7,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[3]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
