/*
               File: GetWWTabelaFilterData
        Description: Get WWTabela Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:33:49.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwtabelafilterdata : GXProcedure
   {
      public getwwtabelafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwtabelafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
         return AV32OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwtabelafilterdata objgetwwtabelafilterdata;
         objgetwwtabelafilterdata = new getwwtabelafilterdata();
         objgetwwtabelafilterdata.AV23DDOName = aP0_DDOName;
         objgetwwtabelafilterdata.AV21SearchTxt = aP1_SearchTxt;
         objgetwwtabelafilterdata.AV22SearchTxtTo = aP2_SearchTxtTo;
         objgetwwtabelafilterdata.AV27OptionsJson = "" ;
         objgetwwtabelafilterdata.AV30OptionsDescJson = "" ;
         objgetwwtabelafilterdata.AV32OptionIndexesJson = "" ;
         objgetwwtabelafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwtabelafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwtabelafilterdata);
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwtabelafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV26Options = (IGxCollection)(new GxSimpleCollection());
         AV29OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV31OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_TABELA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTABELA_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_TABELA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADTABELA_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_TABELA_SISTEMADES") == 0 )
         {
            /* Execute user subroutine: 'LOADTABELA_SISTEMADESOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_TABELA_MODULODES") == 0 )
         {
            /* Execute user subroutine: 'LOADTABELA_MODULODESOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_TABELA_PAINOM") == 0 )
         {
            /* Execute user subroutine: 'LOADTABELA_PAINOMOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV27OptionsJson = AV26Options.ToJSonString(false);
         AV30OptionsDescJson = AV29OptionsDesc.ToJSonString(false);
         AV32OptionIndexesJson = AV31OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV34Session.Get("WWTabelaGridState"), "") == 0 )
         {
            AV36GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWTabelaGridState"), "");
         }
         else
         {
            AV36GridState.FromXml(AV34Session.Get("WWTabelaGridState"), "");
         }
         AV49GXV1 = 1;
         while ( AV49GXV1 <= AV36GridState.gxTpr_Filtervalues.Count )
         {
            AV37GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV36GridState.gxTpr_Filtervalues.Item(AV49GXV1));
            if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "SISTEMA_AREATRABALHOCOD") == 0 )
            {
               AV39Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFTABELA_NOME") == 0 )
            {
               AV10TFTabela_Nome = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFTABELA_NOME_SEL") == 0 )
            {
               AV11TFTabela_Nome_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFTABELA_DESCRICAO") == 0 )
            {
               AV12TFTabela_Descricao = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFTABELA_DESCRICAO_SEL") == 0 )
            {
               AV13TFTabela_Descricao_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFTABELA_SISTEMADES") == 0 )
            {
               AV14TFTabela_SistemaDes = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFTABELA_SISTEMADES_SEL") == 0 )
            {
               AV15TFTabela_SistemaDes_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFTABELA_MODULODES") == 0 )
            {
               AV16TFTabela_ModuloDes = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFTABELA_MODULODES_SEL") == 0 )
            {
               AV17TFTabela_ModuloDes_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFTABELA_PAINOM") == 0 )
            {
               AV18TFTabela_PaiNom = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFTABELA_PAINOM_SEL") == 0 )
            {
               AV19TFTabela_PaiNom_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFTABELA_MELHORACOD") == 0 )
            {
               AV44TFTabela_MelhoraCod = (int)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
               AV45TFTabela_MelhoraCod_To = (int)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFTABELA_ATIVO_SEL") == 0 )
            {
               AV20TFTabela_Ativo_Sel = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            AV49GXV1 = (int)(AV49GXV1+1);
         }
         if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(1));
            AV40DynamicFiltersSelector1 = AV38GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "TABELA_NOME") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV38GridStateDynamicFilter.gxTpr_Operator;
               AV42Tabela_Nome1 = AV38GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "TABELA_MODULODES") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV38GridStateDynamicFilter.gxTpr_Operator;
               AV46Tabela_ModuloDes1 = AV38GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "TABELA_MODULOCOD") == 0 )
            {
               AV43Tabela_ModuloCod1 = (int)(NumberUtil.Val( AV38GridStateDynamicFilter.gxTpr_Value, "."));
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADTABELA_NOMEOPTIONS' Routine */
         AV10TFTabela_Nome = AV21SearchTxt;
         AV11TFTabela_Nome_Sel = "";
         AV51WWTabelaDS_1_Sistema_areatrabalhocod = AV39Sistema_AreaTrabalhoCod;
         AV52WWTabelaDS_2_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV53WWTabelaDS_3_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV54WWTabelaDS_4_Tabela_nome1 = AV42Tabela_Nome1;
         AV55WWTabelaDS_5_Tabela_modulodes1 = AV46Tabela_ModuloDes1;
         AV56WWTabelaDS_6_Tabela_modulocod1 = AV43Tabela_ModuloCod1;
         AV57WWTabelaDS_7_Tftabela_nome = AV10TFTabela_Nome;
         AV58WWTabelaDS_8_Tftabela_nome_sel = AV11TFTabela_Nome_Sel;
         AV59WWTabelaDS_9_Tftabela_descricao = AV12TFTabela_Descricao;
         AV60WWTabelaDS_10_Tftabela_descricao_sel = AV13TFTabela_Descricao_Sel;
         AV61WWTabelaDS_11_Tftabela_sistemades = AV14TFTabela_SistemaDes;
         AV62WWTabelaDS_12_Tftabela_sistemades_sel = AV15TFTabela_SistemaDes_Sel;
         AV63WWTabelaDS_13_Tftabela_modulodes = AV16TFTabela_ModuloDes;
         AV64WWTabelaDS_14_Tftabela_modulodes_sel = AV17TFTabela_ModuloDes_Sel;
         AV65WWTabelaDS_15_Tftabela_painom = AV18TFTabela_PaiNom;
         AV66WWTabelaDS_16_Tftabela_painom_sel = AV19TFTabela_PaiNom_Sel;
         AV67WWTabelaDS_17_Tftabela_melhoracod = AV44TFTabela_MelhoraCod;
         AV68WWTabelaDS_18_Tftabela_melhoracod_to = AV45TFTabela_MelhoraCod_To;
         AV69WWTabelaDS_19_Tftabela_ativo_sel = AV20TFTabela_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV51WWTabelaDS_1_Sistema_areatrabalhocod ,
                                              AV52WWTabelaDS_2_Dynamicfiltersselector1 ,
                                              AV53WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                              AV54WWTabelaDS_4_Tabela_nome1 ,
                                              AV55WWTabelaDS_5_Tabela_modulodes1 ,
                                              AV56WWTabelaDS_6_Tabela_modulocod1 ,
                                              AV58WWTabelaDS_8_Tftabela_nome_sel ,
                                              AV57WWTabelaDS_7_Tftabela_nome ,
                                              AV60WWTabelaDS_10_Tftabela_descricao_sel ,
                                              AV59WWTabelaDS_9_Tftabela_descricao ,
                                              AV62WWTabelaDS_12_Tftabela_sistemades_sel ,
                                              AV61WWTabelaDS_11_Tftabela_sistemades ,
                                              AV64WWTabelaDS_14_Tftabela_modulodes_sel ,
                                              AV63WWTabelaDS_13_Tftabela_modulodes ,
                                              AV66WWTabelaDS_16_Tftabela_painom_sel ,
                                              AV65WWTabelaDS_15_Tftabela_painom ,
                                              AV67WWTabelaDS_17_Tftabela_melhoracod ,
                                              AV68WWTabelaDS_18_Tftabela_melhoracod_to ,
                                              AV69WWTabelaDS_19_Tftabela_ativo_sel ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A173Tabela_Nome ,
                                              A189Tabela_ModuloDes ,
                                              A188Tabela_ModuloCod ,
                                              A175Tabela_Descricao ,
                                              A191Tabela_SistemaDes ,
                                              A182Tabela_PaiNom ,
                                              A746Tabela_MelhoraCod ,
                                              A174Tabela_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV54WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1), 50, "%");
         lV54WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1), 50, "%");
         lV55WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1), 50, "%");
         lV55WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1), 50, "%");
         lV57WWTabelaDS_7_Tftabela_nome = StringUtil.PadR( StringUtil.RTrim( AV57WWTabelaDS_7_Tftabela_nome), 50, "%");
         lV59WWTabelaDS_9_Tftabela_descricao = StringUtil.Concat( StringUtil.RTrim( AV59WWTabelaDS_9_Tftabela_descricao), "%", "");
         lV61WWTabelaDS_11_Tftabela_sistemades = StringUtil.Concat( StringUtil.RTrim( AV61WWTabelaDS_11_Tftabela_sistemades), "%", "");
         lV63WWTabelaDS_13_Tftabela_modulodes = StringUtil.PadR( StringUtil.RTrim( AV63WWTabelaDS_13_Tftabela_modulodes), 50, "%");
         lV65WWTabelaDS_15_Tftabela_painom = StringUtil.PadR( StringUtil.RTrim( AV65WWTabelaDS_15_Tftabela_painom), 50, "%");
         /* Using cursor P00GW2 */
         pr_default.execute(0, new Object[] {AV51WWTabelaDS_1_Sistema_areatrabalhocod, lV54WWTabelaDS_4_Tabela_nome1, lV54WWTabelaDS_4_Tabela_nome1, lV55WWTabelaDS_5_Tabela_modulodes1, lV55WWTabelaDS_5_Tabela_modulodes1, AV56WWTabelaDS_6_Tabela_modulocod1, lV57WWTabelaDS_7_Tftabela_nome, AV58WWTabelaDS_8_Tftabela_nome_sel, lV59WWTabelaDS_9_Tftabela_descricao, AV60WWTabelaDS_10_Tftabela_descricao_sel, lV61WWTabelaDS_11_Tftabela_sistemades, AV62WWTabelaDS_12_Tftabela_sistemades_sel, lV63WWTabelaDS_13_Tftabela_modulodes, AV64WWTabelaDS_14_Tftabela_modulodes_sel, lV65WWTabelaDS_15_Tftabela_painom, AV66WWTabelaDS_16_Tftabela_painom_sel, AV67WWTabelaDS_17_Tftabela_melhoracod, AV68WWTabelaDS_18_Tftabela_melhoracod_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKGW2 = false;
            A190Tabela_SistemaCod = P00GW2_A190Tabela_SistemaCod[0];
            A181Tabela_PaiCod = P00GW2_A181Tabela_PaiCod[0];
            n181Tabela_PaiCod = P00GW2_n181Tabela_PaiCod[0];
            A173Tabela_Nome = P00GW2_A173Tabela_Nome[0];
            A174Tabela_Ativo = P00GW2_A174Tabela_Ativo[0];
            A746Tabela_MelhoraCod = P00GW2_A746Tabela_MelhoraCod[0];
            n746Tabela_MelhoraCod = P00GW2_n746Tabela_MelhoraCod[0];
            A182Tabela_PaiNom = P00GW2_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00GW2_n182Tabela_PaiNom[0];
            A191Tabela_SistemaDes = P00GW2_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00GW2_n191Tabela_SistemaDes[0];
            A175Tabela_Descricao = P00GW2_A175Tabela_Descricao[0];
            n175Tabela_Descricao = P00GW2_n175Tabela_Descricao[0];
            A188Tabela_ModuloCod = P00GW2_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = P00GW2_n188Tabela_ModuloCod[0];
            A189Tabela_ModuloDes = P00GW2_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = P00GW2_n189Tabela_ModuloDes[0];
            A135Sistema_AreaTrabalhoCod = P00GW2_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00GW2_n135Sistema_AreaTrabalhoCod[0];
            A172Tabela_Codigo = P00GW2_A172Tabela_Codigo[0];
            A191Tabela_SistemaDes = P00GW2_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00GW2_n191Tabela_SistemaDes[0];
            A135Sistema_AreaTrabalhoCod = P00GW2_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00GW2_n135Sistema_AreaTrabalhoCod[0];
            A182Tabela_PaiNom = P00GW2_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00GW2_n182Tabela_PaiNom[0];
            A189Tabela_ModuloDes = P00GW2_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = P00GW2_n189Tabela_ModuloDes[0];
            AV33count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00GW2_A173Tabela_Nome[0], A173Tabela_Nome) == 0 ) )
            {
               BRKGW2 = false;
               A172Tabela_Codigo = P00GW2_A172Tabela_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKGW2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A173Tabela_Nome)) )
            {
               AV25Option = A173Tabela_Nome;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKGW2 )
            {
               BRKGW2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADTABELA_DESCRICAOOPTIONS' Routine */
         AV12TFTabela_Descricao = AV21SearchTxt;
         AV13TFTabela_Descricao_Sel = "";
         AV51WWTabelaDS_1_Sistema_areatrabalhocod = AV39Sistema_AreaTrabalhoCod;
         AV52WWTabelaDS_2_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV53WWTabelaDS_3_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV54WWTabelaDS_4_Tabela_nome1 = AV42Tabela_Nome1;
         AV55WWTabelaDS_5_Tabela_modulodes1 = AV46Tabela_ModuloDes1;
         AV56WWTabelaDS_6_Tabela_modulocod1 = AV43Tabela_ModuloCod1;
         AV57WWTabelaDS_7_Tftabela_nome = AV10TFTabela_Nome;
         AV58WWTabelaDS_8_Tftabela_nome_sel = AV11TFTabela_Nome_Sel;
         AV59WWTabelaDS_9_Tftabela_descricao = AV12TFTabela_Descricao;
         AV60WWTabelaDS_10_Tftabela_descricao_sel = AV13TFTabela_Descricao_Sel;
         AV61WWTabelaDS_11_Tftabela_sistemades = AV14TFTabela_SistemaDes;
         AV62WWTabelaDS_12_Tftabela_sistemades_sel = AV15TFTabela_SistemaDes_Sel;
         AV63WWTabelaDS_13_Tftabela_modulodes = AV16TFTabela_ModuloDes;
         AV64WWTabelaDS_14_Tftabela_modulodes_sel = AV17TFTabela_ModuloDes_Sel;
         AV65WWTabelaDS_15_Tftabela_painom = AV18TFTabela_PaiNom;
         AV66WWTabelaDS_16_Tftabela_painom_sel = AV19TFTabela_PaiNom_Sel;
         AV67WWTabelaDS_17_Tftabela_melhoracod = AV44TFTabela_MelhoraCod;
         AV68WWTabelaDS_18_Tftabela_melhoracod_to = AV45TFTabela_MelhoraCod_To;
         AV69WWTabelaDS_19_Tftabela_ativo_sel = AV20TFTabela_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV51WWTabelaDS_1_Sistema_areatrabalhocod ,
                                              AV52WWTabelaDS_2_Dynamicfiltersselector1 ,
                                              AV53WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                              AV54WWTabelaDS_4_Tabela_nome1 ,
                                              AV55WWTabelaDS_5_Tabela_modulodes1 ,
                                              AV56WWTabelaDS_6_Tabela_modulocod1 ,
                                              AV58WWTabelaDS_8_Tftabela_nome_sel ,
                                              AV57WWTabelaDS_7_Tftabela_nome ,
                                              AV60WWTabelaDS_10_Tftabela_descricao_sel ,
                                              AV59WWTabelaDS_9_Tftabela_descricao ,
                                              AV62WWTabelaDS_12_Tftabela_sistemades_sel ,
                                              AV61WWTabelaDS_11_Tftabela_sistemades ,
                                              AV64WWTabelaDS_14_Tftabela_modulodes_sel ,
                                              AV63WWTabelaDS_13_Tftabela_modulodes ,
                                              AV66WWTabelaDS_16_Tftabela_painom_sel ,
                                              AV65WWTabelaDS_15_Tftabela_painom ,
                                              AV67WWTabelaDS_17_Tftabela_melhoracod ,
                                              AV68WWTabelaDS_18_Tftabela_melhoracod_to ,
                                              AV69WWTabelaDS_19_Tftabela_ativo_sel ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A173Tabela_Nome ,
                                              A189Tabela_ModuloDes ,
                                              A188Tabela_ModuloCod ,
                                              A175Tabela_Descricao ,
                                              A191Tabela_SistemaDes ,
                                              A182Tabela_PaiNom ,
                                              A746Tabela_MelhoraCod ,
                                              A174Tabela_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV54WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1), 50, "%");
         lV54WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1), 50, "%");
         lV55WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1), 50, "%");
         lV55WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1), 50, "%");
         lV57WWTabelaDS_7_Tftabela_nome = StringUtil.PadR( StringUtil.RTrim( AV57WWTabelaDS_7_Tftabela_nome), 50, "%");
         lV59WWTabelaDS_9_Tftabela_descricao = StringUtil.Concat( StringUtil.RTrim( AV59WWTabelaDS_9_Tftabela_descricao), "%", "");
         lV61WWTabelaDS_11_Tftabela_sistemades = StringUtil.Concat( StringUtil.RTrim( AV61WWTabelaDS_11_Tftabela_sistemades), "%", "");
         lV63WWTabelaDS_13_Tftabela_modulodes = StringUtil.PadR( StringUtil.RTrim( AV63WWTabelaDS_13_Tftabela_modulodes), 50, "%");
         lV65WWTabelaDS_15_Tftabela_painom = StringUtil.PadR( StringUtil.RTrim( AV65WWTabelaDS_15_Tftabela_painom), 50, "%");
         /* Using cursor P00GW3 */
         pr_default.execute(1, new Object[] {AV51WWTabelaDS_1_Sistema_areatrabalhocod, lV54WWTabelaDS_4_Tabela_nome1, lV54WWTabelaDS_4_Tabela_nome1, lV55WWTabelaDS_5_Tabela_modulodes1, lV55WWTabelaDS_5_Tabela_modulodes1, AV56WWTabelaDS_6_Tabela_modulocod1, lV57WWTabelaDS_7_Tftabela_nome, AV58WWTabelaDS_8_Tftabela_nome_sel, lV59WWTabelaDS_9_Tftabela_descricao, AV60WWTabelaDS_10_Tftabela_descricao_sel, lV61WWTabelaDS_11_Tftabela_sistemades, AV62WWTabelaDS_12_Tftabela_sistemades_sel, lV63WWTabelaDS_13_Tftabela_modulodes, AV64WWTabelaDS_14_Tftabela_modulodes_sel, lV65WWTabelaDS_15_Tftabela_painom, AV66WWTabelaDS_16_Tftabela_painom_sel, AV67WWTabelaDS_17_Tftabela_melhoracod, AV68WWTabelaDS_18_Tftabela_melhoracod_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKGW4 = false;
            A190Tabela_SistemaCod = P00GW3_A190Tabela_SistemaCod[0];
            A181Tabela_PaiCod = P00GW3_A181Tabela_PaiCod[0];
            n181Tabela_PaiCod = P00GW3_n181Tabela_PaiCod[0];
            A175Tabela_Descricao = P00GW3_A175Tabela_Descricao[0];
            n175Tabela_Descricao = P00GW3_n175Tabela_Descricao[0];
            A174Tabela_Ativo = P00GW3_A174Tabela_Ativo[0];
            A746Tabela_MelhoraCod = P00GW3_A746Tabela_MelhoraCod[0];
            n746Tabela_MelhoraCod = P00GW3_n746Tabela_MelhoraCod[0];
            A182Tabela_PaiNom = P00GW3_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00GW3_n182Tabela_PaiNom[0];
            A191Tabela_SistemaDes = P00GW3_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00GW3_n191Tabela_SistemaDes[0];
            A188Tabela_ModuloCod = P00GW3_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = P00GW3_n188Tabela_ModuloCod[0];
            A189Tabela_ModuloDes = P00GW3_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = P00GW3_n189Tabela_ModuloDes[0];
            A173Tabela_Nome = P00GW3_A173Tabela_Nome[0];
            A135Sistema_AreaTrabalhoCod = P00GW3_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00GW3_n135Sistema_AreaTrabalhoCod[0];
            A172Tabela_Codigo = P00GW3_A172Tabela_Codigo[0];
            A191Tabela_SistemaDes = P00GW3_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00GW3_n191Tabela_SistemaDes[0];
            A135Sistema_AreaTrabalhoCod = P00GW3_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00GW3_n135Sistema_AreaTrabalhoCod[0];
            A182Tabela_PaiNom = P00GW3_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00GW3_n182Tabela_PaiNom[0];
            A189Tabela_ModuloDes = P00GW3_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = P00GW3_n189Tabela_ModuloDes[0];
            AV33count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00GW3_A175Tabela_Descricao[0], A175Tabela_Descricao) == 0 ) )
            {
               BRKGW4 = false;
               A172Tabela_Codigo = P00GW3_A172Tabela_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKGW4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A175Tabela_Descricao)) )
            {
               AV25Option = A175Tabela_Descricao;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKGW4 )
            {
               BRKGW4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADTABELA_SISTEMADESOPTIONS' Routine */
         AV14TFTabela_SistemaDes = AV21SearchTxt;
         AV15TFTabela_SistemaDes_Sel = "";
         AV51WWTabelaDS_1_Sistema_areatrabalhocod = AV39Sistema_AreaTrabalhoCod;
         AV52WWTabelaDS_2_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV53WWTabelaDS_3_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV54WWTabelaDS_4_Tabela_nome1 = AV42Tabela_Nome1;
         AV55WWTabelaDS_5_Tabela_modulodes1 = AV46Tabela_ModuloDes1;
         AV56WWTabelaDS_6_Tabela_modulocod1 = AV43Tabela_ModuloCod1;
         AV57WWTabelaDS_7_Tftabela_nome = AV10TFTabela_Nome;
         AV58WWTabelaDS_8_Tftabela_nome_sel = AV11TFTabela_Nome_Sel;
         AV59WWTabelaDS_9_Tftabela_descricao = AV12TFTabela_Descricao;
         AV60WWTabelaDS_10_Tftabela_descricao_sel = AV13TFTabela_Descricao_Sel;
         AV61WWTabelaDS_11_Tftabela_sistemades = AV14TFTabela_SistemaDes;
         AV62WWTabelaDS_12_Tftabela_sistemades_sel = AV15TFTabela_SistemaDes_Sel;
         AV63WWTabelaDS_13_Tftabela_modulodes = AV16TFTabela_ModuloDes;
         AV64WWTabelaDS_14_Tftabela_modulodes_sel = AV17TFTabela_ModuloDes_Sel;
         AV65WWTabelaDS_15_Tftabela_painom = AV18TFTabela_PaiNom;
         AV66WWTabelaDS_16_Tftabela_painom_sel = AV19TFTabela_PaiNom_Sel;
         AV67WWTabelaDS_17_Tftabela_melhoracod = AV44TFTabela_MelhoraCod;
         AV68WWTabelaDS_18_Tftabela_melhoracod_to = AV45TFTabela_MelhoraCod_To;
         AV69WWTabelaDS_19_Tftabela_ativo_sel = AV20TFTabela_Ativo_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV51WWTabelaDS_1_Sistema_areatrabalhocod ,
                                              AV52WWTabelaDS_2_Dynamicfiltersselector1 ,
                                              AV53WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                              AV54WWTabelaDS_4_Tabela_nome1 ,
                                              AV55WWTabelaDS_5_Tabela_modulodes1 ,
                                              AV56WWTabelaDS_6_Tabela_modulocod1 ,
                                              AV58WWTabelaDS_8_Tftabela_nome_sel ,
                                              AV57WWTabelaDS_7_Tftabela_nome ,
                                              AV60WWTabelaDS_10_Tftabela_descricao_sel ,
                                              AV59WWTabelaDS_9_Tftabela_descricao ,
                                              AV62WWTabelaDS_12_Tftabela_sistemades_sel ,
                                              AV61WWTabelaDS_11_Tftabela_sistemades ,
                                              AV64WWTabelaDS_14_Tftabela_modulodes_sel ,
                                              AV63WWTabelaDS_13_Tftabela_modulodes ,
                                              AV66WWTabelaDS_16_Tftabela_painom_sel ,
                                              AV65WWTabelaDS_15_Tftabela_painom ,
                                              AV67WWTabelaDS_17_Tftabela_melhoracod ,
                                              AV68WWTabelaDS_18_Tftabela_melhoracod_to ,
                                              AV69WWTabelaDS_19_Tftabela_ativo_sel ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A173Tabela_Nome ,
                                              A189Tabela_ModuloDes ,
                                              A188Tabela_ModuloCod ,
                                              A175Tabela_Descricao ,
                                              A191Tabela_SistemaDes ,
                                              A182Tabela_PaiNom ,
                                              A746Tabela_MelhoraCod ,
                                              A174Tabela_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV54WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1), 50, "%");
         lV54WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1), 50, "%");
         lV55WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1), 50, "%");
         lV55WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1), 50, "%");
         lV57WWTabelaDS_7_Tftabela_nome = StringUtil.PadR( StringUtil.RTrim( AV57WWTabelaDS_7_Tftabela_nome), 50, "%");
         lV59WWTabelaDS_9_Tftabela_descricao = StringUtil.Concat( StringUtil.RTrim( AV59WWTabelaDS_9_Tftabela_descricao), "%", "");
         lV61WWTabelaDS_11_Tftabela_sistemades = StringUtil.Concat( StringUtil.RTrim( AV61WWTabelaDS_11_Tftabela_sistemades), "%", "");
         lV63WWTabelaDS_13_Tftabela_modulodes = StringUtil.PadR( StringUtil.RTrim( AV63WWTabelaDS_13_Tftabela_modulodes), 50, "%");
         lV65WWTabelaDS_15_Tftabela_painom = StringUtil.PadR( StringUtil.RTrim( AV65WWTabelaDS_15_Tftabela_painom), 50, "%");
         /* Using cursor P00GW4 */
         pr_default.execute(2, new Object[] {AV51WWTabelaDS_1_Sistema_areatrabalhocod, lV54WWTabelaDS_4_Tabela_nome1, lV54WWTabelaDS_4_Tabela_nome1, lV55WWTabelaDS_5_Tabela_modulodes1, lV55WWTabelaDS_5_Tabela_modulodes1, AV56WWTabelaDS_6_Tabela_modulocod1, lV57WWTabelaDS_7_Tftabela_nome, AV58WWTabelaDS_8_Tftabela_nome_sel, lV59WWTabelaDS_9_Tftabela_descricao, AV60WWTabelaDS_10_Tftabela_descricao_sel, lV61WWTabelaDS_11_Tftabela_sistemades, AV62WWTabelaDS_12_Tftabela_sistemades_sel, lV63WWTabelaDS_13_Tftabela_modulodes, AV64WWTabelaDS_14_Tftabela_modulodes_sel, lV65WWTabelaDS_15_Tftabela_painom, AV66WWTabelaDS_16_Tftabela_painom_sel, AV67WWTabelaDS_17_Tftabela_melhoracod, AV68WWTabelaDS_18_Tftabela_melhoracod_to});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKGW6 = false;
            A181Tabela_PaiCod = P00GW4_A181Tabela_PaiCod[0];
            n181Tabela_PaiCod = P00GW4_n181Tabela_PaiCod[0];
            A190Tabela_SistemaCod = P00GW4_A190Tabela_SistemaCod[0];
            A174Tabela_Ativo = P00GW4_A174Tabela_Ativo[0];
            A746Tabela_MelhoraCod = P00GW4_A746Tabela_MelhoraCod[0];
            n746Tabela_MelhoraCod = P00GW4_n746Tabela_MelhoraCod[0];
            A182Tabela_PaiNom = P00GW4_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00GW4_n182Tabela_PaiNom[0];
            A191Tabela_SistemaDes = P00GW4_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00GW4_n191Tabela_SistemaDes[0];
            A175Tabela_Descricao = P00GW4_A175Tabela_Descricao[0];
            n175Tabela_Descricao = P00GW4_n175Tabela_Descricao[0];
            A188Tabela_ModuloCod = P00GW4_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = P00GW4_n188Tabela_ModuloCod[0];
            A189Tabela_ModuloDes = P00GW4_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = P00GW4_n189Tabela_ModuloDes[0];
            A173Tabela_Nome = P00GW4_A173Tabela_Nome[0];
            A135Sistema_AreaTrabalhoCod = P00GW4_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00GW4_n135Sistema_AreaTrabalhoCod[0];
            A172Tabela_Codigo = P00GW4_A172Tabela_Codigo[0];
            A182Tabela_PaiNom = P00GW4_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00GW4_n182Tabela_PaiNom[0];
            A191Tabela_SistemaDes = P00GW4_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00GW4_n191Tabela_SistemaDes[0];
            A135Sistema_AreaTrabalhoCod = P00GW4_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00GW4_n135Sistema_AreaTrabalhoCod[0];
            A189Tabela_ModuloDes = P00GW4_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = P00GW4_n189Tabela_ModuloDes[0];
            AV33count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00GW4_A190Tabela_SistemaCod[0] == A190Tabela_SistemaCod ) )
            {
               BRKGW6 = false;
               A172Tabela_Codigo = P00GW4_A172Tabela_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKGW6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A191Tabela_SistemaDes)) )
            {
               AV25Option = A191Tabela_SistemaDes;
               AV24InsertIndex = 1;
               while ( ( AV24InsertIndex <= AV26Options.Count ) && ( StringUtil.StrCmp(((String)AV26Options.Item(AV24InsertIndex)), AV25Option) < 0 ) )
               {
                  AV24InsertIndex = (int)(AV24InsertIndex+1);
               }
               AV26Options.Add(AV25Option, AV24InsertIndex);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), AV24InsertIndex);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKGW6 )
            {
               BRKGW6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADTABELA_MODULODESOPTIONS' Routine */
         AV16TFTabela_ModuloDes = AV21SearchTxt;
         AV17TFTabela_ModuloDes_Sel = "";
         AV51WWTabelaDS_1_Sistema_areatrabalhocod = AV39Sistema_AreaTrabalhoCod;
         AV52WWTabelaDS_2_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV53WWTabelaDS_3_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV54WWTabelaDS_4_Tabela_nome1 = AV42Tabela_Nome1;
         AV55WWTabelaDS_5_Tabela_modulodes1 = AV46Tabela_ModuloDes1;
         AV56WWTabelaDS_6_Tabela_modulocod1 = AV43Tabela_ModuloCod1;
         AV57WWTabelaDS_7_Tftabela_nome = AV10TFTabela_Nome;
         AV58WWTabelaDS_8_Tftabela_nome_sel = AV11TFTabela_Nome_Sel;
         AV59WWTabelaDS_9_Tftabela_descricao = AV12TFTabela_Descricao;
         AV60WWTabelaDS_10_Tftabela_descricao_sel = AV13TFTabela_Descricao_Sel;
         AV61WWTabelaDS_11_Tftabela_sistemades = AV14TFTabela_SistemaDes;
         AV62WWTabelaDS_12_Tftabela_sistemades_sel = AV15TFTabela_SistemaDes_Sel;
         AV63WWTabelaDS_13_Tftabela_modulodes = AV16TFTabela_ModuloDes;
         AV64WWTabelaDS_14_Tftabela_modulodes_sel = AV17TFTabela_ModuloDes_Sel;
         AV65WWTabelaDS_15_Tftabela_painom = AV18TFTabela_PaiNom;
         AV66WWTabelaDS_16_Tftabela_painom_sel = AV19TFTabela_PaiNom_Sel;
         AV67WWTabelaDS_17_Tftabela_melhoracod = AV44TFTabela_MelhoraCod;
         AV68WWTabelaDS_18_Tftabela_melhoracod_to = AV45TFTabela_MelhoraCod_To;
         AV69WWTabelaDS_19_Tftabela_ativo_sel = AV20TFTabela_Ativo_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV51WWTabelaDS_1_Sistema_areatrabalhocod ,
                                              AV52WWTabelaDS_2_Dynamicfiltersselector1 ,
                                              AV53WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                              AV54WWTabelaDS_4_Tabela_nome1 ,
                                              AV55WWTabelaDS_5_Tabela_modulodes1 ,
                                              AV56WWTabelaDS_6_Tabela_modulocod1 ,
                                              AV58WWTabelaDS_8_Tftabela_nome_sel ,
                                              AV57WWTabelaDS_7_Tftabela_nome ,
                                              AV60WWTabelaDS_10_Tftabela_descricao_sel ,
                                              AV59WWTabelaDS_9_Tftabela_descricao ,
                                              AV62WWTabelaDS_12_Tftabela_sistemades_sel ,
                                              AV61WWTabelaDS_11_Tftabela_sistemades ,
                                              AV64WWTabelaDS_14_Tftabela_modulodes_sel ,
                                              AV63WWTabelaDS_13_Tftabela_modulodes ,
                                              AV66WWTabelaDS_16_Tftabela_painom_sel ,
                                              AV65WWTabelaDS_15_Tftabela_painom ,
                                              AV67WWTabelaDS_17_Tftabela_melhoracod ,
                                              AV68WWTabelaDS_18_Tftabela_melhoracod_to ,
                                              AV69WWTabelaDS_19_Tftabela_ativo_sel ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A173Tabela_Nome ,
                                              A189Tabela_ModuloDes ,
                                              A188Tabela_ModuloCod ,
                                              A175Tabela_Descricao ,
                                              A191Tabela_SistemaDes ,
                                              A182Tabela_PaiNom ,
                                              A746Tabela_MelhoraCod ,
                                              A174Tabela_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV54WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1), 50, "%");
         lV54WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1), 50, "%");
         lV55WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1), 50, "%");
         lV55WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1), 50, "%");
         lV57WWTabelaDS_7_Tftabela_nome = StringUtil.PadR( StringUtil.RTrim( AV57WWTabelaDS_7_Tftabela_nome), 50, "%");
         lV59WWTabelaDS_9_Tftabela_descricao = StringUtil.Concat( StringUtil.RTrim( AV59WWTabelaDS_9_Tftabela_descricao), "%", "");
         lV61WWTabelaDS_11_Tftabela_sistemades = StringUtil.Concat( StringUtil.RTrim( AV61WWTabelaDS_11_Tftabela_sistemades), "%", "");
         lV63WWTabelaDS_13_Tftabela_modulodes = StringUtil.PadR( StringUtil.RTrim( AV63WWTabelaDS_13_Tftabela_modulodes), 50, "%");
         lV65WWTabelaDS_15_Tftabela_painom = StringUtil.PadR( StringUtil.RTrim( AV65WWTabelaDS_15_Tftabela_painom), 50, "%");
         /* Using cursor P00GW5 */
         pr_default.execute(3, new Object[] {AV51WWTabelaDS_1_Sistema_areatrabalhocod, lV54WWTabelaDS_4_Tabela_nome1, lV54WWTabelaDS_4_Tabela_nome1, lV55WWTabelaDS_5_Tabela_modulodes1, lV55WWTabelaDS_5_Tabela_modulodes1, AV56WWTabelaDS_6_Tabela_modulocod1, lV57WWTabelaDS_7_Tftabela_nome, AV58WWTabelaDS_8_Tftabela_nome_sel, lV59WWTabelaDS_9_Tftabela_descricao, AV60WWTabelaDS_10_Tftabela_descricao_sel, lV61WWTabelaDS_11_Tftabela_sistemades, AV62WWTabelaDS_12_Tftabela_sistemades_sel, lV63WWTabelaDS_13_Tftabela_modulodes, AV64WWTabelaDS_14_Tftabela_modulodes_sel, lV65WWTabelaDS_15_Tftabela_painom, AV66WWTabelaDS_16_Tftabela_painom_sel, AV67WWTabelaDS_17_Tftabela_melhoracod, AV68WWTabelaDS_18_Tftabela_melhoracod_to});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKGW8 = false;
            A190Tabela_SistemaCod = P00GW5_A190Tabela_SistemaCod[0];
            A181Tabela_PaiCod = P00GW5_A181Tabela_PaiCod[0];
            n181Tabela_PaiCod = P00GW5_n181Tabela_PaiCod[0];
            A188Tabela_ModuloCod = P00GW5_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = P00GW5_n188Tabela_ModuloCod[0];
            A174Tabela_Ativo = P00GW5_A174Tabela_Ativo[0];
            A746Tabela_MelhoraCod = P00GW5_A746Tabela_MelhoraCod[0];
            n746Tabela_MelhoraCod = P00GW5_n746Tabela_MelhoraCod[0];
            A182Tabela_PaiNom = P00GW5_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00GW5_n182Tabela_PaiNom[0];
            A191Tabela_SistemaDes = P00GW5_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00GW5_n191Tabela_SistemaDes[0];
            A175Tabela_Descricao = P00GW5_A175Tabela_Descricao[0];
            n175Tabela_Descricao = P00GW5_n175Tabela_Descricao[0];
            A189Tabela_ModuloDes = P00GW5_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = P00GW5_n189Tabela_ModuloDes[0];
            A173Tabela_Nome = P00GW5_A173Tabela_Nome[0];
            A135Sistema_AreaTrabalhoCod = P00GW5_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00GW5_n135Sistema_AreaTrabalhoCod[0];
            A172Tabela_Codigo = P00GW5_A172Tabela_Codigo[0];
            A191Tabela_SistemaDes = P00GW5_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00GW5_n191Tabela_SistemaDes[0];
            A135Sistema_AreaTrabalhoCod = P00GW5_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00GW5_n135Sistema_AreaTrabalhoCod[0];
            A182Tabela_PaiNom = P00GW5_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00GW5_n182Tabela_PaiNom[0];
            A189Tabela_ModuloDes = P00GW5_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = P00GW5_n189Tabela_ModuloDes[0];
            AV33count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( P00GW5_A188Tabela_ModuloCod[0] == A188Tabela_ModuloCod ) )
            {
               BRKGW8 = false;
               A172Tabela_Codigo = P00GW5_A172Tabela_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKGW8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A189Tabela_ModuloDes)) )
            {
               AV25Option = A189Tabela_ModuloDes;
               AV24InsertIndex = 1;
               while ( ( AV24InsertIndex <= AV26Options.Count ) && ( StringUtil.StrCmp(((String)AV26Options.Item(AV24InsertIndex)), AV25Option) < 0 ) )
               {
                  AV24InsertIndex = (int)(AV24InsertIndex+1);
               }
               AV26Options.Add(AV25Option, AV24InsertIndex);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), AV24InsertIndex);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKGW8 )
            {
               BRKGW8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADTABELA_PAINOMOPTIONS' Routine */
         AV18TFTabela_PaiNom = AV21SearchTxt;
         AV19TFTabela_PaiNom_Sel = "";
         AV51WWTabelaDS_1_Sistema_areatrabalhocod = AV39Sistema_AreaTrabalhoCod;
         AV52WWTabelaDS_2_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV53WWTabelaDS_3_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV54WWTabelaDS_4_Tabela_nome1 = AV42Tabela_Nome1;
         AV55WWTabelaDS_5_Tabela_modulodes1 = AV46Tabela_ModuloDes1;
         AV56WWTabelaDS_6_Tabela_modulocod1 = AV43Tabela_ModuloCod1;
         AV57WWTabelaDS_7_Tftabela_nome = AV10TFTabela_Nome;
         AV58WWTabelaDS_8_Tftabela_nome_sel = AV11TFTabela_Nome_Sel;
         AV59WWTabelaDS_9_Tftabela_descricao = AV12TFTabela_Descricao;
         AV60WWTabelaDS_10_Tftabela_descricao_sel = AV13TFTabela_Descricao_Sel;
         AV61WWTabelaDS_11_Tftabela_sistemades = AV14TFTabela_SistemaDes;
         AV62WWTabelaDS_12_Tftabela_sistemades_sel = AV15TFTabela_SistemaDes_Sel;
         AV63WWTabelaDS_13_Tftabela_modulodes = AV16TFTabela_ModuloDes;
         AV64WWTabelaDS_14_Tftabela_modulodes_sel = AV17TFTabela_ModuloDes_Sel;
         AV65WWTabelaDS_15_Tftabela_painom = AV18TFTabela_PaiNom;
         AV66WWTabelaDS_16_Tftabela_painom_sel = AV19TFTabela_PaiNom_Sel;
         AV67WWTabelaDS_17_Tftabela_melhoracod = AV44TFTabela_MelhoraCod;
         AV68WWTabelaDS_18_Tftabela_melhoracod_to = AV45TFTabela_MelhoraCod_To;
         AV69WWTabelaDS_19_Tftabela_ativo_sel = AV20TFTabela_Ativo_Sel;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              AV51WWTabelaDS_1_Sistema_areatrabalhocod ,
                                              AV52WWTabelaDS_2_Dynamicfiltersselector1 ,
                                              AV53WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                              AV54WWTabelaDS_4_Tabela_nome1 ,
                                              AV55WWTabelaDS_5_Tabela_modulodes1 ,
                                              AV56WWTabelaDS_6_Tabela_modulocod1 ,
                                              AV58WWTabelaDS_8_Tftabela_nome_sel ,
                                              AV57WWTabelaDS_7_Tftabela_nome ,
                                              AV60WWTabelaDS_10_Tftabela_descricao_sel ,
                                              AV59WWTabelaDS_9_Tftabela_descricao ,
                                              AV62WWTabelaDS_12_Tftabela_sistemades_sel ,
                                              AV61WWTabelaDS_11_Tftabela_sistemades ,
                                              AV64WWTabelaDS_14_Tftabela_modulodes_sel ,
                                              AV63WWTabelaDS_13_Tftabela_modulodes ,
                                              AV66WWTabelaDS_16_Tftabela_painom_sel ,
                                              AV65WWTabelaDS_15_Tftabela_painom ,
                                              AV67WWTabelaDS_17_Tftabela_melhoracod ,
                                              AV68WWTabelaDS_18_Tftabela_melhoracod_to ,
                                              AV69WWTabelaDS_19_Tftabela_ativo_sel ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A173Tabela_Nome ,
                                              A189Tabela_ModuloDes ,
                                              A188Tabela_ModuloCod ,
                                              A175Tabela_Descricao ,
                                              A191Tabela_SistemaDes ,
                                              A182Tabela_PaiNom ,
                                              A746Tabela_MelhoraCod ,
                                              A174Tabela_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV54WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1), 50, "%");
         lV54WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1), 50, "%");
         lV55WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1), 50, "%");
         lV55WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1), 50, "%");
         lV57WWTabelaDS_7_Tftabela_nome = StringUtil.PadR( StringUtil.RTrim( AV57WWTabelaDS_7_Tftabela_nome), 50, "%");
         lV59WWTabelaDS_9_Tftabela_descricao = StringUtil.Concat( StringUtil.RTrim( AV59WWTabelaDS_9_Tftabela_descricao), "%", "");
         lV61WWTabelaDS_11_Tftabela_sistemades = StringUtil.Concat( StringUtil.RTrim( AV61WWTabelaDS_11_Tftabela_sistemades), "%", "");
         lV63WWTabelaDS_13_Tftabela_modulodes = StringUtil.PadR( StringUtil.RTrim( AV63WWTabelaDS_13_Tftabela_modulodes), 50, "%");
         lV65WWTabelaDS_15_Tftabela_painom = StringUtil.PadR( StringUtil.RTrim( AV65WWTabelaDS_15_Tftabela_painom), 50, "%");
         /* Using cursor P00GW6 */
         pr_default.execute(4, new Object[] {AV51WWTabelaDS_1_Sistema_areatrabalhocod, lV54WWTabelaDS_4_Tabela_nome1, lV54WWTabelaDS_4_Tabela_nome1, lV55WWTabelaDS_5_Tabela_modulodes1, lV55WWTabelaDS_5_Tabela_modulodes1, AV56WWTabelaDS_6_Tabela_modulocod1, lV57WWTabelaDS_7_Tftabela_nome, AV58WWTabelaDS_8_Tftabela_nome_sel, lV59WWTabelaDS_9_Tftabela_descricao, AV60WWTabelaDS_10_Tftabela_descricao_sel, lV61WWTabelaDS_11_Tftabela_sistemades, AV62WWTabelaDS_12_Tftabela_sistemades_sel, lV63WWTabelaDS_13_Tftabela_modulodes, AV64WWTabelaDS_14_Tftabela_modulodes_sel, lV65WWTabelaDS_15_Tftabela_painom, AV66WWTabelaDS_16_Tftabela_painom_sel, AV67WWTabelaDS_17_Tftabela_melhoracod, AV68WWTabelaDS_18_Tftabela_melhoracod_to});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKGW10 = false;
            A190Tabela_SistemaCod = P00GW6_A190Tabela_SistemaCod[0];
            A181Tabela_PaiCod = P00GW6_A181Tabela_PaiCod[0];
            n181Tabela_PaiCod = P00GW6_n181Tabela_PaiCod[0];
            A182Tabela_PaiNom = P00GW6_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00GW6_n182Tabela_PaiNom[0];
            A174Tabela_Ativo = P00GW6_A174Tabela_Ativo[0];
            A746Tabela_MelhoraCod = P00GW6_A746Tabela_MelhoraCod[0];
            n746Tabela_MelhoraCod = P00GW6_n746Tabela_MelhoraCod[0];
            A191Tabela_SistemaDes = P00GW6_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00GW6_n191Tabela_SistemaDes[0];
            A175Tabela_Descricao = P00GW6_A175Tabela_Descricao[0];
            n175Tabela_Descricao = P00GW6_n175Tabela_Descricao[0];
            A188Tabela_ModuloCod = P00GW6_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = P00GW6_n188Tabela_ModuloCod[0];
            A189Tabela_ModuloDes = P00GW6_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = P00GW6_n189Tabela_ModuloDes[0];
            A173Tabela_Nome = P00GW6_A173Tabela_Nome[0];
            A135Sistema_AreaTrabalhoCod = P00GW6_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00GW6_n135Sistema_AreaTrabalhoCod[0];
            A172Tabela_Codigo = P00GW6_A172Tabela_Codigo[0];
            A191Tabela_SistemaDes = P00GW6_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00GW6_n191Tabela_SistemaDes[0];
            A135Sistema_AreaTrabalhoCod = P00GW6_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00GW6_n135Sistema_AreaTrabalhoCod[0];
            A182Tabela_PaiNom = P00GW6_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00GW6_n182Tabela_PaiNom[0];
            A189Tabela_ModuloDes = P00GW6_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = P00GW6_n189Tabela_ModuloDes[0];
            AV33count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00GW6_A182Tabela_PaiNom[0], A182Tabela_PaiNom) == 0 ) )
            {
               BRKGW10 = false;
               A181Tabela_PaiCod = P00GW6_A181Tabela_PaiCod[0];
               n181Tabela_PaiCod = P00GW6_n181Tabela_PaiCod[0];
               A172Tabela_Codigo = P00GW6_A172Tabela_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKGW10 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A182Tabela_PaiNom)) )
            {
               AV25Option = A182Tabela_PaiNom;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKGW10 )
            {
               BRKGW10 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV26Options = new GxSimpleCollection();
         AV29OptionsDesc = new GxSimpleCollection();
         AV31OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV34Session = context.GetSession();
         AV36GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV37GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFTabela_Nome = "";
         AV11TFTabela_Nome_Sel = "";
         AV12TFTabela_Descricao = "";
         AV13TFTabela_Descricao_Sel = "";
         AV14TFTabela_SistemaDes = "";
         AV15TFTabela_SistemaDes_Sel = "";
         AV16TFTabela_ModuloDes = "";
         AV17TFTabela_ModuloDes_Sel = "";
         AV18TFTabela_PaiNom = "";
         AV19TFTabela_PaiNom_Sel = "";
         AV38GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV40DynamicFiltersSelector1 = "";
         AV42Tabela_Nome1 = "";
         AV46Tabela_ModuloDes1 = "";
         AV52WWTabelaDS_2_Dynamicfiltersselector1 = "";
         AV54WWTabelaDS_4_Tabela_nome1 = "";
         AV55WWTabelaDS_5_Tabela_modulodes1 = "";
         AV57WWTabelaDS_7_Tftabela_nome = "";
         AV58WWTabelaDS_8_Tftabela_nome_sel = "";
         AV59WWTabelaDS_9_Tftabela_descricao = "";
         AV60WWTabelaDS_10_Tftabela_descricao_sel = "";
         AV61WWTabelaDS_11_Tftabela_sistemades = "";
         AV62WWTabelaDS_12_Tftabela_sistemades_sel = "";
         AV63WWTabelaDS_13_Tftabela_modulodes = "";
         AV64WWTabelaDS_14_Tftabela_modulodes_sel = "";
         AV65WWTabelaDS_15_Tftabela_painom = "";
         AV66WWTabelaDS_16_Tftabela_painom_sel = "";
         scmdbuf = "";
         lV54WWTabelaDS_4_Tabela_nome1 = "";
         lV55WWTabelaDS_5_Tabela_modulodes1 = "";
         lV57WWTabelaDS_7_Tftabela_nome = "";
         lV59WWTabelaDS_9_Tftabela_descricao = "";
         lV61WWTabelaDS_11_Tftabela_sistemades = "";
         lV63WWTabelaDS_13_Tftabela_modulodes = "";
         lV65WWTabelaDS_15_Tftabela_painom = "";
         A173Tabela_Nome = "";
         A189Tabela_ModuloDes = "";
         A175Tabela_Descricao = "";
         A191Tabela_SistemaDes = "";
         A182Tabela_PaiNom = "";
         P00GW2_A190Tabela_SistemaCod = new int[1] ;
         P00GW2_A181Tabela_PaiCod = new int[1] ;
         P00GW2_n181Tabela_PaiCod = new bool[] {false} ;
         P00GW2_A173Tabela_Nome = new String[] {""} ;
         P00GW2_A174Tabela_Ativo = new bool[] {false} ;
         P00GW2_A746Tabela_MelhoraCod = new int[1] ;
         P00GW2_n746Tabela_MelhoraCod = new bool[] {false} ;
         P00GW2_A182Tabela_PaiNom = new String[] {""} ;
         P00GW2_n182Tabela_PaiNom = new bool[] {false} ;
         P00GW2_A191Tabela_SistemaDes = new String[] {""} ;
         P00GW2_n191Tabela_SistemaDes = new bool[] {false} ;
         P00GW2_A175Tabela_Descricao = new String[] {""} ;
         P00GW2_n175Tabela_Descricao = new bool[] {false} ;
         P00GW2_A188Tabela_ModuloCod = new int[1] ;
         P00GW2_n188Tabela_ModuloCod = new bool[] {false} ;
         P00GW2_A189Tabela_ModuloDes = new String[] {""} ;
         P00GW2_n189Tabela_ModuloDes = new bool[] {false} ;
         P00GW2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00GW2_n135Sistema_AreaTrabalhoCod = new bool[] {false} ;
         P00GW2_A172Tabela_Codigo = new int[1] ;
         AV25Option = "";
         P00GW3_A190Tabela_SistemaCod = new int[1] ;
         P00GW3_A181Tabela_PaiCod = new int[1] ;
         P00GW3_n181Tabela_PaiCod = new bool[] {false} ;
         P00GW3_A175Tabela_Descricao = new String[] {""} ;
         P00GW3_n175Tabela_Descricao = new bool[] {false} ;
         P00GW3_A174Tabela_Ativo = new bool[] {false} ;
         P00GW3_A746Tabela_MelhoraCod = new int[1] ;
         P00GW3_n746Tabela_MelhoraCod = new bool[] {false} ;
         P00GW3_A182Tabela_PaiNom = new String[] {""} ;
         P00GW3_n182Tabela_PaiNom = new bool[] {false} ;
         P00GW3_A191Tabela_SistemaDes = new String[] {""} ;
         P00GW3_n191Tabela_SistemaDes = new bool[] {false} ;
         P00GW3_A188Tabela_ModuloCod = new int[1] ;
         P00GW3_n188Tabela_ModuloCod = new bool[] {false} ;
         P00GW3_A189Tabela_ModuloDes = new String[] {""} ;
         P00GW3_n189Tabela_ModuloDes = new bool[] {false} ;
         P00GW3_A173Tabela_Nome = new String[] {""} ;
         P00GW3_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00GW3_n135Sistema_AreaTrabalhoCod = new bool[] {false} ;
         P00GW3_A172Tabela_Codigo = new int[1] ;
         P00GW4_A181Tabela_PaiCod = new int[1] ;
         P00GW4_n181Tabela_PaiCod = new bool[] {false} ;
         P00GW4_A190Tabela_SistemaCod = new int[1] ;
         P00GW4_A174Tabela_Ativo = new bool[] {false} ;
         P00GW4_A746Tabela_MelhoraCod = new int[1] ;
         P00GW4_n746Tabela_MelhoraCod = new bool[] {false} ;
         P00GW4_A182Tabela_PaiNom = new String[] {""} ;
         P00GW4_n182Tabela_PaiNom = new bool[] {false} ;
         P00GW4_A191Tabela_SistemaDes = new String[] {""} ;
         P00GW4_n191Tabela_SistemaDes = new bool[] {false} ;
         P00GW4_A175Tabela_Descricao = new String[] {""} ;
         P00GW4_n175Tabela_Descricao = new bool[] {false} ;
         P00GW4_A188Tabela_ModuloCod = new int[1] ;
         P00GW4_n188Tabela_ModuloCod = new bool[] {false} ;
         P00GW4_A189Tabela_ModuloDes = new String[] {""} ;
         P00GW4_n189Tabela_ModuloDes = new bool[] {false} ;
         P00GW4_A173Tabela_Nome = new String[] {""} ;
         P00GW4_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00GW4_n135Sistema_AreaTrabalhoCod = new bool[] {false} ;
         P00GW4_A172Tabela_Codigo = new int[1] ;
         P00GW5_A190Tabela_SistemaCod = new int[1] ;
         P00GW5_A181Tabela_PaiCod = new int[1] ;
         P00GW5_n181Tabela_PaiCod = new bool[] {false} ;
         P00GW5_A188Tabela_ModuloCod = new int[1] ;
         P00GW5_n188Tabela_ModuloCod = new bool[] {false} ;
         P00GW5_A174Tabela_Ativo = new bool[] {false} ;
         P00GW5_A746Tabela_MelhoraCod = new int[1] ;
         P00GW5_n746Tabela_MelhoraCod = new bool[] {false} ;
         P00GW5_A182Tabela_PaiNom = new String[] {""} ;
         P00GW5_n182Tabela_PaiNom = new bool[] {false} ;
         P00GW5_A191Tabela_SistemaDes = new String[] {""} ;
         P00GW5_n191Tabela_SistemaDes = new bool[] {false} ;
         P00GW5_A175Tabela_Descricao = new String[] {""} ;
         P00GW5_n175Tabela_Descricao = new bool[] {false} ;
         P00GW5_A189Tabela_ModuloDes = new String[] {""} ;
         P00GW5_n189Tabela_ModuloDes = new bool[] {false} ;
         P00GW5_A173Tabela_Nome = new String[] {""} ;
         P00GW5_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00GW5_n135Sistema_AreaTrabalhoCod = new bool[] {false} ;
         P00GW5_A172Tabela_Codigo = new int[1] ;
         P00GW6_A190Tabela_SistemaCod = new int[1] ;
         P00GW6_A181Tabela_PaiCod = new int[1] ;
         P00GW6_n181Tabela_PaiCod = new bool[] {false} ;
         P00GW6_A182Tabela_PaiNom = new String[] {""} ;
         P00GW6_n182Tabela_PaiNom = new bool[] {false} ;
         P00GW6_A174Tabela_Ativo = new bool[] {false} ;
         P00GW6_A746Tabela_MelhoraCod = new int[1] ;
         P00GW6_n746Tabela_MelhoraCod = new bool[] {false} ;
         P00GW6_A191Tabela_SistemaDes = new String[] {""} ;
         P00GW6_n191Tabela_SistemaDes = new bool[] {false} ;
         P00GW6_A175Tabela_Descricao = new String[] {""} ;
         P00GW6_n175Tabela_Descricao = new bool[] {false} ;
         P00GW6_A188Tabela_ModuloCod = new int[1] ;
         P00GW6_n188Tabela_ModuloCod = new bool[] {false} ;
         P00GW6_A189Tabela_ModuloDes = new String[] {""} ;
         P00GW6_n189Tabela_ModuloDes = new bool[] {false} ;
         P00GW6_A173Tabela_Nome = new String[] {""} ;
         P00GW6_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00GW6_n135Sistema_AreaTrabalhoCod = new bool[] {false} ;
         P00GW6_A172Tabela_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwtabelafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00GW2_A190Tabela_SistemaCod, P00GW2_A181Tabela_PaiCod, P00GW2_n181Tabela_PaiCod, P00GW2_A173Tabela_Nome, P00GW2_A174Tabela_Ativo, P00GW2_A746Tabela_MelhoraCod, P00GW2_n746Tabela_MelhoraCod, P00GW2_A182Tabela_PaiNom, P00GW2_n182Tabela_PaiNom, P00GW2_A191Tabela_SistemaDes,
               P00GW2_n191Tabela_SistemaDes, P00GW2_A175Tabela_Descricao, P00GW2_n175Tabela_Descricao, P00GW2_A188Tabela_ModuloCod, P00GW2_n188Tabela_ModuloCod, P00GW2_A189Tabela_ModuloDes, P00GW2_n189Tabela_ModuloDes, P00GW2_A135Sistema_AreaTrabalhoCod, P00GW2_n135Sistema_AreaTrabalhoCod, P00GW2_A172Tabela_Codigo
               }
               , new Object[] {
               P00GW3_A190Tabela_SistemaCod, P00GW3_A181Tabela_PaiCod, P00GW3_n181Tabela_PaiCod, P00GW3_A175Tabela_Descricao, P00GW3_n175Tabela_Descricao, P00GW3_A174Tabela_Ativo, P00GW3_A746Tabela_MelhoraCod, P00GW3_n746Tabela_MelhoraCod, P00GW3_A182Tabela_PaiNom, P00GW3_n182Tabela_PaiNom,
               P00GW3_A191Tabela_SistemaDes, P00GW3_n191Tabela_SistemaDes, P00GW3_A188Tabela_ModuloCod, P00GW3_n188Tabela_ModuloCod, P00GW3_A189Tabela_ModuloDes, P00GW3_n189Tabela_ModuloDes, P00GW3_A173Tabela_Nome, P00GW3_A135Sistema_AreaTrabalhoCod, P00GW3_n135Sistema_AreaTrabalhoCod, P00GW3_A172Tabela_Codigo
               }
               , new Object[] {
               P00GW4_A181Tabela_PaiCod, P00GW4_n181Tabela_PaiCod, P00GW4_A190Tabela_SistemaCod, P00GW4_A174Tabela_Ativo, P00GW4_A746Tabela_MelhoraCod, P00GW4_n746Tabela_MelhoraCod, P00GW4_A182Tabela_PaiNom, P00GW4_n182Tabela_PaiNom, P00GW4_A191Tabela_SistemaDes, P00GW4_n191Tabela_SistemaDes,
               P00GW4_A175Tabela_Descricao, P00GW4_n175Tabela_Descricao, P00GW4_A188Tabela_ModuloCod, P00GW4_n188Tabela_ModuloCod, P00GW4_A189Tabela_ModuloDes, P00GW4_n189Tabela_ModuloDes, P00GW4_A173Tabela_Nome, P00GW4_A135Sistema_AreaTrabalhoCod, P00GW4_n135Sistema_AreaTrabalhoCod, P00GW4_A172Tabela_Codigo
               }
               , new Object[] {
               P00GW5_A190Tabela_SistemaCod, P00GW5_A181Tabela_PaiCod, P00GW5_n181Tabela_PaiCod, P00GW5_A188Tabela_ModuloCod, P00GW5_n188Tabela_ModuloCod, P00GW5_A174Tabela_Ativo, P00GW5_A746Tabela_MelhoraCod, P00GW5_n746Tabela_MelhoraCod, P00GW5_A182Tabela_PaiNom, P00GW5_n182Tabela_PaiNom,
               P00GW5_A191Tabela_SistemaDes, P00GW5_n191Tabela_SistemaDes, P00GW5_A175Tabela_Descricao, P00GW5_n175Tabela_Descricao, P00GW5_A189Tabela_ModuloDes, P00GW5_n189Tabela_ModuloDes, P00GW5_A173Tabela_Nome, P00GW5_A135Sistema_AreaTrabalhoCod, P00GW5_n135Sistema_AreaTrabalhoCod, P00GW5_A172Tabela_Codigo
               }
               , new Object[] {
               P00GW6_A190Tabela_SistemaCod, P00GW6_A181Tabela_PaiCod, P00GW6_n181Tabela_PaiCod, P00GW6_A182Tabela_PaiNom, P00GW6_n182Tabela_PaiNom, P00GW6_A174Tabela_Ativo, P00GW6_A746Tabela_MelhoraCod, P00GW6_n746Tabela_MelhoraCod, P00GW6_A191Tabela_SistemaDes, P00GW6_n191Tabela_SistemaDes,
               P00GW6_A175Tabela_Descricao, P00GW6_n175Tabela_Descricao, P00GW6_A188Tabela_ModuloCod, P00GW6_n188Tabela_ModuloCod, P00GW6_A189Tabela_ModuloDes, P00GW6_n189Tabela_ModuloDes, P00GW6_A173Tabela_Nome, P00GW6_A135Sistema_AreaTrabalhoCod, P00GW6_n135Sistema_AreaTrabalhoCod, P00GW6_A172Tabela_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV20TFTabela_Ativo_Sel ;
      private short AV41DynamicFiltersOperator1 ;
      private short AV53WWTabelaDS_3_Dynamicfiltersoperator1 ;
      private short AV69WWTabelaDS_19_Tftabela_ativo_sel ;
      private int AV49GXV1 ;
      private int AV39Sistema_AreaTrabalhoCod ;
      private int AV44TFTabela_MelhoraCod ;
      private int AV45TFTabela_MelhoraCod_To ;
      private int AV43Tabela_ModuloCod1 ;
      private int AV51WWTabelaDS_1_Sistema_areatrabalhocod ;
      private int AV56WWTabelaDS_6_Tabela_modulocod1 ;
      private int AV67WWTabelaDS_17_Tftabela_melhoracod ;
      private int AV68WWTabelaDS_18_Tftabela_melhoracod_to ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A188Tabela_ModuloCod ;
      private int A746Tabela_MelhoraCod ;
      private int A190Tabela_SistemaCod ;
      private int A181Tabela_PaiCod ;
      private int A172Tabela_Codigo ;
      private int AV24InsertIndex ;
      private long AV33count ;
      private String AV10TFTabela_Nome ;
      private String AV11TFTabela_Nome_Sel ;
      private String AV16TFTabela_ModuloDes ;
      private String AV17TFTabela_ModuloDes_Sel ;
      private String AV18TFTabela_PaiNom ;
      private String AV19TFTabela_PaiNom_Sel ;
      private String AV42Tabela_Nome1 ;
      private String AV46Tabela_ModuloDes1 ;
      private String AV54WWTabelaDS_4_Tabela_nome1 ;
      private String AV55WWTabelaDS_5_Tabela_modulodes1 ;
      private String AV57WWTabelaDS_7_Tftabela_nome ;
      private String AV58WWTabelaDS_8_Tftabela_nome_sel ;
      private String AV63WWTabelaDS_13_Tftabela_modulodes ;
      private String AV64WWTabelaDS_14_Tftabela_modulodes_sel ;
      private String AV65WWTabelaDS_15_Tftabela_painom ;
      private String AV66WWTabelaDS_16_Tftabela_painom_sel ;
      private String scmdbuf ;
      private String lV54WWTabelaDS_4_Tabela_nome1 ;
      private String lV55WWTabelaDS_5_Tabela_modulodes1 ;
      private String lV57WWTabelaDS_7_Tftabela_nome ;
      private String lV63WWTabelaDS_13_Tftabela_modulodes ;
      private String lV65WWTabelaDS_15_Tftabela_painom ;
      private String A173Tabela_Nome ;
      private String A189Tabela_ModuloDes ;
      private String A182Tabela_PaiNom ;
      private bool returnInSub ;
      private bool A174Tabela_Ativo ;
      private bool BRKGW2 ;
      private bool n181Tabela_PaiCod ;
      private bool n746Tabela_MelhoraCod ;
      private bool n182Tabela_PaiNom ;
      private bool n191Tabela_SistemaDes ;
      private bool n175Tabela_Descricao ;
      private bool n188Tabela_ModuloCod ;
      private bool n189Tabela_ModuloDes ;
      private bool n135Sistema_AreaTrabalhoCod ;
      private bool BRKGW4 ;
      private bool BRKGW6 ;
      private bool BRKGW8 ;
      private bool BRKGW10 ;
      private String AV32OptionIndexesJson ;
      private String AV27OptionsJson ;
      private String AV30OptionsDescJson ;
      private String A175Tabela_Descricao ;
      private String AV23DDOName ;
      private String AV21SearchTxt ;
      private String AV22SearchTxtTo ;
      private String AV12TFTabela_Descricao ;
      private String AV13TFTabela_Descricao_Sel ;
      private String AV14TFTabela_SistemaDes ;
      private String AV15TFTabela_SistemaDes_Sel ;
      private String AV40DynamicFiltersSelector1 ;
      private String AV52WWTabelaDS_2_Dynamicfiltersselector1 ;
      private String AV59WWTabelaDS_9_Tftabela_descricao ;
      private String AV60WWTabelaDS_10_Tftabela_descricao_sel ;
      private String AV61WWTabelaDS_11_Tftabela_sistemades ;
      private String AV62WWTabelaDS_12_Tftabela_sistemades_sel ;
      private String lV59WWTabelaDS_9_Tftabela_descricao ;
      private String lV61WWTabelaDS_11_Tftabela_sistemades ;
      private String A191Tabela_SistemaDes ;
      private String AV25Option ;
      private IGxSession AV34Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00GW2_A190Tabela_SistemaCod ;
      private int[] P00GW2_A181Tabela_PaiCod ;
      private bool[] P00GW2_n181Tabela_PaiCod ;
      private String[] P00GW2_A173Tabela_Nome ;
      private bool[] P00GW2_A174Tabela_Ativo ;
      private int[] P00GW2_A746Tabela_MelhoraCod ;
      private bool[] P00GW2_n746Tabela_MelhoraCod ;
      private String[] P00GW2_A182Tabela_PaiNom ;
      private bool[] P00GW2_n182Tabela_PaiNom ;
      private String[] P00GW2_A191Tabela_SistemaDes ;
      private bool[] P00GW2_n191Tabela_SistemaDes ;
      private String[] P00GW2_A175Tabela_Descricao ;
      private bool[] P00GW2_n175Tabela_Descricao ;
      private int[] P00GW2_A188Tabela_ModuloCod ;
      private bool[] P00GW2_n188Tabela_ModuloCod ;
      private String[] P00GW2_A189Tabela_ModuloDes ;
      private bool[] P00GW2_n189Tabela_ModuloDes ;
      private int[] P00GW2_A135Sistema_AreaTrabalhoCod ;
      private bool[] P00GW2_n135Sistema_AreaTrabalhoCod ;
      private int[] P00GW2_A172Tabela_Codigo ;
      private int[] P00GW3_A190Tabela_SistemaCod ;
      private int[] P00GW3_A181Tabela_PaiCod ;
      private bool[] P00GW3_n181Tabela_PaiCod ;
      private String[] P00GW3_A175Tabela_Descricao ;
      private bool[] P00GW3_n175Tabela_Descricao ;
      private bool[] P00GW3_A174Tabela_Ativo ;
      private int[] P00GW3_A746Tabela_MelhoraCod ;
      private bool[] P00GW3_n746Tabela_MelhoraCod ;
      private String[] P00GW3_A182Tabela_PaiNom ;
      private bool[] P00GW3_n182Tabela_PaiNom ;
      private String[] P00GW3_A191Tabela_SistemaDes ;
      private bool[] P00GW3_n191Tabela_SistemaDes ;
      private int[] P00GW3_A188Tabela_ModuloCod ;
      private bool[] P00GW3_n188Tabela_ModuloCod ;
      private String[] P00GW3_A189Tabela_ModuloDes ;
      private bool[] P00GW3_n189Tabela_ModuloDes ;
      private String[] P00GW3_A173Tabela_Nome ;
      private int[] P00GW3_A135Sistema_AreaTrabalhoCod ;
      private bool[] P00GW3_n135Sistema_AreaTrabalhoCod ;
      private int[] P00GW3_A172Tabela_Codigo ;
      private int[] P00GW4_A181Tabela_PaiCod ;
      private bool[] P00GW4_n181Tabela_PaiCod ;
      private int[] P00GW4_A190Tabela_SistemaCod ;
      private bool[] P00GW4_A174Tabela_Ativo ;
      private int[] P00GW4_A746Tabela_MelhoraCod ;
      private bool[] P00GW4_n746Tabela_MelhoraCod ;
      private String[] P00GW4_A182Tabela_PaiNom ;
      private bool[] P00GW4_n182Tabela_PaiNom ;
      private String[] P00GW4_A191Tabela_SistemaDes ;
      private bool[] P00GW4_n191Tabela_SistemaDes ;
      private String[] P00GW4_A175Tabela_Descricao ;
      private bool[] P00GW4_n175Tabela_Descricao ;
      private int[] P00GW4_A188Tabela_ModuloCod ;
      private bool[] P00GW4_n188Tabela_ModuloCod ;
      private String[] P00GW4_A189Tabela_ModuloDes ;
      private bool[] P00GW4_n189Tabela_ModuloDes ;
      private String[] P00GW4_A173Tabela_Nome ;
      private int[] P00GW4_A135Sistema_AreaTrabalhoCod ;
      private bool[] P00GW4_n135Sistema_AreaTrabalhoCod ;
      private int[] P00GW4_A172Tabela_Codigo ;
      private int[] P00GW5_A190Tabela_SistemaCod ;
      private int[] P00GW5_A181Tabela_PaiCod ;
      private bool[] P00GW5_n181Tabela_PaiCod ;
      private int[] P00GW5_A188Tabela_ModuloCod ;
      private bool[] P00GW5_n188Tabela_ModuloCod ;
      private bool[] P00GW5_A174Tabela_Ativo ;
      private int[] P00GW5_A746Tabela_MelhoraCod ;
      private bool[] P00GW5_n746Tabela_MelhoraCod ;
      private String[] P00GW5_A182Tabela_PaiNom ;
      private bool[] P00GW5_n182Tabela_PaiNom ;
      private String[] P00GW5_A191Tabela_SistemaDes ;
      private bool[] P00GW5_n191Tabela_SistemaDes ;
      private String[] P00GW5_A175Tabela_Descricao ;
      private bool[] P00GW5_n175Tabela_Descricao ;
      private String[] P00GW5_A189Tabela_ModuloDes ;
      private bool[] P00GW5_n189Tabela_ModuloDes ;
      private String[] P00GW5_A173Tabela_Nome ;
      private int[] P00GW5_A135Sistema_AreaTrabalhoCod ;
      private bool[] P00GW5_n135Sistema_AreaTrabalhoCod ;
      private int[] P00GW5_A172Tabela_Codigo ;
      private int[] P00GW6_A190Tabela_SistemaCod ;
      private int[] P00GW6_A181Tabela_PaiCod ;
      private bool[] P00GW6_n181Tabela_PaiCod ;
      private String[] P00GW6_A182Tabela_PaiNom ;
      private bool[] P00GW6_n182Tabela_PaiNom ;
      private bool[] P00GW6_A174Tabela_Ativo ;
      private int[] P00GW6_A746Tabela_MelhoraCod ;
      private bool[] P00GW6_n746Tabela_MelhoraCod ;
      private String[] P00GW6_A191Tabela_SistemaDes ;
      private bool[] P00GW6_n191Tabela_SistemaDes ;
      private String[] P00GW6_A175Tabela_Descricao ;
      private bool[] P00GW6_n175Tabela_Descricao ;
      private int[] P00GW6_A188Tabela_ModuloCod ;
      private bool[] P00GW6_n188Tabela_ModuloCod ;
      private String[] P00GW6_A189Tabela_ModuloDes ;
      private bool[] P00GW6_n189Tabela_ModuloDes ;
      private String[] P00GW6_A173Tabela_Nome ;
      private int[] P00GW6_A135Sistema_AreaTrabalhoCod ;
      private bool[] P00GW6_n135Sistema_AreaTrabalhoCod ;
      private int[] P00GW6_A172Tabela_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV36GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV37GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV38GridStateDynamicFilter ;
   }

   public class getwwtabelafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00GW2( IGxContext context ,
                                             int AV51WWTabelaDS_1_Sistema_areatrabalhocod ,
                                             String AV52WWTabelaDS_2_Dynamicfiltersselector1 ,
                                             short AV53WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                             String AV54WWTabelaDS_4_Tabela_nome1 ,
                                             String AV55WWTabelaDS_5_Tabela_modulodes1 ,
                                             int AV56WWTabelaDS_6_Tabela_modulocod1 ,
                                             String AV58WWTabelaDS_8_Tftabela_nome_sel ,
                                             String AV57WWTabelaDS_7_Tftabela_nome ,
                                             String AV60WWTabelaDS_10_Tftabela_descricao_sel ,
                                             String AV59WWTabelaDS_9_Tftabela_descricao ,
                                             String AV62WWTabelaDS_12_Tftabela_sistemades_sel ,
                                             String AV61WWTabelaDS_11_Tftabela_sistemades ,
                                             String AV64WWTabelaDS_14_Tftabela_modulodes_sel ,
                                             String AV63WWTabelaDS_13_Tftabela_modulodes ,
                                             String AV66WWTabelaDS_16_Tftabela_painom_sel ,
                                             String AV65WWTabelaDS_15_Tftabela_painom ,
                                             int AV67WWTabelaDS_17_Tftabela_melhoracod ,
                                             int AV68WWTabelaDS_18_Tftabela_melhoracod_to ,
                                             short AV69WWTabelaDS_19_Tftabela_ativo_sel ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A173Tabela_Nome ,
                                             String A189Tabela_ModuloDes ,
                                             int A188Tabela_ModuloCod ,
                                             String A175Tabela_Descricao ,
                                             String A191Tabela_SistemaDes ,
                                             String A182Tabela_PaiNom ,
                                             int A746Tabela_MelhoraCod ,
                                             bool A174Tabela_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [18] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Tabela_PaiCod] AS Tabela_PaiCod, T1.[Tabela_Nome], T1.[Tabela_Ativo], T1.[Tabela_MelhoraCod], T3.[Tabela_Nome] AS Tabela_PaiNom, T2.[Sistema_Nome] AS Tabela_SistemaDes, T1.[Tabela_Descricao], T1.[Tabela_ModuloCod] AS Tabela_ModuloCod, T4.[Modulo_Nome] AS Tabela_ModuloDes, T2.[Sistema_AreaTrabalhoCod], T1.[Tabela_Codigo] FROM ((([Tabela] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Tabela_SistemaCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T1.[Tabela_PaiCod]) LEFT JOIN [Modulo] T4 WITH (NOLOCK) ON T4.[Modulo_Codigo] = T1.[Tabela_ModuloCod])";
         if ( ! (0==AV51WWTabelaDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_AreaTrabalhoCod] = @AV51WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_AreaTrabalhoCod] = @AV51WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) || ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV54WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV54WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) || ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV54WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like '%' + @lV54WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like '%' + @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like '%' + @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULOCOD") == 0 ) && ( ! (0==AV56WWTabelaDS_6_Tabela_modulocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_ModuloCod] = @AV56WWTabelaDS_6_Tabela_modulocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_ModuloCod] = @AV56WWTabelaDS_6_Tabela_modulocod1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58WWTabelaDS_8_Tftabela_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWTabelaDS_7_Tftabela_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV57WWTabelaDS_7_Tftabela_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV57WWTabelaDS_7_Tftabela_nome)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWTabelaDS_8_Tftabela_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] = @AV58WWTabelaDS_8_Tftabela_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] = @AV58WWTabelaDS_8_Tftabela_nome_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60WWTabelaDS_10_Tftabela_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWTabelaDS_9_Tftabela_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] like @lV59WWTabelaDS_9_Tftabela_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] like @lV59WWTabelaDS_9_Tftabela_descricao)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWTabelaDS_10_Tftabela_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] = @AV60WWTabelaDS_10_Tftabela_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] = @AV60WWTabelaDS_10_Tftabela_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTabelaDS_12_Tftabela_sistemades_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWTabelaDS_11_Tftabela_sistemades)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV61WWTabelaDS_11_Tftabela_sistemades)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV61WWTabelaDS_11_Tftabela_sistemades)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTabelaDS_12_Tftabela_sistemades_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] = @AV62WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] = @AV62WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTabelaDS_14_Tftabela_modulodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWTabelaDS_13_Tftabela_modulodes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like @lV63WWTabelaDS_13_Tftabela_modulodes)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like @lV63WWTabelaDS_13_Tftabela_modulodes)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTabelaDS_14_Tftabela_modulodes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] = @AV64WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] = @AV64WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWTabelaDS_16_Tftabela_painom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWTabelaDS_15_Tftabela_painom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV65WWTabelaDS_15_Tftabela_painom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] like @lV65WWTabelaDS_15_Tftabela_painom)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWTabelaDS_16_Tftabela_painom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV66WWTabelaDS_16_Tftabela_painom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] = @AV66WWTabelaDS_16_Tftabela_painom_sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (0==AV67WWTabelaDS_17_Tftabela_melhoracod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] >= @AV67WWTabelaDS_17_Tftabela_melhoracod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] >= @AV67WWTabelaDS_17_Tftabela_melhoracod)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (0==AV68WWTabelaDS_18_Tftabela_melhoracod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] <= @AV68WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] <= @AV68WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV69WWTabelaDS_19_Tftabela_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 1)";
            }
         }
         if ( AV69WWTabelaDS_19_Tftabela_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Tabela_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00GW3( IGxContext context ,
                                             int AV51WWTabelaDS_1_Sistema_areatrabalhocod ,
                                             String AV52WWTabelaDS_2_Dynamicfiltersselector1 ,
                                             short AV53WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                             String AV54WWTabelaDS_4_Tabela_nome1 ,
                                             String AV55WWTabelaDS_5_Tabela_modulodes1 ,
                                             int AV56WWTabelaDS_6_Tabela_modulocod1 ,
                                             String AV58WWTabelaDS_8_Tftabela_nome_sel ,
                                             String AV57WWTabelaDS_7_Tftabela_nome ,
                                             String AV60WWTabelaDS_10_Tftabela_descricao_sel ,
                                             String AV59WWTabelaDS_9_Tftabela_descricao ,
                                             String AV62WWTabelaDS_12_Tftabela_sistemades_sel ,
                                             String AV61WWTabelaDS_11_Tftabela_sistemades ,
                                             String AV64WWTabelaDS_14_Tftabela_modulodes_sel ,
                                             String AV63WWTabelaDS_13_Tftabela_modulodes ,
                                             String AV66WWTabelaDS_16_Tftabela_painom_sel ,
                                             String AV65WWTabelaDS_15_Tftabela_painom ,
                                             int AV67WWTabelaDS_17_Tftabela_melhoracod ,
                                             int AV68WWTabelaDS_18_Tftabela_melhoracod_to ,
                                             short AV69WWTabelaDS_19_Tftabela_ativo_sel ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A173Tabela_Nome ,
                                             String A189Tabela_ModuloDes ,
                                             int A188Tabela_ModuloCod ,
                                             String A175Tabela_Descricao ,
                                             String A191Tabela_SistemaDes ,
                                             String A182Tabela_PaiNom ,
                                             int A746Tabela_MelhoraCod ,
                                             bool A174Tabela_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [18] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Tabela_PaiCod] AS Tabela_PaiCod, T1.[Tabela_Descricao], T1.[Tabela_Ativo], T1.[Tabela_MelhoraCod], T3.[Tabela_Nome] AS Tabela_PaiNom, T2.[Sistema_Nome] AS Tabela_SistemaDes, T1.[Tabela_ModuloCod] AS Tabela_ModuloCod, T4.[Modulo_Nome] AS Tabela_ModuloDes, T1.[Tabela_Nome], T2.[Sistema_AreaTrabalhoCod], T1.[Tabela_Codigo] FROM ((([Tabela] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Tabela_SistemaCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T1.[Tabela_PaiCod]) LEFT JOIN [Modulo] T4 WITH (NOLOCK) ON T4.[Modulo_Codigo] = T1.[Tabela_ModuloCod])";
         if ( ! (0==AV51WWTabelaDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_AreaTrabalhoCod] = @AV51WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_AreaTrabalhoCod] = @AV51WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) || ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV54WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV54WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) || ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV54WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like '%' + @lV54WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like '%' + @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like '%' + @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULOCOD") == 0 ) && ( ! (0==AV56WWTabelaDS_6_Tabela_modulocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_ModuloCod] = @AV56WWTabelaDS_6_Tabela_modulocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_ModuloCod] = @AV56WWTabelaDS_6_Tabela_modulocod1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58WWTabelaDS_8_Tftabela_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWTabelaDS_7_Tftabela_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV57WWTabelaDS_7_Tftabela_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV57WWTabelaDS_7_Tftabela_nome)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWTabelaDS_8_Tftabela_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] = @AV58WWTabelaDS_8_Tftabela_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] = @AV58WWTabelaDS_8_Tftabela_nome_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60WWTabelaDS_10_Tftabela_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWTabelaDS_9_Tftabela_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] like @lV59WWTabelaDS_9_Tftabela_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] like @lV59WWTabelaDS_9_Tftabela_descricao)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWTabelaDS_10_Tftabela_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] = @AV60WWTabelaDS_10_Tftabela_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] = @AV60WWTabelaDS_10_Tftabela_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTabelaDS_12_Tftabela_sistemades_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWTabelaDS_11_Tftabela_sistemades)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV61WWTabelaDS_11_Tftabela_sistemades)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV61WWTabelaDS_11_Tftabela_sistemades)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTabelaDS_12_Tftabela_sistemades_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] = @AV62WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] = @AV62WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTabelaDS_14_Tftabela_modulodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWTabelaDS_13_Tftabela_modulodes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like @lV63WWTabelaDS_13_Tftabela_modulodes)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like @lV63WWTabelaDS_13_Tftabela_modulodes)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTabelaDS_14_Tftabela_modulodes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] = @AV64WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] = @AV64WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWTabelaDS_16_Tftabela_painom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWTabelaDS_15_Tftabela_painom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV65WWTabelaDS_15_Tftabela_painom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] like @lV65WWTabelaDS_15_Tftabela_painom)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWTabelaDS_16_Tftabela_painom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV66WWTabelaDS_16_Tftabela_painom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] = @AV66WWTabelaDS_16_Tftabela_painom_sel)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (0==AV67WWTabelaDS_17_Tftabela_melhoracod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] >= @AV67WWTabelaDS_17_Tftabela_melhoracod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] >= @AV67WWTabelaDS_17_Tftabela_melhoracod)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (0==AV68WWTabelaDS_18_Tftabela_melhoracod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] <= @AV68WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] <= @AV68WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV69WWTabelaDS_19_Tftabela_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 1)";
            }
         }
         if ( AV69WWTabelaDS_19_Tftabela_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Tabela_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00GW4( IGxContext context ,
                                             int AV51WWTabelaDS_1_Sistema_areatrabalhocod ,
                                             String AV52WWTabelaDS_2_Dynamicfiltersselector1 ,
                                             short AV53WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                             String AV54WWTabelaDS_4_Tabela_nome1 ,
                                             String AV55WWTabelaDS_5_Tabela_modulodes1 ,
                                             int AV56WWTabelaDS_6_Tabela_modulocod1 ,
                                             String AV58WWTabelaDS_8_Tftabela_nome_sel ,
                                             String AV57WWTabelaDS_7_Tftabela_nome ,
                                             String AV60WWTabelaDS_10_Tftabela_descricao_sel ,
                                             String AV59WWTabelaDS_9_Tftabela_descricao ,
                                             String AV62WWTabelaDS_12_Tftabela_sistemades_sel ,
                                             String AV61WWTabelaDS_11_Tftabela_sistemades ,
                                             String AV64WWTabelaDS_14_Tftabela_modulodes_sel ,
                                             String AV63WWTabelaDS_13_Tftabela_modulodes ,
                                             String AV66WWTabelaDS_16_Tftabela_painom_sel ,
                                             String AV65WWTabelaDS_15_Tftabela_painom ,
                                             int AV67WWTabelaDS_17_Tftabela_melhoracod ,
                                             int AV68WWTabelaDS_18_Tftabela_melhoracod_to ,
                                             short AV69WWTabelaDS_19_Tftabela_ativo_sel ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A173Tabela_Nome ,
                                             String A189Tabela_ModuloDes ,
                                             int A188Tabela_ModuloCod ,
                                             String A175Tabela_Descricao ,
                                             String A191Tabela_SistemaDes ,
                                             String A182Tabela_PaiNom ,
                                             int A746Tabela_MelhoraCod ,
                                             bool A174Tabela_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [18] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Tabela_PaiCod] AS Tabela_PaiCod, T1.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Tabela_Ativo], T1.[Tabela_MelhoraCod], T2.[Tabela_Nome] AS Tabela_PaiNom, T3.[Sistema_Nome] AS Tabela_SistemaDes, T1.[Tabela_Descricao], T1.[Tabela_ModuloCod] AS Tabela_ModuloCod, T4.[Modulo_Nome] AS Tabela_ModuloDes, T1.[Tabela_Nome], T3.[Sistema_AreaTrabalhoCod], T1.[Tabela_Codigo] FROM ((([Tabela] T1 WITH (NOLOCK) LEFT JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Tabela_PaiCod]) INNER JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[Tabela_SistemaCod]) LEFT JOIN [Modulo] T4 WITH (NOLOCK) ON T4.[Modulo_Codigo] = T1.[Tabela_ModuloCod])";
         if ( ! (0==AV51WWTabelaDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Sistema_AreaTrabalhoCod] = @AV51WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Sistema_AreaTrabalhoCod] = @AV51WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) || ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV54WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV54WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) || ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV54WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like '%' + @lV54WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like '%' + @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like '%' + @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULOCOD") == 0 ) && ( ! (0==AV56WWTabelaDS_6_Tabela_modulocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_ModuloCod] = @AV56WWTabelaDS_6_Tabela_modulocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_ModuloCod] = @AV56WWTabelaDS_6_Tabela_modulocod1)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58WWTabelaDS_8_Tftabela_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWTabelaDS_7_Tftabela_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV57WWTabelaDS_7_Tftabela_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV57WWTabelaDS_7_Tftabela_nome)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWTabelaDS_8_Tftabela_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] = @AV58WWTabelaDS_8_Tftabela_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] = @AV58WWTabelaDS_8_Tftabela_nome_sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60WWTabelaDS_10_Tftabela_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWTabelaDS_9_Tftabela_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] like @lV59WWTabelaDS_9_Tftabela_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] like @lV59WWTabelaDS_9_Tftabela_descricao)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWTabelaDS_10_Tftabela_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] = @AV60WWTabelaDS_10_Tftabela_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] = @AV60WWTabelaDS_10_Tftabela_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTabelaDS_12_Tftabela_sistemades_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWTabelaDS_11_Tftabela_sistemades)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Sistema_Nome] like @lV61WWTabelaDS_11_Tftabela_sistemades)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Sistema_Nome] like @lV61WWTabelaDS_11_Tftabela_sistemades)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTabelaDS_12_Tftabela_sistemades_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Sistema_Nome] = @AV62WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Sistema_Nome] = @AV62WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTabelaDS_14_Tftabela_modulodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWTabelaDS_13_Tftabela_modulodes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like @lV63WWTabelaDS_13_Tftabela_modulodes)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like @lV63WWTabelaDS_13_Tftabela_modulodes)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTabelaDS_14_Tftabela_modulodes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] = @AV64WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] = @AV64WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWTabelaDS_16_Tftabela_painom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWTabelaDS_15_Tftabela_painom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV65WWTabelaDS_15_Tftabela_painom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV65WWTabelaDS_15_Tftabela_painom)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWTabelaDS_16_Tftabela_painom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] = @AV66WWTabelaDS_16_Tftabela_painom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] = @AV66WWTabelaDS_16_Tftabela_painom_sel)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (0==AV67WWTabelaDS_17_Tftabela_melhoracod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] >= @AV67WWTabelaDS_17_Tftabela_melhoracod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] >= @AV67WWTabelaDS_17_Tftabela_melhoracod)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (0==AV68WWTabelaDS_18_Tftabela_melhoracod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] <= @AV68WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] <= @AV68WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( AV69WWTabelaDS_19_Tftabela_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 1)";
            }
         }
         if ( AV69WWTabelaDS_19_Tftabela_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Tabela_SistemaCod]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00GW5( IGxContext context ,
                                             int AV51WWTabelaDS_1_Sistema_areatrabalhocod ,
                                             String AV52WWTabelaDS_2_Dynamicfiltersselector1 ,
                                             short AV53WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                             String AV54WWTabelaDS_4_Tabela_nome1 ,
                                             String AV55WWTabelaDS_5_Tabela_modulodes1 ,
                                             int AV56WWTabelaDS_6_Tabela_modulocod1 ,
                                             String AV58WWTabelaDS_8_Tftabela_nome_sel ,
                                             String AV57WWTabelaDS_7_Tftabela_nome ,
                                             String AV60WWTabelaDS_10_Tftabela_descricao_sel ,
                                             String AV59WWTabelaDS_9_Tftabela_descricao ,
                                             String AV62WWTabelaDS_12_Tftabela_sistemades_sel ,
                                             String AV61WWTabelaDS_11_Tftabela_sistemades ,
                                             String AV64WWTabelaDS_14_Tftabela_modulodes_sel ,
                                             String AV63WWTabelaDS_13_Tftabela_modulodes ,
                                             String AV66WWTabelaDS_16_Tftabela_painom_sel ,
                                             String AV65WWTabelaDS_15_Tftabela_painom ,
                                             int AV67WWTabelaDS_17_Tftabela_melhoracod ,
                                             int AV68WWTabelaDS_18_Tftabela_melhoracod_to ,
                                             short AV69WWTabelaDS_19_Tftabela_ativo_sel ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A173Tabela_Nome ,
                                             String A189Tabela_ModuloDes ,
                                             int A188Tabela_ModuloCod ,
                                             String A175Tabela_Descricao ,
                                             String A191Tabela_SistemaDes ,
                                             String A182Tabela_PaiNom ,
                                             int A746Tabela_MelhoraCod ,
                                             bool A174Tabela_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [18] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Tabela_PaiCod] AS Tabela_PaiCod, T1.[Tabela_ModuloCod] AS Tabela_ModuloCod, T1.[Tabela_Ativo], T1.[Tabela_MelhoraCod], T3.[Tabela_Nome] AS Tabela_PaiNom, T2.[Sistema_Nome] AS Tabela_SistemaDes, T1.[Tabela_Descricao], T4.[Modulo_Nome] AS Tabela_ModuloDes, T1.[Tabela_Nome], T2.[Sistema_AreaTrabalhoCod], T1.[Tabela_Codigo] FROM ((([Tabela] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Tabela_SistemaCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T1.[Tabela_PaiCod]) LEFT JOIN [Modulo] T4 WITH (NOLOCK) ON T4.[Modulo_Codigo] = T1.[Tabela_ModuloCod])";
         if ( ! (0==AV51WWTabelaDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_AreaTrabalhoCod] = @AV51WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_AreaTrabalhoCod] = @AV51WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) || ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV54WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV54WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) || ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV54WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like '%' + @lV54WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like '%' + @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like '%' + @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULOCOD") == 0 ) && ( ! (0==AV56WWTabelaDS_6_Tabela_modulocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_ModuloCod] = @AV56WWTabelaDS_6_Tabela_modulocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_ModuloCod] = @AV56WWTabelaDS_6_Tabela_modulocod1)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58WWTabelaDS_8_Tftabela_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWTabelaDS_7_Tftabela_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV57WWTabelaDS_7_Tftabela_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV57WWTabelaDS_7_Tftabela_nome)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWTabelaDS_8_Tftabela_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] = @AV58WWTabelaDS_8_Tftabela_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] = @AV58WWTabelaDS_8_Tftabela_nome_sel)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60WWTabelaDS_10_Tftabela_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWTabelaDS_9_Tftabela_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] like @lV59WWTabelaDS_9_Tftabela_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] like @lV59WWTabelaDS_9_Tftabela_descricao)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWTabelaDS_10_Tftabela_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] = @AV60WWTabelaDS_10_Tftabela_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] = @AV60WWTabelaDS_10_Tftabela_descricao_sel)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTabelaDS_12_Tftabela_sistemades_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWTabelaDS_11_Tftabela_sistemades)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV61WWTabelaDS_11_Tftabela_sistemades)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV61WWTabelaDS_11_Tftabela_sistemades)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTabelaDS_12_Tftabela_sistemades_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] = @AV62WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] = @AV62WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTabelaDS_14_Tftabela_modulodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWTabelaDS_13_Tftabela_modulodes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like @lV63WWTabelaDS_13_Tftabela_modulodes)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like @lV63WWTabelaDS_13_Tftabela_modulodes)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTabelaDS_14_Tftabela_modulodes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] = @AV64WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] = @AV64WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWTabelaDS_16_Tftabela_painom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWTabelaDS_15_Tftabela_painom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV65WWTabelaDS_15_Tftabela_painom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] like @lV65WWTabelaDS_15_Tftabela_painom)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWTabelaDS_16_Tftabela_painom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV66WWTabelaDS_16_Tftabela_painom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] = @AV66WWTabelaDS_16_Tftabela_painom_sel)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ! (0==AV67WWTabelaDS_17_Tftabela_melhoracod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] >= @AV67WWTabelaDS_17_Tftabela_melhoracod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] >= @AV67WWTabelaDS_17_Tftabela_melhoracod)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( ! (0==AV68WWTabelaDS_18_Tftabela_melhoracod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] <= @AV68WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] <= @AV68WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( AV69WWTabelaDS_19_Tftabela_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 1)";
            }
         }
         if ( AV69WWTabelaDS_19_Tftabela_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Tabela_ModuloCod]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00GW6( IGxContext context ,
                                             int AV51WWTabelaDS_1_Sistema_areatrabalhocod ,
                                             String AV52WWTabelaDS_2_Dynamicfiltersselector1 ,
                                             short AV53WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                             String AV54WWTabelaDS_4_Tabela_nome1 ,
                                             String AV55WWTabelaDS_5_Tabela_modulodes1 ,
                                             int AV56WWTabelaDS_6_Tabela_modulocod1 ,
                                             String AV58WWTabelaDS_8_Tftabela_nome_sel ,
                                             String AV57WWTabelaDS_7_Tftabela_nome ,
                                             String AV60WWTabelaDS_10_Tftabela_descricao_sel ,
                                             String AV59WWTabelaDS_9_Tftabela_descricao ,
                                             String AV62WWTabelaDS_12_Tftabela_sistemades_sel ,
                                             String AV61WWTabelaDS_11_Tftabela_sistemades ,
                                             String AV64WWTabelaDS_14_Tftabela_modulodes_sel ,
                                             String AV63WWTabelaDS_13_Tftabela_modulodes ,
                                             String AV66WWTabelaDS_16_Tftabela_painom_sel ,
                                             String AV65WWTabelaDS_15_Tftabela_painom ,
                                             int AV67WWTabelaDS_17_Tftabela_melhoracod ,
                                             int AV68WWTabelaDS_18_Tftabela_melhoracod_to ,
                                             short AV69WWTabelaDS_19_Tftabela_ativo_sel ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A173Tabela_Nome ,
                                             String A189Tabela_ModuloDes ,
                                             int A188Tabela_ModuloCod ,
                                             String A175Tabela_Descricao ,
                                             String A191Tabela_SistemaDes ,
                                             String A182Tabela_PaiNom ,
                                             int A746Tabela_MelhoraCod ,
                                             bool A174Tabela_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [18] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Tabela_PaiCod] AS Tabela_PaiCod, T3.[Tabela_Nome] AS Tabela_PaiNom, T1.[Tabela_Ativo], T1.[Tabela_MelhoraCod], T2.[Sistema_Nome] AS Tabela_SistemaDes, T1.[Tabela_Descricao], T1.[Tabela_ModuloCod] AS Tabela_ModuloCod, T4.[Modulo_Nome] AS Tabela_ModuloDes, T1.[Tabela_Nome], T2.[Sistema_AreaTrabalhoCod], T1.[Tabela_Codigo] FROM ((([Tabela] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Tabela_SistemaCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T1.[Tabela_PaiCod]) LEFT JOIN [Modulo] T4 WITH (NOLOCK) ON T4.[Modulo_Codigo] = T1.[Tabela_ModuloCod])";
         if ( ! (0==AV51WWTabelaDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_AreaTrabalhoCod] = @AV51WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_AreaTrabalhoCod] = @AV51WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int9[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) || ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV54WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV54WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int9[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) || ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV54WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like '%' + @lV54WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int9[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int9[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV53WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like '%' + @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like '%' + @lV55WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int9[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULOCOD") == 0 ) && ( ! (0==AV56WWTabelaDS_6_Tabela_modulocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_ModuloCod] = @AV56WWTabelaDS_6_Tabela_modulocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_ModuloCod] = @AV56WWTabelaDS_6_Tabela_modulocod1)";
            }
         }
         else
         {
            GXv_int9[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58WWTabelaDS_8_Tftabela_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWTabelaDS_7_Tftabela_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV57WWTabelaDS_7_Tftabela_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV57WWTabelaDS_7_Tftabela_nome)";
            }
         }
         else
         {
            GXv_int9[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWTabelaDS_8_Tftabela_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] = @AV58WWTabelaDS_8_Tftabela_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] = @AV58WWTabelaDS_8_Tftabela_nome_sel)";
            }
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60WWTabelaDS_10_Tftabela_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWTabelaDS_9_Tftabela_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] like @lV59WWTabelaDS_9_Tftabela_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] like @lV59WWTabelaDS_9_Tftabela_descricao)";
            }
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWTabelaDS_10_Tftabela_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] = @AV60WWTabelaDS_10_Tftabela_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] = @AV60WWTabelaDS_10_Tftabela_descricao_sel)";
            }
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTabelaDS_12_Tftabela_sistemades_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWTabelaDS_11_Tftabela_sistemades)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV61WWTabelaDS_11_Tftabela_sistemades)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV61WWTabelaDS_11_Tftabela_sistemades)";
            }
         }
         else
         {
            GXv_int9[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTabelaDS_12_Tftabela_sistemades_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] = @AV62WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] = @AV62WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
         }
         else
         {
            GXv_int9[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTabelaDS_14_Tftabela_modulodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWTabelaDS_13_Tftabela_modulodes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] like @lV63WWTabelaDS_13_Tftabela_modulodes)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] like @lV63WWTabelaDS_13_Tftabela_modulodes)";
            }
         }
         else
         {
            GXv_int9[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTabelaDS_14_Tftabela_modulodes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Modulo_Nome] = @AV64WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Modulo_Nome] = @AV64WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
         }
         else
         {
            GXv_int9[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWTabelaDS_16_Tftabela_painom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWTabelaDS_15_Tftabela_painom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV65WWTabelaDS_15_Tftabela_painom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] like @lV65WWTabelaDS_15_Tftabela_painom)";
            }
         }
         else
         {
            GXv_int9[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWTabelaDS_16_Tftabela_painom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV66WWTabelaDS_16_Tftabela_painom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] = @AV66WWTabelaDS_16_Tftabela_painom_sel)";
            }
         }
         else
         {
            GXv_int9[15] = 1;
         }
         if ( ! (0==AV67WWTabelaDS_17_Tftabela_melhoracod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] >= @AV67WWTabelaDS_17_Tftabela_melhoracod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] >= @AV67WWTabelaDS_17_Tftabela_melhoracod)";
            }
         }
         else
         {
            GXv_int9[16] = 1;
         }
         if ( ! (0==AV68WWTabelaDS_18_Tftabela_melhoracod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] <= @AV68WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] <= @AV68WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
         }
         else
         {
            GXv_int9[17] = 1;
         }
         if ( AV69WWTabelaDS_19_Tftabela_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 1)";
            }
         }
         if ( AV69WWTabelaDS_19_Tftabela_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Tabela_Nome]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00GW2(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (bool)dynConstraints[27] );
               case 1 :
                     return conditional_P00GW3(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (bool)dynConstraints[27] );
               case 2 :
                     return conditional_P00GW4(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (bool)dynConstraints[27] );
               case 3 :
                     return conditional_P00GW5(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (bool)dynConstraints[27] );
               case 4 :
                     return conditional_P00GW6(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (bool)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00GW2 ;
          prmP00GW2 = new Object[] {
          new Object[] {"@AV51WWTabelaDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV54WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56WWTabelaDS_6_Tabela_modulocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV57WWTabelaDS_7_Tftabela_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58WWTabelaDS_8_Tftabela_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWTabelaDS_9_Tftabela_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV60WWTabelaDS_10_Tftabela_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV61WWTabelaDS_11_Tftabela_sistemades",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV62WWTabelaDS_12_Tftabela_sistemades_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV63WWTabelaDS_13_Tftabela_modulodes",SqlDbType.Char,50,0} ,
          new Object[] {"@AV64WWTabelaDS_14_Tftabela_modulodes_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWTabelaDS_15_Tftabela_painom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWTabelaDS_16_Tftabela_painom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWTabelaDS_17_Tftabela_melhoracod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWTabelaDS_18_Tftabela_melhoracod_to",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00GW3 ;
          prmP00GW3 = new Object[] {
          new Object[] {"@AV51WWTabelaDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV54WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56WWTabelaDS_6_Tabela_modulocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV57WWTabelaDS_7_Tftabela_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58WWTabelaDS_8_Tftabela_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWTabelaDS_9_Tftabela_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV60WWTabelaDS_10_Tftabela_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV61WWTabelaDS_11_Tftabela_sistemades",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV62WWTabelaDS_12_Tftabela_sistemades_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV63WWTabelaDS_13_Tftabela_modulodes",SqlDbType.Char,50,0} ,
          new Object[] {"@AV64WWTabelaDS_14_Tftabela_modulodes_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWTabelaDS_15_Tftabela_painom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWTabelaDS_16_Tftabela_painom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWTabelaDS_17_Tftabela_melhoracod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWTabelaDS_18_Tftabela_melhoracod_to",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00GW4 ;
          prmP00GW4 = new Object[] {
          new Object[] {"@AV51WWTabelaDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV54WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56WWTabelaDS_6_Tabela_modulocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV57WWTabelaDS_7_Tftabela_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58WWTabelaDS_8_Tftabela_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWTabelaDS_9_Tftabela_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV60WWTabelaDS_10_Tftabela_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV61WWTabelaDS_11_Tftabela_sistemades",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV62WWTabelaDS_12_Tftabela_sistemades_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV63WWTabelaDS_13_Tftabela_modulodes",SqlDbType.Char,50,0} ,
          new Object[] {"@AV64WWTabelaDS_14_Tftabela_modulodes_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWTabelaDS_15_Tftabela_painom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWTabelaDS_16_Tftabela_painom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWTabelaDS_17_Tftabela_melhoracod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWTabelaDS_18_Tftabela_melhoracod_to",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00GW5 ;
          prmP00GW5 = new Object[] {
          new Object[] {"@AV51WWTabelaDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV54WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56WWTabelaDS_6_Tabela_modulocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV57WWTabelaDS_7_Tftabela_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58WWTabelaDS_8_Tftabela_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWTabelaDS_9_Tftabela_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV60WWTabelaDS_10_Tftabela_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV61WWTabelaDS_11_Tftabela_sistemades",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV62WWTabelaDS_12_Tftabela_sistemades_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV63WWTabelaDS_13_Tftabela_modulodes",SqlDbType.Char,50,0} ,
          new Object[] {"@AV64WWTabelaDS_14_Tftabela_modulodes_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWTabelaDS_15_Tftabela_painom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWTabelaDS_16_Tftabela_painom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWTabelaDS_17_Tftabela_melhoracod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWTabelaDS_18_Tftabela_melhoracod_to",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00GW6 ;
          prmP00GW6 = new Object[] {
          new Object[] {"@AV51WWTabelaDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV54WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56WWTabelaDS_6_Tabela_modulocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV57WWTabelaDS_7_Tftabela_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58WWTabelaDS_8_Tftabela_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWTabelaDS_9_Tftabela_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV60WWTabelaDS_10_Tftabela_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV61WWTabelaDS_11_Tftabela_sistemades",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV62WWTabelaDS_12_Tftabela_sistemades_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV63WWTabelaDS_13_Tftabela_modulodes",SqlDbType.Char,50,0} ,
          new Object[] {"@AV64WWTabelaDS_14_Tftabela_modulodes_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWTabelaDS_15_Tftabela_painom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWTabelaDS_16_Tftabela_painom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWTabelaDS_17_Tftabela_melhoracod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWTabelaDS_18_Tftabela_melhoracod_to",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00GW2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00GW2,100,0,true,false )
             ,new CursorDef("P00GW3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00GW3,100,0,true,false )
             ,new CursorDef("P00GW4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00GW4,100,0,true,false )
             ,new CursorDef("P00GW5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00GW5,100,0,true,false )
             ,new CursorDef("P00GW6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00GW6,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwtabelafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwtabelafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwtabelafilterdata") )
          {
             return  ;
          }
          getwwtabelafilterdata worker = new getwwtabelafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
