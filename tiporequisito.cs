/*
               File: TipoRequisito
        Description: Tipos de Requisitos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:56:28.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tiporequisito : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"TIPOREQUISITO_LINNEGCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLATIPOREQUISITO_LINNEGCOD54226( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A2039TipoRequisito_LinNegCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2039TipoRequisito_LinNegCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2039TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A2039TipoRequisito_LinNegCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7TipoRequisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7TipoRequisito_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTIPOREQUISITO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7TipoRequisito_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbTipoRequisito_Classificacao.Name = "TIPOREQUISITO_CLASSIFICACAO";
         cmbTipoRequisito_Classificacao.WebTags = "";
         cmbTipoRequisito_Classificacao.addItem("", "(Nenhum)", 0);
         cmbTipoRequisito_Classificacao.addItem("M", "Must", 0);
         cmbTipoRequisito_Classificacao.addItem("S", "Should", 0);
         cmbTipoRequisito_Classificacao.addItem("C", "Could", 0);
         cmbTipoRequisito_Classificacao.addItem("W", "Would", 0);
         if ( cmbTipoRequisito_Classificacao.ItemCount > 0 )
         {
            A2044TipoRequisito_Classificacao = cmbTipoRequisito_Classificacao.getValidValue(A2044TipoRequisito_Classificacao);
            n2044TipoRequisito_Classificacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2044TipoRequisito_Classificacao", A2044TipoRequisito_Classificacao);
         }
         dynTipoRequisito_LinNegCod.Name = "TIPOREQUISITO_LINNEGCOD";
         dynTipoRequisito_LinNegCod.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Tipos de Requisitos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtTipoRequisito_Identificador_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public tiporequisito( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public tiporequisito( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_TipoRequisito_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7TipoRequisito_Codigo = aP1_TipoRequisito_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbTipoRequisito_Classificacao = new GXCombobox();
         dynTipoRequisito_LinNegCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbTipoRequisito_Classificacao.ItemCount > 0 )
         {
            A2044TipoRequisito_Classificacao = cmbTipoRequisito_Classificacao.getValidValue(A2044TipoRequisito_Classificacao);
            n2044TipoRequisito_Classificacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2044TipoRequisito_Classificacao", A2044TipoRequisito_Classificacao);
         }
         if ( dynTipoRequisito_LinNegCod.ItemCount > 0 )
         {
            A2039TipoRequisito_LinNegCod = (int)(NumberUtil.Val( dynTipoRequisito_LinNegCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0))), "."));
            n2039TipoRequisito_LinNegCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2039TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_54226( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_54226e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTipoRequisito_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2041TipoRequisito_Codigo), 6, 0, ",", "")), ((edtTipoRequisito_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2041TipoRequisito_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A2041TipoRequisito_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoRequisito_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtTipoRequisito_Codigo_Visible, edtTipoRequisito_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_TipoRequisito.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_54226( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_54226( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_54226e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_54226( true) ;
         }
         return  ;
      }

      protected void wb_table3_41_54226e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_54226e( true) ;
         }
         else
         {
            wb_table1_2_54226e( false) ;
         }
      }

      protected void wb_table3_41_54226( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_54226e( true) ;
         }
         else
         {
            wb_table3_41_54226e( false) ;
         }
      }

      protected void wb_table2_5_54226( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_54226( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_54226e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_54226e( true) ;
         }
         else
         {
            wb_table2_5_54226e( false) ;
         }
      }

      protected void wb_table4_13_54226( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktiporequisito_identificador_Internalname, "Identificador", "", "", lblTextblocktiporequisito_identificador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTipoRequisito_Identificador_Internalname, A2042TipoRequisito_Identificador, StringUtil.RTrim( context.localUtil.Format( A2042TipoRequisito_Identificador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoRequisito_Identificador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtTipoRequisito_Identificador_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktiporequisito_descricao_Internalname, "Descri��o", "", "", lblTextblocktiporequisito_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtTipoRequisito_Descricao_Internalname, A2043TipoRequisito_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", 0, 1, edtTipoRequisito_Descricao_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_TipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktiporequisito_classificacao_Internalname, "Classifica��o", "", "", lblTextblocktiporequisito_classificacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbTipoRequisito_Classificacao, cmbTipoRequisito_Classificacao_Internalname, StringUtil.RTrim( A2044TipoRequisito_Classificacao), 1, cmbTipoRequisito_Classificacao_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbTipoRequisito_Classificacao.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_TipoRequisito.htm");
            cmbTipoRequisito_Classificacao.CurrentValue = StringUtil.RTrim( A2044TipoRequisito_Classificacao);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTipoRequisito_Classificacao_Internalname, "Values", (String)(cmbTipoRequisito_Classificacao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktiporequisito_linnegcod_Internalname, "Linha de Neg�cio", "", "", lblTextblocktiporequisito_linnegcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynTipoRequisito_LinNegCod, dynTipoRequisito_LinNegCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0)), 1, dynTipoRequisito_LinNegCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynTipoRequisito_LinNegCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_TipoRequisito.htm");
            dynTipoRequisito_LinNegCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoRequisito_LinNegCod_Internalname, "Values", (String)(dynTipoRequisito_LinNegCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktiporequisito_linnegdsc_Internalname, "Descri��o", "", "", lblTextblocktiporequisito_linnegdsc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtTipoRequisito_LinNegDsc_Internalname, A2040TipoRequisito_LinNegDsc, "", "", 0, 1, edtTipoRequisito_LinNegDsc_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_TipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_54226e( true) ;
         }
         else
         {
            wb_table4_13_54226e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11542 */
         E11542 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A2042TipoRequisito_Identificador = cgiGet( edtTipoRequisito_Identificador_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2042TipoRequisito_Identificador", A2042TipoRequisito_Identificador);
               A2043TipoRequisito_Descricao = cgiGet( edtTipoRequisito_Descricao_Internalname);
               n2043TipoRequisito_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2043TipoRequisito_Descricao", A2043TipoRequisito_Descricao);
               n2043TipoRequisito_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A2043TipoRequisito_Descricao)) ? true : false);
               cmbTipoRequisito_Classificacao.CurrentValue = cgiGet( cmbTipoRequisito_Classificacao_Internalname);
               A2044TipoRequisito_Classificacao = cgiGet( cmbTipoRequisito_Classificacao_Internalname);
               n2044TipoRequisito_Classificacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2044TipoRequisito_Classificacao", A2044TipoRequisito_Classificacao);
               n2044TipoRequisito_Classificacao = (String.IsNullOrEmpty(StringUtil.RTrim( A2044TipoRequisito_Classificacao)) ? true : false);
               dynTipoRequisito_LinNegCod.CurrentValue = cgiGet( dynTipoRequisito_LinNegCod_Internalname);
               A2039TipoRequisito_LinNegCod = (int)(NumberUtil.Val( cgiGet( dynTipoRequisito_LinNegCod_Internalname), "."));
               n2039TipoRequisito_LinNegCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2039TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0)));
               n2039TipoRequisito_LinNegCod = ((0==A2039TipoRequisito_LinNegCod) ? true : false);
               A2040TipoRequisito_LinNegDsc = cgiGet( edtTipoRequisito_LinNegDsc_Internalname);
               n2040TipoRequisito_LinNegDsc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2040TipoRequisito_LinNegDsc", A2040TipoRequisito_LinNegDsc);
               A2041TipoRequisito_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTipoRequisito_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
               /* Read saved values. */
               Z2041TipoRequisito_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z2041TipoRequisito_Codigo"), ",", "."));
               Z2042TipoRequisito_Identificador = cgiGet( "Z2042TipoRequisito_Identificador");
               Z2043TipoRequisito_Descricao = cgiGet( "Z2043TipoRequisito_Descricao");
               n2043TipoRequisito_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A2043TipoRequisito_Descricao)) ? true : false);
               Z2044TipoRequisito_Classificacao = cgiGet( "Z2044TipoRequisito_Classificacao");
               n2044TipoRequisito_Classificacao = (String.IsNullOrEmpty(StringUtil.RTrim( A2044TipoRequisito_Classificacao)) ? true : false);
               Z2039TipoRequisito_LinNegCod = (int)(context.localUtil.CToN( cgiGet( "Z2039TipoRequisito_LinNegCod"), ",", "."));
               n2039TipoRequisito_LinNegCod = ((0==A2039TipoRequisito_LinNegCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N2039TipoRequisito_LinNegCod = (int)(context.localUtil.CToN( cgiGet( "N2039TipoRequisito_LinNegCod"), ",", "."));
               n2039TipoRequisito_LinNegCod = ((0==A2039TipoRequisito_LinNegCod) ? true : false);
               AV7TipoRequisito_Codigo = (int)(context.localUtil.CToN( cgiGet( "vTIPOREQUISITO_CODIGO"), ",", "."));
               AV11Insert_TipoRequisito_LinNegCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_TIPOREQUISITO_LINNEGCOD"), ",", "."));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "TipoRequisito";
               A2041TipoRequisito_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTipoRequisito_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2041TipoRequisito_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A2041TipoRequisito_Codigo != Z2041TipoRequisito_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("tiporequisito:[SecurityCheckFailed value for]"+"TipoRequisito_Codigo:"+context.localUtil.Format( (decimal)(A2041TipoRequisito_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("tiporequisito:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A2041TipoRequisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode226 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode226;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound226 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_540( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "TIPOREQUISITO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtTipoRequisito_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11542 */
                           E11542 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12542 */
                           E12542 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12542 */
            E12542 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll54226( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes54226( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_540( )
      {
         BeforeValidate54226( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls54226( ) ;
            }
            else
            {
               CheckExtendedTable54226( ) ;
               CloseExtendedTableCursors54226( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption540( )
      {
      }

      protected void E11542( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "TipoRequisito_LinNegCod") == 0 )
               {
                  AV11Insert_TipoRequisito_LinNegCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_TipoRequisito_LinNegCod), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
         edtTipoRequisito_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoRequisito_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoRequisito_Codigo_Visible), 5, 0)));
      }

      protected void E12542( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwtiporequisito.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM54226( short GX_JID )
      {
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2042TipoRequisito_Identificador = T00543_A2042TipoRequisito_Identificador[0];
               Z2043TipoRequisito_Descricao = T00543_A2043TipoRequisito_Descricao[0];
               Z2044TipoRequisito_Classificacao = T00543_A2044TipoRequisito_Classificacao[0];
               Z2039TipoRequisito_LinNegCod = T00543_A2039TipoRequisito_LinNegCod[0];
            }
            else
            {
               Z2042TipoRequisito_Identificador = A2042TipoRequisito_Identificador;
               Z2043TipoRequisito_Descricao = A2043TipoRequisito_Descricao;
               Z2044TipoRequisito_Classificacao = A2044TipoRequisito_Classificacao;
               Z2039TipoRequisito_LinNegCod = A2039TipoRequisito_LinNegCod;
            }
         }
         if ( GX_JID == -10 )
         {
            Z2041TipoRequisito_Codigo = A2041TipoRequisito_Codigo;
            Z2042TipoRequisito_Identificador = A2042TipoRequisito_Identificador;
            Z2043TipoRequisito_Descricao = A2043TipoRequisito_Descricao;
            Z2044TipoRequisito_Classificacao = A2044TipoRequisito_Classificacao;
            Z2039TipoRequisito_LinNegCod = A2039TipoRequisito_LinNegCod;
            Z2040TipoRequisito_LinNegDsc = A2040TipoRequisito_LinNegDsc;
         }
      }

      protected void standaloneNotModal( )
      {
         GXATIPOREQUISITO_LINNEGCOD_html54226( ) ;
         edtTipoRequisito_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoRequisito_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoRequisito_Codigo_Enabled), 5, 0)));
         AV13Pgmname = "TipoRequisito";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         edtTipoRequisito_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoRequisito_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoRequisito_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7TipoRequisito_Codigo) )
         {
            A2041TipoRequisito_Codigo = AV7TipoRequisito_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_TipoRequisito_LinNegCod) )
         {
            dynTipoRequisito_LinNegCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoRequisito_LinNegCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTipoRequisito_LinNegCod.Enabled), 5, 0)));
         }
         else
         {
            dynTipoRequisito_LinNegCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoRequisito_LinNegCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTipoRequisito_LinNegCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_TipoRequisito_LinNegCod) )
         {
            A2039TipoRequisito_LinNegCod = AV11Insert_TipoRequisito_LinNegCod;
            n2039TipoRequisito_LinNegCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2039TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00544 */
            pr_default.execute(2, new Object[] {n2039TipoRequisito_LinNegCod, A2039TipoRequisito_LinNegCod});
            A2040TipoRequisito_LinNegDsc = T00544_A2040TipoRequisito_LinNegDsc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2040TipoRequisito_LinNegDsc", A2040TipoRequisito_LinNegDsc);
            n2040TipoRequisito_LinNegDsc = T00544_n2040TipoRequisito_LinNegDsc[0];
            pr_default.close(2);
         }
      }

      protected void Load54226( )
      {
         /* Using cursor T00545 */
         pr_default.execute(3, new Object[] {A2041TipoRequisito_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound226 = 1;
            A2042TipoRequisito_Identificador = T00545_A2042TipoRequisito_Identificador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2042TipoRequisito_Identificador", A2042TipoRequisito_Identificador);
            A2043TipoRequisito_Descricao = T00545_A2043TipoRequisito_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2043TipoRequisito_Descricao", A2043TipoRequisito_Descricao);
            n2043TipoRequisito_Descricao = T00545_n2043TipoRequisito_Descricao[0];
            A2044TipoRequisito_Classificacao = T00545_A2044TipoRequisito_Classificacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2044TipoRequisito_Classificacao", A2044TipoRequisito_Classificacao);
            n2044TipoRequisito_Classificacao = T00545_n2044TipoRequisito_Classificacao[0];
            A2040TipoRequisito_LinNegDsc = T00545_A2040TipoRequisito_LinNegDsc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2040TipoRequisito_LinNegDsc", A2040TipoRequisito_LinNegDsc);
            n2040TipoRequisito_LinNegDsc = T00545_n2040TipoRequisito_LinNegDsc[0];
            A2039TipoRequisito_LinNegCod = T00545_A2039TipoRequisito_LinNegCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2039TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0)));
            n2039TipoRequisito_LinNegCod = T00545_n2039TipoRequisito_LinNegCod[0];
            ZM54226( -10) ;
         }
         pr_default.close(3);
         OnLoadActions54226( ) ;
      }

      protected void OnLoadActions54226( )
      {
      }

      protected void CheckExtendedTable54226( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( ( StringUtil.StrCmp(A2044TipoRequisito_Classificacao, "M") == 0 ) || ( StringUtil.StrCmp(A2044TipoRequisito_Classificacao, "S") == 0 ) || ( StringUtil.StrCmp(A2044TipoRequisito_Classificacao, "C") == 0 ) || ( StringUtil.StrCmp(A2044TipoRequisito_Classificacao, "W") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A2044TipoRequisito_Classificacao)) ) )
         {
            GX_msglist.addItem("Campo Classifica��o fora do intervalo", "OutOfRange", 1, "TIPOREQUISITO_CLASSIFICACAO");
            AnyError = 1;
            GX_FocusControl = cmbTipoRequisito_Classificacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00544 */
         pr_default.execute(2, new Object[] {n2039TipoRequisito_LinNegCod, A2039TipoRequisito_LinNegCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A2039TipoRequisito_LinNegCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo Requisito_Linha Negocio'.", "ForeignKeyNotFound", 1, "TIPOREQUISITO_LINNEGCOD");
               AnyError = 1;
               GX_FocusControl = dynTipoRequisito_LinNegCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A2040TipoRequisito_LinNegDsc = T00544_A2040TipoRequisito_LinNegDsc[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2040TipoRequisito_LinNegDsc", A2040TipoRequisito_LinNegDsc);
         n2040TipoRequisito_LinNegDsc = T00544_n2040TipoRequisito_LinNegDsc[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors54226( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_11( int A2039TipoRequisito_LinNegCod )
      {
         /* Using cursor T00546 */
         pr_default.execute(4, new Object[] {n2039TipoRequisito_LinNegCod, A2039TipoRequisito_LinNegCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A2039TipoRequisito_LinNegCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo Requisito_Linha Negocio'.", "ForeignKeyNotFound", 1, "TIPOREQUISITO_LINNEGCOD");
               AnyError = 1;
               GX_FocusControl = dynTipoRequisito_LinNegCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A2040TipoRequisito_LinNegDsc = T00546_A2040TipoRequisito_LinNegDsc[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2040TipoRequisito_LinNegDsc", A2040TipoRequisito_LinNegDsc);
         n2040TipoRequisito_LinNegDsc = T00546_n2040TipoRequisito_LinNegDsc[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A2040TipoRequisito_LinNegDsc)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey54226( )
      {
         /* Using cursor T00547 */
         pr_default.execute(5, new Object[] {A2041TipoRequisito_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound226 = 1;
         }
         else
         {
            RcdFound226 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00543 */
         pr_default.execute(1, new Object[] {A2041TipoRequisito_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM54226( 10) ;
            RcdFound226 = 1;
            A2041TipoRequisito_Codigo = T00543_A2041TipoRequisito_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
            A2042TipoRequisito_Identificador = T00543_A2042TipoRequisito_Identificador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2042TipoRequisito_Identificador", A2042TipoRequisito_Identificador);
            A2043TipoRequisito_Descricao = T00543_A2043TipoRequisito_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2043TipoRequisito_Descricao", A2043TipoRequisito_Descricao);
            n2043TipoRequisito_Descricao = T00543_n2043TipoRequisito_Descricao[0];
            A2044TipoRequisito_Classificacao = T00543_A2044TipoRequisito_Classificacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2044TipoRequisito_Classificacao", A2044TipoRequisito_Classificacao);
            n2044TipoRequisito_Classificacao = T00543_n2044TipoRequisito_Classificacao[0];
            A2039TipoRequisito_LinNegCod = T00543_A2039TipoRequisito_LinNegCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2039TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0)));
            n2039TipoRequisito_LinNegCod = T00543_n2039TipoRequisito_LinNegCod[0];
            Z2041TipoRequisito_Codigo = A2041TipoRequisito_Codigo;
            sMode226 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load54226( ) ;
            if ( AnyError == 1 )
            {
               RcdFound226 = 0;
               InitializeNonKey54226( ) ;
            }
            Gx_mode = sMode226;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound226 = 0;
            InitializeNonKey54226( ) ;
            sMode226 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode226;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey54226( ) ;
         if ( RcdFound226 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound226 = 0;
         /* Using cursor T00548 */
         pr_default.execute(6, new Object[] {A2041TipoRequisito_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T00548_A2041TipoRequisito_Codigo[0] < A2041TipoRequisito_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T00548_A2041TipoRequisito_Codigo[0] > A2041TipoRequisito_Codigo ) ) )
            {
               A2041TipoRequisito_Codigo = T00548_A2041TipoRequisito_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
               RcdFound226 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound226 = 0;
         /* Using cursor T00549 */
         pr_default.execute(7, new Object[] {A2041TipoRequisito_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T00549_A2041TipoRequisito_Codigo[0] > A2041TipoRequisito_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T00549_A2041TipoRequisito_Codigo[0] < A2041TipoRequisito_Codigo ) ) )
            {
               A2041TipoRequisito_Codigo = T00549_A2041TipoRequisito_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
               RcdFound226 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey54226( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtTipoRequisito_Identificador_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert54226( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound226 == 1 )
            {
               if ( A2041TipoRequisito_Codigo != Z2041TipoRequisito_Codigo )
               {
                  A2041TipoRequisito_Codigo = Z2041TipoRequisito_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "TIPOREQUISITO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtTipoRequisito_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtTipoRequisito_Identificador_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update54226( ) ;
                  GX_FocusControl = edtTipoRequisito_Identificador_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A2041TipoRequisito_Codigo != Z2041TipoRequisito_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtTipoRequisito_Identificador_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert54226( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "TIPOREQUISITO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtTipoRequisito_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtTipoRequisito_Identificador_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert54226( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A2041TipoRequisito_Codigo != Z2041TipoRequisito_Codigo )
         {
            A2041TipoRequisito_Codigo = Z2041TipoRequisito_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "TIPOREQUISITO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtTipoRequisito_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtTipoRequisito_Identificador_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency54226( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00542 */
            pr_default.execute(0, new Object[] {A2041TipoRequisito_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"TipoRequisito"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z2042TipoRequisito_Identificador, T00542_A2042TipoRequisito_Identificador[0]) != 0 ) || ( StringUtil.StrCmp(Z2043TipoRequisito_Descricao, T00542_A2043TipoRequisito_Descricao[0]) != 0 ) || ( StringUtil.StrCmp(Z2044TipoRequisito_Classificacao, T00542_A2044TipoRequisito_Classificacao[0]) != 0 ) || ( Z2039TipoRequisito_LinNegCod != T00542_A2039TipoRequisito_LinNegCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z2042TipoRequisito_Identificador, T00542_A2042TipoRequisito_Identificador[0]) != 0 )
               {
                  GXUtil.WriteLog("tiporequisito:[seudo value changed for attri]"+"TipoRequisito_Identificador");
                  GXUtil.WriteLogRaw("Old: ",Z2042TipoRequisito_Identificador);
                  GXUtil.WriteLogRaw("Current: ",T00542_A2042TipoRequisito_Identificador[0]);
               }
               if ( StringUtil.StrCmp(Z2043TipoRequisito_Descricao, T00542_A2043TipoRequisito_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("tiporequisito:[seudo value changed for attri]"+"TipoRequisito_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z2043TipoRequisito_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T00542_A2043TipoRequisito_Descricao[0]);
               }
               if ( StringUtil.StrCmp(Z2044TipoRequisito_Classificacao, T00542_A2044TipoRequisito_Classificacao[0]) != 0 )
               {
                  GXUtil.WriteLog("tiporequisito:[seudo value changed for attri]"+"TipoRequisito_Classificacao");
                  GXUtil.WriteLogRaw("Old: ",Z2044TipoRequisito_Classificacao);
                  GXUtil.WriteLogRaw("Current: ",T00542_A2044TipoRequisito_Classificacao[0]);
               }
               if ( Z2039TipoRequisito_LinNegCod != T00542_A2039TipoRequisito_LinNegCod[0] )
               {
                  GXUtil.WriteLog("tiporequisito:[seudo value changed for attri]"+"TipoRequisito_LinNegCod");
                  GXUtil.WriteLogRaw("Old: ",Z2039TipoRequisito_LinNegCod);
                  GXUtil.WriteLogRaw("Current: ",T00542_A2039TipoRequisito_LinNegCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"TipoRequisito"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert54226( )
      {
         BeforeValidate54226( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable54226( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM54226( 0) ;
            CheckOptimisticConcurrency54226( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm54226( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert54226( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005410 */
                     pr_default.execute(8, new Object[] {A2042TipoRequisito_Identificador, n2043TipoRequisito_Descricao, A2043TipoRequisito_Descricao, n2044TipoRequisito_Classificacao, A2044TipoRequisito_Classificacao, n2039TipoRequisito_LinNegCod, A2039TipoRequisito_LinNegCod});
                     A2041TipoRequisito_Codigo = T005410_A2041TipoRequisito_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("TipoRequisito") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption540( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load54226( ) ;
            }
            EndLevel54226( ) ;
         }
         CloseExtendedTableCursors54226( ) ;
      }

      protected void Update54226( )
      {
         BeforeValidate54226( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable54226( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency54226( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm54226( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate54226( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005411 */
                     pr_default.execute(9, new Object[] {A2042TipoRequisito_Identificador, n2043TipoRequisito_Descricao, A2043TipoRequisito_Descricao, n2044TipoRequisito_Classificacao, A2044TipoRequisito_Classificacao, n2039TipoRequisito_LinNegCod, A2039TipoRequisito_LinNegCod, A2041TipoRequisito_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("TipoRequisito") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"TipoRequisito"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate54226( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel54226( ) ;
         }
         CloseExtendedTableCursors54226( ) ;
      }

      protected void DeferredUpdate54226( )
      {
      }

      protected void delete( )
      {
         BeforeValidate54226( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency54226( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls54226( ) ;
            AfterConfirm54226( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete54226( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T005412 */
                  pr_default.execute(10, new Object[] {A2041TipoRequisito_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("TipoRequisito") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode226 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel54226( ) ;
         Gx_mode = sMode226;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls54226( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T005413 */
            pr_default.execute(11, new Object[] {n2039TipoRequisito_LinNegCod, A2039TipoRequisito_LinNegCod});
            A2040TipoRequisito_LinNegDsc = T005413_A2040TipoRequisito_LinNegDsc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2040TipoRequisito_LinNegDsc", A2040TipoRequisito_LinNegDsc);
            n2040TipoRequisito_LinNegDsc = T005413_n2040TipoRequisito_LinNegDsc[0];
            pr_default.close(11);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T005414 */
            pr_default.execute(12, new Object[] {A2041TipoRequisito_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T213"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
         }
      }

      protected void EndLevel54226( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete54226( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "TipoRequisito");
            if ( AnyError == 0 )
            {
               ConfirmValues540( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "TipoRequisito");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart54226( )
      {
         /* Scan By routine */
         /* Using cursor T005415 */
         pr_default.execute(13);
         RcdFound226 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound226 = 1;
            A2041TipoRequisito_Codigo = T005415_A2041TipoRequisito_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext54226( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound226 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound226 = 1;
            A2041TipoRequisito_Codigo = T005415_A2041TipoRequisito_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd54226( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm54226( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert54226( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate54226( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete54226( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete54226( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate54226( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes54226( )
      {
         edtTipoRequisito_Identificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoRequisito_Identificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoRequisito_Identificador_Enabled), 5, 0)));
         edtTipoRequisito_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoRequisito_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoRequisito_Descricao_Enabled), 5, 0)));
         cmbTipoRequisito_Classificacao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTipoRequisito_Classificacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbTipoRequisito_Classificacao.Enabled), 5, 0)));
         dynTipoRequisito_LinNegCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoRequisito_LinNegCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTipoRequisito_LinNegCod.Enabled), 5, 0)));
         edtTipoRequisito_LinNegDsc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoRequisito_LinNegDsc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoRequisito_LinNegDsc_Enabled), 5, 0)));
         edtTipoRequisito_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoRequisito_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoRequisito_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues540( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812563064");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tiporequisito.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7TipoRequisito_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2041TipoRequisito_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2042TipoRequisito_Identificador", Z2042TipoRequisito_Identificador);
         GxWebStd.gx_hidden_field( context, "Z2043TipoRequisito_Descricao", Z2043TipoRequisito_Descricao);
         GxWebStd.gx_hidden_field( context, "Z2044TipoRequisito_Classificacao", StringUtil.RTrim( Z2044TipoRequisito_Classificacao));
         GxWebStd.gx_hidden_field( context, "Z2039TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2039TipoRequisito_LinNegCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N2039TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vTIPOREQUISITO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7TipoRequisito_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_TIPOREQUISITO_LINNEGCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_TipoRequisito_LinNegCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vTIPOREQUISITO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7TipoRequisito_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "TipoRequisito";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2041TipoRequisito_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("tiporequisito:[SendSecurityCheck value for]"+"TipoRequisito_Codigo:"+context.localUtil.Format( (decimal)(A2041TipoRequisito_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("tiporequisito:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("tiporequisito.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7TipoRequisito_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "TipoRequisito" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tipos de Requisitos" ;
      }

      protected void InitializeNonKey54226( )
      {
         A2039TipoRequisito_LinNegCod = 0;
         n2039TipoRequisito_LinNegCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2039TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0)));
         n2039TipoRequisito_LinNegCod = ((0==A2039TipoRequisito_LinNegCod) ? true : false);
         A2042TipoRequisito_Identificador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2042TipoRequisito_Identificador", A2042TipoRequisito_Identificador);
         A2043TipoRequisito_Descricao = "";
         n2043TipoRequisito_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2043TipoRequisito_Descricao", A2043TipoRequisito_Descricao);
         n2043TipoRequisito_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A2043TipoRequisito_Descricao)) ? true : false);
         A2044TipoRequisito_Classificacao = "";
         n2044TipoRequisito_Classificacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2044TipoRequisito_Classificacao", A2044TipoRequisito_Classificacao);
         n2044TipoRequisito_Classificacao = (String.IsNullOrEmpty(StringUtil.RTrim( A2044TipoRequisito_Classificacao)) ? true : false);
         A2040TipoRequisito_LinNegDsc = "";
         n2040TipoRequisito_LinNegDsc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2040TipoRequisito_LinNegDsc", A2040TipoRequisito_LinNegDsc);
         Z2042TipoRequisito_Identificador = "";
         Z2043TipoRequisito_Descricao = "";
         Z2044TipoRequisito_Classificacao = "";
         Z2039TipoRequisito_LinNegCod = 0;
      }

      protected void InitAll54226( )
      {
         A2041TipoRequisito_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
         InitializeNonKey54226( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181256315");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("tiporequisito.js", "?20205181256315");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktiporequisito_identificador_Internalname = "TEXTBLOCKTIPOREQUISITO_IDENTIFICADOR";
         edtTipoRequisito_Identificador_Internalname = "TIPOREQUISITO_IDENTIFICADOR";
         lblTextblocktiporequisito_descricao_Internalname = "TEXTBLOCKTIPOREQUISITO_DESCRICAO";
         edtTipoRequisito_Descricao_Internalname = "TIPOREQUISITO_DESCRICAO";
         lblTextblocktiporequisito_classificacao_Internalname = "TEXTBLOCKTIPOREQUISITO_CLASSIFICACAO";
         cmbTipoRequisito_Classificacao_Internalname = "TIPOREQUISITO_CLASSIFICACAO";
         lblTextblocktiporequisito_linnegcod_Internalname = "TEXTBLOCKTIPOREQUISITO_LINNEGCOD";
         dynTipoRequisito_LinNegCod_Internalname = "TIPOREQUISITO_LINNEGCOD";
         lblTextblocktiporequisito_linnegdsc_Internalname = "TEXTBLOCKTIPOREQUISITO_LINNEGDSC";
         edtTipoRequisito_LinNegDsc_Internalname = "TIPOREQUISITO_LINNEGDSC";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtTipoRequisito_Codigo_Internalname = "TIPOREQUISITO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Tipo de Requisito";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Tipos de Requisitos";
         edtTipoRequisito_LinNegDsc_Enabled = 0;
         dynTipoRequisito_LinNegCod_Jsonclick = "";
         dynTipoRequisito_LinNegCod.Enabled = 1;
         cmbTipoRequisito_Classificacao_Jsonclick = "";
         cmbTipoRequisito_Classificacao.Enabled = 1;
         edtTipoRequisito_Descricao_Enabled = 1;
         edtTipoRequisito_Identificador_Jsonclick = "";
         edtTipoRequisito_Identificador_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtTipoRequisito_Codigo_Jsonclick = "";
         edtTipoRequisito_Codigo_Enabled = 0;
         edtTipoRequisito_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLATIPOREQUISITO_LINNEGCOD54226( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLATIPOREQUISITO_LINNEGCOD_data54226( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXATIPOREQUISITO_LINNEGCOD_html54226( )
      {
         int gxdynajaxvalue ;
         GXDLATIPOREQUISITO_LINNEGCOD_data54226( ) ;
         gxdynajaxindex = 1;
         dynTipoRequisito_LinNegCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynTipoRequisito_LinNegCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLATIPOREQUISITO_LINNEGCOD_data54226( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T005416 */
         pr_default.execute(14);
         while ( (pr_default.getStatus(14) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T005416_A2039TipoRequisito_LinNegCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T005416_A2037LinhaNegocio_Identificador[0]);
            pr_default.readNext(14);
         }
         pr_default.close(14);
      }

      public void Valid_Tiporequisito_linnegcod( GXCombobox dynGX_Parm1 ,
                                                 String GX_Parm2 )
      {
         dynTipoRequisito_LinNegCod = dynGX_Parm1;
         A2039TipoRequisito_LinNegCod = (int)(NumberUtil.Val( dynTipoRequisito_LinNegCod.CurrentValue, "."));
         n2039TipoRequisito_LinNegCod = false;
         A2040TipoRequisito_LinNegDsc = GX_Parm2;
         n2040TipoRequisito_LinNegDsc = false;
         /* Using cursor T005417 */
         pr_default.execute(15, new Object[] {n2039TipoRequisito_LinNegCod, A2039TipoRequisito_LinNegCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (0==A2039TipoRequisito_LinNegCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo Requisito_Linha Negocio'.", "ForeignKeyNotFound", 1, "TIPOREQUISITO_LINNEGCOD");
               AnyError = 1;
               GX_FocusControl = dynTipoRequisito_LinNegCod_Internalname;
            }
         }
         A2040TipoRequisito_LinNegDsc = T005417_A2040TipoRequisito_LinNegDsc[0];
         n2040TipoRequisito_LinNegDsc = T005417_n2040TipoRequisito_LinNegDsc[0];
         pr_default.close(15);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A2040TipoRequisito_LinNegDsc = "";
            n2040TipoRequisito_LinNegDsc = false;
         }
         isValidOutput.Add(A2040TipoRequisito_LinNegDsc);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7TipoRequisito_Codigo',fld:'vTIPOREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12542',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z2042TipoRequisito_Identificador = "";
         Z2043TipoRequisito_Descricao = "";
         Z2044TipoRequisito_Classificacao = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A2044TipoRequisito_Classificacao = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocktiporequisito_identificador_Jsonclick = "";
         A2042TipoRequisito_Identificador = "";
         lblTextblocktiporequisito_descricao_Jsonclick = "";
         A2043TipoRequisito_Descricao = "";
         lblTextblocktiporequisito_classificacao_Jsonclick = "";
         lblTextblocktiporequisito_linnegcod_Jsonclick = "";
         lblTextblocktiporequisito_linnegdsc_Jsonclick = "";
         A2040TipoRequisito_LinNegDsc = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode226 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z2040TipoRequisito_LinNegDsc = "";
         T00544_A2040TipoRequisito_LinNegDsc = new String[] {""} ;
         T00544_n2040TipoRequisito_LinNegDsc = new bool[] {false} ;
         T00545_A2041TipoRequisito_Codigo = new int[1] ;
         T00545_A2042TipoRequisito_Identificador = new String[] {""} ;
         T00545_A2043TipoRequisito_Descricao = new String[] {""} ;
         T00545_n2043TipoRequisito_Descricao = new bool[] {false} ;
         T00545_A2044TipoRequisito_Classificacao = new String[] {""} ;
         T00545_n2044TipoRequisito_Classificacao = new bool[] {false} ;
         T00545_A2040TipoRequisito_LinNegDsc = new String[] {""} ;
         T00545_n2040TipoRequisito_LinNegDsc = new bool[] {false} ;
         T00545_A2039TipoRequisito_LinNegCod = new int[1] ;
         T00545_n2039TipoRequisito_LinNegCod = new bool[] {false} ;
         T00546_A2040TipoRequisito_LinNegDsc = new String[] {""} ;
         T00546_n2040TipoRequisito_LinNegDsc = new bool[] {false} ;
         T00547_A2041TipoRequisito_Codigo = new int[1] ;
         T00543_A2041TipoRequisito_Codigo = new int[1] ;
         T00543_A2042TipoRequisito_Identificador = new String[] {""} ;
         T00543_A2043TipoRequisito_Descricao = new String[] {""} ;
         T00543_n2043TipoRequisito_Descricao = new bool[] {false} ;
         T00543_A2044TipoRequisito_Classificacao = new String[] {""} ;
         T00543_n2044TipoRequisito_Classificacao = new bool[] {false} ;
         T00543_A2039TipoRequisito_LinNegCod = new int[1] ;
         T00543_n2039TipoRequisito_LinNegCod = new bool[] {false} ;
         T00548_A2041TipoRequisito_Codigo = new int[1] ;
         T00549_A2041TipoRequisito_Codigo = new int[1] ;
         T00542_A2041TipoRequisito_Codigo = new int[1] ;
         T00542_A2042TipoRequisito_Identificador = new String[] {""} ;
         T00542_A2043TipoRequisito_Descricao = new String[] {""} ;
         T00542_n2043TipoRequisito_Descricao = new bool[] {false} ;
         T00542_A2044TipoRequisito_Classificacao = new String[] {""} ;
         T00542_n2044TipoRequisito_Classificacao = new bool[] {false} ;
         T00542_A2039TipoRequisito_LinNegCod = new int[1] ;
         T00542_n2039TipoRequisito_LinNegCod = new bool[] {false} ;
         T005410_A2041TipoRequisito_Codigo = new int[1] ;
         T005413_A2040TipoRequisito_LinNegDsc = new String[] {""} ;
         T005413_n2040TipoRequisito_LinNegDsc = new bool[] {false} ;
         T005414_A1919Requisito_Codigo = new int[1] ;
         T005415_A2041TipoRequisito_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T005416_A2039TipoRequisito_LinNegCod = new int[1] ;
         T005416_n2039TipoRequisito_LinNegCod = new bool[] {false} ;
         T005416_A2037LinhaNegocio_Identificador = new String[] {""} ;
         T005416_n2037LinhaNegocio_Identificador = new bool[] {false} ;
         T005417_A2040TipoRequisito_LinNegDsc = new String[] {""} ;
         T005417_n2040TipoRequisito_LinNegDsc = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tiporequisito__default(),
            new Object[][] {
                new Object[] {
               T00542_A2041TipoRequisito_Codigo, T00542_A2042TipoRequisito_Identificador, T00542_A2043TipoRequisito_Descricao, T00542_n2043TipoRequisito_Descricao, T00542_A2044TipoRequisito_Classificacao, T00542_n2044TipoRequisito_Classificacao, T00542_A2039TipoRequisito_LinNegCod, T00542_n2039TipoRequisito_LinNegCod
               }
               , new Object[] {
               T00543_A2041TipoRequisito_Codigo, T00543_A2042TipoRequisito_Identificador, T00543_A2043TipoRequisito_Descricao, T00543_n2043TipoRequisito_Descricao, T00543_A2044TipoRequisito_Classificacao, T00543_n2044TipoRequisito_Classificacao, T00543_A2039TipoRequisito_LinNegCod, T00543_n2039TipoRequisito_LinNegCod
               }
               , new Object[] {
               T00544_A2040TipoRequisito_LinNegDsc, T00544_n2040TipoRequisito_LinNegDsc
               }
               , new Object[] {
               T00545_A2041TipoRequisito_Codigo, T00545_A2042TipoRequisito_Identificador, T00545_A2043TipoRequisito_Descricao, T00545_n2043TipoRequisito_Descricao, T00545_A2044TipoRequisito_Classificacao, T00545_n2044TipoRequisito_Classificacao, T00545_A2040TipoRequisito_LinNegDsc, T00545_n2040TipoRequisito_LinNegDsc, T00545_A2039TipoRequisito_LinNegCod, T00545_n2039TipoRequisito_LinNegCod
               }
               , new Object[] {
               T00546_A2040TipoRequisito_LinNegDsc, T00546_n2040TipoRequisito_LinNegDsc
               }
               , new Object[] {
               T00547_A2041TipoRequisito_Codigo
               }
               , new Object[] {
               T00548_A2041TipoRequisito_Codigo
               }
               , new Object[] {
               T00549_A2041TipoRequisito_Codigo
               }
               , new Object[] {
               T005410_A2041TipoRequisito_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T005413_A2040TipoRequisito_LinNegDsc, T005413_n2040TipoRequisito_LinNegDsc
               }
               , new Object[] {
               T005414_A1919Requisito_Codigo
               }
               , new Object[] {
               T005415_A2041TipoRequisito_Codigo
               }
               , new Object[] {
               T005416_A2039TipoRequisito_LinNegCod, T005416_A2037LinhaNegocio_Identificador, T005416_n2037LinhaNegocio_Identificador
               }
               , new Object[] {
               T005417_A2040TipoRequisito_LinNegDsc, T005417_n2040TipoRequisito_LinNegDsc
               }
            }
         );
         AV13Pgmname = "TipoRequisito";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound226 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7TipoRequisito_Codigo ;
      private int Z2041TipoRequisito_Codigo ;
      private int Z2039TipoRequisito_LinNegCod ;
      private int N2039TipoRequisito_LinNegCod ;
      private int A2039TipoRequisito_LinNegCod ;
      private int AV7TipoRequisito_Codigo ;
      private int trnEnded ;
      private int A2041TipoRequisito_Codigo ;
      private int edtTipoRequisito_Codigo_Enabled ;
      private int edtTipoRequisito_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtTipoRequisito_Identificador_Enabled ;
      private int edtTipoRequisito_Descricao_Enabled ;
      private int edtTipoRequisito_LinNegDsc_Enabled ;
      private int AV11Insert_TipoRequisito_LinNegCod ;
      private int AV14GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z2044TipoRequisito_Classificacao ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A2044TipoRequisito_Classificacao ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtTipoRequisito_Identificador_Internalname ;
      private String edtTipoRequisito_Codigo_Internalname ;
      private String edtTipoRequisito_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocktiporequisito_identificador_Internalname ;
      private String lblTextblocktiporequisito_identificador_Jsonclick ;
      private String edtTipoRequisito_Identificador_Jsonclick ;
      private String lblTextblocktiporequisito_descricao_Internalname ;
      private String lblTextblocktiporequisito_descricao_Jsonclick ;
      private String edtTipoRequisito_Descricao_Internalname ;
      private String lblTextblocktiporequisito_classificacao_Internalname ;
      private String lblTextblocktiporequisito_classificacao_Jsonclick ;
      private String cmbTipoRequisito_Classificacao_Internalname ;
      private String cmbTipoRequisito_Classificacao_Jsonclick ;
      private String lblTextblocktiporequisito_linnegcod_Internalname ;
      private String lblTextblocktiporequisito_linnegcod_Jsonclick ;
      private String dynTipoRequisito_LinNegCod_Internalname ;
      private String dynTipoRequisito_LinNegCod_Jsonclick ;
      private String lblTextblocktiporequisito_linnegdsc_Internalname ;
      private String lblTextblocktiporequisito_linnegdsc_Jsonclick ;
      private String edtTipoRequisito_LinNegDsc_Internalname ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode226 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool n2039TipoRequisito_LinNegCod ;
      private bool toggleJsOutput ;
      private bool n2044TipoRequisito_Classificacao ;
      private bool wbErr ;
      private bool n2043TipoRequisito_Descricao ;
      private bool n2040TipoRequisito_LinNegDsc ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String Z2042TipoRequisito_Identificador ;
      private String Z2043TipoRequisito_Descricao ;
      private String A2042TipoRequisito_Identificador ;
      private String A2043TipoRequisito_Descricao ;
      private String A2040TipoRequisito_LinNegDsc ;
      private String Z2040TipoRequisito_LinNegDsc ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbTipoRequisito_Classificacao ;
      private GXCombobox dynTipoRequisito_LinNegCod ;
      private IDataStoreProvider pr_default ;
      private String[] T00544_A2040TipoRequisito_LinNegDsc ;
      private bool[] T00544_n2040TipoRequisito_LinNegDsc ;
      private int[] T00545_A2041TipoRequisito_Codigo ;
      private String[] T00545_A2042TipoRequisito_Identificador ;
      private String[] T00545_A2043TipoRequisito_Descricao ;
      private bool[] T00545_n2043TipoRequisito_Descricao ;
      private String[] T00545_A2044TipoRequisito_Classificacao ;
      private bool[] T00545_n2044TipoRequisito_Classificacao ;
      private String[] T00545_A2040TipoRequisito_LinNegDsc ;
      private bool[] T00545_n2040TipoRequisito_LinNegDsc ;
      private int[] T00545_A2039TipoRequisito_LinNegCod ;
      private bool[] T00545_n2039TipoRequisito_LinNegCod ;
      private String[] T00546_A2040TipoRequisito_LinNegDsc ;
      private bool[] T00546_n2040TipoRequisito_LinNegDsc ;
      private int[] T00547_A2041TipoRequisito_Codigo ;
      private int[] T00543_A2041TipoRequisito_Codigo ;
      private String[] T00543_A2042TipoRequisito_Identificador ;
      private String[] T00543_A2043TipoRequisito_Descricao ;
      private bool[] T00543_n2043TipoRequisito_Descricao ;
      private String[] T00543_A2044TipoRequisito_Classificacao ;
      private bool[] T00543_n2044TipoRequisito_Classificacao ;
      private int[] T00543_A2039TipoRequisito_LinNegCod ;
      private bool[] T00543_n2039TipoRequisito_LinNegCod ;
      private int[] T00548_A2041TipoRequisito_Codigo ;
      private int[] T00549_A2041TipoRequisito_Codigo ;
      private int[] T00542_A2041TipoRequisito_Codigo ;
      private String[] T00542_A2042TipoRequisito_Identificador ;
      private String[] T00542_A2043TipoRequisito_Descricao ;
      private bool[] T00542_n2043TipoRequisito_Descricao ;
      private String[] T00542_A2044TipoRequisito_Classificacao ;
      private bool[] T00542_n2044TipoRequisito_Classificacao ;
      private int[] T00542_A2039TipoRequisito_LinNegCod ;
      private bool[] T00542_n2039TipoRequisito_LinNegCod ;
      private int[] T005410_A2041TipoRequisito_Codigo ;
      private String[] T005413_A2040TipoRequisito_LinNegDsc ;
      private bool[] T005413_n2040TipoRequisito_LinNegDsc ;
      private int[] T005414_A1919Requisito_Codigo ;
      private int[] T005415_A2041TipoRequisito_Codigo ;
      private int[] T005416_A2039TipoRequisito_LinNegCod ;
      private bool[] T005416_n2039TipoRequisito_LinNegCod ;
      private String[] T005416_A2037LinhaNegocio_Identificador ;
      private bool[] T005416_n2037LinhaNegocio_Identificador ;
      private String[] T005417_A2040TipoRequisito_LinNegDsc ;
      private bool[] T005417_n2040TipoRequisito_LinNegDsc ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class tiporequisito__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00545 ;
          prmT00545 = new Object[] {
          new Object[] {"@TipoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00544 ;
          prmT00544 = new Object[] {
          new Object[] {"@TipoRequisito_LinNegCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00546 ;
          prmT00546 = new Object[] {
          new Object[] {"@TipoRequisito_LinNegCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00547 ;
          prmT00547 = new Object[] {
          new Object[] {"@TipoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00543 ;
          prmT00543 = new Object[] {
          new Object[] {"@TipoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00548 ;
          prmT00548 = new Object[] {
          new Object[] {"@TipoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00549 ;
          prmT00549 = new Object[] {
          new Object[] {"@TipoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00542 ;
          prmT00542 = new Object[] {
          new Object[] {"@TipoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005410 ;
          prmT005410 = new Object[] {
          new Object[] {"@TipoRequisito_Identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@TipoRequisito_Descricao",SqlDbType.VarChar,250,0} ,
          new Object[] {"@TipoRequisito_Classificacao",SqlDbType.Char,1,0} ,
          new Object[] {"@TipoRequisito_LinNegCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005411 ;
          prmT005411 = new Object[] {
          new Object[] {"@TipoRequisito_Identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@TipoRequisito_Descricao",SqlDbType.VarChar,250,0} ,
          new Object[] {"@TipoRequisito_Classificacao",SqlDbType.Char,1,0} ,
          new Object[] {"@TipoRequisito_LinNegCod",SqlDbType.Int,6,0} ,
          new Object[] {"@TipoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005412 ;
          prmT005412 = new Object[] {
          new Object[] {"@TipoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005413 ;
          prmT005413 = new Object[] {
          new Object[] {"@TipoRequisito_LinNegCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005414 ;
          prmT005414 = new Object[] {
          new Object[] {"@TipoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005415 ;
          prmT005415 = new Object[] {
          } ;
          Object[] prmT005416 ;
          prmT005416 = new Object[] {
          } ;
          Object[] prmT005417 ;
          prmT005417 = new Object[] {
          new Object[] {"@TipoRequisito_LinNegCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00542", "SELECT [TipoRequisito_Codigo], [TipoRequisito_Identificador], [TipoRequisito_Descricao], [TipoRequisito_Classificacao], [TipoRequisito_LinNegCod] AS TipoRequisito_LinNegCod FROM [TipoRequisito] WITH (UPDLOCK) WHERE [TipoRequisito_Codigo] = @TipoRequisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00542,1,0,true,false )
             ,new CursorDef("T00543", "SELECT [TipoRequisito_Codigo], [TipoRequisito_Identificador], [TipoRequisito_Descricao], [TipoRequisito_Classificacao], [TipoRequisito_LinNegCod] AS TipoRequisito_LinNegCod FROM [TipoRequisito] WITH (NOLOCK) WHERE [TipoRequisito_Codigo] = @TipoRequisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00543,1,0,true,false )
             ,new CursorDef("T00544", "SELECT [LinhaNegocio_Descricao] AS TipoRequisito_LinNegDsc FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @TipoRequisito_LinNegCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00544,1,0,true,false )
             ,new CursorDef("T00545", "SELECT TM1.[TipoRequisito_Codigo], TM1.[TipoRequisito_Identificador], TM1.[TipoRequisito_Descricao], TM1.[TipoRequisito_Classificacao], T2.[LinhaNegocio_Descricao] AS TipoRequisito_LinNegDsc, TM1.[TipoRequisito_LinNegCod] AS TipoRequisito_LinNegCod FROM ([TipoRequisito] TM1 WITH (NOLOCK) LEFT JOIN [LinhaNegocio] T2 WITH (NOLOCK) ON T2.[LinhadeNegocio_Codigo] = TM1.[TipoRequisito_LinNegCod]) WHERE TM1.[TipoRequisito_Codigo] = @TipoRequisito_Codigo ORDER BY TM1.[TipoRequisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00545,100,0,true,false )
             ,new CursorDef("T00546", "SELECT [LinhaNegocio_Descricao] AS TipoRequisito_LinNegDsc FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @TipoRequisito_LinNegCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00546,1,0,true,false )
             ,new CursorDef("T00547", "SELECT [TipoRequisito_Codigo] FROM [TipoRequisito] WITH (NOLOCK) WHERE [TipoRequisito_Codigo] = @TipoRequisito_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00547,1,0,true,false )
             ,new CursorDef("T00548", "SELECT TOP 1 [TipoRequisito_Codigo] FROM [TipoRequisito] WITH (NOLOCK) WHERE ( [TipoRequisito_Codigo] > @TipoRequisito_Codigo) ORDER BY [TipoRequisito_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00548,1,0,true,true )
             ,new CursorDef("T00549", "SELECT TOP 1 [TipoRequisito_Codigo] FROM [TipoRequisito] WITH (NOLOCK) WHERE ( [TipoRequisito_Codigo] < @TipoRequisito_Codigo) ORDER BY [TipoRequisito_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00549,1,0,true,true )
             ,new CursorDef("T005410", "INSERT INTO [TipoRequisito]([TipoRequisito_Identificador], [TipoRequisito_Descricao], [TipoRequisito_Classificacao], [TipoRequisito_LinNegCod]) VALUES(@TipoRequisito_Identificador, @TipoRequisito_Descricao, @TipoRequisito_Classificacao, @TipoRequisito_LinNegCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT005410)
             ,new CursorDef("T005411", "UPDATE [TipoRequisito] SET [TipoRequisito_Identificador]=@TipoRequisito_Identificador, [TipoRequisito_Descricao]=@TipoRequisito_Descricao, [TipoRequisito_Classificacao]=@TipoRequisito_Classificacao, [TipoRequisito_LinNegCod]=@TipoRequisito_LinNegCod  WHERE [TipoRequisito_Codigo] = @TipoRequisito_Codigo", GxErrorMask.GX_NOMASK,prmT005411)
             ,new CursorDef("T005412", "DELETE FROM [TipoRequisito]  WHERE [TipoRequisito_Codigo] = @TipoRequisito_Codigo", GxErrorMask.GX_NOMASK,prmT005412)
             ,new CursorDef("T005413", "SELECT [LinhaNegocio_Descricao] AS TipoRequisito_LinNegDsc FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @TipoRequisito_LinNegCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005413,1,0,true,false )
             ,new CursorDef("T005414", "SELECT TOP 1 [Requisito_Codigo] FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_TipoReqCod] = @TipoRequisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005414,1,0,true,true )
             ,new CursorDef("T005415", "SELECT [TipoRequisito_Codigo] FROM [TipoRequisito] WITH (NOLOCK) ORDER BY [TipoRequisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005415,100,0,true,false )
             ,new CursorDef("T005416", "SELECT [LinhadeNegocio_Codigo] AS TipoRequisito_LinNegCod, [LinhaNegocio_Identificador] FROM [LinhaNegocio] WITH (NOLOCK) ORDER BY [LinhaNegocio_Identificador] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005416,0,0,true,false )
             ,new CursorDef("T005417", "SELECT [LinhaNegocio_Descricao] AS TipoRequisito_LinNegDsc FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @TipoRequisito_LinNegCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005417,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[6]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[6]);
                }
                stmt.SetParameter(5, (int)parms[7]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
