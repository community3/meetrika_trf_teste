/*
               File: WWServicoPrioridade
        Description:  Prioridades padr�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:31:55.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwservicoprioridade : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwservicoprioridade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwservicoprioridade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_73 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_73_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_73_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV17ServicoPrioridade_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoPrioridade_Nome1", AV17ServicoPrioridade_Nome1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV21ServicoPrioridade_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoPrioridade_Nome2", AV21ServicoPrioridade_Nome2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV25ServicoPrioridade_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoPrioridade_Nome3", AV25ServicoPrioridade_Nome3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV34TFServicoPrioridade_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFServicoPrioridade_Nome", AV34TFServicoPrioridade_Nome);
               AV35TFServicoPrioridade_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFServicoPrioridade_Nome_Sel", AV35TFServicoPrioridade_Nome_Sel);
               AV38TFServicoPrioridade_Finalidade = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFServicoPrioridade_Finalidade", AV38TFServicoPrioridade_Finalidade);
               AV39TFServicoPrioridade_Finalidade_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFServicoPrioridade_Finalidade_Sel", AV39TFServicoPrioridade_Finalidade_Sel);
               AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace", AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace);
               AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace", AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace);
               AV61Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A1440ServicoPrioridade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A1439ServicoPrioridade_SrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ServicoPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ServicoPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ServicoPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoPrioridade_Nome, AV35TFServicoPrioridade_Nome_Sel, AV38TFServicoPrioridade_Finalidade, AV39TFServicoPrioridade_Finalidade_Sel, AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV61Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAKQ2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTKQ2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051813315576");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwservicoprioridade.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOPRIORIDADE_NOME1", StringUtil.RTrim( AV17ServicoPrioridade_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOPRIORIDADE_NOME2", StringUtil.RTrim( AV21ServicoPrioridade_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOPRIORIDADE_NOME3", StringUtil.RTrim( AV25ServicoPrioridade_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOPRIORIDADE_NOME", StringUtil.RTrim( AV34TFServicoPrioridade_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOPRIORIDADE_NOME_SEL", StringUtil.RTrim( AV35TFServicoPrioridade_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOPRIORIDADE_FINALIDADE", AV38TFServicoPrioridade_Finalidade);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOPRIORIDADE_FINALIDADE_SEL", AV39TFServicoPrioridade_Finalidade_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_73", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_73), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV41DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV41DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOPRIORIDADE_NOMETITLEFILTERDATA", AV33ServicoPrioridade_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOPRIORIDADE_NOMETITLEFILTERDATA", AV33ServicoPrioridade_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOPRIORIDADE_FINALIDADETITLEFILTERDATA", AV37ServicoPrioridade_FinalidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOPRIORIDADE_FINALIDADETITLEFILTERDATA", AV37ServicoPrioridade_FinalidadeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV61Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Caption", StringUtil.RTrim( Ddo_servicoprioridade_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Tooltip", StringUtil.RTrim( Ddo_servicoprioridade_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Cls", StringUtil.RTrim( Ddo_servicoprioridade_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_servicoprioridade_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_servicoprioridade_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicoprioridade_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicoprioridade_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_servicoprioridade_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_servicoprioridade_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Sortedstatus", StringUtil.RTrim( Ddo_servicoprioridade_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Includefilter", StringUtil.BoolToStr( Ddo_servicoprioridade_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Filtertype", StringUtil.RTrim( Ddo_servicoprioridade_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_servicoprioridade_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_servicoprioridade_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Datalisttype", StringUtil.RTrim( Ddo_servicoprioridade_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Datalistproc", StringUtil.RTrim( Ddo_servicoprioridade_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servicoprioridade_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Sortasc", StringUtil.RTrim( Ddo_servicoprioridade_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Sortdsc", StringUtil.RTrim( Ddo_servicoprioridade_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Loadingdata", StringUtil.RTrim( Ddo_servicoprioridade_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Cleanfilter", StringUtil.RTrim( Ddo_servicoprioridade_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Noresultsfound", StringUtil.RTrim( Ddo_servicoprioridade_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_servicoprioridade_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Caption", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Tooltip", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Cls", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Filteredtext_set", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_servicoprioridade_finalidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_servicoprioridade_finalidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Sortedstatus", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Includefilter", StringUtil.BoolToStr( Ddo_servicoprioridade_finalidade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Filtertype", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_servicoprioridade_finalidade_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_servicoprioridade_finalidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Datalisttype", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Datalistproc", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servicoprioridade_finalidade_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Sortasc", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Sortdsc", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Loadingdata", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Cleanfilter", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Noresultsfound", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Activeeventkey", StringUtil.RTrim( Ddo_servicoprioridade_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_servicoprioridade_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_servicoprioridade_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Activeeventkey", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Filteredtext_get", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOPRIORIDADE_FINALIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEKQ2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTKQ2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwservicoprioridade.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWServicoPrioridade" ;
      }

      public override String GetPgmdesc( )
      {
         return " Prioridades padr�o" ;
      }

      protected void WBKQ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_KQ2( true) ;
         }
         else
         {
            wb_table1_2_KQ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_KQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(84, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(85, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicoprioridade_nome_Internalname, StringUtil.RTrim( AV34TFServicoPrioridade_Nome), StringUtil.RTrim( context.localUtil.Format( AV34TFServicoPrioridade_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicoprioridade_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicoprioridade_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoPrioridade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicoprioridade_nome_sel_Internalname, StringUtil.RTrim( AV35TFServicoPrioridade_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV35TFServicoPrioridade_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicoprioridade_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicoprioridade_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoPrioridade.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfservicoprioridade_finalidade_Internalname, AV38TFServicoPrioridade_Finalidade, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"", 0, edtavTfservicoprioridade_finalidade_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWServicoPrioridade.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfservicoprioridade_finalidade_sel_Internalname, AV39TFServicoPrioridade_Finalidade_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"", 0, edtavTfservicoprioridade_finalidade_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWServicoPrioridade.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOPRIORIDADE_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Internalname, AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", 0, edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServicoPrioridade.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOPRIORIDADE_FINALIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Internalname, AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"", 0, edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServicoPrioridade.htm");
         }
         wbLoad = true;
      }

      protected void STARTKQ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Prioridades padr�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPKQ0( ) ;
      }

      protected void WSKQ2( )
      {
         STARTKQ2( ) ;
         EVTKQ2( ) ;
      }

      protected void EVTKQ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11KQ2 */
                              E11KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOPRIORIDADE_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12KQ2 */
                              E12KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOPRIORIDADE_FINALIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13KQ2 */
                              E13KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14KQ2 */
                              E14KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15KQ2 */
                              E15KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16KQ2 */
                              E16KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17KQ2 */
                              E17KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18KQ2 */
                              E18KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19KQ2 */
                              E19KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20KQ2 */
                              E20KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21KQ2 */
                              E21KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22KQ2 */
                              E22KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23KQ2 */
                              E23KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24KQ2 */
                              E24KQ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_73_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
                              SubsflControlProps_732( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV59Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV60Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A1440ServicoPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoPrioridade_Codigo_Internalname), ",", "."));
                              A1439ServicoPrioridade_SrvCod = (int)(context.localUtil.CToN( cgiGet( edtServicoPrioridade_SrvCod_Internalname), ",", "."));
                              A1441ServicoPrioridade_Nome = StringUtil.Upper( cgiGet( edtServicoPrioridade_Nome_Internalname));
                              A1442ServicoPrioridade_Finalidade = cgiGet( edtServicoPrioridade_Finalidade_Internalname);
                              n1442ServicoPrioridade_Finalidade = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25KQ2 */
                                    E25KQ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26KQ2 */
                                    E26KQ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27KQ2 */
                                    E27KQ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servicoprioridade_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOPRIORIDADE_NOME1"), AV17ServicoPrioridade_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servicoprioridade_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOPRIORIDADE_NOME2"), AV21ServicoPrioridade_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servicoprioridade_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOPRIORIDADE_NOME3"), AV25ServicoPrioridade_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicoprioridade_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOPRIORIDADE_NOME"), AV34TFServicoPrioridade_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicoprioridade_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOPRIORIDADE_NOME_SEL"), AV35TFServicoPrioridade_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicoprioridade_finalidade Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOPRIORIDADE_FINALIDADE"), AV38TFServicoPrioridade_Finalidade) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicoprioridade_finalidade_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOPRIORIDADE_FINALIDADE_SEL"), AV39TFServicoPrioridade_Finalidade_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKQ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAKQ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SERVICOPRIORIDADE_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SERVICOPRIORIDADE_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("SERVICOPRIORIDADE_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_732( ) ;
         while ( nGXsfl_73_idx <= nRC_GXsfl_73 )
         {
            sendrow_732( ) ;
            nGXsfl_73_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_732( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV17ServicoPrioridade_Nome1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV21ServicoPrioridade_Nome2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV25ServicoPrioridade_Nome3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV34TFServicoPrioridade_Nome ,
                                       String AV35TFServicoPrioridade_Nome_Sel ,
                                       String AV38TFServicoPrioridade_Finalidade ,
                                       String AV39TFServicoPrioridade_Finalidade_Sel ,
                                       String AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace ,
                                       String AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace ,
                                       String AV61Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A1440ServicoPrioridade_Codigo ,
                                       int A1439ServicoPrioridade_SrvCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFKQ2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICOPRIORIDADE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1440ServicoPrioridade_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SERVICOPRIORIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICOPRIORIDADE_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1441ServicoPrioridade_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "SERVICOPRIORIDADE_NOME", StringUtil.RTrim( A1441ServicoPrioridade_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICOPRIORIDADE_FINALIDADE", GetSecureSignedToken( "", A1442ServicoPrioridade_Finalidade));
         GxWebStd.gx_hidden_field( context, "SERVICOPRIORIDADE_FINALIDADE", A1442ServicoPrioridade_Finalidade);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKQ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV61Pgmname = "WWServicoPrioridade";
         context.Gx_err = 0;
      }

      protected void RFKQ2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 73;
         /* Execute user event: E26KQ2 */
         E26KQ2 ();
         nGXsfl_73_idx = 1;
         sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
         SubsflControlProps_732( ) ;
         nGXsfl_73_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_732( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1 ,
                                                 AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 ,
                                                 AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 ,
                                                 AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2 ,
                                                 AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 ,
                                                 AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 ,
                                                 AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3 ,
                                                 AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 ,
                                                 AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel ,
                                                 AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome ,
                                                 AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel ,
                                                 AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade ,
                                                 A1441ServicoPrioridade_Nome ,
                                                 A1442ServicoPrioridade_Finalidade ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 = StringUtil.PadR( StringUtil.RTrim( AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1), 50, "%");
            lV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 = StringUtil.PadR( StringUtil.RTrim( AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2), 50, "%");
            lV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 = StringUtil.PadR( StringUtil.RTrim( AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3), 50, "%");
            lV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome = StringUtil.PadR( StringUtil.RTrim( AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome), 50, "%");
            lV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade = StringUtil.Concat( StringUtil.RTrim( AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade), "%", "");
            /* Using cursor H00KQ2 */
            pr_default.execute(0, new Object[] {lV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1, lV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2, lV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3, lV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome, AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel, lV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade, AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_73_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1442ServicoPrioridade_Finalidade = H00KQ2_A1442ServicoPrioridade_Finalidade[0];
               n1442ServicoPrioridade_Finalidade = H00KQ2_n1442ServicoPrioridade_Finalidade[0];
               A1441ServicoPrioridade_Nome = H00KQ2_A1441ServicoPrioridade_Nome[0];
               A1439ServicoPrioridade_SrvCod = H00KQ2_A1439ServicoPrioridade_SrvCod[0];
               A1440ServicoPrioridade_Codigo = H00KQ2_A1440ServicoPrioridade_Codigo[0];
               /* Execute user event: E27KQ2 */
               E27KQ2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 73;
            WBKQ0( ) ;
         }
         nGXsfl_73_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 = AV17ServicoPrioridade_Nome1;
         AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 = AV21ServicoPrioridade_Nome2;
         AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 = AV25ServicoPrioridade_Nome3;
         AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome = AV34TFServicoPrioridade_Nome;
         AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel = AV35TFServicoPrioridade_Nome_Sel;
         AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade = AV38TFServicoPrioridade_Finalidade;
         AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel = AV39TFServicoPrioridade_Finalidade_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1 ,
                                              AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 ,
                                              AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 ,
                                              AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2 ,
                                              AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 ,
                                              AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 ,
                                              AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3 ,
                                              AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 ,
                                              AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel ,
                                              AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome ,
                                              AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel ,
                                              AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade ,
                                              A1441ServicoPrioridade_Nome ,
                                              A1442ServicoPrioridade_Finalidade ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 = StringUtil.PadR( StringUtil.RTrim( AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1), 50, "%");
         lV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 = StringUtil.PadR( StringUtil.RTrim( AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2), 50, "%");
         lV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 = StringUtil.PadR( StringUtil.RTrim( AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3), 50, "%");
         lV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome = StringUtil.PadR( StringUtil.RTrim( AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome), 50, "%");
         lV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade = StringUtil.Concat( StringUtil.RTrim( AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade), "%", "");
         /* Using cursor H00KQ3 */
         pr_default.execute(1, new Object[] {lV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1, lV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2, lV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3, lV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome, AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel, lV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade, AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel});
         GRID_nRecordCount = H00KQ3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 = AV17ServicoPrioridade_Nome1;
         AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 = AV21ServicoPrioridade_Nome2;
         AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 = AV25ServicoPrioridade_Nome3;
         AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome = AV34TFServicoPrioridade_Nome;
         AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel = AV35TFServicoPrioridade_Nome_Sel;
         AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade = AV38TFServicoPrioridade_Finalidade;
         AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel = AV39TFServicoPrioridade_Finalidade_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ServicoPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ServicoPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ServicoPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoPrioridade_Nome, AV35TFServicoPrioridade_Nome_Sel, AV38TFServicoPrioridade_Finalidade, AV39TFServicoPrioridade_Finalidade_Sel, AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV61Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 = AV17ServicoPrioridade_Nome1;
         AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 = AV21ServicoPrioridade_Nome2;
         AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 = AV25ServicoPrioridade_Nome3;
         AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome = AV34TFServicoPrioridade_Nome;
         AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel = AV35TFServicoPrioridade_Nome_Sel;
         AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade = AV38TFServicoPrioridade_Finalidade;
         AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel = AV39TFServicoPrioridade_Finalidade_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ServicoPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ServicoPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ServicoPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoPrioridade_Nome, AV35TFServicoPrioridade_Nome_Sel, AV38TFServicoPrioridade_Finalidade, AV39TFServicoPrioridade_Finalidade_Sel, AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV61Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 = AV17ServicoPrioridade_Nome1;
         AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 = AV21ServicoPrioridade_Nome2;
         AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 = AV25ServicoPrioridade_Nome3;
         AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome = AV34TFServicoPrioridade_Nome;
         AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel = AV35TFServicoPrioridade_Nome_Sel;
         AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade = AV38TFServicoPrioridade_Finalidade;
         AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel = AV39TFServicoPrioridade_Finalidade_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ServicoPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ServicoPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ServicoPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoPrioridade_Nome, AV35TFServicoPrioridade_Nome_Sel, AV38TFServicoPrioridade_Finalidade, AV39TFServicoPrioridade_Finalidade_Sel, AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV61Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 = AV17ServicoPrioridade_Nome1;
         AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 = AV21ServicoPrioridade_Nome2;
         AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 = AV25ServicoPrioridade_Nome3;
         AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome = AV34TFServicoPrioridade_Nome;
         AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel = AV35TFServicoPrioridade_Nome_Sel;
         AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade = AV38TFServicoPrioridade_Finalidade;
         AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel = AV39TFServicoPrioridade_Finalidade_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ServicoPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ServicoPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ServicoPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoPrioridade_Nome, AV35TFServicoPrioridade_Nome_Sel, AV38TFServicoPrioridade_Finalidade, AV39TFServicoPrioridade_Finalidade_Sel, AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV61Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 = AV17ServicoPrioridade_Nome1;
         AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 = AV21ServicoPrioridade_Nome2;
         AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 = AV25ServicoPrioridade_Nome3;
         AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome = AV34TFServicoPrioridade_Nome;
         AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel = AV35TFServicoPrioridade_Nome_Sel;
         AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade = AV38TFServicoPrioridade_Finalidade;
         AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel = AV39TFServicoPrioridade_Finalidade_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ServicoPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ServicoPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ServicoPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoPrioridade_Nome, AV35TFServicoPrioridade_Nome_Sel, AV38TFServicoPrioridade_Finalidade, AV39TFServicoPrioridade_Finalidade_Sel, AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV61Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUPKQ0( )
      {
         /* Before Start, stand alone formulas. */
         AV61Pgmname = "WWServicoPrioridade";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25KQ2 */
         E25KQ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV41DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOPRIORIDADE_NOMETITLEFILTERDATA"), AV33ServicoPrioridade_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOPRIORIDADE_FINALIDADETITLEFILTERDATA"), AV37ServicoPrioridade_FinalidadeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV17ServicoPrioridade_Nome1 = StringUtil.Upper( cgiGet( edtavServicoprioridade_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoPrioridade_Nome1", AV17ServicoPrioridade_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            AV21ServicoPrioridade_Nome2 = StringUtil.Upper( cgiGet( edtavServicoprioridade_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoPrioridade_Nome2", AV21ServicoPrioridade_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            AV25ServicoPrioridade_Nome3 = StringUtil.Upper( cgiGet( edtavServicoprioridade_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoPrioridade_Nome3", AV25ServicoPrioridade_Nome3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV34TFServicoPrioridade_Nome = StringUtil.Upper( cgiGet( edtavTfservicoprioridade_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFServicoPrioridade_Nome", AV34TFServicoPrioridade_Nome);
            AV35TFServicoPrioridade_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfservicoprioridade_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFServicoPrioridade_Nome_Sel", AV35TFServicoPrioridade_Nome_Sel);
            AV38TFServicoPrioridade_Finalidade = cgiGet( edtavTfservicoprioridade_finalidade_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFServicoPrioridade_Finalidade", AV38TFServicoPrioridade_Finalidade);
            AV39TFServicoPrioridade_Finalidade_Sel = cgiGet( edtavTfservicoprioridade_finalidade_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFServicoPrioridade_Finalidade_Sel", AV39TFServicoPrioridade_Finalidade_Sel);
            AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace = cgiGet( edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace", AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace);
            AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace = cgiGet( edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace", AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_73 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_73"), ",", "."));
            AV43GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV44GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_servicoprioridade_nome_Caption = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Caption");
            Ddo_servicoprioridade_nome_Tooltip = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Tooltip");
            Ddo_servicoprioridade_nome_Cls = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Cls");
            Ddo_servicoprioridade_nome_Filteredtext_set = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Filteredtext_set");
            Ddo_servicoprioridade_nome_Selectedvalue_set = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Selectedvalue_set");
            Ddo_servicoprioridade_nome_Dropdownoptionstype = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Dropdownoptionstype");
            Ddo_servicoprioridade_nome_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Titlecontrolidtoreplace");
            Ddo_servicoprioridade_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Includesortasc"));
            Ddo_servicoprioridade_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Includesortdsc"));
            Ddo_servicoprioridade_nome_Sortedstatus = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Sortedstatus");
            Ddo_servicoprioridade_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Includefilter"));
            Ddo_servicoprioridade_nome_Filtertype = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Filtertype");
            Ddo_servicoprioridade_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Filterisrange"));
            Ddo_servicoprioridade_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Includedatalist"));
            Ddo_servicoprioridade_nome_Datalisttype = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Datalisttype");
            Ddo_servicoprioridade_nome_Datalistproc = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Datalistproc");
            Ddo_servicoprioridade_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servicoprioridade_nome_Sortasc = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Sortasc");
            Ddo_servicoprioridade_nome_Sortdsc = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Sortdsc");
            Ddo_servicoprioridade_nome_Loadingdata = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Loadingdata");
            Ddo_servicoprioridade_nome_Cleanfilter = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Cleanfilter");
            Ddo_servicoprioridade_nome_Noresultsfound = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Noresultsfound");
            Ddo_servicoprioridade_nome_Searchbuttontext = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Searchbuttontext");
            Ddo_servicoprioridade_finalidade_Caption = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Caption");
            Ddo_servicoprioridade_finalidade_Tooltip = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Tooltip");
            Ddo_servicoprioridade_finalidade_Cls = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Cls");
            Ddo_servicoprioridade_finalidade_Filteredtext_set = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Filteredtext_set");
            Ddo_servicoprioridade_finalidade_Selectedvalue_set = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Selectedvalue_set");
            Ddo_servicoprioridade_finalidade_Dropdownoptionstype = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Dropdownoptionstype");
            Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Titlecontrolidtoreplace");
            Ddo_servicoprioridade_finalidade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Includesortasc"));
            Ddo_servicoprioridade_finalidade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Includesortdsc"));
            Ddo_servicoprioridade_finalidade_Sortedstatus = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Sortedstatus");
            Ddo_servicoprioridade_finalidade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Includefilter"));
            Ddo_servicoprioridade_finalidade_Filtertype = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Filtertype");
            Ddo_servicoprioridade_finalidade_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Filterisrange"));
            Ddo_servicoprioridade_finalidade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Includedatalist"));
            Ddo_servicoprioridade_finalidade_Datalisttype = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Datalisttype");
            Ddo_servicoprioridade_finalidade_Datalistproc = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Datalistproc");
            Ddo_servicoprioridade_finalidade_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servicoprioridade_finalidade_Sortasc = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Sortasc");
            Ddo_servicoprioridade_finalidade_Sortdsc = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Sortdsc");
            Ddo_servicoprioridade_finalidade_Loadingdata = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Loadingdata");
            Ddo_servicoprioridade_finalidade_Cleanfilter = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Cleanfilter");
            Ddo_servicoprioridade_finalidade_Noresultsfound = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Noresultsfound");
            Ddo_servicoprioridade_finalidade_Searchbuttontext = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_servicoprioridade_nome_Activeeventkey = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Activeeventkey");
            Ddo_servicoprioridade_nome_Filteredtext_get = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Filteredtext_get");
            Ddo_servicoprioridade_nome_Selectedvalue_get = cgiGet( "DDO_SERVICOPRIORIDADE_NOME_Selectedvalue_get");
            Ddo_servicoprioridade_finalidade_Activeeventkey = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Activeeventkey");
            Ddo_servicoprioridade_finalidade_Filteredtext_get = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Filteredtext_get");
            Ddo_servicoprioridade_finalidade_Selectedvalue_get = cgiGet( "DDO_SERVICOPRIORIDADE_FINALIDADE_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOPRIORIDADE_NOME1"), AV17ServicoPrioridade_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOPRIORIDADE_NOME2"), AV21ServicoPrioridade_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOPRIORIDADE_NOME3"), AV25ServicoPrioridade_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOPRIORIDADE_NOME"), AV34TFServicoPrioridade_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOPRIORIDADE_NOME_SEL"), AV35TFServicoPrioridade_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOPRIORIDADE_FINALIDADE"), AV38TFServicoPrioridade_Finalidade) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOPRIORIDADE_FINALIDADE_SEL"), AV39TFServicoPrioridade_Finalidade_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25KQ2 */
         E25KQ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25KQ2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "SERVICOPRIORIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "SERVICOPRIORIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "SERVICOPRIORIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfservicoprioridade_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicoprioridade_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicoprioridade_nome_Visible), 5, 0)));
         edtavTfservicoprioridade_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicoprioridade_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicoprioridade_nome_sel_Visible), 5, 0)));
         edtavTfservicoprioridade_finalidade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicoprioridade_finalidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicoprioridade_finalidade_Visible), 5, 0)));
         edtavTfservicoprioridade_finalidade_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicoprioridade_finalidade_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicoprioridade_finalidade_sel_Visible), 5, 0)));
         Ddo_servicoprioridade_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoPrioridade_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_nome_Internalname, "TitleControlIdToReplace", Ddo_servicoprioridade_nome_Titlecontrolidtoreplace);
         AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace = Ddo_servicoprioridade_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace", AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace);
         edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoPrioridade_Finalidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_finalidade_Internalname, "TitleControlIdToReplace", Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace);
         AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace = Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace", AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace);
         edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Prioridades padr�o";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Finalidade", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV41DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV41DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E26KQ2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV33ServicoPrioridade_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37ServicoPrioridade_FinalidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtServicoPrioridade_Nome_Titleformat = 2;
         edtServicoPrioridade_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoPrioridade_Nome_Internalname, "Title", edtServicoPrioridade_Nome_Title);
         edtServicoPrioridade_Finalidade_Titleformat = 2;
         edtServicoPrioridade_Finalidade_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Finalidade", AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoPrioridade_Finalidade_Internalname, "Title", edtServicoPrioridade_Finalidade_Title);
         AV43GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43GridCurrentPage), 10, 0)));
         AV44GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44GridPageCount), 10, 0)));
         AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 = AV17ServicoPrioridade_Nome1;
         AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 = AV21ServicoPrioridade_Nome2;
         AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 = AV25ServicoPrioridade_Nome3;
         AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome = AV34TFServicoPrioridade_Nome;
         AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel = AV35TFServicoPrioridade_Nome_Sel;
         AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade = AV38TFServicoPrioridade_Finalidade;
         AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel = AV39TFServicoPrioridade_Finalidade_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33ServicoPrioridade_NomeTitleFilterData", AV33ServicoPrioridade_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37ServicoPrioridade_FinalidadeTitleFilterData", AV37ServicoPrioridade_FinalidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11KQ2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV42PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV42PageToGo) ;
         }
      }

      protected void E12KQ2( )
      {
         /* Ddo_servicoprioridade_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicoprioridade_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicoprioridade_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_nome_Internalname, "SortedStatus", Ddo_servicoprioridade_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicoprioridade_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicoprioridade_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_nome_Internalname, "SortedStatus", Ddo_servicoprioridade_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicoprioridade_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFServicoPrioridade_Nome = Ddo_servicoprioridade_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFServicoPrioridade_Nome", AV34TFServicoPrioridade_Nome);
            AV35TFServicoPrioridade_Nome_Sel = Ddo_servicoprioridade_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFServicoPrioridade_Nome_Sel", AV35TFServicoPrioridade_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13KQ2( )
      {
         /* Ddo_servicoprioridade_finalidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicoprioridade_finalidade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicoprioridade_finalidade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_finalidade_Internalname, "SortedStatus", Ddo_servicoprioridade_finalidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicoprioridade_finalidade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicoprioridade_finalidade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_finalidade_Internalname, "SortedStatus", Ddo_servicoprioridade_finalidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicoprioridade_finalidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFServicoPrioridade_Finalidade = Ddo_servicoprioridade_finalidade_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFServicoPrioridade_Finalidade", AV38TFServicoPrioridade_Finalidade);
            AV39TFServicoPrioridade_Finalidade_Sel = Ddo_servicoprioridade_finalidade_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFServicoPrioridade_Finalidade_Sel", AV39TFServicoPrioridade_Finalidade_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E27KQ2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV59Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("servicoprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1440ServicoPrioridade_Codigo) + "," + UrlEncode("" +A1439ServicoPrioridade_SrvCod);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV60Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("servicoprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1440ServicoPrioridade_Codigo) + "," + UrlEncode("" +A1439ServicoPrioridade_SrvCod);
         edtServicoPrioridade_Nome_Link = formatLink("viewservicoprioridade.aspx") + "?" + UrlEncode("" +A1440ServicoPrioridade_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 73;
         }
         sendrow_732( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(73, GridRow);
         }
      }

      protected void E14KQ2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E20KQ2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E15KQ2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ServicoPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ServicoPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ServicoPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoPrioridade_Nome, AV35TFServicoPrioridade_Nome_Sel, AV38TFServicoPrioridade_Finalidade, AV39TFServicoPrioridade_Finalidade_Sel, AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV61Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21KQ2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22KQ2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E16KQ2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ServicoPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ServicoPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ServicoPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoPrioridade_Nome, AV35TFServicoPrioridade_Nome_Sel, AV38TFServicoPrioridade_Finalidade, AV39TFServicoPrioridade_Finalidade_Sel, AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV61Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23KQ2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17KQ2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ServicoPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ServicoPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ServicoPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoPrioridade_Nome, AV35TFServicoPrioridade_Nome_Sel, AV38TFServicoPrioridade_Finalidade, AV39TFServicoPrioridade_Finalidade_Sel, AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV61Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E24KQ2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18KQ2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E19KQ2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("servicoprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_servicoprioridade_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_nome_Internalname, "SortedStatus", Ddo_servicoprioridade_nome_Sortedstatus);
         Ddo_servicoprioridade_finalidade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_finalidade_Internalname, "SortedStatus", Ddo_servicoprioridade_finalidade_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_servicoprioridade_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_nome_Internalname, "SortedStatus", Ddo_servicoprioridade_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_servicoprioridade_finalidade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_finalidade_Internalname, "SortedStatus", Ddo_servicoprioridade_finalidade_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavServicoprioridade_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicoprioridade_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicoprioridade_nome1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOPRIORIDADE_NOME") == 0 )
         {
            edtavServicoprioridade_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicoprioridade_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicoprioridade_nome1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavServicoprioridade_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicoprioridade_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicoprioridade_nome2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICOPRIORIDADE_NOME") == 0 )
         {
            edtavServicoprioridade_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicoprioridade_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicoprioridade_nome2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavServicoprioridade_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicoprioridade_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicoprioridade_nome3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SERVICOPRIORIDADE_NOME") == 0 )
         {
            edtavServicoprioridade_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicoprioridade_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicoprioridade_nome3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "SERVICOPRIORIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV21ServicoPrioridade_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoPrioridade_Nome2", AV21ServicoPrioridade_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "SERVICOPRIORIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV25ServicoPrioridade_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoPrioridade_Nome3", AV25ServicoPrioridade_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV34TFServicoPrioridade_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFServicoPrioridade_Nome", AV34TFServicoPrioridade_Nome);
         Ddo_servicoprioridade_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_nome_Internalname, "FilteredText_set", Ddo_servicoprioridade_nome_Filteredtext_set);
         AV35TFServicoPrioridade_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFServicoPrioridade_Nome_Sel", AV35TFServicoPrioridade_Nome_Sel);
         Ddo_servicoprioridade_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_nome_Internalname, "SelectedValue_set", Ddo_servicoprioridade_nome_Selectedvalue_set);
         AV38TFServicoPrioridade_Finalidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFServicoPrioridade_Finalidade", AV38TFServicoPrioridade_Finalidade);
         Ddo_servicoprioridade_finalidade_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_finalidade_Internalname, "FilteredText_set", Ddo_servicoprioridade_finalidade_Filteredtext_set);
         AV39TFServicoPrioridade_Finalidade_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFServicoPrioridade_Finalidade_Sel", AV39TFServicoPrioridade_Finalidade_Sel);
         Ddo_servicoprioridade_finalidade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_finalidade_Internalname, "SelectedValue_set", Ddo_servicoprioridade_finalidade_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "SERVICOPRIORIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17ServicoPrioridade_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoPrioridade_Nome1", AV17ServicoPrioridade_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV61Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV61Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV61Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV62GXV1 = 1;
         while ( AV62GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV62GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOPRIORIDADE_NOME") == 0 )
            {
               AV34TFServicoPrioridade_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFServicoPrioridade_Nome", AV34TFServicoPrioridade_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFServicoPrioridade_Nome)) )
               {
                  Ddo_servicoprioridade_nome_Filteredtext_set = AV34TFServicoPrioridade_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_nome_Internalname, "FilteredText_set", Ddo_servicoprioridade_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOPRIORIDADE_NOME_SEL") == 0 )
            {
               AV35TFServicoPrioridade_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFServicoPrioridade_Nome_Sel", AV35TFServicoPrioridade_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFServicoPrioridade_Nome_Sel)) )
               {
                  Ddo_servicoprioridade_nome_Selectedvalue_set = AV35TFServicoPrioridade_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_nome_Internalname, "SelectedValue_set", Ddo_servicoprioridade_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOPRIORIDADE_FINALIDADE") == 0 )
            {
               AV38TFServicoPrioridade_Finalidade = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFServicoPrioridade_Finalidade", AV38TFServicoPrioridade_Finalidade);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFServicoPrioridade_Finalidade)) )
               {
                  Ddo_servicoprioridade_finalidade_Filteredtext_set = AV38TFServicoPrioridade_Finalidade;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_finalidade_Internalname, "FilteredText_set", Ddo_servicoprioridade_finalidade_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOPRIORIDADE_FINALIDADE_SEL") == 0 )
            {
               AV39TFServicoPrioridade_Finalidade_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFServicoPrioridade_Finalidade_Sel", AV39TFServicoPrioridade_Finalidade_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFServicoPrioridade_Finalidade_Sel)) )
               {
                  Ddo_servicoprioridade_finalidade_Selectedvalue_set = AV39TFServicoPrioridade_Finalidade_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoprioridade_finalidade_Internalname, "SelectedValue_set", Ddo_servicoprioridade_finalidade_Selectedvalue_set);
               }
            }
            AV62GXV1 = (int)(AV62GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOPRIORIDADE_NOME") == 0 )
            {
               AV17ServicoPrioridade_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoPrioridade_Nome1", AV17ServicoPrioridade_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICOPRIORIDADE_NOME") == 0 )
               {
                  AV21ServicoPrioridade_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoPrioridade_Nome2", AV21ServicoPrioridade_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SERVICOPRIORIDADE_NOME") == 0 )
                  {
                     AV25ServicoPrioridade_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoPrioridade_Nome3", AV25ServicoPrioridade_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV61Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFServicoPrioridade_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOPRIORIDADE_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV34TFServicoPrioridade_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFServicoPrioridade_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOPRIORIDADE_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV35TFServicoPrioridade_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFServicoPrioridade_Finalidade)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOPRIORIDADE_FINALIDADE";
            AV11GridStateFilterValue.gxTpr_Value = AV38TFServicoPrioridade_Finalidade;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFServicoPrioridade_Finalidade_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOPRIORIDADE_FINALIDADE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFServicoPrioridade_Finalidade_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV61Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOPRIORIDADE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ServicoPrioridade_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17ServicoPrioridade_Nome1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICOPRIORIDADE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ServicoPrioridade_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21ServicoPrioridade_Nome2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SERVICOPRIORIDADE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ServicoPrioridade_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ServicoPrioridade_Nome3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV61Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ServicoPrioridade";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_KQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_KQ2( true) ;
         }
         else
         {
            wb_table2_8_KQ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_KQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_67_KQ2( true) ;
         }
         else
         {
            wb_table3_67_KQ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_67_KQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_KQ2e( true) ;
         }
         else
         {
            wb_table1_2_KQ2e( false) ;
         }
      }

      protected void wb_table3_67_KQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_70_KQ2( true) ;
         }
         else
         {
            wb_table4_70_KQ2( false) ;
         }
         return  ;
      }

      protected void wb_table4_70_KQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_67_KQ2e( true) ;
         }
         else
         {
            wb_table3_67_KQ2e( false) ;
         }
      }

      protected void wb_table4_70_KQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"73\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Prioridade_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoPrioridade_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoPrioridade_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoPrioridade_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoPrioridade_Finalidade_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoPrioridade_Finalidade_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoPrioridade_Finalidade_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1441ServicoPrioridade_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoPrioridade_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoPrioridade_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtServicoPrioridade_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1442ServicoPrioridade_Finalidade);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoPrioridade_Finalidade_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoPrioridade_Finalidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 73 )
         {
            wbEnd = 0;
            nRC_GXsfl_73 = (short)(nGXsfl_73_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_70_KQ2e( true) ;
         }
         else
         {
            wb_table4_70_KQ2e( false) ;
         }
      }

      protected void wb_table2_8_KQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServicoprioridadetitle_Internalname, "Prioridades padr�o", "", "", lblServicoprioridadetitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_KQ2( true) ;
         }
         else
         {
            wb_table5_13_KQ2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_KQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWServicoPrioridade.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_KQ2( true) ;
         }
         else
         {
            wb_table6_23_KQ2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_KQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_KQ2e( true) ;
         }
         else
         {
            wb_table2_8_KQ2e( false) ;
         }
      }

      protected void wb_table6_23_KQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_KQ2( true) ;
         }
         else
         {
            wb_table7_28_KQ2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_KQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_KQ2e( true) ;
         }
         else
         {
            wb_table6_23_KQ2e( false) ;
         }
      }

      protected void wb_table7_28_KQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWServicoPrioridade.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicoprioridade_nome1_Internalname, StringUtil.RTrim( AV17ServicoPrioridade_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17ServicoPrioridade_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicoprioridade_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServicoprioridade_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoPrioridade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_WWServicoPrioridade.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicoprioridade_nome2_Internalname, StringUtil.RTrim( AV21ServicoPrioridade_Nome2), StringUtil.RTrim( context.localUtil.Format( AV21ServicoPrioridade_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicoprioridade_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServicoprioridade_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoPrioridade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWServicoPrioridade.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicoprioridade_nome3_Internalname, StringUtil.RTrim( AV25ServicoPrioridade_Nome3), StringUtil.RTrim( context.localUtil.Format( AV25ServicoPrioridade_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicoprioridade_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServicoprioridade_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_KQ2e( true) ;
         }
         else
         {
            wb_table7_28_KQ2e( false) ;
         }
      }

      protected void wb_table5_13_KQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_KQ2e( true) ;
         }
         else
         {
            wb_table5_13_KQ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKQ2( ) ;
         WSKQ2( ) ;
         WEKQ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181332025");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwservicoprioridade.js", "?20205181332025");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_732( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_73_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_73_idx;
         edtServicoPrioridade_Codigo_Internalname = "SERVICOPRIORIDADE_CODIGO_"+sGXsfl_73_idx;
         edtServicoPrioridade_SrvCod_Internalname = "SERVICOPRIORIDADE_SRVCOD_"+sGXsfl_73_idx;
         edtServicoPrioridade_Nome_Internalname = "SERVICOPRIORIDADE_NOME_"+sGXsfl_73_idx;
         edtServicoPrioridade_Finalidade_Internalname = "SERVICOPRIORIDADE_FINALIDADE_"+sGXsfl_73_idx;
      }

      protected void SubsflControlProps_fel_732( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_73_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_73_fel_idx;
         edtServicoPrioridade_Codigo_Internalname = "SERVICOPRIORIDADE_CODIGO_"+sGXsfl_73_fel_idx;
         edtServicoPrioridade_SrvCod_Internalname = "SERVICOPRIORIDADE_SRVCOD_"+sGXsfl_73_fel_idx;
         edtServicoPrioridade_Nome_Internalname = "SERVICOPRIORIDADE_NOME_"+sGXsfl_73_fel_idx;
         edtServicoPrioridade_Finalidade_Internalname = "SERVICOPRIORIDADE_FINALIDADE_"+sGXsfl_73_fel_idx;
      }

      protected void sendrow_732( )
      {
         SubsflControlProps_732( ) ;
         WBKQ0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_73_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_73_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_73_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV59Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV59Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV60Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV60Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoPrioridade_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1440ServicoPrioridade_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoPrioridade_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoPrioridade_SrvCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1439ServicoPrioridade_SrvCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoPrioridade_SrvCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoPrioridade_Nome_Internalname,StringUtil.RTrim( A1441ServicoPrioridade_Nome),StringUtil.RTrim( context.localUtil.Format( A1441ServicoPrioridade_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtServicoPrioridade_Nome_Link,(String)"",(String)"",(String)"",(String)edtServicoPrioridade_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoPrioridade_Finalidade_Internalname,(String)A1442ServicoPrioridade_Finalidade,(String)A1442ServicoPrioridade_Finalidade,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoPrioridade_Finalidade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)73,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICOPRIORIDADE_CODIGO"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, context.localUtil.Format( (decimal)(A1440ServicoPrioridade_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICOPRIORIDADE_NOME"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, StringUtil.RTrim( context.localUtil.Format( A1441ServicoPrioridade_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICOPRIORIDADE_FINALIDADE"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, A1442ServicoPrioridade_Finalidade));
            GridContainer.AddRow(GridRow);
            nGXsfl_73_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_732( ) ;
         }
         /* End function sendrow_732 */
      }

      protected void init_default_properties( )
      {
         lblServicoprioridadetitle_Internalname = "SERVICOPRIORIDADETITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavServicoprioridade_nome1_Internalname = "vSERVICOPRIORIDADE_NOME1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavServicoprioridade_nome2_Internalname = "vSERVICOPRIORIDADE_NOME2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavServicoprioridade_nome3_Internalname = "vSERVICOPRIORIDADE_NOME3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtServicoPrioridade_Codigo_Internalname = "SERVICOPRIORIDADE_CODIGO";
         edtServicoPrioridade_SrvCod_Internalname = "SERVICOPRIORIDADE_SRVCOD";
         edtServicoPrioridade_Nome_Internalname = "SERVICOPRIORIDADE_NOME";
         edtServicoPrioridade_Finalidade_Internalname = "SERVICOPRIORIDADE_FINALIDADE";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfservicoprioridade_nome_Internalname = "vTFSERVICOPRIORIDADE_NOME";
         edtavTfservicoprioridade_nome_sel_Internalname = "vTFSERVICOPRIORIDADE_NOME_SEL";
         edtavTfservicoprioridade_finalidade_Internalname = "vTFSERVICOPRIORIDADE_FINALIDADE";
         edtavTfservicoprioridade_finalidade_sel_Internalname = "vTFSERVICOPRIORIDADE_FINALIDADE_SEL";
         Ddo_servicoprioridade_nome_Internalname = "DDO_SERVICOPRIORIDADE_NOME";
         edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Internalname = "vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE";
         Ddo_servicoprioridade_finalidade_Internalname = "DDO_SERVICOPRIORIDADE_FINALIDADE";
         edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Internalname = "vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtServicoPrioridade_Finalidade_Jsonclick = "";
         edtServicoPrioridade_Nome_Jsonclick = "";
         edtServicoPrioridade_SrvCod_Jsonclick = "";
         edtServicoPrioridade_Codigo_Jsonclick = "";
         edtavServicoprioridade_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavServicoprioridade_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavServicoprioridade_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtServicoPrioridade_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtServicoPrioridade_Finalidade_Titleformat = 0;
         edtServicoPrioridade_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavServicoprioridade_nome3_Visible = 1;
         edtavServicoprioridade_nome2_Visible = 1;
         edtavServicoprioridade_nome1_Visible = 1;
         edtServicoPrioridade_Finalidade_Title = "Finalidade";
         edtServicoPrioridade_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfservicoprioridade_finalidade_sel_Visible = 1;
         edtavTfservicoprioridade_finalidade_Visible = 1;
         edtavTfservicoprioridade_nome_sel_Jsonclick = "";
         edtavTfservicoprioridade_nome_sel_Visible = 1;
         edtavTfservicoprioridade_nome_Jsonclick = "";
         edtavTfservicoprioridade_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_servicoprioridade_finalidade_Searchbuttontext = "Pesquisar";
         Ddo_servicoprioridade_finalidade_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servicoprioridade_finalidade_Cleanfilter = "Limpar pesquisa";
         Ddo_servicoprioridade_finalidade_Loadingdata = "Carregando dados...";
         Ddo_servicoprioridade_finalidade_Sortdsc = "Ordenar de Z � A";
         Ddo_servicoprioridade_finalidade_Sortasc = "Ordenar de A � Z";
         Ddo_servicoprioridade_finalidade_Datalistupdateminimumcharacters = 0;
         Ddo_servicoprioridade_finalidade_Datalistproc = "GetWWServicoPrioridadeFilterData";
         Ddo_servicoprioridade_finalidade_Datalisttype = "Dynamic";
         Ddo_servicoprioridade_finalidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_finalidade_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servicoprioridade_finalidade_Filtertype = "Character";
         Ddo_servicoprioridade_finalidade_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_finalidade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_finalidade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace = "";
         Ddo_servicoprioridade_finalidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicoprioridade_finalidade_Cls = "ColumnSettings";
         Ddo_servicoprioridade_finalidade_Tooltip = "Op��es";
         Ddo_servicoprioridade_finalidade_Caption = "";
         Ddo_servicoprioridade_nome_Searchbuttontext = "Pesquisar";
         Ddo_servicoprioridade_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servicoprioridade_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_servicoprioridade_nome_Loadingdata = "Carregando dados...";
         Ddo_servicoprioridade_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_servicoprioridade_nome_Sortasc = "Ordenar de A � Z";
         Ddo_servicoprioridade_nome_Datalistupdateminimumcharacters = 0;
         Ddo_servicoprioridade_nome_Datalistproc = "GetWWServicoPrioridadeFilterData";
         Ddo_servicoprioridade_nome_Datalisttype = "Dynamic";
         Ddo_servicoprioridade_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servicoprioridade_nome_Filtertype = "Character";
         Ddo_servicoprioridade_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_nome_Titlecontrolidtoreplace = "";
         Ddo_servicoprioridade_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicoprioridade_nome_Cls = "ColumnSettings";
         Ddo_servicoprioridade_nome_Tooltip = "Op��es";
         Ddo_servicoprioridade_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Prioridades padr�o";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoPrioridade_Nome1',fld:'vSERVICOPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoPrioridade_Nome2',fld:'vSERVICOPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoPrioridade_Nome3',fld:'vSERVICOPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV34TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV39TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV33ServicoPrioridade_NomeTitleFilterData',fld:'vSERVICOPRIORIDADE_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV37ServicoPrioridade_FinalidadeTitleFilterData',fld:'vSERVICOPRIORIDADE_FINALIDADETITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoPrioridade_Nome_Titleformat',ctrl:'SERVICOPRIORIDADE_NOME',prop:'Titleformat'},{av:'edtServicoPrioridade_Nome_Title',ctrl:'SERVICOPRIORIDADE_NOME',prop:'Title'},{av:'edtServicoPrioridade_Finalidade_Titleformat',ctrl:'SERVICOPRIORIDADE_FINALIDADE',prop:'Titleformat'},{av:'edtServicoPrioridade_Finalidade_Title',ctrl:'SERVICOPRIORIDADE_FINALIDADE',prop:'Title'},{av:'AV43GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV44GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11KQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoPrioridade_Nome1',fld:'vSERVICOPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoPrioridade_Nome2',fld:'vSERVICOPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoPrioridade_Nome3',fld:'vSERVICOPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV39TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SERVICOPRIORIDADE_NOME.ONOPTIONCLICKED","{handler:'E12KQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoPrioridade_Nome1',fld:'vSERVICOPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoPrioridade_Nome2',fld:'vSERVICOPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoPrioridade_Nome3',fld:'vSERVICOPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV39TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_servicoprioridade_nome_Activeeventkey',ctrl:'DDO_SERVICOPRIORIDADE_NOME',prop:'ActiveEventKey'},{av:'Ddo_servicoprioridade_nome_Filteredtext_get',ctrl:'DDO_SERVICOPRIORIDADE_NOME',prop:'FilteredText_get'},{av:'Ddo_servicoprioridade_nome_Selectedvalue_get',ctrl:'DDO_SERVICOPRIORIDADE_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicoprioridade_nome_Sortedstatus',ctrl:'DDO_SERVICOPRIORIDADE_NOME',prop:'SortedStatus'},{av:'AV34TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_servicoprioridade_finalidade_Sortedstatus',ctrl:'DDO_SERVICOPRIORIDADE_FINALIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICOPRIORIDADE_FINALIDADE.ONOPTIONCLICKED","{handler:'E13KQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoPrioridade_Nome1',fld:'vSERVICOPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoPrioridade_Nome2',fld:'vSERVICOPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoPrioridade_Nome3',fld:'vSERVICOPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV39TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_servicoprioridade_finalidade_Activeeventkey',ctrl:'DDO_SERVICOPRIORIDADE_FINALIDADE',prop:'ActiveEventKey'},{av:'Ddo_servicoprioridade_finalidade_Filteredtext_get',ctrl:'DDO_SERVICOPRIORIDADE_FINALIDADE',prop:'FilteredText_get'},{av:'Ddo_servicoprioridade_finalidade_Selectedvalue_get',ctrl:'DDO_SERVICOPRIORIDADE_FINALIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicoprioridade_finalidade_Sortedstatus',ctrl:'DDO_SERVICOPRIORIDADE_FINALIDADE',prop:'SortedStatus'},{av:'AV38TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV39TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'Ddo_servicoprioridade_nome_Sortedstatus',ctrl:'DDO_SERVICOPRIORIDADE_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E27KQ2',iparms:[{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtServicoPrioridade_Nome_Link',ctrl:'SERVICOPRIORIDADE_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14KQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoPrioridade_Nome1',fld:'vSERVICOPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoPrioridade_Nome2',fld:'vSERVICOPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoPrioridade_Nome3',fld:'vSERVICOPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV39TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20KQ2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15KQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoPrioridade_Nome1',fld:'vSERVICOPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoPrioridade_Nome2',fld:'vSERVICOPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoPrioridade_Nome3',fld:'vSERVICOPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV39TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoPrioridade_Nome2',fld:'vSERVICOPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoPrioridade_Nome3',fld:'vSERVICOPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoPrioridade_Nome1',fld:'vSERVICOPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServicoprioridade_nome2_Visible',ctrl:'vSERVICOPRIORIDADE_NOME2',prop:'Visible'},{av:'edtavServicoprioridade_nome3_Visible',ctrl:'vSERVICOPRIORIDADE_NOME3',prop:'Visible'},{av:'edtavServicoprioridade_nome1_Visible',ctrl:'vSERVICOPRIORIDADE_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21KQ2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavServicoprioridade_nome1_Visible',ctrl:'vSERVICOPRIORIDADE_NOME1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22KQ2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16KQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoPrioridade_Nome1',fld:'vSERVICOPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoPrioridade_Nome2',fld:'vSERVICOPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoPrioridade_Nome3',fld:'vSERVICOPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV39TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoPrioridade_Nome2',fld:'vSERVICOPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoPrioridade_Nome3',fld:'vSERVICOPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoPrioridade_Nome1',fld:'vSERVICOPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServicoprioridade_nome2_Visible',ctrl:'vSERVICOPRIORIDADE_NOME2',prop:'Visible'},{av:'edtavServicoprioridade_nome3_Visible',ctrl:'vSERVICOPRIORIDADE_NOME3',prop:'Visible'},{av:'edtavServicoprioridade_nome1_Visible',ctrl:'vSERVICOPRIORIDADE_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23KQ2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavServicoprioridade_nome2_Visible',ctrl:'vSERVICOPRIORIDADE_NOME2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E17KQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoPrioridade_Nome1',fld:'vSERVICOPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoPrioridade_Nome2',fld:'vSERVICOPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoPrioridade_Nome3',fld:'vSERVICOPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV39TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoPrioridade_Nome2',fld:'vSERVICOPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoPrioridade_Nome3',fld:'vSERVICOPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoPrioridade_Nome1',fld:'vSERVICOPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServicoprioridade_nome2_Visible',ctrl:'vSERVICOPRIORIDADE_NOME2',prop:'Visible'},{av:'edtavServicoprioridade_nome3_Visible',ctrl:'vSERVICOPRIORIDADE_NOME3',prop:'Visible'},{av:'edtavServicoprioridade_nome1_Visible',ctrl:'vSERVICOPRIORIDADE_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E24KQ2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavServicoprioridade_nome3_Visible',ctrl:'vSERVICOPRIORIDADE_NOME3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E18KQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoPrioridade_Nome1',fld:'vSERVICOPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoPrioridade_Nome2',fld:'vSERVICOPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoPrioridade_Nome3',fld:'vSERVICOPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV39TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV34TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'Ddo_servicoprioridade_nome_Filteredtext_set',ctrl:'DDO_SERVICOPRIORIDADE_NOME',prop:'FilteredText_set'},{av:'AV35TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_servicoprioridade_nome_Selectedvalue_set',ctrl:'DDO_SERVICOPRIORIDADE_NOME',prop:'SelectedValue_set'},{av:'AV38TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'Ddo_servicoprioridade_finalidade_Filteredtext_set',ctrl:'DDO_SERVICOPRIORIDADE_FINALIDADE',prop:'FilteredText_set'},{av:'AV39TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'Ddo_servicoprioridade_finalidade_Selectedvalue_set',ctrl:'DDO_SERVICOPRIORIDADE_FINALIDADE',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ServicoPrioridade_Nome1',fld:'vSERVICOPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavServicoprioridade_nome1_Visible',ctrl:'vSERVICOPRIORIDADE_NOME1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ServicoPrioridade_Nome2',fld:'vSERVICOPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ServicoPrioridade_Nome3',fld:'vSERVICOPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServicoprioridade_nome2_Visible',ctrl:'vSERVICOPRIORIDADE_NOME2',prop:'Visible'},{av:'edtavServicoprioridade_nome3_Visible',ctrl:'vSERVICOPRIORIDADE_NOME3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E19KQ2',iparms:[{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_servicoprioridade_nome_Activeeventkey = "";
         Ddo_servicoprioridade_nome_Filteredtext_get = "";
         Ddo_servicoprioridade_nome_Selectedvalue_get = "";
         Ddo_servicoprioridade_finalidade_Activeeventkey = "";
         Ddo_servicoprioridade_finalidade_Filteredtext_get = "";
         Ddo_servicoprioridade_finalidade_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ServicoPrioridade_Nome1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21ServicoPrioridade_Nome2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25ServicoPrioridade_Nome3 = "";
         AV34TFServicoPrioridade_Nome = "";
         AV35TFServicoPrioridade_Nome_Sel = "";
         AV38TFServicoPrioridade_Finalidade = "";
         AV39TFServicoPrioridade_Finalidade_Sel = "";
         AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace = "";
         AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace = "";
         AV61Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV41DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV33ServicoPrioridade_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37ServicoPrioridade_FinalidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_servicoprioridade_nome_Filteredtext_set = "";
         Ddo_servicoprioridade_nome_Selectedvalue_set = "";
         Ddo_servicoprioridade_nome_Sortedstatus = "";
         Ddo_servicoprioridade_finalidade_Filteredtext_set = "";
         Ddo_servicoprioridade_finalidade_Selectedvalue_set = "";
         Ddo_servicoprioridade_finalidade_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV59Update_GXI = "";
         AV29Delete = "";
         AV60Delete_GXI = "";
         A1441ServicoPrioridade_Nome = "";
         A1442ServicoPrioridade_Finalidade = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 = "";
         lV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 = "";
         lV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 = "";
         lV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome = "";
         lV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade = "";
         AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1 = "";
         AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 = "";
         AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2 = "";
         AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 = "";
         AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3 = "";
         AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 = "";
         AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel = "";
         AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome = "";
         AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel = "";
         AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade = "";
         H00KQ2_A1442ServicoPrioridade_Finalidade = new String[] {""} ;
         H00KQ2_n1442ServicoPrioridade_Finalidade = new bool[] {false} ;
         H00KQ2_A1441ServicoPrioridade_Nome = new String[] {""} ;
         H00KQ2_A1439ServicoPrioridade_SrvCod = new int[1] ;
         H00KQ2_A1440ServicoPrioridade_Codigo = new int[1] ;
         H00KQ3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblServicoprioridadetitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwservicoprioridade__default(),
            new Object[][] {
                new Object[] {
               H00KQ2_A1442ServicoPrioridade_Finalidade, H00KQ2_n1442ServicoPrioridade_Finalidade, H00KQ2_A1441ServicoPrioridade_Nome, H00KQ2_A1439ServicoPrioridade_SrvCod, H00KQ2_A1440ServicoPrioridade_Codigo
               }
               , new Object[] {
               H00KQ3_AGRID_nRecordCount
               }
            }
         );
         AV61Pgmname = "WWServicoPrioridade";
         /* GeneXus formulas. */
         AV61Pgmname = "WWServicoPrioridade";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_73 ;
      private short nGXsfl_73_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_73_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtServicoPrioridade_Nome_Titleformat ;
      private short edtServicoPrioridade_Finalidade_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A1440ServicoPrioridade_Codigo ;
      private int A1439ServicoPrioridade_SrvCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_servicoprioridade_nome_Datalistupdateminimumcharacters ;
      private int Ddo_servicoprioridade_finalidade_Datalistupdateminimumcharacters ;
      private int edtavTfservicoprioridade_nome_Visible ;
      private int edtavTfservicoprioridade_nome_sel_Visible ;
      private int edtavTfservicoprioridade_finalidade_Visible ;
      private int edtavTfservicoprioridade_finalidade_sel_Visible ;
      private int edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV42PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavServicoprioridade_nome1_Visible ;
      private int edtavServicoprioridade_nome2_Visible ;
      private int edtavServicoprioridade_nome3_Visible ;
      private int AV62GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV43GridCurrentPage ;
      private long AV44GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_servicoprioridade_nome_Activeeventkey ;
      private String Ddo_servicoprioridade_nome_Filteredtext_get ;
      private String Ddo_servicoprioridade_nome_Selectedvalue_get ;
      private String Ddo_servicoprioridade_finalidade_Activeeventkey ;
      private String Ddo_servicoprioridade_finalidade_Filteredtext_get ;
      private String Ddo_servicoprioridade_finalidade_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_73_idx="0001" ;
      private String AV17ServicoPrioridade_Nome1 ;
      private String AV21ServicoPrioridade_Nome2 ;
      private String AV25ServicoPrioridade_Nome3 ;
      private String AV34TFServicoPrioridade_Nome ;
      private String AV35TFServicoPrioridade_Nome_Sel ;
      private String AV61Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_servicoprioridade_nome_Caption ;
      private String Ddo_servicoprioridade_nome_Tooltip ;
      private String Ddo_servicoprioridade_nome_Cls ;
      private String Ddo_servicoprioridade_nome_Filteredtext_set ;
      private String Ddo_servicoprioridade_nome_Selectedvalue_set ;
      private String Ddo_servicoprioridade_nome_Dropdownoptionstype ;
      private String Ddo_servicoprioridade_nome_Titlecontrolidtoreplace ;
      private String Ddo_servicoprioridade_nome_Sortedstatus ;
      private String Ddo_servicoprioridade_nome_Filtertype ;
      private String Ddo_servicoprioridade_nome_Datalisttype ;
      private String Ddo_servicoprioridade_nome_Datalistproc ;
      private String Ddo_servicoprioridade_nome_Sortasc ;
      private String Ddo_servicoprioridade_nome_Sortdsc ;
      private String Ddo_servicoprioridade_nome_Loadingdata ;
      private String Ddo_servicoprioridade_nome_Cleanfilter ;
      private String Ddo_servicoprioridade_nome_Noresultsfound ;
      private String Ddo_servicoprioridade_nome_Searchbuttontext ;
      private String Ddo_servicoprioridade_finalidade_Caption ;
      private String Ddo_servicoprioridade_finalidade_Tooltip ;
      private String Ddo_servicoprioridade_finalidade_Cls ;
      private String Ddo_servicoprioridade_finalidade_Filteredtext_set ;
      private String Ddo_servicoprioridade_finalidade_Selectedvalue_set ;
      private String Ddo_servicoprioridade_finalidade_Dropdownoptionstype ;
      private String Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace ;
      private String Ddo_servicoprioridade_finalidade_Sortedstatus ;
      private String Ddo_servicoprioridade_finalidade_Filtertype ;
      private String Ddo_servicoprioridade_finalidade_Datalisttype ;
      private String Ddo_servicoprioridade_finalidade_Datalistproc ;
      private String Ddo_servicoprioridade_finalidade_Sortasc ;
      private String Ddo_servicoprioridade_finalidade_Sortdsc ;
      private String Ddo_servicoprioridade_finalidade_Loadingdata ;
      private String Ddo_servicoprioridade_finalidade_Cleanfilter ;
      private String Ddo_servicoprioridade_finalidade_Noresultsfound ;
      private String Ddo_servicoprioridade_finalidade_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfservicoprioridade_nome_Internalname ;
      private String edtavTfservicoprioridade_nome_Jsonclick ;
      private String edtavTfservicoprioridade_nome_sel_Internalname ;
      private String edtavTfservicoprioridade_nome_sel_Jsonclick ;
      private String edtavTfservicoprioridade_finalidade_Internalname ;
      private String edtavTfservicoprioridade_finalidade_sel_Internalname ;
      private String edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtServicoPrioridade_Codigo_Internalname ;
      private String edtServicoPrioridade_SrvCod_Internalname ;
      private String A1441ServicoPrioridade_Nome ;
      private String edtServicoPrioridade_Nome_Internalname ;
      private String edtServicoPrioridade_Finalidade_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 ;
      private String lV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 ;
      private String lV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 ;
      private String lV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome ;
      private String AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 ;
      private String AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 ;
      private String AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 ;
      private String AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel ;
      private String AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavServicoprioridade_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavServicoprioridade_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavServicoprioridade_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_servicoprioridade_nome_Internalname ;
      private String Ddo_servicoprioridade_finalidade_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtServicoPrioridade_Nome_Title ;
      private String edtServicoPrioridade_Finalidade_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtServicoPrioridade_Nome_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblServicoprioridadetitle_Internalname ;
      private String lblServicoprioridadetitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavServicoprioridade_nome1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavServicoprioridade_nome2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavServicoprioridade_nome3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_73_fel_idx="0001" ;
      private String ROClassString ;
      private String edtServicoPrioridade_Codigo_Jsonclick ;
      private String edtServicoPrioridade_SrvCod_Jsonclick ;
      private String edtServicoPrioridade_Nome_Jsonclick ;
      private String edtServicoPrioridade_Finalidade_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_servicoprioridade_nome_Includesortasc ;
      private bool Ddo_servicoprioridade_nome_Includesortdsc ;
      private bool Ddo_servicoprioridade_nome_Includefilter ;
      private bool Ddo_servicoprioridade_nome_Filterisrange ;
      private bool Ddo_servicoprioridade_nome_Includedatalist ;
      private bool Ddo_servicoprioridade_finalidade_Includesortasc ;
      private bool Ddo_servicoprioridade_finalidade_Includesortdsc ;
      private bool Ddo_servicoprioridade_finalidade_Includefilter ;
      private bool Ddo_servicoprioridade_finalidade_Filterisrange ;
      private bool Ddo_servicoprioridade_finalidade_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1442ServicoPrioridade_Finalidade ;
      private bool AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 ;
      private bool AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String A1442ServicoPrioridade_Finalidade ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV38TFServicoPrioridade_Finalidade ;
      private String AV39TFServicoPrioridade_Finalidade_Sel ;
      private String AV36ddo_ServicoPrioridade_NomeTitleControlIdToReplace ;
      private String AV40ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace ;
      private String AV59Update_GXI ;
      private String AV60Delete_GXI ;
      private String lV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade ;
      private String AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1 ;
      private String AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2 ;
      private String AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3 ;
      private String AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel ;
      private String AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00KQ2_A1442ServicoPrioridade_Finalidade ;
      private bool[] H00KQ2_n1442ServicoPrioridade_Finalidade ;
      private String[] H00KQ2_A1441ServicoPrioridade_Nome ;
      private int[] H00KQ2_A1439ServicoPrioridade_SrvCod ;
      private int[] H00KQ2_A1440ServicoPrioridade_Codigo ;
      private long[] H00KQ3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33ServicoPrioridade_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37ServicoPrioridade_FinalidadeTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV41DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwservicoprioridade__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00KQ2( IGxContext context ,
                                             String AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1 ,
                                             String AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 ,
                                             bool AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 ,
                                             String AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2 ,
                                             String AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 ,
                                             bool AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 ,
                                             String AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3 ,
                                             String AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 ,
                                             String AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel ,
                                             String AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome ,
                                             String AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel ,
                                             String AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade ,
                                             String A1441ServicoPrioridade_Nome ,
                                             String A1442ServicoPrioridade_Finalidade ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ServicoPrioridade_Finalidade], [ServicoPrioridade_Nome], [ServicoPrioridade_SrvCod], [ServicoPrioridade_Codigo]";
         sFromString = " FROM [ServicoPrioridade] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1, "SERVICOPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] like '%' + @lV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Nome] like '%' + @lV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2, "SERVICOPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] like '%' + @lV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Nome] like '%' + @lV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3, "SERVICOPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] like '%' + @lV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Nome] like '%' + @lV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 + '%')";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] like @lV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Nome] like @lV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] = @AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Nome] = @AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Finalidade] like @lV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Finalidade] like @lV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Finalidade] = @AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Finalidade] = @AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoPrioridade_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoPrioridade_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoPrioridade_Finalidade]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoPrioridade_Finalidade] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoPrioridade_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00KQ3( IGxContext context ,
                                             String AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1 ,
                                             String AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 ,
                                             bool AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 ,
                                             String AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2 ,
                                             String AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 ,
                                             bool AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 ,
                                             String AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3 ,
                                             String AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 ,
                                             String AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel ,
                                             String AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome ,
                                             String AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel ,
                                             String AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade ,
                                             String A1441ServicoPrioridade_Nome ,
                                             String A1442ServicoPrioridade_Finalidade ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ServicoPrioridade] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV47WWServicoPrioridadeDS_1_Dynamicfiltersselector1, "SERVICOPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] like '%' + @lV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Nome] like '%' + @lV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( AV49WWServicoPrioridadeDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV50WWServicoPrioridadeDS_4_Dynamicfiltersselector2, "SERVICOPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] like '%' + @lV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Nome] like '%' + @lV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV52WWServicoPrioridadeDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV53WWServicoPrioridadeDS_7_Dynamicfiltersselector3, "SERVICOPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] like '%' + @lV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Nome] like '%' + @lV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] like @lV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Nome] like @lV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] = @AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Nome] = @AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Finalidade] like @lV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Finalidade] like @lV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoPrioridade_Finalidade] = @AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoPrioridade_Finalidade] = @AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00KQ2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] );
               case 1 :
                     return conditional_H00KQ3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KQ2 ;
          prmH00KQ2 = new Object[] {
          new Object[] {"@lV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00KQ3 ;
          prmH00KQ3 = new Object[] {
          new Object[] {"@lV48WWServicoPrioridadeDS_2_Servicoprioridade_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV51WWServicoPrioridadeDS_5_Servicoprioridade_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWServicoPrioridadeDS_8_Servicoprioridade_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWServicoPrioridadeDS_9_Tfservicoprioridade_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56WWServicoPrioridadeDS_10_Tfservicoprioridade_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57WWServicoPrioridadeDS_11_Tfservicoprioridade_finalidade",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV58WWServicoPrioridadeDS_12_Tfservicoprioridade_finalidade_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KQ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KQ2,11,0,true,false )
             ,new CursorDef("H00KQ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KQ3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

}
