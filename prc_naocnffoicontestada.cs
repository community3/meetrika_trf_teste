/*
               File: PRC_NaoCnfFoiContestada
        Description: Nao Cnf Foi Contestada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:13.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_naocnffoicontestada : GXProcedure
   {
      public prc_naocnffoicontestada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_naocnffoicontestada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultadoNaoCnf_OSCod ,
                           out bool aP1_Contestada )
      {
         this.A2020ContagemResultadoNaoCnf_OSCod = aP0_ContagemResultadoNaoCnf_OSCod;
         this.AV8Contestada = false ;
         initialize();
         executePrivate();
         aP0_ContagemResultadoNaoCnf_OSCod=this.A2020ContagemResultadoNaoCnf_OSCod;
         aP1_Contestada=this.AV8Contestada;
      }

      public bool executeUdp( ref int aP0_ContagemResultadoNaoCnf_OSCod )
      {
         this.A2020ContagemResultadoNaoCnf_OSCod = aP0_ContagemResultadoNaoCnf_OSCod;
         this.AV8Contestada = false ;
         initialize();
         executePrivate();
         aP0_ContagemResultadoNaoCnf_OSCod=this.A2020ContagemResultadoNaoCnf_OSCod;
         aP1_Contestada=this.AV8Contestada;
         return AV8Contestada ;
      }

      public void executeSubmit( ref int aP0_ContagemResultadoNaoCnf_OSCod ,
                                 out bool aP1_Contestada )
      {
         prc_naocnffoicontestada objprc_naocnffoicontestada;
         objprc_naocnffoicontestada = new prc_naocnffoicontestada();
         objprc_naocnffoicontestada.A2020ContagemResultadoNaoCnf_OSCod = aP0_ContagemResultadoNaoCnf_OSCod;
         objprc_naocnffoicontestada.AV8Contestada = false ;
         objprc_naocnffoicontestada.context.SetSubmitInitialConfig(context);
         objprc_naocnffoicontestada.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_naocnffoicontestada);
         aP0_ContagemResultadoNaoCnf_OSCod=this.A2020ContagemResultadoNaoCnf_OSCod;
         aP1_Contestada=this.AV8Contestada;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_naocnffoicontestada)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00WT2 */
         pr_default.execute(0, new Object[] {A2020ContagemResultadoNaoCnf_OSCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2057ContagemResultadoNaoCnf_Contestada = P00WT2_A2057ContagemResultadoNaoCnf_Contestada[0];
            n2057ContagemResultadoNaoCnf_Contestada = P00WT2_n2057ContagemResultadoNaoCnf_Contestada[0];
            A2024ContagemResultadoNaoCnf_Codigo = P00WT2_A2024ContagemResultadoNaoCnf_Codigo[0];
            AV8Contestada = A2057ContagemResultadoNaoCnf_Contestada;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00WT2_A2020ContagemResultadoNaoCnf_OSCod = new int[1] ;
         P00WT2_A2057ContagemResultadoNaoCnf_Contestada = new bool[] {false} ;
         P00WT2_n2057ContagemResultadoNaoCnf_Contestada = new bool[] {false} ;
         P00WT2_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_naocnffoicontestada__default(),
            new Object[][] {
                new Object[] {
               P00WT2_A2020ContagemResultadoNaoCnf_OSCod, P00WT2_A2057ContagemResultadoNaoCnf_Contestada, P00WT2_n2057ContagemResultadoNaoCnf_Contestada, P00WT2_A2024ContagemResultadoNaoCnf_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A2020ContagemResultadoNaoCnf_OSCod ;
      private int A2024ContagemResultadoNaoCnf_Codigo ;
      private String scmdbuf ;
      private bool AV8Contestada ;
      private bool A2057ContagemResultadoNaoCnf_Contestada ;
      private bool n2057ContagemResultadoNaoCnf_Contestada ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultadoNaoCnf_OSCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00WT2_A2020ContagemResultadoNaoCnf_OSCod ;
      private bool[] P00WT2_A2057ContagemResultadoNaoCnf_Contestada ;
      private bool[] P00WT2_n2057ContagemResultadoNaoCnf_Contestada ;
      private int[] P00WT2_A2024ContagemResultadoNaoCnf_Codigo ;
      private bool aP1_Contestada ;
   }

   public class prc_naocnffoicontestada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WT2 ;
          prmP00WT2 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_OSCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WT2", "SELECT [ContagemResultadoNaoCnf_OSCod], [ContagemResultadoNaoCnf_Contestada], [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) WHERE [ContagemResultadoNaoCnf_OSCod] = @ContagemResultadoNaoCnf_OSCod ORDER BY [ContagemResultadoNaoCnf_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WT2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
