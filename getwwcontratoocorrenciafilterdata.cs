/*
               File: GetWWContratoOcorrenciaFilterData
        Description: Get WWContrato Ocorrencia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:52:12.48
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratoocorrenciafilterdata : GXProcedure
   {
      public getwwcontratoocorrenciafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratoocorrenciafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratoocorrenciafilterdata objgetwwcontratoocorrenciafilterdata;
         objgetwwcontratoocorrenciafilterdata = new getwwcontratoocorrenciafilterdata();
         objgetwwcontratoocorrenciafilterdata.AV18DDOName = aP0_DDOName;
         objgetwwcontratoocorrenciafilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwcontratoocorrenciafilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratoocorrenciafilterdata.AV22OptionsJson = "" ;
         objgetwwcontratoocorrenciafilterdata.AV25OptionsDescJson = "" ;
         objgetwwcontratoocorrenciafilterdata.AV27OptionIndexesJson = "" ;
         objgetwwcontratoocorrenciafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratoocorrenciafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratoocorrenciafilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratoocorrenciafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOOCORRENCIA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIA_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOOCORRENCIA_NAOCNFCOD") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIA_NAOCNFCODOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWContratoOcorrenciaGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoOcorrenciaGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWContratoOcorrenciaGridState"), "");
         }
         AV50GXV1 = 1;
         while ( AV50GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV50GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_DATA") == 0 )
            {
               AV12TFContratoOcorrencia_Data = context.localUtil.CToD( AV32GridStateFilterValue.gxTpr_Value, 2);
               AV13TFContratoOcorrencia_Data_To = context.localUtil.CToD( AV32GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_DESCRICAO") == 0 )
            {
               AV14TFContratoOcorrencia_Descricao = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_DESCRICAO_SEL") == 0 )
            {
               AV15TFContratoOcorrencia_Descricao_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_NAOCNFCOD") == 0 )
            {
               AV45TFContratoOcorrencia_NaoCnfCod = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_NAOCNFCOD_SEL") == 0 )
            {
               AV47TFContratoOcorrencia_NaoCnfCod_Sel = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            AV50GXV1 = (int)(AV50GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 )
            {
               AV36ContratoOcorrencia_Data1 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Value, 2);
               AV37ContratoOcorrencia_Data_To1 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV38ContratoOcorrencia_Descricao1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV39DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV40DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 )
               {
                  AV42ContratoOcorrencia_Data2 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Value, 2);
                  AV43ContratoOcorrencia_Data_To2 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 )
               {
                  AV41DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV44ContratoOcorrencia_Descricao2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV16SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 = AV36ContratoOcorrencia_Data1;
         AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 = AV37ContratoOcorrencia_Data_To1;
         AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = AV38ContratoOcorrencia_Descricao1;
         AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 = AV39DynamicFiltersEnabled2;
         AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 = AV40DynamicFiltersSelector2;
         AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 = AV41DynamicFiltersOperator2;
         AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 = AV42ContratoOcorrencia_Data2;
         AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 = AV43ContratoOcorrencia_Data_To2;
         AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = AV44ContratoOcorrencia_Descricao2;
         AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero = AV10TFContrato_Numero;
         AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data = AV12TFContratoOcorrencia_Data;
         AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to = AV13TFContratoOcorrencia_Data_To;
         AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = AV14TFContratoOcorrencia_Descricao;
         AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel = AV15TFContratoOcorrencia_Descricao_Sel;
         AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = AV45TFContratoOcorrencia_NaoCnfCod;
         AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel = AV47TFContratoOcorrencia_NaoCnfCod_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 ,
                                              AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 ,
                                              AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 ,
                                              AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 ,
                                              AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ,
                                              AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 ,
                                              AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 ,
                                              AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 ,
                                              AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 ,
                                              AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 ,
                                              AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ,
                                              AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel ,
                                              AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero ,
                                              AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data ,
                                              AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to ,
                                              AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel ,
                                              AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ,
                                              AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel ,
                                              AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ,
                                              A295ContratoOcorrencia_Data ,
                                              A296ContratoOcorrencia_Descricao ,
                                              A77Contrato_Numero ,
                                              A427NaoConformidade_Nome ,
                                              A2027ContratoOcorrencia_NaoCnfCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1), "%", "");
         lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1), "%", "");
         lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2), "%", "");
         lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2), "%", "");
         lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero), 20, "%");
         lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = StringUtil.Concat( StringUtil.RTrim( AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao), "%", "");
         lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = StringUtil.PadR( StringUtil.RTrim( AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod), 50, "%");
         /* Using cursor P00K42 */
         pr_default.execute(0, new Object[] {AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1, AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1, lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1, lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1, AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2, AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2, lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2, lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2, lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero, AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel, AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data, AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to, lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao, AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel, lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod, AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKK42 = false;
            A74Contrato_Codigo = P00K42_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00K42_A77Contrato_Numero[0];
            A2027ContratoOcorrencia_NaoCnfCod = P00K42_A2027ContratoOcorrencia_NaoCnfCod[0];
            n2027ContratoOcorrencia_NaoCnfCod = P00K42_n2027ContratoOcorrencia_NaoCnfCod[0];
            A427NaoConformidade_Nome = P00K42_A427NaoConformidade_Nome[0];
            n427NaoConformidade_Nome = P00K42_n427NaoConformidade_Nome[0];
            A296ContratoOcorrencia_Descricao = P00K42_A296ContratoOcorrencia_Descricao[0];
            A295ContratoOcorrencia_Data = P00K42_A295ContratoOcorrencia_Data[0];
            A294ContratoOcorrencia_Codigo = P00K42_A294ContratoOcorrencia_Codigo[0];
            A77Contrato_Numero = P00K42_A77Contrato_Numero[0];
            A427NaoConformidade_Nome = P00K42_A427NaoConformidade_Nome[0];
            n427NaoConformidade_Nome = P00K42_n427NaoConformidade_Nome[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00K42_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKK42 = false;
               A74Contrato_Codigo = P00K42_A74Contrato_Codigo[0];
               A294ContratoOcorrencia_Codigo = P00K42_A294ContratoOcorrencia_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKK42 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV20Option = A77Contrato_Numero;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK42 )
            {
               BRKK42 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOOCORRENCIA_DESCRICAOOPTIONS' Routine */
         AV14TFContratoOcorrencia_Descricao = AV16SearchTxt;
         AV15TFContratoOcorrencia_Descricao_Sel = "";
         AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 = AV36ContratoOcorrencia_Data1;
         AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 = AV37ContratoOcorrencia_Data_To1;
         AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = AV38ContratoOcorrencia_Descricao1;
         AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 = AV39DynamicFiltersEnabled2;
         AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 = AV40DynamicFiltersSelector2;
         AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 = AV41DynamicFiltersOperator2;
         AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 = AV42ContratoOcorrencia_Data2;
         AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 = AV43ContratoOcorrencia_Data_To2;
         AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = AV44ContratoOcorrencia_Descricao2;
         AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero = AV10TFContrato_Numero;
         AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data = AV12TFContratoOcorrencia_Data;
         AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to = AV13TFContratoOcorrencia_Data_To;
         AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = AV14TFContratoOcorrencia_Descricao;
         AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel = AV15TFContratoOcorrencia_Descricao_Sel;
         AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = AV45TFContratoOcorrencia_NaoCnfCod;
         AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel = AV47TFContratoOcorrencia_NaoCnfCod_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 ,
                                              AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 ,
                                              AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 ,
                                              AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 ,
                                              AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ,
                                              AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 ,
                                              AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 ,
                                              AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 ,
                                              AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 ,
                                              AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 ,
                                              AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ,
                                              AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel ,
                                              AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero ,
                                              AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data ,
                                              AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to ,
                                              AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel ,
                                              AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ,
                                              AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel ,
                                              AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ,
                                              A295ContratoOcorrencia_Data ,
                                              A296ContratoOcorrencia_Descricao ,
                                              A77Contrato_Numero ,
                                              A427NaoConformidade_Nome ,
                                              A2027ContratoOcorrencia_NaoCnfCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1), "%", "");
         lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1), "%", "");
         lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2), "%", "");
         lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2), "%", "");
         lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero), 20, "%");
         lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = StringUtil.Concat( StringUtil.RTrim( AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao), "%", "");
         lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = StringUtil.PadR( StringUtil.RTrim( AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod), 50, "%");
         /* Using cursor P00K43 */
         pr_default.execute(1, new Object[] {AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1, AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1, lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1, lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1, AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2, AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2, lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2, lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2, lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero, AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel, AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data, AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to, lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao, AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel, lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod, AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKK44 = false;
            A74Contrato_Codigo = P00K43_A74Contrato_Codigo[0];
            A296ContratoOcorrencia_Descricao = P00K43_A296ContratoOcorrencia_Descricao[0];
            A2027ContratoOcorrencia_NaoCnfCod = P00K43_A2027ContratoOcorrencia_NaoCnfCod[0];
            n2027ContratoOcorrencia_NaoCnfCod = P00K43_n2027ContratoOcorrencia_NaoCnfCod[0];
            A427NaoConformidade_Nome = P00K43_A427NaoConformidade_Nome[0];
            n427NaoConformidade_Nome = P00K43_n427NaoConformidade_Nome[0];
            A77Contrato_Numero = P00K43_A77Contrato_Numero[0];
            A295ContratoOcorrencia_Data = P00K43_A295ContratoOcorrencia_Data[0];
            A294ContratoOcorrencia_Codigo = P00K43_A294ContratoOcorrencia_Codigo[0];
            A77Contrato_Numero = P00K43_A77Contrato_Numero[0];
            A427NaoConformidade_Nome = P00K43_A427NaoConformidade_Nome[0];
            n427NaoConformidade_Nome = P00K43_n427NaoConformidade_Nome[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00K43_A296ContratoOcorrencia_Descricao[0], A296ContratoOcorrencia_Descricao) == 0 ) )
            {
               BRKK44 = false;
               A294ContratoOcorrencia_Codigo = P00K43_A294ContratoOcorrencia_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKK44 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A296ContratoOcorrencia_Descricao)) )
            {
               AV20Option = A296ContratoOcorrencia_Descricao;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK44 )
            {
               BRKK44 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATOOCORRENCIA_NAOCNFCODOPTIONS' Routine */
         AV45TFContratoOcorrencia_NaoCnfCod = AV16SearchTxt;
         AV47TFContratoOcorrencia_NaoCnfCod_Sel = 0;
         AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 = AV36ContratoOcorrencia_Data1;
         AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 = AV37ContratoOcorrencia_Data_To1;
         AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = AV38ContratoOcorrencia_Descricao1;
         AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 = AV39DynamicFiltersEnabled2;
         AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 = AV40DynamicFiltersSelector2;
         AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 = AV41DynamicFiltersOperator2;
         AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 = AV42ContratoOcorrencia_Data2;
         AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 = AV43ContratoOcorrencia_Data_To2;
         AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = AV44ContratoOcorrencia_Descricao2;
         AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero = AV10TFContrato_Numero;
         AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data = AV12TFContratoOcorrencia_Data;
         AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to = AV13TFContratoOcorrencia_Data_To;
         AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = AV14TFContratoOcorrencia_Descricao;
         AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel = AV15TFContratoOcorrencia_Descricao_Sel;
         AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = AV45TFContratoOcorrencia_NaoCnfCod;
         AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel = AV47TFContratoOcorrencia_NaoCnfCod_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 ,
                                              AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 ,
                                              AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 ,
                                              AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 ,
                                              AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ,
                                              AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 ,
                                              AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 ,
                                              AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 ,
                                              AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 ,
                                              AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 ,
                                              AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ,
                                              AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel ,
                                              AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero ,
                                              AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data ,
                                              AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to ,
                                              AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel ,
                                              AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ,
                                              AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel ,
                                              AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ,
                                              A295ContratoOcorrencia_Data ,
                                              A296ContratoOcorrencia_Descricao ,
                                              A77Contrato_Numero ,
                                              A427NaoConformidade_Nome ,
                                              A2027ContratoOcorrencia_NaoCnfCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1), "%", "");
         lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1), "%", "");
         lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2), "%", "");
         lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2), "%", "");
         lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero), 20, "%");
         lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = StringUtil.Concat( StringUtil.RTrim( AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao), "%", "");
         lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = StringUtil.PadR( StringUtil.RTrim( AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod), 50, "%");
         /* Using cursor P00K44 */
         pr_default.execute(2, new Object[] {AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1, AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1, lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1, lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1, AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2, AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2, lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2, lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2, lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero, AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel, AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data, AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to, lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao, AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel, lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod, AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKK46 = false;
            A74Contrato_Codigo = P00K44_A74Contrato_Codigo[0];
            A2027ContratoOcorrencia_NaoCnfCod = P00K44_A2027ContratoOcorrencia_NaoCnfCod[0];
            n2027ContratoOcorrencia_NaoCnfCod = P00K44_n2027ContratoOcorrencia_NaoCnfCod[0];
            A427NaoConformidade_Nome = P00K44_A427NaoConformidade_Nome[0];
            n427NaoConformidade_Nome = P00K44_n427NaoConformidade_Nome[0];
            A77Contrato_Numero = P00K44_A77Contrato_Numero[0];
            A296ContratoOcorrencia_Descricao = P00K44_A296ContratoOcorrencia_Descricao[0];
            A295ContratoOcorrencia_Data = P00K44_A295ContratoOcorrencia_Data[0];
            A294ContratoOcorrencia_Codigo = P00K44_A294ContratoOcorrencia_Codigo[0];
            A77Contrato_Numero = P00K44_A77Contrato_Numero[0];
            A427NaoConformidade_Nome = P00K44_A427NaoConformidade_Nome[0];
            n427NaoConformidade_Nome = P00K44_n427NaoConformidade_Nome[0];
            AV28count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00K44_A2027ContratoOcorrencia_NaoCnfCod[0] == A2027ContratoOcorrencia_NaoCnfCod ) )
            {
               BRKK46 = false;
               A294ContratoOcorrencia_Codigo = P00K44_A294ContratoOcorrencia_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKK46 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A427NaoConformidade_Nome)) )
            {
               AV20Option = StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0);
               AV23OptionDesc = A427NaoConformidade_Nome;
               AV19InsertIndex = 1;
               while ( ( AV19InsertIndex <= AV21Options.Count ) && ( StringUtil.StrCmp(((String)AV24OptionsDesc.Item(AV19InsertIndex)), AV23OptionDesc) < 0 ) )
               {
                  AV19InsertIndex = (int)(AV19InsertIndex+1);
               }
               AV21Options.Add(AV20Option, AV19InsertIndex);
               AV24OptionsDesc.Add(AV23OptionDesc, AV19InsertIndex);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), AV19InsertIndex);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK46 )
            {
               BRKK46 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContratoOcorrencia_Data = DateTime.MinValue;
         AV13TFContratoOcorrencia_Data_To = DateTime.MinValue;
         AV14TFContratoOcorrencia_Descricao = "";
         AV15TFContratoOcorrencia_Descricao_Sel = "";
         AV45TFContratoOcorrencia_NaoCnfCod = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV36ContratoOcorrencia_Data1 = DateTime.MinValue;
         AV37ContratoOcorrencia_Data_To1 = DateTime.MinValue;
         AV38ContratoOcorrencia_Descricao1 = "";
         AV40DynamicFiltersSelector2 = "";
         AV42ContratoOcorrencia_Data2 = DateTime.MinValue;
         AV43ContratoOcorrencia_Data_To2 = DateTime.MinValue;
         AV44ContratoOcorrencia_Descricao2 = "";
         AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 = "";
         AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 = DateTime.MinValue;
         AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 = DateTime.MinValue;
         AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = "";
         AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 = "";
         AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 = DateTime.MinValue;
         AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 = DateTime.MinValue;
         AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = "";
         AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero = "";
         AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel = "";
         AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data = DateTime.MinValue;
         AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to = DateTime.MinValue;
         AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = "";
         AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel = "";
         AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = "";
         scmdbuf = "";
         lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = "";
         lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = "";
         lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero = "";
         lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = "";
         lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = "";
         A295ContratoOcorrencia_Data = DateTime.MinValue;
         A296ContratoOcorrencia_Descricao = "";
         A77Contrato_Numero = "";
         A427NaoConformidade_Nome = "";
         P00K42_A74Contrato_Codigo = new int[1] ;
         P00K42_A77Contrato_Numero = new String[] {""} ;
         P00K42_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         P00K42_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         P00K42_A427NaoConformidade_Nome = new String[] {""} ;
         P00K42_n427NaoConformidade_Nome = new bool[] {false} ;
         P00K42_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         P00K42_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00K42_A294ContratoOcorrencia_Codigo = new int[1] ;
         AV20Option = "";
         P00K43_A74Contrato_Codigo = new int[1] ;
         P00K43_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         P00K43_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         P00K43_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         P00K43_A427NaoConformidade_Nome = new String[] {""} ;
         P00K43_n427NaoConformidade_Nome = new bool[] {false} ;
         P00K43_A77Contrato_Numero = new String[] {""} ;
         P00K43_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00K43_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00K44_A74Contrato_Codigo = new int[1] ;
         P00K44_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         P00K44_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         P00K44_A427NaoConformidade_Nome = new String[] {""} ;
         P00K44_n427NaoConformidade_Nome = new bool[] {false} ;
         P00K44_A77Contrato_Numero = new String[] {""} ;
         P00K44_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         P00K44_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00K44_A294ContratoOcorrencia_Codigo = new int[1] ;
         AV23OptionDesc = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratoocorrenciafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00K42_A74Contrato_Codigo, P00K42_A77Contrato_Numero, P00K42_A2027ContratoOcorrencia_NaoCnfCod, P00K42_n2027ContratoOcorrencia_NaoCnfCod, P00K42_A427NaoConformidade_Nome, P00K42_n427NaoConformidade_Nome, P00K42_A296ContratoOcorrencia_Descricao, P00K42_A295ContratoOcorrencia_Data, P00K42_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               P00K43_A74Contrato_Codigo, P00K43_A296ContratoOcorrencia_Descricao, P00K43_A2027ContratoOcorrencia_NaoCnfCod, P00K43_n2027ContratoOcorrencia_NaoCnfCod, P00K43_A427NaoConformidade_Nome, P00K43_n427NaoConformidade_Nome, P00K43_A77Contrato_Numero, P00K43_A295ContratoOcorrencia_Data, P00K43_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               P00K44_A74Contrato_Codigo, P00K44_A2027ContratoOcorrencia_NaoCnfCod, P00K44_n2027ContratoOcorrencia_NaoCnfCod, P00K44_A427NaoConformidade_Nome, P00K44_n427NaoConformidade_Nome, P00K44_A77Contrato_Numero, P00K44_A296ContratoOcorrencia_Descricao, P00K44_A295ContratoOcorrencia_Data, P00K44_A294ContratoOcorrencia_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV35DynamicFiltersOperator1 ;
      private short AV41DynamicFiltersOperator2 ;
      private short AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 ;
      private short AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 ;
      private int AV50GXV1 ;
      private int AV47TFContratoOcorrencia_NaoCnfCod_Sel ;
      private int AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel ;
      private int A2027ContratoOcorrencia_NaoCnfCod ;
      private int A74Contrato_Codigo ;
      private int A294ContratoOcorrencia_Codigo ;
      private int AV19InsertIndex ;
      private long AV28count ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String AV45TFContratoOcorrencia_NaoCnfCod ;
      private String AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero ;
      private String AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel ;
      private String AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ;
      private String scmdbuf ;
      private String lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero ;
      private String lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ;
      private String A77Contrato_Numero ;
      private String A427NaoConformidade_Nome ;
      private DateTime AV12TFContratoOcorrencia_Data ;
      private DateTime AV13TFContratoOcorrencia_Data_To ;
      private DateTime AV36ContratoOcorrencia_Data1 ;
      private DateTime AV37ContratoOcorrencia_Data_To1 ;
      private DateTime AV42ContratoOcorrencia_Data2 ;
      private DateTime AV43ContratoOcorrencia_Data_To2 ;
      private DateTime AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 ;
      private DateTime AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 ;
      private DateTime AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 ;
      private DateTime AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 ;
      private DateTime AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data ;
      private DateTime AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to ;
      private DateTime A295ContratoOcorrencia_Data ;
      private bool returnInSub ;
      private bool AV39DynamicFiltersEnabled2 ;
      private bool AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 ;
      private bool BRKK42 ;
      private bool n2027ContratoOcorrencia_NaoCnfCod ;
      private bool n427NaoConformidade_Nome ;
      private bool BRKK44 ;
      private bool BRKK46 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV14TFContratoOcorrencia_Descricao ;
      private String AV15TFContratoOcorrencia_Descricao_Sel ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV38ContratoOcorrencia_Descricao1 ;
      private String AV40DynamicFiltersSelector2 ;
      private String AV44ContratoOcorrencia_Descricao2 ;
      private String AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 ;
      private String AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ;
      private String AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 ;
      private String AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ;
      private String AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ;
      private String AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel ;
      private String lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ;
      private String lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ;
      private String lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ;
      private String A296ContratoOcorrencia_Descricao ;
      private String AV20Option ;
      private String AV23OptionDesc ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00K42_A74Contrato_Codigo ;
      private String[] P00K42_A77Contrato_Numero ;
      private int[] P00K42_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] P00K42_n2027ContratoOcorrencia_NaoCnfCod ;
      private String[] P00K42_A427NaoConformidade_Nome ;
      private bool[] P00K42_n427NaoConformidade_Nome ;
      private String[] P00K42_A296ContratoOcorrencia_Descricao ;
      private DateTime[] P00K42_A295ContratoOcorrencia_Data ;
      private int[] P00K42_A294ContratoOcorrencia_Codigo ;
      private int[] P00K43_A74Contrato_Codigo ;
      private String[] P00K43_A296ContratoOcorrencia_Descricao ;
      private int[] P00K43_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] P00K43_n2027ContratoOcorrencia_NaoCnfCod ;
      private String[] P00K43_A427NaoConformidade_Nome ;
      private bool[] P00K43_n427NaoConformidade_Nome ;
      private String[] P00K43_A77Contrato_Numero ;
      private DateTime[] P00K43_A295ContratoOcorrencia_Data ;
      private int[] P00K43_A294ContratoOcorrencia_Codigo ;
      private int[] P00K44_A74Contrato_Codigo ;
      private int[] P00K44_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] P00K44_n2027ContratoOcorrencia_NaoCnfCod ;
      private String[] P00K44_A427NaoConformidade_Nome ;
      private bool[] P00K44_n427NaoConformidade_Nome ;
      private String[] P00K44_A77Contrato_Numero ;
      private String[] P00K44_A296ContratoOcorrencia_Descricao ;
      private DateTime[] P00K44_A295ContratoOcorrencia_Data ;
      private int[] P00K44_A294ContratoOcorrencia_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getwwcontratoocorrenciafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00K42( IGxContext context ,
                                             String AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 ,
                                             DateTime AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 ,
                                             short AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ,
                                             bool AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 ,
                                             String AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 ,
                                             DateTime AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 ,
                                             short AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 ,
                                             String AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ,
                                             String AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel ,
                                             String AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero ,
                                             DateTime AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data ,
                                             DateTime AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to ,
                                             String AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel ,
                                             String AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ,
                                             int AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel ,
                                             String AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ,
                                             DateTime A295ContratoOcorrencia_Data ,
                                             String A296ContratoOcorrencia_Descricao ,
                                             String A77Contrato_Numero ,
                                             String A427NaoConformidade_Nome ,
                                             int A2027ContratoOcorrencia_NaoCnfCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [16] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contrato_Numero], T1.[ContratoOcorrencia_NaoCnfCod] AS ContratoOcorrencia_NaoCnfCod, T3.[NaoConformidade_Nome], T1.[ContratoOcorrencia_Descricao], T1.[ContratoOcorrencia_Data], T1.[ContratoOcorrencia_Codigo] FROM (([ContratoOcorrencia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN [NaoConformidade] T3 WITH (NOLOCK) ON T3.[NaoConformidade_Codigo] = T1.[ContratoOcorrencia_NaoCnfCod])";
         if ( ( StringUtil.StrCmp(AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] = @AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] = @AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( (0==AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[NaoConformidade_Nome] like @lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[NaoConformidade_Nome] like @lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_NaoCnfCod] = @AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_NaoCnfCod] = @AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00K43( IGxContext context ,
                                             String AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 ,
                                             DateTime AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 ,
                                             short AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ,
                                             bool AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 ,
                                             String AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 ,
                                             DateTime AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 ,
                                             short AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 ,
                                             String AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ,
                                             String AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel ,
                                             String AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero ,
                                             DateTime AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data ,
                                             DateTime AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to ,
                                             String AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel ,
                                             String AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ,
                                             int AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel ,
                                             String AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ,
                                             DateTime A295ContratoOcorrencia_Data ,
                                             String A296ContratoOcorrencia_Descricao ,
                                             String A77Contrato_Numero ,
                                             String A427NaoConformidade_Nome ,
                                             int A2027ContratoOcorrencia_NaoCnfCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [16] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[ContratoOcorrencia_Descricao], T1.[ContratoOcorrencia_NaoCnfCod] AS ContratoOcorrencia_NaoCnfCod, T3.[NaoConformidade_Nome], T2.[Contrato_Numero], T1.[ContratoOcorrencia_Data], T1.[ContratoOcorrencia_Codigo] FROM (([ContratoOcorrencia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN [NaoConformidade] T3 WITH (NOLOCK) ON T3.[NaoConformidade_Codigo] = T1.[ContratoOcorrencia_NaoCnfCod])";
         if ( ( StringUtil.StrCmp(AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] = @AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] = @AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( (0==AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[NaoConformidade_Nome] like @lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[NaoConformidade_Nome] like @lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_NaoCnfCod] = @AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_NaoCnfCod] = @AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoOcorrencia_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00K44( IGxContext context ,
                                             String AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 ,
                                             DateTime AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 ,
                                             short AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ,
                                             bool AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 ,
                                             String AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 ,
                                             DateTime AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 ,
                                             short AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 ,
                                             String AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ,
                                             String AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel ,
                                             String AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero ,
                                             DateTime AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data ,
                                             DateTime AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to ,
                                             String AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel ,
                                             String AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ,
                                             int AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel ,
                                             String AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ,
                                             DateTime A295ContratoOcorrencia_Data ,
                                             String A296ContratoOcorrencia_Descricao ,
                                             String A77Contrato_Numero ,
                                             String A427NaoConformidade_Nome ,
                                             int A2027ContratoOcorrencia_NaoCnfCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [16] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[ContratoOcorrencia_NaoCnfCod] AS ContratoOcorrencia_NaoCnfCod, T3.[NaoConformidade_Nome], T2.[Contrato_Numero], T1.[ContratoOcorrencia_Descricao], T1.[ContratoOcorrencia_Data], T1.[ContratoOcorrencia_Codigo] FROM (([ContratoOcorrencia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN [NaoConformidade] T3 WITH (NOLOCK) ON T3.[NaoConformidade_Codigo] = T1.[ContratoOcorrencia_NaoCnfCod])";
         if ( ( StringUtil.StrCmp(AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV53WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV57WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV59WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratoOcorrenciaDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] = @AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] = @AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( (0==AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[NaoConformidade_Nome] like @lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[NaoConformidade_Nome] like @lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_NaoCnfCod] = @AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_NaoCnfCod] = @AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoOcorrencia_NaoCnfCod]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00K42(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] );
               case 1 :
                     return conditional_P00K43(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] );
               case 2 :
                     return conditional_P00K44(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00K42 ;
          prmP00K42 = new Object[] {
          new Object[] {"@AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod",SqlDbType.Char,50,0} ,
          new Object[] {"@AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00K43 ;
          prmP00K43 = new Object[] {
          new Object[] {"@AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod",SqlDbType.Char,50,0} ,
          new Object[] {"@AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00K44 ;
          prmP00K44 = new Object[] {
          new Object[] {"@AV54WWContratoOcorrenciaDS_3_Contratoocorrencia_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV56WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV60WWContratoOcorrenciaDS_9_Contratoocorrencia_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV61WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV62WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV63WWContratoOcorrenciaDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV64WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV65WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV66WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV67WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV68WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV69WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod",SqlDbType.Char,50,0} ,
          new Object[] {"@AV70WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00K42", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K42,100,0,true,false )
             ,new CursorDef("P00K43", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K43,100,0,true,false )
             ,new CursorDef("P00K44", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K44,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 20) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratoocorrenciafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratoocorrenciafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratoocorrenciafilterdata") )
          {
             return  ;
          }
          getwwcontratoocorrenciafilterdata worker = new getwwcontratoocorrenciafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
