/*
               File: PRC_ClonarServico
        Description: Clonar Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:3:40.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_clonarservico : GXProcedure
   {
      public prc_clonarservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_clonarservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo ,
                           int aP1_Servico_Codigo ,
                           String aP2_Alias )
      {
         this.AV8ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV9Servico_Codigo = aP1_Servico_Codigo;
         this.AV24Alias = aP2_Alias;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContratoServicos_Codigo ,
                                 int aP1_Servico_Codigo ,
                                 String aP2_Alias )
      {
         prc_clonarservico objprc_clonarservico;
         objprc_clonarservico = new prc_clonarservico();
         objprc_clonarservico.AV8ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_clonarservico.AV9Servico_Codigo = aP1_Servico_Codigo;
         objprc_clonarservico.AV24Alias = aP2_Alias;
         objprc_clonarservico.context.SetSubmitInitialConfig(context);
         objprc_clonarservico.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_clonarservico);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_clonarservico)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00992 */
         pr_default.execute(0, new Object[] {AV8ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1858ContratoServicos_Alias = P00992_A1858ContratoServicos_Alias[0];
            n1858ContratoServicos_Alias = P00992_n1858ContratoServicos_Alias[0];
            A155Servico_Codigo = P00992_A155Servico_Codigo[0];
            A2094ContratoServicos_SolicitaGestorSistema = P00992_A2094ContratoServicos_SolicitaGestorSistema[0];
            A2074ContratoServicos_CalculoRmn = P00992_A2074ContratoServicos_CalculoRmn[0];
            n2074ContratoServicos_CalculoRmn = P00992_n2074ContratoServicos_CalculoRmn[0];
            A1538ContratoServicos_PercPgm = P00992_A1538ContratoServicos_PercPgm[0];
            n1538ContratoServicos_PercPgm = P00992_n1538ContratoServicos_PercPgm[0];
            A1799ContratoServicos_LimiteProposta = P00992_A1799ContratoServicos_LimiteProposta[0];
            n1799ContratoServicos_LimiteProposta = P00992_n1799ContratoServicos_LimiteProposta[0];
            A1723ContratoServicos_CodigoFiscal = P00992_A1723ContratoServicos_CodigoFiscal[0];
            n1723ContratoServicos_CodigoFiscal = P00992_n1723ContratoServicos_CodigoFiscal[0];
            A1649ContratoServicos_PrazoInicio = P00992_A1649ContratoServicos_PrazoInicio[0];
            n1649ContratoServicos_PrazoInicio = P00992_n1649ContratoServicos_PrazoInicio[0];
            A1539ContratoServicos_PercCnc = P00992_A1539ContratoServicos_PercCnc[0];
            n1539ContratoServicos_PercCnc = P00992_n1539ContratoServicos_PercCnc[0];
            A1537ContratoServicos_PercTmp = P00992_A1537ContratoServicos_PercTmp[0];
            n1537ContratoServicos_PercTmp = P00992_n1537ContratoServicos_PercTmp[0];
            A1531ContratoServicos_TipoHierarquia = P00992_A1531ContratoServicos_TipoHierarquia[0];
            n1531ContratoServicos_TipoHierarquia = P00992_n1531ContratoServicos_TipoHierarquia[0];
            A1516ContratoServicos_TmpEstAnl = P00992_A1516ContratoServicos_TmpEstAnl[0];
            n1516ContratoServicos_TmpEstAnl = P00992_n1516ContratoServicos_TmpEstAnl[0];
            A1502ContratoServicos_TmpEstCrr = P00992_A1502ContratoServicos_TmpEstCrr[0];
            n1502ContratoServicos_TmpEstCrr = P00992_n1502ContratoServicos_TmpEstCrr[0];
            A1501ContratoServicos_TmpEstExc = P00992_A1501ContratoServicos_TmpEstExc[0];
            n1501ContratoServicos_TmpEstExc = P00992_n1501ContratoServicos_TmpEstExc[0];
            A1455ContratoServicos_IndiceDivergencia = P00992_A1455ContratoServicos_IndiceDivergencia[0];
            n1455ContratoServicos_IndiceDivergencia = P00992_n1455ContratoServicos_IndiceDivergencia[0];
            A1454ContratoServicos_PrazoTpDias = P00992_A1454ContratoServicos_PrazoTpDias[0];
            n1454ContratoServicos_PrazoTpDias = P00992_n1454ContratoServicos_PrazoTpDias[0];
            A1397ContratoServicos_NaoRequerAtr = P00992_A1397ContratoServicos_NaoRequerAtr[0];
            n1397ContratoServicos_NaoRequerAtr = P00992_n1397ContratoServicos_NaoRequerAtr[0];
            A1341ContratoServicos_FatorCnvUndCnt = P00992_A1341ContratoServicos_FatorCnvUndCnt[0];
            n1341ContratoServicos_FatorCnvUndCnt = P00992_n1341ContratoServicos_FatorCnvUndCnt[0];
            A1340ContratoServicos_QntUntCns = P00992_A1340ContratoServicos_QntUntCns[0];
            n1340ContratoServicos_QntUntCns = P00992_n1340ContratoServicos_QntUntCns[0];
            A1325ContratoServicos_StatusPagFnc = P00992_A1325ContratoServicos_StatusPagFnc[0];
            n1325ContratoServicos_StatusPagFnc = P00992_n1325ContratoServicos_StatusPagFnc[0];
            A1266ContratoServicos_Momento = P00992_A1266ContratoServicos_Momento[0];
            n1266ContratoServicos_Momento = P00992_n1266ContratoServicos_Momento[0];
            A1225ContratoServicos_PrazoCorrecaoTipo = P00992_A1225ContratoServicos_PrazoCorrecaoTipo[0];
            n1225ContratoServicos_PrazoCorrecaoTipo = P00992_n1225ContratoServicos_PrazoCorrecaoTipo[0];
            A1224ContratoServicos_PrazoCorrecao = P00992_A1224ContratoServicos_PrazoCorrecao[0];
            n1224ContratoServicos_PrazoCorrecao = P00992_n1224ContratoServicos_PrazoCorrecao[0];
            A1217ContratoServicos_EspelhaAceite = P00992_A1217ContratoServicos_EspelhaAceite[0];
            n1217ContratoServicos_EspelhaAceite = P00992_n1217ContratoServicos_EspelhaAceite[0];
            A1212ContratoServicos_UnidadeContratada = P00992_A1212ContratoServicos_UnidadeContratada[0];
            A1191ContratoServicos_Produtividade = P00992_A1191ContratoServicos_Produtividade[0];
            n1191ContratoServicos_Produtividade = P00992_n1191ContratoServicos_Produtividade[0];
            A1190ContratoServicos_PrazoImediato = P00992_A1190ContratoServicos_PrazoImediato[0];
            n1190ContratoServicos_PrazoImediato = P00992_n1190ContratoServicos_PrazoImediato[0];
            A1182ContratoServicos_PrazoAtendeGarantia = P00992_A1182ContratoServicos_PrazoAtendeGarantia[0];
            n1182ContratoServicos_PrazoAtendeGarantia = P00992_n1182ContratoServicos_PrazoAtendeGarantia[0];
            A1181ContratoServicos_PrazoGarantia = P00992_A1181ContratoServicos_PrazoGarantia[0];
            n1181ContratoServicos_PrazoGarantia = P00992_n1181ContratoServicos_PrazoGarantia[0];
            A1153ContratoServicos_PrazoResposta = P00992_A1153ContratoServicos_PrazoResposta[0];
            n1153ContratoServicos_PrazoResposta = P00992_n1153ContratoServicos_PrazoResposta[0];
            A1152ContratoServicos_PrazoAnalise = P00992_A1152ContratoServicos_PrazoAnalise[0];
            n1152ContratoServicos_PrazoAnalise = P00992_n1152ContratoServicos_PrazoAnalise[0];
            A888ContratoServicos_HmlSemCnf = P00992_A888ContratoServicos_HmlSemCnf[0];
            n888ContratoServicos_HmlSemCnf = P00992_n888ContratoServicos_HmlSemCnf[0];
            A868ContratoServicos_TipoVnc = P00992_A868ContratoServicos_TipoVnc[0];
            n868ContratoServicos_TipoVnc = P00992_n868ContratoServicos_TipoVnc[0];
            A639ContratoServicos_LocalExec = P00992_A639ContratoServicos_LocalExec[0];
            A638ContratoServicos_Ativo = P00992_A638ContratoServicos_Ativo[0];
            A607ServicoContrato_Faturamento = P00992_A607ServicoContrato_Faturamento[0];
            A558Servico_Percentual = P00992_A558Servico_Percentual[0];
            n558Servico_Percentual = P00992_n558Servico_Percentual[0];
            A557Servico_VlrUnidadeContratada = P00992_A557Servico_VlrUnidadeContratada[0];
            A555Servico_QtdContratada = P00992_A555Servico_QtdContratada[0];
            n555Servico_QtdContratada = P00992_n555Servico_QtdContratada[0];
            A74Contrato_Codigo = P00992_A74Contrato_Codigo[0];
            A160ContratoServicos_Codigo = P00992_A160ContratoServicos_Codigo[0];
            W160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            /*
               INSERT RECORD ON TABLE ContratoServicos

            */
            W155Servico_Codigo = A155Servico_Codigo;
            W1858ContratoServicos_Alias = A1858ContratoServicos_Alias;
            n1858ContratoServicos_Alias = false;
            A155Servico_Codigo = AV9Servico_Codigo;
            A1858ContratoServicos_Alias = AV24Alias;
            n1858ContratoServicos_Alias = false;
            /* Using cursor P00993 */
            pr_default.execute(1, new Object[] {A74Contrato_Codigo, A155Servico_Codigo, n555Servico_QtdContratada, A555Servico_QtdContratada, A557Servico_VlrUnidadeContratada, n558Servico_Percentual, A558Servico_Percentual, A607ServicoContrato_Faturamento, A638ContratoServicos_Ativo, A639ContratoServicos_LocalExec, n868ContratoServicos_TipoVnc, A868ContratoServicos_TipoVnc, n888ContratoServicos_HmlSemCnf, A888ContratoServicos_HmlSemCnf, n1152ContratoServicos_PrazoAnalise, A1152ContratoServicos_PrazoAnalise, n1153ContratoServicos_PrazoResposta, A1153ContratoServicos_PrazoResposta, n1181ContratoServicos_PrazoGarantia, A1181ContratoServicos_PrazoGarantia, n1182ContratoServicos_PrazoAtendeGarantia, A1182ContratoServicos_PrazoAtendeGarantia, n1190ContratoServicos_PrazoImediato, A1190ContratoServicos_PrazoImediato, n1191ContratoServicos_Produtividade, A1191ContratoServicos_Produtividade, A1212ContratoServicos_UnidadeContratada, n1217ContratoServicos_EspelhaAceite, A1217ContratoServicos_EspelhaAceite, n1224ContratoServicos_PrazoCorrecao, A1224ContratoServicos_PrazoCorrecao, n1225ContratoServicos_PrazoCorrecaoTipo, A1225ContratoServicos_PrazoCorrecaoTipo, n1266ContratoServicos_Momento, A1266ContratoServicos_Momento, n1325ContratoServicos_StatusPagFnc, A1325ContratoServicos_StatusPagFnc, n1340ContratoServicos_QntUntCns, A1340ContratoServicos_QntUntCns, n1341ContratoServicos_FatorCnvUndCnt, A1341ContratoServicos_FatorCnvUndCnt, n1397ContratoServicos_NaoRequerAtr, A1397ContratoServicos_NaoRequerAtr, n1454ContratoServicos_PrazoTpDias, A1454ContratoServicos_PrazoTpDias, n1455ContratoServicos_IndiceDivergencia, A1455ContratoServicos_IndiceDivergencia, n1501ContratoServicos_TmpEstExc, A1501ContratoServicos_TmpEstExc, n1502ContratoServicos_TmpEstCrr, A1502ContratoServicos_TmpEstCrr, n1516ContratoServicos_TmpEstAnl, A1516ContratoServicos_TmpEstAnl, n1531ContratoServicos_TipoHierarquia, A1531ContratoServicos_TipoHierarquia, n1537ContratoServicos_PercTmp, A1537ContratoServicos_PercTmp, n1539ContratoServicos_PercCnc, A1539ContratoServicos_PercCnc, n1649ContratoServicos_PrazoInicio, A1649ContratoServicos_PrazoInicio, n1723ContratoServicos_CodigoFiscal, A1723ContratoServicos_CodigoFiscal, n1799ContratoServicos_LimiteProposta, A1799ContratoServicos_LimiteProposta, n1538ContratoServicos_PercPgm, A1538ContratoServicos_PercPgm, n1858ContratoServicos_Alias, A1858ContratoServicos_Alias, n2074ContratoServicos_CalculoRmn, A2074ContratoServicos_CalculoRmn, A2094ContratoServicos_SolicitaGestorSistema});
            A160ContratoServicos_Codigo = P00993_A160ContratoServicos_Codigo[0];
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
            if ( (pr_default.getStatus(1) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            A155Servico_Codigo = W155Servico_Codigo;
            A1858ContratoServicos_Alias = W1858ContratoServicos_Alias;
            n1858ContratoServicos_Alias = false;
            /* End Insert */
            AV10NovoContratoServicos_Codigo = A160ContratoServicos_Codigo;
            /* Using cursor P00994 */
            pr_default.execute(2, new Object[] {AV8ContratoServicos_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A926ContratoServicosTelas_ContratoCod = P00994_A926ContratoServicosTelas_ContratoCod[0];
               A934ContratoServicosTelas_TextMouse = P00994_A934ContratoServicosTelas_TextMouse[0];
               A932ContratoServicosTelas_Status = P00994_A932ContratoServicosTelas_Status[0];
               n932ContratoServicosTelas_Status = P00994_n932ContratoServicosTelas_Status[0];
               A929ContratoServicosTelas_Parms = P00994_A929ContratoServicosTelas_Parms[0];
               n929ContratoServicosTelas_Parms = P00994_n929ContratoServicosTelas_Parms[0];
               A928ContratoServicosTelas_Link = P00994_A928ContratoServicosTelas_Link[0];
               A931ContratoServicosTelas_Tela = P00994_A931ContratoServicosTelas_Tela[0];
               A938ContratoServicosTelas_Sequencial = P00994_A938ContratoServicosTelas_Sequencial[0];
               W926ContratoServicosTelas_ContratoCod = A926ContratoServicosTelas_ContratoCod;
               /*
                  INSERT RECORD ON TABLE ContratoServicosTelas

               */
               W926ContratoServicosTelas_ContratoCod = A926ContratoServicosTelas_ContratoCod;
               A926ContratoServicosTelas_ContratoCod = AV10NovoContratoServicos_Codigo;
               /* Using cursor P00995 */
               pr_default.execute(3, new Object[] {A926ContratoServicosTelas_ContratoCod, A938ContratoServicosTelas_Sequencial, A931ContratoServicosTelas_Tela, A928ContratoServicosTelas_Link, n929ContratoServicosTelas_Parms, A929ContratoServicosTelas_Parms, n932ContratoServicosTelas_Status, A932ContratoServicosTelas_Status, A934ContratoServicosTelas_TextMouse});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosTelas") ;
               if ( (pr_default.getStatus(3) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A926ContratoServicosTelas_ContratoCod = W926ContratoServicosTelas_ContratoCod;
               /* End Insert */
               A926ContratoServicosTelas_ContratoCod = W926ContratoServicosTelas_ContratoCod;
               pr_default.readNext(2);
            }
            pr_default.close(2);
            /* Using cursor P00996 */
            pr_default.execute(4, new Object[] {AV8ContratoServicos_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A1270ContratoServicosIndicador_CntSrvCod = P00996_A1270ContratoServicosIndicador_CntSrvCod[0];
               A2052ContratoServicosIndicador_Formato = P00996_A2052ContratoServicosIndicador_Formato[0];
               A2051ContratoServicosIndicador_Sigla = P00996_A2051ContratoServicosIndicador_Sigla[0];
               n2051ContratoServicosIndicador_Sigla = P00996_n2051ContratoServicosIndicador_Sigla[0];
               A1345ContratoServicosIndicador_CalculoSob = P00996_A1345ContratoServicosIndicador_CalculoSob[0];
               n1345ContratoServicosIndicador_CalculoSob = P00996_n1345ContratoServicosIndicador_CalculoSob[0];
               A1310ContratoServicosIndicador_Vigencia = P00996_A1310ContratoServicosIndicador_Vigencia[0];
               n1310ContratoServicosIndicador_Vigencia = P00996_n1310ContratoServicosIndicador_Vigencia[0];
               A1309ContratoServicosIndicador_Periodicidade = P00996_A1309ContratoServicosIndicador_Periodicidade[0];
               n1309ContratoServicosIndicador_Periodicidade = P00996_n1309ContratoServicosIndicador_Periodicidade[0];
               A1308ContratoServicosIndicador_Tipo = P00996_A1308ContratoServicosIndicador_Tipo[0];
               n1308ContratoServicosIndicador_Tipo = P00996_n1308ContratoServicosIndicador_Tipo[0];
               A1307ContratoServicosIndicador_InstrumentoMedicao = P00996_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
               n1307ContratoServicosIndicador_InstrumentoMedicao = P00996_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
               A1306ContratoServicosIndicador_Meta = P00996_A1306ContratoServicosIndicador_Meta[0];
               n1306ContratoServicosIndicador_Meta = P00996_n1306ContratoServicosIndicador_Meta[0];
               A1305ContratoServicosIndicador_Finalidade = P00996_A1305ContratoServicosIndicador_Finalidade[0];
               n1305ContratoServicosIndicador_Finalidade = P00996_n1305ContratoServicosIndicador_Finalidade[0];
               A1274ContratoServicosIndicador_Indicador = P00996_A1274ContratoServicosIndicador_Indicador[0];
               A1271ContratoServicosIndicador_Numero = P00996_A1271ContratoServicosIndicador_Numero[0];
               A1269ContratoServicosIndicador_Codigo = P00996_A1269ContratoServicosIndicador_Codigo[0];
               W1270ContratoServicosIndicador_CntSrvCod = A1270ContratoServicosIndicador_CntSrvCod;
               /*
                  INSERT RECORD ON TABLE ContratoServicosIndicador

               */
               W1270ContratoServicosIndicador_CntSrvCod = A1270ContratoServicosIndicador_CntSrvCod;
               A1270ContratoServicosIndicador_CntSrvCod = AV10NovoContratoServicos_Codigo;
               /* Using cursor P00997 */
               pr_default.execute(5, new Object[] {A1270ContratoServicosIndicador_CntSrvCod, A1271ContratoServicosIndicador_Numero, A1274ContratoServicosIndicador_Indicador, n1305ContratoServicosIndicador_Finalidade, A1305ContratoServicosIndicador_Finalidade, n1306ContratoServicosIndicador_Meta, A1306ContratoServicosIndicador_Meta, n1307ContratoServicosIndicador_InstrumentoMedicao, A1307ContratoServicosIndicador_InstrumentoMedicao, n1308ContratoServicosIndicador_Tipo, A1308ContratoServicosIndicador_Tipo, n1309ContratoServicosIndicador_Periodicidade, A1309ContratoServicosIndicador_Periodicidade, n1310ContratoServicosIndicador_Vigencia, A1310ContratoServicosIndicador_Vigencia, n1345ContratoServicosIndicador_CalculoSob, A1345ContratoServicosIndicador_CalculoSob, n2051ContratoServicosIndicador_Sigla, A2051ContratoServicosIndicador_Sigla, A2052ContratoServicosIndicador_Formato});
               A1269ContratoServicosIndicador_Codigo = P00997_A1269ContratoServicosIndicador_Codigo[0];
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosIndicador") ;
               if ( (pr_default.getStatus(5) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A1270ContratoServicosIndicador_CntSrvCod = W1270ContratoServicosIndicador_CntSrvCod;
               /* End Insert */
               AV16ContratoServicosIndicador_Codigo = A1269ContratoServicosIndicador_Codigo;
               /* Using cursor P00998 */
               pr_default.execute(6, new Object[] {A1269ContratoServicosIndicador_Codigo, A160ContratoServicos_Codigo});
               while ( (pr_default.getStatus(6) != 101) )
               {
                  A1299ContratoServicosIndicadorFaixa_Codigo = P00998_A1299ContratoServicosIndicadorFaixa_Codigo[0];
                  A1304ContratoServicosIndicadorFaixa_Und = P00998_A1304ContratoServicosIndicadorFaixa_Und[0];
                  A1303ContratoServicosIndicadorFaixa_Reduz = P00998_A1303ContratoServicosIndicadorFaixa_Reduz[0];
                  A1302ContratoServicosIndicadorFaixa_Ate = P00998_A1302ContratoServicosIndicadorFaixa_Ate[0];
                  A1301ContratoServicosIndicadorFaixa_Desde = P00998_A1301ContratoServicosIndicadorFaixa_Desde[0];
                  A1300ContratoServicosIndicadorFaixa_Numero = P00998_A1300ContratoServicosIndicadorFaixa_Numero[0];
                  W1269ContratoServicosIndicador_Codigo = A1269ContratoServicosIndicador_Codigo;
                  AV12Sequencial = (short)(A1299ContratoServicosIndicadorFaixa_Codigo);
                  /*
                     INSERT RECORD ON TABLE ContratoServicosIndicadorFaixas

                  */
                  W1269ContratoServicosIndicador_Codigo = A1269ContratoServicosIndicador_Codigo;
                  W1299ContratoServicosIndicadorFaixa_Codigo = A1299ContratoServicosIndicadorFaixa_Codigo;
                  A1269ContratoServicosIndicador_Codigo = AV16ContratoServicosIndicador_Codigo;
                  A1299ContratoServicosIndicadorFaixa_Codigo = AV12Sequencial;
                  /* Using cursor P00999 */
                  pr_default.execute(7, new Object[] {A1269ContratoServicosIndicador_Codigo, A1299ContratoServicosIndicadorFaixa_Codigo, A1300ContratoServicosIndicadorFaixa_Numero, A1301ContratoServicosIndicadorFaixa_Desde, A1302ContratoServicosIndicadorFaixa_Ate, A1303ContratoServicosIndicadorFaixa_Reduz, A1304ContratoServicosIndicadorFaixa_Und});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosIndicadorFaixas") ;
                  if ( (pr_default.getStatus(7) == 1) )
                  {
                     context.Gx_err = 1;
                     Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                  }
                  else
                  {
                     context.Gx_err = 0;
                     Gx_emsg = "";
                  }
                  A1269ContratoServicosIndicador_Codigo = W1269ContratoServicosIndicador_Codigo;
                  A1299ContratoServicosIndicadorFaixa_Codigo = W1299ContratoServicosIndicadorFaixa_Codigo;
                  /* End Insert */
                  A1269ContratoServicosIndicador_Codigo = W1269ContratoServicosIndicador_Codigo;
                  pr_default.readNext(6);
               }
               pr_default.close(6);
               A1270ContratoServicosIndicador_CntSrvCod = W1270ContratoServicosIndicador_CntSrvCod;
               pr_default.readNext(4);
            }
            pr_default.close(4);
            /* Using cursor P009910 */
            pr_default.execute(8, new Object[] {AV8ContratoServicos_Codigo});
            while ( (pr_default.getStatus(8) != 101) )
            {
               A903ContratoServicosPrazo_CntSrvCod = P009910_A903ContratoServicosPrazo_CntSrvCod[0];
               A1823ContratoServicosPrazo_Momento = P009910_A1823ContratoServicosPrazo_Momento[0];
               n1823ContratoServicosPrazo_Momento = P009910_n1823ContratoServicosPrazo_Momento[0];
               A1456ContratoServicosPrazo_Cada = P009910_A1456ContratoServicosPrazo_Cada[0];
               n1456ContratoServicosPrazo_Cada = P009910_n1456ContratoServicosPrazo_Cada[0];
               A1265ContratoServicosPrazo_DiasBaixa = P009910_A1265ContratoServicosPrazo_DiasBaixa[0];
               n1265ContratoServicosPrazo_DiasBaixa = P009910_n1265ContratoServicosPrazo_DiasBaixa[0];
               A1264ContratoServicosPrazo_DiasMedia = P009910_A1264ContratoServicosPrazo_DiasMedia[0];
               n1264ContratoServicosPrazo_DiasMedia = P009910_n1264ContratoServicosPrazo_DiasMedia[0];
               A1263ContratoServicosPrazo_DiasAlta = P009910_A1263ContratoServicosPrazo_DiasAlta[0];
               n1263ContratoServicosPrazo_DiasAlta = P009910_n1263ContratoServicosPrazo_DiasAlta[0];
               A905ContratoServicosPrazo_Dias = P009910_A905ContratoServicosPrazo_Dias[0];
               n905ContratoServicosPrazo_Dias = P009910_n905ContratoServicosPrazo_Dias[0];
               A904ContratoServicosPrazo_Tipo = P009910_A904ContratoServicosPrazo_Tipo[0];
               W903ContratoServicosPrazo_CntSrvCod = A903ContratoServicosPrazo_CntSrvCod;
               /*
                  INSERT RECORD ON TABLE ContratoServicosPrazo

               */
               W903ContratoServicosPrazo_CntSrvCod = A903ContratoServicosPrazo_CntSrvCod;
               A903ContratoServicosPrazo_CntSrvCod = AV10NovoContratoServicos_Codigo;
               /* Using cursor P009911 */
               pr_default.execute(9, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A904ContratoServicosPrazo_Tipo, n905ContratoServicosPrazo_Dias, A905ContratoServicosPrazo_Dias, n1263ContratoServicosPrazo_DiasAlta, A1263ContratoServicosPrazo_DiasAlta, n1264ContratoServicosPrazo_DiasMedia, A1264ContratoServicosPrazo_DiasMedia, n1265ContratoServicosPrazo_DiasBaixa, A1265ContratoServicosPrazo_DiasBaixa, n1456ContratoServicosPrazo_Cada, A1456ContratoServicosPrazo_Cada, n1823ContratoServicosPrazo_Momento, A1823ContratoServicosPrazo_Momento});
               pr_default.close(9);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazo") ;
               if ( (pr_default.getStatus(9) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A903ContratoServicosPrazo_CntSrvCod = W903ContratoServicosPrazo_CntSrvCod;
               /* End Insert */
               /* Using cursor P009912 */
               pr_default.execute(10, new Object[] {AV8ContratoServicos_Codigo});
               while ( (pr_default.getStatus(10) != 101) )
               {
                  A906ContratoServicosPrazoRegra_Sequencial = P009912_A906ContratoServicosPrazoRegra_Sequencial[0];
                  A903ContratoServicosPrazo_CntSrvCod = P009912_A903ContratoServicosPrazo_CntSrvCod[0];
                  A909ContratoServicosPrazoRegra_Dias = P009912_A909ContratoServicosPrazoRegra_Dias[0];
                  A908ContratoServicosPrazoRegra_Fim = P009912_A908ContratoServicosPrazoRegra_Fim[0];
                  A907ContratoServicosPrazoRegra_Inicio = P009912_A907ContratoServicosPrazoRegra_Inicio[0];
                  W903ContratoServicosPrazo_CntSrvCod = A903ContratoServicosPrazo_CntSrvCod;
                  AV12Sequencial = A906ContratoServicosPrazoRegra_Sequencial;
                  /*
                     INSERT RECORD ON TABLE ContratoServicosPrazoContratoServicosPrazoRegra

                  */
                  W903ContratoServicosPrazo_CntSrvCod = A903ContratoServicosPrazo_CntSrvCod;
                  W906ContratoServicosPrazoRegra_Sequencial = A906ContratoServicosPrazoRegra_Sequencial;
                  A903ContratoServicosPrazo_CntSrvCod = AV10NovoContratoServicos_Codigo;
                  A906ContratoServicosPrazoRegra_Sequencial = AV12Sequencial;
                  /* Using cursor P009913 */
                  pr_default.execute(11, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A906ContratoServicosPrazoRegra_Sequencial, A907ContratoServicosPrazoRegra_Inicio, A908ContratoServicosPrazoRegra_Fim, A909ContratoServicosPrazoRegra_Dias});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazoContratoServicosPrazoRegra") ;
                  if ( (pr_default.getStatus(11) == 1) )
                  {
                     context.Gx_err = 1;
                     Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                  }
                  else
                  {
                     context.Gx_err = 0;
                     Gx_emsg = "";
                  }
                  A903ContratoServicosPrazo_CntSrvCod = W903ContratoServicosPrazo_CntSrvCod;
                  A906ContratoServicosPrazoRegra_Sequencial = W906ContratoServicosPrazoRegra_Sequencial;
                  /* End Insert */
                  A903ContratoServicosPrazo_CntSrvCod = W903ContratoServicosPrazo_CntSrvCod;
                  pr_default.readNext(10);
               }
               pr_default.close(10);
               A903ContratoServicosPrazo_CntSrvCod = W903ContratoServicosPrazo_CntSrvCod;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(8);
            /* Using cursor P009914 */
            pr_default.execute(12, new Object[] {AV8ContratoServicos_Codigo});
            while ( (pr_default.getStatus(12) != 101) )
            {
               A1335ContratoServicosPrioridade_CntSrvCod = P009914_A1335ContratoServicosPrioridade_CntSrvCod[0];
               A2067ContratoServicosPrioridade_Peso = P009914_A2067ContratoServicosPrioridade_Peso[0];
               n2067ContratoServicosPrioridade_Peso = P009914_n2067ContratoServicosPrioridade_Peso[0];
               A2066ContratoServicosPrioridade_Ordem = P009914_A2066ContratoServicosPrioridade_Ordem[0];
               n2066ContratoServicosPrioridade_Ordem = P009914_n2066ContratoServicosPrioridade_Ordem[0];
               A1359ContratoServicosPrioridade_Finalidade = P009914_A1359ContratoServicosPrioridade_Finalidade[0];
               n1359ContratoServicosPrioridade_Finalidade = P009914_n1359ContratoServicosPrioridade_Finalidade[0];
               A1339ContratoServicosPrioridade_PercPrazo = P009914_A1339ContratoServicosPrioridade_PercPrazo[0];
               n1339ContratoServicosPrioridade_PercPrazo = P009914_n1339ContratoServicosPrioridade_PercPrazo[0];
               A1338ContratoServicosPrioridade_PercValorB = P009914_A1338ContratoServicosPrioridade_PercValorB[0];
               n1338ContratoServicosPrioridade_PercValorB = P009914_n1338ContratoServicosPrioridade_PercValorB[0];
               A1337ContratoServicosPrioridade_Nome = P009914_A1337ContratoServicosPrioridade_Nome[0];
               A1336ContratoServicosPrioridade_Codigo = P009914_A1336ContratoServicosPrioridade_Codigo[0];
               W1335ContratoServicosPrioridade_CntSrvCod = A1335ContratoServicosPrioridade_CntSrvCod;
               /*
                  INSERT RECORD ON TABLE ContratoServicosPrioridade

               */
               W1335ContratoServicosPrioridade_CntSrvCod = A1335ContratoServicosPrioridade_CntSrvCod;
               A1335ContratoServicosPrioridade_CntSrvCod = AV10NovoContratoServicos_Codigo;
               /* Using cursor P009915 */
               pr_default.execute(13, new Object[] {A1335ContratoServicosPrioridade_CntSrvCod, A1337ContratoServicosPrioridade_Nome, n1338ContratoServicosPrioridade_PercValorB, A1338ContratoServicosPrioridade_PercValorB, n1339ContratoServicosPrioridade_PercPrazo, A1339ContratoServicosPrioridade_PercPrazo, n1359ContratoServicosPrioridade_Finalidade, A1359ContratoServicosPrioridade_Finalidade, n2066ContratoServicosPrioridade_Ordem, A2066ContratoServicosPrioridade_Ordem, n2067ContratoServicosPrioridade_Peso, A2067ContratoServicosPrioridade_Peso});
               A1336ContratoServicosPrioridade_Codigo = P009915_A1336ContratoServicosPrioridade_Codigo[0];
               pr_default.close(13);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
               if ( (pr_default.getStatus(13) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A1335ContratoServicosPrioridade_CntSrvCod = W1335ContratoServicosPrioridade_CntSrvCod;
               /* End Insert */
               AV18ContratoServicosPrioridade_Codigo = A1336ContratoServicosPrioridade_Codigo;
               A1335ContratoServicosPrioridade_CntSrvCod = W1335ContratoServicosPrioridade_CntSrvCod;
               pr_default.readNext(12);
            }
            pr_default.close(12);
            /* Using cursor P009916 */
            pr_default.execute(14, new Object[] {AV8ContratoServicos_Codigo});
            while ( (pr_default.getStatus(14) != 101) )
            {
               A915ContratoSrvVnc_CntSrvCod = P009916_A915ContratoSrvVnc_CntSrvCod[0];
               A2108ContratoServicosVnc_Descricao = P009916_A2108ContratoServicosVnc_Descricao[0];
               n2108ContratoServicosVnc_Descricao = P009916_n2108ContratoServicosVnc_Descricao[0];
               A1821ContratoSrvVnc_SrvVncRef = P009916_A1821ContratoSrvVnc_SrvVncRef[0];
               n1821ContratoSrvVnc_SrvVncRef = P009916_n1821ContratoSrvVnc_SrvVncRef[0];
               A1818ContratoServicosVnc_ClonarLink = P009916_A1818ContratoServicosVnc_ClonarLink[0];
               n1818ContratoServicosVnc_ClonarLink = P009916_n1818ContratoServicosVnc_ClonarLink[0];
               A1801ContratoSrvVnc_NovoRspDmn = P009916_A1801ContratoSrvVnc_NovoRspDmn[0];
               n1801ContratoSrvVnc_NovoRspDmn = P009916_n1801ContratoSrvVnc_NovoRspDmn[0];
               A1800ContratoSrvVnc_DoStatusDmn = P009916_A1800ContratoSrvVnc_DoStatusDmn[0];
               n1800ContratoSrvVnc_DoStatusDmn = P009916_n1800ContratoSrvVnc_DoStatusDmn[0];
               A1743ContratoSrvVnc_NovoStatusDmn = P009916_A1743ContratoSrvVnc_NovoStatusDmn[0];
               n1743ContratoSrvVnc_NovoStatusDmn = P009916_n1743ContratoSrvVnc_NovoStatusDmn[0];
               A1745ContratoSrvVnc_VincularCom = P009916_A1745ContratoSrvVnc_VincularCom[0];
               n1745ContratoSrvVnc_VincularCom = P009916_n1745ContratoSrvVnc_VincularCom[0];
               A1663ContratoSrvVnc_SrvVncStatus = P009916_A1663ContratoSrvVnc_SrvVncStatus[0];
               n1663ContratoSrvVnc_SrvVncStatus = P009916_n1663ContratoSrvVnc_SrvVncStatus[0];
               A1589ContratoSrvVnc_SrvVncCntSrvCod = P009916_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               n1589ContratoSrvVnc_SrvVncCntSrvCod = P009916_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               A1453ContratoServicosVnc_Ativo = P009916_A1453ContratoServicosVnc_Ativo[0];
               n1453ContratoServicosVnc_Ativo = P009916_n1453ContratoServicosVnc_Ativo[0];
               A1438ContratoServicosVnc_Gestor = P009916_A1438ContratoServicosVnc_Gestor[0];
               n1438ContratoServicosVnc_Gestor = P009916_n1438ContratoServicosVnc_Gestor[0];
               A1437ContratoServicosVnc_NaoClonaInfo = P009916_A1437ContratoServicosVnc_NaoClonaInfo[0];
               n1437ContratoServicosVnc_NaoClonaInfo = P009916_n1437ContratoServicosVnc_NaoClonaInfo[0];
               A1145ContratoServicosVnc_ClonaSrvOri = P009916_A1145ContratoServicosVnc_ClonaSrvOri[0];
               n1145ContratoServicosVnc_ClonaSrvOri = P009916_n1145ContratoServicosVnc_ClonaSrvOri[0];
               A1090ContratoSrvVnc_SemCusto = P009916_A1090ContratoSrvVnc_SemCusto[0];
               n1090ContratoSrvVnc_SemCusto = P009916_n1090ContratoSrvVnc_SemCusto[0];
               A1088ContratoSrvVnc_PrestadoraCod = P009916_A1088ContratoSrvVnc_PrestadoraCod[0];
               n1088ContratoSrvVnc_PrestadoraCod = P009916_n1088ContratoSrvVnc_PrestadoraCod[0];
               A1084ContratoSrvVnc_StatusDmn = P009916_A1084ContratoSrvVnc_StatusDmn[0];
               n1084ContratoSrvVnc_StatusDmn = P009916_n1084ContratoSrvVnc_StatusDmn[0];
               A917ContratoSrvVnc_Codigo = P009916_A917ContratoSrvVnc_Codigo[0];
               W915ContratoSrvVnc_CntSrvCod = A915ContratoSrvVnc_CntSrvCod;
               /*
                  INSERT RECORD ON TABLE ContratoServicosVnc

               */
               W915ContratoSrvVnc_CntSrvCod = A915ContratoSrvVnc_CntSrvCod;
               A915ContratoSrvVnc_CntSrvCod = AV10NovoContratoServicos_Codigo;
               /* Using cursor P009917 */
               pr_default.execute(15, new Object[] {A915ContratoSrvVnc_CntSrvCod, n1084ContratoSrvVnc_StatusDmn, A1084ContratoSrvVnc_StatusDmn, n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod, n1090ContratoSrvVnc_SemCusto, A1090ContratoSrvVnc_SemCusto, n1145ContratoServicosVnc_ClonaSrvOri, A1145ContratoServicosVnc_ClonaSrvOri, n1437ContratoServicosVnc_NaoClonaInfo, A1437ContratoServicosVnc_NaoClonaInfo, n1438ContratoServicosVnc_Gestor, A1438ContratoServicosVnc_Gestor, n1453ContratoServicosVnc_Ativo, A1453ContratoServicosVnc_Ativo, n1589ContratoSrvVnc_SrvVncCntSrvCod, A1589ContratoSrvVnc_SrvVncCntSrvCod, n1663ContratoSrvVnc_SrvVncStatus, A1663ContratoSrvVnc_SrvVncStatus, n1745ContratoSrvVnc_VincularCom, A1745ContratoSrvVnc_VincularCom, n1743ContratoSrvVnc_NovoStatusDmn, A1743ContratoSrvVnc_NovoStatusDmn, n1800ContratoSrvVnc_DoStatusDmn, A1800ContratoSrvVnc_DoStatusDmn, n1801ContratoSrvVnc_NovoRspDmn, A1801ContratoSrvVnc_NovoRspDmn, n1818ContratoServicosVnc_ClonarLink, A1818ContratoServicosVnc_ClonarLink, n1821ContratoSrvVnc_SrvVncRef, A1821ContratoSrvVnc_SrvVncRef, n2108ContratoServicosVnc_Descricao, A2108ContratoServicosVnc_Descricao});
               A917ContratoSrvVnc_Codigo = P009917_A917ContratoSrvVnc_Codigo[0];
               pr_default.close(15);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosVnc") ;
               if ( (pr_default.getStatus(15) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A915ContratoSrvVnc_CntSrvCod = W915ContratoSrvVnc_CntSrvCod;
               /* End Insert */
               AV19ContratoSrvVnc_CntSrvCod = A915ContratoSrvVnc_CntSrvCod;
               A915ContratoSrvVnc_CntSrvCod = W915ContratoSrvVnc_CntSrvCod;
               pr_default.readNext(14);
            }
            pr_default.close(14);
            /* Using cursor P009918 */
            pr_default.execute(16, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(16) != 101) )
            {
               A1063ContratoServicosSistemas_SistemaCod = P009918_A1063ContratoServicosSistemas_SistemaCod[0];
               A1067ContratoServicosSistemas_ServicoCod = P009918_A1067ContratoServicosSistemas_ServicoCod[0];
               AV23Sistemas.Add(A1063ContratoServicosSistemas_SistemaCod, 0);
               pr_default.readNext(16);
            }
            pr_default.close(16);
            /* Using cursor P009919 */
            pr_default.execute(17, new Object[] {AV8ContratoServicos_Codigo});
            while ( (pr_default.getStatus(17) != 101) )
            {
               A2069ContratoServicosRmn_Sequencial = P009919_A2069ContratoServicosRmn_Sequencial[0];
               A160ContratoServicos_Codigo = P009919_A160ContratoServicos_Codigo[0];
               A2072ContratoServicosRmn_Valor = P009919_A2072ContratoServicosRmn_Valor[0];
               A2071ContratoServicosRmn_Fim = P009919_A2071ContratoServicosRmn_Fim[0];
               A2070ContratoServicosRmn_Inicio = P009919_A2070ContratoServicosRmn_Inicio[0];
               W160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
               AV12Sequencial = A2069ContratoServicosRmn_Sequencial;
               /*
                  INSERT RECORD ON TABLE ContratoServicosRmn

               */
               W160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
               W2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
               A160ContratoServicos_Codigo = AV10NovoContratoServicos_Codigo;
               A2069ContratoServicosRmn_Sequencial = AV12Sequencial;
               /* Using cursor P009920 */
               pr_default.execute(18, new Object[] {A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial, A2070ContratoServicosRmn_Inicio, A2071ContratoServicosRmn_Fim, A2072ContratoServicosRmn_Valor});
               pr_default.close(18);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosRmn") ;
               if ( (pr_default.getStatus(18) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A160ContratoServicos_Codigo = W160ContratoServicos_Codigo;
               A2069ContratoServicosRmn_Sequencial = W2069ContratoServicosRmn_Sequencial;
               /* End Insert */
               A160ContratoServicos_Codigo = W160ContratoServicos_Codigo;
               pr_default.readNext(17);
            }
            pr_default.close(17);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            A160ContratoServicos_Codigo = W160ContratoServicos_Codigo;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         AV37GXV1 = 1;
         while ( AV37GXV1 <= AV23Sistemas.Count )
         {
            AV22Codigo = (int)(AV23Sistemas.GetNumeric(AV37GXV1));
            /*
               INSERT RECORD ON TABLE ContratoServicosSistemas

            */
            A160ContratoServicos_Codigo = AV10NovoContratoServicos_Codigo;
            A1067ContratoServicosSistemas_ServicoCod = AV9Servico_Codigo;
            A1063ContratoServicosSistemas_SistemaCod = AV22Codigo;
            /* Using cursor P009921 */
            pr_default.execute(19, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
            pr_default.close(19);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosSistemas") ;
            if ( (pr_default.getStatus(19) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            AV37GXV1 = (int)(AV37GXV1+1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ClonarServico");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00992_A1858ContratoServicos_Alias = new String[] {""} ;
         P00992_n1858ContratoServicos_Alias = new bool[] {false} ;
         P00992_A155Servico_Codigo = new int[1] ;
         P00992_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         P00992_A2074ContratoServicos_CalculoRmn = new short[1] ;
         P00992_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         P00992_A1538ContratoServicos_PercPgm = new short[1] ;
         P00992_n1538ContratoServicos_PercPgm = new bool[] {false} ;
         P00992_A1799ContratoServicos_LimiteProposta = new decimal[1] ;
         P00992_n1799ContratoServicos_LimiteProposta = new bool[] {false} ;
         P00992_A1723ContratoServicos_CodigoFiscal = new String[] {""} ;
         P00992_n1723ContratoServicos_CodigoFiscal = new bool[] {false} ;
         P00992_A1649ContratoServicos_PrazoInicio = new short[1] ;
         P00992_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         P00992_A1539ContratoServicos_PercCnc = new short[1] ;
         P00992_n1539ContratoServicos_PercCnc = new bool[] {false} ;
         P00992_A1537ContratoServicos_PercTmp = new short[1] ;
         P00992_n1537ContratoServicos_PercTmp = new bool[] {false} ;
         P00992_A1531ContratoServicos_TipoHierarquia = new short[1] ;
         P00992_n1531ContratoServicos_TipoHierarquia = new bool[] {false} ;
         P00992_A1516ContratoServicos_TmpEstAnl = new int[1] ;
         P00992_n1516ContratoServicos_TmpEstAnl = new bool[] {false} ;
         P00992_A1502ContratoServicos_TmpEstCrr = new int[1] ;
         P00992_n1502ContratoServicos_TmpEstCrr = new bool[] {false} ;
         P00992_A1501ContratoServicos_TmpEstExc = new int[1] ;
         P00992_n1501ContratoServicos_TmpEstExc = new bool[] {false} ;
         P00992_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         P00992_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         P00992_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P00992_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         P00992_A1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         P00992_n1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         P00992_A1341ContratoServicos_FatorCnvUndCnt = new decimal[1] ;
         P00992_n1341ContratoServicos_FatorCnvUndCnt = new bool[] {false} ;
         P00992_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         P00992_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         P00992_A1325ContratoServicos_StatusPagFnc = new String[] {""} ;
         P00992_n1325ContratoServicos_StatusPagFnc = new bool[] {false} ;
         P00992_A1266ContratoServicos_Momento = new String[] {""} ;
         P00992_n1266ContratoServicos_Momento = new bool[] {false} ;
         P00992_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         P00992_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         P00992_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         P00992_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         P00992_A1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         P00992_n1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         P00992_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         P00992_A1191ContratoServicos_Produtividade = new decimal[1] ;
         P00992_n1191ContratoServicos_Produtividade = new bool[] {false} ;
         P00992_A1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         P00992_n1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         P00992_A1182ContratoServicos_PrazoAtendeGarantia = new short[1] ;
         P00992_n1182ContratoServicos_PrazoAtendeGarantia = new bool[] {false} ;
         P00992_A1181ContratoServicos_PrazoGarantia = new short[1] ;
         P00992_n1181ContratoServicos_PrazoGarantia = new bool[] {false} ;
         P00992_A1153ContratoServicos_PrazoResposta = new short[1] ;
         P00992_n1153ContratoServicos_PrazoResposta = new bool[] {false} ;
         P00992_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         P00992_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         P00992_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00992_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00992_A868ContratoServicos_TipoVnc = new String[] {""} ;
         P00992_n868ContratoServicos_TipoVnc = new bool[] {false} ;
         P00992_A639ContratoServicos_LocalExec = new String[] {""} ;
         P00992_A638ContratoServicos_Ativo = new bool[] {false} ;
         P00992_A607ServicoContrato_Faturamento = new String[] {""} ;
         P00992_A558Servico_Percentual = new decimal[1] ;
         P00992_n558Servico_Percentual = new bool[] {false} ;
         P00992_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         P00992_A555Servico_QtdContratada = new long[1] ;
         P00992_n555Servico_QtdContratada = new bool[] {false} ;
         P00992_A74Contrato_Codigo = new int[1] ;
         P00992_A160ContratoServicos_Codigo = new int[1] ;
         A1858ContratoServicos_Alias = "";
         A1723ContratoServicos_CodigoFiscal = "";
         A1454ContratoServicos_PrazoTpDias = "";
         A1325ContratoServicos_StatusPagFnc = "";
         A1266ContratoServicos_Momento = "";
         A1225ContratoServicos_PrazoCorrecaoTipo = "";
         A868ContratoServicos_TipoVnc = "";
         A639ContratoServicos_LocalExec = "";
         A607ServicoContrato_Faturamento = "";
         W1858ContratoServicos_Alias = "";
         P00993_A160ContratoServicos_Codigo = new int[1] ;
         Gx_emsg = "";
         P00994_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00994_A934ContratoServicosTelas_TextMouse = new String[] {""} ;
         P00994_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00994_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00994_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00994_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00994_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00994_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00994_A938ContratoServicosTelas_Sequencial = new short[1] ;
         A934ContratoServicosTelas_TextMouse = "";
         A932ContratoServicosTelas_Status = "";
         A929ContratoServicosTelas_Parms = "";
         A928ContratoServicosTelas_Link = "";
         A931ContratoServicosTelas_Tela = "";
         P00996_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P00996_A2052ContratoServicosIndicador_Formato = new short[1] ;
         P00996_A2051ContratoServicosIndicador_Sigla = new String[] {""} ;
         P00996_n2051ContratoServicosIndicador_Sigla = new bool[] {false} ;
         P00996_A1345ContratoServicosIndicador_CalculoSob = new String[] {""} ;
         P00996_n1345ContratoServicosIndicador_CalculoSob = new bool[] {false} ;
         P00996_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         P00996_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         P00996_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         P00996_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         P00996_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P00996_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P00996_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         P00996_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         P00996_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         P00996_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         P00996_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         P00996_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         P00996_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         P00996_A1271ContratoServicosIndicador_Numero = new short[1] ;
         P00996_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         A2051ContratoServicosIndicador_Sigla = "";
         A1345ContratoServicosIndicador_CalculoSob = "";
         A1310ContratoServicosIndicador_Vigencia = "";
         A1309ContratoServicosIndicador_Periodicidade = "";
         A1308ContratoServicosIndicador_Tipo = "";
         A1307ContratoServicosIndicador_InstrumentoMedicao = "";
         A1306ContratoServicosIndicador_Meta = "";
         A1305ContratoServicosIndicador_Finalidade = "";
         A1274ContratoServicosIndicador_Indicador = "";
         P00997_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00998_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00998_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         P00998_A1304ContratoServicosIndicadorFaixa_Und = new short[1] ;
         P00998_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         P00998_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         P00998_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         P00998_A1300ContratoServicosIndicadorFaixa_Numero = new short[1] ;
         P009910_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P009910_A1823ContratoServicosPrazo_Momento = new short[1] ;
         P009910_n1823ContratoServicosPrazo_Momento = new bool[] {false} ;
         P009910_A1456ContratoServicosPrazo_Cada = new decimal[1] ;
         P009910_n1456ContratoServicosPrazo_Cada = new bool[] {false} ;
         P009910_A1265ContratoServicosPrazo_DiasBaixa = new short[1] ;
         P009910_n1265ContratoServicosPrazo_DiasBaixa = new bool[] {false} ;
         P009910_A1264ContratoServicosPrazo_DiasMedia = new short[1] ;
         P009910_n1264ContratoServicosPrazo_DiasMedia = new bool[] {false} ;
         P009910_A1263ContratoServicosPrazo_DiasAlta = new short[1] ;
         P009910_n1263ContratoServicosPrazo_DiasAlta = new bool[] {false} ;
         P009910_A905ContratoServicosPrazo_Dias = new short[1] ;
         P009910_n905ContratoServicosPrazo_Dias = new bool[] {false} ;
         P009910_A904ContratoServicosPrazo_Tipo = new String[] {""} ;
         A904ContratoServicosPrazo_Tipo = "";
         P009912_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         P009912_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P009912_A909ContratoServicosPrazoRegra_Dias = new short[1] ;
         P009912_A908ContratoServicosPrazoRegra_Fim = new decimal[1] ;
         P009912_A907ContratoServicosPrazoRegra_Inicio = new decimal[1] ;
         P009914_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         P009914_A2067ContratoServicosPrioridade_Peso = new short[1] ;
         P009914_n2067ContratoServicosPrioridade_Peso = new bool[] {false} ;
         P009914_A2066ContratoServicosPrioridade_Ordem = new short[1] ;
         P009914_n2066ContratoServicosPrioridade_Ordem = new bool[] {false} ;
         P009914_A1359ContratoServicosPrioridade_Finalidade = new String[] {""} ;
         P009914_n1359ContratoServicosPrioridade_Finalidade = new bool[] {false} ;
         P009914_A1339ContratoServicosPrioridade_PercPrazo = new decimal[1] ;
         P009914_n1339ContratoServicosPrioridade_PercPrazo = new bool[] {false} ;
         P009914_A1338ContratoServicosPrioridade_PercValorB = new decimal[1] ;
         P009914_n1338ContratoServicosPrioridade_PercValorB = new bool[] {false} ;
         P009914_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         P009914_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         A1359ContratoServicosPrioridade_Finalidade = "";
         A1337ContratoServicosPrioridade_Nome = "";
         P009915_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         P009916_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P009916_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         P009916_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         P009916_A1821ContratoSrvVnc_SrvVncRef = new short[1] ;
         P009916_n1821ContratoSrvVnc_SrvVncRef = new bool[] {false} ;
         P009916_A1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P009916_n1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P009916_A1801ContratoSrvVnc_NovoRspDmn = new int[1] ;
         P009916_n1801ContratoSrvVnc_NovoRspDmn = new bool[] {false} ;
         P009916_A1800ContratoSrvVnc_DoStatusDmn = new String[] {""} ;
         P009916_n1800ContratoSrvVnc_DoStatusDmn = new bool[] {false} ;
         P009916_A1743ContratoSrvVnc_NovoStatusDmn = new String[] {""} ;
         P009916_n1743ContratoSrvVnc_NovoStatusDmn = new bool[] {false} ;
         P009916_A1745ContratoSrvVnc_VincularCom = new String[] {""} ;
         P009916_n1745ContratoSrvVnc_VincularCom = new bool[] {false} ;
         P009916_A1663ContratoSrvVnc_SrvVncStatus = new String[] {""} ;
         P009916_n1663ContratoSrvVnc_SrvVncStatus = new bool[] {false} ;
         P009916_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         P009916_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         P009916_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P009916_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P009916_A1438ContratoServicosVnc_Gestor = new int[1] ;
         P009916_n1438ContratoServicosVnc_Gestor = new bool[] {false} ;
         P009916_A1437ContratoServicosVnc_NaoClonaInfo = new bool[] {false} ;
         P009916_n1437ContratoServicosVnc_NaoClonaInfo = new bool[] {false} ;
         P009916_A1145ContratoServicosVnc_ClonaSrvOri = new bool[] {false} ;
         P009916_n1145ContratoServicosVnc_ClonaSrvOri = new bool[] {false} ;
         P009916_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P009916_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P009916_A1088ContratoSrvVnc_PrestadoraCod = new int[1] ;
         P009916_n1088ContratoSrvVnc_PrestadoraCod = new bool[] {false} ;
         P009916_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         P009916_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         P009916_A917ContratoSrvVnc_Codigo = new int[1] ;
         A2108ContratoServicosVnc_Descricao = "";
         A1800ContratoSrvVnc_DoStatusDmn = "";
         A1743ContratoSrvVnc_NovoStatusDmn = "";
         A1745ContratoSrvVnc_VincularCom = "";
         A1663ContratoSrvVnc_SrvVncStatus = "";
         A1084ContratoSrvVnc_StatusDmn = "";
         P009917_A917ContratoSrvVnc_Codigo = new int[1] ;
         P009918_A160ContratoServicos_Codigo = new int[1] ;
         P009918_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         P009918_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         AV23Sistemas = new GxSimpleCollection();
         P009919_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         P009919_A160ContratoServicos_Codigo = new int[1] ;
         P009919_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         P009919_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         P009919_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_clonarservico__default(),
            new Object[][] {
                new Object[] {
               P00992_A1858ContratoServicos_Alias, P00992_n1858ContratoServicos_Alias, P00992_A155Servico_Codigo, P00992_A2094ContratoServicos_SolicitaGestorSistema, P00992_A2074ContratoServicos_CalculoRmn, P00992_n2074ContratoServicos_CalculoRmn, P00992_A1538ContratoServicos_PercPgm, P00992_n1538ContratoServicos_PercPgm, P00992_A1799ContratoServicos_LimiteProposta, P00992_n1799ContratoServicos_LimiteProposta,
               P00992_A1723ContratoServicos_CodigoFiscal, P00992_n1723ContratoServicos_CodigoFiscal, P00992_A1649ContratoServicos_PrazoInicio, P00992_n1649ContratoServicos_PrazoInicio, P00992_A1539ContratoServicos_PercCnc, P00992_n1539ContratoServicos_PercCnc, P00992_A1537ContratoServicos_PercTmp, P00992_n1537ContratoServicos_PercTmp, P00992_A1531ContratoServicos_TipoHierarquia, P00992_n1531ContratoServicos_TipoHierarquia,
               P00992_A1516ContratoServicos_TmpEstAnl, P00992_n1516ContratoServicos_TmpEstAnl, P00992_A1502ContratoServicos_TmpEstCrr, P00992_n1502ContratoServicos_TmpEstCrr, P00992_A1501ContratoServicos_TmpEstExc, P00992_n1501ContratoServicos_TmpEstExc, P00992_A1455ContratoServicos_IndiceDivergencia, P00992_n1455ContratoServicos_IndiceDivergencia, P00992_A1454ContratoServicos_PrazoTpDias, P00992_n1454ContratoServicos_PrazoTpDias,
               P00992_A1397ContratoServicos_NaoRequerAtr, P00992_n1397ContratoServicos_NaoRequerAtr, P00992_A1341ContratoServicos_FatorCnvUndCnt, P00992_n1341ContratoServicos_FatorCnvUndCnt, P00992_A1340ContratoServicos_QntUntCns, P00992_n1340ContratoServicos_QntUntCns, P00992_A1325ContratoServicos_StatusPagFnc, P00992_n1325ContratoServicos_StatusPagFnc, P00992_A1266ContratoServicos_Momento, P00992_n1266ContratoServicos_Momento,
               P00992_A1225ContratoServicos_PrazoCorrecaoTipo, P00992_n1225ContratoServicos_PrazoCorrecaoTipo, P00992_A1224ContratoServicos_PrazoCorrecao, P00992_n1224ContratoServicos_PrazoCorrecao, P00992_A1217ContratoServicos_EspelhaAceite, P00992_n1217ContratoServicos_EspelhaAceite, P00992_A1212ContratoServicos_UnidadeContratada, P00992_A1191ContratoServicos_Produtividade, P00992_n1191ContratoServicos_Produtividade, P00992_A1190ContratoServicos_PrazoImediato,
               P00992_n1190ContratoServicos_PrazoImediato, P00992_A1182ContratoServicos_PrazoAtendeGarantia, P00992_n1182ContratoServicos_PrazoAtendeGarantia, P00992_A1181ContratoServicos_PrazoGarantia, P00992_n1181ContratoServicos_PrazoGarantia, P00992_A1153ContratoServicos_PrazoResposta, P00992_n1153ContratoServicos_PrazoResposta, P00992_A1152ContratoServicos_PrazoAnalise, P00992_n1152ContratoServicos_PrazoAnalise, P00992_A888ContratoServicos_HmlSemCnf,
               P00992_n888ContratoServicos_HmlSemCnf, P00992_A868ContratoServicos_TipoVnc, P00992_n868ContratoServicos_TipoVnc, P00992_A639ContratoServicos_LocalExec, P00992_A638ContratoServicos_Ativo, P00992_A607ServicoContrato_Faturamento, P00992_A558Servico_Percentual, P00992_n558Servico_Percentual, P00992_A557Servico_VlrUnidadeContratada, P00992_A555Servico_QtdContratada,
               P00992_n555Servico_QtdContratada, P00992_A74Contrato_Codigo, P00992_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P00993_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P00994_A926ContratoServicosTelas_ContratoCod, P00994_A934ContratoServicosTelas_TextMouse, P00994_A932ContratoServicosTelas_Status, P00994_n932ContratoServicosTelas_Status, P00994_A929ContratoServicosTelas_Parms, P00994_n929ContratoServicosTelas_Parms, P00994_A928ContratoServicosTelas_Link, P00994_A931ContratoServicosTelas_Tela, P00994_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               }
               , new Object[] {
               P00996_A1270ContratoServicosIndicador_CntSrvCod, P00996_A2052ContratoServicosIndicador_Formato, P00996_A2051ContratoServicosIndicador_Sigla, P00996_n2051ContratoServicosIndicador_Sigla, P00996_A1345ContratoServicosIndicador_CalculoSob, P00996_n1345ContratoServicosIndicador_CalculoSob, P00996_A1310ContratoServicosIndicador_Vigencia, P00996_n1310ContratoServicosIndicador_Vigencia, P00996_A1309ContratoServicosIndicador_Periodicidade, P00996_n1309ContratoServicosIndicador_Periodicidade,
               P00996_A1308ContratoServicosIndicador_Tipo, P00996_n1308ContratoServicosIndicador_Tipo, P00996_A1307ContratoServicosIndicador_InstrumentoMedicao, P00996_n1307ContratoServicosIndicador_InstrumentoMedicao, P00996_A1306ContratoServicosIndicador_Meta, P00996_n1306ContratoServicosIndicador_Meta, P00996_A1305ContratoServicosIndicador_Finalidade, P00996_n1305ContratoServicosIndicador_Finalidade, P00996_A1274ContratoServicosIndicador_Indicador, P00996_A1271ContratoServicosIndicador_Numero,
               P00996_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               P00997_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               P00998_A1269ContratoServicosIndicador_Codigo, P00998_A1299ContratoServicosIndicadorFaixa_Codigo, P00998_A1304ContratoServicosIndicadorFaixa_Und, P00998_A1303ContratoServicosIndicadorFaixa_Reduz, P00998_A1302ContratoServicosIndicadorFaixa_Ate, P00998_A1301ContratoServicosIndicadorFaixa_Desde, P00998_A1300ContratoServicosIndicadorFaixa_Numero
               }
               , new Object[] {
               }
               , new Object[] {
               P009910_A903ContratoServicosPrazo_CntSrvCod, P009910_A1823ContratoServicosPrazo_Momento, P009910_n1823ContratoServicosPrazo_Momento, P009910_A1456ContratoServicosPrazo_Cada, P009910_n1456ContratoServicosPrazo_Cada, P009910_A1265ContratoServicosPrazo_DiasBaixa, P009910_n1265ContratoServicosPrazo_DiasBaixa, P009910_A1264ContratoServicosPrazo_DiasMedia, P009910_n1264ContratoServicosPrazo_DiasMedia, P009910_A1263ContratoServicosPrazo_DiasAlta,
               P009910_n1263ContratoServicosPrazo_DiasAlta, P009910_A905ContratoServicosPrazo_Dias, P009910_n905ContratoServicosPrazo_Dias, P009910_A904ContratoServicosPrazo_Tipo
               }
               , new Object[] {
               }
               , new Object[] {
               P009912_A906ContratoServicosPrazoRegra_Sequencial, P009912_A903ContratoServicosPrazo_CntSrvCod, P009912_A909ContratoServicosPrazoRegra_Dias, P009912_A908ContratoServicosPrazoRegra_Fim, P009912_A907ContratoServicosPrazoRegra_Inicio
               }
               , new Object[] {
               }
               , new Object[] {
               P009914_A1335ContratoServicosPrioridade_CntSrvCod, P009914_A2067ContratoServicosPrioridade_Peso, P009914_n2067ContratoServicosPrioridade_Peso, P009914_A2066ContratoServicosPrioridade_Ordem, P009914_n2066ContratoServicosPrioridade_Ordem, P009914_A1359ContratoServicosPrioridade_Finalidade, P009914_n1359ContratoServicosPrioridade_Finalidade, P009914_A1339ContratoServicosPrioridade_PercPrazo, P009914_n1339ContratoServicosPrioridade_PercPrazo, P009914_A1338ContratoServicosPrioridade_PercValorB,
               P009914_n1338ContratoServicosPrioridade_PercValorB, P009914_A1337ContratoServicosPrioridade_Nome, P009914_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               P009915_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               P009916_A915ContratoSrvVnc_CntSrvCod, P009916_A2108ContratoServicosVnc_Descricao, P009916_n2108ContratoServicosVnc_Descricao, P009916_A1821ContratoSrvVnc_SrvVncRef, P009916_n1821ContratoSrvVnc_SrvVncRef, P009916_A1818ContratoServicosVnc_ClonarLink, P009916_n1818ContratoServicosVnc_ClonarLink, P009916_A1801ContratoSrvVnc_NovoRspDmn, P009916_n1801ContratoSrvVnc_NovoRspDmn, P009916_A1800ContratoSrvVnc_DoStatusDmn,
               P009916_n1800ContratoSrvVnc_DoStatusDmn, P009916_A1743ContratoSrvVnc_NovoStatusDmn, P009916_n1743ContratoSrvVnc_NovoStatusDmn, P009916_A1745ContratoSrvVnc_VincularCom, P009916_n1745ContratoSrvVnc_VincularCom, P009916_A1663ContratoSrvVnc_SrvVncStatus, P009916_n1663ContratoSrvVnc_SrvVncStatus, P009916_A1589ContratoSrvVnc_SrvVncCntSrvCod, P009916_n1589ContratoSrvVnc_SrvVncCntSrvCod, P009916_A1453ContratoServicosVnc_Ativo,
               P009916_n1453ContratoServicosVnc_Ativo, P009916_A1438ContratoServicosVnc_Gestor, P009916_n1438ContratoServicosVnc_Gestor, P009916_A1437ContratoServicosVnc_NaoClonaInfo, P009916_n1437ContratoServicosVnc_NaoClonaInfo, P009916_A1145ContratoServicosVnc_ClonaSrvOri, P009916_n1145ContratoServicosVnc_ClonaSrvOri, P009916_A1090ContratoSrvVnc_SemCusto, P009916_n1090ContratoSrvVnc_SemCusto, P009916_A1088ContratoSrvVnc_PrestadoraCod,
               P009916_n1088ContratoSrvVnc_PrestadoraCod, P009916_A1084ContratoSrvVnc_StatusDmn, P009916_n1084ContratoSrvVnc_StatusDmn, P009916_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               P009917_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               P009918_A160ContratoServicos_Codigo, P009918_A1063ContratoServicosSistemas_SistemaCod, P009918_A1067ContratoServicosSistemas_ServicoCod
               }
               , new Object[] {
               P009919_A2069ContratoServicosRmn_Sequencial, P009919_A160ContratoServicos_Codigo, P009919_A2072ContratoServicosRmn_Valor, P009919_A2071ContratoServicosRmn_Fim, P009919_A2070ContratoServicosRmn_Inicio
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A2074ContratoServicos_CalculoRmn ;
      private short A1538ContratoServicos_PercPgm ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short A1539ContratoServicos_PercCnc ;
      private short A1537ContratoServicos_PercTmp ;
      private short A1531ContratoServicos_TipoHierarquia ;
      private short A1224ContratoServicos_PrazoCorrecao ;
      private short A1182ContratoServicos_PrazoAtendeGarantia ;
      private short A1181ContratoServicos_PrazoGarantia ;
      private short A1153ContratoServicos_PrazoResposta ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short A938ContratoServicosTelas_Sequencial ;
      private short A2052ContratoServicosIndicador_Formato ;
      private short A1271ContratoServicosIndicador_Numero ;
      private short A1304ContratoServicosIndicadorFaixa_Und ;
      private short A1300ContratoServicosIndicadorFaixa_Numero ;
      private short AV12Sequencial ;
      private short A1823ContratoServicosPrazo_Momento ;
      private short A1265ContratoServicosPrazo_DiasBaixa ;
      private short A1264ContratoServicosPrazo_DiasMedia ;
      private short A1263ContratoServicosPrazo_DiasAlta ;
      private short A905ContratoServicosPrazo_Dias ;
      private short A906ContratoServicosPrazoRegra_Sequencial ;
      private short A909ContratoServicosPrazoRegra_Dias ;
      private short W906ContratoServicosPrazoRegra_Sequencial ;
      private short A2067ContratoServicosPrioridade_Peso ;
      private short A2066ContratoServicosPrioridade_Ordem ;
      private short A1821ContratoSrvVnc_SrvVncRef ;
      private short A2069ContratoServicosRmn_Sequencial ;
      private short W2069ContratoServicosRmn_Sequencial ;
      private int AV8ContratoServicos_Codigo ;
      private int AV9Servico_Codigo ;
      private int A155Servico_Codigo ;
      private int A1516ContratoServicos_TmpEstAnl ;
      private int A1502ContratoServicos_TmpEstCrr ;
      private int A1501ContratoServicos_TmpEstExc ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int W160ContratoServicos_Codigo ;
      private int GX_INS34 ;
      private int W155Servico_Codigo ;
      private int AV10NovoContratoServicos_Codigo ;
      private int A926ContratoServicosTelas_ContratoCod ;
      private int W926ContratoServicosTelas_ContratoCod ;
      private int GX_INS116 ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int W1270ContratoServicosIndicador_CntSrvCod ;
      private int GX_INS154 ;
      private int AV16ContratoServicosIndicador_Codigo ;
      private int A1299ContratoServicosIndicadorFaixa_Codigo ;
      private int W1269ContratoServicosIndicador_Codigo ;
      private int GX_INS159 ;
      private int W1299ContratoServicosIndicadorFaixa_Codigo ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private int W903ContratoServicosPrazo_CntSrvCod ;
      private int GX_INS112 ;
      private int GX_INS113 ;
      private int A1335ContratoServicosPrioridade_CntSrvCod ;
      private int A1336ContratoServicosPrioridade_Codigo ;
      private int W1335ContratoServicosPrioridade_CntSrvCod ;
      private int GX_INS162 ;
      private int AV18ContratoServicosPrioridade_Codigo ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A1801ContratoSrvVnc_NovoRspDmn ;
      private int A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int A1438ContratoServicosVnc_Gestor ;
      private int A1088ContratoSrvVnc_PrestadoraCod ;
      private int A917ContratoSrvVnc_Codigo ;
      private int W915ContratoSrvVnc_CntSrvCod ;
      private int GX_INS114 ;
      private int AV19ContratoSrvVnc_CntSrvCod ;
      private int A1063ContratoServicosSistemas_SistemaCod ;
      private int A1067ContratoServicosSistemas_ServicoCod ;
      private int GX_INS228 ;
      private int AV37GXV1 ;
      private int AV22Codigo ;
      private int GX_INS128 ;
      private long A555Servico_QtdContratada ;
      private decimal A1799ContratoServicos_LimiteProposta ;
      private decimal A1455ContratoServicos_IndiceDivergencia ;
      private decimal A1341ContratoServicos_FatorCnvUndCnt ;
      private decimal A1340ContratoServicos_QntUntCns ;
      private decimal A1191ContratoServicos_Produtividade ;
      private decimal A558Servico_Percentual ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal A1303ContratoServicosIndicadorFaixa_Reduz ;
      private decimal A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal A1456ContratoServicosPrazo_Cada ;
      private decimal A908ContratoServicosPrazoRegra_Fim ;
      private decimal A907ContratoServicosPrazoRegra_Inicio ;
      private decimal A1339ContratoServicosPrioridade_PercPrazo ;
      private decimal A1338ContratoServicosPrioridade_PercValorB ;
      private decimal A2072ContratoServicosRmn_Valor ;
      private decimal A2071ContratoServicosRmn_Fim ;
      private decimal A2070ContratoServicosRmn_Inicio ;
      private String AV24Alias ;
      private String scmdbuf ;
      private String A1858ContratoServicos_Alias ;
      private String A1723ContratoServicos_CodigoFiscal ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String A1325ContratoServicos_StatusPagFnc ;
      private String A1266ContratoServicos_Momento ;
      private String A1225ContratoServicos_PrazoCorrecaoTipo ;
      private String A868ContratoServicos_TipoVnc ;
      private String A639ContratoServicos_LocalExec ;
      private String W1858ContratoServicos_Alias ;
      private String Gx_emsg ;
      private String A934ContratoServicosTelas_TextMouse ;
      private String A932ContratoServicosTelas_Status ;
      private String A931ContratoServicosTelas_Tela ;
      private String A2051ContratoServicosIndicador_Sigla ;
      private String A1345ContratoServicosIndicador_CalculoSob ;
      private String A1309ContratoServicosIndicador_Periodicidade ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private String A904ContratoServicosPrazo_Tipo ;
      private String A1337ContratoServicosPrioridade_Nome ;
      private String A1800ContratoSrvVnc_DoStatusDmn ;
      private String A1743ContratoSrvVnc_NovoStatusDmn ;
      private String A1745ContratoSrvVnc_VincularCom ;
      private String A1663ContratoSrvVnc_SrvVncStatus ;
      private String A1084ContratoSrvVnc_StatusDmn ;
      private bool n1858ContratoServicos_Alias ;
      private bool A2094ContratoServicos_SolicitaGestorSistema ;
      private bool n2074ContratoServicos_CalculoRmn ;
      private bool n1538ContratoServicos_PercPgm ;
      private bool n1799ContratoServicos_LimiteProposta ;
      private bool n1723ContratoServicos_CodigoFiscal ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool n1539ContratoServicos_PercCnc ;
      private bool n1537ContratoServicos_PercTmp ;
      private bool n1531ContratoServicos_TipoHierarquia ;
      private bool n1516ContratoServicos_TmpEstAnl ;
      private bool n1502ContratoServicos_TmpEstCrr ;
      private bool n1501ContratoServicos_TmpEstExc ;
      private bool n1455ContratoServicos_IndiceDivergencia ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool A1397ContratoServicos_NaoRequerAtr ;
      private bool n1397ContratoServicos_NaoRequerAtr ;
      private bool n1341ContratoServicos_FatorCnvUndCnt ;
      private bool n1340ContratoServicos_QntUntCns ;
      private bool n1325ContratoServicos_StatusPagFnc ;
      private bool n1266ContratoServicos_Momento ;
      private bool n1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool n1224ContratoServicos_PrazoCorrecao ;
      private bool A1217ContratoServicos_EspelhaAceite ;
      private bool n1217ContratoServicos_EspelhaAceite ;
      private bool n1191ContratoServicos_Produtividade ;
      private bool A1190ContratoServicos_PrazoImediato ;
      private bool n1190ContratoServicos_PrazoImediato ;
      private bool n1182ContratoServicos_PrazoAtendeGarantia ;
      private bool n1181ContratoServicos_PrazoGarantia ;
      private bool n1153ContratoServicos_PrazoResposta ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool A888ContratoServicos_HmlSemCnf ;
      private bool n888ContratoServicos_HmlSemCnf ;
      private bool n868ContratoServicos_TipoVnc ;
      private bool A638ContratoServicos_Ativo ;
      private bool n558Servico_Percentual ;
      private bool n555Servico_QtdContratada ;
      private bool n932ContratoServicosTelas_Status ;
      private bool n929ContratoServicosTelas_Parms ;
      private bool n2051ContratoServicosIndicador_Sigla ;
      private bool n1345ContratoServicosIndicador_CalculoSob ;
      private bool n1310ContratoServicosIndicador_Vigencia ;
      private bool n1309ContratoServicosIndicador_Periodicidade ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private bool n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool n1306ContratoServicosIndicador_Meta ;
      private bool n1305ContratoServicosIndicador_Finalidade ;
      private bool n1823ContratoServicosPrazo_Momento ;
      private bool n1456ContratoServicosPrazo_Cada ;
      private bool n1265ContratoServicosPrazo_DiasBaixa ;
      private bool n1264ContratoServicosPrazo_DiasMedia ;
      private bool n1263ContratoServicosPrazo_DiasAlta ;
      private bool n905ContratoServicosPrazo_Dias ;
      private bool n2067ContratoServicosPrioridade_Peso ;
      private bool n2066ContratoServicosPrioridade_Ordem ;
      private bool n1359ContratoServicosPrioridade_Finalidade ;
      private bool n1339ContratoServicosPrioridade_PercPrazo ;
      private bool n1338ContratoServicosPrioridade_PercValorB ;
      private bool n2108ContratoServicosVnc_Descricao ;
      private bool n1821ContratoSrvVnc_SrvVncRef ;
      private bool A1818ContratoServicosVnc_ClonarLink ;
      private bool n1818ContratoServicosVnc_ClonarLink ;
      private bool n1801ContratoSrvVnc_NovoRspDmn ;
      private bool n1800ContratoSrvVnc_DoStatusDmn ;
      private bool n1743ContratoSrvVnc_NovoStatusDmn ;
      private bool n1745ContratoSrvVnc_VincularCom ;
      private bool n1663ContratoSrvVnc_SrvVncStatus ;
      private bool n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool A1453ContratoServicosVnc_Ativo ;
      private bool n1453ContratoServicosVnc_Ativo ;
      private bool n1438ContratoServicosVnc_Gestor ;
      private bool A1437ContratoServicosVnc_NaoClonaInfo ;
      private bool n1437ContratoServicosVnc_NaoClonaInfo ;
      private bool A1145ContratoServicosVnc_ClonaSrvOri ;
      private bool n1145ContratoServicosVnc_ClonaSrvOri ;
      private bool A1090ContratoSrvVnc_SemCusto ;
      private bool n1090ContratoSrvVnc_SemCusto ;
      private bool n1088ContratoSrvVnc_PrestadoraCod ;
      private bool n1084ContratoSrvVnc_StatusDmn ;
      private String A929ContratoServicosTelas_Parms ;
      private String A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String A1306ContratoServicosIndicador_Meta ;
      private String A1305ContratoServicosIndicador_Finalidade ;
      private String A1274ContratoServicosIndicador_Indicador ;
      private String A1359ContratoServicosPrioridade_Finalidade ;
      private String A607ServicoContrato_Faturamento ;
      private String A928ContratoServicosTelas_Link ;
      private String A1310ContratoServicosIndicador_Vigencia ;
      private String A2108ContratoServicosVnc_Descricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00992_A1858ContratoServicos_Alias ;
      private bool[] P00992_n1858ContratoServicos_Alias ;
      private int[] P00992_A155Servico_Codigo ;
      private bool[] P00992_A2094ContratoServicos_SolicitaGestorSistema ;
      private short[] P00992_A2074ContratoServicos_CalculoRmn ;
      private bool[] P00992_n2074ContratoServicos_CalculoRmn ;
      private short[] P00992_A1538ContratoServicos_PercPgm ;
      private bool[] P00992_n1538ContratoServicos_PercPgm ;
      private decimal[] P00992_A1799ContratoServicos_LimiteProposta ;
      private bool[] P00992_n1799ContratoServicos_LimiteProposta ;
      private String[] P00992_A1723ContratoServicos_CodigoFiscal ;
      private bool[] P00992_n1723ContratoServicos_CodigoFiscal ;
      private short[] P00992_A1649ContratoServicos_PrazoInicio ;
      private bool[] P00992_n1649ContratoServicos_PrazoInicio ;
      private short[] P00992_A1539ContratoServicos_PercCnc ;
      private bool[] P00992_n1539ContratoServicos_PercCnc ;
      private short[] P00992_A1537ContratoServicos_PercTmp ;
      private bool[] P00992_n1537ContratoServicos_PercTmp ;
      private short[] P00992_A1531ContratoServicos_TipoHierarquia ;
      private bool[] P00992_n1531ContratoServicos_TipoHierarquia ;
      private int[] P00992_A1516ContratoServicos_TmpEstAnl ;
      private bool[] P00992_n1516ContratoServicos_TmpEstAnl ;
      private int[] P00992_A1502ContratoServicos_TmpEstCrr ;
      private bool[] P00992_n1502ContratoServicos_TmpEstCrr ;
      private int[] P00992_A1501ContratoServicos_TmpEstExc ;
      private bool[] P00992_n1501ContratoServicos_TmpEstExc ;
      private decimal[] P00992_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] P00992_n1455ContratoServicos_IndiceDivergencia ;
      private String[] P00992_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P00992_n1454ContratoServicos_PrazoTpDias ;
      private bool[] P00992_A1397ContratoServicos_NaoRequerAtr ;
      private bool[] P00992_n1397ContratoServicos_NaoRequerAtr ;
      private decimal[] P00992_A1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] P00992_n1341ContratoServicos_FatorCnvUndCnt ;
      private decimal[] P00992_A1340ContratoServicos_QntUntCns ;
      private bool[] P00992_n1340ContratoServicos_QntUntCns ;
      private String[] P00992_A1325ContratoServicos_StatusPagFnc ;
      private bool[] P00992_n1325ContratoServicos_StatusPagFnc ;
      private String[] P00992_A1266ContratoServicos_Momento ;
      private bool[] P00992_n1266ContratoServicos_Momento ;
      private String[] P00992_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] P00992_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private short[] P00992_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] P00992_n1224ContratoServicos_PrazoCorrecao ;
      private bool[] P00992_A1217ContratoServicos_EspelhaAceite ;
      private bool[] P00992_n1217ContratoServicos_EspelhaAceite ;
      private int[] P00992_A1212ContratoServicos_UnidadeContratada ;
      private decimal[] P00992_A1191ContratoServicos_Produtividade ;
      private bool[] P00992_n1191ContratoServicos_Produtividade ;
      private bool[] P00992_A1190ContratoServicos_PrazoImediato ;
      private bool[] P00992_n1190ContratoServicos_PrazoImediato ;
      private short[] P00992_A1182ContratoServicos_PrazoAtendeGarantia ;
      private bool[] P00992_n1182ContratoServicos_PrazoAtendeGarantia ;
      private short[] P00992_A1181ContratoServicos_PrazoGarantia ;
      private bool[] P00992_n1181ContratoServicos_PrazoGarantia ;
      private short[] P00992_A1153ContratoServicos_PrazoResposta ;
      private bool[] P00992_n1153ContratoServicos_PrazoResposta ;
      private short[] P00992_A1152ContratoServicos_PrazoAnalise ;
      private bool[] P00992_n1152ContratoServicos_PrazoAnalise ;
      private bool[] P00992_A888ContratoServicos_HmlSemCnf ;
      private bool[] P00992_n888ContratoServicos_HmlSemCnf ;
      private String[] P00992_A868ContratoServicos_TipoVnc ;
      private bool[] P00992_n868ContratoServicos_TipoVnc ;
      private String[] P00992_A639ContratoServicos_LocalExec ;
      private bool[] P00992_A638ContratoServicos_Ativo ;
      private String[] P00992_A607ServicoContrato_Faturamento ;
      private decimal[] P00992_A558Servico_Percentual ;
      private bool[] P00992_n558Servico_Percentual ;
      private decimal[] P00992_A557Servico_VlrUnidadeContratada ;
      private long[] P00992_A555Servico_QtdContratada ;
      private bool[] P00992_n555Servico_QtdContratada ;
      private int[] P00992_A74Contrato_Codigo ;
      private int[] P00992_A160ContratoServicos_Codigo ;
      private int[] P00993_A160ContratoServicos_Codigo ;
      private int[] P00994_A926ContratoServicosTelas_ContratoCod ;
      private String[] P00994_A934ContratoServicosTelas_TextMouse ;
      private String[] P00994_A932ContratoServicosTelas_Status ;
      private bool[] P00994_n932ContratoServicosTelas_Status ;
      private String[] P00994_A929ContratoServicosTelas_Parms ;
      private bool[] P00994_n929ContratoServicosTelas_Parms ;
      private String[] P00994_A928ContratoServicosTelas_Link ;
      private String[] P00994_A931ContratoServicosTelas_Tela ;
      private short[] P00994_A938ContratoServicosTelas_Sequencial ;
      private int[] P00996_A1270ContratoServicosIndicador_CntSrvCod ;
      private short[] P00996_A2052ContratoServicosIndicador_Formato ;
      private String[] P00996_A2051ContratoServicosIndicador_Sigla ;
      private bool[] P00996_n2051ContratoServicosIndicador_Sigla ;
      private String[] P00996_A1345ContratoServicosIndicador_CalculoSob ;
      private bool[] P00996_n1345ContratoServicosIndicador_CalculoSob ;
      private String[] P00996_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] P00996_n1310ContratoServicosIndicador_Vigencia ;
      private String[] P00996_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] P00996_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] P00996_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P00996_n1308ContratoServicosIndicador_Tipo ;
      private String[] P00996_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] P00996_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] P00996_A1306ContratoServicosIndicador_Meta ;
      private bool[] P00996_n1306ContratoServicosIndicador_Meta ;
      private String[] P00996_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] P00996_n1305ContratoServicosIndicador_Finalidade ;
      private String[] P00996_A1274ContratoServicosIndicador_Indicador ;
      private short[] P00996_A1271ContratoServicosIndicador_Numero ;
      private int[] P00996_A1269ContratoServicosIndicador_Codigo ;
      private int[] P00997_A1269ContratoServicosIndicador_Codigo ;
      private int[] P00998_A1269ContratoServicosIndicador_Codigo ;
      private int[] P00998_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private short[] P00998_A1304ContratoServicosIndicadorFaixa_Und ;
      private decimal[] P00998_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private decimal[] P00998_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] P00998_A1301ContratoServicosIndicadorFaixa_Desde ;
      private short[] P00998_A1300ContratoServicosIndicadorFaixa_Numero ;
      private int[] P009910_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] P009910_A1823ContratoServicosPrazo_Momento ;
      private bool[] P009910_n1823ContratoServicosPrazo_Momento ;
      private decimal[] P009910_A1456ContratoServicosPrazo_Cada ;
      private bool[] P009910_n1456ContratoServicosPrazo_Cada ;
      private short[] P009910_A1265ContratoServicosPrazo_DiasBaixa ;
      private bool[] P009910_n1265ContratoServicosPrazo_DiasBaixa ;
      private short[] P009910_A1264ContratoServicosPrazo_DiasMedia ;
      private bool[] P009910_n1264ContratoServicosPrazo_DiasMedia ;
      private short[] P009910_A1263ContratoServicosPrazo_DiasAlta ;
      private bool[] P009910_n1263ContratoServicosPrazo_DiasAlta ;
      private short[] P009910_A905ContratoServicosPrazo_Dias ;
      private bool[] P009910_n905ContratoServicosPrazo_Dias ;
      private String[] P009910_A904ContratoServicosPrazo_Tipo ;
      private short[] P009912_A906ContratoServicosPrazoRegra_Sequencial ;
      private int[] P009912_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] P009912_A909ContratoServicosPrazoRegra_Dias ;
      private decimal[] P009912_A908ContratoServicosPrazoRegra_Fim ;
      private decimal[] P009912_A907ContratoServicosPrazoRegra_Inicio ;
      private int[] P009914_A1335ContratoServicosPrioridade_CntSrvCod ;
      private short[] P009914_A2067ContratoServicosPrioridade_Peso ;
      private bool[] P009914_n2067ContratoServicosPrioridade_Peso ;
      private short[] P009914_A2066ContratoServicosPrioridade_Ordem ;
      private bool[] P009914_n2066ContratoServicosPrioridade_Ordem ;
      private String[] P009914_A1359ContratoServicosPrioridade_Finalidade ;
      private bool[] P009914_n1359ContratoServicosPrioridade_Finalidade ;
      private decimal[] P009914_A1339ContratoServicosPrioridade_PercPrazo ;
      private bool[] P009914_n1339ContratoServicosPrioridade_PercPrazo ;
      private decimal[] P009914_A1338ContratoServicosPrioridade_PercValorB ;
      private bool[] P009914_n1338ContratoServicosPrioridade_PercValorB ;
      private String[] P009914_A1337ContratoServicosPrioridade_Nome ;
      private int[] P009914_A1336ContratoServicosPrioridade_Codigo ;
      private int[] P009915_A1336ContratoServicosPrioridade_Codigo ;
      private int[] P009916_A915ContratoSrvVnc_CntSrvCod ;
      private String[] P009916_A2108ContratoServicosVnc_Descricao ;
      private bool[] P009916_n2108ContratoServicosVnc_Descricao ;
      private short[] P009916_A1821ContratoSrvVnc_SrvVncRef ;
      private bool[] P009916_n1821ContratoSrvVnc_SrvVncRef ;
      private bool[] P009916_A1818ContratoServicosVnc_ClonarLink ;
      private bool[] P009916_n1818ContratoServicosVnc_ClonarLink ;
      private int[] P009916_A1801ContratoSrvVnc_NovoRspDmn ;
      private bool[] P009916_n1801ContratoSrvVnc_NovoRspDmn ;
      private String[] P009916_A1800ContratoSrvVnc_DoStatusDmn ;
      private bool[] P009916_n1800ContratoSrvVnc_DoStatusDmn ;
      private String[] P009916_A1743ContratoSrvVnc_NovoStatusDmn ;
      private bool[] P009916_n1743ContratoSrvVnc_NovoStatusDmn ;
      private String[] P009916_A1745ContratoSrvVnc_VincularCom ;
      private bool[] P009916_n1745ContratoSrvVnc_VincularCom ;
      private String[] P009916_A1663ContratoSrvVnc_SrvVncStatus ;
      private bool[] P009916_n1663ContratoSrvVnc_SrvVncStatus ;
      private int[] P009916_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P009916_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P009916_A1453ContratoServicosVnc_Ativo ;
      private bool[] P009916_n1453ContratoServicosVnc_Ativo ;
      private int[] P009916_A1438ContratoServicosVnc_Gestor ;
      private bool[] P009916_n1438ContratoServicosVnc_Gestor ;
      private bool[] P009916_A1437ContratoServicosVnc_NaoClonaInfo ;
      private bool[] P009916_n1437ContratoServicosVnc_NaoClonaInfo ;
      private bool[] P009916_A1145ContratoServicosVnc_ClonaSrvOri ;
      private bool[] P009916_n1145ContratoServicosVnc_ClonaSrvOri ;
      private bool[] P009916_A1090ContratoSrvVnc_SemCusto ;
      private bool[] P009916_n1090ContratoSrvVnc_SemCusto ;
      private int[] P009916_A1088ContratoSrvVnc_PrestadoraCod ;
      private bool[] P009916_n1088ContratoSrvVnc_PrestadoraCod ;
      private String[] P009916_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] P009916_n1084ContratoSrvVnc_StatusDmn ;
      private int[] P009916_A917ContratoSrvVnc_Codigo ;
      private int[] P009917_A917ContratoSrvVnc_Codigo ;
      private int[] P009918_A160ContratoServicos_Codigo ;
      private int[] P009918_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] P009918_A1067ContratoServicosSistemas_ServicoCod ;
      private short[] P009919_A2069ContratoServicosRmn_Sequencial ;
      private int[] P009919_A160ContratoServicos_Codigo ;
      private decimal[] P009919_A2072ContratoServicosRmn_Valor ;
      private decimal[] P009919_A2071ContratoServicosRmn_Fim ;
      private decimal[] P009919_A2070ContratoServicosRmn_Inicio ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV23Sistemas ;
   }

   public class prc_clonarservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new UpdateCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00992 ;
          prmP00992 = new Object[] {
          new Object[] {"@AV8ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00993 ;
          prmP00993 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_QtdContratada",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Servico_VlrUnidadeContratada",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Servico_Percentual",SqlDbType.Decimal,7,3} ,
          new Object[] {"@ServicoContrato_Faturamento",SqlDbType.VarChar,5,0} ,
          new Object[] {"@ContratoServicos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_LocalExec",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_TipoVnc",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_HmlSemCnf",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_PrazoAnalise",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoResposta",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoGarantia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoAtendeGarantia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoImediato",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_EspelhaAceite",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_PrazoCorrecao",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoCorrecaoTipo",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_Momento",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_StatusPagFnc",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_QntUntCns",SqlDbType.Decimal,9,4} ,
          new Object[] {"@ContratoServicos_FatorCnvUndCnt",SqlDbType.Decimal,9,4} ,
          new Object[] {"@ContratoServicos_NaoRequerAtr",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_PrazoTpDias",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_IndiceDivergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TipoHierarquia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoInicio",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_CodigoFiscal",SqlDbType.Char,5,0} ,
          new Object[] {"@ContratoServicos_LimiteProposta",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_PercPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_Alias",SqlDbType.Char,15,0} ,
          new Object[] {"@ContratoServicos_CalculoRmn",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_SolicitaGestorSistema",SqlDbType.Bit,4,0}
          } ;
          String cmdBufferP00993 ;
          cmdBufferP00993=" INSERT INTO [ContratoServicos]([Contrato_Codigo], [Servico_Codigo], [Servico_QtdContratada], [Servico_VlrUnidadeContratada], [Servico_Percentual], [ServicoContrato_Faturamento], [ContratoServicos_Ativo], [ContratoServicos_LocalExec], [ContratoServicos_TipoVnc], [ContratoServicos_HmlSemCnf], [ContratoServicos_PrazoAnalise], [ContratoServicos_PrazoResposta], [ContratoServicos_PrazoGarantia], [ContratoServicos_PrazoAtendeGarantia], [ContratoServicos_PrazoImediato], [ContratoServicos_Produtividade], [ContratoServicos_UnidadeContratada], [ContratoServicos_EspelhaAceite], [ContratoServicos_PrazoCorrecao], [ContratoServicos_PrazoCorrecaoTipo], [ContratoServicos_Momento], [ContratoServicos_StatusPagFnc], [ContratoServicos_QntUntCns], [ContratoServicos_FatorCnvUndCnt], [ContratoServicos_NaoRequerAtr], [ContratoServicos_PrazoTpDias], [ContratoServicos_IndiceDivergencia], [ContratoServicos_TmpEstExc], [ContratoServicos_TmpEstCrr], [ContratoServicos_TmpEstAnl], [ContratoServicos_TipoHierarquia], [ContratoServicos_PercTmp], [ContratoServicos_PercCnc], [ContratoServicos_PrazoInicio], [ContratoServicos_CodigoFiscal], [ContratoServicos_LimiteProposta], [ContratoServicos_PercPgm], [ContratoServicos_Alias], [ContratoServicos_CalculoRmn], [ContratoServicos_SolicitaGestorSistema]) VALUES(@Contrato_Codigo, @Servico_Codigo, @Servico_QtdContratada, @Servico_VlrUnidadeContratada, @Servico_Percentual, @ServicoContrato_Faturamento, @ContratoServicos_Ativo, @ContratoServicos_LocalExec, @ContratoServicos_TipoVnc, @ContratoServicos_HmlSemCnf, @ContratoServicos_PrazoAnalise, @ContratoServicos_PrazoResposta, @ContratoServicos_PrazoGarantia, @ContratoServicos_PrazoAtendeGarantia, @ContratoServicos_PrazoImediato, @ContratoServicos_Produtividade, @ContratoServicos_UnidadeContratada, @ContratoServicos_EspelhaAceite, "
          + " @ContratoServicos_PrazoCorrecao, @ContratoServicos_PrazoCorrecaoTipo, @ContratoServicos_Momento, @ContratoServicos_StatusPagFnc, @ContratoServicos_QntUntCns, @ContratoServicos_FatorCnvUndCnt, @ContratoServicos_NaoRequerAtr, @ContratoServicos_PrazoTpDias, @ContratoServicos_IndiceDivergencia, @ContratoServicos_TmpEstExc, @ContratoServicos_TmpEstCrr, @ContratoServicos_TmpEstAnl, @ContratoServicos_TipoHierarquia, @ContratoServicos_PercTmp, @ContratoServicos_PercCnc, @ContratoServicos_PrazoInicio, @ContratoServicos_CodigoFiscal, @ContratoServicos_LimiteProposta, @ContratoServicos_PercPgm, @ContratoServicos_Alias, @ContratoServicos_CalculoRmn, @ContratoServicos_SolicitaGestorSistema); SELECT SCOPE_IDENTITY()" ;
          Object[] prmP00994 ;
          prmP00994 = new Object[] {
          new Object[] {"@AV8ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00995 ;
          prmP00995 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosTelas_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@ContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@ContratoServicosTelas_Parms",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosTelas_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicosTelas_TextMouse",SqlDbType.Char,100,0}
          } ;
          Object[] prmP00996 ;
          prmP00996 = new Object[] {
          new Object[] {"@AV8ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00997 ;
          prmP00997 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosIndicador_Indicador",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_Finalidade",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_Meta",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_InstrumentoMedicao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_Tipo",SqlDbType.Char,2,0} ,
          new Object[] {"@ContratoServicosIndicador_Periodicidade",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicosIndicador_Vigencia",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContratoServicosIndicador_CalculoSob",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicosIndicador_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@ContratoServicosIndicador_Formato",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00998 ;
          prmP00998 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00999 ;
          prmP00999 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Desde",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Ate",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Und",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP009910 ;
          prmP009910 = new Object[] {
          new Object[] {"@AV8ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009911 ;
          prmP009911 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazo_Tipo",SqlDbType.Char,20,0} ,
          new Object[] {"@ContratoServicosPrazo_Dias",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazo_DiasAlta",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazo_DiasMedia",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazo_DiasBaixa",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazo_Cada",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosPrazo_Momento",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP009912 ;
          prmP009912 = new Object[] {
          new Object[] {"@AV8ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009913 ;
          prmP009913 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoRegra_Sequencial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosPrazoRegra_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosPrazoRegra_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosPrazoRegra_Dias",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmP009914 ;
          prmP009914 = new Object[] {
          new Object[] {"@AV8ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009915 ;
          prmP009915 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrioridade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@ContratoServicosPrioridade_PercValorB",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosPrioridade_PercPrazo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosPrioridade_Finalidade",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosPrioridade_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrioridade_Peso",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP009916 ;
          prmP009916 = new Object[] {
          new Object[] {"@AV8ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009917 ;
          prmP009917 = new Object[] {
          new Object[] {"@ContratoSrvVnc_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSrvVnc_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoSrvVnc_PrestadoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSrvVnc_SemCusto",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicosVnc_ClonaSrvOri",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicosVnc_NaoClonaInfo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicosVnc_Gestor",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosVnc_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoSrvVnc_SrvVncCntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSrvVnc_SrvVncStatus",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoSrvVnc_VincularCom",SqlDbType.Char,20,0} ,
          new Object[] {"@ContratoSrvVnc_NovoStatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoSrvVnc_DoStatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoSrvVnc_NovoRspDmn",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosVnc_ClonarLink",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoSrvVnc_SrvVncRef",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosVnc_Descricao",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP009918 ;
          prmP009918 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009919 ;
          prmP009919 = new Object[] {
          new Object[] {"@AV8ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009920 ;
          prmP009920 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosRmn_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosRmn_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosRmn_Valor",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmP009921 ;
          prmP009921 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00992", "SELECT [ContratoServicos_Alias], [Servico_Codigo], [ContratoServicos_SolicitaGestorSistema], [ContratoServicos_CalculoRmn], [ContratoServicos_PercPgm], [ContratoServicos_LimiteProposta], [ContratoServicos_CodigoFiscal], [ContratoServicos_PrazoInicio], [ContratoServicos_PercCnc], [ContratoServicos_PercTmp], [ContratoServicos_TipoHierarquia], [ContratoServicos_TmpEstAnl], [ContratoServicos_TmpEstCrr], [ContratoServicos_TmpEstExc], [ContratoServicos_IndiceDivergencia], [ContratoServicos_PrazoTpDias], [ContratoServicos_NaoRequerAtr], [ContratoServicos_FatorCnvUndCnt], [ContratoServicos_QntUntCns], [ContratoServicos_StatusPagFnc], [ContratoServicos_Momento], [ContratoServicos_PrazoCorrecaoTipo], [ContratoServicos_PrazoCorrecao], [ContratoServicos_EspelhaAceite], [ContratoServicos_UnidadeContratada], [ContratoServicos_Produtividade], [ContratoServicos_PrazoImediato], [ContratoServicos_PrazoAtendeGarantia], [ContratoServicos_PrazoGarantia], [ContratoServicos_PrazoResposta], [ContratoServicos_PrazoAnalise], [ContratoServicos_HmlSemCnf], [ContratoServicos_TipoVnc], [ContratoServicos_LocalExec], [ContratoServicos_Ativo], [ServicoContrato_Faturamento], [Servico_Percentual], [Servico_VlrUnidadeContratada], [Servico_QtdContratada], [Contrato_Codigo], [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV8ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00992,1,0,true,true )
             ,new CursorDef("P00993", cmdBufferP00993, GxErrorMask.GX_NOMASK,prmP00993)
             ,new CursorDef("P00994", "SELECT [ContratoServicosTelas_ContratoCod], [ContratoServicosTelas_TextMouse], [ContratoServicosTelas_Status], [ContratoServicosTelas_Parms], [ContratoServicosTelas_Link], [ContratoServicosTelas_Tela], [ContratoServicosTelas_Sequencial] FROM [ContratoServicosTelas] WITH (NOLOCK) WHERE [ContratoServicosTelas_ContratoCod] = @AV8ContratoServicos_Codigo ORDER BY [ContratoServicosTelas_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00994,100,0,true,false )
             ,new CursorDef("P00995", "INSERT INTO [ContratoServicosTelas]([ContratoServicosTelas_ContratoCod], [ContratoServicosTelas_Sequencial], [ContratoServicosTelas_Tela], [ContratoServicosTelas_Link], [ContratoServicosTelas_Parms], [ContratoServicosTelas_Status], [ContratoServicosTelas_TextMouse]) VALUES(@ContratoServicosTelas_ContratoCod, @ContratoServicosTelas_Sequencial, @ContratoServicosTelas_Tela, @ContratoServicosTelas_Link, @ContratoServicosTelas_Parms, @ContratoServicosTelas_Status, @ContratoServicosTelas_TextMouse)", GxErrorMask.GX_NOMASK,prmP00995)
             ,new CursorDef("P00996", "SELECT [ContratoServicosIndicador_CntSrvCod], [ContratoServicosIndicador_Formato], [ContratoServicosIndicador_Sigla], [ContratoServicosIndicador_CalculoSob], [ContratoServicosIndicador_Vigencia], [ContratoServicosIndicador_Periodicidade], [ContratoServicosIndicador_Tipo], [ContratoServicosIndicador_InstrumentoMedicao], [ContratoServicosIndicador_Meta], [ContratoServicosIndicador_Finalidade], [ContratoServicosIndicador_Indicador], [ContratoServicosIndicador_Numero], [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_CntSrvCod] = @AV8ContratoServicos_Codigo ORDER BY [ContratoServicosIndicador_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00996,100,0,true,false )
             ,new CursorDef("P00997", "INSERT INTO [ContratoServicosIndicador]([ContratoServicosIndicador_CntSrvCod], [ContratoServicosIndicador_Numero], [ContratoServicosIndicador_Indicador], [ContratoServicosIndicador_Finalidade], [ContratoServicosIndicador_Meta], [ContratoServicosIndicador_InstrumentoMedicao], [ContratoServicosIndicador_Tipo], [ContratoServicosIndicador_Periodicidade], [ContratoServicosIndicador_Vigencia], [ContratoServicosIndicador_CalculoSob], [ContratoServicosIndicador_Sigla], [ContratoServicosIndicador_Formato]) VALUES(@ContratoServicosIndicador_CntSrvCod, @ContratoServicosIndicador_Numero, @ContratoServicosIndicador_Indicador, @ContratoServicosIndicador_Finalidade, @ContratoServicosIndicador_Meta, @ContratoServicosIndicador_InstrumentoMedicao, @ContratoServicosIndicador_Tipo, @ContratoServicosIndicador_Periodicidade, @ContratoServicosIndicador_Vigencia, @ContratoServicosIndicador_CalculoSob, @ContratoServicosIndicador_Sigla, @ContratoServicosIndicador_Formato); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00997)
             ,new CursorDef("P00998", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo], [ContratoServicosIndicadorFaixa_Und], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Numero] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE ([ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo) AND (@ContratoServicos_Codigo = @ContratoServicos_Codigo) ORDER BY [ContratoServicosIndicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00998,100,0,true,false )
             ,new CursorDef("P00999", "INSERT INTO [ContratoServicosIndicadorFaixas]([ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo], [ContratoServicosIndicadorFaixa_Numero], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Und]) VALUES(@ContratoServicosIndicador_Codigo, @ContratoServicosIndicadorFaixa_Codigo, @ContratoServicosIndicadorFaixa_Numero, @ContratoServicosIndicadorFaixa_Desde, @ContratoServicosIndicadorFaixa_Ate, @ContratoServicosIndicadorFaixa_Reduz, @ContratoServicosIndicadorFaixa_Und)", GxErrorMask.GX_NOMASK,prmP00999)
             ,new CursorDef("P009910", "SELECT [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazo_Momento], [ContratoServicosPrazo_Cada], [ContratoServicosPrazo_DiasBaixa], [ContratoServicosPrazo_DiasMedia], [ContratoServicosPrazo_DiasAlta], [ContratoServicosPrazo_Dias], [ContratoServicosPrazo_Tipo] FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @AV8ContratoServicos_Codigo ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009910,1,0,true,true )
             ,new CursorDef("P009911", "INSERT INTO [ContratoServicosPrazo]([ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazo_Tipo], [ContratoServicosPrazo_Dias], [ContratoServicosPrazo_DiasAlta], [ContratoServicosPrazo_DiasMedia], [ContratoServicosPrazo_DiasBaixa], [ContratoServicosPrazo_Cada], [ContratoServicosPrazo_Momento]) VALUES(@ContratoServicosPrazo_CntSrvCod, @ContratoServicosPrazo_Tipo, @ContratoServicosPrazo_Dias, @ContratoServicosPrazo_DiasAlta, @ContratoServicosPrazo_DiasMedia, @ContratoServicosPrazo_DiasBaixa, @ContratoServicosPrazo_Cada, @ContratoServicosPrazo_Momento)", GxErrorMask.GX_NOMASK,prmP009911)
             ,new CursorDef("P009912", "SELECT [ContratoServicosPrazoRegra_Sequencial], [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoRegra_Dias], [ContratoServicosPrazoRegra_Fim], [ContratoServicosPrazoRegra_Inicio] FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @AV8ContratoServicos_Codigo ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009912,100,0,true,false )
             ,new CursorDef("P009913", "INSERT INTO [ContratoServicosPrazoContratoServicosPrazoRegra]([ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoRegra_Sequencial], [ContratoServicosPrazoRegra_Inicio], [ContratoServicosPrazoRegra_Fim], [ContratoServicosPrazoRegra_Dias]) VALUES(@ContratoServicosPrazo_CntSrvCod, @ContratoServicosPrazoRegra_Sequencial, @ContratoServicosPrazoRegra_Inicio, @ContratoServicosPrazoRegra_Fim, @ContratoServicosPrazoRegra_Dias)", GxErrorMask.GX_NOMASK,prmP009913)
             ,new CursorDef("P009914", "SELECT [ContratoServicosPrioridade_CntSrvCod], [ContratoServicosPrioridade_Peso], [ContratoServicosPrioridade_Ordem], [ContratoServicosPrioridade_Finalidade], [ContratoServicosPrioridade_PercPrazo], [ContratoServicosPrioridade_PercValorB], [ContratoServicosPrioridade_Nome], [ContratoServicosPrioridade_Codigo] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_CntSrvCod] = @AV8ContratoServicos_Codigo ORDER BY [ContratoServicosPrioridade_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009914,100,0,true,false )
             ,new CursorDef("P009915", "INSERT INTO [ContratoServicosPrioridade]([ContratoServicosPrioridade_CntSrvCod], [ContratoServicosPrioridade_Nome], [ContratoServicosPrioridade_PercValorB], [ContratoServicosPrioridade_PercPrazo], [ContratoServicosPrioridade_Finalidade], [ContratoServicosPrioridade_Ordem], [ContratoServicosPrioridade_Peso]) VALUES(@ContratoServicosPrioridade_CntSrvCod, @ContratoServicosPrioridade_Nome, @ContratoServicosPrioridade_PercValorB, @ContratoServicosPrioridade_PercPrazo, @ContratoServicosPrioridade_Finalidade, @ContratoServicosPrioridade_Ordem, @ContratoServicosPrioridade_Peso); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP009915)
             ,new CursorDef("P009916", "SELECT [ContratoSrvVnc_CntSrvCod], [ContratoServicosVnc_Descricao], [ContratoSrvVnc_SrvVncRef], [ContratoServicosVnc_ClonarLink], [ContratoSrvVnc_NovoRspDmn], [ContratoSrvVnc_DoStatusDmn], [ContratoSrvVnc_NovoStatusDmn], [ContratoSrvVnc_VincularCom], [ContratoSrvVnc_SrvVncStatus], [ContratoSrvVnc_SrvVncCntSrvCod], [ContratoServicosVnc_Ativo], [ContratoServicosVnc_Gestor], [ContratoServicosVnc_NaoClonaInfo], [ContratoServicosVnc_ClonaSrvOri], [ContratoSrvVnc_SemCusto], [ContratoSrvVnc_PrestadoraCod], [ContratoSrvVnc_StatusDmn], [ContratoSrvVnc_Codigo] FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE [ContratoSrvVnc_CntSrvCod] = @AV8ContratoServicos_Codigo ORDER BY [ContratoSrvVnc_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009916,100,0,true,false )
             ,new CursorDef("P009917", "INSERT INTO [ContratoServicosVnc]([ContratoSrvVnc_CntSrvCod], [ContratoSrvVnc_StatusDmn], [ContratoSrvVnc_PrestadoraCod], [ContratoSrvVnc_SemCusto], [ContratoServicosVnc_ClonaSrvOri], [ContratoServicosVnc_NaoClonaInfo], [ContratoServicosVnc_Gestor], [ContratoServicosVnc_Ativo], [ContratoSrvVnc_SrvVncCntSrvCod], [ContratoSrvVnc_SrvVncStatus], [ContratoSrvVnc_VincularCom], [ContratoSrvVnc_NovoStatusDmn], [ContratoSrvVnc_DoStatusDmn], [ContratoSrvVnc_NovoRspDmn], [ContratoServicosVnc_ClonarLink], [ContratoSrvVnc_SrvVncRef], [ContratoServicosVnc_Descricao]) VALUES(@ContratoSrvVnc_CntSrvCod, @ContratoSrvVnc_StatusDmn, @ContratoSrvVnc_PrestadoraCod, @ContratoSrvVnc_SemCusto, @ContratoServicosVnc_ClonaSrvOri, @ContratoServicosVnc_NaoClonaInfo, @ContratoServicosVnc_Gestor, @ContratoServicosVnc_Ativo, @ContratoSrvVnc_SrvVncCntSrvCod, @ContratoSrvVnc_SrvVncStatus, @ContratoSrvVnc_VincularCom, @ContratoSrvVnc_NovoStatusDmn, @ContratoSrvVnc_DoStatusDmn, @ContratoSrvVnc_NovoRspDmn, @ContratoServicosVnc_ClonarLink, @ContratoSrvVnc_SrvVncRef, @ContratoServicosVnc_Descricao); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP009917)
             ,new CursorDef("P009918", "SELECT [ContratoServicos_Codigo], [ContratoServicosSistemas_SistemaCod], [ContratoServicosSistemas_ServicoCod] FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009918,100,0,false,false )
             ,new CursorDef("P009919", "SELECT [ContratoServicosRmn_Sequencial], [ContratoServicos_Codigo], [ContratoServicosRmn_Valor], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Inicio] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV8ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009919,100,0,true,false )
             ,new CursorDef("P009920", "INSERT INTO [ContratoServicosRmn]([ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor]) VALUES(@ContratoServicos_Codigo, @ContratoServicosRmn_Sequencial, @ContratoServicosRmn_Inicio, @ContratoServicosRmn_Fim, @ContratoServicosRmn_Valor)", GxErrorMask.GX_NOMASK,prmP009920)
             ,new CursorDef("P009921", "INSERT INTO [ContratoServicosSistemas]([ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod], [ContratoServicosSistemas_SistemaCod]) VALUES(@ContratoServicos_Codigo, @ContratoServicosSistemas_ServicoCod, @ContratoServicosSistemas_SistemaCod)", GxErrorMask.GX_NOMASK,prmP009921)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 5) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((short[]) buf[12])[0] = rslt.getShort(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((short[]) buf[16])[0] = rslt.getShort(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((short[]) buf[18])[0] = rslt.getShort(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((bool[]) buf[30])[0] = rslt.getBool(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((String[]) buf[36])[0] = rslt.getString(20, 1) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((String[]) buf[38])[0] = rslt.getString(21, 1) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((String[]) buf[40])[0] = rslt.getString(22, 1) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((short[]) buf[42])[0] = rslt.getShort(23) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((bool[]) buf[44])[0] = rslt.getBool(24) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((int[]) buf[46])[0] = rslt.getInt(25) ;
                ((decimal[]) buf[47])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(26);
                ((bool[]) buf[49])[0] = rslt.getBool(27) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(27);
                ((short[]) buf[51])[0] = rslt.getShort(28) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(28);
                ((short[]) buf[53])[0] = rslt.getShort(29) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(29);
                ((short[]) buf[55])[0] = rslt.getShort(30) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(30);
                ((short[]) buf[57])[0] = rslt.getShort(31) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(31);
                ((bool[]) buf[59])[0] = rslt.getBool(32) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(32);
                ((String[]) buf[61])[0] = rslt.getString(33, 1) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(33);
                ((String[]) buf[63])[0] = rslt.getString(34, 1) ;
                ((bool[]) buf[64])[0] = rslt.getBool(35) ;
                ((String[]) buf[65])[0] = rslt.getVarchar(36) ;
                ((decimal[]) buf[66])[0] = rslt.getDecimal(37) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(37);
                ((decimal[]) buf[68])[0] = rslt.getDecimal(38) ;
                ((long[]) buf[69])[0] = rslt.getLong(39) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(39);
                ((int[]) buf[71])[0] = rslt.getInt(40) ;
                ((int[]) buf[72])[0] = rslt.getInt(41) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((short[]) buf[8])[0] = rslt.getShort(7) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 2) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getLongVarchar(11) ;
                ((short[]) buf[19])[0] = rslt.getShort(12) ;
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 20) ;
                return;
             case 10 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 20) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((bool[]) buf[23])[0] = rslt.getBool(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((bool[]) buf[25])[0] = rslt.getBool(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((bool[]) buf[27])[0] = rslt.getBool(15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((int[]) buf[29])[0] = rslt.getInt(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((String[]) buf[31])[0] = rslt.getString(17, 1) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((int[]) buf[33])[0] = rslt.getInt(18) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 17 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (long)parms[3]);
                }
                stmt.SetParameter(4, (decimal)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                stmt.SetParameter(6, (String)parms[7]);
                stmt.SetParameter(7, (bool)parms[8]);
                stmt.SetParameter(8, (String)parms[9]);
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 9 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 15 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(15, (bool)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[25]);
                }
                stmt.SetParameter(17, (int)parms[26]);
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 18 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(18, (bool)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 19 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(19, (short)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 20 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 21 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 22 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 24 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(24, (decimal)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 25 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(25, (bool)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 26 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(26, (String)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 27 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(27, (decimal)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[48]);
                }
                if ( (bool)parms[49] )
                {
                   stmt.setNull( 29 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(29, (int)parms[50]);
                }
                if ( (bool)parms[51] )
                {
                   stmt.setNull( 30 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(30, (int)parms[52]);
                }
                if ( (bool)parms[53] )
                {
                   stmt.setNull( 31 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(31, (short)parms[54]);
                }
                if ( (bool)parms[55] )
                {
                   stmt.setNull( 32 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(32, (short)parms[56]);
                }
                if ( (bool)parms[57] )
                {
                   stmt.setNull( 33 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(33, (short)parms[58]);
                }
                if ( (bool)parms[59] )
                {
                   stmt.setNull( 34 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(34, (short)parms[60]);
                }
                if ( (bool)parms[61] )
                {
                   stmt.setNull( 35 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(35, (String)parms[62]);
                }
                if ( (bool)parms[63] )
                {
                   stmt.setNull( 36 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(36, (decimal)parms[64]);
                }
                if ( (bool)parms[65] )
                {
                   stmt.setNull( 37 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(37, (short)parms[66]);
                }
                if ( (bool)parms[67] )
                {
                   stmt.setNull( 38 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(38, (String)parms[68]);
                }
                if ( (bool)parms[69] )
                {
                   stmt.setNull( 39 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(39, (short)parms[70]);
                }
                stmt.SetParameter(40, (bool)parms[71]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[7]);
                }
                stmt.SetParameter(7, (String)parms[8]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[18]);
                }
                stmt.SetParameter(12, (short)parms[19]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (short)parms[6]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[13]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(7, (short)parms[11]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(8, (bool)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 15 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(15, (bool)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 16 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(16, (short)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 17 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[32]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
       }
    }

 }

}
