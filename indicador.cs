/*
               File: Indicador
        Description: Indicador
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:56:20.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class indicador : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"INDICADOR_NAOCNFCOD") == 0 )
         {
            ajax_req_read_hidden_sdt(GetNextPar( ), AV3WWPContext);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAINDICADOR_NAOCNFCOD55227( AV3WWPContext) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_16") == 0 )
         {
            A2062Indicador_NaoCnfCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2062Indicador_NaoCnfCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2062Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_16( A2062Indicador_NaoCnfCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A2061Indicador_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2061Indicador_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2061Indicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2061Indicador_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A2061Indicador_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Indicador_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Indicador_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vINDICADOR_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Indicador_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbIndicador_Refer.Name = "INDICADOR_REFER";
         cmbIndicador_Refer.WebTags = "";
         cmbIndicador_Refer.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
         cmbIndicador_Refer.addItem("1", "Prazo", 0);
         cmbIndicador_Refer.addItem("2", "Frequ�ncia", 0);
         cmbIndicador_Refer.addItem("3", "Valores", 0);
         if ( cmbIndicador_Refer.ItemCount > 0 )
         {
            A2063Indicador_Refer = (short)(NumberUtil.Val( cmbIndicador_Refer.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0))), "."));
            n2063Indicador_Refer = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2063Indicador_Refer", StringUtil.LTrim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0)));
         }
         cmbIndicador_Aplica.Name = "INDICADOR_APLICA";
         cmbIndicador_Aplica.WebTags = "";
         cmbIndicador_Aplica.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
         cmbIndicador_Aplica.addItem("1", "Demandas", 0);
         cmbIndicador_Aplica.addItem("2", "Lotes", 0);
         if ( cmbIndicador_Aplica.ItemCount > 0 )
         {
            A2064Indicador_Aplica = (short)(NumberUtil.Val( cmbIndicador_Aplica.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0))), "."));
            n2064Indicador_Aplica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2064Indicador_Aplica", StringUtil.LTrim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0)));
         }
         dynIndicador_NaoCnfCod.Name = "INDICADOR_NAOCNFCOD";
         dynIndicador_NaoCnfCod.WebTags = "";
         cmbIndicador_Tipo.Name = "INDICADOR_TIPO";
         cmbIndicador_Tipo.WebTags = "";
         cmbIndicador_Tipo.addItem("P", "Pontualidade (demanda)", 0);
         cmbIndicador_Tipo.addItem("PL", "Frequ�ncia de atrasos (lote)", 0);
         cmbIndicador_Tipo.addItem("D", "Diverg�ncias (demanda)", 0);
         cmbIndicador_Tipo.addItem("FD", "Frequ�ncia de diverg�ncias (lote)", 0);
         cmbIndicador_Tipo.addItem("FP", "Frequ�ncia de pend�ncias (�ndice de rejei��o de demandas) (lote)", 0);
         cmbIndicador_Tipo.addItem("AC", "Ajustes de contagens (qtd) (lote)", 0);
         cmbIndicador_Tipo.addItem("AV", "Ajustes de contagens (soma do valor bruto) (lote)", 0);
         cmbIndicador_Tipo.addItem("T", "Tempestividade", 0);
         if ( cmbIndicador_Tipo.ItemCount > 0 )
         {
            A2065Indicador_Tipo = cmbIndicador_Tipo.getValidValue(A2065Indicador_Tipo);
            n2065Indicador_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2065Indicador_Tipo", A2065Indicador_Tipo);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Indicador", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtIndicador_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public indicador( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public indicador( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Indicador_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Indicador_Codigo = aP1_Indicador_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbIndicador_Refer = new GXCombobox();
         cmbIndicador_Aplica = new GXCombobox();
         dynIndicador_NaoCnfCod = new GXCombobox();
         cmbIndicador_Tipo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbIndicador_Refer.ItemCount > 0 )
         {
            A2063Indicador_Refer = (short)(NumberUtil.Val( cmbIndicador_Refer.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0))), "."));
            n2063Indicador_Refer = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2063Indicador_Refer", StringUtil.LTrim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0)));
         }
         if ( cmbIndicador_Aplica.ItemCount > 0 )
         {
            A2064Indicador_Aplica = (short)(NumberUtil.Val( cmbIndicador_Aplica.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0))), "."));
            n2064Indicador_Aplica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2064Indicador_Aplica", StringUtil.LTrim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0)));
         }
         if ( dynIndicador_NaoCnfCod.ItemCount > 0 )
         {
            A2062Indicador_NaoCnfCod = (int)(NumberUtil.Val( dynIndicador_NaoCnfCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0))), "."));
            n2062Indicador_NaoCnfCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2062Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)));
         }
         if ( cmbIndicador_Tipo.ItemCount > 0 )
         {
            A2065Indicador_Tipo = cmbIndicador_Tipo.getValidValue(A2065Indicador_Tipo);
            n2065Indicador_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2065Indicador_Tipo", A2065Indicador_Tipo);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_55227( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_55227e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_55227( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_55227( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_55227e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_55227( true) ;
         }
         return  ;
      }

      protected void wb_table3_41_55227e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_55227e( true) ;
         }
         else
         {
            wb_table1_2_55227e( false) ;
         }
      }

      protected void wb_table3_41_55227( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Indicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Indicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Indicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_55227e( true) ;
         }
         else
         {
            wb_table3_41_55227e( false) ;
         }
      }

      protected void wb_table2_5_55227( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_55227( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_55227e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_55227e( true) ;
         }
         else
         {
            wb_table2_5_55227e( false) ;
         }
      }

      protected void wb_table4_13_55227( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockindicador_nome_Internalname, "Nome", "", "", lblTextblockindicador_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Indicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtIndicador_Nome_Internalname, StringUtil.RTrim( A2059Indicador_Nome), StringUtil.RTrim( context.localUtil.Format( A2059Indicador_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtIndicador_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtIndicador_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Indicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockindicador_refer_Internalname, "Refere-se a", "", "", lblTextblockindicador_refer_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Indicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbIndicador_Refer, cmbIndicador_Refer_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0)), 1, cmbIndicador_Refer_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbIndicador_Refer.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_Indicador.htm");
            cmbIndicador_Refer.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbIndicador_Refer_Internalname, "Values", (String)(cmbIndicador_Refer.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockindicador_aplica_Internalname, "Aplica-se a", "", "", lblTextblockindicador_aplica_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Indicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbIndicador_Aplica, cmbIndicador_Aplica_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0)), 1, cmbIndicador_Aplica_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbIndicador_Aplica.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_Indicador.htm");
            cmbIndicador_Aplica.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbIndicador_Aplica_Internalname, "Values", (String)(cmbIndicador_Aplica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockindicador_naocnfcod_Internalname, "N�o Conformidade", "", "", lblTextblockindicador_naocnfcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Indicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynIndicador_NaoCnfCod, dynIndicador_NaoCnfCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)), 1, dynIndicador_NaoCnfCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynIndicador_NaoCnfCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_Indicador.htm");
            dynIndicador_NaoCnfCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynIndicador_NaoCnfCod_Internalname, "Values", (String)(dynIndicador_NaoCnfCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockindicador_tipo_Internalname, "Associado a", "", "", lblTextblockindicador_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Indicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbIndicador_Tipo, cmbIndicador_Tipo_Internalname, StringUtil.RTrim( A2065Indicador_Tipo), 1, cmbIndicador_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbIndicador_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_Indicador.htm");
            cmbIndicador_Tipo.CurrentValue = StringUtil.RTrim( A2065Indicador_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbIndicador_Tipo_Internalname, "Values", (String)(cmbIndicador_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_55227e( true) ;
         }
         else
         {
            wb_table4_13_55227e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11552 */
         E11552 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A2059Indicador_Nome = StringUtil.Upper( cgiGet( edtIndicador_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2059Indicador_Nome", A2059Indicador_Nome);
               cmbIndicador_Refer.CurrentValue = cgiGet( cmbIndicador_Refer_Internalname);
               A2063Indicador_Refer = (short)(NumberUtil.Val( cgiGet( cmbIndicador_Refer_Internalname), "."));
               n2063Indicador_Refer = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2063Indicador_Refer", StringUtil.LTrim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0)));
               n2063Indicador_Refer = ((0==A2063Indicador_Refer) ? true : false);
               cmbIndicador_Aplica.CurrentValue = cgiGet( cmbIndicador_Aplica_Internalname);
               A2064Indicador_Aplica = (short)(NumberUtil.Val( cgiGet( cmbIndicador_Aplica_Internalname), "."));
               n2064Indicador_Aplica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2064Indicador_Aplica", StringUtil.LTrim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0)));
               n2064Indicador_Aplica = ((0==A2064Indicador_Aplica) ? true : false);
               dynIndicador_NaoCnfCod.CurrentValue = cgiGet( dynIndicador_NaoCnfCod_Internalname);
               A2062Indicador_NaoCnfCod = (int)(NumberUtil.Val( cgiGet( dynIndicador_NaoCnfCod_Internalname), "."));
               n2062Indicador_NaoCnfCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2062Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)));
               n2062Indicador_NaoCnfCod = ((0==A2062Indicador_NaoCnfCod) ? true : false);
               cmbIndicador_Tipo.CurrentValue = cgiGet( cmbIndicador_Tipo_Internalname);
               A2065Indicador_Tipo = cgiGet( cmbIndicador_Tipo_Internalname);
               n2065Indicador_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2065Indicador_Tipo", A2065Indicador_Tipo);
               n2065Indicador_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A2065Indicador_Tipo)) ? true : false);
               /* Read saved values. */
               Z2058Indicador_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z2058Indicador_Codigo"), ",", "."));
               Z2059Indicador_Nome = cgiGet( "Z2059Indicador_Nome");
               Z2063Indicador_Refer = (short)(context.localUtil.CToN( cgiGet( "Z2063Indicador_Refer"), ",", "."));
               n2063Indicador_Refer = ((0==A2063Indicador_Refer) ? true : false);
               Z2064Indicador_Aplica = (short)(context.localUtil.CToN( cgiGet( "Z2064Indicador_Aplica"), ",", "."));
               n2064Indicador_Aplica = ((0==A2064Indicador_Aplica) ? true : false);
               Z2065Indicador_Tipo = cgiGet( "Z2065Indicador_Tipo");
               n2065Indicador_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A2065Indicador_Tipo)) ? true : false);
               Z2061Indicador_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z2061Indicador_AreaTrabalhoCod"), ",", "."));
               n2061Indicador_AreaTrabalhoCod = ((0==A2061Indicador_AreaTrabalhoCod) ? true : false);
               Z2062Indicador_NaoCnfCod = (int)(context.localUtil.CToN( cgiGet( "Z2062Indicador_NaoCnfCod"), ",", "."));
               n2062Indicador_NaoCnfCod = ((0==A2062Indicador_NaoCnfCod) ? true : false);
               A2061Indicador_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z2061Indicador_AreaTrabalhoCod"), ",", "."));
               n2061Indicador_AreaTrabalhoCod = false;
               n2061Indicador_AreaTrabalhoCod = ((0==A2061Indicador_AreaTrabalhoCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N2061Indicador_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N2061Indicador_AreaTrabalhoCod"), ",", "."));
               n2061Indicador_AreaTrabalhoCod = ((0==A2061Indicador_AreaTrabalhoCod) ? true : false);
               N2062Indicador_NaoCnfCod = (int)(context.localUtil.CToN( cgiGet( "N2062Indicador_NaoCnfCod"), ",", "."));
               n2062Indicador_NaoCnfCod = ((0==A2062Indicador_NaoCnfCod) ? true : false);
               AV7Indicador_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINDICADOR_CODIGO"), ",", "."));
               A2058Indicador_Codigo = (int)(context.localUtil.CToN( cgiGet( "INDICADOR_CODIGO"), ",", "."));
               AV11Insert_Indicador_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_INDICADOR_AREATRABALHOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A2061Indicador_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "INDICADOR_AREATRABALHOCOD"), ",", "."));
               n2061Indicador_AreaTrabalhoCod = ((0==A2061Indicador_AreaTrabalhoCod) ? true : false);
               AV12Insert_Indicador_NaoCnfCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_INDICADOR_NAOCNFCOD"), ",", "."));
               ajax_req_read_hidden_sdt(cgiGet( "vWWPCONTEXT"), AV3WWPContext);
               ajax_req_read_hidden_sdt(cgiGet( "vAUDITINGOBJECT"), AV14AuditingObject);
               AV15Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Indicador";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2058Indicador_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2061Indicador_AreaTrabalhoCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV15Pgmname, ""));
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("indicador:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("indicador:[SecurityCheckFailed value for]"+"Indicador_Codigo:"+context.localUtil.Format( (decimal)(A2058Indicador_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("indicador:[SecurityCheckFailed value for]"+"Indicador_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A2061Indicador_AreaTrabalhoCod), "ZZZZZ9"));
                  GXUtil.WriteLog("indicador:[SecurityCheckFailed value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV15Pgmname, "")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A2058Indicador_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode227 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode227;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound227 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_550( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11552 */
                           E11552 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12552 */
                           E12552 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12552 */
            E12552 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll55227( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes55227( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_550( )
      {
         BeforeValidate55227( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls55227( ) ;
            }
            else
            {
               CheckExtendedTable55227( ) ;
               CloseExtendedTableCursors55227( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption550( )
      {
      }

      protected void E11552( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV3WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV15Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV16GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            while ( AV16GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV16GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Indicador_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_Indicador_AreaTrabalhoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Indicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Indicador_AreaTrabalhoCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Indicador_NaoCnfCod") == 0 )
               {
                  AV12Insert_Indicador_NaoCnfCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Indicador_NaoCnfCod), 6, 0)));
               }
               AV16GXV1 = (int)(AV16GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            }
         }
      }

      protected void E12552( )
      {
         /* After Trn Routine */
         new wwpbaseobjects.audittransaction(context ).execute(  AV14AuditingObject,  AV15Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Pgmname", AV15Pgmname);
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwindicador.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM55227( short GX_JID )
      {
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2059Indicador_Nome = T00553_A2059Indicador_Nome[0];
               Z2063Indicador_Refer = T00553_A2063Indicador_Refer[0];
               Z2064Indicador_Aplica = T00553_A2064Indicador_Aplica[0];
               Z2065Indicador_Tipo = T00553_A2065Indicador_Tipo[0];
               Z2061Indicador_AreaTrabalhoCod = T00553_A2061Indicador_AreaTrabalhoCod[0];
               Z2062Indicador_NaoCnfCod = T00553_A2062Indicador_NaoCnfCod[0];
            }
            else
            {
               Z2059Indicador_Nome = A2059Indicador_Nome;
               Z2063Indicador_Refer = A2063Indicador_Refer;
               Z2064Indicador_Aplica = A2064Indicador_Aplica;
               Z2065Indicador_Tipo = A2065Indicador_Tipo;
               Z2061Indicador_AreaTrabalhoCod = A2061Indicador_AreaTrabalhoCod;
               Z2062Indicador_NaoCnfCod = A2062Indicador_NaoCnfCod;
            }
         }
         if ( GX_JID == -14 )
         {
            Z2058Indicador_Codigo = A2058Indicador_Codigo;
            Z2059Indicador_Nome = A2059Indicador_Nome;
            Z2063Indicador_Refer = A2063Indicador_Refer;
            Z2064Indicador_Aplica = A2064Indicador_Aplica;
            Z2065Indicador_Tipo = A2065Indicador_Tipo;
            Z2061Indicador_AreaTrabalhoCod = A2061Indicador_AreaTrabalhoCod;
            Z2062Indicador_NaoCnfCod = A2062Indicador_NaoCnfCod;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV15Pgmname = "Indicador";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Pgmname", AV15Pgmname);
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Indicador_Codigo) )
         {
            A2058Indicador_Codigo = AV7Indicador_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
         }
         GXAINDICADOR_NAOCNFCOD_html55227( AV3WWPContext) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Indicador_NaoCnfCod) )
         {
            dynIndicador_NaoCnfCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynIndicador_NaoCnfCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynIndicador_NaoCnfCod.Enabled), 5, 0)));
         }
         else
         {
            dynIndicador_NaoCnfCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynIndicador_NaoCnfCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynIndicador_NaoCnfCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Indicador_NaoCnfCod) )
         {
            A2062Indicador_NaoCnfCod = AV12Insert_Indicador_NaoCnfCod;
            n2062Indicador_NaoCnfCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2062Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Indicador_AreaTrabalhoCod) )
         {
            A2061Indicador_AreaTrabalhoCod = AV11Insert_Indicador_AreaTrabalhoCod;
            n2061Indicador_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2061Indicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2061Indicador_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A2061Indicador_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
            {
               A2061Indicador_AreaTrabalhoCod = AV3WWPContext.gxTpr_Areatrabalho_codigo;
               n2061Indicador_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2061Indicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2061Indicador_AreaTrabalhoCod), 6, 0)));
            }
         }
      }

      protected void Load55227( )
      {
         /* Using cursor T00556 */
         pr_default.execute(4, new Object[] {A2058Indicador_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound227 = 1;
            A2059Indicador_Nome = T00556_A2059Indicador_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2059Indicador_Nome", A2059Indicador_Nome);
            A2063Indicador_Refer = T00556_A2063Indicador_Refer[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2063Indicador_Refer", StringUtil.LTrim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0)));
            n2063Indicador_Refer = T00556_n2063Indicador_Refer[0];
            A2064Indicador_Aplica = T00556_A2064Indicador_Aplica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2064Indicador_Aplica", StringUtil.LTrim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0)));
            n2064Indicador_Aplica = T00556_n2064Indicador_Aplica[0];
            A2065Indicador_Tipo = T00556_A2065Indicador_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2065Indicador_Tipo", A2065Indicador_Tipo);
            n2065Indicador_Tipo = T00556_n2065Indicador_Tipo[0];
            A2061Indicador_AreaTrabalhoCod = T00556_A2061Indicador_AreaTrabalhoCod[0];
            n2061Indicador_AreaTrabalhoCod = T00556_n2061Indicador_AreaTrabalhoCod[0];
            A2062Indicador_NaoCnfCod = T00556_A2062Indicador_NaoCnfCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2062Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)));
            n2062Indicador_NaoCnfCod = T00556_n2062Indicador_NaoCnfCod[0];
            ZM55227( -14) ;
         }
         pr_default.close(4);
         OnLoadActions55227( ) ;
      }

      protected void OnLoadActions55227( )
      {
      }

      protected void CheckExtendedTable55227( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T00555 */
         pr_default.execute(3, new Object[] {n2062Indicador_NaoCnfCod, A2062Indicador_NaoCnfCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A2062Indicador_NaoCnfCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Indicador_Nao Conformidade'.", "ForeignKeyNotFound", 1, "INDICADOR_NAOCNFCOD");
               AnyError = 1;
               GX_FocusControl = dynIndicador_NaoCnfCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(3);
         /* Using cursor T00554 */
         pr_default.execute(2, new Object[] {n2061Indicador_AreaTrabalhoCod, A2061Indicador_AreaTrabalhoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A2061Indicador_AreaTrabalhoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Indicador_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors55227( )
      {
         pr_default.close(3);
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_16( int A2062Indicador_NaoCnfCod )
      {
         /* Using cursor T00557 */
         pr_default.execute(5, new Object[] {n2062Indicador_NaoCnfCod, A2062Indicador_NaoCnfCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A2062Indicador_NaoCnfCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Indicador_Nao Conformidade'.", "ForeignKeyNotFound", 1, "INDICADOR_NAOCNFCOD");
               AnyError = 1;
               GX_FocusControl = dynIndicador_NaoCnfCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_15( int A2061Indicador_AreaTrabalhoCod )
      {
         /* Using cursor T00558 */
         pr_default.execute(6, new Object[] {n2061Indicador_AreaTrabalhoCod, A2061Indicador_AreaTrabalhoCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A2061Indicador_AreaTrabalhoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Indicador_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey55227( )
      {
         /* Using cursor T00559 */
         pr_default.execute(7, new Object[] {A2058Indicador_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound227 = 1;
         }
         else
         {
            RcdFound227 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00553 */
         pr_default.execute(1, new Object[] {A2058Indicador_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM55227( 14) ;
            RcdFound227 = 1;
            A2058Indicador_Codigo = T00553_A2058Indicador_Codigo[0];
            A2059Indicador_Nome = T00553_A2059Indicador_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2059Indicador_Nome", A2059Indicador_Nome);
            A2063Indicador_Refer = T00553_A2063Indicador_Refer[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2063Indicador_Refer", StringUtil.LTrim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0)));
            n2063Indicador_Refer = T00553_n2063Indicador_Refer[0];
            A2064Indicador_Aplica = T00553_A2064Indicador_Aplica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2064Indicador_Aplica", StringUtil.LTrim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0)));
            n2064Indicador_Aplica = T00553_n2064Indicador_Aplica[0];
            A2065Indicador_Tipo = T00553_A2065Indicador_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2065Indicador_Tipo", A2065Indicador_Tipo);
            n2065Indicador_Tipo = T00553_n2065Indicador_Tipo[0];
            A2061Indicador_AreaTrabalhoCod = T00553_A2061Indicador_AreaTrabalhoCod[0];
            n2061Indicador_AreaTrabalhoCod = T00553_n2061Indicador_AreaTrabalhoCod[0];
            A2062Indicador_NaoCnfCod = T00553_A2062Indicador_NaoCnfCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2062Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)));
            n2062Indicador_NaoCnfCod = T00553_n2062Indicador_NaoCnfCod[0];
            Z2058Indicador_Codigo = A2058Indicador_Codigo;
            sMode227 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load55227( ) ;
            if ( AnyError == 1 )
            {
               RcdFound227 = 0;
               InitializeNonKey55227( ) ;
            }
            Gx_mode = sMode227;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound227 = 0;
            InitializeNonKey55227( ) ;
            sMode227 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode227;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey55227( ) ;
         if ( RcdFound227 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound227 = 0;
         /* Using cursor T005510 */
         pr_default.execute(8, new Object[] {A2058Indicador_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T005510_A2058Indicador_Codigo[0] < A2058Indicador_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T005510_A2058Indicador_Codigo[0] > A2058Indicador_Codigo ) ) )
            {
               A2058Indicador_Codigo = T005510_A2058Indicador_Codigo[0];
               RcdFound227 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound227 = 0;
         /* Using cursor T005511 */
         pr_default.execute(9, new Object[] {A2058Indicador_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T005511_A2058Indicador_Codigo[0] > A2058Indicador_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T005511_A2058Indicador_Codigo[0] < A2058Indicador_Codigo ) ) )
            {
               A2058Indicador_Codigo = T005511_A2058Indicador_Codigo[0];
               RcdFound227 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey55227( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtIndicador_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert55227( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound227 == 1 )
            {
               if ( A2058Indicador_Codigo != Z2058Indicador_Codigo )
               {
                  A2058Indicador_Codigo = Z2058Indicador_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtIndicador_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update55227( ) ;
                  GX_FocusControl = edtIndicador_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A2058Indicador_Codigo != Z2058Indicador_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtIndicador_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert55227( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtIndicador_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert55227( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A2058Indicador_Codigo != Z2058Indicador_Codigo )
         {
            A2058Indicador_Codigo = Z2058Indicador_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtIndicador_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency55227( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00552 */
            pr_default.execute(0, new Object[] {A2058Indicador_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Indicador"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z2059Indicador_Nome, T00552_A2059Indicador_Nome[0]) != 0 ) || ( Z2063Indicador_Refer != T00552_A2063Indicador_Refer[0] ) || ( Z2064Indicador_Aplica != T00552_A2064Indicador_Aplica[0] ) || ( StringUtil.StrCmp(Z2065Indicador_Tipo, T00552_A2065Indicador_Tipo[0]) != 0 ) || ( Z2061Indicador_AreaTrabalhoCod != T00552_A2061Indicador_AreaTrabalhoCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2062Indicador_NaoCnfCod != T00552_A2062Indicador_NaoCnfCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z2059Indicador_Nome, T00552_A2059Indicador_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"Indicador_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z2059Indicador_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00552_A2059Indicador_Nome[0]);
               }
               if ( Z2063Indicador_Refer != T00552_A2063Indicador_Refer[0] )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"Indicador_Refer");
                  GXUtil.WriteLogRaw("Old: ",Z2063Indicador_Refer);
                  GXUtil.WriteLogRaw("Current: ",T00552_A2063Indicador_Refer[0]);
               }
               if ( Z2064Indicador_Aplica != T00552_A2064Indicador_Aplica[0] )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"Indicador_Aplica");
                  GXUtil.WriteLogRaw("Old: ",Z2064Indicador_Aplica);
                  GXUtil.WriteLogRaw("Current: ",T00552_A2064Indicador_Aplica[0]);
               }
               if ( StringUtil.StrCmp(Z2065Indicador_Tipo, T00552_A2065Indicador_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"Indicador_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z2065Indicador_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T00552_A2065Indicador_Tipo[0]);
               }
               if ( Z2061Indicador_AreaTrabalhoCod != T00552_A2061Indicador_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"Indicador_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z2061Indicador_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T00552_A2061Indicador_AreaTrabalhoCod[0]);
               }
               if ( Z2062Indicador_NaoCnfCod != T00552_A2062Indicador_NaoCnfCod[0] )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"Indicador_NaoCnfCod");
                  GXUtil.WriteLogRaw("Old: ",Z2062Indicador_NaoCnfCod);
                  GXUtil.WriteLogRaw("Current: ",T00552_A2062Indicador_NaoCnfCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Indicador"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert55227( )
      {
         BeforeValidate55227( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable55227( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM55227( 0) ;
            CheckOptimisticConcurrency55227( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm55227( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert55227( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005512 */
                     pr_default.execute(10, new Object[] {A2059Indicador_Nome, n2063Indicador_Refer, A2063Indicador_Refer, n2064Indicador_Aplica, A2064Indicador_Aplica, n2065Indicador_Tipo, A2065Indicador_Tipo, n2061Indicador_AreaTrabalhoCod, A2061Indicador_AreaTrabalhoCod, n2062Indicador_NaoCnfCod, A2062Indicador_NaoCnfCod});
                     A2058Indicador_Codigo = T005512_A2058Indicador_Codigo[0];
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Indicador") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption550( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load55227( ) ;
            }
            EndLevel55227( ) ;
         }
         CloseExtendedTableCursors55227( ) ;
      }

      protected void Update55227( )
      {
         BeforeValidate55227( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable55227( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency55227( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm55227( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate55227( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005513 */
                     pr_default.execute(11, new Object[] {A2059Indicador_Nome, n2063Indicador_Refer, A2063Indicador_Refer, n2064Indicador_Aplica, A2064Indicador_Aplica, n2065Indicador_Tipo, A2065Indicador_Tipo, n2061Indicador_AreaTrabalhoCod, A2061Indicador_AreaTrabalhoCod, n2062Indicador_NaoCnfCod, A2062Indicador_NaoCnfCod, A2058Indicador_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Indicador") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Indicador"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate55227( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel55227( ) ;
         }
         CloseExtendedTableCursors55227( ) ;
      }

      protected void DeferredUpdate55227( )
      {
      }

      protected void delete( )
      {
         BeforeValidate55227( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency55227( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls55227( ) ;
            AfterConfirm55227( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete55227( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T005514 */
                  pr_default.execute(12, new Object[] {A2058Indicador_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("Indicador") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode227 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel55227( ) ;
         Gx_mode = sMode227;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls55227( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T005515 */
            pr_default.execute(13, new Object[] {A2058Indicador_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Nao Cnf"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel55227( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete55227( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Indicador");
            if ( AnyError == 0 )
            {
               ConfirmValues550( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Indicador");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart55227( )
      {
         /* Scan By routine */
         /* Using cursor T005516 */
         pr_default.execute(14);
         RcdFound227 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound227 = 1;
            A2058Indicador_Codigo = T005516_A2058Indicador_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext55227( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound227 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound227 = 1;
            A2058Indicador_Codigo = T005516_A2058Indicador_Codigo[0];
         }
      }

      protected void ScanEnd55227( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm55227( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert55227( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate55227( )
      {
         /* Before Update Rules */
         new loadauditindicador(context ).execute(  "Y", ref  AV14AuditingObject,  A2058Indicador_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeDelete55227( )
      {
         /* Before Delete Rules */
         new loadauditindicador(context ).execute(  "Y", ref  AV14AuditingObject,  A2058Indicador_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeComplete55227( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditindicador(context ).execute(  "N", ref  AV14AuditingObject,  A2058Indicador_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditindicador(context ).execute(  "N", ref  AV14AuditingObject,  A2058Indicador_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void BeforeValidate55227( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes55227( )
      {
         edtIndicador_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIndicador_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIndicador_Nome_Enabled), 5, 0)));
         cmbIndicador_Refer.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbIndicador_Refer_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbIndicador_Refer.Enabled), 5, 0)));
         cmbIndicador_Aplica.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbIndicador_Aplica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbIndicador_Aplica.Enabled), 5, 0)));
         dynIndicador_NaoCnfCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynIndicador_NaoCnfCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynIndicador_NaoCnfCod.Enabled), 5, 0)));
         cmbIndicador_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbIndicador_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbIndicador_Tipo.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues550( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205623562160");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("indicador.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Indicador_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z2058Indicador_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2058Indicador_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2059Indicador_Nome", StringUtil.RTrim( Z2059Indicador_Nome));
         GxWebStd.gx_hidden_field( context, "Z2063Indicador_Refer", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2063Indicador_Refer), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2064Indicador_Aplica", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2064Indicador_Aplica), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2065Indicador_Tipo", StringUtil.RTrim( Z2065Indicador_Tipo));
         GxWebStd.gx_hidden_field( context, "Z2061Indicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2061Indicador_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2062Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2062Indicador_NaoCnfCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N2061Indicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2061Indicador_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N2062Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2062Indicador_NaoCnfCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vINDICADOR_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Indicador_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "INDICADOR_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2058Indicador_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_INDICADOR_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Indicador_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "INDICADOR_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2061Indicador_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_INDICADOR_NAOCNFCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Indicador_NaoCnfCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV3WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV3WWPContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITINGOBJECT", AV14AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITINGOBJECT", AV14AuditingObject);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV15Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vINDICADOR_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Indicador_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Indicador";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2058Indicador_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2061Indicador_AreaTrabalhoCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV15Pgmname, ""));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("indicador:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("indicador:[SendSecurityCheck value for]"+"Indicador_Codigo:"+context.localUtil.Format( (decimal)(A2058Indicador_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("indicador:[SendSecurityCheck value for]"+"Indicador_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A2061Indicador_AreaTrabalhoCod), "ZZZZZ9"));
         GXUtil.WriteLog("indicador:[SendSecurityCheck value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV15Pgmname, "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("indicador.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Indicador_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Indicador" ;
      }

      public override String GetPgmdesc( )
      {
         return "Indicador" ;
      }

      protected void InitializeNonKey55227( )
      {
         A2062Indicador_NaoCnfCod = 0;
         n2062Indicador_NaoCnfCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2062Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)));
         n2062Indicador_NaoCnfCod = ((0==A2062Indicador_NaoCnfCod) ? true : false);
         AV14AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A2059Indicador_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2059Indicador_Nome", A2059Indicador_Nome);
         A2063Indicador_Refer = 0;
         n2063Indicador_Refer = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2063Indicador_Refer", StringUtil.LTrim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0)));
         n2063Indicador_Refer = ((0==A2063Indicador_Refer) ? true : false);
         A2064Indicador_Aplica = 0;
         n2064Indicador_Aplica = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2064Indicador_Aplica", StringUtil.LTrim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0)));
         n2064Indicador_Aplica = ((0==A2064Indicador_Aplica) ? true : false);
         A2065Indicador_Tipo = "";
         n2065Indicador_Tipo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2065Indicador_Tipo", A2065Indicador_Tipo);
         n2065Indicador_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A2065Indicador_Tipo)) ? true : false);
         A2061Indicador_AreaTrabalhoCod = AV3WWPContext.gxTpr_Areatrabalho_codigo;
         n2061Indicador_AreaTrabalhoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2061Indicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2061Indicador_AreaTrabalhoCod), 6, 0)));
         Z2059Indicador_Nome = "";
         Z2063Indicador_Refer = 0;
         Z2064Indicador_Aplica = 0;
         Z2065Indicador_Tipo = "";
         Z2061Indicador_AreaTrabalhoCod = 0;
         Z2062Indicador_NaoCnfCod = 0;
      }

      protected void InitAll55227( )
      {
         A2058Indicador_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
         InitializeNonKey55227( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2061Indicador_AreaTrabalhoCod = i2061Indicador_AreaTrabalhoCod;
         n2061Indicador_AreaTrabalhoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2061Indicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2061Indicador_AreaTrabalhoCod), 6, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205623562187");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("indicador.js", "?20205623562187");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockindicador_nome_Internalname = "TEXTBLOCKINDICADOR_NOME";
         edtIndicador_Nome_Internalname = "INDICADOR_NOME";
         lblTextblockindicador_refer_Internalname = "TEXTBLOCKINDICADOR_REFER";
         cmbIndicador_Refer_Internalname = "INDICADOR_REFER";
         lblTextblockindicador_aplica_Internalname = "TEXTBLOCKINDICADOR_APLICA";
         cmbIndicador_Aplica_Internalname = "INDICADOR_APLICA";
         lblTextblockindicador_naocnfcod_Internalname = "TEXTBLOCKINDICADOR_NAOCNFCOD";
         dynIndicador_NaoCnfCod_Internalname = "INDICADOR_NAOCNFCOD";
         lblTextblockindicador_tipo_Internalname = "TEXTBLOCKINDICADOR_TIPO";
         cmbIndicador_Tipo_Internalname = "INDICADOR_TIPO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Indicador";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Indicador";
         cmbIndicador_Tipo_Jsonclick = "";
         cmbIndicador_Tipo.Enabled = 1;
         dynIndicador_NaoCnfCod_Jsonclick = "";
         dynIndicador_NaoCnfCod.Enabled = 1;
         cmbIndicador_Aplica_Jsonclick = "";
         cmbIndicador_Aplica.Enabled = 1;
         cmbIndicador_Refer_Jsonclick = "";
         cmbIndicador_Refer.Enabled = 1;
         edtIndicador_Nome_Jsonclick = "";
         edtIndicador_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAINDICADOR_NAOCNFCOD55227( wwpbaseobjects.SdtWWPContext AV3WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAINDICADOR_NAOCNFCOD_data55227( AV3WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAINDICADOR_NAOCNFCOD_html55227( wwpbaseobjects.SdtWWPContext AV3WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLAINDICADOR_NAOCNFCOD_data55227( AV3WWPContext) ;
         gxdynajaxindex = 1;
         dynIndicador_NaoCnfCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynIndicador_NaoCnfCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAINDICADOR_NAOCNFCOD_data55227( wwpbaseobjects.SdtWWPContext AV3WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T005517 */
         pr_default.execute(15, new Object[] {AV3WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(15) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T005517_A426NaoConformidade_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T005517_A427NaoConformidade_Nome[0]));
            pr_default.readNext(15);
         }
         pr_default.close(15);
      }

      protected void XC_10_55227( wwpbaseobjects.SdtAuditingObject AV14AuditingObject ,
                                  int A2058Indicador_Codigo ,
                                  String Gx_mode )
      {
         new loadauditindicador(context ).execute(  "Y", ref  AV14AuditingObject,  A2058Indicador_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV14AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_11_55227( wwpbaseobjects.SdtAuditingObject AV14AuditingObject ,
                                  int A2058Indicador_Codigo ,
                                  String Gx_mode )
      {
         new loadauditindicador(context ).execute(  "Y", ref  AV14AuditingObject,  A2058Indicador_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV14AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_12_55227( String Gx_mode ,
                                  wwpbaseobjects.SdtAuditingObject AV14AuditingObject ,
                                  int A2058Indicador_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditindicador(context ).execute(  "N", ref  AV14AuditingObject,  A2058Indicador_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV14AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_13_55227( String Gx_mode ,
                                  wwpbaseobjects.SdtAuditingObject AV14AuditingObject ,
                                  int A2058Indicador_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditindicador(context ).execute(  "N", ref  AV14AuditingObject,  A2058Indicador_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV14AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Indicador_naocnfcod( GXCombobox dynGX_Parm1 )
      {
         dynIndicador_NaoCnfCod = dynGX_Parm1;
         A2062Indicador_NaoCnfCod = (int)(NumberUtil.Val( dynIndicador_NaoCnfCod.CurrentValue, "."));
         n2062Indicador_NaoCnfCod = false;
         /* Using cursor T005518 */
         pr_default.execute(16, new Object[] {n2062Indicador_NaoCnfCod, A2062Indicador_NaoCnfCod});
         if ( (pr_default.getStatus(16) == 101) )
         {
            if ( ! ( (0==A2062Indicador_NaoCnfCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Indicador_Nao Conformidade'.", "ForeignKeyNotFound", 1, "INDICADOR_NAOCNFCOD");
               AnyError = 1;
               GX_FocusControl = dynIndicador_NaoCnfCod_Internalname;
            }
         }
         pr_default.close(16);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Indicador_Codigo',fld:'vINDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12552',iparms:[{av:'AV14AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV15Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(16);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z2059Indicador_Nome = "";
         Z2065Indicador_Tipo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV3WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         A2065Indicador_Tipo = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockindicador_nome_Jsonclick = "";
         A2059Indicador_Nome = "";
         lblTextblockindicador_refer_Jsonclick = "";
         lblTextblockindicador_aplica_Jsonclick = "";
         lblTextblockindicador_naocnfcod_Jsonclick = "";
         lblTextblockindicador_tipo_Jsonclick = "";
         AV11Insert_Indicador_AreaTrabalhoCod = AV3WWPContext.gxTpr_Areatrabalho_codigo;
         AV14AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV15Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode227 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         T00556_A2058Indicador_Codigo = new int[1] ;
         T00556_A2059Indicador_Nome = new String[] {""} ;
         T00556_A2063Indicador_Refer = new short[1] ;
         T00556_n2063Indicador_Refer = new bool[] {false} ;
         T00556_A2064Indicador_Aplica = new short[1] ;
         T00556_n2064Indicador_Aplica = new bool[] {false} ;
         T00556_A2065Indicador_Tipo = new String[] {""} ;
         T00556_n2065Indicador_Tipo = new bool[] {false} ;
         T00556_A2061Indicador_AreaTrabalhoCod = new int[1] ;
         T00556_n2061Indicador_AreaTrabalhoCod = new bool[] {false} ;
         T00556_A2062Indicador_NaoCnfCod = new int[1] ;
         T00556_n2062Indicador_NaoCnfCod = new bool[] {false} ;
         T00555_A2062Indicador_NaoCnfCod = new int[1] ;
         T00555_n2062Indicador_NaoCnfCod = new bool[] {false} ;
         T00554_A2061Indicador_AreaTrabalhoCod = new int[1] ;
         T00554_n2061Indicador_AreaTrabalhoCod = new bool[] {false} ;
         T00557_A2062Indicador_NaoCnfCod = new int[1] ;
         T00557_n2062Indicador_NaoCnfCod = new bool[] {false} ;
         T00558_A2061Indicador_AreaTrabalhoCod = new int[1] ;
         T00558_n2061Indicador_AreaTrabalhoCod = new bool[] {false} ;
         T00559_A2058Indicador_Codigo = new int[1] ;
         T00553_A2058Indicador_Codigo = new int[1] ;
         T00553_A2059Indicador_Nome = new String[] {""} ;
         T00553_A2063Indicador_Refer = new short[1] ;
         T00553_n2063Indicador_Refer = new bool[] {false} ;
         T00553_A2064Indicador_Aplica = new short[1] ;
         T00553_n2064Indicador_Aplica = new bool[] {false} ;
         T00553_A2065Indicador_Tipo = new String[] {""} ;
         T00553_n2065Indicador_Tipo = new bool[] {false} ;
         T00553_A2061Indicador_AreaTrabalhoCod = new int[1] ;
         T00553_n2061Indicador_AreaTrabalhoCod = new bool[] {false} ;
         T00553_A2062Indicador_NaoCnfCod = new int[1] ;
         T00553_n2062Indicador_NaoCnfCod = new bool[] {false} ;
         T005510_A2058Indicador_Codigo = new int[1] ;
         T005511_A2058Indicador_Codigo = new int[1] ;
         T00552_A2058Indicador_Codigo = new int[1] ;
         T00552_A2059Indicador_Nome = new String[] {""} ;
         T00552_A2063Indicador_Refer = new short[1] ;
         T00552_n2063Indicador_Refer = new bool[] {false} ;
         T00552_A2064Indicador_Aplica = new short[1] ;
         T00552_n2064Indicador_Aplica = new bool[] {false} ;
         T00552_A2065Indicador_Tipo = new String[] {""} ;
         T00552_n2065Indicador_Tipo = new bool[] {false} ;
         T00552_A2061Indicador_AreaTrabalhoCod = new int[1] ;
         T00552_n2061Indicador_AreaTrabalhoCod = new bool[] {false} ;
         T00552_A2062Indicador_NaoCnfCod = new int[1] ;
         T00552_n2062Indicador_NaoCnfCod = new bool[] {false} ;
         T005512_A2058Indicador_Codigo = new int[1] ;
         T005515_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         T005516_A2058Indicador_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T005517_A426NaoConformidade_Codigo = new int[1] ;
         T005517_A427NaoConformidade_Nome = new String[] {""} ;
         T005517_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         T005518_A2062Indicador_NaoCnfCod = new int[1] ;
         T005518_n2062Indicador_NaoCnfCod = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.indicador__default(),
            new Object[][] {
                new Object[] {
               T00552_A2058Indicador_Codigo, T00552_A2059Indicador_Nome, T00552_A2063Indicador_Refer, T00552_n2063Indicador_Refer, T00552_A2064Indicador_Aplica, T00552_n2064Indicador_Aplica, T00552_A2065Indicador_Tipo, T00552_n2065Indicador_Tipo, T00552_A2061Indicador_AreaTrabalhoCod, T00552_n2061Indicador_AreaTrabalhoCod,
               T00552_A2062Indicador_NaoCnfCod, T00552_n2062Indicador_NaoCnfCod
               }
               , new Object[] {
               T00553_A2058Indicador_Codigo, T00553_A2059Indicador_Nome, T00553_A2063Indicador_Refer, T00553_n2063Indicador_Refer, T00553_A2064Indicador_Aplica, T00553_n2064Indicador_Aplica, T00553_A2065Indicador_Tipo, T00553_n2065Indicador_Tipo, T00553_A2061Indicador_AreaTrabalhoCod, T00553_n2061Indicador_AreaTrabalhoCod,
               T00553_A2062Indicador_NaoCnfCod, T00553_n2062Indicador_NaoCnfCod
               }
               , new Object[] {
               T00554_A2061Indicador_AreaTrabalhoCod
               }
               , new Object[] {
               T00555_A2062Indicador_NaoCnfCod
               }
               , new Object[] {
               T00556_A2058Indicador_Codigo, T00556_A2059Indicador_Nome, T00556_A2063Indicador_Refer, T00556_n2063Indicador_Refer, T00556_A2064Indicador_Aplica, T00556_n2064Indicador_Aplica, T00556_A2065Indicador_Tipo, T00556_n2065Indicador_Tipo, T00556_A2061Indicador_AreaTrabalhoCod, T00556_n2061Indicador_AreaTrabalhoCod,
               T00556_A2062Indicador_NaoCnfCod, T00556_n2062Indicador_NaoCnfCod
               }
               , new Object[] {
               T00557_A2062Indicador_NaoCnfCod
               }
               , new Object[] {
               T00558_A2061Indicador_AreaTrabalhoCod
               }
               , new Object[] {
               T00559_A2058Indicador_Codigo
               }
               , new Object[] {
               T005510_A2058Indicador_Codigo
               }
               , new Object[] {
               T005511_A2058Indicador_Codigo
               }
               , new Object[] {
               T005512_A2058Indicador_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T005515_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               T005516_A2058Indicador_Codigo
               }
               , new Object[] {
               T005517_A426NaoConformidade_Codigo, T005517_A427NaoConformidade_Nome, T005517_A428NaoConformidade_AreaTrabalhoCod
               }
               , new Object[] {
               T005518_A2062Indicador_NaoCnfCod
               }
            }
         );
         Z2061Indicador_AreaTrabalhoCod = AV3WWPContext.gxTpr_Areatrabalho_codigo;
         n2061Indicador_AreaTrabalhoCod = false;
         N2061Indicador_AreaTrabalhoCod = AV3WWPContext.gxTpr_Areatrabalho_codigo;
         n2061Indicador_AreaTrabalhoCod = false;
         i2061Indicador_AreaTrabalhoCod = AV3WWPContext.gxTpr_Areatrabalho_codigo;
         n2061Indicador_AreaTrabalhoCod = false;
         A2061Indicador_AreaTrabalhoCod = AV3WWPContext.gxTpr_Areatrabalho_codigo;
         n2061Indicador_AreaTrabalhoCod = false;
         AV15Pgmname = "Indicador";
      }

      private short Z2063Indicador_Refer ;
      private short Z2064Indicador_Aplica ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A2063Indicador_Refer ;
      private short A2064Indicador_Aplica ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound227 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Indicador_Codigo ;
      private int Z2058Indicador_Codigo ;
      private int Z2061Indicador_AreaTrabalhoCod ;
      private int Z2062Indicador_NaoCnfCod ;
      private int N2061Indicador_AreaTrabalhoCod ;
      private int N2062Indicador_NaoCnfCod ;
      private int A2062Indicador_NaoCnfCod ;
      private int A2061Indicador_AreaTrabalhoCod ;
      private int AV7Indicador_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtIndicador_Nome_Enabled ;
      private int A2058Indicador_Codigo ;
      private int AV11Insert_Indicador_AreaTrabalhoCod ;
      private int AV12Insert_Indicador_NaoCnfCod ;
      private int AV16GXV1 ;
      private int i2061Indicador_AreaTrabalhoCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z2059Indicador_Nome ;
      private String Z2065Indicador_Tipo ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A2065Indicador_Tipo ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtIndicador_Nome_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockindicador_nome_Internalname ;
      private String lblTextblockindicador_nome_Jsonclick ;
      private String A2059Indicador_Nome ;
      private String edtIndicador_Nome_Jsonclick ;
      private String lblTextblockindicador_refer_Internalname ;
      private String lblTextblockindicador_refer_Jsonclick ;
      private String cmbIndicador_Refer_Internalname ;
      private String cmbIndicador_Refer_Jsonclick ;
      private String lblTextblockindicador_aplica_Internalname ;
      private String lblTextblockindicador_aplica_Jsonclick ;
      private String cmbIndicador_Aplica_Internalname ;
      private String cmbIndicador_Aplica_Jsonclick ;
      private String lblTextblockindicador_naocnfcod_Internalname ;
      private String lblTextblockindicador_naocnfcod_Jsonclick ;
      private String dynIndicador_NaoCnfCod_Internalname ;
      private String dynIndicador_NaoCnfCod_Jsonclick ;
      private String lblTextblockindicador_tipo_Internalname ;
      private String lblTextblockindicador_tipo_Jsonclick ;
      private String cmbIndicador_Tipo_Internalname ;
      private String cmbIndicador_Tipo_Jsonclick ;
      private String AV15Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode227 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool n2062Indicador_NaoCnfCod ;
      private bool n2061Indicador_AreaTrabalhoCod ;
      private bool toggleJsOutput ;
      private bool n2063Indicador_Refer ;
      private bool n2064Indicador_Aplica ;
      private bool n2065Indicador_Tipo ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbIndicador_Refer ;
      private GXCombobox cmbIndicador_Aplica ;
      private GXCombobox dynIndicador_NaoCnfCod ;
      private GXCombobox cmbIndicador_Tipo ;
      private IDataStoreProvider pr_default ;
      private int[] T00556_A2058Indicador_Codigo ;
      private String[] T00556_A2059Indicador_Nome ;
      private short[] T00556_A2063Indicador_Refer ;
      private bool[] T00556_n2063Indicador_Refer ;
      private short[] T00556_A2064Indicador_Aplica ;
      private bool[] T00556_n2064Indicador_Aplica ;
      private String[] T00556_A2065Indicador_Tipo ;
      private bool[] T00556_n2065Indicador_Tipo ;
      private int[] T00556_A2061Indicador_AreaTrabalhoCod ;
      private bool[] T00556_n2061Indicador_AreaTrabalhoCod ;
      private int[] T00556_A2062Indicador_NaoCnfCod ;
      private bool[] T00556_n2062Indicador_NaoCnfCod ;
      private int[] T00555_A2062Indicador_NaoCnfCod ;
      private bool[] T00555_n2062Indicador_NaoCnfCod ;
      private int[] T00554_A2061Indicador_AreaTrabalhoCod ;
      private bool[] T00554_n2061Indicador_AreaTrabalhoCod ;
      private int[] T00557_A2062Indicador_NaoCnfCod ;
      private bool[] T00557_n2062Indicador_NaoCnfCod ;
      private int[] T00558_A2061Indicador_AreaTrabalhoCod ;
      private bool[] T00558_n2061Indicador_AreaTrabalhoCod ;
      private int[] T00559_A2058Indicador_Codigo ;
      private int[] T00553_A2058Indicador_Codigo ;
      private String[] T00553_A2059Indicador_Nome ;
      private short[] T00553_A2063Indicador_Refer ;
      private bool[] T00553_n2063Indicador_Refer ;
      private short[] T00553_A2064Indicador_Aplica ;
      private bool[] T00553_n2064Indicador_Aplica ;
      private String[] T00553_A2065Indicador_Tipo ;
      private bool[] T00553_n2065Indicador_Tipo ;
      private int[] T00553_A2061Indicador_AreaTrabalhoCod ;
      private bool[] T00553_n2061Indicador_AreaTrabalhoCod ;
      private int[] T00553_A2062Indicador_NaoCnfCod ;
      private bool[] T00553_n2062Indicador_NaoCnfCod ;
      private int[] T005510_A2058Indicador_Codigo ;
      private int[] T005511_A2058Indicador_Codigo ;
      private int[] T00552_A2058Indicador_Codigo ;
      private String[] T00552_A2059Indicador_Nome ;
      private short[] T00552_A2063Indicador_Refer ;
      private bool[] T00552_n2063Indicador_Refer ;
      private short[] T00552_A2064Indicador_Aplica ;
      private bool[] T00552_n2064Indicador_Aplica ;
      private String[] T00552_A2065Indicador_Tipo ;
      private bool[] T00552_n2065Indicador_Tipo ;
      private int[] T00552_A2061Indicador_AreaTrabalhoCod ;
      private bool[] T00552_n2061Indicador_AreaTrabalhoCod ;
      private int[] T00552_A2062Indicador_NaoCnfCod ;
      private bool[] T00552_n2062Indicador_NaoCnfCod ;
      private int[] T005512_A2058Indicador_Codigo ;
      private int[] T005515_A2024ContagemResultadoNaoCnf_Codigo ;
      private int[] T005516_A2058Indicador_Codigo ;
      private int[] T005517_A426NaoConformidade_Codigo ;
      private String[] T005517_A427NaoConformidade_Nome ;
      private int[] T005517_A428NaoConformidade_AreaTrabalhoCod ;
      private int[] T005518_A2062Indicador_NaoCnfCod ;
      private bool[] T005518_n2062Indicador_NaoCnfCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV3WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtAuditingObject AV14AuditingObject ;
   }

   public class indicador__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00556 ;
          prmT00556 = new Object[] {
          new Object[] {"@Indicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00555 ;
          prmT00555 = new Object[] {
          new Object[] {"@Indicador_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00554 ;
          prmT00554 = new Object[] {
          new Object[] {"@Indicador_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00557 ;
          prmT00557 = new Object[] {
          new Object[] {"@Indicador_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00558 ;
          prmT00558 = new Object[] {
          new Object[] {"@Indicador_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00559 ;
          prmT00559 = new Object[] {
          new Object[] {"@Indicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00553 ;
          prmT00553 = new Object[] {
          new Object[] {"@Indicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005510 ;
          prmT005510 = new Object[] {
          new Object[] {"@Indicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005511 ;
          prmT005511 = new Object[] {
          new Object[] {"@Indicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00552 ;
          prmT00552 = new Object[] {
          new Object[] {"@Indicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005512 ;
          prmT005512 = new Object[] {
          new Object[] {"@Indicador_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Indicador_Refer",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Indicador_Aplica",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Indicador_Tipo",SqlDbType.Char,2,0} ,
          new Object[] {"@Indicador_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Indicador_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005513 ;
          prmT005513 = new Object[] {
          new Object[] {"@Indicador_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Indicador_Refer",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Indicador_Aplica",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Indicador_Tipo",SqlDbType.Char,2,0} ,
          new Object[] {"@Indicador_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Indicador_NaoCnfCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Indicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005514 ;
          prmT005514 = new Object[] {
          new Object[] {"@Indicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005515 ;
          prmT005515 = new Object[] {
          new Object[] {"@Indicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005516 ;
          prmT005516 = new Object[] {
          } ;
          Object[] prmT005517 ;
          prmT005517 = new Object[] {
          new Object[] {"@AV3WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005518 ;
          prmT005518 = new Object[] {
          new Object[] {"@Indicador_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00552", "SELECT [Indicador_Codigo], [Indicador_Nome], [Indicador_Refer], [Indicador_Aplica], [Indicador_Tipo], [Indicador_AreaTrabalhoCod] AS Indicador_AreaTrabalhoCod, [Indicador_NaoCnfCod] AS Indicador_NaoCnfCod FROM [Indicador] WITH (UPDLOCK) WHERE [Indicador_Codigo] = @Indicador_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00552,1,0,true,false )
             ,new CursorDef("T00553", "SELECT [Indicador_Codigo], [Indicador_Nome], [Indicador_Refer], [Indicador_Aplica], [Indicador_Tipo], [Indicador_AreaTrabalhoCod] AS Indicador_AreaTrabalhoCod, [Indicador_NaoCnfCod] AS Indicador_NaoCnfCod FROM [Indicador] WITH (NOLOCK) WHERE [Indicador_Codigo] = @Indicador_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00553,1,0,true,false )
             ,new CursorDef("T00554", "SELECT [AreaTrabalho_Codigo] AS Indicador_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Indicador_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00554,1,0,true,false )
             ,new CursorDef("T00555", "SELECT [NaoConformidade_Codigo] AS Indicador_NaoCnfCod FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @Indicador_NaoCnfCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00555,1,0,true,false )
             ,new CursorDef("T00556", "SELECT TM1.[Indicador_Codigo], TM1.[Indicador_Nome], TM1.[Indicador_Refer], TM1.[Indicador_Aplica], TM1.[Indicador_Tipo], TM1.[Indicador_AreaTrabalhoCod] AS Indicador_AreaTrabalhoCod, TM1.[Indicador_NaoCnfCod] AS Indicador_NaoCnfCod FROM [Indicador] TM1 WITH (NOLOCK) WHERE TM1.[Indicador_Codigo] = @Indicador_Codigo ORDER BY TM1.[Indicador_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00556,100,0,true,false )
             ,new CursorDef("T00557", "SELECT [NaoConformidade_Codigo] AS Indicador_NaoCnfCod FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @Indicador_NaoCnfCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00557,1,0,true,false )
             ,new CursorDef("T00558", "SELECT [AreaTrabalho_Codigo] AS Indicador_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Indicador_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00558,1,0,true,false )
             ,new CursorDef("T00559", "SELECT [Indicador_Codigo] FROM [Indicador] WITH (NOLOCK) WHERE [Indicador_Codigo] = @Indicador_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00559,1,0,true,false )
             ,new CursorDef("T005510", "SELECT TOP 1 [Indicador_Codigo] FROM [Indicador] WITH (NOLOCK) WHERE ( [Indicador_Codigo] > @Indicador_Codigo) ORDER BY [Indicador_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005510,1,0,true,true )
             ,new CursorDef("T005511", "SELECT TOP 1 [Indicador_Codigo] FROM [Indicador] WITH (NOLOCK) WHERE ( [Indicador_Codigo] < @Indicador_Codigo) ORDER BY [Indicador_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005511,1,0,true,true )
             ,new CursorDef("T005512", "INSERT INTO [Indicador]([Indicador_Nome], [Indicador_Refer], [Indicador_Aplica], [Indicador_Tipo], [Indicador_AreaTrabalhoCod], [Indicador_NaoCnfCod]) VALUES(@Indicador_Nome, @Indicador_Refer, @Indicador_Aplica, @Indicador_Tipo, @Indicador_AreaTrabalhoCod, @Indicador_NaoCnfCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT005512)
             ,new CursorDef("T005513", "UPDATE [Indicador] SET [Indicador_Nome]=@Indicador_Nome, [Indicador_Refer]=@Indicador_Refer, [Indicador_Aplica]=@Indicador_Aplica, [Indicador_Tipo]=@Indicador_Tipo, [Indicador_AreaTrabalhoCod]=@Indicador_AreaTrabalhoCod, [Indicador_NaoCnfCod]=@Indicador_NaoCnfCod  WHERE [Indicador_Codigo] = @Indicador_Codigo", GxErrorMask.GX_NOMASK,prmT005513)
             ,new CursorDef("T005514", "DELETE FROM [Indicador]  WHERE [Indicador_Codigo] = @Indicador_Codigo", GxErrorMask.GX_NOMASK,prmT005514)
             ,new CursorDef("T005515", "SELECT TOP 1 [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) WHERE [ContagemResultadoNaoCnf_IndCod] = @Indicador_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005515,1,0,true,true )
             ,new CursorDef("T005516", "SELECT [Indicador_Codigo] FROM [Indicador] WITH (NOLOCK) ORDER BY [Indicador_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005516,100,0,true,false )
             ,new CursorDef("T005517", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Nome], [NaoConformidade_AreaTrabalhoCod] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_AreaTrabalhoCod] = @AV3WWPCo_1Areatrabalho_codigo ORDER BY [NaoConformidade_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005517,0,0,true,false )
             ,new CursorDef("T005518", "SELECT [NaoConformidade_Codigo] AS Indicador_NaoCnfCod FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @Indicador_NaoCnfCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005518,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 2) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 2) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 2) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                stmt.SetParameter(7, (int)parms[11]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
