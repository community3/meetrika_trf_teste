/*
               File: PRC_UPDPrioridadeOrdem
        Description: Update Prioridade Ordem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:16.44
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updprioridadeordem : GXProcedure
   {
      public prc_updprioridadeordem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updprioridadeordem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           bool aP1_Up )
      {
         this.AV11Codigo = aP0_Codigo;
         this.AV8Up = aP1_Up;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Codigo ,
                                 bool aP1_Up )
      {
         prc_updprioridadeordem objprc_updprioridadeordem;
         objprc_updprioridadeordem = new prc_updprioridadeordem();
         objprc_updprioridadeordem.AV11Codigo = aP0_Codigo;
         objprc_updprioridadeordem.AV8Up = aP1_Up;
         objprc_updprioridadeordem.context.SetSubmitInitialConfig(context);
         objprc_updprioridadeordem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updprioridadeordem);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updprioridadeordem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00X22 */
         pr_default.execute(0, new Object[] {AV11Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1336ContratoServicosPrioridade_Codigo = P00X22_A1336ContratoServicosPrioridade_Codigo[0];
            A2066ContratoServicosPrioridade_Ordem = P00X22_A2066ContratoServicosPrioridade_Ordem[0];
            n2066ContratoServicosPrioridade_Ordem = P00X22_n2066ContratoServicosPrioridade_Ordem[0];
            A1335ContratoServicosPrioridade_CntSrvCod = P00X22_A1335ContratoServicosPrioridade_CntSrvCod[0];
            if ( AV8Up )
            {
               A2066ContratoServicosPrioridade_Ordem = (short)(A2066ContratoServicosPrioridade_Ordem-1);
               n2066ContratoServicosPrioridade_Ordem = false;
            }
            else
            {
               A2066ContratoServicosPrioridade_Ordem = (short)(A2066ContratoServicosPrioridade_Ordem+1);
               n2066ContratoServicosPrioridade_Ordem = false;
            }
            AV10CntSrvCodigo = A1335ContratoServicosPrioridade_CntSrvCod;
            AV9Ordem = A2066ContratoServicosPrioridade_Ordem;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00X23 */
            pr_default.execute(1, new Object[] {n2066ContratoServicosPrioridade_Ordem, A2066ContratoServicosPrioridade_Ordem, A1336ContratoServicosPrioridade_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
            if (true) break;
            /* Using cursor P00X24 */
            pr_default.execute(2, new Object[] {n2066ContratoServicosPrioridade_Ordem, A2066ContratoServicosPrioridade_Ordem, A1336ContratoServicosPrioridade_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P00X25 */
         pr_default.execute(3, new Object[] {AV10CntSrvCodigo, AV11Codigo, AV9Ordem});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A1336ContratoServicosPrioridade_Codigo = P00X25_A1336ContratoServicosPrioridade_Codigo[0];
            A2066ContratoServicosPrioridade_Ordem = P00X25_A2066ContratoServicosPrioridade_Ordem[0];
            n2066ContratoServicosPrioridade_Ordem = P00X25_n2066ContratoServicosPrioridade_Ordem[0];
            A1335ContratoServicosPrioridade_CntSrvCod = P00X25_A1335ContratoServicosPrioridade_CntSrvCod[0];
            if ( AV8Up )
            {
               A2066ContratoServicosPrioridade_Ordem = (short)(A2066ContratoServicosPrioridade_Ordem+1);
               n2066ContratoServicosPrioridade_Ordem = false;
            }
            else
            {
               A2066ContratoServicosPrioridade_Ordem = (short)(A2066ContratoServicosPrioridade_Ordem-1);
               n2066ContratoServicosPrioridade_Ordem = false;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00X26 */
            pr_default.execute(4, new Object[] {n2066ContratoServicosPrioridade_Ordem, A2066ContratoServicosPrioridade_Ordem, A1336ContratoServicosPrioridade_Codigo});
            pr_default.close(4);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
            if (true) break;
            /* Using cursor P00X27 */
            pr_default.execute(5, new Object[] {n2066ContratoServicosPrioridade_Ordem, A2066ContratoServicosPrioridade_Ordem, A1336ContratoServicosPrioridade_Codigo});
            pr_default.close(5);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
            pr_default.readNext(3);
         }
         pr_default.close(3);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UPDPrioridadeOrdem");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00X22_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         P00X22_A2066ContratoServicosPrioridade_Ordem = new short[1] ;
         P00X22_n2066ContratoServicosPrioridade_Ordem = new bool[] {false} ;
         P00X22_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         P00X25_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         P00X25_A2066ContratoServicosPrioridade_Ordem = new short[1] ;
         P00X25_n2066ContratoServicosPrioridade_Ordem = new bool[] {false} ;
         P00X25_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updprioridadeordem__default(),
            new Object[][] {
                new Object[] {
               P00X22_A1336ContratoServicosPrioridade_Codigo, P00X22_A2066ContratoServicosPrioridade_Ordem, P00X22_n2066ContratoServicosPrioridade_Ordem, P00X22_A1335ContratoServicosPrioridade_CntSrvCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00X25_A1336ContratoServicosPrioridade_Codigo, P00X25_A2066ContratoServicosPrioridade_Ordem, P00X25_n2066ContratoServicosPrioridade_Ordem, P00X25_A1335ContratoServicosPrioridade_CntSrvCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A2066ContratoServicosPrioridade_Ordem ;
      private short AV9Ordem ;
      private int AV11Codigo ;
      private int A1336ContratoServicosPrioridade_Codigo ;
      private int A1335ContratoServicosPrioridade_CntSrvCod ;
      private int AV10CntSrvCodigo ;
      private String scmdbuf ;
      private bool AV8Up ;
      private bool n2066ContratoServicosPrioridade_Ordem ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00X22_A1336ContratoServicosPrioridade_Codigo ;
      private short[] P00X22_A2066ContratoServicosPrioridade_Ordem ;
      private bool[] P00X22_n2066ContratoServicosPrioridade_Ordem ;
      private int[] P00X22_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] P00X25_A1336ContratoServicosPrioridade_Codigo ;
      private short[] P00X25_A2066ContratoServicosPrioridade_Ordem ;
      private bool[] P00X25_n2066ContratoServicosPrioridade_Ordem ;
      private int[] P00X25_A1335ContratoServicosPrioridade_CntSrvCod ;
   }

   public class prc_updprioridadeordem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00X22 ;
          prmP00X22 = new Object[] {
          new Object[] {"@AV11Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X23 ;
          prmP00X23 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X24 ;
          prmP00X24 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X25 ;
          prmP00X25 = new Object[] {
          new Object[] {"@AV10CntSrvCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Ordem",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmP00X26 ;
          prmP00X26 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X27 ;
          prmP00X27 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00X22", "SELECT TOP 1 [ContratoServicosPrioridade_Codigo], [ContratoServicosPrioridade_Ordem], [ContratoServicosPrioridade_CntSrvCod] FROM [ContratoServicosPrioridade] WITH (UPDLOCK) WHERE [ContratoServicosPrioridade_Codigo] = @AV11Codigo ORDER BY [ContratoServicosPrioridade_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00X22,1,0,true,true )
             ,new CursorDef("P00X23", "UPDATE [ContratoServicosPrioridade] SET [ContratoServicosPrioridade_Ordem]=@ContratoServicosPrioridade_Ordem  WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X23)
             ,new CursorDef("P00X24", "UPDATE [ContratoServicosPrioridade] SET [ContratoServicosPrioridade_Ordem]=@ContratoServicosPrioridade_Ordem  WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X24)
             ,new CursorDef("P00X25", "SELECT TOP 1 [ContratoServicosPrioridade_Codigo], [ContratoServicosPrioridade_Ordem], [ContratoServicosPrioridade_CntSrvCod] FROM [ContratoServicosPrioridade] WITH (UPDLOCK) WHERE ([ContratoServicosPrioridade_CntSrvCod] = @AV10CntSrvCodigo) AND ([ContratoServicosPrioridade_Codigo] <> @AV11Codigo) AND ([ContratoServicosPrioridade_Ordem] = @AV9Ordem) ORDER BY [ContratoServicosPrioridade_CntSrvCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00X25,1,0,true,true )
             ,new CursorDef("P00X26", "UPDATE [ContratoServicosPrioridade] SET [ContratoServicosPrioridade_Ordem]=@ContratoServicosPrioridade_Ordem  WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X26)
             ,new CursorDef("P00X27", "UPDATE [ContratoServicosPrioridade] SET [ContratoServicosPrioridade_Ordem]=@ContratoServicosPrioridade_Ordem  WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X27)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
