/*
               File: type_SdtSDT_RedmineIssuePut
        Description: SDT_RedmineIssuePut
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:6.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_RedmineIssuePut" )]
   [XmlType(TypeName =  "SDT_RedmineIssuePut" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineIssuePut_Issue ))]
   [Serializable]
   public class SdtSDT_RedmineIssuePut : GxUserType
   {
      public SdtSDT_RedmineIssuePut( )
      {
         /* Constructor for serialization */
      }

      public SdtSDT_RedmineIssuePut( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_RedmineIssuePut deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_RedmineIssuePut)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_RedmineIssuePut obj ;
         obj = this;
         obj.gxTpr_Issue = deserialized.gxTpr_Issue;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Issue") )
               {
                  if ( gxTv_SdtSDT_RedmineIssuePut_Issue == null )
                  {
                     gxTv_SdtSDT_RedmineIssuePut_Issue = new SdtSDT_RedmineIssuePut_Issue(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_RedmineIssuePut_Issue.readxml(oReader, "Issue");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_RedmineIssuePut";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         if ( gxTv_SdtSDT_RedmineIssuePut_Issue != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtSDT_RedmineIssuePut_Issue.writexml(oWriter, "Issue", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         if ( gxTv_SdtSDT_RedmineIssuePut_Issue != null )
         {
            AddObjectProperty("Issue", gxTv_SdtSDT_RedmineIssuePut_Issue, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Issue" )]
      [  XmlElement( ElementName = "Issue"   )]
      public SdtSDT_RedmineIssuePut_Issue gxTpr_Issue
      {
         get {
            if ( gxTv_SdtSDT_RedmineIssuePut_Issue == null )
            {
               gxTv_SdtSDT_RedmineIssuePut_Issue = new SdtSDT_RedmineIssuePut_Issue(context);
            }
            return gxTv_SdtSDT_RedmineIssuePut_Issue ;
         }

         set {
            gxTv_SdtSDT_RedmineIssuePut_Issue = value;
         }

      }

      public void gxTv_SdtSDT_RedmineIssuePut_Issue_SetNull( )
      {
         gxTv_SdtSDT_RedmineIssuePut_Issue = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineIssuePut_Issue_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineIssuePut_Issue == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected SdtSDT_RedmineIssuePut_Issue gxTv_SdtSDT_RedmineIssuePut_Issue=null ;
   }

   [DataContract(Name = @"SDT_RedmineIssuePut", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_RedmineIssuePut_RESTInterface : GxGenericCollectionItem<SdtSDT_RedmineIssuePut>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_RedmineIssuePut_RESTInterface( ) : base()
      {
      }

      public SdtSDT_RedmineIssuePut_RESTInterface( SdtSDT_RedmineIssuePut psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Issue" , Order = 0 )]
      public SdtSDT_RedmineIssuePut_Issue_RESTInterface gxTpr_Issue
      {
         get {
            return new SdtSDT_RedmineIssuePut_Issue_RESTInterface(sdt.gxTpr_Issue) ;
         }

         set {
            sdt.gxTpr_Issue = value.sdt;
         }

      }

      public SdtSDT_RedmineIssuePut sdt
      {
         get {
            return (SdtSDT_RedmineIssuePut)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_RedmineIssuePut() ;
         }
      }

   }

}
