/*
               File: ContagemItem
        Description: Contagem Item
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:9:16.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemitem : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel6"+"_"+"CONTAGEMITEM_FMAR") == 0 )
         {
            A224ContagemItem_Lancamento = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX6ASACONTAGEMITEM_FMAR1744( A224ContagemItem_Lancamento) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel7"+"_"+"CONTAGEMITEM_FSAR") == 0 )
         {
            A224ContagemItem_Lancamento = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX7ASACONTAGEMITEM_FSAR1744( A224ContagemItem_Lancamento) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel8"+"_"+"FUNCAOAPF_COMPLEXIDADE") == 0 )
         {
            A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n165FuncaoAPF_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX8ASAFUNCAOAPF_COMPLEXIDADE1744( A165FuncaoAPF_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_50") == 0 )
         {
            A224ContagemItem_Lancamento = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_50( A224ContagemItem_Lancamento) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_44") == 0 )
         {
            A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n165FuncaoAPF_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_44( A165FuncaoAPF_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_45") == 0 )
         {
            A226ContagemItem_FSReferenciaTecnicaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n226ContagemItem_FSReferenciaTecnicaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A226ContagemItem_FSReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_45( A226ContagemItem_FSReferenciaTecnicaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_46") == 0 )
         {
            A255ContagemItem_FMReferenciaTecnicaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n255ContagemItem_FMReferenciaTecnicaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A255ContagemItem_FMReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_46( A255ContagemItem_FMReferenciaTecnicaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContagemItem_Lancamento = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemItem_Lancamento), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMITEM_LANCAMENTO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemItem_Lancamento), "ZZZZZ9")));
               A192Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContagemItem_TipoUnidade.Name = "CONTAGEMITEM_TIPOUNIDADE";
         cmbContagemItem_TipoUnidade.WebTags = "";
         if ( cmbContagemItem_TipoUnidade.ItemCount > 0 )
         {
            A952ContagemItem_TipoUnidade = cmbContagemItem_TipoUnidade.getValidValue(A952ContagemItem_TipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A952ContagemItem_TipoUnidade", A952ContagemItem_TipoUnidade);
         }
         cmbContagem_Tecnica.Name = "CONTAGEM_TECNICA";
         cmbContagem_Tecnica.WebTags = "";
         cmbContagem_Tecnica.addItem("", "Nenhuma", 0);
         cmbContagem_Tecnica.addItem("1", "Indicativa", 0);
         cmbContagem_Tecnica.addItem("2", "Estimada", 0);
         cmbContagem_Tecnica.addItem("3", "Detalhada", 0);
         if ( cmbContagem_Tecnica.ItemCount > 0 )
         {
            A195Contagem_Tecnica = cmbContagem_Tecnica.getValidValue(A195Contagem_Tecnica);
            n195Contagem_Tecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
         }
         cmbContagem_Tipo.Name = "CONTAGEM_TIPO";
         cmbContagem_Tipo.WebTags = "";
         cmbContagem_Tipo.addItem("", "(Nenhum)", 0);
         cmbContagem_Tipo.addItem("D", "Projeto de Desenvolvimento", 0);
         cmbContagem_Tipo.addItem("M", "Projeto de Melhor�a", 0);
         cmbContagem_Tipo.addItem("C", "Projeto de Contagem", 0);
         cmbContagem_Tipo.addItem("A", "Aplica��o", 0);
         if ( cmbContagem_Tipo.ItemCount > 0 )
         {
            A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
            n196Contagem_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A196Contagem_Tipo", A196Contagem_Tipo);
         }
         cmbContagemItem_ValidacaoStatusFinal.Name = "CONTAGEMITEM_VALIDACAOSTATUSFINAL";
         cmbContagemItem_ValidacaoStatusFinal.WebTags = "";
         cmbContagemItem_ValidacaoStatusFinal.addItem("1", "Aprovado", 0);
         cmbContagemItem_ValidacaoStatusFinal.addItem("2", "N�o Aprovado", 0);
         if ( cmbContagemItem_ValidacaoStatusFinal.ItemCount > 0 )
         {
            A260ContagemItem_ValidacaoStatusFinal = (short)(NumberUtil.Val( cmbContagemItem_ValidacaoStatusFinal.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
         }
         cmbFuncaoAPF_Tipo.Name = "FUNCAOAPF_TIPO";
         cmbFuncaoAPF_Tipo.WebTags = "";
         cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
         cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
         cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
         cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
         cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
         if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
         {
            A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
         }
         cmbFuncaoAPF_Complexidade.Name = "FUNCAOAPF_COMPLEXIDADE";
         cmbFuncaoAPF_Complexidade.WebTags = "";
         cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
         cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
         cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
         cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
         if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
         {
            A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         }
         cmbContagemItem_FSValidacao.Name = "CONTAGEMITEM_FSVALIDACAO";
         cmbContagemItem_FSValidacao.WebTags = "";
         cmbContagemItem_FSValidacao.addItem("1", "Aprovado", 0);
         cmbContagemItem_FSValidacao.addItem("2", "N�o Aprovado", 0);
         if ( cmbContagemItem_FSValidacao.ItemCount > 0 )
         {
            A267ContagemItem_FSValidacao = (short)(NumberUtil.Val( cmbContagemItem_FSValidacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0))), "."));
            n267ContagemItem_FSValidacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A267ContagemItem_FSValidacao", StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0));
         }
         cmbContagemItem_FSTipoUnidade.Name = "CONTAGEMITEM_FSTIPOUNIDADE";
         cmbContagemItem_FSTipoUnidade.WebTags = "";
         cmbContagemItem_FSTipoUnidade.addItem("EE", "Entrada Externa - EE", 0);
         cmbContagemItem_FSTipoUnidade.addItem("CE", "Consulta Externa - CE", 0);
         cmbContagemItem_FSTipoUnidade.addItem("SE", "Sa�da Externa - SE", 0);
         cmbContagemItem_FSTipoUnidade.addItem("ALI", "Arquivo L�gico Interno - ALI", 0);
         cmbContagemItem_FSTipoUnidade.addItem("aie", "Arquivo de Interface Externa - AIE", 0);
         cmbContagemItem_FSTipoUnidade.addItem("DC", "Dados de C�digo - DC", 0);
         if ( cmbContagemItem_FSTipoUnidade.ItemCount > 0 )
         {
            A228ContagemItem_FSTipoUnidade = cmbContagemItem_FSTipoUnidade.getValidValue(A228ContagemItem_FSTipoUnidade);
            n228ContagemItem_FSTipoUnidade = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A228ContagemItem_FSTipoUnidade", A228ContagemItem_FSTipoUnidade);
         }
         cmbContagemItem_FMValidacao.Name = "CONTAGEMITEM_FMVALIDACAO";
         cmbContagemItem_FMValidacao.WebTags = "";
         cmbContagemItem_FMValidacao.addItem("1", "Aprovado", 0);
         cmbContagemItem_FMValidacao.addItem("2", "N�o Aprovado", 0);
         if ( cmbContagemItem_FMValidacao.ItemCount > 0 )
         {
            A268ContagemItem_FMValidacao = (short)(NumberUtil.Val( cmbContagemItem_FMValidacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0))), "."));
            n268ContagemItem_FMValidacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A268ContagemItem_FMValidacao", StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Item", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemItem_PFB_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemitem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemitem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContagemItem_Lancamento ,
                           ref int aP2_Contagem_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContagemItem_Lancamento = aP1_ContagemItem_Lancamento;
         this.A192Contagem_Codigo = aP2_Contagem_Codigo;
         executePrivate();
         aP2_Contagem_Codigo=this.A192Contagem_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContagemItem_TipoUnidade = new GXCombobox();
         cmbContagem_Tecnica = new GXCombobox();
         cmbContagem_Tipo = new GXCombobox();
         cmbContagemItem_ValidacaoStatusFinal = new GXCombobox();
         cmbFuncaoAPF_Tipo = new GXCombobox();
         cmbFuncaoAPF_Complexidade = new GXCombobox();
         cmbContagemItem_FSValidacao = new GXCombobox();
         cmbContagemItem_FSTipoUnidade = new GXCombobox();
         cmbContagemItem_FMValidacao = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContagemItem_TipoUnidade.ItemCount > 0 )
         {
            A952ContagemItem_TipoUnidade = cmbContagemItem_TipoUnidade.getValidValue(A952ContagemItem_TipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A952ContagemItem_TipoUnidade", A952ContagemItem_TipoUnidade);
         }
         if ( cmbContagem_Tecnica.ItemCount > 0 )
         {
            A195Contagem_Tecnica = cmbContagem_Tecnica.getValidValue(A195Contagem_Tecnica);
            n195Contagem_Tecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
         }
         if ( cmbContagem_Tipo.ItemCount > 0 )
         {
            A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
            n196Contagem_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A196Contagem_Tipo", A196Contagem_Tipo);
         }
         if ( cmbContagemItem_ValidacaoStatusFinal.ItemCount > 0 )
         {
            A260ContagemItem_ValidacaoStatusFinal = (short)(NumberUtil.Val( cmbContagemItem_ValidacaoStatusFinal.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
         }
         if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
         {
            A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
         }
         if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
         {
            A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         }
         if ( cmbContagemItem_FSValidacao.ItemCount > 0 )
         {
            A267ContagemItem_FSValidacao = (short)(NumberUtil.Val( cmbContagemItem_FSValidacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0))), "."));
            n267ContagemItem_FSValidacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A267ContagemItem_FSValidacao", StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0));
         }
         if ( cmbContagemItem_FSTipoUnidade.ItemCount > 0 )
         {
            A228ContagemItem_FSTipoUnidade = cmbContagemItem_FSTipoUnidade.getValidValue(A228ContagemItem_FSTipoUnidade);
            n228ContagemItem_FSTipoUnidade = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A228ContagemItem_FSTipoUnidade", A228ContagemItem_FSTipoUnidade);
         }
         if ( cmbContagemItem_FMValidacao.ItemCount > 0 )
         {
            A268ContagemItem_FMValidacao = (short)(NumberUtil.Val( cmbContagemItem_FMValidacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0))), "."));
            n268ContagemItem_FMValidacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A268ContagemItem_FMValidacao", StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1744( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 324,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioAuditorCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0, ",", "")), ((edtContagem_UsuarioAuditorCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A216Contagem_UsuarioAuditorCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A216Contagem_UsuarioAuditorCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,324);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioAuditorCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_UsuarioAuditorCod_Visible, edtContagem_UsuarioAuditorCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 325,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioAuditorPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0, ",", "")), ((edtContagem_UsuarioAuditorPessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,325);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioAuditorPessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_UsuarioAuditorPessoaCod_Visible, edtContagem_UsuarioAuditorPessoaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 326,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_FabricaSoftwareCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0, ",", "")), ((edtContagem_FabricaSoftwareCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A207Contagem_FabricaSoftwareCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A207Contagem_FabricaSoftwareCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,326);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_FabricaSoftwareCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_FabricaSoftwareCod_Visible, edtContagem_FabricaSoftwareCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 327,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_FabricaSoftwarePessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0, ",", "")), ((edtContagem_FabricaSoftwarePessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,327);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_FabricaSoftwarePessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_FabricaSoftwarePessoaCod_Visible, edtContagem_FabricaSoftwarePessoaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemItem.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioContadorCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0, ",", "")), ((edtContagem_UsuarioContadorCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioContadorCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_UsuarioContadorCod_Visible, edtContagem_UsuarioContadorCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItem.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioContadorPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0, ",", "")), ((edtContagem_UsuarioContadorPessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A214Contagem_UsuarioContadorPessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A214Contagem_UsuarioContadorPessoaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioContadorPessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_UsuarioContadorPessoaCod_Visible, edtContagem_UsuarioContadorPessoaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItem.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemitemtitle_Internalname, "Contagem Item", "", "", lblContagemitemtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_1744( true) ;
         }
         return  ;
      }

      protected void wb_table2_8_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_316_1744( true) ;
         }
         return  ;
      }

      protected void wb_table3_316_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1744e( true) ;
         }
         else
         {
            wb_table1_2_1744e( false) ;
         }
      }

      protected void wb_table3_316_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 319,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 321,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 323,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_316_1744e( true) ;
         }
         else
         {
            wb_table3_316_1744e( false) ;
         }
      }

      protected void wb_table2_8_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_1744( true) ;
         }
         return  ;
      }

      protected void wb_table4_16_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_1744e( true) ;
         }
         else
         {
            wb_table2_8_1744e( false) ;
         }
      }

      protected void wb_table4_16_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_datacriacao_Internalname, "Data Cria��o", "", "", lblTextblockcontagem_datacriacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagem_DataCriacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagem_DataCriacao_Internalname, context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"), context.localUtil.Format( A197Contagem_DataCriacao, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_DataCriacao_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContagem_DataCriacao_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContagemItem.htm");
            GxWebStd.gx_bitmap( context, edtContagem_DataCriacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagem_DataCriacao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemItem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_areatrabalhocod_Internalname, "C�d. �rea de Trabalho", "", "", lblTextblockcontagem_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0, ",", "")), ((edtContagem_AreaTrabalhoCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_AreaTrabalhoCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_AreaTrabalhoCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_areatrabalhodes_Internalname, "�rea de Trabalho", "", "", lblTextblockcontagem_areatrabalhodes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_AreaTrabalhoDes_Internalname, A194Contagem_AreaTrabalhoDes, StringUtil.RTrim( context.localUtil.Format( A194Contagem_AreaTrabalhoDes, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_AreaTrabalhoDes_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_AreaTrabalhoDes_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_pfb_Internalname, "PFB", "", "", lblTextblockcontagemitem_pfb_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_PFB_Internalname, StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ",", "")), ((edtContagemItem_PFB_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_PFB_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_PFB_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_pfl_Internalname, "PFL", "", "", lblTextblockcontagemitem_pfl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_PFL_Internalname, StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ",", "")), ((edtContagemItem_PFL_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_PFL_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_PFL_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_tipounidade_Internalname, "Tipo", "", "", lblTextblockcontagemitem_tipounidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemItem_TipoUnidade, cmbContagemItem_TipoUnidade_Internalname, StringUtil.RTrim( A952ContagemItem_TipoUnidade), 1, cmbContagemItem_TipoUnidade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "svchar", "", 1, cmbContagemItem_TipoUnidade.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_ContagemItem.htm");
            cmbContagemItem_TipoUnidade.CurrentValue = StringUtil.RTrim( A952ContagemItem_TipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_TipoUnidade_Internalname, "Values", (String)(cmbContagemItem_TipoUnidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_evidencias_Internalname, "Evidencias", "", "", lblTextblockcontagemitem_evidencias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemItem_Evidencias_Internalname, A953ContagemItem_Evidencias, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", 0, 1, edtContagemItem_Evidencias_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "5000", -1, "", "", -1, true, "Evidencias", "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_qtdinm_Internalname, "INM", "", "", lblTextblockcontagemitem_qtdinm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_QtdINM_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A954ContagemItem_QtdINM), 4, 0, ",", "")), ((edtContagemItem_QtdINM_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A954ContagemItem_QtdINM), "ZZZ9")) : context.localUtil.Format( (decimal)(A954ContagemItem_QtdINM), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_QtdINM_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_QtdINM_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_cp_Internalname, "CP", "", "", lblTextblockcontagemitem_cp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_CP_Internalname, StringUtil.RTrim( A955ContagemItem_CP), StringUtil.RTrim( context.localUtil.Format( A955ContagemItem_CP, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_CP_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_CP_Enabled, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "CP", "left", true, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_ra_Internalname, "A", "", "", lblTextblockcontagemitem_ra_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_RA_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A956ContagemItem_RA), 3, 0, ",", "")), ((edtContagemItem_RA_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A956ContagemItem_RA), "ZZ9")) : context.localUtil.Format( (decimal)(A956ContagemItem_RA), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_RA_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_RA_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_der_Internalname, "DER", "", "", lblTextblockcontagemitem_der_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_DER_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A957ContagemItem_DER), 3, 0, ",", "")), ((edtContagemItem_DER_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A957ContagemItem_DER), "ZZ9")) : context.localUtil.Format( (decimal)(A957ContagemItem_DER), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_DER_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_DER_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_funcao_Internalname, "de Dados", "", "", lblTextblockcontagemitem_funcao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_Funcao_Internalname, A958ContagemItem_Funcao, StringUtil.RTrim( context.localUtil.Format( A958ContagemItem_Funcao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_Funcao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_Funcao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_79_1744( true) ;
         }
         return  ;
      }

      protected void wb_table5_79_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_1744e( true) ;
         }
         else
         {
            wb_table4_16_1744e( false) ;
         }
      }

      protected void wb_table5_79_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_82_1744( true) ;
         }
         return  ;
      }

      protected void wb_table6_82_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_79_1744e( true) ;
         }
         else
         {
            wb_table5_79_1744e( false) ;
         }
      }

      protected void wb_table6_82_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_85_1744( true) ;
         }
         return  ;
      }

      protected void wb_table7_85_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_141_1744( true) ;
         }
         return  ;
      }

      protected void wb_table8_141_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_82_1744e( true) ;
         }
         else
         {
            wb_table6_82_1744e( false) ;
         }
      }

      protected void wb_table8_141_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable4_Internalname, tblUnnamedtable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table9_144_1744( true) ;
         }
         return  ;
      }

      protected void wb_table9_144_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table10_235_1744( true) ;
         }
         return  ;
      }

      protected void wb_table10_235_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table11_239_1744( true) ;
         }
         return  ;
      }

      protected void wb_table11_239_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_141_1744e( true) ;
         }
         else
         {
            wb_table8_141_1744e( false) ;
         }
      }

      protected void wb_table11_239_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablefm_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablefm_Internalname, tblTablefm_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 242,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbtnassociaratributosfm_Internalname, "", "Sel. Atributos FM", bttBtnbtnassociaratributosfm_Jsonclick, 7, "Clique aqui p/ Selecionar os Atributos da F�brica de Software!", "", StyleString, ClassString, bttBtnbtnassociaratributosfm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"e111744_client"+"'", TempTags, "", 2, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup8_Internalname, "F�brica de M�trica", 1, 0, "px", 0, "px", "Group", "", "HLP_ContagemItem.htm");
            wb_table12_246_1744( true) ;
         }
         return  ;
      }

      protected void wb_table12_246_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_239_1744e( true) ;
         }
         else
         {
            wb_table11_239_1744e( false) ;
         }
      }

      protected void wb_table12_246_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable7_Internalname, tblUnnamedtable7_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_usuarioauditorpessoanom_Internalname, "Auditor", "", "", lblTextblockcontagem_usuarioauditorpessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 251,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioAuditorPessoaNom_Internalname, StringUtil.RTrim( A218Contagem_UsuarioAuditorPessoaNom), StringUtil.RTrim( context.localUtil.Format( A218Contagem_UsuarioAuditorPessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,251);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioAuditorPessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_UsuarioAuditorPessoaNom_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmvalidacao_Internalname, "Valida��o", "", "", lblTextblockcontagemitem_fmvalidacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 256,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemItem_FMValidacao, cmbContagemItem_FMValidacao_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0)), 1, cmbContagemItem_FMValidacao_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContagemItem_FMValidacao.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,256);\"", "", true, "HLP_ContagemItem.htm");
            cmbContagemItem_FMValidacao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_FMValidacao_Internalname, "Values", (String)(cmbContagemItem_FMValidacao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmreferenciatecnicacod_Internalname, "Refer�ncia", "", "", lblTextblockcontagemitem_fmreferenciatecnicacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table13_261_1744( true) ;
         }
         return  ;
      }

      protected void wb_table13_261_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmreferenciatecnicaval_Internalname, "Deflator/Inflator", "", "", lblTextblockcontagemitem_fmreferenciatecnicaval_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMReferenciaTecnicaVal_Internalname, StringUtil.LTrim( StringUtil.NToC( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5, ",", "")), ((edtContagemItem_FMReferenciaTecnicaVal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A256ContagemItem_FMReferenciaTecnicaVal, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A256ContagemItem_FMReferenciaTecnicaVal, "ZZZ,ZZZ,ZZZ,ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMReferenciaTecnicaVal_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FMReferenciaTecnicaVal_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmder_Internalname, "DER", "", "", lblTextblockcontagemitem_fmder_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table14_276_1744( true) ;
         }
         return  ;
      }

      protected void wb_table14_276_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmqtdinm_Internalname, "Qtd. INM", "", "", lblTextblockcontagemitem_fmqtdinm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table15_292_1744( true) ;
         }
         return  ;
      }

      protected void wb_table15_292_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmpontofuncaobruto_Internalname, "PF Bruto", "", "", lblTextblockcontagemitem_fmpontofuncaobruto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table16_304_1744( true) ;
         }
         return  ;
      }

      protected void wb_table16_304_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_246_1744e( true) ;
         }
         else
         {
            wb_table12_246_1744e( false) ;
         }
      }

      protected void wb_table16_304_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fmpontofuncaobruto_Internalname, tblTablemergedcontagemitem_fmpontofuncaobruto_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 307,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMPontoFuncaoBruto_Internalname, StringUtil.LTrim( StringUtil.NToC( A241ContagemItem_FMPontoFuncaoBruto, 7, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( A241ContagemItem_FMPontoFuncaoBruto, "ZZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,307);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMPontoFuncaoBruto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FMPontoFuncaoBruto_Enabled, 1, "text", "", 7, "chr", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "TotalPF", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmpontofuncaoliquido_Internalname, "PF Liquido", "", "", lblTextblockcontagemitem_fmpontofuncaoliquido_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 311,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMPontoFuncaoLiquido_Internalname, StringUtil.LTrim( StringUtil.NToC( A242ContagemItem_FMPontoFuncaoLiquido, 7, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( A242ContagemItem_FMPontoFuncaoLiquido, "ZZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,311);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMPontoFuncaoLiquido_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FMPontoFuncaoLiquido_Enabled, 1, "text", "", 7, "chr", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "TotalPF", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table16_304_1744e( true) ;
         }
         else
         {
            wb_table16_304_1744e( false) ;
         }
      }

      protected void wb_table15_292_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fmqtdinm_Internalname, tblTablemergedcontagemitem_fmqtdinm_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 295,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMQTDInm_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A239ContagemItem_FMQTDInm), 9, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A239ContagemItem_FMQTDInm), "ZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,295);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMQTDInm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FMQTDInm_Enabled, 1, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "QuantidadeGrande", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmcp_Internalname, "Complexidade", "", "", lblTextblockcontagemitem_fmcp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 299,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMCP_Internalname, StringUtil.RTrim( A240ContagemItem_FMCP), StringUtil.RTrim( context.localUtil.Format( A240ContagemItem_FMCP, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,299);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMCP_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FMCP_Enabled, 1, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "CP", "left", true, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_292_1744e( true) ;
         }
         else
         {
            wb_table15_292_1744e( false) ;
         }
      }

      protected void wb_table14_276_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fmder_Internalname, tblTablemergedcontagemitem_fmder_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMDER_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A251ContagemItem_FMDER), 3, 0, ",", "")), ((edtContagemItem_FMDER_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A251ContagemItem_FMDER), "ZZ9")) : context.localUtil.Format( (decimal)(A251ContagemItem_FMDER), "ZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMDER_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FMDER_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemitem_fmder_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemitem_fmder_righttext_Internalname, "(Atributos)", "", "", lblContagemitem_fmder_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmar_Internalname, "AR", "", "", lblTextblockcontagemitem_fmar_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMAR_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A252ContagemItem_FMAR), 3, 0, ",", "")), ((edtContagemItem_FMAR_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A252ContagemItem_FMAR), "ZZ9")) : context.localUtil.Format( (decimal)(A252ContagemItem_FMAR), "ZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMAR_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FMAR_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemitem_fmar_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemitem_fmar_righttext_Internalname, "(Tabelas)", "", "", lblContagemitem_fmar_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_276_1744e( true) ;
         }
         else
         {
            wb_table14_276_1744e( false) ;
         }
      }

      protected void wb_table13_261_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fmreferenciatecnicacod_Internalname, tblTablemergedcontagemitem_fmreferenciatecnicacod_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 264,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMReferenciaTecnicaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,264);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMReferenciaTecnicaCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FMReferenciaTecnicaCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItem.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_255_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_255_Link, "", "", context.GetTheme( ), imgprompt_255_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMReferenciaTecnicaNom_Internalname, StringUtil.RTrim( A257ContagemItem_FMReferenciaTecnicaNom), StringUtil.RTrim( context.localUtil.Format( A257ContagemItem_FMReferenciaTecnicaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMReferenciaTecnicaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FMReferenciaTecnicaNom_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_261_1744e( true) ;
         }
         else
         {
            wb_table13_261_1744e( false) ;
         }
      }

      protected void wb_table10_235_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable6_Internalname, tblUnnamedtable6_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_235_1744e( true) ;
         }
         else
         {
            wb_table10_235_1744e( false) ;
         }
      }

      protected void wb_table9_144_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable5_Internalname, tblUnnamedtable5_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbtnselecionaratributosfs_Internalname, "", "Sel. Atributos FS", bttBtnbtnselecionaratributosfs_Jsonclick, 7, "Clique aqui p/ Selecionar os Atributos da F�brica de Software!", "", StyleString, ClassString, bttBtnbtnselecionaratributosfs_Visible, bttBtnbtnselecionaratributosfs_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"e121744_client"+"'", TempTags, "", 2, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup10_Internalname, "F�brica de Software", 1, 0, "px", 0, "px", "Group", "", "HLP_ContagemItem.htm");
            wb_table17_151_1744( true) ;
         }
         return  ;
      }

      protected void wb_table17_151_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_144_1744e( true) ;
         }
         else
         {
            wb_table9_144_1744e( false) ;
         }
      }

      protected void wb_table17_151_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable9_Internalname, tblUnnamedtable9_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_fabricasoftwarepessoanom_Internalname, "Contratada", "", "", lblTextblockcontagem_fabricasoftwarepessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 156,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_FabricaSoftwarePessoaNom_Internalname, StringUtil.RTrim( A209Contagem_FabricaSoftwarePessoaNom), StringUtil.RTrim( context.localUtil.Format( A209Contagem_FabricaSoftwarePessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,156);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_FabricaSoftwarePessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_FabricaSoftwarePessoaNom_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_usuariocontadorpessoanom_Internalname, "Contador", "", "", lblTextblockcontagem_usuariocontadorpessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioContadorPessoaNom_Internalname, StringUtil.RTrim( A215Contagem_UsuarioContadorPessoaNom), StringUtil.RTrim( context.localUtil.Format( A215Contagem_UsuarioContadorPessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioContadorPessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_UsuarioContadorPessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsvalidacao_Internalname, "Valida��o", "", "", lblTextblockcontagemitem_fsvalidacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table18_166_1744( true) ;
         }
         return  ;
      }

      protected void wb_table18_166_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsreferenciatecnicacod_Internalname, "Refer�ncia", "", "", lblTextblockcontagemitem_fsreferenciatecnicacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table19_178_1744( true) ;
         }
         return  ;
      }

      protected void wb_table19_178_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsreferenciatecnicaval_Internalname, "Deflator/Inflator", "", "", lblTextblockcontagemitem_fsreferenciatecnicaval_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSReferenciaTecnicaVal_Internalname, StringUtil.LTrim( StringUtil.NToC( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5, ",", "")), ((edtContagemItem_FSReferenciaTecnicaVal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A227ContagemItem_FSReferenciaTecnicaVal, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A227ContagemItem_FSReferenciaTecnicaVal, "ZZZ,ZZZ,ZZZ,ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSReferenciaTecnicaVal_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FSReferenciaTecnicaVal_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsder_Internalname, "DER", "", "", lblTextblockcontagemitem_fsder_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table20_193_1744( true) ;
         }
         return  ;
      }

      protected void wb_table20_193_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsqtdinm_Internalname, "Qtd. INM", "", "", lblTextblockcontagemitem_fsqtdinm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table21_209_1744( true) ;
         }
         return  ;
      }

      protected void wb_table21_209_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fspontofuncaobruto_Internalname, "PF Bruto", "", "", lblTextblockcontagemitem_fspontofuncaobruto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table22_221_1744( true) ;
         }
         return  ;
      }

      protected void wb_table22_221_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsevidencias_Internalname, "Evid�ncias", "", "", lblTextblockcontagemitem_fsevidencias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 233,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemItem_FSEvidencias_Internalname, A238ContagemItem_FSEvidencias, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,233);\"", 0, 1, edtContagemItem_FSEvidencias_Enabled, 1, 80, "chr", 10, "row", StyleString, ClassString, "", "5000", -1, "", "", -1, true, "Evidencias", "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table17_151_1744e( true) ;
         }
         else
         {
            wb_table17_151_1744e( false) ;
         }
      }

      protected void wb_table22_221_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fspontofuncaobruto_Internalname, tblTablemergedcontagemitem_fspontofuncaobruto_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 224,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSPontoFuncaoBruto_Internalname, StringUtil.LTrim( StringUtil.NToC( A236ContagemItem_FSPontoFuncaoBruto, 7, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( A236ContagemItem_FSPontoFuncaoBruto, "ZZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,224);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSPontoFuncaoBruto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FSPontoFuncaoBruto_Enabled, 1, "text", "", 7, "chr", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "TotalPF", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fspontofuncaoliquido_Internalname, "PF L�quido", "", "", lblTextblockcontagemitem_fspontofuncaoliquido_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 228,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSPontoFuncaoLiquido_Internalname, StringUtil.LTrim( StringUtil.NToC( A237ContagemItem_FSPontoFuncaoLiquido, 7, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( A237ContagemItem_FSPontoFuncaoLiquido, "ZZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,228);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSPontoFuncaoLiquido_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FSPontoFuncaoLiquido_Enabled, 1, "text", "", 7, "chr", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "TotalPF", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table22_221_1744e( true) ;
         }
         else
         {
            wb_table22_221_1744e( false) ;
         }
      }

      protected void wb_table21_209_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fsqtdinm_Internalname, tblTablemergedcontagemitem_fsqtdinm_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 212,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSQTDInm_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A234ContagemItem_FSQTDInm), 9, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A234ContagemItem_FSQTDInm), "ZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,212);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSQTDInm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FSQTDInm_Enabled, 1, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "QuantidadeGrande", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fscp_Internalname, "Complexidade", "", "", lblTextblockcontagemitem_fscp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 216,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSCP_Internalname, StringUtil.RTrim( A235ContagemItem_FSCP), StringUtil.RTrim( context.localUtil.Format( A235ContagemItem_FSCP, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,216);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSCP_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FSCP_Enabled, 1, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "CP", "left", true, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table21_209_1744e( true) ;
         }
         else
         {
            wb_table21_209_1744e( false) ;
         }
      }

      protected void wb_table20_193_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fsder_Internalname, tblTablemergedcontagemitem_fsder_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSDER_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A232ContagemItem_FSDER), 3, 0, ",", "")), ((edtContagemItem_FSDER_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A232ContagemItem_FSDER), "ZZ9")) : context.localUtil.Format( (decimal)(A232ContagemItem_FSDER), "ZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSDER_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FSDER_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemitem_fsder_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemitem_fsder_righttext_Internalname, "(Atributos)", "", "", lblContagemitem_fsder_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsar_Internalname, "AR", "", "", lblTextblockcontagemitem_fsar_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSAR_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A233ContagemItem_FSAR), 3, 0, ",", "")), ((edtContagemItem_FSAR_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A233ContagemItem_FSAR), "ZZ9")) : context.localUtil.Format( (decimal)(A233ContagemItem_FSAR), "ZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSAR_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FSAR_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemitem_fsar_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemitem_fsar_righttext_Internalname, "(Tabelas)", "", "", lblContagemitem_fsar_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table20_193_1744e( true) ;
         }
         else
         {
            wb_table20_193_1744e( false) ;
         }
      }

      protected void wb_table19_178_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fsreferenciatecnicacod_Internalname, tblTablemergedcontagemitem_fsreferenciatecnicacod_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 181,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSReferenciaTecnicaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,181);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSReferenciaTecnicaCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FSReferenciaTecnicaCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItem.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_226_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_226_Link, "", "", context.GetTheme( ), imgprompt_226_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSReferenciaTecnicaNom_Internalname, StringUtil.RTrim( A269ContagemItem_FSReferenciaTecnicaNom), StringUtil.RTrim( context.localUtil.Format( A269ContagemItem_FSReferenciaTecnicaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSReferenciaTecnicaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_FSReferenciaTecnicaNom_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table19_178_1744e( true) ;
         }
         else
         {
            wb_table19_178_1744e( false) ;
         }
      }

      protected void wb_table18_166_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fsvalidacao_Internalname, tblTablemergedcontagemitem_fsvalidacao_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 169,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemItem_FSValidacao, cmbContagemItem_FSValidacao_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0)), 1, cmbContagemItem_FSValidacao_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContagemItem_FSValidacao.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,169);\"", "", true, "HLP_ContagemItem.htm");
            cmbContagemItem_FSValidacao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_FSValidacao_Internalname, "Values", (String)(cmbContagemItem_FSValidacao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fstipounidade_Internalname, "Unidade", "", "", lblTextblockcontagemitem_fstipounidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 173,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemItem_FSTipoUnidade, cmbContagemItem_FSTipoUnidade_Internalname, StringUtil.RTrim( A228ContagemItem_FSTipoUnidade), 1, cmbContagemItem_FSTipoUnidade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "svchar", "", 1, cmbContagemItem_FSTipoUnidade.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,173);\"", "", true, "HLP_ContagemItem.htm");
            cmbContagemItem_FSTipoUnidade.CurrentValue = StringUtil.RTrim( A228ContagemItem_FSTipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_FSTipoUnidade_Internalname, "Values", (String)(cmbContagemItem_FSTipoUnidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table18_166_1744e( true) ;
         }
         else
         {
            wb_table18_166_1744e( false) ;
         }
      }

      protected void wb_table7_85_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_lancamento_Internalname, "Lan�amento", "", "", lblTextblockcontagemitem_lancamento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_Lancamento_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ",", "")), ((edtContagemItem_Lancamento_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_Lancamento_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_Lancamento_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_codigo_Internalname, "Contagem", "", "", lblTextblockcontagem_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table23_95_1744( true) ;
         }
         return  ;
      }

      protected void wb_table23_95_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_codigo_Internalname, "Fun��o APF", "", "", lblTextblockfuncaoapf_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table24_119_1744( true) ;
         }
         return  ;
      }

      protected void wb_table24_119_1744e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_requisito_Internalname, "Requisito", "", "", lblTextblockcontagemitem_requisito_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_Requisito_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A229ContagemItem_Requisito), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A229ContagemItem_Requisito), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_Requisito_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_Requisito_Enabled, 1, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Requisito", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_85_1744e( true) ;
         }
         else
         {
            wb_table7_85_1744e( false) ;
         }
      }

      protected void wb_table24_119_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedfuncaoapf_codigo_Internalname, tblTablemergedfuncaoapf_codigo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncaoAPF_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItem.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_165_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_165_Link, "", "", context.GetTheme( ), imgprompt_165_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Nome_Internalname, A166FuncaoAPF_Nome, "", "", 0, 1, edtFuncaoAPF_Nome_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_tipo_Internalname, "Tipo", "", "", lblTextblockfuncaoapf_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_Tipo, cmbFuncaoAPF_Tipo_Internalname, StringUtil.RTrim( A184FuncaoAPF_Tipo), 1, cmbFuncaoAPF_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbFuncaoAPF_Tipo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContagemItem.htm");
            cmbFuncaoAPF_Tipo.CurrentValue = StringUtil.RTrim( A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Tipo_Internalname, "Values", (String)(cmbFuncaoAPF_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_complexidade_Internalname, "Complexidade", "", "", lblTextblockfuncaoapf_complexidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_Complexidade, cmbFuncaoAPF_Complexidade_Internalname, StringUtil.RTrim( A185FuncaoAPF_Complexidade), 1, cmbFuncaoAPF_Complexidade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbFuncaoAPF_Complexidade.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContagemItem.htm");
            cmbFuncaoAPF_Complexidade.CurrentValue = StringUtil.RTrim( A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Complexidade_Internalname, "Values", (String)(cmbFuncaoAPF_Complexidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table24_119_1744e( true) ;
         }
         else
         {
            wb_table24_119_1744e( false) ;
         }
      }

      protected void wb_table23_95_1744( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagem_codigo_Internalname, tblTablemergedcontagem_codigo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ",", "")), ((edtContagem_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_tecnica_Internalname, "T�cnica", "", "", lblTextblockcontagem_tecnica_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagem_Tecnica, cmbContagem_Tecnica_Internalname, StringUtil.RTrim( A195Contagem_Tecnica), 1, cmbContagem_Tecnica_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContagem_Tecnica.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContagemItem.htm");
            cmbContagem_Tecnica.CurrentValue = StringUtil.RTrim( A195Contagem_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tecnica_Internalname, "Values", (String)(cmbContagem_Tecnica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_tipo_Internalname, "Tipo", "", "", lblTextblockcontagem_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagem_Tipo, cmbContagem_Tipo_Internalname, StringUtil.RTrim( A196Contagem_Tipo), 1, cmbContagem_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContagem_Tipo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContagemItem.htm");
            cmbContagem_Tipo.CurrentValue = StringUtil.RTrim( A196Contagem_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tipo_Internalname, "Values", (String)(cmbContagem_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_sequencial_Internalname, "Sequencial", "", "", lblTextblockcontagemitem_sequencial_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_Sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A225ContagemItem_Sequencial), 3, 0, ",", "")), ((edtContagemItem_Sequencial_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")) : context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_Sequencial_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_Sequencial_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Sequencial", "right", false, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_validacaostatusfinal_Internalname, "Status FINAL do Item", "", "", lblTextblockcontagemitem_validacaostatusfinal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemItem_ValidacaoStatusFinal, cmbContagemItem_ValidacaoStatusFinal_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0)), 1, cmbContagemItem_ValidacaoStatusFinal_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContagemItem_ValidacaoStatusFinal.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContagemItem.htm");
            cmbContagemItem_ValidacaoStatusFinal.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_ValidacaoStatusFinal_Internalname, "Values", (String)(cmbContagemItem_ValidacaoStatusFinal.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table23_95_1744e( true) ;
         }
         else
         {
            wb_table23_95_1744e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13172 */
         E13172 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A197Contagem_DataCriacao = context.localUtil.CToD( cgiGet( edtContagem_DataCriacao_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
               A193Contagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_AreaTrabalhoCod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
               A194Contagem_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtContagem_AreaTrabalhoDes_Internalname));
               n194Contagem_AreaTrabalhoDes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_PFB_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_PFB_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_PFB");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_PFB_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A950ContagemItem_PFB = 0;
                  n950ContagemItem_PFB = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A950ContagemItem_PFB", StringUtil.LTrim( StringUtil.Str( A950ContagemItem_PFB, 14, 5)));
               }
               else
               {
                  A950ContagemItem_PFB = context.localUtil.CToN( cgiGet( edtContagemItem_PFB_Internalname), ",", ".");
                  n950ContagemItem_PFB = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A950ContagemItem_PFB", StringUtil.LTrim( StringUtil.Str( A950ContagemItem_PFB, 14, 5)));
               }
               n950ContagemItem_PFB = ((Convert.ToDecimal(0)==A950ContagemItem_PFB) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_PFL_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_PFL_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_PFL");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_PFL_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A951ContagemItem_PFL = 0;
                  n951ContagemItem_PFL = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A951ContagemItem_PFL", StringUtil.LTrim( StringUtil.Str( A951ContagemItem_PFL, 14, 5)));
               }
               else
               {
                  A951ContagemItem_PFL = context.localUtil.CToN( cgiGet( edtContagemItem_PFL_Internalname), ",", ".");
                  n951ContagemItem_PFL = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A951ContagemItem_PFL", StringUtil.LTrim( StringUtil.Str( A951ContagemItem_PFL, 14, 5)));
               }
               n951ContagemItem_PFL = ((Convert.ToDecimal(0)==A951ContagemItem_PFL) ? true : false);
               cmbContagemItem_TipoUnidade.CurrentValue = cgiGet( cmbContagemItem_TipoUnidade_Internalname);
               A952ContagemItem_TipoUnidade = cgiGet( cmbContagemItem_TipoUnidade_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A952ContagemItem_TipoUnidade", A952ContagemItem_TipoUnidade);
               A953ContagemItem_Evidencias = cgiGet( edtContagemItem_Evidencias_Internalname);
               n953ContagemItem_Evidencias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A953ContagemItem_Evidencias", A953ContagemItem_Evidencias);
               n953ContagemItem_Evidencias = (String.IsNullOrEmpty(StringUtil.RTrim( A953ContagemItem_Evidencias)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_QtdINM_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_QtdINM_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_QTDINM");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_QtdINM_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A954ContagemItem_QtdINM = 0;
                  n954ContagemItem_QtdINM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A954ContagemItem_QtdINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A954ContagemItem_QtdINM), 4, 0)));
               }
               else
               {
                  A954ContagemItem_QtdINM = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_QtdINM_Internalname), ",", "."));
                  n954ContagemItem_QtdINM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A954ContagemItem_QtdINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A954ContagemItem_QtdINM), 4, 0)));
               }
               n954ContagemItem_QtdINM = ((0==A954ContagemItem_QtdINM) ? true : false);
               A955ContagemItem_CP = cgiGet( edtContagemItem_CP_Internalname);
               n955ContagemItem_CP = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A955ContagemItem_CP", A955ContagemItem_CP);
               n955ContagemItem_CP = (String.IsNullOrEmpty(StringUtil.RTrim( A955ContagemItem_CP)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_RA_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_RA_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_RA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_RA_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A956ContagemItem_RA = 0;
                  n956ContagemItem_RA = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A956ContagemItem_RA", StringUtil.LTrim( StringUtil.Str( (decimal)(A956ContagemItem_RA), 3, 0)));
               }
               else
               {
                  A956ContagemItem_RA = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_RA_Internalname), ",", "."));
                  n956ContagemItem_RA = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A956ContagemItem_RA", StringUtil.LTrim( StringUtil.Str( (decimal)(A956ContagemItem_RA), 3, 0)));
               }
               n956ContagemItem_RA = ((0==A956ContagemItem_RA) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_DER_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_DER_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_DER");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_DER_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A957ContagemItem_DER = 0;
                  n957ContagemItem_DER = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A957ContagemItem_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A957ContagemItem_DER), 3, 0)));
               }
               else
               {
                  A957ContagemItem_DER = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_DER_Internalname), ",", "."));
                  n957ContagemItem_DER = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A957ContagemItem_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A957ContagemItem_DER), 3, 0)));
               }
               n957ContagemItem_DER = ((0==A957ContagemItem_DER) ? true : false);
               A958ContagemItem_Funcao = cgiGet( edtContagemItem_Funcao_Internalname);
               n958ContagemItem_Funcao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A958ContagemItem_Funcao", A958ContagemItem_Funcao);
               n958ContagemItem_Funcao = (String.IsNullOrEmpty(StringUtil.RTrim( A958ContagemItem_Funcao)) ? true : false);
               A224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_Lancamento_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
               A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
               cmbContagem_Tecnica.CurrentValue = cgiGet( cmbContagem_Tecnica_Internalname);
               A195Contagem_Tecnica = cgiGet( cmbContagem_Tecnica_Internalname);
               n195Contagem_Tecnica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
               cmbContagem_Tipo.CurrentValue = cgiGet( cmbContagem_Tipo_Internalname);
               A196Contagem_Tipo = cgiGet( cmbContagem_Tipo_Internalname);
               n196Contagem_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A196Contagem_Tipo", A196Contagem_Tipo);
               A225ContagemItem_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_Sequencial_Internalname), ",", "."));
               n225ContagemItem_Sequencial = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A225ContagemItem_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A225ContagemItem_Sequencial), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMITEM_SEQUENCIAL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")));
               cmbContagemItem_ValidacaoStatusFinal.CurrentValue = cgiGet( cmbContagemItem_ValidacaoStatusFinal_Internalname);
               A260ContagemItem_ValidacaoStatusFinal = (short)(NumberUtil.Val( cgiGet( cmbContagemItem_ValidacaoStatusFinal_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAOAPF_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A165FuncaoAPF_Codigo = 0;
                  n165FuncaoAPF_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               }
               else
               {
                  A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
                  n165FuncaoAPF_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               }
               n165FuncaoAPF_Codigo = ((0==A165FuncaoAPF_Codigo) ? true : false);
               A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
               cmbFuncaoAPF_Tipo.CurrentValue = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
               A184FuncaoAPF_Tipo = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
               cmbFuncaoAPF_Complexidade.CurrentValue = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
               A185FuncaoAPF_Complexidade = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_Requisito_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_Requisito_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_REQUISITO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_Requisito_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A229ContagemItem_Requisito = 0;
                  n229ContagemItem_Requisito = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A229ContagemItem_Requisito", StringUtil.LTrim( StringUtil.Str( (decimal)(A229ContagemItem_Requisito), 3, 0)));
               }
               else
               {
                  A229ContagemItem_Requisito = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_Requisito_Internalname), ",", "."));
                  n229ContagemItem_Requisito = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A229ContagemItem_Requisito", StringUtil.LTrim( StringUtil.Str( (decimal)(A229ContagemItem_Requisito), 3, 0)));
               }
               n229ContagemItem_Requisito = ((0==A229ContagemItem_Requisito) ? true : false);
               A209Contagem_FabricaSoftwarePessoaNom = StringUtil.Upper( cgiGet( edtContagem_FabricaSoftwarePessoaNom_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A209Contagem_FabricaSoftwarePessoaNom", A209Contagem_FabricaSoftwarePessoaNom);
               A215Contagem_UsuarioContadorPessoaNom = StringUtil.Upper( cgiGet( edtContagem_UsuarioContadorPessoaNom_Internalname));
               n215Contagem_UsuarioContadorPessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
               cmbContagemItem_FSValidacao.CurrentValue = cgiGet( cmbContagemItem_FSValidacao_Internalname);
               A267ContagemItem_FSValidacao = (short)(NumberUtil.Val( cgiGet( cmbContagemItem_FSValidacao_Internalname), "."));
               n267ContagemItem_FSValidacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A267ContagemItem_FSValidacao", StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0));
               n267ContagemItem_FSValidacao = ((0==A267ContagemItem_FSValidacao) ? true : false);
               cmbContagemItem_FSTipoUnidade.CurrentValue = cgiGet( cmbContagemItem_FSTipoUnidade_Internalname);
               A228ContagemItem_FSTipoUnidade = cgiGet( cmbContagemItem_FSTipoUnidade_Internalname);
               n228ContagemItem_FSTipoUnidade = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A228ContagemItem_FSTipoUnidade", A228ContagemItem_FSTipoUnidade);
               n228ContagemItem_FSTipoUnidade = (String.IsNullOrEmpty(StringUtil.RTrim( A228ContagemItem_FSTipoUnidade)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FSReferenciaTecnicaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FSReferenciaTecnicaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_FSREFERENCIATECNICACOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_FSReferenciaTecnicaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A226ContagemItem_FSReferenciaTecnicaCod = 0;
                  n226ContagemItem_FSReferenciaTecnicaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A226ContagemItem_FSReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0)));
               }
               else
               {
                  A226ContagemItem_FSReferenciaTecnicaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_FSReferenciaTecnicaCod_Internalname), ",", "."));
                  n226ContagemItem_FSReferenciaTecnicaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A226ContagemItem_FSReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0)));
               }
               n226ContagemItem_FSReferenciaTecnicaCod = ((0==A226ContagemItem_FSReferenciaTecnicaCod) ? true : false);
               A269ContagemItem_FSReferenciaTecnicaNom = StringUtil.Upper( cgiGet( edtContagemItem_FSReferenciaTecnicaNom_Internalname));
               n269ContagemItem_FSReferenciaTecnicaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A269ContagemItem_FSReferenciaTecnicaNom", A269ContagemItem_FSReferenciaTecnicaNom);
               A227ContagemItem_FSReferenciaTecnicaVal = context.localUtil.CToN( cgiGet( edtContagemItem_FSReferenciaTecnicaVal_Internalname), ",", ".");
               n227ContagemItem_FSReferenciaTecnicaVal = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A227ContagemItem_FSReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5)));
               A232ContagemItem_FSDER = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_FSDER_Internalname), ",", "."));
               n232ContagemItem_FSDER = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A232ContagemItem_FSDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A232ContagemItem_FSDER), 3, 0)));
               A233ContagemItem_FSAR = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_FSAR_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A233ContagemItem_FSAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A233ContagemItem_FSAR), 3, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FSQTDInm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FSQTDInm_Internalname), ",", ".") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_FSQTDINM");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_FSQTDInm_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A234ContagemItem_FSQTDInm = 0;
                  n234ContagemItem_FSQTDInm = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A234ContagemItem_FSQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A234ContagemItem_FSQTDInm), 9, 0)));
               }
               else
               {
                  A234ContagemItem_FSQTDInm = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_FSQTDInm_Internalname), ",", "."));
                  n234ContagemItem_FSQTDInm = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A234ContagemItem_FSQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A234ContagemItem_FSQTDInm), 9, 0)));
               }
               n234ContagemItem_FSQTDInm = ((0==A234ContagemItem_FSQTDInm) ? true : false);
               A235ContagemItem_FSCP = cgiGet( edtContagemItem_FSCP_Internalname);
               n235ContagemItem_FSCP = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A235ContagemItem_FSCP", A235ContagemItem_FSCP);
               n235ContagemItem_FSCP = (String.IsNullOrEmpty(StringUtil.RTrim( A235ContagemItem_FSCP)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FSPontoFuncaoBruto_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FSPontoFuncaoBruto_Internalname), ",", ".") > 9999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_FSPONTOFUNCAOBRUTO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_FSPontoFuncaoBruto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A236ContagemItem_FSPontoFuncaoBruto = 0;
                  n236ContagemItem_FSPontoFuncaoBruto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A236ContagemItem_FSPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A236ContagemItem_FSPontoFuncaoBruto, 7, 2)));
               }
               else
               {
                  A236ContagemItem_FSPontoFuncaoBruto = context.localUtil.CToN( cgiGet( edtContagemItem_FSPontoFuncaoBruto_Internalname), ",", ".");
                  n236ContagemItem_FSPontoFuncaoBruto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A236ContagemItem_FSPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A236ContagemItem_FSPontoFuncaoBruto, 7, 2)));
               }
               n236ContagemItem_FSPontoFuncaoBruto = ((Convert.ToDecimal(0)==A236ContagemItem_FSPontoFuncaoBruto) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FSPontoFuncaoLiquido_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FSPontoFuncaoLiquido_Internalname), ",", ".") > 9999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_FSPONTOFUNCAOLIQUIDO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_FSPontoFuncaoLiquido_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A237ContagemItem_FSPontoFuncaoLiquido = 0;
                  n237ContagemItem_FSPontoFuncaoLiquido = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A237ContagemItem_FSPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A237ContagemItem_FSPontoFuncaoLiquido, 7, 2)));
               }
               else
               {
                  A237ContagemItem_FSPontoFuncaoLiquido = context.localUtil.CToN( cgiGet( edtContagemItem_FSPontoFuncaoLiquido_Internalname), ",", ".");
                  n237ContagemItem_FSPontoFuncaoLiquido = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A237ContagemItem_FSPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A237ContagemItem_FSPontoFuncaoLiquido, 7, 2)));
               }
               n237ContagemItem_FSPontoFuncaoLiquido = ((Convert.ToDecimal(0)==A237ContagemItem_FSPontoFuncaoLiquido) ? true : false);
               A238ContagemItem_FSEvidencias = cgiGet( edtContagemItem_FSEvidencias_Internalname);
               n238ContagemItem_FSEvidencias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A238ContagemItem_FSEvidencias", A238ContagemItem_FSEvidencias);
               n238ContagemItem_FSEvidencias = (String.IsNullOrEmpty(StringUtil.RTrim( A238ContagemItem_FSEvidencias)) ? true : false);
               A218Contagem_UsuarioAuditorPessoaNom = StringUtil.Upper( cgiGet( edtContagem_UsuarioAuditorPessoaNom_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A218Contagem_UsuarioAuditorPessoaNom", A218Contagem_UsuarioAuditorPessoaNom);
               cmbContagemItem_FMValidacao.CurrentValue = cgiGet( cmbContagemItem_FMValidacao_Internalname);
               A268ContagemItem_FMValidacao = (short)(NumberUtil.Val( cgiGet( cmbContagemItem_FMValidacao_Internalname), "."));
               n268ContagemItem_FMValidacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A268ContagemItem_FMValidacao", StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0));
               n268ContagemItem_FMValidacao = ((0==A268ContagemItem_FMValidacao) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FMReferenciaTecnicaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FMReferenciaTecnicaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_FMREFERENCIATECNICACOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_FMReferenciaTecnicaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A255ContagemItem_FMReferenciaTecnicaCod = 0;
                  n255ContagemItem_FMReferenciaTecnicaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A255ContagemItem_FMReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), 6, 0)));
               }
               else
               {
                  A255ContagemItem_FMReferenciaTecnicaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_FMReferenciaTecnicaCod_Internalname), ",", "."));
                  n255ContagemItem_FMReferenciaTecnicaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A255ContagemItem_FMReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), 6, 0)));
               }
               n255ContagemItem_FMReferenciaTecnicaCod = ((0==A255ContagemItem_FMReferenciaTecnicaCod) ? true : false);
               A257ContagemItem_FMReferenciaTecnicaNom = StringUtil.Upper( cgiGet( edtContagemItem_FMReferenciaTecnicaNom_Internalname));
               n257ContagemItem_FMReferenciaTecnicaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A257ContagemItem_FMReferenciaTecnicaNom", A257ContagemItem_FMReferenciaTecnicaNom);
               A256ContagemItem_FMReferenciaTecnicaVal = context.localUtil.CToN( cgiGet( edtContagemItem_FMReferenciaTecnicaVal_Internalname), ",", ".");
               n256ContagemItem_FMReferenciaTecnicaVal = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A256ContagemItem_FMReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5)));
               A251ContagemItem_FMDER = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_FMDER_Internalname), ",", "."));
               n251ContagemItem_FMDER = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
               A252ContagemItem_FMAR = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_FMAR_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A252ContagemItem_FMAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A252ContagemItem_FMAR), 3, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FMQTDInm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FMQTDInm_Internalname), ",", ".") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_FMQTDINM");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_FMQTDInm_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A239ContagemItem_FMQTDInm = 0;
                  n239ContagemItem_FMQTDInm = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A239ContagemItem_FMQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A239ContagemItem_FMQTDInm), 9, 0)));
               }
               else
               {
                  A239ContagemItem_FMQTDInm = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_FMQTDInm_Internalname), ",", "."));
                  n239ContagemItem_FMQTDInm = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A239ContagemItem_FMQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A239ContagemItem_FMQTDInm), 9, 0)));
               }
               n239ContagemItem_FMQTDInm = ((0==A239ContagemItem_FMQTDInm) ? true : false);
               A240ContagemItem_FMCP = cgiGet( edtContagemItem_FMCP_Internalname);
               n240ContagemItem_FMCP = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A240ContagemItem_FMCP", A240ContagemItem_FMCP);
               n240ContagemItem_FMCP = (String.IsNullOrEmpty(StringUtil.RTrim( A240ContagemItem_FMCP)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FMPontoFuncaoBruto_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FMPontoFuncaoBruto_Internalname), ",", ".") > 9999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_FMPONTOFUNCAOBRUTO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_FMPontoFuncaoBruto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A241ContagemItem_FMPontoFuncaoBruto = 0;
                  n241ContagemItem_FMPontoFuncaoBruto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A241ContagemItem_FMPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A241ContagemItem_FMPontoFuncaoBruto, 7, 2)));
               }
               else
               {
                  A241ContagemItem_FMPontoFuncaoBruto = context.localUtil.CToN( cgiGet( edtContagemItem_FMPontoFuncaoBruto_Internalname), ",", ".");
                  n241ContagemItem_FMPontoFuncaoBruto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A241ContagemItem_FMPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A241ContagemItem_FMPontoFuncaoBruto, 7, 2)));
               }
               n241ContagemItem_FMPontoFuncaoBruto = ((Convert.ToDecimal(0)==A241ContagemItem_FMPontoFuncaoBruto) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FMPontoFuncaoLiquido_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FMPontoFuncaoLiquido_Internalname), ",", ".") > 9999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_FMPONTOFUNCAOLIQUIDO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_FMPontoFuncaoLiquido_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A242ContagemItem_FMPontoFuncaoLiquido = 0;
                  n242ContagemItem_FMPontoFuncaoLiquido = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A242ContagemItem_FMPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A242ContagemItem_FMPontoFuncaoLiquido, 7, 2)));
               }
               else
               {
                  A242ContagemItem_FMPontoFuncaoLiquido = context.localUtil.CToN( cgiGet( edtContagemItem_FMPontoFuncaoLiquido_Internalname), ",", ".");
                  n242ContagemItem_FMPontoFuncaoLiquido = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A242ContagemItem_FMPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A242ContagemItem_FMPontoFuncaoLiquido, 7, 2)));
               }
               n242ContagemItem_FMPontoFuncaoLiquido = ((Convert.ToDecimal(0)==A242ContagemItem_FMPontoFuncaoLiquido) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_UsuarioAuditorCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_UsuarioAuditorCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_USUARIOAUDITORCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_UsuarioAuditorCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A216Contagem_UsuarioAuditorCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A216Contagem_UsuarioAuditorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0)));
               }
               else
               {
                  A216Contagem_UsuarioAuditorCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioAuditorCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A216Contagem_UsuarioAuditorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_UsuarioAuditorPessoaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_UsuarioAuditorPessoaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_USUARIOAUDITORPESSOACOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_UsuarioAuditorPessoaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A217Contagem_UsuarioAuditorPessoaCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A217Contagem_UsuarioAuditorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0)));
               }
               else
               {
                  A217Contagem_UsuarioAuditorPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioAuditorPessoaCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A217Contagem_UsuarioAuditorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_FabricaSoftwareCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_FabricaSoftwareCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_FABRICASOFTWARECOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_FabricaSoftwareCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A207Contagem_FabricaSoftwareCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A207Contagem_FabricaSoftwareCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0)));
               }
               else
               {
                  A207Contagem_FabricaSoftwareCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_FabricaSoftwareCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A207Contagem_FabricaSoftwareCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_FabricaSoftwarePessoaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_FabricaSoftwarePessoaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_FABRICASOFTWAREPESSOACOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_FabricaSoftwarePessoaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A208Contagem_FabricaSoftwarePessoaCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A208Contagem_FabricaSoftwarePessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0)));
               }
               else
               {
                  A208Contagem_FabricaSoftwarePessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_FabricaSoftwarePessoaCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A208Contagem_FabricaSoftwarePessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0)));
               }
               A213Contagem_UsuarioContadorCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorCod_Internalname), ",", "."));
               n213Contagem_UsuarioContadorCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
               A214Contagem_UsuarioContadorPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorPessoaCod_Internalname), ",", "."));
               n214Contagem_UsuarioContadorPessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
               /* Read saved values. */
               Z224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( "Z224ContagemItem_Lancamento"), ",", "."));
               Z268ContagemItem_FMValidacao = (short)(context.localUtil.CToN( cgiGet( "Z268ContagemItem_FMValidacao"), ",", "."));
               n268ContagemItem_FMValidacao = ((0==A268ContagemItem_FMValidacao) ? true : false);
               Z207Contagem_FabricaSoftwareCod = (int)(context.localUtil.CToN( cgiGet( "Z207Contagem_FabricaSoftwareCod"), ",", "."));
               Z208Contagem_FabricaSoftwarePessoaCod = (int)(context.localUtil.CToN( cgiGet( "Z208Contagem_FabricaSoftwarePessoaCod"), ",", "."));
               Z209Contagem_FabricaSoftwarePessoaNom = cgiGet( "Z209Contagem_FabricaSoftwarePessoaNom");
               Z216Contagem_UsuarioAuditorCod = (int)(context.localUtil.CToN( cgiGet( "Z216Contagem_UsuarioAuditorCod"), ",", "."));
               Z217Contagem_UsuarioAuditorPessoaCod = (int)(context.localUtil.CToN( cgiGet( "Z217Contagem_UsuarioAuditorPessoaCod"), ",", "."));
               Z218Contagem_UsuarioAuditorPessoaNom = cgiGet( "Z218Contagem_UsuarioAuditorPessoaNom");
               Z225ContagemItem_Sequencial = (short)(context.localUtil.CToN( cgiGet( "Z225ContagemItem_Sequencial"), ",", "."));
               n225ContagemItem_Sequencial = ((0==A225ContagemItem_Sequencial) ? true : false);
               Z229ContagemItem_Requisito = (short)(context.localUtil.CToN( cgiGet( "Z229ContagemItem_Requisito"), ",", "."));
               n229ContagemItem_Requisito = ((0==A229ContagemItem_Requisito) ? true : false);
               Z950ContagemItem_PFB = context.localUtil.CToN( cgiGet( "Z950ContagemItem_PFB"), ",", ".");
               n950ContagemItem_PFB = ((Convert.ToDecimal(0)==A950ContagemItem_PFB) ? true : false);
               Z951ContagemItem_PFL = context.localUtil.CToN( cgiGet( "Z951ContagemItem_PFL"), ",", ".");
               n951ContagemItem_PFL = ((Convert.ToDecimal(0)==A951ContagemItem_PFL) ? true : false);
               Z952ContagemItem_TipoUnidade = cgiGet( "Z952ContagemItem_TipoUnidade");
               Z954ContagemItem_QtdINM = (short)(context.localUtil.CToN( cgiGet( "Z954ContagemItem_QtdINM"), ",", "."));
               n954ContagemItem_QtdINM = ((0==A954ContagemItem_QtdINM) ? true : false);
               Z955ContagemItem_CP = cgiGet( "Z955ContagemItem_CP");
               n955ContagemItem_CP = (String.IsNullOrEmpty(StringUtil.RTrim( A955ContagemItem_CP)) ? true : false);
               Z956ContagemItem_RA = (short)(context.localUtil.CToN( cgiGet( "Z956ContagemItem_RA"), ",", "."));
               n956ContagemItem_RA = ((0==A956ContagemItem_RA) ? true : false);
               Z957ContagemItem_DER = (short)(context.localUtil.CToN( cgiGet( "Z957ContagemItem_DER"), ",", "."));
               n957ContagemItem_DER = ((0==A957ContagemItem_DER) ? true : false);
               Z958ContagemItem_Funcao = cgiGet( "Z958ContagemItem_Funcao");
               n958ContagemItem_Funcao = (String.IsNullOrEmpty(StringUtil.RTrim( A958ContagemItem_Funcao)) ? true : false);
               Z228ContagemItem_FSTipoUnidade = cgiGet( "Z228ContagemItem_FSTipoUnidade");
               n228ContagemItem_FSTipoUnidade = (String.IsNullOrEmpty(StringUtil.RTrim( A228ContagemItem_FSTipoUnidade)) ? true : false);
               Z267ContagemItem_FSValidacao = (short)(context.localUtil.CToN( cgiGet( "Z267ContagemItem_FSValidacao"), ",", "."));
               n267ContagemItem_FSValidacao = ((0==A267ContagemItem_FSValidacao) ? true : false);
               Z234ContagemItem_FSQTDInm = (int)(context.localUtil.CToN( cgiGet( "Z234ContagemItem_FSQTDInm"), ",", "."));
               n234ContagemItem_FSQTDInm = ((0==A234ContagemItem_FSQTDInm) ? true : false);
               Z235ContagemItem_FSCP = cgiGet( "Z235ContagemItem_FSCP");
               n235ContagemItem_FSCP = (String.IsNullOrEmpty(StringUtil.RTrim( A235ContagemItem_FSCP)) ? true : false);
               Z236ContagemItem_FSPontoFuncaoBruto = context.localUtil.CToN( cgiGet( "Z236ContagemItem_FSPontoFuncaoBruto"), ",", ".");
               n236ContagemItem_FSPontoFuncaoBruto = ((Convert.ToDecimal(0)==A236ContagemItem_FSPontoFuncaoBruto) ? true : false);
               Z237ContagemItem_FSPontoFuncaoLiquido = context.localUtil.CToN( cgiGet( "Z237ContagemItem_FSPontoFuncaoLiquido"), ",", ".");
               n237ContagemItem_FSPontoFuncaoLiquido = ((Convert.ToDecimal(0)==A237ContagemItem_FSPontoFuncaoLiquido) ? true : false);
               Z239ContagemItem_FMQTDInm = (int)(context.localUtil.CToN( cgiGet( "Z239ContagemItem_FMQTDInm"), ",", "."));
               n239ContagemItem_FMQTDInm = ((0==A239ContagemItem_FMQTDInm) ? true : false);
               Z240ContagemItem_FMCP = cgiGet( "Z240ContagemItem_FMCP");
               n240ContagemItem_FMCP = (String.IsNullOrEmpty(StringUtil.RTrim( A240ContagemItem_FMCP)) ? true : false);
               Z241ContagemItem_FMPontoFuncaoBruto = context.localUtil.CToN( cgiGet( "Z241ContagemItem_FMPontoFuncaoBruto"), ",", ".");
               n241ContagemItem_FMPontoFuncaoBruto = ((Convert.ToDecimal(0)==A241ContagemItem_FMPontoFuncaoBruto) ? true : false);
               Z242ContagemItem_FMPontoFuncaoLiquido = context.localUtil.CToN( cgiGet( "Z242ContagemItem_FMPontoFuncaoLiquido"), ",", ".");
               n242ContagemItem_FMPontoFuncaoLiquido = ((Convert.ToDecimal(0)==A242ContagemItem_FMPontoFuncaoLiquido) ? true : false);
               Z165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z165FuncaoAPF_Codigo"), ",", "."));
               n165FuncaoAPF_Codigo = ((0==A165FuncaoAPF_Codigo) ? true : false);
               Z226ContagemItem_FSReferenciaTecnicaCod = (int)(context.localUtil.CToN( cgiGet( "Z226ContagemItem_FSReferenciaTecnicaCod"), ",", "."));
               n226ContagemItem_FSReferenciaTecnicaCod = ((0==A226ContagemItem_FSReferenciaTecnicaCod) ? true : false);
               Z255ContagemItem_FMReferenciaTecnicaCod = (int)(context.localUtil.CToN( cgiGet( "Z255ContagemItem_FMReferenciaTecnicaCod"), ",", "."));
               n255ContagemItem_FMReferenciaTecnicaCod = ((0==A255ContagemItem_FMReferenciaTecnicaCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( "N165FuncaoAPF_Codigo"), ",", "."));
               n165FuncaoAPF_Codigo = ((0==A165FuncaoAPF_Codigo) ? true : false);
               N226ContagemItem_FSReferenciaTecnicaCod = (int)(context.localUtil.CToN( cgiGet( "N226ContagemItem_FSReferenciaTecnicaCod"), ",", "."));
               n226ContagemItem_FSReferenciaTecnicaCod = ((0==A226ContagemItem_FSReferenciaTecnicaCod) ? true : false);
               N255ContagemItem_FMReferenciaTecnicaCod = (int)(context.localUtil.CToN( cgiGet( "N255ContagemItem_FMReferenciaTecnicaCod"), ",", "."));
               n255ContagemItem_FMReferenciaTecnicaCod = ((0==A255ContagemItem_FMReferenciaTecnicaCod) ? true : false);
               AV7ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( "vCONTAGEMITEM_LANCAMENTO"), ",", "."));
               AV11Insert_Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEM_CODIGO"), ",", "."));
               AV12Insert_FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_FUNCAOAPF_CODIGO"), ",", "."));
               AV13Insert_ContagemItem_FSReferenciaTecnicaCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEMITEM_FSREFERENCIATECNICACOD"), ",", "."));
               AV14Insert_ContagemItem_FMReferenciaTecnicaCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEMITEM_FMREFERENCIATECNICACOD"), ",", "."));
               AV16Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContagemItem";
               A224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_Lancamento_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9");
               A225ContagemItem_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_Sequencial_Internalname), ",", "."));
               n225ContagemItem_Sequencial = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A225ContagemItem_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A225ContagemItem_Sequencial), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMITEM_SEQUENCIAL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A224ContagemItem_Lancamento != Z224ContagemItem_Lancamento ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contagemitem:[SecurityCheckFailed value for]"+"ContagemItem_Lancamento:"+context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9"));
                  GXUtil.WriteLog("contagemitem:[SecurityCheckFailed value for]"+"ContagemItem_Sequencial:"+context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9"));
                  GXUtil.WriteLog("contagemitem:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A224ContagemItem_Lancamento = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode44 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode44;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound44 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_170( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTAGEMITEM_LANCAMENTO");
                        AnyError = 1;
                        GX_FocusControl = edtContagemItem_Lancamento_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13172 */
                           E13172 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14172 */
                           E14172 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E14172 */
            E14172 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1744( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1744( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_170( )
      {
         BeforeValidate1744( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1744( ) ;
            }
            else
            {
               CheckExtendedTable1744( ) ;
               CloseExtendedTableCursors1744( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption170( )
      {
      }

      protected void E13172( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV16Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV17GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            while ( AV17GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV15TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV17GXV1));
               if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "Contagem_Codigo") == 0 )
               {
                  AV11Insert_Contagem_Codigo = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contagem_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "FuncaoAPF_Codigo") == 0 )
               {
                  AV12Insert_FuncaoAPF_Codigo = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_FuncaoAPF_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "ContagemItem_FSReferenciaTecnicaCod") == 0 )
               {
                  AV13Insert_ContagemItem_FSReferenciaTecnicaCod = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_ContagemItem_FSReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_ContagemItem_FSReferenciaTecnicaCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "ContagemItem_FMReferenciaTecnicaCod") == 0 )
               {
                  AV14Insert_ContagemItem_FMReferenciaTecnicaCod = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Insert_ContagemItem_FMReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Insert_ContagemItem_FMReferenciaTecnicaCod), 6, 0)));
               }
               AV17GXV1 = (int)(AV17GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            }
         }
         edtContagem_UsuarioAuditorCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioAuditorCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioAuditorCod_Visible), 5, 0)));
         edtContagem_UsuarioAuditorPessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioAuditorPessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioAuditorPessoaCod_Visible), 5, 0)));
         edtContagem_FabricaSoftwareCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_FabricaSoftwareCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_FabricaSoftwareCod_Visible), 5, 0)));
         edtContagem_FabricaSoftwarePessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_FabricaSoftwarePessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_FabricaSoftwarePessoaCod_Visible), 5, 0)));
         edtContagem_UsuarioContadorCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioContadorCod_Visible), 5, 0)));
         edtContagem_UsuarioContadorPessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorPessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioContadorPessoaCod_Visible), 5, 0)));
      }

      protected void E14172( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            context.wjLoc = formatLink("contagemitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A224ContagemItem_Lancamento) + "," + UrlEncode("" +A192Contagem_Codigo);
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A192Contagem_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1744( short GX_JID )
      {
         if ( ( GX_JID == 41 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z268ContagemItem_FMValidacao = T00173_A268ContagemItem_FMValidacao[0];
               Z207Contagem_FabricaSoftwareCod = T00173_A207Contagem_FabricaSoftwareCod[0];
               Z208Contagem_FabricaSoftwarePessoaCod = T00173_A208Contagem_FabricaSoftwarePessoaCod[0];
               Z209Contagem_FabricaSoftwarePessoaNom = T00173_A209Contagem_FabricaSoftwarePessoaNom[0];
               Z216Contagem_UsuarioAuditorCod = T00173_A216Contagem_UsuarioAuditorCod[0];
               Z217Contagem_UsuarioAuditorPessoaCod = T00173_A217Contagem_UsuarioAuditorPessoaCod[0];
               Z218Contagem_UsuarioAuditorPessoaNom = T00173_A218Contagem_UsuarioAuditorPessoaNom[0];
               Z225ContagemItem_Sequencial = T00173_A225ContagemItem_Sequencial[0];
               Z229ContagemItem_Requisito = T00173_A229ContagemItem_Requisito[0];
               Z950ContagemItem_PFB = T00173_A950ContagemItem_PFB[0];
               Z951ContagemItem_PFL = T00173_A951ContagemItem_PFL[0];
               Z952ContagemItem_TipoUnidade = T00173_A952ContagemItem_TipoUnidade[0];
               Z954ContagemItem_QtdINM = T00173_A954ContagemItem_QtdINM[0];
               Z955ContagemItem_CP = T00173_A955ContagemItem_CP[0];
               Z956ContagemItem_RA = T00173_A956ContagemItem_RA[0];
               Z957ContagemItem_DER = T00173_A957ContagemItem_DER[0];
               Z958ContagemItem_Funcao = T00173_A958ContagemItem_Funcao[0];
               Z228ContagemItem_FSTipoUnidade = T00173_A228ContagemItem_FSTipoUnidade[0];
               Z267ContagemItem_FSValidacao = T00173_A267ContagemItem_FSValidacao[0];
               Z234ContagemItem_FSQTDInm = T00173_A234ContagemItem_FSQTDInm[0];
               Z235ContagemItem_FSCP = T00173_A235ContagemItem_FSCP[0];
               Z236ContagemItem_FSPontoFuncaoBruto = T00173_A236ContagemItem_FSPontoFuncaoBruto[0];
               Z237ContagemItem_FSPontoFuncaoLiquido = T00173_A237ContagemItem_FSPontoFuncaoLiquido[0];
               Z239ContagemItem_FMQTDInm = T00173_A239ContagemItem_FMQTDInm[0];
               Z240ContagemItem_FMCP = T00173_A240ContagemItem_FMCP[0];
               Z241ContagemItem_FMPontoFuncaoBruto = T00173_A241ContagemItem_FMPontoFuncaoBruto[0];
               Z242ContagemItem_FMPontoFuncaoLiquido = T00173_A242ContagemItem_FMPontoFuncaoLiquido[0];
               Z165FuncaoAPF_Codigo = T00173_A165FuncaoAPF_Codigo[0];
               Z226ContagemItem_FSReferenciaTecnicaCod = T00173_A226ContagemItem_FSReferenciaTecnicaCod[0];
               Z255ContagemItem_FMReferenciaTecnicaCod = T00173_A255ContagemItem_FMReferenciaTecnicaCod[0];
            }
            else
            {
               Z268ContagemItem_FMValidacao = A268ContagemItem_FMValidacao;
               Z207Contagem_FabricaSoftwareCod = A207Contagem_FabricaSoftwareCod;
               Z208Contagem_FabricaSoftwarePessoaCod = A208Contagem_FabricaSoftwarePessoaCod;
               Z209Contagem_FabricaSoftwarePessoaNom = A209Contagem_FabricaSoftwarePessoaNom;
               Z216Contagem_UsuarioAuditorCod = A216Contagem_UsuarioAuditorCod;
               Z217Contagem_UsuarioAuditorPessoaCod = A217Contagem_UsuarioAuditorPessoaCod;
               Z218Contagem_UsuarioAuditorPessoaNom = A218Contagem_UsuarioAuditorPessoaNom;
               Z225ContagemItem_Sequencial = A225ContagemItem_Sequencial;
               Z229ContagemItem_Requisito = A229ContagemItem_Requisito;
               Z950ContagemItem_PFB = A950ContagemItem_PFB;
               Z951ContagemItem_PFL = A951ContagemItem_PFL;
               Z952ContagemItem_TipoUnidade = A952ContagemItem_TipoUnidade;
               Z954ContagemItem_QtdINM = A954ContagemItem_QtdINM;
               Z955ContagemItem_CP = A955ContagemItem_CP;
               Z956ContagemItem_RA = A956ContagemItem_RA;
               Z957ContagemItem_DER = A957ContagemItem_DER;
               Z958ContagemItem_Funcao = A958ContagemItem_Funcao;
               Z228ContagemItem_FSTipoUnidade = A228ContagemItem_FSTipoUnidade;
               Z267ContagemItem_FSValidacao = A267ContagemItem_FSValidacao;
               Z234ContagemItem_FSQTDInm = A234ContagemItem_FSQTDInm;
               Z235ContagemItem_FSCP = A235ContagemItem_FSCP;
               Z236ContagemItem_FSPontoFuncaoBruto = A236ContagemItem_FSPontoFuncaoBruto;
               Z237ContagemItem_FSPontoFuncaoLiquido = A237ContagemItem_FSPontoFuncaoLiquido;
               Z239ContagemItem_FMQTDInm = A239ContagemItem_FMQTDInm;
               Z240ContagemItem_FMCP = A240ContagemItem_FMCP;
               Z241ContagemItem_FMPontoFuncaoBruto = A241ContagemItem_FMPontoFuncaoBruto;
               Z242ContagemItem_FMPontoFuncaoLiquido = A242ContagemItem_FMPontoFuncaoLiquido;
               Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
               Z226ContagemItem_FSReferenciaTecnicaCod = A226ContagemItem_FSReferenciaTecnicaCod;
               Z255ContagemItem_FMReferenciaTecnicaCod = A255ContagemItem_FMReferenciaTecnicaCod;
            }
         }
         if ( GX_JID == -41 )
         {
            Z192Contagem_Codigo = A192Contagem_Codigo;
            Z224ContagemItem_Lancamento = A224ContagemItem_Lancamento;
            Z268ContagemItem_FMValidacao = A268ContagemItem_FMValidacao;
            Z207Contagem_FabricaSoftwareCod = A207Contagem_FabricaSoftwareCod;
            Z208Contagem_FabricaSoftwarePessoaCod = A208Contagem_FabricaSoftwarePessoaCod;
            Z209Contagem_FabricaSoftwarePessoaNom = A209Contagem_FabricaSoftwarePessoaNom;
            Z216Contagem_UsuarioAuditorCod = A216Contagem_UsuarioAuditorCod;
            Z217Contagem_UsuarioAuditorPessoaCod = A217Contagem_UsuarioAuditorPessoaCod;
            Z218Contagem_UsuarioAuditorPessoaNom = A218Contagem_UsuarioAuditorPessoaNom;
            Z225ContagemItem_Sequencial = A225ContagemItem_Sequencial;
            Z229ContagemItem_Requisito = A229ContagemItem_Requisito;
            Z950ContagemItem_PFB = A950ContagemItem_PFB;
            Z951ContagemItem_PFL = A951ContagemItem_PFL;
            Z952ContagemItem_TipoUnidade = A952ContagemItem_TipoUnidade;
            Z953ContagemItem_Evidencias = A953ContagemItem_Evidencias;
            Z954ContagemItem_QtdINM = A954ContagemItem_QtdINM;
            Z955ContagemItem_CP = A955ContagemItem_CP;
            Z956ContagemItem_RA = A956ContagemItem_RA;
            Z957ContagemItem_DER = A957ContagemItem_DER;
            Z958ContagemItem_Funcao = A958ContagemItem_Funcao;
            Z228ContagemItem_FSTipoUnidade = A228ContagemItem_FSTipoUnidade;
            Z267ContagemItem_FSValidacao = A267ContagemItem_FSValidacao;
            Z234ContagemItem_FSQTDInm = A234ContagemItem_FSQTDInm;
            Z235ContagemItem_FSCP = A235ContagemItem_FSCP;
            Z236ContagemItem_FSPontoFuncaoBruto = A236ContagemItem_FSPontoFuncaoBruto;
            Z237ContagemItem_FSPontoFuncaoLiquido = A237ContagemItem_FSPontoFuncaoLiquido;
            Z238ContagemItem_FSEvidencias = A238ContagemItem_FSEvidencias;
            Z239ContagemItem_FMQTDInm = A239ContagemItem_FMQTDInm;
            Z240ContagemItem_FMCP = A240ContagemItem_FMCP;
            Z241ContagemItem_FMPontoFuncaoBruto = A241ContagemItem_FMPontoFuncaoBruto;
            Z242ContagemItem_FMPontoFuncaoLiquido = A242ContagemItem_FMPontoFuncaoLiquido;
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
            Z226ContagemItem_FSReferenciaTecnicaCod = A226ContagemItem_FSReferenciaTecnicaCod;
            Z255ContagemItem_FMReferenciaTecnicaCod = A255ContagemItem_FMReferenciaTecnicaCod;
            Z197Contagem_DataCriacao = A197Contagem_DataCriacao;
            Z195Contagem_Tecnica = A195Contagem_Tecnica;
            Z196Contagem_Tipo = A196Contagem_Tipo;
            Z193Contagem_AreaTrabalhoCod = A193Contagem_AreaTrabalhoCod;
            Z213Contagem_UsuarioContadorCod = A213Contagem_UsuarioContadorCod;
            Z194Contagem_AreaTrabalhoDes = A194Contagem_AreaTrabalhoDes;
            Z214Contagem_UsuarioContadorPessoaCod = A214Contagem_UsuarioContadorPessoaCod;
            Z215Contagem_UsuarioContadorPessoaNom = A215Contagem_UsuarioContadorPessoaNom;
            Z232ContagemItem_FSDER = A232ContagemItem_FSDER;
            Z251ContagemItem_FMDER = A251ContagemItem_FMDER;
            Z166FuncaoAPF_Nome = A166FuncaoAPF_Nome;
            Z184FuncaoAPF_Tipo = A184FuncaoAPF_Tipo;
            Z269ContagemItem_FSReferenciaTecnicaNom = A269ContagemItem_FSReferenciaTecnicaNom;
            Z227ContagemItem_FSReferenciaTecnicaVal = A227ContagemItem_FSReferenciaTecnicaVal;
            Z257ContagemItem_FMReferenciaTecnicaNom = A257ContagemItem_FMReferenciaTecnicaNom;
            Z256ContagemItem_FMReferenciaTecnicaVal = A256ContagemItem_FMReferenciaTecnicaVal;
         }
      }

      protected void standaloneNotModal( )
      {
         /* Using cursor T00175 */
         pr_default.execute(2);
         if ( (pr_default.getStatus(2) != 101) )
         {
            A232ContagemItem_FSDER = T00175_A232ContagemItem_FSDER[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A232ContagemItem_FSDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A232ContagemItem_FSDER), 3, 0)));
            n232ContagemItem_FSDER = T00175_n232ContagemItem_FSDER[0];
         }
         else
         {
            A232ContagemItem_FSDER = 0;
            n232ContagemItem_FSDER = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A232ContagemItem_FSDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A232ContagemItem_FSDER), 3, 0)));
         }
         pr_default.close(2);
         edtContagem_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Codigo_Enabled), 5, 0)));
         edtContagemItem_Sequencial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Sequencial_Enabled), 5, 0)));
         edtContagemItem_Lancamento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Lancamento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Lancamento_Enabled), 5, 0)));
         AV16Pgmname = "ContagemItem";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Pgmname", AV16Pgmname);
         imgprompt_165_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptfuncaoapf.aspx"+"',["+"{Ctrl:gx.dom.el('"+"FUNCAOAPF_CODIGO"+"'), id:'"+"FUNCAOAPF_CODIGO"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"FUNCAOAPF_NOME"+"'), id:'"+"FUNCAOAPF_NOME"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_226_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptreferenciatecnica.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CONTAGEMITEM_FSREFERENCIATECNICACOD"+"'), id:'"+"CONTAGEMITEM_FSREFERENCIATECNICACOD"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"CONTAGEMITEM_FSREFERENCIATECNICANOM"+"'), id:'"+"CONTAGEMITEM_FSREFERENCIATECNICANOM"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_255_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptreferenciatecnica.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CONTAGEMITEM_FMREFERENCIATECNICACOD"+"'), id:'"+"CONTAGEMITEM_FMREFERENCIATECNICACOD"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"CONTAGEMITEM_FMREFERENCIATECNICANOM"+"'), id:'"+"CONTAGEMITEM_FMREFERENCIATECNICANOM"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         edtContagem_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Codigo_Enabled), 5, 0)));
         edtContagemItem_Sequencial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Sequencial_Enabled), 5, 0)));
         edtContagemItem_Lancamento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Lancamento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Lancamento_Enabled), 5, 0)));
         if ( AV8WWPContext.gxTpr_Userehcontador )
         {
            edtContagemItem_FMPontoFuncaoBruto_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMPontoFuncaoBruto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMPontoFuncaoBruto_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehcontador )
         {
            edtContagemItem_FMPontoFuncaoLiquido_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMPontoFuncaoLiquido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMPontoFuncaoLiquido_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehauditorfm )
         {
            edtContagemItem_FSPontoFuncaoBruto_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSPontoFuncaoBruto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSPontoFuncaoBruto_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehauditorfm )
         {
            edtContagemItem_FSPontoFuncaoLiquido_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSPontoFuncaoLiquido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSPontoFuncaoLiquido_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehauditorfm )
         {
            edtContagemItem_FSEvidencias_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSEvidencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSEvidencias_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehauditorfm )
         {
            edtContagemItem_Requisito_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Requisito_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Requisito_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehauditorfm )
         {
            edtFuncaoAPF_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Enabled), 5, 0)));
         }
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContagemItem_Lancamento) )
         {
            A224ContagemItem_Lancamento = AV7ContagemItem_Lancamento;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         }
         /* Using cursor T00176 */
         pr_default.execute(3, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A197Contagem_DataCriacao = T00176_A197Contagem_DataCriacao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
         A195Contagem_Tecnica = T00176_A195Contagem_Tecnica[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
         n195Contagem_Tecnica = T00176_n195Contagem_Tecnica[0];
         A196Contagem_Tipo = T00176_A196Contagem_Tipo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A196Contagem_Tipo", A196Contagem_Tipo);
         n196Contagem_Tipo = T00176_n196Contagem_Tipo[0];
         A193Contagem_AreaTrabalhoCod = T00176_A193Contagem_AreaTrabalhoCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
         A213Contagem_UsuarioContadorCod = T00176_A213Contagem_UsuarioContadorCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
         n213Contagem_UsuarioContadorCod = T00176_n213Contagem_UsuarioContadorCod[0];
         pr_default.close(3);
         /* Using cursor T001710 */
         pr_default.execute(7, new Object[] {A193Contagem_AreaTrabalhoCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A194Contagem_AreaTrabalhoDes = T001710_A194Contagem_AreaTrabalhoDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
         n194Contagem_AreaTrabalhoDes = T001710_n194Contagem_AreaTrabalhoDes[0];
         pr_default.close(7);
         /* Using cursor T001711 */
         pr_default.execute(8, new Object[] {n213Contagem_UsuarioContadorCod, A213Contagem_UsuarioContadorCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A213Contagem_UsuarioContadorCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contador'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A214Contagem_UsuarioContadorPessoaCod = T001711_A214Contagem_UsuarioContadorPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
         n214Contagem_UsuarioContadorPessoaCod = T001711_n214Contagem_UsuarioContadorPessoaCod[0];
         pr_default.close(8);
         /* Using cursor T001712 */
         pr_default.execute(9, new Object[] {n214Contagem_UsuarioContadorPessoaCod, A214Contagem_UsuarioContadorPessoaCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A214Contagem_UsuarioContadorPessoaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A215Contagem_UsuarioContadorPessoaNom = T001712_A215Contagem_UsuarioContadorPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
         n215Contagem_UsuarioContadorPessoaNom = T001712_n215Contagem_UsuarioContadorPessoaNom[0];
         pr_default.close(9);
         if ( AV8WWPContext.gxTpr_Userehcontador )
         {
            cmbContagemItem_FMValidacao.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_FMValidacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemItem_FMValidacao.Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehcontador )
         {
            edtContagemItem_FMQTDInm_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMQTDInm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMQTDInm_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehcontador )
         {
            edtContagemItem_FMCP_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMCP_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMCP_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehcontador )
         {
            edtContagemItem_FMPontoFuncaoBruto_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMPontoFuncaoBruto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMPontoFuncaoBruto_Enabled), 5, 0)));
         }
         else
         {
            edtContagemItem_FMPontoFuncaoBruto_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMPontoFuncaoBruto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMPontoFuncaoBruto_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehcontador )
         {
            edtContagemItem_FMPontoFuncaoLiquido_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMPontoFuncaoLiquido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMPontoFuncaoLiquido_Enabled), 5, 0)));
         }
         else
         {
            edtContagemItem_FMPontoFuncaoLiquido_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMPontoFuncaoLiquido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMPontoFuncaoLiquido_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehcontador )
         {
            bttBtnbtnselecionaratributosfs_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnbtnselecionaratributosfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnbtnselecionaratributosfs_Enabled), 5, 0)));
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehauditorfm )
            {
               bttBtnbtnselecionaratributosfs_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnbtnselecionaratributosfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnbtnselecionaratributosfs_Enabled), 5, 0)));
            }
         }
         if ( AV8WWPContext.gxTpr_Userehauditorfm )
         {
            cmbContagemItem_FSValidacao.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_FSValidacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemItem_FSValidacao.Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehauditorfm )
         {
            edtContagemItem_FSQTDInm_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSQTDInm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSQTDInm_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehauditorfm )
         {
            edtContagemItem_FSCP_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSCP_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSCP_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehauditorfm )
         {
            edtContagemItem_FSPontoFuncaoBruto_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSPontoFuncaoBruto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSPontoFuncaoBruto_Enabled), 5, 0)));
         }
         else
         {
            edtContagemItem_FSPontoFuncaoBruto_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSPontoFuncaoBruto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSPontoFuncaoBruto_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehauditorfm )
         {
            edtContagemItem_FSPontoFuncaoLiquido_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSPontoFuncaoLiquido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSPontoFuncaoLiquido_Enabled), 5, 0)));
         }
         else
         {
            edtContagemItem_FSPontoFuncaoLiquido_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSPontoFuncaoLiquido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSPontoFuncaoLiquido_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehauditorfm )
         {
            edtContagemItem_FSEvidencias_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSEvidencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSEvidencias_Enabled), 5, 0)));
         }
         else
         {
            edtContagemItem_FSEvidencias_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSEvidencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSEvidencias_Enabled), 5, 0)));
         }
         if ( AV8WWPContext.gxTpr_Userehauditorfm )
         {
            edtContagemItem_Requisito_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Requisito_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Requisito_Enabled), 5, 0)));
         }
         else
         {
            edtContagemItem_Requisito_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Requisito_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Requisito_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_FuncaoAPF_Codigo) )
         {
            edtFuncaoAPF_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Enabled), 5, 0)));
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehauditorfm )
            {
               edtFuncaoAPF_Codigo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Enabled), 5, 0)));
            }
            else
            {
               edtFuncaoAPF_Codigo_Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Enabled), 5, 0)));
            }
         }
         if ( AV8WWPContext.gxTpr_Userehauditorfm )
         {
            edtContagemItem_FSReferenciaTecnicaCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSReferenciaTecnicaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSReferenciaTecnicaCod_Enabled), 5, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_ContagemItem_FSReferenciaTecnicaCod) )
            {
               edtContagemItem_FSReferenciaTecnicaCod_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSReferenciaTecnicaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSReferenciaTecnicaCod_Enabled), 5, 0)));
            }
            else
            {
               edtContagemItem_FSReferenciaTecnicaCod_Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSReferenciaTecnicaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSReferenciaTecnicaCod_Enabled), 5, 0)));
            }
         }
         if ( AV8WWPContext.gxTpr_Userehcontador )
         {
            edtContagemItem_FMReferenciaTecnicaCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMReferenciaTecnicaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMReferenciaTecnicaCod_Enabled), 5, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_ContagemItem_FMReferenciaTecnicaCod) )
            {
               edtContagemItem_FMReferenciaTecnicaCod_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMReferenciaTecnicaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMReferenciaTecnicaCod_Enabled), 5, 0)));
            }
            else
            {
               edtContagemItem_FMReferenciaTecnicaCod_Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMReferenciaTecnicaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMReferenciaTecnicaCod_Enabled), 5, 0)));
            }
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            tblTablefm_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablefm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablefm_Visible), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_ContagemItem_FMReferenciaTecnicaCod) )
         {
            A255ContagemItem_FMReferenciaTecnicaCod = AV14Insert_ContagemItem_FMReferenciaTecnicaCod;
            n255ContagemItem_FMReferenciaTecnicaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A255ContagemItem_FMReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_ContagemItem_FSReferenciaTecnicaCod) )
         {
            A226ContagemItem_FSReferenciaTecnicaCod = AV13Insert_ContagemItem_FSReferenciaTecnicaCod;
            n226ContagemItem_FSReferenciaTecnicaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A226ContagemItem_FSReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_FuncaoAPF_Codigo) )
         {
            A165FuncaoAPF_Codigo = AV12Insert_FuncaoAPF_Codigo;
            n165FuncaoAPF_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T001714 */
            pr_default.execute(10, new Object[] {A224ContagemItem_Lancamento});
            if ( (pr_default.getStatus(10) != 101) )
            {
               A251ContagemItem_FMDER = T001714_A251ContagemItem_FMDER[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
               n251ContagemItem_FMDER = T001714_n251ContagemItem_FMDER[0];
            }
            else
            {
               A251ContagemItem_FMDER = 0;
               n251ContagemItem_FMDER = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
            }
            pr_default.close(10);
            GXt_int1 = A252ContagemItem_FMAR;
            new prc_calculaaruniquefm(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            A252ContagemItem_FMAR = (short)(GXt_int1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A252ContagemItem_FMAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A252ContagemItem_FMAR), 3, 0)));
            GXt_int1 = A233ContagemItem_FSAR;
            new prc_calculaaruniquefs(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            A233ContagemItem_FSAR = (short)(GXt_int1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A233ContagemItem_FSAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A233ContagemItem_FSAR), 3, 0)));
            /* Using cursor T00179 */
            pr_default.execute(6, new Object[] {n255ContagemItem_FMReferenciaTecnicaCod, A255ContagemItem_FMReferenciaTecnicaCod});
            A257ContagemItem_FMReferenciaTecnicaNom = T00179_A257ContagemItem_FMReferenciaTecnicaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A257ContagemItem_FMReferenciaTecnicaNom", A257ContagemItem_FMReferenciaTecnicaNom);
            n257ContagemItem_FMReferenciaTecnicaNom = T00179_n257ContagemItem_FMReferenciaTecnicaNom[0];
            A256ContagemItem_FMReferenciaTecnicaVal = T00179_A256ContagemItem_FMReferenciaTecnicaVal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A256ContagemItem_FMReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5)));
            n256ContagemItem_FMReferenciaTecnicaVal = T00179_n256ContagemItem_FMReferenciaTecnicaVal[0];
            pr_default.close(6);
            /* Using cursor T00178 */
            pr_default.execute(5, new Object[] {n226ContagemItem_FSReferenciaTecnicaCod, A226ContagemItem_FSReferenciaTecnicaCod});
            A269ContagemItem_FSReferenciaTecnicaNom = T00178_A269ContagemItem_FSReferenciaTecnicaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A269ContagemItem_FSReferenciaTecnicaNom", A269ContagemItem_FSReferenciaTecnicaNom);
            n269ContagemItem_FSReferenciaTecnicaNom = T00178_n269ContagemItem_FSReferenciaTecnicaNom[0];
            A227ContagemItem_FSReferenciaTecnicaVal = T00178_A227ContagemItem_FSReferenciaTecnicaVal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A227ContagemItem_FSReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5)));
            n227ContagemItem_FSReferenciaTecnicaVal = T00178_n227ContagemItem_FSReferenciaTecnicaVal[0];
            pr_default.close(5);
            /* Using cursor T00177 */
            pr_default.execute(4, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
            A166FuncaoAPF_Nome = T00177_A166FuncaoAPF_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            A184FuncaoAPF_Tipo = T00177_A184FuncaoAPF_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
            pr_default.close(4);
            GXt_char2 = A185FuncaoAPF_Complexidade;
            new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A185FuncaoAPF_Complexidade = GXt_char2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contagem_Codigo) )
            {
               A192Contagem_Codigo = AV11Insert_Contagem_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
            }
         }
      }

      protected void Load1744( )
      {
         /* Using cursor T001717 */
         pr_default.execute(11, new Object[] {A224ContagemItem_Lancamento, A192Contagem_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound44 = 1;
            A268ContagemItem_FMValidacao = T001717_A268ContagemItem_FMValidacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A268ContagemItem_FMValidacao", StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0));
            n268ContagemItem_FMValidacao = T001717_n268ContagemItem_FMValidacao[0];
            A197Contagem_DataCriacao = T001717_A197Contagem_DataCriacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
            A194Contagem_AreaTrabalhoDes = T001717_A194Contagem_AreaTrabalhoDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
            n194Contagem_AreaTrabalhoDes = T001717_n194Contagem_AreaTrabalhoDes[0];
            A195Contagem_Tecnica = T001717_A195Contagem_Tecnica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
            n195Contagem_Tecnica = T001717_n195Contagem_Tecnica[0];
            A196Contagem_Tipo = T001717_A196Contagem_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A196Contagem_Tipo", A196Contagem_Tipo);
            n196Contagem_Tipo = T001717_n196Contagem_Tipo[0];
            A207Contagem_FabricaSoftwareCod = T001717_A207Contagem_FabricaSoftwareCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A207Contagem_FabricaSoftwareCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0)));
            A208Contagem_FabricaSoftwarePessoaCod = T001717_A208Contagem_FabricaSoftwarePessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A208Contagem_FabricaSoftwarePessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0)));
            A209Contagem_FabricaSoftwarePessoaNom = T001717_A209Contagem_FabricaSoftwarePessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A209Contagem_FabricaSoftwarePessoaNom", A209Contagem_FabricaSoftwarePessoaNom);
            A215Contagem_UsuarioContadorPessoaNom = T001717_A215Contagem_UsuarioContadorPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
            n215Contagem_UsuarioContadorPessoaNom = T001717_n215Contagem_UsuarioContadorPessoaNom[0];
            A216Contagem_UsuarioAuditorCod = T001717_A216Contagem_UsuarioAuditorCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A216Contagem_UsuarioAuditorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0)));
            A217Contagem_UsuarioAuditorPessoaCod = T001717_A217Contagem_UsuarioAuditorPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A217Contagem_UsuarioAuditorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0)));
            A218Contagem_UsuarioAuditorPessoaNom = T001717_A218Contagem_UsuarioAuditorPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A218Contagem_UsuarioAuditorPessoaNom", A218Contagem_UsuarioAuditorPessoaNom);
            A225ContagemItem_Sequencial = T001717_A225ContagemItem_Sequencial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A225ContagemItem_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A225ContagemItem_Sequencial), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMITEM_SEQUENCIAL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")));
            n225ContagemItem_Sequencial = T001717_n225ContagemItem_Sequencial[0];
            A229ContagemItem_Requisito = T001717_A229ContagemItem_Requisito[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A229ContagemItem_Requisito", StringUtil.LTrim( StringUtil.Str( (decimal)(A229ContagemItem_Requisito), 3, 0)));
            n229ContagemItem_Requisito = T001717_n229ContagemItem_Requisito[0];
            A950ContagemItem_PFB = T001717_A950ContagemItem_PFB[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A950ContagemItem_PFB", StringUtil.LTrim( StringUtil.Str( A950ContagemItem_PFB, 14, 5)));
            n950ContagemItem_PFB = T001717_n950ContagemItem_PFB[0];
            A951ContagemItem_PFL = T001717_A951ContagemItem_PFL[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A951ContagemItem_PFL", StringUtil.LTrim( StringUtil.Str( A951ContagemItem_PFL, 14, 5)));
            n951ContagemItem_PFL = T001717_n951ContagemItem_PFL[0];
            A166FuncaoAPF_Nome = T001717_A166FuncaoAPF_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            A184FuncaoAPF_Tipo = T001717_A184FuncaoAPF_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
            A952ContagemItem_TipoUnidade = T001717_A952ContagemItem_TipoUnidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A952ContagemItem_TipoUnidade", A952ContagemItem_TipoUnidade);
            A953ContagemItem_Evidencias = T001717_A953ContagemItem_Evidencias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A953ContagemItem_Evidencias", A953ContagemItem_Evidencias);
            n953ContagemItem_Evidencias = T001717_n953ContagemItem_Evidencias[0];
            A954ContagemItem_QtdINM = T001717_A954ContagemItem_QtdINM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A954ContagemItem_QtdINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A954ContagemItem_QtdINM), 4, 0)));
            n954ContagemItem_QtdINM = T001717_n954ContagemItem_QtdINM[0];
            A955ContagemItem_CP = T001717_A955ContagemItem_CP[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A955ContagemItem_CP", A955ContagemItem_CP);
            n955ContagemItem_CP = T001717_n955ContagemItem_CP[0];
            A956ContagemItem_RA = T001717_A956ContagemItem_RA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A956ContagemItem_RA", StringUtil.LTrim( StringUtil.Str( (decimal)(A956ContagemItem_RA), 3, 0)));
            n956ContagemItem_RA = T001717_n956ContagemItem_RA[0];
            A957ContagemItem_DER = T001717_A957ContagemItem_DER[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A957ContagemItem_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A957ContagemItem_DER), 3, 0)));
            n957ContagemItem_DER = T001717_n957ContagemItem_DER[0];
            A958ContagemItem_Funcao = T001717_A958ContagemItem_Funcao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A958ContagemItem_Funcao", A958ContagemItem_Funcao);
            n958ContagemItem_Funcao = T001717_n958ContagemItem_Funcao[0];
            A228ContagemItem_FSTipoUnidade = T001717_A228ContagemItem_FSTipoUnidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A228ContagemItem_FSTipoUnidade", A228ContagemItem_FSTipoUnidade);
            n228ContagemItem_FSTipoUnidade = T001717_n228ContagemItem_FSTipoUnidade[0];
            A267ContagemItem_FSValidacao = T001717_A267ContagemItem_FSValidacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A267ContagemItem_FSValidacao", StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0));
            n267ContagemItem_FSValidacao = T001717_n267ContagemItem_FSValidacao[0];
            A269ContagemItem_FSReferenciaTecnicaNom = T001717_A269ContagemItem_FSReferenciaTecnicaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A269ContagemItem_FSReferenciaTecnicaNom", A269ContagemItem_FSReferenciaTecnicaNom);
            n269ContagemItem_FSReferenciaTecnicaNom = T001717_n269ContagemItem_FSReferenciaTecnicaNom[0];
            A227ContagemItem_FSReferenciaTecnicaVal = T001717_A227ContagemItem_FSReferenciaTecnicaVal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A227ContagemItem_FSReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5)));
            n227ContagemItem_FSReferenciaTecnicaVal = T001717_n227ContagemItem_FSReferenciaTecnicaVal[0];
            A234ContagemItem_FSQTDInm = T001717_A234ContagemItem_FSQTDInm[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A234ContagemItem_FSQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A234ContagemItem_FSQTDInm), 9, 0)));
            n234ContagemItem_FSQTDInm = T001717_n234ContagemItem_FSQTDInm[0];
            A235ContagemItem_FSCP = T001717_A235ContagemItem_FSCP[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A235ContagemItem_FSCP", A235ContagemItem_FSCP);
            n235ContagemItem_FSCP = T001717_n235ContagemItem_FSCP[0];
            A236ContagemItem_FSPontoFuncaoBruto = T001717_A236ContagemItem_FSPontoFuncaoBruto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A236ContagemItem_FSPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A236ContagemItem_FSPontoFuncaoBruto, 7, 2)));
            n236ContagemItem_FSPontoFuncaoBruto = T001717_n236ContagemItem_FSPontoFuncaoBruto[0];
            A237ContagemItem_FSPontoFuncaoLiquido = T001717_A237ContagemItem_FSPontoFuncaoLiquido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A237ContagemItem_FSPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A237ContagemItem_FSPontoFuncaoLiquido, 7, 2)));
            n237ContagemItem_FSPontoFuncaoLiquido = T001717_n237ContagemItem_FSPontoFuncaoLiquido[0];
            A238ContagemItem_FSEvidencias = T001717_A238ContagemItem_FSEvidencias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A238ContagemItem_FSEvidencias", A238ContagemItem_FSEvidencias);
            n238ContagemItem_FSEvidencias = T001717_n238ContagemItem_FSEvidencias[0];
            A257ContagemItem_FMReferenciaTecnicaNom = T001717_A257ContagemItem_FMReferenciaTecnicaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A257ContagemItem_FMReferenciaTecnicaNom", A257ContagemItem_FMReferenciaTecnicaNom);
            n257ContagemItem_FMReferenciaTecnicaNom = T001717_n257ContagemItem_FMReferenciaTecnicaNom[0];
            A256ContagemItem_FMReferenciaTecnicaVal = T001717_A256ContagemItem_FMReferenciaTecnicaVal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A256ContagemItem_FMReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5)));
            n256ContagemItem_FMReferenciaTecnicaVal = T001717_n256ContagemItem_FMReferenciaTecnicaVal[0];
            A239ContagemItem_FMQTDInm = T001717_A239ContagemItem_FMQTDInm[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A239ContagemItem_FMQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A239ContagemItem_FMQTDInm), 9, 0)));
            n239ContagemItem_FMQTDInm = T001717_n239ContagemItem_FMQTDInm[0];
            A240ContagemItem_FMCP = T001717_A240ContagemItem_FMCP[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A240ContagemItem_FMCP", A240ContagemItem_FMCP);
            n240ContagemItem_FMCP = T001717_n240ContagemItem_FMCP[0];
            A241ContagemItem_FMPontoFuncaoBruto = T001717_A241ContagemItem_FMPontoFuncaoBruto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A241ContagemItem_FMPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A241ContagemItem_FMPontoFuncaoBruto, 7, 2)));
            n241ContagemItem_FMPontoFuncaoBruto = T001717_n241ContagemItem_FMPontoFuncaoBruto[0];
            A242ContagemItem_FMPontoFuncaoLiquido = T001717_A242ContagemItem_FMPontoFuncaoLiquido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A242ContagemItem_FMPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A242ContagemItem_FMPontoFuncaoLiquido, 7, 2)));
            n242ContagemItem_FMPontoFuncaoLiquido = T001717_n242ContagemItem_FMPontoFuncaoLiquido[0];
            A165FuncaoAPF_Codigo = T001717_A165FuncaoAPF_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            n165FuncaoAPF_Codigo = T001717_n165FuncaoAPF_Codigo[0];
            A226ContagemItem_FSReferenciaTecnicaCod = T001717_A226ContagemItem_FSReferenciaTecnicaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A226ContagemItem_FSReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0)));
            n226ContagemItem_FSReferenciaTecnicaCod = T001717_n226ContagemItem_FSReferenciaTecnicaCod[0];
            A255ContagemItem_FMReferenciaTecnicaCod = T001717_A255ContagemItem_FMReferenciaTecnicaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A255ContagemItem_FMReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), 6, 0)));
            n255ContagemItem_FMReferenciaTecnicaCod = T001717_n255ContagemItem_FMReferenciaTecnicaCod[0];
            A193Contagem_AreaTrabalhoCod = T001717_A193Contagem_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
            A213Contagem_UsuarioContadorCod = T001717_A213Contagem_UsuarioContadorCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
            n213Contagem_UsuarioContadorCod = T001717_n213Contagem_UsuarioContadorCod[0];
            A214Contagem_UsuarioContadorPessoaCod = T001717_A214Contagem_UsuarioContadorPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
            n214Contagem_UsuarioContadorPessoaCod = T001717_n214Contagem_UsuarioContadorPessoaCod[0];
            A232ContagemItem_FSDER = T001717_A232ContagemItem_FSDER[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A232ContagemItem_FSDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A232ContagemItem_FSDER), 3, 0)));
            n232ContagemItem_FSDER = T001717_n232ContagemItem_FSDER[0];
            A251ContagemItem_FMDER = T001717_A251ContagemItem_FMDER[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
            n251ContagemItem_FMDER = T001717_n251ContagemItem_FMDER[0];
            ZM1744( -41) ;
         }
         pr_default.close(11);
         OnLoadActions1744( ) ;
      }

      protected void OnLoadActions1744( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contagem_Codigo) )
         {
            A192Contagem_Codigo = AV11Insert_Contagem_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
         }
         GXt_int1 = A252ContagemItem_FMAR;
         new prc_calculaaruniquefm(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         A252ContagemItem_FMAR = (short)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A252ContagemItem_FMAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A252ContagemItem_FMAR), 3, 0)));
         GXt_int1 = A233ContagemItem_FSAR;
         new prc_calculaaruniquefs(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         A233ContagemItem_FSAR = (short)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A233ContagemItem_FSAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A233ContagemItem_FSAR), 3, 0)));
         GXt_char2 = A185FuncaoAPF_Complexidade;
         new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A185FuncaoAPF_Complexidade = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         if ( ( ( A267ContagemItem_FSValidacao == 1 ) ) && ( ( A268ContagemItem_FMValidacao == 1 ) ) )
         {
            A260ContagemItem_ValidacaoStatusFinal = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
         }
         else
         {
            A260ContagemItem_ValidacaoStatusFinal = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
         }
      }

      protected void CheckExtendedTable1744( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contagem_Codigo) )
         {
            A192Contagem_Codigo = AV11Insert_Contagem_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
         }
         /* Using cursor T001714 */
         pr_default.execute(10, new Object[] {A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(10) != 101) )
         {
            A251ContagemItem_FMDER = T001714_A251ContagemItem_FMDER[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
            n251ContagemItem_FMDER = T001714_n251ContagemItem_FMDER[0];
         }
         else
         {
            A251ContagemItem_FMDER = 0;
            n251ContagemItem_FMDER = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
         }
         pr_default.close(10);
         GXt_int1 = A252ContagemItem_FMAR;
         new prc_calculaaruniquefm(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         A252ContagemItem_FMAR = (short)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A252ContagemItem_FMAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A252ContagemItem_FMAR), 3, 0)));
         GXt_int1 = A233ContagemItem_FSAR;
         new prc_calculaaruniquefs(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         A233ContagemItem_FSAR = (short)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A233ContagemItem_FSAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A233ContagemItem_FSAR), 3, 0)));
         /* Using cursor T00177 */
         pr_default.execute(4, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A165FuncaoAPF_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Fun��es APF - An�lise de Ponto de Fun��o'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_CODIGO");
               AnyError = 1;
               GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A166FuncaoAPF_Nome = T00177_A166FuncaoAPF_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
         A184FuncaoAPF_Tipo = T00177_A184FuncaoAPF_Tipo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
         pr_default.close(4);
         GXt_char2 = A185FuncaoAPF_Complexidade;
         new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A185FuncaoAPF_Complexidade = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         if ( ! ( ( StringUtil.StrCmp(A228ContagemItem_FSTipoUnidade, "EE") == 0 ) || ( StringUtil.StrCmp(A228ContagemItem_FSTipoUnidade, "CE") == 0 ) || ( StringUtil.StrCmp(A228ContagemItem_FSTipoUnidade, "SE") == 0 ) || ( StringUtil.StrCmp(A228ContagemItem_FSTipoUnidade, "ALI") == 0 ) || ( StringUtil.StrCmp(A228ContagemItem_FSTipoUnidade, "aie") == 0 ) || ( StringUtil.StrCmp(A228ContagemItem_FSTipoUnidade, "DC") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A228ContagemItem_FSTipoUnidade)) ) )
         {
            GX_msglist.addItem("Campo Tipo PF(F�b. Software) fora do intervalo", "OutOfRange", 1, "CONTAGEMITEM_FSTIPOUNIDADE");
            AnyError = 1;
            GX_FocusControl = cmbContagemItem_FSTipoUnidade_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( A267ContagemItem_FSValidacao == 1 ) || ( A267ContagemItem_FSValidacao == 2 ) || (0==A267ContagemItem_FSValidacao) ) )
         {
            GX_msglist.addItem("Campo Valida��o(Status) F�brica de Software fora do intervalo", "OutOfRange", 1, "CONTAGEMITEM_FSVALIDACAO");
            AnyError = 1;
            GX_FocusControl = cmbContagemItem_FSValidacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ( ( A267ContagemItem_FSValidacao == 1 ) ) && ( ( A268ContagemItem_FMValidacao == 1 ) ) )
         {
            A260ContagemItem_ValidacaoStatusFinal = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
         }
         else
         {
            A260ContagemItem_ValidacaoStatusFinal = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
         }
         /* Using cursor T00178 */
         pr_default.execute(5, new Object[] {n226ContagemItem_FSReferenciaTecnicaCod, A226ContagemItem_FSReferenciaTecnicaCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A226ContagemItem_FSReferenciaTecnicaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Refer�ncia T�cnica'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_FSREFERENCIATECNICACOD");
               AnyError = 1;
               GX_FocusControl = edtContagemItem_FSReferenciaTecnicaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A269ContagemItem_FSReferenciaTecnicaNom = T00178_A269ContagemItem_FSReferenciaTecnicaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A269ContagemItem_FSReferenciaTecnicaNom", A269ContagemItem_FSReferenciaTecnicaNom);
         n269ContagemItem_FSReferenciaTecnicaNom = T00178_n269ContagemItem_FSReferenciaTecnicaNom[0];
         A227ContagemItem_FSReferenciaTecnicaVal = T00178_A227ContagemItem_FSReferenciaTecnicaVal[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A227ContagemItem_FSReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5)));
         n227ContagemItem_FSReferenciaTecnicaVal = T00178_n227ContagemItem_FSReferenciaTecnicaVal[0];
         pr_default.close(5);
         if ( ! ( ( A268ContagemItem_FMValidacao == 1 ) || ( A268ContagemItem_FMValidacao == 2 ) || (0==A268ContagemItem_FMValidacao) ) )
         {
            GX_msglist.addItem("Campo Valida��o(Status) F�brica de M�trica fora do intervalo", "OutOfRange", 1, "CONTAGEMITEM_FMVALIDACAO");
            AnyError = 1;
            GX_FocusControl = cmbContagemItem_FMValidacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00179 */
         pr_default.execute(6, new Object[] {n255ContagemItem_FMReferenciaTecnicaCod, A255ContagemItem_FMReferenciaTecnicaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A255ContagemItem_FMReferenciaTecnicaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Item_FMReferencia Tecnica'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_FMREFERENCIATECNICACOD");
               AnyError = 1;
               GX_FocusControl = edtContagemItem_FMReferenciaTecnicaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A257ContagemItem_FMReferenciaTecnicaNom = T00179_A257ContagemItem_FMReferenciaTecnicaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A257ContagemItem_FMReferenciaTecnicaNom", A257ContagemItem_FMReferenciaTecnicaNom);
         n257ContagemItem_FMReferenciaTecnicaNom = T00179_n257ContagemItem_FMReferenciaTecnicaNom[0];
         A256ContagemItem_FMReferenciaTecnicaVal = T00179_A256ContagemItem_FMReferenciaTecnicaVal[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A256ContagemItem_FMReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5)));
         n256ContagemItem_FMReferenciaTecnicaVal = T00179_n256ContagemItem_FMReferenciaTecnicaVal[0];
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors1744( )
      {
         pr_default.close(10);
         pr_default.close(4);
         pr_default.close(5);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_50( int A224ContagemItem_Lancamento )
      {
         /* Using cursor T001719 */
         pr_default.execute(12, new Object[] {A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(12) != 101) )
         {
            A251ContagemItem_FMDER = T001719_A251ContagemItem_FMDER[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
            n251ContagemItem_FMDER = T001719_n251ContagemItem_FMDER[0];
         }
         else
         {
            A251ContagemItem_FMDER = 0;
            n251ContagemItem_FMDER = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A251ContagemItem_FMDER), 3, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void gxLoad_44( int A165FuncaoAPF_Codigo )
      {
         /* Using cursor T001720 */
         pr_default.execute(13, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            if ( ! ( (0==A165FuncaoAPF_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Fun��es APF - An�lise de Ponto de Fun��o'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_CODIGO");
               AnyError = 1;
               GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A166FuncaoAPF_Nome = T001720_A166FuncaoAPF_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
         A184FuncaoAPF_Tipo = T001720_A184FuncaoAPF_Tipo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A166FuncaoAPF_Nome)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A184FuncaoAPF_Tipo))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(13) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(13);
      }

      protected void gxLoad_45( int A226ContagemItem_FSReferenciaTecnicaCod )
      {
         /* Using cursor T001721 */
         pr_default.execute(14, new Object[] {n226ContagemItem_FSReferenciaTecnicaCod, A226ContagemItem_FSReferenciaTecnicaCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            if ( ! ( (0==A226ContagemItem_FSReferenciaTecnicaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Refer�ncia T�cnica'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_FSREFERENCIATECNICACOD");
               AnyError = 1;
               GX_FocusControl = edtContagemItem_FSReferenciaTecnicaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A269ContagemItem_FSReferenciaTecnicaNom = T001721_A269ContagemItem_FSReferenciaTecnicaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A269ContagemItem_FSReferenciaTecnicaNom", A269ContagemItem_FSReferenciaTecnicaNom);
         n269ContagemItem_FSReferenciaTecnicaNom = T001721_n269ContagemItem_FSReferenciaTecnicaNom[0];
         A227ContagemItem_FSReferenciaTecnicaVal = T001721_A227ContagemItem_FSReferenciaTecnicaVal[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A227ContagemItem_FSReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5)));
         n227ContagemItem_FSReferenciaTecnicaVal = T001721_n227ContagemItem_FSReferenciaTecnicaVal[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A269ContagemItem_FSReferenciaTecnicaNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(14) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(14);
      }

      protected void gxLoad_46( int A255ContagemItem_FMReferenciaTecnicaCod )
      {
         /* Using cursor T001722 */
         pr_default.execute(15, new Object[] {n255ContagemItem_FMReferenciaTecnicaCod, A255ContagemItem_FMReferenciaTecnicaCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (0==A255ContagemItem_FMReferenciaTecnicaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Item_FMReferencia Tecnica'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_FMREFERENCIATECNICACOD");
               AnyError = 1;
               GX_FocusControl = edtContagemItem_FMReferenciaTecnicaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A257ContagemItem_FMReferenciaTecnicaNom = T001722_A257ContagemItem_FMReferenciaTecnicaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A257ContagemItem_FMReferenciaTecnicaNom", A257ContagemItem_FMReferenciaTecnicaNom);
         n257ContagemItem_FMReferenciaTecnicaNom = T001722_n257ContagemItem_FMReferenciaTecnicaNom[0];
         A256ContagemItem_FMReferenciaTecnicaVal = T001722_A256ContagemItem_FMReferenciaTecnicaVal[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A256ContagemItem_FMReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5)));
         n256ContagemItem_FMReferenciaTecnicaVal = T001722_n256ContagemItem_FMReferenciaTecnicaVal[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A257ContagemItem_FMReferenciaTecnicaNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(15) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(15);
      }

      protected void GetKey1744( )
      {
         /* Using cursor T001723 */
         pr_default.execute(16, new Object[] {A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound44 = 1;
         }
         else
         {
            RcdFound44 = 0;
         }
         pr_default.close(16);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00173 */
         pr_default.execute(1, new Object[] {A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(1) != 101) && ( T00173_A192Contagem_Codigo[0] == A192Contagem_Codigo ) )
         {
            ZM1744( 41) ;
            RcdFound44 = 1;
            A224ContagemItem_Lancamento = T00173_A224ContagemItem_Lancamento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            A268ContagemItem_FMValidacao = T00173_A268ContagemItem_FMValidacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A268ContagemItem_FMValidacao", StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0));
            n268ContagemItem_FMValidacao = T00173_n268ContagemItem_FMValidacao[0];
            A207Contagem_FabricaSoftwareCod = T00173_A207Contagem_FabricaSoftwareCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A207Contagem_FabricaSoftwareCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0)));
            A208Contagem_FabricaSoftwarePessoaCod = T00173_A208Contagem_FabricaSoftwarePessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A208Contagem_FabricaSoftwarePessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0)));
            A209Contagem_FabricaSoftwarePessoaNom = T00173_A209Contagem_FabricaSoftwarePessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A209Contagem_FabricaSoftwarePessoaNom", A209Contagem_FabricaSoftwarePessoaNom);
            A216Contagem_UsuarioAuditorCod = T00173_A216Contagem_UsuarioAuditorCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A216Contagem_UsuarioAuditorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0)));
            A217Contagem_UsuarioAuditorPessoaCod = T00173_A217Contagem_UsuarioAuditorPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A217Contagem_UsuarioAuditorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0)));
            A218Contagem_UsuarioAuditorPessoaNom = T00173_A218Contagem_UsuarioAuditorPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A218Contagem_UsuarioAuditorPessoaNom", A218Contagem_UsuarioAuditorPessoaNom);
            A225ContagemItem_Sequencial = T00173_A225ContagemItem_Sequencial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A225ContagemItem_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A225ContagemItem_Sequencial), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMITEM_SEQUENCIAL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")));
            n225ContagemItem_Sequencial = T00173_n225ContagemItem_Sequencial[0];
            A229ContagemItem_Requisito = T00173_A229ContagemItem_Requisito[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A229ContagemItem_Requisito", StringUtil.LTrim( StringUtil.Str( (decimal)(A229ContagemItem_Requisito), 3, 0)));
            n229ContagemItem_Requisito = T00173_n229ContagemItem_Requisito[0];
            A950ContagemItem_PFB = T00173_A950ContagemItem_PFB[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A950ContagemItem_PFB", StringUtil.LTrim( StringUtil.Str( A950ContagemItem_PFB, 14, 5)));
            n950ContagemItem_PFB = T00173_n950ContagemItem_PFB[0];
            A951ContagemItem_PFL = T00173_A951ContagemItem_PFL[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A951ContagemItem_PFL", StringUtil.LTrim( StringUtil.Str( A951ContagemItem_PFL, 14, 5)));
            n951ContagemItem_PFL = T00173_n951ContagemItem_PFL[0];
            A952ContagemItem_TipoUnidade = T00173_A952ContagemItem_TipoUnidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A952ContagemItem_TipoUnidade", A952ContagemItem_TipoUnidade);
            A953ContagemItem_Evidencias = T00173_A953ContagemItem_Evidencias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A953ContagemItem_Evidencias", A953ContagemItem_Evidencias);
            n953ContagemItem_Evidencias = T00173_n953ContagemItem_Evidencias[0];
            A954ContagemItem_QtdINM = T00173_A954ContagemItem_QtdINM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A954ContagemItem_QtdINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A954ContagemItem_QtdINM), 4, 0)));
            n954ContagemItem_QtdINM = T00173_n954ContagemItem_QtdINM[0];
            A955ContagemItem_CP = T00173_A955ContagemItem_CP[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A955ContagemItem_CP", A955ContagemItem_CP);
            n955ContagemItem_CP = T00173_n955ContagemItem_CP[0];
            A956ContagemItem_RA = T00173_A956ContagemItem_RA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A956ContagemItem_RA", StringUtil.LTrim( StringUtil.Str( (decimal)(A956ContagemItem_RA), 3, 0)));
            n956ContagemItem_RA = T00173_n956ContagemItem_RA[0];
            A957ContagemItem_DER = T00173_A957ContagemItem_DER[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A957ContagemItem_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A957ContagemItem_DER), 3, 0)));
            n957ContagemItem_DER = T00173_n957ContagemItem_DER[0];
            A958ContagemItem_Funcao = T00173_A958ContagemItem_Funcao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A958ContagemItem_Funcao", A958ContagemItem_Funcao);
            n958ContagemItem_Funcao = T00173_n958ContagemItem_Funcao[0];
            A228ContagemItem_FSTipoUnidade = T00173_A228ContagemItem_FSTipoUnidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A228ContagemItem_FSTipoUnidade", A228ContagemItem_FSTipoUnidade);
            n228ContagemItem_FSTipoUnidade = T00173_n228ContagemItem_FSTipoUnidade[0];
            A267ContagemItem_FSValidacao = T00173_A267ContagemItem_FSValidacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A267ContagemItem_FSValidacao", StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0));
            n267ContagemItem_FSValidacao = T00173_n267ContagemItem_FSValidacao[0];
            A234ContagemItem_FSQTDInm = T00173_A234ContagemItem_FSQTDInm[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A234ContagemItem_FSQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A234ContagemItem_FSQTDInm), 9, 0)));
            n234ContagemItem_FSQTDInm = T00173_n234ContagemItem_FSQTDInm[0];
            A235ContagemItem_FSCP = T00173_A235ContagemItem_FSCP[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A235ContagemItem_FSCP", A235ContagemItem_FSCP);
            n235ContagemItem_FSCP = T00173_n235ContagemItem_FSCP[0];
            A236ContagemItem_FSPontoFuncaoBruto = T00173_A236ContagemItem_FSPontoFuncaoBruto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A236ContagemItem_FSPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A236ContagemItem_FSPontoFuncaoBruto, 7, 2)));
            n236ContagemItem_FSPontoFuncaoBruto = T00173_n236ContagemItem_FSPontoFuncaoBruto[0];
            A237ContagemItem_FSPontoFuncaoLiquido = T00173_A237ContagemItem_FSPontoFuncaoLiquido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A237ContagemItem_FSPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A237ContagemItem_FSPontoFuncaoLiquido, 7, 2)));
            n237ContagemItem_FSPontoFuncaoLiquido = T00173_n237ContagemItem_FSPontoFuncaoLiquido[0];
            A238ContagemItem_FSEvidencias = T00173_A238ContagemItem_FSEvidencias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A238ContagemItem_FSEvidencias", A238ContagemItem_FSEvidencias);
            n238ContagemItem_FSEvidencias = T00173_n238ContagemItem_FSEvidencias[0];
            A239ContagemItem_FMQTDInm = T00173_A239ContagemItem_FMQTDInm[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A239ContagemItem_FMQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A239ContagemItem_FMQTDInm), 9, 0)));
            n239ContagemItem_FMQTDInm = T00173_n239ContagemItem_FMQTDInm[0];
            A240ContagemItem_FMCP = T00173_A240ContagemItem_FMCP[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A240ContagemItem_FMCP", A240ContagemItem_FMCP);
            n240ContagemItem_FMCP = T00173_n240ContagemItem_FMCP[0];
            A241ContagemItem_FMPontoFuncaoBruto = T00173_A241ContagemItem_FMPontoFuncaoBruto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A241ContagemItem_FMPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A241ContagemItem_FMPontoFuncaoBruto, 7, 2)));
            n241ContagemItem_FMPontoFuncaoBruto = T00173_n241ContagemItem_FMPontoFuncaoBruto[0];
            A242ContagemItem_FMPontoFuncaoLiquido = T00173_A242ContagemItem_FMPontoFuncaoLiquido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A242ContagemItem_FMPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A242ContagemItem_FMPontoFuncaoLiquido, 7, 2)));
            n242ContagemItem_FMPontoFuncaoLiquido = T00173_n242ContagemItem_FMPontoFuncaoLiquido[0];
            A165FuncaoAPF_Codigo = T00173_A165FuncaoAPF_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            n165FuncaoAPF_Codigo = T00173_n165FuncaoAPF_Codigo[0];
            A226ContagemItem_FSReferenciaTecnicaCod = T00173_A226ContagemItem_FSReferenciaTecnicaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A226ContagemItem_FSReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0)));
            n226ContagemItem_FSReferenciaTecnicaCod = T00173_n226ContagemItem_FSReferenciaTecnicaCod[0];
            A255ContagemItem_FMReferenciaTecnicaCod = T00173_A255ContagemItem_FMReferenciaTecnicaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A255ContagemItem_FMReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), 6, 0)));
            n255ContagemItem_FMReferenciaTecnicaCod = T00173_n255ContagemItem_FMReferenciaTecnicaCod[0];
            Z224ContagemItem_Lancamento = A224ContagemItem_Lancamento;
            sMode44 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1744( ) ;
            if ( AnyError == 1 )
            {
               RcdFound44 = 0;
               InitializeNonKey1744( ) ;
            }
            Gx_mode = sMode44;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound44 = 0;
            InitializeNonKey1744( ) ;
            sMode44 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode44;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1744( ) ;
         if ( RcdFound44 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound44 = 0;
         /* Using cursor T001724 */
         pr_default.execute(17, new Object[] {A224ContagemItem_Lancamento, A192Contagem_Codigo});
         if ( (pr_default.getStatus(17) != 101) )
         {
            while ( (pr_default.getStatus(17) != 101) && ( ( T001724_A224ContagemItem_Lancamento[0] < A224ContagemItem_Lancamento ) ) && ( T001724_A192Contagem_Codigo[0] == A192Contagem_Codigo ) )
            {
               pr_default.readNext(17);
            }
            if ( (pr_default.getStatus(17) != 101) && ( ( T001724_A224ContagemItem_Lancamento[0] > A224ContagemItem_Lancamento ) ) && ( T001724_A192Contagem_Codigo[0] == A192Contagem_Codigo ) )
            {
               A224ContagemItem_Lancamento = T001724_A224ContagemItem_Lancamento[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
               RcdFound44 = 1;
            }
         }
         pr_default.close(17);
      }

      protected void move_previous( )
      {
         RcdFound44 = 0;
         /* Using cursor T001725 */
         pr_default.execute(18, new Object[] {A224ContagemItem_Lancamento, A192Contagem_Codigo});
         if ( (pr_default.getStatus(18) != 101) )
         {
            while ( (pr_default.getStatus(18) != 101) && ( ( T001725_A224ContagemItem_Lancamento[0] > A224ContagemItem_Lancamento ) ) && ( T001725_A192Contagem_Codigo[0] == A192Contagem_Codigo ) )
            {
               pr_default.readNext(18);
            }
            if ( (pr_default.getStatus(18) != 101) && ( ( T001725_A224ContagemItem_Lancamento[0] < A224ContagemItem_Lancamento ) ) && ( T001725_A192Contagem_Codigo[0] == A192Contagem_Codigo ) )
            {
               A224ContagemItem_Lancamento = T001725_A224ContagemItem_Lancamento[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
               RcdFound44 = 1;
            }
         }
         pr_default.close(18);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1744( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemItem_PFB_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1744( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound44 == 1 )
            {
               if ( A224ContagemItem_Lancamento != Z224ContagemItem_Lancamento )
               {
                  A224ContagemItem_Lancamento = Z224ContagemItem_Lancamento;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMITEM_LANCAMENTO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_Lancamento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemItem_PFB_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1744( ) ;
                  GX_FocusControl = edtContagemItem_PFB_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A224ContagemItem_Lancamento != Z224ContagemItem_Lancamento )
               {
                  /* Insert record */
                  GX_FocusControl = edtContagemItem_PFB_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1744( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMITEM_LANCAMENTO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemItem_Lancamento_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContagemItem_PFB_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1744( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A224ContagemItem_Lancamento != Z224ContagemItem_Lancamento )
         {
            A224ContagemItem_Lancamento = Z224ContagemItem_Lancamento;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMITEM_LANCAMENTO");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_Lancamento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemItem_PFB_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1744( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00172 */
            pr_default.execute(0, new Object[] {A224ContagemItem_Lancamento});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemItem"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z268ContagemItem_FMValidacao != T00172_A268ContagemItem_FMValidacao[0] ) || ( Z207Contagem_FabricaSoftwareCod != T00172_A207Contagem_FabricaSoftwareCod[0] ) || ( Z208Contagem_FabricaSoftwarePessoaCod != T00172_A208Contagem_FabricaSoftwarePessoaCod[0] ) || ( StringUtil.StrCmp(Z209Contagem_FabricaSoftwarePessoaNom, T00172_A209Contagem_FabricaSoftwarePessoaNom[0]) != 0 ) || ( Z216Contagem_UsuarioAuditorCod != T00172_A216Contagem_UsuarioAuditorCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z217Contagem_UsuarioAuditorPessoaCod != T00172_A217Contagem_UsuarioAuditorPessoaCod[0] ) || ( StringUtil.StrCmp(Z218Contagem_UsuarioAuditorPessoaNom, T00172_A218Contagem_UsuarioAuditorPessoaNom[0]) != 0 ) || ( Z225ContagemItem_Sequencial != T00172_A225ContagemItem_Sequencial[0] ) || ( Z229ContagemItem_Requisito != T00172_A229ContagemItem_Requisito[0] ) || ( Z950ContagemItem_PFB != T00172_A950ContagemItem_PFB[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z951ContagemItem_PFL != T00172_A951ContagemItem_PFL[0] ) || ( StringUtil.StrCmp(Z952ContagemItem_TipoUnidade, T00172_A952ContagemItem_TipoUnidade[0]) != 0 ) || ( Z954ContagemItem_QtdINM != T00172_A954ContagemItem_QtdINM[0] ) || ( StringUtil.StrCmp(Z955ContagemItem_CP, T00172_A955ContagemItem_CP[0]) != 0 ) || ( Z956ContagemItem_RA != T00172_A956ContagemItem_RA[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z957ContagemItem_DER != T00172_A957ContagemItem_DER[0] ) || ( StringUtil.StrCmp(Z958ContagemItem_Funcao, T00172_A958ContagemItem_Funcao[0]) != 0 ) || ( StringUtil.StrCmp(Z228ContagemItem_FSTipoUnidade, T00172_A228ContagemItem_FSTipoUnidade[0]) != 0 ) || ( Z267ContagemItem_FSValidacao != T00172_A267ContagemItem_FSValidacao[0] ) || ( Z234ContagemItem_FSQTDInm != T00172_A234ContagemItem_FSQTDInm[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z235ContagemItem_FSCP, T00172_A235ContagemItem_FSCP[0]) != 0 ) || ( Z236ContagemItem_FSPontoFuncaoBruto != T00172_A236ContagemItem_FSPontoFuncaoBruto[0] ) || ( Z237ContagemItem_FSPontoFuncaoLiquido != T00172_A237ContagemItem_FSPontoFuncaoLiquido[0] ) || ( Z239ContagemItem_FMQTDInm != T00172_A239ContagemItem_FMQTDInm[0] ) || ( StringUtil.StrCmp(Z240ContagemItem_FMCP, T00172_A240ContagemItem_FMCP[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z241ContagemItem_FMPontoFuncaoBruto != T00172_A241ContagemItem_FMPontoFuncaoBruto[0] ) || ( Z242ContagemItem_FMPontoFuncaoLiquido != T00172_A242ContagemItem_FMPontoFuncaoLiquido[0] ) || ( Z165FuncaoAPF_Codigo != T00172_A165FuncaoAPF_Codigo[0] ) || ( Z226ContagemItem_FSReferenciaTecnicaCod != T00172_A226ContagemItem_FSReferenciaTecnicaCod[0] ) || ( Z255ContagemItem_FMReferenciaTecnicaCod != T00172_A255ContagemItem_FMReferenciaTecnicaCod[0] ) )
            {
               if ( Z268ContagemItem_FMValidacao != T00172_A268ContagemItem_FMValidacao[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_FMValidacao");
                  GXUtil.WriteLogRaw("Old: ",Z268ContagemItem_FMValidacao);
                  GXUtil.WriteLogRaw("Current: ",T00172_A268ContagemItem_FMValidacao[0]);
               }
               if ( Z207Contagem_FabricaSoftwareCod != T00172_A207Contagem_FabricaSoftwareCod[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"Contagem_FabricaSoftwareCod");
                  GXUtil.WriteLogRaw("Old: ",Z207Contagem_FabricaSoftwareCod);
                  GXUtil.WriteLogRaw("Current: ",T00172_A207Contagem_FabricaSoftwareCod[0]);
               }
               if ( Z208Contagem_FabricaSoftwarePessoaCod != T00172_A208Contagem_FabricaSoftwarePessoaCod[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"Contagem_FabricaSoftwarePessoaCod");
                  GXUtil.WriteLogRaw("Old: ",Z208Contagem_FabricaSoftwarePessoaCod);
                  GXUtil.WriteLogRaw("Current: ",T00172_A208Contagem_FabricaSoftwarePessoaCod[0]);
               }
               if ( StringUtil.StrCmp(Z209Contagem_FabricaSoftwarePessoaNom, T00172_A209Contagem_FabricaSoftwarePessoaNom[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"Contagem_FabricaSoftwarePessoaNom");
                  GXUtil.WriteLogRaw("Old: ",Z209Contagem_FabricaSoftwarePessoaNom);
                  GXUtil.WriteLogRaw("Current: ",T00172_A209Contagem_FabricaSoftwarePessoaNom[0]);
               }
               if ( Z216Contagem_UsuarioAuditorCod != T00172_A216Contagem_UsuarioAuditorCod[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"Contagem_UsuarioAuditorCod");
                  GXUtil.WriteLogRaw("Old: ",Z216Contagem_UsuarioAuditorCod);
                  GXUtil.WriteLogRaw("Current: ",T00172_A216Contagem_UsuarioAuditorCod[0]);
               }
               if ( Z217Contagem_UsuarioAuditorPessoaCod != T00172_A217Contagem_UsuarioAuditorPessoaCod[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"Contagem_UsuarioAuditorPessoaCod");
                  GXUtil.WriteLogRaw("Old: ",Z217Contagem_UsuarioAuditorPessoaCod);
                  GXUtil.WriteLogRaw("Current: ",T00172_A217Contagem_UsuarioAuditorPessoaCod[0]);
               }
               if ( StringUtil.StrCmp(Z218Contagem_UsuarioAuditorPessoaNom, T00172_A218Contagem_UsuarioAuditorPessoaNom[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"Contagem_UsuarioAuditorPessoaNom");
                  GXUtil.WriteLogRaw("Old: ",Z218Contagem_UsuarioAuditorPessoaNom);
                  GXUtil.WriteLogRaw("Current: ",T00172_A218Contagem_UsuarioAuditorPessoaNom[0]);
               }
               if ( Z225ContagemItem_Sequencial != T00172_A225ContagemItem_Sequencial[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_Sequencial");
                  GXUtil.WriteLogRaw("Old: ",Z225ContagemItem_Sequencial);
                  GXUtil.WriteLogRaw("Current: ",T00172_A225ContagemItem_Sequencial[0]);
               }
               if ( Z229ContagemItem_Requisito != T00172_A229ContagemItem_Requisito[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_Requisito");
                  GXUtil.WriteLogRaw("Old: ",Z229ContagemItem_Requisito);
                  GXUtil.WriteLogRaw("Current: ",T00172_A229ContagemItem_Requisito[0]);
               }
               if ( Z950ContagemItem_PFB != T00172_A950ContagemItem_PFB[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_PFB");
                  GXUtil.WriteLogRaw("Old: ",Z950ContagemItem_PFB);
                  GXUtil.WriteLogRaw("Current: ",T00172_A950ContagemItem_PFB[0]);
               }
               if ( Z951ContagemItem_PFL != T00172_A951ContagemItem_PFL[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_PFL");
                  GXUtil.WriteLogRaw("Old: ",Z951ContagemItem_PFL);
                  GXUtil.WriteLogRaw("Current: ",T00172_A951ContagemItem_PFL[0]);
               }
               if ( StringUtil.StrCmp(Z952ContagemItem_TipoUnidade, T00172_A952ContagemItem_TipoUnidade[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_TipoUnidade");
                  GXUtil.WriteLogRaw("Old: ",Z952ContagemItem_TipoUnidade);
                  GXUtil.WriteLogRaw("Current: ",T00172_A952ContagemItem_TipoUnidade[0]);
               }
               if ( Z954ContagemItem_QtdINM != T00172_A954ContagemItem_QtdINM[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_QtdINM");
                  GXUtil.WriteLogRaw("Old: ",Z954ContagemItem_QtdINM);
                  GXUtil.WriteLogRaw("Current: ",T00172_A954ContagemItem_QtdINM[0]);
               }
               if ( StringUtil.StrCmp(Z955ContagemItem_CP, T00172_A955ContagemItem_CP[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_CP");
                  GXUtil.WriteLogRaw("Old: ",Z955ContagemItem_CP);
                  GXUtil.WriteLogRaw("Current: ",T00172_A955ContagemItem_CP[0]);
               }
               if ( Z956ContagemItem_RA != T00172_A956ContagemItem_RA[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_RA");
                  GXUtil.WriteLogRaw("Old: ",Z956ContagemItem_RA);
                  GXUtil.WriteLogRaw("Current: ",T00172_A956ContagemItem_RA[0]);
               }
               if ( Z957ContagemItem_DER != T00172_A957ContagemItem_DER[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_DER");
                  GXUtil.WriteLogRaw("Old: ",Z957ContagemItem_DER);
                  GXUtil.WriteLogRaw("Current: ",T00172_A957ContagemItem_DER[0]);
               }
               if ( StringUtil.StrCmp(Z958ContagemItem_Funcao, T00172_A958ContagemItem_Funcao[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_Funcao");
                  GXUtil.WriteLogRaw("Old: ",Z958ContagemItem_Funcao);
                  GXUtil.WriteLogRaw("Current: ",T00172_A958ContagemItem_Funcao[0]);
               }
               if ( StringUtil.StrCmp(Z228ContagemItem_FSTipoUnidade, T00172_A228ContagemItem_FSTipoUnidade[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_FSTipoUnidade");
                  GXUtil.WriteLogRaw("Old: ",Z228ContagemItem_FSTipoUnidade);
                  GXUtil.WriteLogRaw("Current: ",T00172_A228ContagemItem_FSTipoUnidade[0]);
               }
               if ( Z267ContagemItem_FSValidacao != T00172_A267ContagemItem_FSValidacao[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_FSValidacao");
                  GXUtil.WriteLogRaw("Old: ",Z267ContagemItem_FSValidacao);
                  GXUtil.WriteLogRaw("Current: ",T00172_A267ContagemItem_FSValidacao[0]);
               }
               if ( Z234ContagemItem_FSQTDInm != T00172_A234ContagemItem_FSQTDInm[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_FSQTDInm");
                  GXUtil.WriteLogRaw("Old: ",Z234ContagemItem_FSQTDInm);
                  GXUtil.WriteLogRaw("Current: ",T00172_A234ContagemItem_FSQTDInm[0]);
               }
               if ( StringUtil.StrCmp(Z235ContagemItem_FSCP, T00172_A235ContagemItem_FSCP[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_FSCP");
                  GXUtil.WriteLogRaw("Old: ",Z235ContagemItem_FSCP);
                  GXUtil.WriteLogRaw("Current: ",T00172_A235ContagemItem_FSCP[0]);
               }
               if ( Z236ContagemItem_FSPontoFuncaoBruto != T00172_A236ContagemItem_FSPontoFuncaoBruto[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_FSPontoFuncaoBruto");
                  GXUtil.WriteLogRaw("Old: ",Z236ContagemItem_FSPontoFuncaoBruto);
                  GXUtil.WriteLogRaw("Current: ",T00172_A236ContagemItem_FSPontoFuncaoBruto[0]);
               }
               if ( Z237ContagemItem_FSPontoFuncaoLiquido != T00172_A237ContagemItem_FSPontoFuncaoLiquido[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_FSPontoFuncaoLiquido");
                  GXUtil.WriteLogRaw("Old: ",Z237ContagemItem_FSPontoFuncaoLiquido);
                  GXUtil.WriteLogRaw("Current: ",T00172_A237ContagemItem_FSPontoFuncaoLiquido[0]);
               }
               if ( Z239ContagemItem_FMQTDInm != T00172_A239ContagemItem_FMQTDInm[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_FMQTDInm");
                  GXUtil.WriteLogRaw("Old: ",Z239ContagemItem_FMQTDInm);
                  GXUtil.WriteLogRaw("Current: ",T00172_A239ContagemItem_FMQTDInm[0]);
               }
               if ( StringUtil.StrCmp(Z240ContagemItem_FMCP, T00172_A240ContagemItem_FMCP[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_FMCP");
                  GXUtil.WriteLogRaw("Old: ",Z240ContagemItem_FMCP);
                  GXUtil.WriteLogRaw("Current: ",T00172_A240ContagemItem_FMCP[0]);
               }
               if ( Z241ContagemItem_FMPontoFuncaoBruto != T00172_A241ContagemItem_FMPontoFuncaoBruto[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_FMPontoFuncaoBruto");
                  GXUtil.WriteLogRaw("Old: ",Z241ContagemItem_FMPontoFuncaoBruto);
                  GXUtil.WriteLogRaw("Current: ",T00172_A241ContagemItem_FMPontoFuncaoBruto[0]);
               }
               if ( Z242ContagemItem_FMPontoFuncaoLiquido != T00172_A242ContagemItem_FMPontoFuncaoLiquido[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_FMPontoFuncaoLiquido");
                  GXUtil.WriteLogRaw("Old: ",Z242ContagemItem_FMPontoFuncaoLiquido);
                  GXUtil.WriteLogRaw("Current: ",T00172_A242ContagemItem_FMPontoFuncaoLiquido[0]);
               }
               if ( Z165FuncaoAPF_Codigo != T00172_A165FuncaoAPF_Codigo[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"FuncaoAPF_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z165FuncaoAPF_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00172_A165FuncaoAPF_Codigo[0]);
               }
               if ( Z226ContagemItem_FSReferenciaTecnicaCod != T00172_A226ContagemItem_FSReferenciaTecnicaCod[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_FSReferenciaTecnicaCod");
                  GXUtil.WriteLogRaw("Old: ",Z226ContagemItem_FSReferenciaTecnicaCod);
                  GXUtil.WriteLogRaw("Current: ",T00172_A226ContagemItem_FSReferenciaTecnicaCod[0]);
               }
               if ( Z255ContagemItem_FMReferenciaTecnicaCod != T00172_A255ContagemItem_FMReferenciaTecnicaCod[0] )
               {
                  GXUtil.WriteLog("contagemitem:[seudo value changed for attri]"+"ContagemItem_FMReferenciaTecnicaCod");
                  GXUtil.WriteLogRaw("Old: ",Z255ContagemItem_FMReferenciaTecnicaCod);
                  GXUtil.WriteLogRaw("Current: ",T00172_A255ContagemItem_FMReferenciaTecnicaCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemItem"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1744( )
      {
         BeforeValidate1744( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1744( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1744( 0) ;
            CheckOptimisticConcurrency1744( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1744( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1744( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001726 */
                     pr_default.execute(19, new Object[] {A192Contagem_Codigo, n268ContagemItem_FMValidacao, A268ContagemItem_FMValidacao, A207Contagem_FabricaSoftwareCod, A208Contagem_FabricaSoftwarePessoaCod, A209Contagem_FabricaSoftwarePessoaNom, A216Contagem_UsuarioAuditorCod, A217Contagem_UsuarioAuditorPessoaCod, A218Contagem_UsuarioAuditorPessoaNom, n225ContagemItem_Sequencial, A225ContagemItem_Sequencial, n229ContagemItem_Requisito, A229ContagemItem_Requisito, n950ContagemItem_PFB, A950ContagemItem_PFB, n951ContagemItem_PFL, A951ContagemItem_PFL, A952ContagemItem_TipoUnidade, n953ContagemItem_Evidencias, A953ContagemItem_Evidencias, n954ContagemItem_QtdINM, A954ContagemItem_QtdINM, n955ContagemItem_CP, A955ContagemItem_CP, n956ContagemItem_RA, A956ContagemItem_RA, n957ContagemItem_DER, A957ContagemItem_DER, n958ContagemItem_Funcao, A958ContagemItem_Funcao, n228ContagemItem_FSTipoUnidade, A228ContagemItem_FSTipoUnidade, n267ContagemItem_FSValidacao, A267ContagemItem_FSValidacao, n234ContagemItem_FSQTDInm, A234ContagemItem_FSQTDInm, n235ContagemItem_FSCP, A235ContagemItem_FSCP, n236ContagemItem_FSPontoFuncaoBruto, A236ContagemItem_FSPontoFuncaoBruto, n237ContagemItem_FSPontoFuncaoLiquido, A237ContagemItem_FSPontoFuncaoLiquido, n238ContagemItem_FSEvidencias, A238ContagemItem_FSEvidencias, n239ContagemItem_FMQTDInm, A239ContagemItem_FMQTDInm, n240ContagemItem_FMCP, A240ContagemItem_FMCP, n241ContagemItem_FMPontoFuncaoBruto, A241ContagemItem_FMPontoFuncaoBruto, n242ContagemItem_FMPontoFuncaoLiquido, A242ContagemItem_FMPontoFuncaoLiquido, n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo, n226ContagemItem_FSReferenciaTecnicaCod, A226ContagemItem_FSReferenciaTecnicaCod, n255ContagemItem_FMReferenciaTecnicaCod, A255ContagemItem_FMReferenciaTecnicaCod});
                     A224ContagemItem_Lancamento = T001726_A224ContagemItem_Lancamento[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
                     pr_default.close(19);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemItem") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption170( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1744( ) ;
            }
            EndLevel1744( ) ;
         }
         CloseExtendedTableCursors1744( ) ;
      }

      protected void Update1744( )
      {
         BeforeValidate1744( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1744( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1744( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1744( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1744( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001727 */
                     pr_default.execute(20, new Object[] {A192Contagem_Codigo, n268ContagemItem_FMValidacao, A268ContagemItem_FMValidacao, A207Contagem_FabricaSoftwareCod, A208Contagem_FabricaSoftwarePessoaCod, A209Contagem_FabricaSoftwarePessoaNom, A216Contagem_UsuarioAuditorCod, A217Contagem_UsuarioAuditorPessoaCod, A218Contagem_UsuarioAuditorPessoaNom, n225ContagemItem_Sequencial, A225ContagemItem_Sequencial, n229ContagemItem_Requisito, A229ContagemItem_Requisito, n950ContagemItem_PFB, A950ContagemItem_PFB, n951ContagemItem_PFL, A951ContagemItem_PFL, A952ContagemItem_TipoUnidade, n953ContagemItem_Evidencias, A953ContagemItem_Evidencias, n954ContagemItem_QtdINM, A954ContagemItem_QtdINM, n955ContagemItem_CP, A955ContagemItem_CP, n956ContagemItem_RA, A956ContagemItem_RA, n957ContagemItem_DER, A957ContagemItem_DER, n958ContagemItem_Funcao, A958ContagemItem_Funcao, n228ContagemItem_FSTipoUnidade, A228ContagemItem_FSTipoUnidade, n267ContagemItem_FSValidacao, A267ContagemItem_FSValidacao, n234ContagemItem_FSQTDInm, A234ContagemItem_FSQTDInm, n235ContagemItem_FSCP, A235ContagemItem_FSCP, n236ContagemItem_FSPontoFuncaoBruto, A236ContagemItem_FSPontoFuncaoBruto, n237ContagemItem_FSPontoFuncaoLiquido, A237ContagemItem_FSPontoFuncaoLiquido, n238ContagemItem_FSEvidencias, A238ContagemItem_FSEvidencias, n239ContagemItem_FMQTDInm, A239ContagemItem_FMQTDInm, n240ContagemItem_FMCP, A240ContagemItem_FMCP, n241ContagemItem_FMPontoFuncaoBruto, A241ContagemItem_FMPontoFuncaoBruto, n242ContagemItem_FMPontoFuncaoLiquido, A242ContagemItem_FMPontoFuncaoLiquido, n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo, n226ContagemItem_FSReferenciaTecnicaCod, A226ContagemItem_FSReferenciaTecnicaCod, n255ContagemItem_FMReferenciaTecnicaCod, A255ContagemItem_FMReferenciaTecnicaCod, A224ContagemItem_Lancamento});
                     pr_default.close(20);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemItem") ;
                     if ( (pr_default.getStatus(20) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemItem"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1744( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1744( ) ;
         }
         CloseExtendedTableCursors1744( ) ;
      }

      protected void DeferredUpdate1744( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1744( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1744( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1744( ) ;
            AfterConfirm1744( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1744( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001728 */
                  pr_default.execute(21, new Object[] {A224ContagemItem_Lancamento});
                  pr_default.close(21);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemItem") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode44 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1744( ) ;
         Gx_mode = sMode44;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1744( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001730 */
            pr_default.execute(22, new Object[] {A224ContagemItem_Lancamento});
            if ( (pr_default.getStatus(22) != 101) )
            {
               A251ContagemItem_FMDER = T001730_A251ContagemItem_FMDER[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
               n251ContagemItem_FMDER = T001730_n251ContagemItem_FMDER[0];
            }
            else
            {
               A251ContagemItem_FMDER = 0;
               n251ContagemItem_FMDER = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
            }
            pr_default.close(22);
            GXt_int1 = A252ContagemItem_FMAR;
            new prc_calculaaruniquefm(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            A252ContagemItem_FMAR = (short)(GXt_int1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A252ContagemItem_FMAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A252ContagemItem_FMAR), 3, 0)));
            GXt_int1 = A233ContagemItem_FSAR;
            new prc_calculaaruniquefs(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            A233ContagemItem_FSAR = (short)(GXt_int1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A233ContagemItem_FSAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A233ContagemItem_FSAR), 3, 0)));
            /* Using cursor T001731 */
            pr_default.execute(23, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
            A166FuncaoAPF_Nome = T001731_A166FuncaoAPF_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            A184FuncaoAPF_Tipo = T001731_A184FuncaoAPF_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
            pr_default.close(23);
            GXt_char2 = A185FuncaoAPF_Complexidade;
            new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A185FuncaoAPF_Complexidade = GXt_char2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
            /* Using cursor T001732 */
            pr_default.execute(24, new Object[] {n226ContagemItem_FSReferenciaTecnicaCod, A226ContagemItem_FSReferenciaTecnicaCod});
            A269ContagemItem_FSReferenciaTecnicaNom = T001732_A269ContagemItem_FSReferenciaTecnicaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A269ContagemItem_FSReferenciaTecnicaNom", A269ContagemItem_FSReferenciaTecnicaNom);
            n269ContagemItem_FSReferenciaTecnicaNom = T001732_n269ContagemItem_FSReferenciaTecnicaNom[0];
            A227ContagemItem_FSReferenciaTecnicaVal = T001732_A227ContagemItem_FSReferenciaTecnicaVal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A227ContagemItem_FSReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5)));
            n227ContagemItem_FSReferenciaTecnicaVal = T001732_n227ContagemItem_FSReferenciaTecnicaVal[0];
            pr_default.close(24);
            if ( ( ( A267ContagemItem_FSValidacao == 1 ) ) && ( ( A268ContagemItem_FMValidacao == 1 ) ) )
            {
               A260ContagemItem_ValidacaoStatusFinal = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
            }
            else
            {
               A260ContagemItem_ValidacaoStatusFinal = 2;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
            }
            /* Using cursor T001733 */
            pr_default.execute(25, new Object[] {n255ContagemItem_FMReferenciaTecnicaCod, A255ContagemItem_FMReferenciaTecnicaCod});
            A257ContagemItem_FMReferenciaTecnicaNom = T001733_A257ContagemItem_FMReferenciaTecnicaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A257ContagemItem_FMReferenciaTecnicaNom", A257ContagemItem_FMReferenciaTecnicaNom);
            n257ContagemItem_FMReferenciaTecnicaNom = T001733_n257ContagemItem_FMReferenciaTecnicaNom[0];
            A256ContagemItem_FMReferenciaTecnicaVal = T001733_A256ContagemItem_FMReferenciaTecnicaVal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A256ContagemItem_FMReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5)));
            n256ContagemItem_FMReferenciaTecnicaVal = T001733_n256ContagemItem_FMReferenciaTecnicaVal[0];
            pr_default.close(25);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T001734 */
            pr_default.execute(26, new Object[] {A224ContagemItem_Lancamento});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Item Atributos FMetrica"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
            /* Using cursor T001735 */
            pr_default.execute(27, new Object[] {A224ContagemItem_Lancamento});
            if ( (pr_default.getStatus(27) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Item Parecer"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(27);
            /* Using cursor T001736 */
            pr_default.execute(28, new Object[] {A224ContagemItem_Lancamento});
            if ( (pr_default.getStatus(28) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Item Atributos FSoftware"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(28);
         }
      }

      protected void EndLevel1744( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1744( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(23);
            pr_default.close(24);
            pr_default.close(25);
            pr_default.close(22);
            context.CommitDataStores( "ContagemItem");
            if ( AnyError == 0 )
            {
               ConfirmValues170( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(23);
            pr_default.close(24);
            pr_default.close(25);
            pr_default.close(22);
            context.RollbackDataStores( "ContagemItem");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1744( )
      {
         /* Scan By routine */
         /* Using cursor T001737 */
         pr_default.execute(29, new Object[] {A192Contagem_Codigo});
         RcdFound44 = 0;
         if ( (pr_default.getStatus(29) != 101) )
         {
            RcdFound44 = 1;
            A224ContagemItem_Lancamento = T001737_A224ContagemItem_Lancamento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1744( )
      {
         /* Scan next routine */
         pr_default.readNext(29);
         RcdFound44 = 0;
         if ( (pr_default.getStatus(29) != 101) )
         {
            RcdFound44 = 1;
            A224ContagemItem_Lancamento = T001737_A224ContagemItem_Lancamento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         }
      }

      protected void ScanEnd1744( )
      {
         pr_default.close(29);
      }

      protected void AfterConfirm1744( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1744( )
      {
         /* Before Insert Rules */
         A268ContagemItem_FMValidacao = 2;
         n268ContagemItem_FMValidacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A268ContagemItem_FMValidacao", StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0));
      }

      protected void BeforeUpdate1744( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1744( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1744( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1744( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1744( )
      {
         edtContagem_DataCriacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_DataCriacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_DataCriacao_Enabled), 5, 0)));
         edtContagem_AreaTrabalhoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_AreaTrabalhoCod_Enabled), 5, 0)));
         edtContagem_AreaTrabalhoDes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_AreaTrabalhoDes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_AreaTrabalhoDes_Enabled), 5, 0)));
         edtContagemItem_PFB_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_PFB_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_PFB_Enabled), 5, 0)));
         edtContagemItem_PFL_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_PFL_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_PFL_Enabled), 5, 0)));
         cmbContagemItem_TipoUnidade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_TipoUnidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemItem_TipoUnidade.Enabled), 5, 0)));
         edtContagemItem_Evidencias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Evidencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Evidencias_Enabled), 5, 0)));
         edtContagemItem_QtdINM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_QtdINM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_QtdINM_Enabled), 5, 0)));
         edtContagemItem_CP_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_CP_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_CP_Enabled), 5, 0)));
         edtContagemItem_RA_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_RA_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_RA_Enabled), 5, 0)));
         edtContagemItem_DER_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_DER_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_DER_Enabled), 5, 0)));
         edtContagemItem_Funcao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Funcao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Funcao_Enabled), 5, 0)));
         edtContagemItem_Lancamento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Lancamento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Lancamento_Enabled), 5, 0)));
         edtContagem_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Codigo_Enabled), 5, 0)));
         cmbContagem_Tecnica.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tecnica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagem_Tecnica.Enabled), 5, 0)));
         cmbContagem_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagem_Tipo.Enabled), 5, 0)));
         edtContagemItem_Sequencial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Sequencial_Enabled), 5, 0)));
         cmbContagemItem_ValidacaoStatusFinal.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_ValidacaoStatusFinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemItem_ValidacaoStatusFinal.Enabled), 5, 0)));
         edtFuncaoAPF_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Enabled), 5, 0)));
         edtFuncaoAPF_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Nome_Enabled), 5, 0)));
         cmbFuncaoAPF_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoAPF_Tipo.Enabled), 5, 0)));
         cmbFuncaoAPF_Complexidade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Complexidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoAPF_Complexidade.Enabled), 5, 0)));
         edtContagemItem_Requisito_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Requisito_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Requisito_Enabled), 5, 0)));
         edtContagem_FabricaSoftwarePessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_FabricaSoftwarePessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_FabricaSoftwarePessoaNom_Enabled), 5, 0)));
         edtContagem_UsuarioContadorPessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorPessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioContadorPessoaNom_Enabled), 5, 0)));
         cmbContagemItem_FSValidacao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_FSValidacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemItem_FSValidacao.Enabled), 5, 0)));
         cmbContagemItem_FSTipoUnidade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_FSTipoUnidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemItem_FSTipoUnidade.Enabled), 5, 0)));
         edtContagemItem_FSReferenciaTecnicaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSReferenciaTecnicaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSReferenciaTecnicaCod_Enabled), 5, 0)));
         edtContagemItem_FSReferenciaTecnicaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSReferenciaTecnicaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSReferenciaTecnicaNom_Enabled), 5, 0)));
         edtContagemItem_FSReferenciaTecnicaVal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSReferenciaTecnicaVal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSReferenciaTecnicaVal_Enabled), 5, 0)));
         edtContagemItem_FSDER_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSDER_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSDER_Enabled), 5, 0)));
         edtContagemItem_FSAR_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSAR_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSAR_Enabled), 5, 0)));
         edtContagemItem_FSQTDInm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSQTDInm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSQTDInm_Enabled), 5, 0)));
         edtContagemItem_FSCP_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSCP_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSCP_Enabled), 5, 0)));
         edtContagemItem_FSPontoFuncaoBruto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSPontoFuncaoBruto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSPontoFuncaoBruto_Enabled), 5, 0)));
         edtContagemItem_FSPontoFuncaoLiquido_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSPontoFuncaoLiquido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSPontoFuncaoLiquido_Enabled), 5, 0)));
         edtContagemItem_FSEvidencias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FSEvidencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSEvidencias_Enabled), 5, 0)));
         edtContagem_UsuarioAuditorPessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioAuditorPessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioAuditorPessoaNom_Enabled), 5, 0)));
         cmbContagemItem_FMValidacao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_FMValidacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemItem_FMValidacao.Enabled), 5, 0)));
         edtContagemItem_FMReferenciaTecnicaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMReferenciaTecnicaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMReferenciaTecnicaCod_Enabled), 5, 0)));
         edtContagemItem_FMReferenciaTecnicaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMReferenciaTecnicaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMReferenciaTecnicaNom_Enabled), 5, 0)));
         edtContagemItem_FMReferenciaTecnicaVal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMReferenciaTecnicaVal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMReferenciaTecnicaVal_Enabled), 5, 0)));
         edtContagemItem_FMDER_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMDER_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMDER_Enabled), 5, 0)));
         edtContagemItem_FMAR_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMAR_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMAR_Enabled), 5, 0)));
         edtContagemItem_FMQTDInm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMQTDInm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMQTDInm_Enabled), 5, 0)));
         edtContagemItem_FMCP_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMCP_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMCP_Enabled), 5, 0)));
         edtContagemItem_FMPontoFuncaoBruto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMPontoFuncaoBruto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMPontoFuncaoBruto_Enabled), 5, 0)));
         edtContagemItem_FMPontoFuncaoLiquido_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMPontoFuncaoLiquido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMPontoFuncaoLiquido_Enabled), 5, 0)));
         edtContagem_UsuarioAuditorCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioAuditorCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioAuditorCod_Enabled), 5, 0)));
         edtContagem_UsuarioAuditorPessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioAuditorPessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioAuditorPessoaCod_Enabled), 5, 0)));
         edtContagem_FabricaSoftwareCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_FabricaSoftwareCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_FabricaSoftwareCod_Enabled), 5, 0)));
         edtContagem_FabricaSoftwarePessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_FabricaSoftwarePessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_FabricaSoftwarePessoaCod_Enabled), 5, 0)));
         edtContagem_UsuarioContadorCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioContadorCod_Enabled), 5, 0)));
         edtContagem_UsuarioContadorPessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorPessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioContadorPessoaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues170( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205211892252");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemitem.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContagemItem_Lancamento) + "," + UrlEncode("" +A192Contagem_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z224ContagemItem_Lancamento), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z268ContagemItem_FMValidacao", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z268ContagemItem_FMValidacao), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z207Contagem_FabricaSoftwareCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z207Contagem_FabricaSoftwareCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z208Contagem_FabricaSoftwarePessoaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z208Contagem_FabricaSoftwarePessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z209Contagem_FabricaSoftwarePessoaNom", StringUtil.RTrim( Z209Contagem_FabricaSoftwarePessoaNom));
         GxWebStd.gx_hidden_field( context, "Z216Contagem_UsuarioAuditorCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z216Contagem_UsuarioAuditorCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z217Contagem_UsuarioAuditorPessoaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z217Contagem_UsuarioAuditorPessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z218Contagem_UsuarioAuditorPessoaNom", StringUtil.RTrim( Z218Contagem_UsuarioAuditorPessoaNom));
         GxWebStd.gx_hidden_field( context, "Z225ContagemItem_Sequencial", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z225ContagemItem_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z229ContagemItem_Requisito", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z229ContagemItem_Requisito), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z950ContagemItem_PFB", StringUtil.LTrim( StringUtil.NToC( Z950ContagemItem_PFB, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z951ContagemItem_PFL", StringUtil.LTrim( StringUtil.NToC( Z951ContagemItem_PFL, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z952ContagemItem_TipoUnidade", Z952ContagemItem_TipoUnidade);
         GxWebStd.gx_hidden_field( context, "Z954ContagemItem_QtdINM", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z954ContagemItem_QtdINM), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z955ContagemItem_CP", StringUtil.RTrim( Z955ContagemItem_CP));
         GxWebStd.gx_hidden_field( context, "Z956ContagemItem_RA", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z956ContagemItem_RA), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z957ContagemItem_DER", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z957ContagemItem_DER), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z958ContagemItem_Funcao", Z958ContagemItem_Funcao);
         GxWebStd.gx_hidden_field( context, "Z228ContagemItem_FSTipoUnidade", Z228ContagemItem_FSTipoUnidade);
         GxWebStd.gx_hidden_field( context, "Z267ContagemItem_FSValidacao", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z267ContagemItem_FSValidacao), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z234ContagemItem_FSQTDInm", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z234ContagemItem_FSQTDInm), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z235ContagemItem_FSCP", StringUtil.RTrim( Z235ContagemItem_FSCP));
         GxWebStd.gx_hidden_field( context, "Z236ContagemItem_FSPontoFuncaoBruto", StringUtil.LTrim( StringUtil.NToC( Z236ContagemItem_FSPontoFuncaoBruto, 7, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z237ContagemItem_FSPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.NToC( Z237ContagemItem_FSPontoFuncaoLiquido, 7, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z239ContagemItem_FMQTDInm", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z239ContagemItem_FMQTDInm), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z240ContagemItem_FMCP", StringUtil.RTrim( Z240ContagemItem_FMCP));
         GxWebStd.gx_hidden_field( context, "Z241ContagemItem_FMPontoFuncaoBruto", StringUtil.LTrim( StringUtil.NToC( Z241ContagemItem_FMPontoFuncaoBruto, 7, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z242ContagemItem_FMPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.NToC( Z242ContagemItem_FMPontoFuncaoLiquido, 7, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z226ContagemItem_FSReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z226ContagemItem_FSReferenciaTecnicaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z255ContagemItem_FMReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z255ContagemItem_FMReferenciaTecnicaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N226ContagemItem_FSReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N255ContagemItem_FMReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMITEM_LANCAMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemItem_Lancamento), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contagem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEMITEM_FSREFERENCIATECNICACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_ContagemItem_FSReferenciaTecnicaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEMITEM_FMREFERENCIATECNICACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Insert_ContagemItem_FMReferenciaTecnicaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV16Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_SEQUENCIAL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMITEM_LANCAMENTO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemItem_Lancamento), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContagemItem";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemitem:[SendSecurityCheck value for]"+"ContagemItem_Lancamento:"+context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9"));
         GXUtil.WriteLog("contagemitem:[SendSecurityCheck value for]"+"ContagemItem_Sequencial:"+context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9"));
         GXUtil.WriteLog("contagemitem:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemitem.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContagemItem_Lancamento) + "," + UrlEncode("" +A192Contagem_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContagemItem" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Item" ;
      }

      protected void InitializeNonKey1744( )
      {
         A165FuncaoAPF_Codigo = 0;
         n165FuncaoAPF_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         n165FuncaoAPF_Codigo = ((0==A165FuncaoAPF_Codigo) ? true : false);
         A226ContagemItem_FSReferenciaTecnicaCod = 0;
         n226ContagemItem_FSReferenciaTecnicaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A226ContagemItem_FSReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0)));
         n226ContagemItem_FSReferenciaTecnicaCod = ((0==A226ContagemItem_FSReferenciaTecnicaCod) ? true : false);
         A255ContagemItem_FMReferenciaTecnicaCod = 0;
         n255ContagemItem_FMReferenciaTecnicaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A255ContagemItem_FMReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), 6, 0)));
         n255ContagemItem_FMReferenciaTecnicaCod = ((0==A255ContagemItem_FMReferenciaTecnicaCod) ? true : false);
         A268ContagemItem_FMValidacao = 0;
         n268ContagemItem_FMValidacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A268ContagemItem_FMValidacao", StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0));
         n268ContagemItem_FMValidacao = ((0==A268ContagemItem_FMValidacao) ? true : false);
         A260ContagemItem_ValidacaoStatusFinal = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
         A185FuncaoAPF_Complexidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         A233ContagemItem_FSAR = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A233ContagemItem_FSAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A233ContagemItem_FSAR), 3, 0)));
         A252ContagemItem_FMAR = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A252ContagemItem_FMAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A252ContagemItem_FMAR), 3, 0)));
         A207Contagem_FabricaSoftwareCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A207Contagem_FabricaSoftwareCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0)));
         A208Contagem_FabricaSoftwarePessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A208Contagem_FabricaSoftwarePessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0)));
         A209Contagem_FabricaSoftwarePessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A209Contagem_FabricaSoftwarePessoaNom", A209Contagem_FabricaSoftwarePessoaNom);
         A216Contagem_UsuarioAuditorCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A216Contagem_UsuarioAuditorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0)));
         A217Contagem_UsuarioAuditorPessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A217Contagem_UsuarioAuditorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0)));
         A218Contagem_UsuarioAuditorPessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A218Contagem_UsuarioAuditorPessoaNom", A218Contagem_UsuarioAuditorPessoaNom);
         A225ContagemItem_Sequencial = 0;
         n225ContagemItem_Sequencial = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A225ContagemItem_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A225ContagemItem_Sequencial), 3, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMITEM_SEQUENCIAL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")));
         n225ContagemItem_Sequencial = ((0==A225ContagemItem_Sequencial) ? true : false);
         A229ContagemItem_Requisito = 0;
         n229ContagemItem_Requisito = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A229ContagemItem_Requisito", StringUtil.LTrim( StringUtil.Str( (decimal)(A229ContagemItem_Requisito), 3, 0)));
         n229ContagemItem_Requisito = ((0==A229ContagemItem_Requisito) ? true : false);
         A950ContagemItem_PFB = 0;
         n950ContagemItem_PFB = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A950ContagemItem_PFB", StringUtil.LTrim( StringUtil.Str( A950ContagemItem_PFB, 14, 5)));
         n950ContagemItem_PFB = ((Convert.ToDecimal(0)==A950ContagemItem_PFB) ? true : false);
         A951ContagemItem_PFL = 0;
         n951ContagemItem_PFL = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A951ContagemItem_PFL", StringUtil.LTrim( StringUtil.Str( A951ContagemItem_PFL, 14, 5)));
         n951ContagemItem_PFL = ((Convert.ToDecimal(0)==A951ContagemItem_PFL) ? true : false);
         A166FuncaoAPF_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
         A184FuncaoAPF_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
         A952ContagemItem_TipoUnidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A952ContagemItem_TipoUnidade", A952ContagemItem_TipoUnidade);
         A953ContagemItem_Evidencias = "";
         n953ContagemItem_Evidencias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A953ContagemItem_Evidencias", A953ContagemItem_Evidencias);
         n953ContagemItem_Evidencias = (String.IsNullOrEmpty(StringUtil.RTrim( A953ContagemItem_Evidencias)) ? true : false);
         A954ContagemItem_QtdINM = 0;
         n954ContagemItem_QtdINM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A954ContagemItem_QtdINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A954ContagemItem_QtdINM), 4, 0)));
         n954ContagemItem_QtdINM = ((0==A954ContagemItem_QtdINM) ? true : false);
         A955ContagemItem_CP = "";
         n955ContagemItem_CP = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A955ContagemItem_CP", A955ContagemItem_CP);
         n955ContagemItem_CP = (String.IsNullOrEmpty(StringUtil.RTrim( A955ContagemItem_CP)) ? true : false);
         A956ContagemItem_RA = 0;
         n956ContagemItem_RA = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A956ContagemItem_RA", StringUtil.LTrim( StringUtil.Str( (decimal)(A956ContagemItem_RA), 3, 0)));
         n956ContagemItem_RA = ((0==A956ContagemItem_RA) ? true : false);
         A957ContagemItem_DER = 0;
         n957ContagemItem_DER = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A957ContagemItem_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A957ContagemItem_DER), 3, 0)));
         n957ContagemItem_DER = ((0==A957ContagemItem_DER) ? true : false);
         A958ContagemItem_Funcao = "";
         n958ContagemItem_Funcao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A958ContagemItem_Funcao", A958ContagemItem_Funcao);
         n958ContagemItem_Funcao = (String.IsNullOrEmpty(StringUtil.RTrim( A958ContagemItem_Funcao)) ? true : false);
         A228ContagemItem_FSTipoUnidade = "";
         n228ContagemItem_FSTipoUnidade = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A228ContagemItem_FSTipoUnidade", A228ContagemItem_FSTipoUnidade);
         n228ContagemItem_FSTipoUnidade = (String.IsNullOrEmpty(StringUtil.RTrim( A228ContagemItem_FSTipoUnidade)) ? true : false);
         A267ContagemItem_FSValidacao = 0;
         n267ContagemItem_FSValidacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A267ContagemItem_FSValidacao", StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0));
         n267ContagemItem_FSValidacao = ((0==A267ContagemItem_FSValidacao) ? true : false);
         A269ContagemItem_FSReferenciaTecnicaNom = "";
         n269ContagemItem_FSReferenciaTecnicaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A269ContagemItem_FSReferenciaTecnicaNom", A269ContagemItem_FSReferenciaTecnicaNom);
         A227ContagemItem_FSReferenciaTecnicaVal = 0;
         n227ContagemItem_FSReferenciaTecnicaVal = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A227ContagemItem_FSReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5)));
         A234ContagemItem_FSQTDInm = 0;
         n234ContagemItem_FSQTDInm = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A234ContagemItem_FSQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A234ContagemItem_FSQTDInm), 9, 0)));
         n234ContagemItem_FSQTDInm = ((0==A234ContagemItem_FSQTDInm) ? true : false);
         A235ContagemItem_FSCP = "";
         n235ContagemItem_FSCP = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A235ContagemItem_FSCP", A235ContagemItem_FSCP);
         n235ContagemItem_FSCP = (String.IsNullOrEmpty(StringUtil.RTrim( A235ContagemItem_FSCP)) ? true : false);
         A236ContagemItem_FSPontoFuncaoBruto = 0;
         n236ContagemItem_FSPontoFuncaoBruto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A236ContagemItem_FSPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A236ContagemItem_FSPontoFuncaoBruto, 7, 2)));
         n236ContagemItem_FSPontoFuncaoBruto = ((Convert.ToDecimal(0)==A236ContagemItem_FSPontoFuncaoBruto) ? true : false);
         A237ContagemItem_FSPontoFuncaoLiquido = 0;
         n237ContagemItem_FSPontoFuncaoLiquido = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A237ContagemItem_FSPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A237ContagemItem_FSPontoFuncaoLiquido, 7, 2)));
         n237ContagemItem_FSPontoFuncaoLiquido = ((Convert.ToDecimal(0)==A237ContagemItem_FSPontoFuncaoLiquido) ? true : false);
         A238ContagemItem_FSEvidencias = "";
         n238ContagemItem_FSEvidencias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A238ContagemItem_FSEvidencias", A238ContagemItem_FSEvidencias);
         n238ContagemItem_FSEvidencias = (String.IsNullOrEmpty(StringUtil.RTrim( A238ContagemItem_FSEvidencias)) ? true : false);
         A257ContagemItem_FMReferenciaTecnicaNom = "";
         n257ContagemItem_FMReferenciaTecnicaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A257ContagemItem_FMReferenciaTecnicaNom", A257ContagemItem_FMReferenciaTecnicaNom);
         A256ContagemItem_FMReferenciaTecnicaVal = 0;
         n256ContagemItem_FMReferenciaTecnicaVal = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A256ContagemItem_FMReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5)));
         A251ContagemItem_FMDER = 0;
         n251ContagemItem_FMDER = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
         A239ContagemItem_FMQTDInm = 0;
         n239ContagemItem_FMQTDInm = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A239ContagemItem_FMQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A239ContagemItem_FMQTDInm), 9, 0)));
         n239ContagemItem_FMQTDInm = ((0==A239ContagemItem_FMQTDInm) ? true : false);
         A240ContagemItem_FMCP = "";
         n240ContagemItem_FMCP = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A240ContagemItem_FMCP", A240ContagemItem_FMCP);
         n240ContagemItem_FMCP = (String.IsNullOrEmpty(StringUtil.RTrim( A240ContagemItem_FMCP)) ? true : false);
         A241ContagemItem_FMPontoFuncaoBruto = 0;
         n241ContagemItem_FMPontoFuncaoBruto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A241ContagemItem_FMPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A241ContagemItem_FMPontoFuncaoBruto, 7, 2)));
         n241ContagemItem_FMPontoFuncaoBruto = ((Convert.ToDecimal(0)==A241ContagemItem_FMPontoFuncaoBruto) ? true : false);
         A242ContagemItem_FMPontoFuncaoLiquido = 0;
         n242ContagemItem_FMPontoFuncaoLiquido = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A242ContagemItem_FMPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A242ContagemItem_FMPontoFuncaoLiquido, 7, 2)));
         n242ContagemItem_FMPontoFuncaoLiquido = ((Convert.ToDecimal(0)==A242ContagemItem_FMPontoFuncaoLiquido) ? true : false);
         Z268ContagemItem_FMValidacao = 0;
         Z207Contagem_FabricaSoftwareCod = 0;
         Z208Contagem_FabricaSoftwarePessoaCod = 0;
         Z209Contagem_FabricaSoftwarePessoaNom = "";
         Z216Contagem_UsuarioAuditorCod = 0;
         Z217Contagem_UsuarioAuditorPessoaCod = 0;
         Z218Contagem_UsuarioAuditorPessoaNom = "";
         Z225ContagemItem_Sequencial = 0;
         Z229ContagemItem_Requisito = 0;
         Z950ContagemItem_PFB = 0;
         Z951ContagemItem_PFL = 0;
         Z952ContagemItem_TipoUnidade = "";
         Z954ContagemItem_QtdINM = 0;
         Z955ContagemItem_CP = "";
         Z956ContagemItem_RA = 0;
         Z957ContagemItem_DER = 0;
         Z958ContagemItem_Funcao = "";
         Z228ContagemItem_FSTipoUnidade = "";
         Z267ContagemItem_FSValidacao = 0;
         Z234ContagemItem_FSQTDInm = 0;
         Z235ContagemItem_FSCP = "";
         Z236ContagemItem_FSPontoFuncaoBruto = 0;
         Z237ContagemItem_FSPontoFuncaoLiquido = 0;
         Z239ContagemItem_FMQTDInm = 0;
         Z240ContagemItem_FMCP = "";
         Z241ContagemItem_FMPontoFuncaoBruto = 0;
         Z242ContagemItem_FMPontoFuncaoLiquido = 0;
         Z165FuncaoAPF_Codigo = 0;
         Z226ContagemItem_FSReferenciaTecnicaCod = 0;
         Z255ContagemItem_FMReferenciaTecnicaCod = 0;
      }

      protected void InitAll1744( )
      {
         A224ContagemItem_Lancamento = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         InitializeNonKey1744( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020521189230");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemitem.js", "?2020521189230");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblContagemitemtitle_Internalname = "CONTAGEMITEMTITLE";
         lblTextblockcontagem_datacriacao_Internalname = "TEXTBLOCKCONTAGEM_DATACRIACAO";
         edtContagem_DataCriacao_Internalname = "CONTAGEM_DATACRIACAO";
         lblTextblockcontagem_areatrabalhocod_Internalname = "TEXTBLOCKCONTAGEM_AREATRABALHOCOD";
         edtContagem_AreaTrabalhoCod_Internalname = "CONTAGEM_AREATRABALHOCOD";
         lblTextblockcontagem_areatrabalhodes_Internalname = "TEXTBLOCKCONTAGEM_AREATRABALHODES";
         edtContagem_AreaTrabalhoDes_Internalname = "CONTAGEM_AREATRABALHODES";
         lblTextblockcontagemitem_pfb_Internalname = "TEXTBLOCKCONTAGEMITEM_PFB";
         edtContagemItem_PFB_Internalname = "CONTAGEMITEM_PFB";
         lblTextblockcontagemitem_pfl_Internalname = "TEXTBLOCKCONTAGEMITEM_PFL";
         edtContagemItem_PFL_Internalname = "CONTAGEMITEM_PFL";
         lblTextblockcontagemitem_tipounidade_Internalname = "TEXTBLOCKCONTAGEMITEM_TIPOUNIDADE";
         cmbContagemItem_TipoUnidade_Internalname = "CONTAGEMITEM_TIPOUNIDADE";
         lblTextblockcontagemitem_evidencias_Internalname = "TEXTBLOCKCONTAGEMITEM_EVIDENCIAS";
         edtContagemItem_Evidencias_Internalname = "CONTAGEMITEM_EVIDENCIAS";
         lblTextblockcontagemitem_qtdinm_Internalname = "TEXTBLOCKCONTAGEMITEM_QTDINM";
         edtContagemItem_QtdINM_Internalname = "CONTAGEMITEM_QTDINM";
         lblTextblockcontagemitem_cp_Internalname = "TEXTBLOCKCONTAGEMITEM_CP";
         edtContagemItem_CP_Internalname = "CONTAGEMITEM_CP";
         lblTextblockcontagemitem_ra_Internalname = "TEXTBLOCKCONTAGEMITEM_RA";
         edtContagemItem_RA_Internalname = "CONTAGEMITEM_RA";
         lblTextblockcontagemitem_der_Internalname = "TEXTBLOCKCONTAGEMITEM_DER";
         edtContagemItem_DER_Internalname = "CONTAGEMITEM_DER";
         lblTextblockcontagemitem_funcao_Internalname = "TEXTBLOCKCONTAGEMITEM_FUNCAO";
         edtContagemItem_Funcao_Internalname = "CONTAGEMITEM_FUNCAO";
         lblTextblockcontagemitem_lancamento_Internalname = "TEXTBLOCKCONTAGEMITEM_LANCAMENTO";
         edtContagemItem_Lancamento_Internalname = "CONTAGEMITEM_LANCAMENTO";
         lblTextblockcontagem_codigo_Internalname = "TEXTBLOCKCONTAGEM_CODIGO";
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO";
         lblTextblockcontagem_tecnica_Internalname = "TEXTBLOCKCONTAGEM_TECNICA";
         cmbContagem_Tecnica_Internalname = "CONTAGEM_TECNICA";
         lblTextblockcontagem_tipo_Internalname = "TEXTBLOCKCONTAGEM_TIPO";
         cmbContagem_Tipo_Internalname = "CONTAGEM_TIPO";
         lblTextblockcontagemitem_sequencial_Internalname = "TEXTBLOCKCONTAGEMITEM_SEQUENCIAL";
         edtContagemItem_Sequencial_Internalname = "CONTAGEMITEM_SEQUENCIAL";
         lblTextblockcontagemitem_validacaostatusfinal_Internalname = "TEXTBLOCKCONTAGEMITEM_VALIDACAOSTATUSFINAL";
         cmbContagemItem_ValidacaoStatusFinal_Internalname = "CONTAGEMITEM_VALIDACAOSTATUSFINAL";
         tblTablemergedcontagem_codigo_Internalname = "TABLEMERGEDCONTAGEM_CODIGO";
         lblTextblockfuncaoapf_codigo_Internalname = "TEXTBLOCKFUNCAOAPF_CODIGO";
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO";
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME";
         lblTextblockfuncaoapf_tipo_Internalname = "TEXTBLOCKFUNCAOAPF_TIPO";
         cmbFuncaoAPF_Tipo_Internalname = "FUNCAOAPF_TIPO";
         lblTextblockfuncaoapf_complexidade_Internalname = "TEXTBLOCKFUNCAOAPF_COMPLEXIDADE";
         cmbFuncaoAPF_Complexidade_Internalname = "FUNCAOAPF_COMPLEXIDADE";
         tblTablemergedfuncaoapf_codigo_Internalname = "TABLEMERGEDFUNCAOAPF_CODIGO";
         lblTextblockcontagemitem_requisito_Internalname = "TEXTBLOCKCONTAGEMITEM_REQUISITO";
         edtContagemItem_Requisito_Internalname = "CONTAGEMITEM_REQUISITO";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         bttBtnbtnselecionaratributosfs_Internalname = "BTNBTNSELECIONARATRIBUTOSFS";
         lblTextblockcontagem_fabricasoftwarepessoanom_Internalname = "TEXTBLOCKCONTAGEM_FABRICASOFTWAREPESSOANOM";
         edtContagem_FabricaSoftwarePessoaNom_Internalname = "CONTAGEM_FABRICASOFTWAREPESSOANOM";
         lblTextblockcontagem_usuariocontadorpessoanom_Internalname = "TEXTBLOCKCONTAGEM_USUARIOCONTADORPESSOANOM";
         edtContagem_UsuarioContadorPessoaNom_Internalname = "CONTAGEM_USUARIOCONTADORPESSOANOM";
         lblTextblockcontagemitem_fsvalidacao_Internalname = "TEXTBLOCKCONTAGEMITEM_FSVALIDACAO";
         cmbContagemItem_FSValidacao_Internalname = "CONTAGEMITEM_FSVALIDACAO";
         lblTextblockcontagemitem_fstipounidade_Internalname = "TEXTBLOCKCONTAGEMITEM_FSTIPOUNIDADE";
         cmbContagemItem_FSTipoUnidade_Internalname = "CONTAGEMITEM_FSTIPOUNIDADE";
         tblTablemergedcontagemitem_fsvalidacao_Internalname = "TABLEMERGEDCONTAGEMITEM_FSVALIDACAO";
         lblTextblockcontagemitem_fsreferenciatecnicacod_Internalname = "TEXTBLOCKCONTAGEMITEM_FSREFERENCIATECNICACOD";
         edtContagemItem_FSReferenciaTecnicaCod_Internalname = "CONTAGEMITEM_FSREFERENCIATECNICACOD";
         edtContagemItem_FSReferenciaTecnicaNom_Internalname = "CONTAGEMITEM_FSREFERENCIATECNICANOM";
         tblTablemergedcontagemitem_fsreferenciatecnicacod_Internalname = "TABLEMERGEDCONTAGEMITEM_FSREFERENCIATECNICACOD";
         lblTextblockcontagemitem_fsreferenciatecnicaval_Internalname = "TEXTBLOCKCONTAGEMITEM_FSREFERENCIATECNICAVAL";
         edtContagemItem_FSReferenciaTecnicaVal_Internalname = "CONTAGEMITEM_FSREFERENCIATECNICAVAL";
         lblTextblockcontagemitem_fsder_Internalname = "TEXTBLOCKCONTAGEMITEM_FSDER";
         edtContagemItem_FSDER_Internalname = "CONTAGEMITEM_FSDER";
         lblContagemitem_fsder_righttext_Internalname = "CONTAGEMITEM_FSDER_RIGHTTEXT";
         cellContagemitem_fsder_righttext_cell_Internalname = "CONTAGEMITEM_FSDER_RIGHTTEXT_CELL";
         lblTextblockcontagemitem_fsar_Internalname = "TEXTBLOCKCONTAGEMITEM_FSAR";
         edtContagemItem_FSAR_Internalname = "CONTAGEMITEM_FSAR";
         lblContagemitem_fsar_righttext_Internalname = "CONTAGEMITEM_FSAR_RIGHTTEXT";
         cellContagemitem_fsar_righttext_cell_Internalname = "CONTAGEMITEM_FSAR_RIGHTTEXT_CELL";
         tblTablemergedcontagemitem_fsder_Internalname = "TABLEMERGEDCONTAGEMITEM_FSDER";
         lblTextblockcontagemitem_fsqtdinm_Internalname = "TEXTBLOCKCONTAGEMITEM_FSQTDINM";
         edtContagemItem_FSQTDInm_Internalname = "CONTAGEMITEM_FSQTDINM";
         lblTextblockcontagemitem_fscp_Internalname = "TEXTBLOCKCONTAGEMITEM_FSCP";
         edtContagemItem_FSCP_Internalname = "CONTAGEMITEM_FSCP";
         tblTablemergedcontagemitem_fsqtdinm_Internalname = "TABLEMERGEDCONTAGEMITEM_FSQTDINM";
         lblTextblockcontagemitem_fspontofuncaobruto_Internalname = "TEXTBLOCKCONTAGEMITEM_FSPONTOFUNCAOBRUTO";
         edtContagemItem_FSPontoFuncaoBruto_Internalname = "CONTAGEMITEM_FSPONTOFUNCAOBRUTO";
         lblTextblockcontagemitem_fspontofuncaoliquido_Internalname = "TEXTBLOCKCONTAGEMITEM_FSPONTOFUNCAOLIQUIDO";
         edtContagemItem_FSPontoFuncaoLiquido_Internalname = "CONTAGEMITEM_FSPONTOFUNCAOLIQUIDO";
         tblTablemergedcontagemitem_fspontofuncaobruto_Internalname = "TABLEMERGEDCONTAGEMITEM_FSPONTOFUNCAOBRUTO";
         lblTextblockcontagemitem_fsevidencias_Internalname = "TEXTBLOCKCONTAGEMITEM_FSEVIDENCIAS";
         edtContagemItem_FSEvidencias_Internalname = "CONTAGEMITEM_FSEVIDENCIAS";
         tblUnnamedtable9_Internalname = "UNNAMEDTABLE9";
         grpUnnamedgroup10_Internalname = "UNNAMEDGROUP10";
         tblUnnamedtable5_Internalname = "UNNAMEDTABLE5";
         tblUnnamedtable6_Internalname = "UNNAMEDTABLE6";
         bttBtnbtnassociaratributosfm_Internalname = "BTNBTNASSOCIARATRIBUTOSFM";
         lblTextblockcontagem_usuarioauditorpessoanom_Internalname = "TEXTBLOCKCONTAGEM_USUARIOAUDITORPESSOANOM";
         edtContagem_UsuarioAuditorPessoaNom_Internalname = "CONTAGEM_USUARIOAUDITORPESSOANOM";
         lblTextblockcontagemitem_fmvalidacao_Internalname = "TEXTBLOCKCONTAGEMITEM_FMVALIDACAO";
         cmbContagemItem_FMValidacao_Internalname = "CONTAGEMITEM_FMVALIDACAO";
         lblTextblockcontagemitem_fmreferenciatecnicacod_Internalname = "TEXTBLOCKCONTAGEMITEM_FMREFERENCIATECNICACOD";
         edtContagemItem_FMReferenciaTecnicaCod_Internalname = "CONTAGEMITEM_FMREFERENCIATECNICACOD";
         edtContagemItem_FMReferenciaTecnicaNom_Internalname = "CONTAGEMITEM_FMREFERENCIATECNICANOM";
         tblTablemergedcontagemitem_fmreferenciatecnicacod_Internalname = "TABLEMERGEDCONTAGEMITEM_FMREFERENCIATECNICACOD";
         lblTextblockcontagemitem_fmreferenciatecnicaval_Internalname = "TEXTBLOCKCONTAGEMITEM_FMREFERENCIATECNICAVAL";
         edtContagemItem_FMReferenciaTecnicaVal_Internalname = "CONTAGEMITEM_FMREFERENCIATECNICAVAL";
         lblTextblockcontagemitem_fmder_Internalname = "TEXTBLOCKCONTAGEMITEM_FMDER";
         edtContagemItem_FMDER_Internalname = "CONTAGEMITEM_FMDER";
         lblContagemitem_fmder_righttext_Internalname = "CONTAGEMITEM_FMDER_RIGHTTEXT";
         cellContagemitem_fmder_righttext_cell_Internalname = "CONTAGEMITEM_FMDER_RIGHTTEXT_CELL";
         lblTextblockcontagemitem_fmar_Internalname = "TEXTBLOCKCONTAGEMITEM_FMAR";
         edtContagemItem_FMAR_Internalname = "CONTAGEMITEM_FMAR";
         lblContagemitem_fmar_righttext_Internalname = "CONTAGEMITEM_FMAR_RIGHTTEXT";
         cellContagemitem_fmar_righttext_cell_Internalname = "CONTAGEMITEM_FMAR_RIGHTTEXT_CELL";
         tblTablemergedcontagemitem_fmder_Internalname = "TABLEMERGEDCONTAGEMITEM_FMDER";
         lblTextblockcontagemitem_fmqtdinm_Internalname = "TEXTBLOCKCONTAGEMITEM_FMQTDINM";
         edtContagemItem_FMQTDInm_Internalname = "CONTAGEMITEM_FMQTDINM";
         lblTextblockcontagemitem_fmcp_Internalname = "TEXTBLOCKCONTAGEMITEM_FMCP";
         edtContagemItem_FMCP_Internalname = "CONTAGEMITEM_FMCP";
         tblTablemergedcontagemitem_fmqtdinm_Internalname = "TABLEMERGEDCONTAGEMITEM_FMQTDINM";
         lblTextblockcontagemitem_fmpontofuncaobruto_Internalname = "TEXTBLOCKCONTAGEMITEM_FMPONTOFUNCAOBRUTO";
         edtContagemItem_FMPontoFuncaoBruto_Internalname = "CONTAGEMITEM_FMPONTOFUNCAOBRUTO";
         lblTextblockcontagemitem_fmpontofuncaoliquido_Internalname = "TEXTBLOCKCONTAGEMITEM_FMPONTOFUNCAOLIQUIDO";
         edtContagemItem_FMPontoFuncaoLiquido_Internalname = "CONTAGEMITEM_FMPONTOFUNCAOLIQUIDO";
         tblTablemergedcontagemitem_fmpontofuncaobruto_Internalname = "TABLEMERGEDCONTAGEMITEM_FMPONTOFUNCAOBRUTO";
         tblUnnamedtable7_Internalname = "UNNAMEDTABLE7";
         grpUnnamedgroup8_Internalname = "UNNAMEDGROUP8";
         tblTablefm_Internalname = "TABLEFM";
         tblUnnamedtable4_Internalname = "UNNAMEDTABLE4";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContagem_UsuarioAuditorCod_Internalname = "CONTAGEM_USUARIOAUDITORCOD";
         edtContagem_UsuarioAuditorPessoaCod_Internalname = "CONTAGEM_USUARIOAUDITORPESSOACOD";
         edtContagem_FabricaSoftwareCod_Internalname = "CONTAGEM_FABRICASOFTWARECOD";
         edtContagem_FabricaSoftwarePessoaCod_Internalname = "CONTAGEM_FABRICASOFTWAREPESSOACOD";
         edtContagem_UsuarioContadorCod_Internalname = "CONTAGEM_USUARIOCONTADORCOD";
         edtContagem_UsuarioContadorPessoaCod_Internalname = "CONTAGEM_USUARIOCONTADORPESSOACOD";
         Form.Internalname = "FORM";
         imgprompt_165_Internalname = "PROMPT_165";
         imgprompt_226_Internalname = "PROMPT_226";
         imgprompt_255_Internalname = "PROMPT_255";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Manuten��o - Item da Contagem";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Item";
         cmbContagemItem_ValidacaoStatusFinal_Jsonclick = "";
         cmbContagemItem_ValidacaoStatusFinal.Enabled = 0;
         edtContagemItem_Sequencial_Jsonclick = "";
         edtContagemItem_Sequencial_Enabled = 0;
         cmbContagem_Tipo_Jsonclick = "";
         cmbContagem_Tipo.Enabled = 0;
         cmbContagem_Tecnica_Jsonclick = "";
         cmbContagem_Tecnica.Enabled = 0;
         edtContagem_Codigo_Jsonclick = "";
         edtContagem_Codigo_Enabled = 0;
         cmbFuncaoAPF_Complexidade_Jsonclick = "";
         cmbFuncaoAPF_Complexidade.Enabled = 0;
         cmbFuncaoAPF_Tipo_Jsonclick = "";
         cmbFuncaoAPF_Tipo.Enabled = 0;
         edtFuncaoAPF_Nome_Enabled = 0;
         imgprompt_165_Visible = 1;
         imgprompt_165_Link = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         edtFuncaoAPF_Codigo_Enabled = 1;
         edtContagemItem_Requisito_Jsonclick = "";
         edtContagemItem_Requisito_Enabled = 1;
         edtContagemItem_Lancamento_Jsonclick = "";
         edtContagemItem_Lancamento_Enabled = 0;
         cmbContagemItem_FSTipoUnidade_Jsonclick = "";
         cmbContagemItem_FSTipoUnidade.Enabled = 1;
         cmbContagemItem_FSValidacao_Jsonclick = "";
         cmbContagemItem_FSValidacao.Enabled = 1;
         edtContagemItem_FSReferenciaTecnicaNom_Jsonclick = "";
         edtContagemItem_FSReferenciaTecnicaNom_Enabled = 0;
         imgprompt_226_Visible = 1;
         imgprompt_226_Link = "";
         edtContagemItem_FSReferenciaTecnicaCod_Jsonclick = "";
         edtContagemItem_FSReferenciaTecnicaCod_Enabled = 1;
         edtContagemItem_FSAR_Jsonclick = "";
         edtContagemItem_FSAR_Enabled = 0;
         edtContagemItem_FSDER_Jsonclick = "";
         edtContagemItem_FSDER_Enabled = 0;
         edtContagemItem_FSCP_Jsonclick = "";
         edtContagemItem_FSCP_Enabled = 1;
         edtContagemItem_FSQTDInm_Jsonclick = "";
         edtContagemItem_FSQTDInm_Enabled = 1;
         edtContagemItem_FSPontoFuncaoLiquido_Jsonclick = "";
         edtContagemItem_FSPontoFuncaoLiquido_Enabled = 1;
         edtContagemItem_FSPontoFuncaoBruto_Jsonclick = "";
         edtContagemItem_FSPontoFuncaoBruto_Enabled = 1;
         edtContagemItem_FSEvidencias_Enabled = 1;
         edtContagemItem_FSReferenciaTecnicaVal_Jsonclick = "";
         edtContagemItem_FSReferenciaTecnicaVal_Enabled = 0;
         edtContagem_UsuarioContadorPessoaNom_Jsonclick = "";
         edtContagem_UsuarioContadorPessoaNom_Enabled = 0;
         edtContagem_FabricaSoftwarePessoaNom_Jsonclick = "";
         edtContagem_FabricaSoftwarePessoaNom_Enabled = 1;
         bttBtnbtnselecionaratributosfs_Enabled = 1;
         bttBtnbtnselecionaratributosfs_Visible = 1;
         edtContagemItem_FMReferenciaTecnicaNom_Jsonclick = "";
         edtContagemItem_FMReferenciaTecnicaNom_Enabled = 0;
         imgprompt_255_Visible = 1;
         imgprompt_255_Link = "";
         edtContagemItem_FMReferenciaTecnicaCod_Jsonclick = "";
         edtContagemItem_FMReferenciaTecnicaCod_Enabled = 1;
         edtContagemItem_FMAR_Jsonclick = "";
         edtContagemItem_FMAR_Enabled = 0;
         edtContagemItem_FMDER_Jsonclick = "";
         edtContagemItem_FMDER_Enabled = 0;
         edtContagemItem_FMCP_Jsonclick = "";
         edtContagemItem_FMCP_Enabled = 1;
         edtContagemItem_FMQTDInm_Jsonclick = "";
         edtContagemItem_FMQTDInm_Enabled = 1;
         edtContagemItem_FMPontoFuncaoLiquido_Jsonclick = "";
         edtContagemItem_FMPontoFuncaoLiquido_Enabled = 1;
         edtContagemItem_FMPontoFuncaoBruto_Jsonclick = "";
         edtContagemItem_FMPontoFuncaoBruto_Enabled = 1;
         edtContagemItem_FMReferenciaTecnicaVal_Jsonclick = "";
         edtContagemItem_FMReferenciaTecnicaVal_Enabled = 0;
         cmbContagemItem_FMValidacao_Jsonclick = "";
         cmbContagemItem_FMValidacao.Enabled = 1;
         edtContagem_UsuarioAuditorPessoaNom_Jsonclick = "";
         edtContagem_UsuarioAuditorPessoaNom_Enabled = 1;
         bttBtnbtnassociaratributosfm_Visible = 1;
         tblTablefm_Visible = 1;
         edtContagemItem_Funcao_Jsonclick = "";
         edtContagemItem_Funcao_Enabled = 1;
         edtContagemItem_DER_Jsonclick = "";
         edtContagemItem_DER_Enabled = 1;
         edtContagemItem_RA_Jsonclick = "";
         edtContagemItem_RA_Enabled = 1;
         edtContagemItem_CP_Jsonclick = "";
         edtContagemItem_CP_Enabled = 1;
         edtContagemItem_QtdINM_Jsonclick = "";
         edtContagemItem_QtdINM_Enabled = 1;
         edtContagemItem_Evidencias_Enabled = 1;
         cmbContagemItem_TipoUnidade_Jsonclick = "";
         cmbContagemItem_TipoUnidade.Enabled = 1;
         edtContagemItem_PFL_Jsonclick = "";
         edtContagemItem_PFL_Enabled = 1;
         edtContagemItem_PFB_Jsonclick = "";
         edtContagemItem_PFB_Enabled = 1;
         edtContagem_AreaTrabalhoDes_Jsonclick = "";
         edtContagem_AreaTrabalhoDes_Enabled = 0;
         edtContagem_AreaTrabalhoCod_Jsonclick = "";
         edtContagem_AreaTrabalhoCod_Enabled = 0;
         edtContagem_DataCriacao_Jsonclick = "";
         edtContagem_DataCriacao_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContagem_UsuarioContadorPessoaCod_Jsonclick = "";
         edtContagem_UsuarioContadorPessoaCod_Enabled = 0;
         edtContagem_UsuarioContadorPessoaCod_Visible = 1;
         edtContagem_UsuarioContadorCod_Jsonclick = "";
         edtContagem_UsuarioContadorCod_Enabled = 0;
         edtContagem_UsuarioContadorCod_Visible = 1;
         edtContagem_FabricaSoftwarePessoaCod_Jsonclick = "";
         edtContagem_FabricaSoftwarePessoaCod_Enabled = 1;
         edtContagem_FabricaSoftwarePessoaCod_Visible = 1;
         edtContagem_FabricaSoftwareCod_Jsonclick = "";
         edtContagem_FabricaSoftwareCod_Enabled = 1;
         edtContagem_FabricaSoftwareCod_Visible = 1;
         edtContagem_UsuarioAuditorPessoaCod_Jsonclick = "";
         edtContagem_UsuarioAuditorPessoaCod_Enabled = 1;
         edtContagem_UsuarioAuditorPessoaCod_Visible = 1;
         edtContagem_UsuarioAuditorCod_Jsonclick = "";
         edtContagem_UsuarioAuditorCod_Enabled = 1;
         edtContagem_UsuarioAuditorCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GX6ASACONTAGEMITEM_FMAR1744( int A224ContagemItem_Lancamento )
      {
         GXt_int1 = A252ContagemItem_FMAR;
         new prc_calculaaruniquefm(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         A252ContagemItem_FMAR = (short)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A252ContagemItem_FMAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A252ContagemItem_FMAR), 3, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A252ContagemItem_FMAR), 3, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX7ASACONTAGEMITEM_FSAR1744( int A224ContagemItem_Lancamento )
      {
         GXt_int1 = A233ContagemItem_FSAR;
         new prc_calculaaruniquefs(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         A233ContagemItem_FSAR = (short)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A233ContagemItem_FSAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A233ContagemItem_FSAR), 3, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A233ContagemItem_FSAR), 3, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX8ASAFUNCAOAPF_COMPLEXIDADE1744( int A165FuncaoAPF_Codigo )
      {
         GXt_char2 = A185FuncaoAPF_Complexidade;
         new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A185FuncaoAPF_Complexidade = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A185FuncaoAPF_Complexidade))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Contagemitem_lancamento( int GX_Parm1 ,
                                                 short GX_Parm2 ,
                                                 short GX_Parm3 ,
                                                 short GX_Parm4 )
      {
         A224ContagemItem_Lancamento = GX_Parm1;
         A251ContagemItem_FMDER = GX_Parm2;
         n251ContagemItem_FMDER = false;
         A252ContagemItem_FMAR = GX_Parm3;
         A233ContagemItem_FSAR = GX_Parm4;
         /* Using cursor T001730 */
         pr_default.execute(22, new Object[] {A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(22) != 101) )
         {
            A251ContagemItem_FMDER = T001730_A251ContagemItem_FMDER[0];
            n251ContagemItem_FMDER = T001730_n251ContagemItem_FMDER[0];
         }
         else
         {
            A251ContagemItem_FMDER = 0;
            n251ContagemItem_FMDER = false;
         }
         pr_default.close(22);
         GXt_int1 = A252ContagemItem_FMAR;
         new prc_calculaaruniquefm(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
         A252ContagemItem_FMAR = (short)(GXt_int1);
         GXt_int1 = A233ContagemItem_FSAR;
         new prc_calculaaruniquefs(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
         A233ContagemItem_FSAR = (short)(GXt_int1);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A251ContagemItem_FMDER = 0;
            n251ContagemItem_FMDER = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A251ContagemItem_FMDER), 3, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A252ContagemItem_FMAR), 3, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A233ContagemItem_FSAR), 3, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Funcaoapf_codigo( int GX_Parm1 ,
                                          String GX_Parm2 ,
                                          GXCombobox cmbGX_Parm3 ,
                                          GXCombobox cmbGX_Parm4 )
      {
         A165FuncaoAPF_Codigo = GX_Parm1;
         n165FuncaoAPF_Codigo = false;
         A166FuncaoAPF_Nome = GX_Parm2;
         cmbFuncaoAPF_Tipo = cmbGX_Parm3;
         A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.CurrentValue;
         cmbFuncaoAPF_Tipo.CurrentValue = A184FuncaoAPF_Tipo;
         cmbFuncaoAPF_Complexidade = cmbGX_Parm4;
         A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.CurrentValue;
         cmbFuncaoAPF_Complexidade.CurrentValue = A185FuncaoAPF_Complexidade;
         /* Using cursor T001731 */
         pr_default.execute(23, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(23) == 101) )
         {
            if ( ! ( (0==A165FuncaoAPF_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Fun��es APF - An�lise de Ponto de Fun��o'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_CODIGO");
               AnyError = 1;
               GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
            }
         }
         A166FuncaoAPF_Nome = T001731_A166FuncaoAPF_Nome[0];
         A184FuncaoAPF_Tipo = T001731_A184FuncaoAPF_Tipo[0];
         cmbFuncaoAPF_Tipo.CurrentValue = A184FuncaoAPF_Tipo;
         pr_default.close(23);
         GXt_char2 = A185FuncaoAPF_Complexidade;
         new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char2) ;
         A185FuncaoAPF_Complexidade = GXt_char2;
         cmbFuncaoAPF_Complexidade.CurrentValue = A185FuncaoAPF_Complexidade;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A166FuncaoAPF_Nome = "";
            A184FuncaoAPF_Tipo = "";
            cmbFuncaoAPF_Tipo.CurrentValue = A184FuncaoAPF_Tipo;
         }
         isValidOutput.Add(A166FuncaoAPF_Nome);
         cmbFuncaoAPF_Tipo.CurrentValue = A184FuncaoAPF_Tipo;
         isValidOutput.Add(cmbFuncaoAPF_Tipo);
         cmbFuncaoAPF_Complexidade.CurrentValue = A185FuncaoAPF_Complexidade;
         isValidOutput.Add(cmbFuncaoAPF_Complexidade);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemitem_fsreferenciatecnicacod( int GX_Parm1 ,
                                                             String GX_Parm2 ,
                                                             decimal GX_Parm3 )
      {
         A226ContagemItem_FSReferenciaTecnicaCod = GX_Parm1;
         n226ContagemItem_FSReferenciaTecnicaCod = false;
         A269ContagemItem_FSReferenciaTecnicaNom = GX_Parm2;
         n269ContagemItem_FSReferenciaTecnicaNom = false;
         A227ContagemItem_FSReferenciaTecnicaVal = GX_Parm3;
         n227ContagemItem_FSReferenciaTecnicaVal = false;
         /* Using cursor T001732 */
         pr_default.execute(24, new Object[] {n226ContagemItem_FSReferenciaTecnicaCod, A226ContagemItem_FSReferenciaTecnicaCod});
         if ( (pr_default.getStatus(24) == 101) )
         {
            if ( ! ( (0==A226ContagemItem_FSReferenciaTecnicaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Refer�ncia T�cnica'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_FSREFERENCIATECNICACOD");
               AnyError = 1;
               GX_FocusControl = edtContagemItem_FSReferenciaTecnicaCod_Internalname;
            }
         }
         A269ContagemItem_FSReferenciaTecnicaNom = T001732_A269ContagemItem_FSReferenciaTecnicaNom[0];
         n269ContagemItem_FSReferenciaTecnicaNom = T001732_n269ContagemItem_FSReferenciaTecnicaNom[0];
         A227ContagemItem_FSReferenciaTecnicaVal = T001732_A227ContagemItem_FSReferenciaTecnicaVal[0];
         n227ContagemItem_FSReferenciaTecnicaVal = T001732_n227ContagemItem_FSReferenciaTecnicaVal[0];
         pr_default.close(24);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A269ContagemItem_FSReferenciaTecnicaNom = "";
            n269ContagemItem_FSReferenciaTecnicaNom = false;
            A227ContagemItem_FSReferenciaTecnicaVal = 0;
            n227ContagemItem_FSReferenciaTecnicaVal = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A269ContagemItem_FSReferenciaTecnicaNom));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemitem_fmreferenciatecnicacod( int GX_Parm1 ,
                                                             String GX_Parm2 ,
                                                             decimal GX_Parm3 )
      {
         A255ContagemItem_FMReferenciaTecnicaCod = GX_Parm1;
         n255ContagemItem_FMReferenciaTecnicaCod = false;
         A257ContagemItem_FMReferenciaTecnicaNom = GX_Parm2;
         n257ContagemItem_FMReferenciaTecnicaNom = false;
         A256ContagemItem_FMReferenciaTecnicaVal = GX_Parm3;
         n256ContagemItem_FMReferenciaTecnicaVal = false;
         /* Using cursor T001733 */
         pr_default.execute(25, new Object[] {n255ContagemItem_FMReferenciaTecnicaCod, A255ContagemItem_FMReferenciaTecnicaCod});
         if ( (pr_default.getStatus(25) == 101) )
         {
            if ( ! ( (0==A255ContagemItem_FMReferenciaTecnicaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Item_FMReferencia Tecnica'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_FMREFERENCIATECNICACOD");
               AnyError = 1;
               GX_FocusControl = edtContagemItem_FMReferenciaTecnicaCod_Internalname;
            }
         }
         A257ContagemItem_FMReferenciaTecnicaNom = T001733_A257ContagemItem_FMReferenciaTecnicaNom[0];
         n257ContagemItem_FMReferenciaTecnicaNom = T001733_n257ContagemItem_FMReferenciaTecnicaNom[0];
         A256ContagemItem_FMReferenciaTecnicaVal = T001733_A256ContagemItem_FMReferenciaTecnicaVal[0];
         n256ContagemItem_FMReferenciaTecnicaVal = T001733_n256ContagemItem_FMReferenciaTecnicaVal[0];
         pr_default.close(25);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A257ContagemItem_FMReferenciaTecnicaNom = "";
            n257ContagemItem_FMReferenciaTecnicaNom = false;
            A256ContagemItem_FMReferenciaTecnicaVal = 0;
            n256ContagemItem_FMReferenciaTecnicaVal = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A257ContagemItem_FMReferenciaTecnicaNom));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContagemItem_Lancamento',fld:'vCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E14172',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOBTNASSOCIARATRIBUTOSFM'","{handler:'E111744',iparms:[{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOBTNSELECIONARATRIBUTOSFS'","{handler:'E121744',iparms:[{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(23);
         pr_default.close(24);
         pr_default.close(25);
         pr_default.close(22);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z209Contagem_FabricaSoftwarePessoaNom = "";
         Z218Contagem_UsuarioAuditorPessoaNom = "";
         Z952ContagemItem_TipoUnidade = "";
         Z955ContagemItem_CP = "";
         Z958ContagemItem_Funcao = "";
         Z228ContagemItem_FSTipoUnidade = "";
         Z235ContagemItem_FSCP = "";
         Z240ContagemItem_FMCP = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A952ContagemItem_TipoUnidade = "";
         A195Contagem_Tecnica = "";
         A196Contagem_Tipo = "";
         A184FuncaoAPF_Tipo = "";
         A185FuncaoAPF_Complexidade = "";
         A228ContagemItem_FSTipoUnidade = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         lblContagemitemtitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontagem_datacriacao_Jsonclick = "";
         A197Contagem_DataCriacao = DateTime.MinValue;
         lblTextblockcontagem_areatrabalhocod_Jsonclick = "";
         lblTextblockcontagem_areatrabalhodes_Jsonclick = "";
         A194Contagem_AreaTrabalhoDes = "";
         lblTextblockcontagemitem_pfb_Jsonclick = "";
         lblTextblockcontagemitem_pfl_Jsonclick = "";
         lblTextblockcontagemitem_tipounidade_Jsonclick = "";
         lblTextblockcontagemitem_evidencias_Jsonclick = "";
         A953ContagemItem_Evidencias = "";
         lblTextblockcontagemitem_qtdinm_Jsonclick = "";
         lblTextblockcontagemitem_cp_Jsonclick = "";
         A955ContagemItem_CP = "";
         lblTextblockcontagemitem_ra_Jsonclick = "";
         lblTextblockcontagemitem_der_Jsonclick = "";
         lblTextblockcontagemitem_funcao_Jsonclick = "";
         A958ContagemItem_Funcao = "";
         bttBtnbtnassociaratributosfm_Jsonclick = "";
         lblTextblockcontagem_usuarioauditorpessoanom_Jsonclick = "";
         A218Contagem_UsuarioAuditorPessoaNom = "";
         lblTextblockcontagemitem_fmvalidacao_Jsonclick = "";
         lblTextblockcontagemitem_fmreferenciatecnicacod_Jsonclick = "";
         lblTextblockcontagemitem_fmreferenciatecnicaval_Jsonclick = "";
         lblTextblockcontagemitem_fmder_Jsonclick = "";
         lblTextblockcontagemitem_fmqtdinm_Jsonclick = "";
         lblTextblockcontagemitem_fmpontofuncaobruto_Jsonclick = "";
         lblTextblockcontagemitem_fmpontofuncaoliquido_Jsonclick = "";
         lblTextblockcontagemitem_fmcp_Jsonclick = "";
         A240ContagemItem_FMCP = "";
         lblContagemitem_fmder_righttext_Jsonclick = "";
         lblTextblockcontagemitem_fmar_Jsonclick = "";
         lblContagemitem_fmar_righttext_Jsonclick = "";
         A257ContagemItem_FMReferenciaTecnicaNom = "";
         bttBtnbtnselecionaratributosfs_Jsonclick = "";
         lblTextblockcontagem_fabricasoftwarepessoanom_Jsonclick = "";
         A209Contagem_FabricaSoftwarePessoaNom = "";
         lblTextblockcontagem_usuariocontadorpessoanom_Jsonclick = "";
         A215Contagem_UsuarioContadorPessoaNom = "";
         lblTextblockcontagemitem_fsvalidacao_Jsonclick = "";
         lblTextblockcontagemitem_fsreferenciatecnicacod_Jsonclick = "";
         lblTextblockcontagemitem_fsreferenciatecnicaval_Jsonclick = "";
         lblTextblockcontagemitem_fsder_Jsonclick = "";
         lblTextblockcontagemitem_fsqtdinm_Jsonclick = "";
         lblTextblockcontagemitem_fspontofuncaobruto_Jsonclick = "";
         lblTextblockcontagemitem_fsevidencias_Jsonclick = "";
         A238ContagemItem_FSEvidencias = "";
         lblTextblockcontagemitem_fspontofuncaoliquido_Jsonclick = "";
         lblTextblockcontagemitem_fscp_Jsonclick = "";
         A235ContagemItem_FSCP = "";
         lblContagemitem_fsder_righttext_Jsonclick = "";
         lblTextblockcontagemitem_fsar_Jsonclick = "";
         lblContagemitem_fsar_righttext_Jsonclick = "";
         A269ContagemItem_FSReferenciaTecnicaNom = "";
         lblTextblockcontagemitem_fstipounidade_Jsonclick = "";
         lblTextblockcontagemitem_lancamento_Jsonclick = "";
         lblTextblockcontagem_codigo_Jsonclick = "";
         lblTextblockfuncaoapf_codigo_Jsonclick = "";
         lblTextblockcontagemitem_requisito_Jsonclick = "";
         A166FuncaoAPF_Nome = "";
         lblTextblockfuncaoapf_tipo_Jsonclick = "";
         lblTextblockfuncaoapf_complexidade_Jsonclick = "";
         lblTextblockcontagem_tecnica_Jsonclick = "";
         lblTextblockcontagem_tipo_Jsonclick = "";
         lblTextblockcontagemitem_sequencial_Jsonclick = "";
         lblTextblockcontagemitem_validacaostatusfinal_Jsonclick = "";
         AV16Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode44 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV15TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z953ContagemItem_Evidencias = "";
         Z238ContagemItem_FSEvidencias = "";
         Z197Contagem_DataCriacao = DateTime.MinValue;
         Z195Contagem_Tecnica = "";
         Z196Contagem_Tipo = "";
         Z194Contagem_AreaTrabalhoDes = "";
         Z215Contagem_UsuarioContadorPessoaNom = "";
         Z166FuncaoAPF_Nome = "";
         Z184FuncaoAPF_Tipo = "";
         Z269ContagemItem_FSReferenciaTecnicaNom = "";
         Z257ContagemItem_FMReferenciaTecnicaNom = "";
         T00175_A232ContagemItem_FSDER = new short[1] ;
         T00175_n232ContagemItem_FSDER = new bool[] {false} ;
         T00176_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         T00176_A195Contagem_Tecnica = new String[] {""} ;
         T00176_n195Contagem_Tecnica = new bool[] {false} ;
         T00176_A196Contagem_Tipo = new String[] {""} ;
         T00176_n196Contagem_Tipo = new bool[] {false} ;
         T00176_A193Contagem_AreaTrabalhoCod = new int[1] ;
         T00176_A213Contagem_UsuarioContadorCod = new int[1] ;
         T00176_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         T001710_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         T001710_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         T001711_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         T001711_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         T001712_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         T001712_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         T001714_A251ContagemItem_FMDER = new short[1] ;
         T001714_n251ContagemItem_FMDER = new bool[] {false} ;
         T00179_A257ContagemItem_FMReferenciaTecnicaNom = new String[] {""} ;
         T00179_n257ContagemItem_FMReferenciaTecnicaNom = new bool[] {false} ;
         T00179_A256ContagemItem_FMReferenciaTecnicaVal = new decimal[1] ;
         T00179_n256ContagemItem_FMReferenciaTecnicaVal = new bool[] {false} ;
         T00178_A269ContagemItem_FSReferenciaTecnicaNom = new String[] {""} ;
         T00178_n269ContagemItem_FSReferenciaTecnicaNom = new bool[] {false} ;
         T00178_A227ContagemItem_FSReferenciaTecnicaVal = new decimal[1] ;
         T00178_n227ContagemItem_FSReferenciaTecnicaVal = new bool[] {false} ;
         T00177_A166FuncaoAPF_Nome = new String[] {""} ;
         T00177_A184FuncaoAPF_Tipo = new String[] {""} ;
         T001717_A192Contagem_Codigo = new int[1] ;
         T001717_A224ContagemItem_Lancamento = new int[1] ;
         T001717_A268ContagemItem_FMValidacao = new short[1] ;
         T001717_n268ContagemItem_FMValidacao = new bool[] {false} ;
         T001717_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         T001717_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         T001717_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         T001717_A195Contagem_Tecnica = new String[] {""} ;
         T001717_n195Contagem_Tecnica = new bool[] {false} ;
         T001717_A196Contagem_Tipo = new String[] {""} ;
         T001717_n196Contagem_Tipo = new bool[] {false} ;
         T001717_A207Contagem_FabricaSoftwareCod = new int[1] ;
         T001717_A208Contagem_FabricaSoftwarePessoaCod = new int[1] ;
         T001717_A209Contagem_FabricaSoftwarePessoaNom = new String[] {""} ;
         T001717_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         T001717_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         T001717_A216Contagem_UsuarioAuditorCod = new int[1] ;
         T001717_A217Contagem_UsuarioAuditorPessoaCod = new int[1] ;
         T001717_A218Contagem_UsuarioAuditorPessoaNom = new String[] {""} ;
         T001717_A225ContagemItem_Sequencial = new short[1] ;
         T001717_n225ContagemItem_Sequencial = new bool[] {false} ;
         T001717_A229ContagemItem_Requisito = new short[1] ;
         T001717_n229ContagemItem_Requisito = new bool[] {false} ;
         T001717_A950ContagemItem_PFB = new decimal[1] ;
         T001717_n950ContagemItem_PFB = new bool[] {false} ;
         T001717_A951ContagemItem_PFL = new decimal[1] ;
         T001717_n951ContagemItem_PFL = new bool[] {false} ;
         T001717_A166FuncaoAPF_Nome = new String[] {""} ;
         T001717_A184FuncaoAPF_Tipo = new String[] {""} ;
         T001717_A952ContagemItem_TipoUnidade = new String[] {""} ;
         T001717_A953ContagemItem_Evidencias = new String[] {""} ;
         T001717_n953ContagemItem_Evidencias = new bool[] {false} ;
         T001717_A954ContagemItem_QtdINM = new short[1] ;
         T001717_n954ContagemItem_QtdINM = new bool[] {false} ;
         T001717_A955ContagemItem_CP = new String[] {""} ;
         T001717_n955ContagemItem_CP = new bool[] {false} ;
         T001717_A956ContagemItem_RA = new short[1] ;
         T001717_n956ContagemItem_RA = new bool[] {false} ;
         T001717_A957ContagemItem_DER = new short[1] ;
         T001717_n957ContagemItem_DER = new bool[] {false} ;
         T001717_A958ContagemItem_Funcao = new String[] {""} ;
         T001717_n958ContagemItem_Funcao = new bool[] {false} ;
         T001717_A228ContagemItem_FSTipoUnidade = new String[] {""} ;
         T001717_n228ContagemItem_FSTipoUnidade = new bool[] {false} ;
         T001717_A267ContagemItem_FSValidacao = new short[1] ;
         T001717_n267ContagemItem_FSValidacao = new bool[] {false} ;
         T001717_A269ContagemItem_FSReferenciaTecnicaNom = new String[] {""} ;
         T001717_n269ContagemItem_FSReferenciaTecnicaNom = new bool[] {false} ;
         T001717_A227ContagemItem_FSReferenciaTecnicaVal = new decimal[1] ;
         T001717_n227ContagemItem_FSReferenciaTecnicaVal = new bool[] {false} ;
         T001717_A234ContagemItem_FSQTDInm = new int[1] ;
         T001717_n234ContagemItem_FSQTDInm = new bool[] {false} ;
         T001717_A235ContagemItem_FSCP = new String[] {""} ;
         T001717_n235ContagemItem_FSCP = new bool[] {false} ;
         T001717_A236ContagemItem_FSPontoFuncaoBruto = new decimal[1] ;
         T001717_n236ContagemItem_FSPontoFuncaoBruto = new bool[] {false} ;
         T001717_A237ContagemItem_FSPontoFuncaoLiquido = new decimal[1] ;
         T001717_n237ContagemItem_FSPontoFuncaoLiquido = new bool[] {false} ;
         T001717_A238ContagemItem_FSEvidencias = new String[] {""} ;
         T001717_n238ContagemItem_FSEvidencias = new bool[] {false} ;
         T001717_A257ContagemItem_FMReferenciaTecnicaNom = new String[] {""} ;
         T001717_n257ContagemItem_FMReferenciaTecnicaNom = new bool[] {false} ;
         T001717_A256ContagemItem_FMReferenciaTecnicaVal = new decimal[1] ;
         T001717_n256ContagemItem_FMReferenciaTecnicaVal = new bool[] {false} ;
         T001717_A239ContagemItem_FMQTDInm = new int[1] ;
         T001717_n239ContagemItem_FMQTDInm = new bool[] {false} ;
         T001717_A240ContagemItem_FMCP = new String[] {""} ;
         T001717_n240ContagemItem_FMCP = new bool[] {false} ;
         T001717_A241ContagemItem_FMPontoFuncaoBruto = new decimal[1] ;
         T001717_n241ContagemItem_FMPontoFuncaoBruto = new bool[] {false} ;
         T001717_A242ContagemItem_FMPontoFuncaoLiquido = new decimal[1] ;
         T001717_n242ContagemItem_FMPontoFuncaoLiquido = new bool[] {false} ;
         T001717_A165FuncaoAPF_Codigo = new int[1] ;
         T001717_n165FuncaoAPF_Codigo = new bool[] {false} ;
         T001717_A226ContagemItem_FSReferenciaTecnicaCod = new int[1] ;
         T001717_n226ContagemItem_FSReferenciaTecnicaCod = new bool[] {false} ;
         T001717_A255ContagemItem_FMReferenciaTecnicaCod = new int[1] ;
         T001717_n255ContagemItem_FMReferenciaTecnicaCod = new bool[] {false} ;
         T001717_A193Contagem_AreaTrabalhoCod = new int[1] ;
         T001717_A213Contagem_UsuarioContadorCod = new int[1] ;
         T001717_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         T001717_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         T001717_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         T001717_A232ContagemItem_FSDER = new short[1] ;
         T001717_n232ContagemItem_FSDER = new bool[] {false} ;
         T001717_A251ContagemItem_FMDER = new short[1] ;
         T001717_n251ContagemItem_FMDER = new bool[] {false} ;
         T001719_A251ContagemItem_FMDER = new short[1] ;
         T001719_n251ContagemItem_FMDER = new bool[] {false} ;
         T001720_A166FuncaoAPF_Nome = new String[] {""} ;
         T001720_A184FuncaoAPF_Tipo = new String[] {""} ;
         T001721_A269ContagemItem_FSReferenciaTecnicaNom = new String[] {""} ;
         T001721_n269ContagemItem_FSReferenciaTecnicaNom = new bool[] {false} ;
         T001721_A227ContagemItem_FSReferenciaTecnicaVal = new decimal[1] ;
         T001721_n227ContagemItem_FSReferenciaTecnicaVal = new bool[] {false} ;
         T001722_A257ContagemItem_FMReferenciaTecnicaNom = new String[] {""} ;
         T001722_n257ContagemItem_FMReferenciaTecnicaNom = new bool[] {false} ;
         T001722_A256ContagemItem_FMReferenciaTecnicaVal = new decimal[1] ;
         T001722_n256ContagemItem_FMReferenciaTecnicaVal = new bool[] {false} ;
         T001723_A224ContagemItem_Lancamento = new int[1] ;
         T00173_A192Contagem_Codigo = new int[1] ;
         T00173_A224ContagemItem_Lancamento = new int[1] ;
         T00173_A268ContagemItem_FMValidacao = new short[1] ;
         T00173_n268ContagemItem_FMValidacao = new bool[] {false} ;
         T00173_A207Contagem_FabricaSoftwareCod = new int[1] ;
         T00173_A208Contagem_FabricaSoftwarePessoaCod = new int[1] ;
         T00173_A209Contagem_FabricaSoftwarePessoaNom = new String[] {""} ;
         T00173_A216Contagem_UsuarioAuditorCod = new int[1] ;
         T00173_A217Contagem_UsuarioAuditorPessoaCod = new int[1] ;
         T00173_A218Contagem_UsuarioAuditorPessoaNom = new String[] {""} ;
         T00173_A225ContagemItem_Sequencial = new short[1] ;
         T00173_n225ContagemItem_Sequencial = new bool[] {false} ;
         T00173_A229ContagemItem_Requisito = new short[1] ;
         T00173_n229ContagemItem_Requisito = new bool[] {false} ;
         T00173_A950ContagemItem_PFB = new decimal[1] ;
         T00173_n950ContagemItem_PFB = new bool[] {false} ;
         T00173_A951ContagemItem_PFL = new decimal[1] ;
         T00173_n951ContagemItem_PFL = new bool[] {false} ;
         T00173_A952ContagemItem_TipoUnidade = new String[] {""} ;
         T00173_A953ContagemItem_Evidencias = new String[] {""} ;
         T00173_n953ContagemItem_Evidencias = new bool[] {false} ;
         T00173_A954ContagemItem_QtdINM = new short[1] ;
         T00173_n954ContagemItem_QtdINM = new bool[] {false} ;
         T00173_A955ContagemItem_CP = new String[] {""} ;
         T00173_n955ContagemItem_CP = new bool[] {false} ;
         T00173_A956ContagemItem_RA = new short[1] ;
         T00173_n956ContagemItem_RA = new bool[] {false} ;
         T00173_A957ContagemItem_DER = new short[1] ;
         T00173_n957ContagemItem_DER = new bool[] {false} ;
         T00173_A958ContagemItem_Funcao = new String[] {""} ;
         T00173_n958ContagemItem_Funcao = new bool[] {false} ;
         T00173_A228ContagemItem_FSTipoUnidade = new String[] {""} ;
         T00173_n228ContagemItem_FSTipoUnidade = new bool[] {false} ;
         T00173_A267ContagemItem_FSValidacao = new short[1] ;
         T00173_n267ContagemItem_FSValidacao = new bool[] {false} ;
         T00173_A234ContagemItem_FSQTDInm = new int[1] ;
         T00173_n234ContagemItem_FSQTDInm = new bool[] {false} ;
         T00173_A235ContagemItem_FSCP = new String[] {""} ;
         T00173_n235ContagemItem_FSCP = new bool[] {false} ;
         T00173_A236ContagemItem_FSPontoFuncaoBruto = new decimal[1] ;
         T00173_n236ContagemItem_FSPontoFuncaoBruto = new bool[] {false} ;
         T00173_A237ContagemItem_FSPontoFuncaoLiquido = new decimal[1] ;
         T00173_n237ContagemItem_FSPontoFuncaoLiquido = new bool[] {false} ;
         T00173_A238ContagemItem_FSEvidencias = new String[] {""} ;
         T00173_n238ContagemItem_FSEvidencias = new bool[] {false} ;
         T00173_A239ContagemItem_FMQTDInm = new int[1] ;
         T00173_n239ContagemItem_FMQTDInm = new bool[] {false} ;
         T00173_A240ContagemItem_FMCP = new String[] {""} ;
         T00173_n240ContagemItem_FMCP = new bool[] {false} ;
         T00173_A241ContagemItem_FMPontoFuncaoBruto = new decimal[1] ;
         T00173_n241ContagemItem_FMPontoFuncaoBruto = new bool[] {false} ;
         T00173_A242ContagemItem_FMPontoFuncaoLiquido = new decimal[1] ;
         T00173_n242ContagemItem_FMPontoFuncaoLiquido = new bool[] {false} ;
         T00173_A165FuncaoAPF_Codigo = new int[1] ;
         T00173_n165FuncaoAPF_Codigo = new bool[] {false} ;
         T00173_A226ContagemItem_FSReferenciaTecnicaCod = new int[1] ;
         T00173_n226ContagemItem_FSReferenciaTecnicaCod = new bool[] {false} ;
         T00173_A255ContagemItem_FMReferenciaTecnicaCod = new int[1] ;
         T00173_n255ContagemItem_FMReferenciaTecnicaCod = new bool[] {false} ;
         T001724_A224ContagemItem_Lancamento = new int[1] ;
         T001724_A192Contagem_Codigo = new int[1] ;
         T001725_A224ContagemItem_Lancamento = new int[1] ;
         T001725_A192Contagem_Codigo = new int[1] ;
         T00172_A192Contagem_Codigo = new int[1] ;
         T00172_A224ContagemItem_Lancamento = new int[1] ;
         T00172_A268ContagemItem_FMValidacao = new short[1] ;
         T00172_n268ContagemItem_FMValidacao = new bool[] {false} ;
         T00172_A207Contagem_FabricaSoftwareCod = new int[1] ;
         T00172_A208Contagem_FabricaSoftwarePessoaCod = new int[1] ;
         T00172_A209Contagem_FabricaSoftwarePessoaNom = new String[] {""} ;
         T00172_A216Contagem_UsuarioAuditorCod = new int[1] ;
         T00172_A217Contagem_UsuarioAuditorPessoaCod = new int[1] ;
         T00172_A218Contagem_UsuarioAuditorPessoaNom = new String[] {""} ;
         T00172_A225ContagemItem_Sequencial = new short[1] ;
         T00172_n225ContagemItem_Sequencial = new bool[] {false} ;
         T00172_A229ContagemItem_Requisito = new short[1] ;
         T00172_n229ContagemItem_Requisito = new bool[] {false} ;
         T00172_A950ContagemItem_PFB = new decimal[1] ;
         T00172_n950ContagemItem_PFB = new bool[] {false} ;
         T00172_A951ContagemItem_PFL = new decimal[1] ;
         T00172_n951ContagemItem_PFL = new bool[] {false} ;
         T00172_A952ContagemItem_TipoUnidade = new String[] {""} ;
         T00172_A953ContagemItem_Evidencias = new String[] {""} ;
         T00172_n953ContagemItem_Evidencias = new bool[] {false} ;
         T00172_A954ContagemItem_QtdINM = new short[1] ;
         T00172_n954ContagemItem_QtdINM = new bool[] {false} ;
         T00172_A955ContagemItem_CP = new String[] {""} ;
         T00172_n955ContagemItem_CP = new bool[] {false} ;
         T00172_A956ContagemItem_RA = new short[1] ;
         T00172_n956ContagemItem_RA = new bool[] {false} ;
         T00172_A957ContagemItem_DER = new short[1] ;
         T00172_n957ContagemItem_DER = new bool[] {false} ;
         T00172_A958ContagemItem_Funcao = new String[] {""} ;
         T00172_n958ContagemItem_Funcao = new bool[] {false} ;
         T00172_A228ContagemItem_FSTipoUnidade = new String[] {""} ;
         T00172_n228ContagemItem_FSTipoUnidade = new bool[] {false} ;
         T00172_A267ContagemItem_FSValidacao = new short[1] ;
         T00172_n267ContagemItem_FSValidacao = new bool[] {false} ;
         T00172_A234ContagemItem_FSQTDInm = new int[1] ;
         T00172_n234ContagemItem_FSQTDInm = new bool[] {false} ;
         T00172_A235ContagemItem_FSCP = new String[] {""} ;
         T00172_n235ContagemItem_FSCP = new bool[] {false} ;
         T00172_A236ContagemItem_FSPontoFuncaoBruto = new decimal[1] ;
         T00172_n236ContagemItem_FSPontoFuncaoBruto = new bool[] {false} ;
         T00172_A237ContagemItem_FSPontoFuncaoLiquido = new decimal[1] ;
         T00172_n237ContagemItem_FSPontoFuncaoLiquido = new bool[] {false} ;
         T00172_A238ContagemItem_FSEvidencias = new String[] {""} ;
         T00172_n238ContagemItem_FSEvidencias = new bool[] {false} ;
         T00172_A239ContagemItem_FMQTDInm = new int[1] ;
         T00172_n239ContagemItem_FMQTDInm = new bool[] {false} ;
         T00172_A240ContagemItem_FMCP = new String[] {""} ;
         T00172_n240ContagemItem_FMCP = new bool[] {false} ;
         T00172_A241ContagemItem_FMPontoFuncaoBruto = new decimal[1] ;
         T00172_n241ContagemItem_FMPontoFuncaoBruto = new bool[] {false} ;
         T00172_A242ContagemItem_FMPontoFuncaoLiquido = new decimal[1] ;
         T00172_n242ContagemItem_FMPontoFuncaoLiquido = new bool[] {false} ;
         T00172_A165FuncaoAPF_Codigo = new int[1] ;
         T00172_n165FuncaoAPF_Codigo = new bool[] {false} ;
         T00172_A226ContagemItem_FSReferenciaTecnicaCod = new int[1] ;
         T00172_n226ContagemItem_FSReferenciaTecnicaCod = new bool[] {false} ;
         T00172_A255ContagemItem_FMReferenciaTecnicaCod = new int[1] ;
         T00172_n255ContagemItem_FMReferenciaTecnicaCod = new bool[] {false} ;
         T001726_A224ContagemItem_Lancamento = new int[1] ;
         T001730_A251ContagemItem_FMDER = new short[1] ;
         T001730_n251ContagemItem_FMDER = new bool[] {false} ;
         T001731_A166FuncaoAPF_Nome = new String[] {""} ;
         T001731_A184FuncaoAPF_Tipo = new String[] {""} ;
         T001732_A269ContagemItem_FSReferenciaTecnicaNom = new String[] {""} ;
         T001732_n269ContagemItem_FSReferenciaTecnicaNom = new bool[] {false} ;
         T001732_A227ContagemItem_FSReferenciaTecnicaVal = new decimal[1] ;
         T001732_n227ContagemItem_FSReferenciaTecnicaVal = new bool[] {false} ;
         T001733_A257ContagemItem_FMReferenciaTecnicaNom = new String[] {""} ;
         T001733_n257ContagemItem_FMReferenciaTecnicaNom = new bool[] {false} ;
         T001733_A256ContagemItem_FMReferenciaTecnicaVal = new decimal[1] ;
         T001733_n256ContagemItem_FMReferenciaTecnicaVal = new bool[] {false} ;
         T001734_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         T001734_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         T001735_A243ContagemItemParecer_Codigo = new int[1] ;
         T001736_A224ContagemItem_Lancamento = new int[1] ;
         T001736_A379ContagemItem_AtributosCod = new int[1] ;
         T001737_A224ContagemItem_Lancamento = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         GXt_char2 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemitem__default(),
            new Object[][] {
                new Object[] {
               T00172_A192Contagem_Codigo, T00172_A224ContagemItem_Lancamento, T00172_A268ContagemItem_FMValidacao, T00172_n268ContagemItem_FMValidacao, T00172_A207Contagem_FabricaSoftwareCod, T00172_A208Contagem_FabricaSoftwarePessoaCod, T00172_A209Contagem_FabricaSoftwarePessoaNom, T00172_A216Contagem_UsuarioAuditorCod, T00172_A217Contagem_UsuarioAuditorPessoaCod, T00172_A218Contagem_UsuarioAuditorPessoaNom,
               T00172_A225ContagemItem_Sequencial, T00172_n225ContagemItem_Sequencial, T00172_A229ContagemItem_Requisito, T00172_n229ContagemItem_Requisito, T00172_A950ContagemItem_PFB, T00172_n950ContagemItem_PFB, T00172_A951ContagemItem_PFL, T00172_n951ContagemItem_PFL, T00172_A952ContagemItem_TipoUnidade, T00172_A953ContagemItem_Evidencias,
               T00172_n953ContagemItem_Evidencias, T00172_A954ContagemItem_QtdINM, T00172_n954ContagemItem_QtdINM, T00172_A955ContagemItem_CP, T00172_n955ContagemItem_CP, T00172_A956ContagemItem_RA, T00172_n956ContagemItem_RA, T00172_A957ContagemItem_DER, T00172_n957ContagemItem_DER, T00172_A958ContagemItem_Funcao,
               T00172_n958ContagemItem_Funcao, T00172_A228ContagemItem_FSTipoUnidade, T00172_n228ContagemItem_FSTipoUnidade, T00172_A267ContagemItem_FSValidacao, T00172_n267ContagemItem_FSValidacao, T00172_A234ContagemItem_FSQTDInm, T00172_n234ContagemItem_FSQTDInm, T00172_A235ContagemItem_FSCP, T00172_n235ContagemItem_FSCP, T00172_A236ContagemItem_FSPontoFuncaoBruto,
               T00172_n236ContagemItem_FSPontoFuncaoBruto, T00172_A237ContagemItem_FSPontoFuncaoLiquido, T00172_n237ContagemItem_FSPontoFuncaoLiquido, T00172_A238ContagemItem_FSEvidencias, T00172_n238ContagemItem_FSEvidencias, T00172_A239ContagemItem_FMQTDInm, T00172_n239ContagemItem_FMQTDInm, T00172_A240ContagemItem_FMCP, T00172_n240ContagemItem_FMCP, T00172_A241ContagemItem_FMPontoFuncaoBruto,
               T00172_n241ContagemItem_FMPontoFuncaoBruto, T00172_A242ContagemItem_FMPontoFuncaoLiquido, T00172_n242ContagemItem_FMPontoFuncaoLiquido, T00172_A165FuncaoAPF_Codigo, T00172_n165FuncaoAPF_Codigo, T00172_A226ContagemItem_FSReferenciaTecnicaCod, T00172_n226ContagemItem_FSReferenciaTecnicaCod, T00172_A255ContagemItem_FMReferenciaTecnicaCod, T00172_n255ContagemItem_FMReferenciaTecnicaCod
               }
               , new Object[] {
               T00173_A192Contagem_Codigo, T00173_A224ContagemItem_Lancamento, T00173_A268ContagemItem_FMValidacao, T00173_n268ContagemItem_FMValidacao, T00173_A207Contagem_FabricaSoftwareCod, T00173_A208Contagem_FabricaSoftwarePessoaCod, T00173_A209Contagem_FabricaSoftwarePessoaNom, T00173_A216Contagem_UsuarioAuditorCod, T00173_A217Contagem_UsuarioAuditorPessoaCod, T00173_A218Contagem_UsuarioAuditorPessoaNom,
               T00173_A225ContagemItem_Sequencial, T00173_n225ContagemItem_Sequencial, T00173_A229ContagemItem_Requisito, T00173_n229ContagemItem_Requisito, T00173_A950ContagemItem_PFB, T00173_n950ContagemItem_PFB, T00173_A951ContagemItem_PFL, T00173_n951ContagemItem_PFL, T00173_A952ContagemItem_TipoUnidade, T00173_A953ContagemItem_Evidencias,
               T00173_n953ContagemItem_Evidencias, T00173_A954ContagemItem_QtdINM, T00173_n954ContagemItem_QtdINM, T00173_A955ContagemItem_CP, T00173_n955ContagemItem_CP, T00173_A956ContagemItem_RA, T00173_n956ContagemItem_RA, T00173_A957ContagemItem_DER, T00173_n957ContagemItem_DER, T00173_A958ContagemItem_Funcao,
               T00173_n958ContagemItem_Funcao, T00173_A228ContagemItem_FSTipoUnidade, T00173_n228ContagemItem_FSTipoUnidade, T00173_A267ContagemItem_FSValidacao, T00173_n267ContagemItem_FSValidacao, T00173_A234ContagemItem_FSQTDInm, T00173_n234ContagemItem_FSQTDInm, T00173_A235ContagemItem_FSCP, T00173_n235ContagemItem_FSCP, T00173_A236ContagemItem_FSPontoFuncaoBruto,
               T00173_n236ContagemItem_FSPontoFuncaoBruto, T00173_A237ContagemItem_FSPontoFuncaoLiquido, T00173_n237ContagemItem_FSPontoFuncaoLiquido, T00173_A238ContagemItem_FSEvidencias, T00173_n238ContagemItem_FSEvidencias, T00173_A239ContagemItem_FMQTDInm, T00173_n239ContagemItem_FMQTDInm, T00173_A240ContagemItem_FMCP, T00173_n240ContagemItem_FMCP, T00173_A241ContagemItem_FMPontoFuncaoBruto,
               T00173_n241ContagemItem_FMPontoFuncaoBruto, T00173_A242ContagemItem_FMPontoFuncaoLiquido, T00173_n242ContagemItem_FMPontoFuncaoLiquido, T00173_A165FuncaoAPF_Codigo, T00173_n165FuncaoAPF_Codigo, T00173_A226ContagemItem_FSReferenciaTecnicaCod, T00173_n226ContagemItem_FSReferenciaTecnicaCod, T00173_A255ContagemItem_FMReferenciaTecnicaCod, T00173_n255ContagemItem_FMReferenciaTecnicaCod
               }
               , new Object[] {
               T00175_A232ContagemItem_FSDER, T00175_n232ContagemItem_FSDER
               }
               , new Object[] {
               T00176_A197Contagem_DataCriacao, T00176_A195Contagem_Tecnica, T00176_n195Contagem_Tecnica, T00176_A196Contagem_Tipo, T00176_n196Contagem_Tipo, T00176_A193Contagem_AreaTrabalhoCod, T00176_A213Contagem_UsuarioContadorCod, T00176_n213Contagem_UsuarioContadorCod
               }
               , new Object[] {
               T00177_A166FuncaoAPF_Nome, T00177_A184FuncaoAPF_Tipo
               }
               , new Object[] {
               T00178_A269ContagemItem_FSReferenciaTecnicaNom, T00178_n269ContagemItem_FSReferenciaTecnicaNom, T00178_A227ContagemItem_FSReferenciaTecnicaVal, T00178_n227ContagemItem_FSReferenciaTecnicaVal
               }
               , new Object[] {
               T00179_A257ContagemItem_FMReferenciaTecnicaNom, T00179_n257ContagemItem_FMReferenciaTecnicaNom, T00179_A256ContagemItem_FMReferenciaTecnicaVal, T00179_n256ContagemItem_FMReferenciaTecnicaVal
               }
               , new Object[] {
               T001710_A194Contagem_AreaTrabalhoDes, T001710_n194Contagem_AreaTrabalhoDes
               }
               , new Object[] {
               T001711_A214Contagem_UsuarioContadorPessoaCod, T001711_n214Contagem_UsuarioContadorPessoaCod
               }
               , new Object[] {
               T001712_A215Contagem_UsuarioContadorPessoaNom, T001712_n215Contagem_UsuarioContadorPessoaNom
               }
               , new Object[] {
               T001714_A251ContagemItem_FMDER, T001714_n251ContagemItem_FMDER
               }
               , new Object[] {
               T001717_A192Contagem_Codigo, T001717_A224ContagemItem_Lancamento, T001717_A268ContagemItem_FMValidacao, T001717_n268ContagemItem_FMValidacao, T001717_A197Contagem_DataCriacao, T001717_A194Contagem_AreaTrabalhoDes, T001717_n194Contagem_AreaTrabalhoDes, T001717_A195Contagem_Tecnica, T001717_n195Contagem_Tecnica, T001717_A196Contagem_Tipo,
               T001717_n196Contagem_Tipo, T001717_A207Contagem_FabricaSoftwareCod, T001717_A208Contagem_FabricaSoftwarePessoaCod, T001717_A209Contagem_FabricaSoftwarePessoaNom, T001717_A215Contagem_UsuarioContadorPessoaNom, T001717_n215Contagem_UsuarioContadorPessoaNom, T001717_A216Contagem_UsuarioAuditorCod, T001717_A217Contagem_UsuarioAuditorPessoaCod, T001717_A218Contagem_UsuarioAuditorPessoaNom, T001717_A225ContagemItem_Sequencial,
               T001717_n225ContagemItem_Sequencial, T001717_A229ContagemItem_Requisito, T001717_n229ContagemItem_Requisito, T001717_A950ContagemItem_PFB, T001717_n950ContagemItem_PFB, T001717_A951ContagemItem_PFL, T001717_n951ContagemItem_PFL, T001717_A166FuncaoAPF_Nome, T001717_A184FuncaoAPF_Tipo, T001717_A952ContagemItem_TipoUnidade,
               T001717_A953ContagemItem_Evidencias, T001717_n953ContagemItem_Evidencias, T001717_A954ContagemItem_QtdINM, T001717_n954ContagemItem_QtdINM, T001717_A955ContagemItem_CP, T001717_n955ContagemItem_CP, T001717_A956ContagemItem_RA, T001717_n956ContagemItem_RA, T001717_A957ContagemItem_DER, T001717_n957ContagemItem_DER,
               T001717_A958ContagemItem_Funcao, T001717_n958ContagemItem_Funcao, T001717_A228ContagemItem_FSTipoUnidade, T001717_n228ContagemItem_FSTipoUnidade, T001717_A267ContagemItem_FSValidacao, T001717_n267ContagemItem_FSValidacao, T001717_A269ContagemItem_FSReferenciaTecnicaNom, T001717_n269ContagemItem_FSReferenciaTecnicaNom, T001717_A227ContagemItem_FSReferenciaTecnicaVal, T001717_n227ContagemItem_FSReferenciaTecnicaVal,
               T001717_A234ContagemItem_FSQTDInm, T001717_n234ContagemItem_FSQTDInm, T001717_A235ContagemItem_FSCP, T001717_n235ContagemItem_FSCP, T001717_A236ContagemItem_FSPontoFuncaoBruto, T001717_n236ContagemItem_FSPontoFuncaoBruto, T001717_A237ContagemItem_FSPontoFuncaoLiquido, T001717_n237ContagemItem_FSPontoFuncaoLiquido, T001717_A238ContagemItem_FSEvidencias, T001717_n238ContagemItem_FSEvidencias,
               T001717_A257ContagemItem_FMReferenciaTecnicaNom, T001717_n257ContagemItem_FMReferenciaTecnicaNom, T001717_A256ContagemItem_FMReferenciaTecnicaVal, T001717_n256ContagemItem_FMReferenciaTecnicaVal, T001717_A239ContagemItem_FMQTDInm, T001717_n239ContagemItem_FMQTDInm, T001717_A240ContagemItem_FMCP, T001717_n240ContagemItem_FMCP, T001717_A241ContagemItem_FMPontoFuncaoBruto, T001717_n241ContagemItem_FMPontoFuncaoBruto,
               T001717_A242ContagemItem_FMPontoFuncaoLiquido, T001717_n242ContagemItem_FMPontoFuncaoLiquido, T001717_A165FuncaoAPF_Codigo, T001717_n165FuncaoAPF_Codigo, T001717_A226ContagemItem_FSReferenciaTecnicaCod, T001717_n226ContagemItem_FSReferenciaTecnicaCod, T001717_A255ContagemItem_FMReferenciaTecnicaCod, T001717_n255ContagemItem_FMReferenciaTecnicaCod, T001717_A193Contagem_AreaTrabalhoCod, T001717_A213Contagem_UsuarioContadorCod,
               T001717_n213Contagem_UsuarioContadorCod, T001717_A214Contagem_UsuarioContadorPessoaCod, T001717_n214Contagem_UsuarioContadorPessoaCod, T001717_A232ContagemItem_FSDER, T001717_n232ContagemItem_FSDER, T001717_A251ContagemItem_FMDER, T001717_n251ContagemItem_FMDER
               }
               , new Object[] {
               T001719_A251ContagemItem_FMDER, T001719_n251ContagemItem_FMDER
               }
               , new Object[] {
               T001720_A166FuncaoAPF_Nome, T001720_A184FuncaoAPF_Tipo
               }
               , new Object[] {
               T001721_A269ContagemItem_FSReferenciaTecnicaNom, T001721_n269ContagemItem_FSReferenciaTecnicaNom, T001721_A227ContagemItem_FSReferenciaTecnicaVal, T001721_n227ContagemItem_FSReferenciaTecnicaVal
               }
               , new Object[] {
               T001722_A257ContagemItem_FMReferenciaTecnicaNom, T001722_n257ContagemItem_FMReferenciaTecnicaNom, T001722_A256ContagemItem_FMReferenciaTecnicaVal, T001722_n256ContagemItem_FMReferenciaTecnicaVal
               }
               , new Object[] {
               T001723_A224ContagemItem_Lancamento
               }
               , new Object[] {
               T001724_A224ContagemItem_Lancamento, T001724_A192Contagem_Codigo
               }
               , new Object[] {
               T001725_A224ContagemItem_Lancamento, T001725_A192Contagem_Codigo
               }
               , new Object[] {
               T001726_A224ContagemItem_Lancamento
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001730_A251ContagemItem_FMDER, T001730_n251ContagemItem_FMDER
               }
               , new Object[] {
               T001731_A166FuncaoAPF_Nome, T001731_A184FuncaoAPF_Tipo
               }
               , new Object[] {
               T001732_A269ContagemItem_FSReferenciaTecnicaNom, T001732_n269ContagemItem_FSReferenciaTecnicaNom, T001732_A227ContagemItem_FSReferenciaTecnicaVal, T001732_n227ContagemItem_FSReferenciaTecnicaVal
               }
               , new Object[] {
               T001733_A257ContagemItem_FMReferenciaTecnicaNom, T001733_n257ContagemItem_FMReferenciaTecnicaNom, T001733_A256ContagemItem_FMReferenciaTecnicaVal, T001733_n256ContagemItem_FMReferenciaTecnicaVal
               }
               , new Object[] {
               T001734_A261ContagemItemAtributosFMetrica_ItemContagemLan, T001734_A249ContagemItem_FMetricasAtributosCod
               }
               , new Object[] {
               T001735_A243ContagemItemParecer_Codigo
               }
               , new Object[] {
               T001736_A224ContagemItem_Lancamento, T001736_A379ContagemItem_AtributosCod
               }
               , new Object[] {
               T001737_A224ContagemItem_Lancamento
               }
            }
         );
         Z192Contagem_Codigo = 0;
         A192Contagem_Codigo = 0;
         AV16Pgmname = "ContagemItem";
      }

      private short Z268ContagemItem_FMValidacao ;
      private short Z225ContagemItem_Sequencial ;
      private short Z229ContagemItem_Requisito ;
      private short Z954ContagemItem_QtdINM ;
      private short Z956ContagemItem_RA ;
      private short Z957ContagemItem_DER ;
      private short Z267ContagemItem_FSValidacao ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A260ContagemItem_ValidacaoStatusFinal ;
      private short A267ContagemItem_FSValidacao ;
      private short A268ContagemItem_FMValidacao ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A954ContagemItem_QtdINM ;
      private short A956ContagemItem_RA ;
      private short A957ContagemItem_DER ;
      private short A251ContagemItem_FMDER ;
      private short A252ContagemItem_FMAR ;
      private short A232ContagemItem_FSDER ;
      private short A233ContagemItem_FSAR ;
      private short A229ContagemItem_Requisito ;
      private short A225ContagemItem_Sequencial ;
      private short RcdFound44 ;
      private short GX_JID ;
      private short Z232ContagemItem_FSDER ;
      private short Z251ContagemItem_FMDER ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ContagemItem_Lancamento ;
      private int wcpOA192Contagem_Codigo ;
      private int Z224ContagemItem_Lancamento ;
      private int Z207Contagem_FabricaSoftwareCod ;
      private int Z208Contagem_FabricaSoftwarePessoaCod ;
      private int Z216Contagem_UsuarioAuditorCod ;
      private int Z217Contagem_UsuarioAuditorPessoaCod ;
      private int Z234ContagemItem_FSQTDInm ;
      private int Z239ContagemItem_FMQTDInm ;
      private int Z165FuncaoAPF_Codigo ;
      private int Z226ContagemItem_FSReferenciaTecnicaCod ;
      private int Z255ContagemItem_FMReferenciaTecnicaCod ;
      private int N165FuncaoAPF_Codigo ;
      private int N226ContagemItem_FSReferenciaTecnicaCod ;
      private int N255ContagemItem_FMReferenciaTecnicaCod ;
      private int A224ContagemItem_Lancamento ;
      private int A165FuncaoAPF_Codigo ;
      private int A226ContagemItem_FSReferenciaTecnicaCod ;
      private int A255ContagemItem_FMReferenciaTecnicaCod ;
      private int AV7ContagemItem_Lancamento ;
      private int A192Contagem_Codigo ;
      private int trnEnded ;
      private int A216Contagem_UsuarioAuditorCod ;
      private int edtContagem_UsuarioAuditorCod_Enabled ;
      private int edtContagem_UsuarioAuditorCod_Visible ;
      private int A217Contagem_UsuarioAuditorPessoaCod ;
      private int edtContagem_UsuarioAuditorPessoaCod_Enabled ;
      private int edtContagem_UsuarioAuditorPessoaCod_Visible ;
      private int A207Contagem_FabricaSoftwareCod ;
      private int edtContagem_FabricaSoftwareCod_Enabled ;
      private int edtContagem_FabricaSoftwareCod_Visible ;
      private int A208Contagem_FabricaSoftwarePessoaCod ;
      private int edtContagem_FabricaSoftwarePessoaCod_Enabled ;
      private int edtContagem_FabricaSoftwarePessoaCod_Visible ;
      private int A213Contagem_UsuarioContadorCod ;
      private int edtContagem_UsuarioContadorCod_Enabled ;
      private int edtContagem_UsuarioContadorCod_Visible ;
      private int A214Contagem_UsuarioContadorPessoaCod ;
      private int edtContagem_UsuarioContadorPessoaCod_Enabled ;
      private int edtContagem_UsuarioContadorPessoaCod_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContagem_DataCriacao_Enabled ;
      private int A193Contagem_AreaTrabalhoCod ;
      private int edtContagem_AreaTrabalhoCod_Enabled ;
      private int edtContagem_AreaTrabalhoDes_Enabled ;
      private int edtContagemItem_PFB_Enabled ;
      private int edtContagemItem_PFL_Enabled ;
      private int edtContagemItem_Evidencias_Enabled ;
      private int edtContagemItem_QtdINM_Enabled ;
      private int edtContagemItem_CP_Enabled ;
      private int edtContagemItem_RA_Enabled ;
      private int edtContagemItem_DER_Enabled ;
      private int edtContagemItem_Funcao_Enabled ;
      private int tblTablefm_Visible ;
      private int bttBtnbtnassociaratributosfm_Visible ;
      private int edtContagem_UsuarioAuditorPessoaNom_Enabled ;
      private int edtContagemItem_FMReferenciaTecnicaVal_Enabled ;
      private int edtContagemItem_FMPontoFuncaoBruto_Enabled ;
      private int edtContagemItem_FMPontoFuncaoLiquido_Enabled ;
      private int A239ContagemItem_FMQTDInm ;
      private int edtContagemItem_FMQTDInm_Enabled ;
      private int edtContagemItem_FMCP_Enabled ;
      private int edtContagemItem_FMDER_Enabled ;
      private int edtContagemItem_FMAR_Enabled ;
      private int edtContagemItem_FMReferenciaTecnicaCod_Enabled ;
      private int imgprompt_255_Visible ;
      private int edtContagemItem_FMReferenciaTecnicaNom_Enabled ;
      private int bttBtnbtnselecionaratributosfs_Visible ;
      private int bttBtnbtnselecionaratributosfs_Enabled ;
      private int edtContagem_FabricaSoftwarePessoaNom_Enabled ;
      private int edtContagem_UsuarioContadorPessoaNom_Enabled ;
      private int edtContagemItem_FSReferenciaTecnicaVal_Enabled ;
      private int edtContagemItem_FSEvidencias_Enabled ;
      private int edtContagemItem_FSPontoFuncaoBruto_Enabled ;
      private int edtContagemItem_FSPontoFuncaoLiquido_Enabled ;
      private int A234ContagemItem_FSQTDInm ;
      private int edtContagemItem_FSQTDInm_Enabled ;
      private int edtContagemItem_FSCP_Enabled ;
      private int edtContagemItem_FSDER_Enabled ;
      private int edtContagemItem_FSAR_Enabled ;
      private int edtContagemItem_FSReferenciaTecnicaCod_Enabled ;
      private int imgprompt_226_Visible ;
      private int edtContagemItem_FSReferenciaTecnicaNom_Enabled ;
      private int edtContagemItem_Lancamento_Enabled ;
      private int edtContagemItem_Requisito_Enabled ;
      private int edtFuncaoAPF_Codigo_Enabled ;
      private int imgprompt_165_Visible ;
      private int edtFuncaoAPF_Nome_Enabled ;
      private int edtContagem_Codigo_Enabled ;
      private int edtContagemItem_Sequencial_Enabled ;
      private int AV11Insert_Contagem_Codigo ;
      private int AV12Insert_FuncaoAPF_Codigo ;
      private int AV13Insert_ContagemItem_FSReferenciaTecnicaCod ;
      private int AV14Insert_ContagemItem_FMReferenciaTecnicaCod ;
      private int AV17GXV1 ;
      private int Z192Contagem_Codigo ;
      private int Z193Contagem_AreaTrabalhoCod ;
      private int Z213Contagem_UsuarioContadorCod ;
      private int Z214Contagem_UsuarioContadorPessoaCod ;
      private int idxLst ;
      private int GXt_int1 ;
      private decimal Z950ContagemItem_PFB ;
      private decimal Z951ContagemItem_PFL ;
      private decimal Z236ContagemItem_FSPontoFuncaoBruto ;
      private decimal Z237ContagemItem_FSPontoFuncaoLiquido ;
      private decimal Z241ContagemItem_FMPontoFuncaoBruto ;
      private decimal Z242ContagemItem_FMPontoFuncaoLiquido ;
      private decimal A950ContagemItem_PFB ;
      private decimal A951ContagemItem_PFL ;
      private decimal A256ContagemItem_FMReferenciaTecnicaVal ;
      private decimal A241ContagemItem_FMPontoFuncaoBruto ;
      private decimal A242ContagemItem_FMPontoFuncaoLiquido ;
      private decimal A227ContagemItem_FSReferenciaTecnicaVal ;
      private decimal A236ContagemItem_FSPontoFuncaoBruto ;
      private decimal A237ContagemItem_FSPontoFuncaoLiquido ;
      private decimal Z227ContagemItem_FSReferenciaTecnicaVal ;
      private decimal Z256ContagemItem_FMReferenciaTecnicaVal ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z209Contagem_FabricaSoftwarePessoaNom ;
      private String Z218Contagem_UsuarioAuditorPessoaNom ;
      private String Z955ContagemItem_CP ;
      private String Z235ContagemItem_FSCP ;
      private String Z240ContagemItem_FMCP ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A195Contagem_Tecnica ;
      private String A196Contagem_Tipo ;
      private String A184FuncaoAPF_Tipo ;
      private String A185FuncaoAPF_Complexidade ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemItem_PFB_Internalname ;
      private String TempTags ;
      private String edtContagem_UsuarioAuditorCod_Internalname ;
      private String edtContagem_UsuarioAuditorCod_Jsonclick ;
      private String edtContagem_UsuarioAuditorPessoaCod_Internalname ;
      private String edtContagem_UsuarioAuditorPessoaCod_Jsonclick ;
      private String edtContagem_FabricaSoftwareCod_Internalname ;
      private String edtContagem_FabricaSoftwareCod_Jsonclick ;
      private String edtContagem_FabricaSoftwarePessoaCod_Internalname ;
      private String edtContagem_FabricaSoftwarePessoaCod_Jsonclick ;
      private String edtContagem_UsuarioContadorCod_Internalname ;
      private String edtContagem_UsuarioContadorCod_Jsonclick ;
      private String edtContagem_UsuarioContadorPessoaCod_Internalname ;
      private String edtContagem_UsuarioContadorPessoaCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblContagemitemtitle_Internalname ;
      private String lblContagemitemtitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagem_datacriacao_Internalname ;
      private String lblTextblockcontagem_datacriacao_Jsonclick ;
      private String edtContagem_DataCriacao_Internalname ;
      private String edtContagem_DataCriacao_Jsonclick ;
      private String lblTextblockcontagem_areatrabalhocod_Internalname ;
      private String lblTextblockcontagem_areatrabalhocod_Jsonclick ;
      private String edtContagem_AreaTrabalhoCod_Internalname ;
      private String edtContagem_AreaTrabalhoCod_Jsonclick ;
      private String lblTextblockcontagem_areatrabalhodes_Internalname ;
      private String lblTextblockcontagem_areatrabalhodes_Jsonclick ;
      private String edtContagem_AreaTrabalhoDes_Internalname ;
      private String edtContagem_AreaTrabalhoDes_Jsonclick ;
      private String lblTextblockcontagemitem_pfb_Internalname ;
      private String lblTextblockcontagemitem_pfb_Jsonclick ;
      private String edtContagemItem_PFB_Jsonclick ;
      private String lblTextblockcontagemitem_pfl_Internalname ;
      private String lblTextblockcontagemitem_pfl_Jsonclick ;
      private String edtContagemItem_PFL_Internalname ;
      private String edtContagemItem_PFL_Jsonclick ;
      private String lblTextblockcontagemitem_tipounidade_Internalname ;
      private String lblTextblockcontagemitem_tipounidade_Jsonclick ;
      private String cmbContagemItem_TipoUnidade_Internalname ;
      private String cmbContagemItem_TipoUnidade_Jsonclick ;
      private String lblTextblockcontagemitem_evidencias_Internalname ;
      private String lblTextblockcontagemitem_evidencias_Jsonclick ;
      private String edtContagemItem_Evidencias_Internalname ;
      private String lblTextblockcontagemitem_qtdinm_Internalname ;
      private String lblTextblockcontagemitem_qtdinm_Jsonclick ;
      private String edtContagemItem_QtdINM_Internalname ;
      private String edtContagemItem_QtdINM_Jsonclick ;
      private String lblTextblockcontagemitem_cp_Internalname ;
      private String lblTextblockcontagemitem_cp_Jsonclick ;
      private String edtContagemItem_CP_Internalname ;
      private String A955ContagemItem_CP ;
      private String edtContagemItem_CP_Jsonclick ;
      private String lblTextblockcontagemitem_ra_Internalname ;
      private String lblTextblockcontagemitem_ra_Jsonclick ;
      private String edtContagemItem_RA_Internalname ;
      private String edtContagemItem_RA_Jsonclick ;
      private String lblTextblockcontagemitem_der_Internalname ;
      private String lblTextblockcontagemitem_der_Jsonclick ;
      private String edtContagemItem_DER_Internalname ;
      private String edtContagemItem_DER_Jsonclick ;
      private String lblTextblockcontagemitem_funcao_Internalname ;
      private String lblTextblockcontagemitem_funcao_Jsonclick ;
      private String edtContagemItem_Funcao_Internalname ;
      private String edtContagemItem_Funcao_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String tblUnnamedtable4_Internalname ;
      private String tblTablefm_Internalname ;
      private String bttBtnbtnassociaratributosfm_Internalname ;
      private String bttBtnbtnassociaratributosfm_Jsonclick ;
      private String grpUnnamedgroup8_Internalname ;
      private String tblUnnamedtable7_Internalname ;
      private String lblTextblockcontagem_usuarioauditorpessoanom_Internalname ;
      private String lblTextblockcontagem_usuarioauditorpessoanom_Jsonclick ;
      private String edtContagem_UsuarioAuditorPessoaNom_Internalname ;
      private String A218Contagem_UsuarioAuditorPessoaNom ;
      private String edtContagem_UsuarioAuditorPessoaNom_Jsonclick ;
      private String lblTextblockcontagemitem_fmvalidacao_Internalname ;
      private String lblTextblockcontagemitem_fmvalidacao_Jsonclick ;
      private String cmbContagemItem_FMValidacao_Internalname ;
      private String cmbContagemItem_FMValidacao_Jsonclick ;
      private String lblTextblockcontagemitem_fmreferenciatecnicacod_Internalname ;
      private String lblTextblockcontagemitem_fmreferenciatecnicacod_Jsonclick ;
      private String lblTextblockcontagemitem_fmreferenciatecnicaval_Internalname ;
      private String lblTextblockcontagemitem_fmreferenciatecnicaval_Jsonclick ;
      private String edtContagemItem_FMReferenciaTecnicaVal_Internalname ;
      private String edtContagemItem_FMReferenciaTecnicaVal_Jsonclick ;
      private String lblTextblockcontagemitem_fmder_Internalname ;
      private String lblTextblockcontagemitem_fmder_Jsonclick ;
      private String lblTextblockcontagemitem_fmqtdinm_Internalname ;
      private String lblTextblockcontagemitem_fmqtdinm_Jsonclick ;
      private String lblTextblockcontagemitem_fmpontofuncaobruto_Internalname ;
      private String lblTextblockcontagemitem_fmpontofuncaobruto_Jsonclick ;
      private String tblTablemergedcontagemitem_fmpontofuncaobruto_Internalname ;
      private String edtContagemItem_FMPontoFuncaoBruto_Internalname ;
      private String edtContagemItem_FMPontoFuncaoBruto_Jsonclick ;
      private String lblTextblockcontagemitem_fmpontofuncaoliquido_Internalname ;
      private String lblTextblockcontagemitem_fmpontofuncaoliquido_Jsonclick ;
      private String edtContagemItem_FMPontoFuncaoLiquido_Internalname ;
      private String edtContagemItem_FMPontoFuncaoLiquido_Jsonclick ;
      private String tblTablemergedcontagemitem_fmqtdinm_Internalname ;
      private String edtContagemItem_FMQTDInm_Internalname ;
      private String edtContagemItem_FMQTDInm_Jsonclick ;
      private String lblTextblockcontagemitem_fmcp_Internalname ;
      private String lblTextblockcontagemitem_fmcp_Jsonclick ;
      private String edtContagemItem_FMCP_Internalname ;
      private String A240ContagemItem_FMCP ;
      private String edtContagemItem_FMCP_Jsonclick ;
      private String tblTablemergedcontagemitem_fmder_Internalname ;
      private String edtContagemItem_FMDER_Internalname ;
      private String edtContagemItem_FMDER_Jsonclick ;
      private String cellContagemitem_fmder_righttext_cell_Internalname ;
      private String lblContagemitem_fmder_righttext_Internalname ;
      private String lblContagemitem_fmder_righttext_Jsonclick ;
      private String lblTextblockcontagemitem_fmar_Internalname ;
      private String lblTextblockcontagemitem_fmar_Jsonclick ;
      private String edtContagemItem_FMAR_Internalname ;
      private String edtContagemItem_FMAR_Jsonclick ;
      private String cellContagemitem_fmar_righttext_cell_Internalname ;
      private String lblContagemitem_fmar_righttext_Internalname ;
      private String lblContagemitem_fmar_righttext_Jsonclick ;
      private String tblTablemergedcontagemitem_fmreferenciatecnicacod_Internalname ;
      private String edtContagemItem_FMReferenciaTecnicaCod_Internalname ;
      private String edtContagemItem_FMReferenciaTecnicaCod_Jsonclick ;
      private String imgprompt_255_Internalname ;
      private String imgprompt_255_Link ;
      private String edtContagemItem_FMReferenciaTecnicaNom_Internalname ;
      private String A257ContagemItem_FMReferenciaTecnicaNom ;
      private String edtContagemItem_FMReferenciaTecnicaNom_Jsonclick ;
      private String tblUnnamedtable6_Internalname ;
      private String tblUnnamedtable5_Internalname ;
      private String bttBtnbtnselecionaratributosfs_Internalname ;
      private String bttBtnbtnselecionaratributosfs_Jsonclick ;
      private String grpUnnamedgroup10_Internalname ;
      private String tblUnnamedtable9_Internalname ;
      private String lblTextblockcontagem_fabricasoftwarepessoanom_Internalname ;
      private String lblTextblockcontagem_fabricasoftwarepessoanom_Jsonclick ;
      private String edtContagem_FabricaSoftwarePessoaNom_Internalname ;
      private String A209Contagem_FabricaSoftwarePessoaNom ;
      private String edtContagem_FabricaSoftwarePessoaNom_Jsonclick ;
      private String lblTextblockcontagem_usuariocontadorpessoanom_Internalname ;
      private String lblTextblockcontagem_usuariocontadorpessoanom_Jsonclick ;
      private String edtContagem_UsuarioContadorPessoaNom_Internalname ;
      private String A215Contagem_UsuarioContadorPessoaNom ;
      private String edtContagem_UsuarioContadorPessoaNom_Jsonclick ;
      private String lblTextblockcontagemitem_fsvalidacao_Internalname ;
      private String lblTextblockcontagemitem_fsvalidacao_Jsonclick ;
      private String lblTextblockcontagemitem_fsreferenciatecnicacod_Internalname ;
      private String lblTextblockcontagemitem_fsreferenciatecnicacod_Jsonclick ;
      private String lblTextblockcontagemitem_fsreferenciatecnicaval_Internalname ;
      private String lblTextblockcontagemitem_fsreferenciatecnicaval_Jsonclick ;
      private String edtContagemItem_FSReferenciaTecnicaVal_Internalname ;
      private String edtContagemItem_FSReferenciaTecnicaVal_Jsonclick ;
      private String lblTextblockcontagemitem_fsder_Internalname ;
      private String lblTextblockcontagemitem_fsder_Jsonclick ;
      private String lblTextblockcontagemitem_fsqtdinm_Internalname ;
      private String lblTextblockcontagemitem_fsqtdinm_Jsonclick ;
      private String lblTextblockcontagemitem_fspontofuncaobruto_Internalname ;
      private String lblTextblockcontagemitem_fspontofuncaobruto_Jsonclick ;
      private String lblTextblockcontagemitem_fsevidencias_Internalname ;
      private String lblTextblockcontagemitem_fsevidencias_Jsonclick ;
      private String edtContagemItem_FSEvidencias_Internalname ;
      private String tblTablemergedcontagemitem_fspontofuncaobruto_Internalname ;
      private String edtContagemItem_FSPontoFuncaoBruto_Internalname ;
      private String edtContagemItem_FSPontoFuncaoBruto_Jsonclick ;
      private String lblTextblockcontagemitem_fspontofuncaoliquido_Internalname ;
      private String lblTextblockcontagemitem_fspontofuncaoliquido_Jsonclick ;
      private String edtContagemItem_FSPontoFuncaoLiquido_Internalname ;
      private String edtContagemItem_FSPontoFuncaoLiquido_Jsonclick ;
      private String tblTablemergedcontagemitem_fsqtdinm_Internalname ;
      private String edtContagemItem_FSQTDInm_Internalname ;
      private String edtContagemItem_FSQTDInm_Jsonclick ;
      private String lblTextblockcontagemitem_fscp_Internalname ;
      private String lblTextblockcontagemitem_fscp_Jsonclick ;
      private String edtContagemItem_FSCP_Internalname ;
      private String A235ContagemItem_FSCP ;
      private String edtContagemItem_FSCP_Jsonclick ;
      private String tblTablemergedcontagemitem_fsder_Internalname ;
      private String edtContagemItem_FSDER_Internalname ;
      private String edtContagemItem_FSDER_Jsonclick ;
      private String cellContagemitem_fsder_righttext_cell_Internalname ;
      private String lblContagemitem_fsder_righttext_Internalname ;
      private String lblContagemitem_fsder_righttext_Jsonclick ;
      private String lblTextblockcontagemitem_fsar_Internalname ;
      private String lblTextblockcontagemitem_fsar_Jsonclick ;
      private String edtContagemItem_FSAR_Internalname ;
      private String edtContagemItem_FSAR_Jsonclick ;
      private String cellContagemitem_fsar_righttext_cell_Internalname ;
      private String lblContagemitem_fsar_righttext_Internalname ;
      private String lblContagemitem_fsar_righttext_Jsonclick ;
      private String tblTablemergedcontagemitem_fsreferenciatecnicacod_Internalname ;
      private String edtContagemItem_FSReferenciaTecnicaCod_Internalname ;
      private String edtContagemItem_FSReferenciaTecnicaCod_Jsonclick ;
      private String imgprompt_226_Internalname ;
      private String imgprompt_226_Link ;
      private String edtContagemItem_FSReferenciaTecnicaNom_Internalname ;
      private String A269ContagemItem_FSReferenciaTecnicaNom ;
      private String edtContagemItem_FSReferenciaTecnicaNom_Jsonclick ;
      private String tblTablemergedcontagemitem_fsvalidacao_Internalname ;
      private String cmbContagemItem_FSValidacao_Internalname ;
      private String cmbContagemItem_FSValidacao_Jsonclick ;
      private String lblTextblockcontagemitem_fstipounidade_Internalname ;
      private String lblTextblockcontagemitem_fstipounidade_Jsonclick ;
      private String cmbContagemItem_FSTipoUnidade_Internalname ;
      private String cmbContagemItem_FSTipoUnidade_Jsonclick ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTextblockcontagemitem_lancamento_Internalname ;
      private String lblTextblockcontagemitem_lancamento_Jsonclick ;
      private String edtContagemItem_Lancamento_Internalname ;
      private String edtContagemItem_Lancamento_Jsonclick ;
      private String lblTextblockcontagem_codigo_Internalname ;
      private String lblTextblockcontagem_codigo_Jsonclick ;
      private String lblTextblockfuncaoapf_codigo_Internalname ;
      private String lblTextblockfuncaoapf_codigo_Jsonclick ;
      private String lblTextblockcontagemitem_requisito_Internalname ;
      private String lblTextblockcontagemitem_requisito_Jsonclick ;
      private String edtContagemItem_Requisito_Internalname ;
      private String edtContagemItem_Requisito_Jsonclick ;
      private String tblTablemergedfuncaoapf_codigo_Internalname ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String imgprompt_165_Internalname ;
      private String imgprompt_165_Link ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String lblTextblockfuncaoapf_tipo_Internalname ;
      private String lblTextblockfuncaoapf_tipo_Jsonclick ;
      private String cmbFuncaoAPF_Tipo_Internalname ;
      private String cmbFuncaoAPF_Tipo_Jsonclick ;
      private String lblTextblockfuncaoapf_complexidade_Internalname ;
      private String lblTextblockfuncaoapf_complexidade_Jsonclick ;
      private String cmbFuncaoAPF_Complexidade_Internalname ;
      private String cmbFuncaoAPF_Complexidade_Jsonclick ;
      private String tblTablemergedcontagem_codigo_Internalname ;
      private String edtContagem_Codigo_Internalname ;
      private String edtContagem_Codigo_Jsonclick ;
      private String lblTextblockcontagem_tecnica_Internalname ;
      private String lblTextblockcontagem_tecnica_Jsonclick ;
      private String cmbContagem_Tecnica_Internalname ;
      private String cmbContagem_Tecnica_Jsonclick ;
      private String lblTextblockcontagem_tipo_Internalname ;
      private String lblTextblockcontagem_tipo_Jsonclick ;
      private String cmbContagem_Tipo_Internalname ;
      private String cmbContagem_Tipo_Jsonclick ;
      private String lblTextblockcontagemitem_sequencial_Internalname ;
      private String lblTextblockcontagemitem_sequencial_Jsonclick ;
      private String edtContagemItem_Sequencial_Internalname ;
      private String edtContagemItem_Sequencial_Jsonclick ;
      private String lblTextblockcontagemitem_validacaostatusfinal_Internalname ;
      private String lblTextblockcontagemitem_validacaostatusfinal_Jsonclick ;
      private String cmbContagemItem_ValidacaoStatusFinal_Internalname ;
      private String cmbContagemItem_ValidacaoStatusFinal_Jsonclick ;
      private String AV16Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode44 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z195Contagem_Tecnica ;
      private String Z196Contagem_Tipo ;
      private String Z215Contagem_UsuarioContadorPessoaNom ;
      private String Z184FuncaoAPF_Tipo ;
      private String Z269ContagemItem_FSReferenciaTecnicaNom ;
      private String Z257ContagemItem_FMReferenciaTecnicaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String GXt_char2 ;
      private DateTime A197Contagem_DataCriacao ;
      private DateTime Z197Contagem_DataCriacao ;
      private bool entryPointCalled ;
      private bool n165FuncaoAPF_Codigo ;
      private bool n226ContagemItem_FSReferenciaTecnicaCod ;
      private bool n255ContagemItem_FMReferenciaTecnicaCod ;
      private bool toggleJsOutput ;
      private bool n195Contagem_Tecnica ;
      private bool n196Contagem_Tipo ;
      private bool n267ContagemItem_FSValidacao ;
      private bool n228ContagemItem_FSTipoUnidade ;
      private bool n268ContagemItem_FMValidacao ;
      private bool wbErr ;
      private bool n194Contagem_AreaTrabalhoDes ;
      private bool n950ContagemItem_PFB ;
      private bool n951ContagemItem_PFL ;
      private bool n953ContagemItem_Evidencias ;
      private bool n954ContagemItem_QtdINM ;
      private bool n955ContagemItem_CP ;
      private bool n956ContagemItem_RA ;
      private bool n957ContagemItem_DER ;
      private bool n958ContagemItem_Funcao ;
      private bool n225ContagemItem_Sequencial ;
      private bool n229ContagemItem_Requisito ;
      private bool n215Contagem_UsuarioContadorPessoaNom ;
      private bool n269ContagemItem_FSReferenciaTecnicaNom ;
      private bool n227ContagemItem_FSReferenciaTecnicaVal ;
      private bool n232ContagemItem_FSDER ;
      private bool n234ContagemItem_FSQTDInm ;
      private bool n235ContagemItem_FSCP ;
      private bool n236ContagemItem_FSPontoFuncaoBruto ;
      private bool n237ContagemItem_FSPontoFuncaoLiquido ;
      private bool n238ContagemItem_FSEvidencias ;
      private bool n257ContagemItem_FMReferenciaTecnicaNom ;
      private bool n256ContagemItem_FMReferenciaTecnicaVal ;
      private bool n251ContagemItem_FMDER ;
      private bool n239ContagemItem_FMQTDInm ;
      private bool n240ContagemItem_FMCP ;
      private bool n241ContagemItem_FMPontoFuncaoBruto ;
      private bool n242ContagemItem_FMPontoFuncaoLiquido ;
      private bool n213Contagem_UsuarioContadorCod ;
      private bool n214Contagem_UsuarioContadorPessoaCod ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A953ContagemItem_Evidencias ;
      private String A238ContagemItem_FSEvidencias ;
      private String Z953ContagemItem_Evidencias ;
      private String Z238ContagemItem_FSEvidencias ;
      private String Z952ContagemItem_TipoUnidade ;
      private String Z958ContagemItem_Funcao ;
      private String Z228ContagemItem_FSTipoUnidade ;
      private String A952ContagemItem_TipoUnidade ;
      private String A228ContagemItem_FSTipoUnidade ;
      private String A194Contagem_AreaTrabalhoDes ;
      private String A958ContagemItem_Funcao ;
      private String A166FuncaoAPF_Nome ;
      private String Z194Contagem_AreaTrabalhoDes ;
      private String Z166FuncaoAPF_Nome ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Contagem_Codigo ;
      private GXCombobox cmbContagemItem_TipoUnidade ;
      private GXCombobox cmbContagem_Tecnica ;
      private GXCombobox cmbContagem_Tipo ;
      private GXCombobox cmbContagemItem_ValidacaoStatusFinal ;
      private GXCombobox cmbFuncaoAPF_Tipo ;
      private GXCombobox cmbFuncaoAPF_Complexidade ;
      private GXCombobox cmbContagemItem_FSValidacao ;
      private GXCombobox cmbContagemItem_FSTipoUnidade ;
      private GXCombobox cmbContagemItem_FMValidacao ;
      private IDataStoreProvider pr_default ;
      private short[] T00175_A232ContagemItem_FSDER ;
      private bool[] T00175_n232ContagemItem_FSDER ;
      private DateTime[] T00176_A197Contagem_DataCriacao ;
      private String[] T00176_A195Contagem_Tecnica ;
      private bool[] T00176_n195Contagem_Tecnica ;
      private String[] T00176_A196Contagem_Tipo ;
      private bool[] T00176_n196Contagem_Tipo ;
      private int[] T00176_A193Contagem_AreaTrabalhoCod ;
      private int[] T00176_A213Contagem_UsuarioContadorCod ;
      private bool[] T00176_n213Contagem_UsuarioContadorCod ;
      private String[] T001710_A194Contagem_AreaTrabalhoDes ;
      private bool[] T001710_n194Contagem_AreaTrabalhoDes ;
      private int[] T001711_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] T001711_n214Contagem_UsuarioContadorPessoaCod ;
      private String[] T001712_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] T001712_n215Contagem_UsuarioContadorPessoaNom ;
      private short[] T001714_A251ContagemItem_FMDER ;
      private bool[] T001714_n251ContagemItem_FMDER ;
      private String[] T00179_A257ContagemItem_FMReferenciaTecnicaNom ;
      private bool[] T00179_n257ContagemItem_FMReferenciaTecnicaNom ;
      private decimal[] T00179_A256ContagemItem_FMReferenciaTecnicaVal ;
      private bool[] T00179_n256ContagemItem_FMReferenciaTecnicaVal ;
      private String[] T00178_A269ContagemItem_FSReferenciaTecnicaNom ;
      private bool[] T00178_n269ContagemItem_FSReferenciaTecnicaNom ;
      private decimal[] T00178_A227ContagemItem_FSReferenciaTecnicaVal ;
      private bool[] T00178_n227ContagemItem_FSReferenciaTecnicaVal ;
      private String[] T00177_A166FuncaoAPF_Nome ;
      private String[] T00177_A184FuncaoAPF_Tipo ;
      private int[] T001717_A192Contagem_Codigo ;
      private int[] T001717_A224ContagemItem_Lancamento ;
      private short[] T001717_A268ContagemItem_FMValidacao ;
      private bool[] T001717_n268ContagemItem_FMValidacao ;
      private DateTime[] T001717_A197Contagem_DataCriacao ;
      private String[] T001717_A194Contagem_AreaTrabalhoDes ;
      private bool[] T001717_n194Contagem_AreaTrabalhoDes ;
      private String[] T001717_A195Contagem_Tecnica ;
      private bool[] T001717_n195Contagem_Tecnica ;
      private String[] T001717_A196Contagem_Tipo ;
      private bool[] T001717_n196Contagem_Tipo ;
      private int[] T001717_A207Contagem_FabricaSoftwareCod ;
      private int[] T001717_A208Contagem_FabricaSoftwarePessoaCod ;
      private String[] T001717_A209Contagem_FabricaSoftwarePessoaNom ;
      private String[] T001717_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] T001717_n215Contagem_UsuarioContadorPessoaNom ;
      private int[] T001717_A216Contagem_UsuarioAuditorCod ;
      private int[] T001717_A217Contagem_UsuarioAuditorPessoaCod ;
      private String[] T001717_A218Contagem_UsuarioAuditorPessoaNom ;
      private short[] T001717_A225ContagemItem_Sequencial ;
      private bool[] T001717_n225ContagemItem_Sequencial ;
      private short[] T001717_A229ContagemItem_Requisito ;
      private bool[] T001717_n229ContagemItem_Requisito ;
      private decimal[] T001717_A950ContagemItem_PFB ;
      private bool[] T001717_n950ContagemItem_PFB ;
      private decimal[] T001717_A951ContagemItem_PFL ;
      private bool[] T001717_n951ContagemItem_PFL ;
      private String[] T001717_A166FuncaoAPF_Nome ;
      private String[] T001717_A184FuncaoAPF_Tipo ;
      private String[] T001717_A952ContagemItem_TipoUnidade ;
      private String[] T001717_A953ContagemItem_Evidencias ;
      private bool[] T001717_n953ContagemItem_Evidencias ;
      private short[] T001717_A954ContagemItem_QtdINM ;
      private bool[] T001717_n954ContagemItem_QtdINM ;
      private String[] T001717_A955ContagemItem_CP ;
      private bool[] T001717_n955ContagemItem_CP ;
      private short[] T001717_A956ContagemItem_RA ;
      private bool[] T001717_n956ContagemItem_RA ;
      private short[] T001717_A957ContagemItem_DER ;
      private bool[] T001717_n957ContagemItem_DER ;
      private String[] T001717_A958ContagemItem_Funcao ;
      private bool[] T001717_n958ContagemItem_Funcao ;
      private String[] T001717_A228ContagemItem_FSTipoUnidade ;
      private bool[] T001717_n228ContagemItem_FSTipoUnidade ;
      private short[] T001717_A267ContagemItem_FSValidacao ;
      private bool[] T001717_n267ContagemItem_FSValidacao ;
      private String[] T001717_A269ContagemItem_FSReferenciaTecnicaNom ;
      private bool[] T001717_n269ContagemItem_FSReferenciaTecnicaNom ;
      private decimal[] T001717_A227ContagemItem_FSReferenciaTecnicaVal ;
      private bool[] T001717_n227ContagemItem_FSReferenciaTecnicaVal ;
      private int[] T001717_A234ContagemItem_FSQTDInm ;
      private bool[] T001717_n234ContagemItem_FSQTDInm ;
      private String[] T001717_A235ContagemItem_FSCP ;
      private bool[] T001717_n235ContagemItem_FSCP ;
      private decimal[] T001717_A236ContagemItem_FSPontoFuncaoBruto ;
      private bool[] T001717_n236ContagemItem_FSPontoFuncaoBruto ;
      private decimal[] T001717_A237ContagemItem_FSPontoFuncaoLiquido ;
      private bool[] T001717_n237ContagemItem_FSPontoFuncaoLiquido ;
      private String[] T001717_A238ContagemItem_FSEvidencias ;
      private bool[] T001717_n238ContagemItem_FSEvidencias ;
      private String[] T001717_A257ContagemItem_FMReferenciaTecnicaNom ;
      private bool[] T001717_n257ContagemItem_FMReferenciaTecnicaNom ;
      private decimal[] T001717_A256ContagemItem_FMReferenciaTecnicaVal ;
      private bool[] T001717_n256ContagemItem_FMReferenciaTecnicaVal ;
      private int[] T001717_A239ContagemItem_FMQTDInm ;
      private bool[] T001717_n239ContagemItem_FMQTDInm ;
      private String[] T001717_A240ContagemItem_FMCP ;
      private bool[] T001717_n240ContagemItem_FMCP ;
      private decimal[] T001717_A241ContagemItem_FMPontoFuncaoBruto ;
      private bool[] T001717_n241ContagemItem_FMPontoFuncaoBruto ;
      private decimal[] T001717_A242ContagemItem_FMPontoFuncaoLiquido ;
      private bool[] T001717_n242ContagemItem_FMPontoFuncaoLiquido ;
      private int[] T001717_A165FuncaoAPF_Codigo ;
      private bool[] T001717_n165FuncaoAPF_Codigo ;
      private int[] T001717_A226ContagemItem_FSReferenciaTecnicaCod ;
      private bool[] T001717_n226ContagemItem_FSReferenciaTecnicaCod ;
      private int[] T001717_A255ContagemItem_FMReferenciaTecnicaCod ;
      private bool[] T001717_n255ContagemItem_FMReferenciaTecnicaCod ;
      private int[] T001717_A193Contagem_AreaTrabalhoCod ;
      private int[] T001717_A213Contagem_UsuarioContadorCod ;
      private bool[] T001717_n213Contagem_UsuarioContadorCod ;
      private int[] T001717_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] T001717_n214Contagem_UsuarioContadorPessoaCod ;
      private short[] T001717_A232ContagemItem_FSDER ;
      private bool[] T001717_n232ContagemItem_FSDER ;
      private short[] T001717_A251ContagemItem_FMDER ;
      private bool[] T001717_n251ContagemItem_FMDER ;
      private short[] T001719_A251ContagemItem_FMDER ;
      private bool[] T001719_n251ContagemItem_FMDER ;
      private String[] T001720_A166FuncaoAPF_Nome ;
      private String[] T001720_A184FuncaoAPF_Tipo ;
      private String[] T001721_A269ContagemItem_FSReferenciaTecnicaNom ;
      private bool[] T001721_n269ContagemItem_FSReferenciaTecnicaNom ;
      private decimal[] T001721_A227ContagemItem_FSReferenciaTecnicaVal ;
      private bool[] T001721_n227ContagemItem_FSReferenciaTecnicaVal ;
      private String[] T001722_A257ContagemItem_FMReferenciaTecnicaNom ;
      private bool[] T001722_n257ContagemItem_FMReferenciaTecnicaNom ;
      private decimal[] T001722_A256ContagemItem_FMReferenciaTecnicaVal ;
      private bool[] T001722_n256ContagemItem_FMReferenciaTecnicaVal ;
      private int[] T001723_A224ContagemItem_Lancamento ;
      private int[] T00173_A192Contagem_Codigo ;
      private int[] T00173_A224ContagemItem_Lancamento ;
      private short[] T00173_A268ContagemItem_FMValidacao ;
      private bool[] T00173_n268ContagemItem_FMValidacao ;
      private int[] T00173_A207Contagem_FabricaSoftwareCod ;
      private int[] T00173_A208Contagem_FabricaSoftwarePessoaCod ;
      private String[] T00173_A209Contagem_FabricaSoftwarePessoaNom ;
      private int[] T00173_A216Contagem_UsuarioAuditorCod ;
      private int[] T00173_A217Contagem_UsuarioAuditorPessoaCod ;
      private String[] T00173_A218Contagem_UsuarioAuditorPessoaNom ;
      private short[] T00173_A225ContagemItem_Sequencial ;
      private bool[] T00173_n225ContagemItem_Sequencial ;
      private short[] T00173_A229ContagemItem_Requisito ;
      private bool[] T00173_n229ContagemItem_Requisito ;
      private decimal[] T00173_A950ContagemItem_PFB ;
      private bool[] T00173_n950ContagemItem_PFB ;
      private decimal[] T00173_A951ContagemItem_PFL ;
      private bool[] T00173_n951ContagemItem_PFL ;
      private String[] T00173_A952ContagemItem_TipoUnidade ;
      private String[] T00173_A953ContagemItem_Evidencias ;
      private bool[] T00173_n953ContagemItem_Evidencias ;
      private short[] T00173_A954ContagemItem_QtdINM ;
      private bool[] T00173_n954ContagemItem_QtdINM ;
      private String[] T00173_A955ContagemItem_CP ;
      private bool[] T00173_n955ContagemItem_CP ;
      private short[] T00173_A956ContagemItem_RA ;
      private bool[] T00173_n956ContagemItem_RA ;
      private short[] T00173_A957ContagemItem_DER ;
      private bool[] T00173_n957ContagemItem_DER ;
      private String[] T00173_A958ContagemItem_Funcao ;
      private bool[] T00173_n958ContagemItem_Funcao ;
      private String[] T00173_A228ContagemItem_FSTipoUnidade ;
      private bool[] T00173_n228ContagemItem_FSTipoUnidade ;
      private short[] T00173_A267ContagemItem_FSValidacao ;
      private bool[] T00173_n267ContagemItem_FSValidacao ;
      private int[] T00173_A234ContagemItem_FSQTDInm ;
      private bool[] T00173_n234ContagemItem_FSQTDInm ;
      private String[] T00173_A235ContagemItem_FSCP ;
      private bool[] T00173_n235ContagemItem_FSCP ;
      private decimal[] T00173_A236ContagemItem_FSPontoFuncaoBruto ;
      private bool[] T00173_n236ContagemItem_FSPontoFuncaoBruto ;
      private decimal[] T00173_A237ContagemItem_FSPontoFuncaoLiquido ;
      private bool[] T00173_n237ContagemItem_FSPontoFuncaoLiquido ;
      private String[] T00173_A238ContagemItem_FSEvidencias ;
      private bool[] T00173_n238ContagemItem_FSEvidencias ;
      private int[] T00173_A239ContagemItem_FMQTDInm ;
      private bool[] T00173_n239ContagemItem_FMQTDInm ;
      private String[] T00173_A240ContagemItem_FMCP ;
      private bool[] T00173_n240ContagemItem_FMCP ;
      private decimal[] T00173_A241ContagemItem_FMPontoFuncaoBruto ;
      private bool[] T00173_n241ContagemItem_FMPontoFuncaoBruto ;
      private decimal[] T00173_A242ContagemItem_FMPontoFuncaoLiquido ;
      private bool[] T00173_n242ContagemItem_FMPontoFuncaoLiquido ;
      private int[] T00173_A165FuncaoAPF_Codigo ;
      private bool[] T00173_n165FuncaoAPF_Codigo ;
      private int[] T00173_A226ContagemItem_FSReferenciaTecnicaCod ;
      private bool[] T00173_n226ContagemItem_FSReferenciaTecnicaCod ;
      private int[] T00173_A255ContagemItem_FMReferenciaTecnicaCod ;
      private bool[] T00173_n255ContagemItem_FMReferenciaTecnicaCod ;
      private int[] T001724_A224ContagemItem_Lancamento ;
      private int[] T001724_A192Contagem_Codigo ;
      private int[] T001725_A224ContagemItem_Lancamento ;
      private int[] T001725_A192Contagem_Codigo ;
      private int[] T00172_A192Contagem_Codigo ;
      private int[] T00172_A224ContagemItem_Lancamento ;
      private short[] T00172_A268ContagemItem_FMValidacao ;
      private bool[] T00172_n268ContagemItem_FMValidacao ;
      private int[] T00172_A207Contagem_FabricaSoftwareCod ;
      private int[] T00172_A208Contagem_FabricaSoftwarePessoaCod ;
      private String[] T00172_A209Contagem_FabricaSoftwarePessoaNom ;
      private int[] T00172_A216Contagem_UsuarioAuditorCod ;
      private int[] T00172_A217Contagem_UsuarioAuditorPessoaCod ;
      private String[] T00172_A218Contagem_UsuarioAuditorPessoaNom ;
      private short[] T00172_A225ContagemItem_Sequencial ;
      private bool[] T00172_n225ContagemItem_Sequencial ;
      private short[] T00172_A229ContagemItem_Requisito ;
      private bool[] T00172_n229ContagemItem_Requisito ;
      private decimal[] T00172_A950ContagemItem_PFB ;
      private bool[] T00172_n950ContagemItem_PFB ;
      private decimal[] T00172_A951ContagemItem_PFL ;
      private bool[] T00172_n951ContagemItem_PFL ;
      private String[] T00172_A952ContagemItem_TipoUnidade ;
      private String[] T00172_A953ContagemItem_Evidencias ;
      private bool[] T00172_n953ContagemItem_Evidencias ;
      private short[] T00172_A954ContagemItem_QtdINM ;
      private bool[] T00172_n954ContagemItem_QtdINM ;
      private String[] T00172_A955ContagemItem_CP ;
      private bool[] T00172_n955ContagemItem_CP ;
      private short[] T00172_A956ContagemItem_RA ;
      private bool[] T00172_n956ContagemItem_RA ;
      private short[] T00172_A957ContagemItem_DER ;
      private bool[] T00172_n957ContagemItem_DER ;
      private String[] T00172_A958ContagemItem_Funcao ;
      private bool[] T00172_n958ContagemItem_Funcao ;
      private String[] T00172_A228ContagemItem_FSTipoUnidade ;
      private bool[] T00172_n228ContagemItem_FSTipoUnidade ;
      private short[] T00172_A267ContagemItem_FSValidacao ;
      private bool[] T00172_n267ContagemItem_FSValidacao ;
      private int[] T00172_A234ContagemItem_FSQTDInm ;
      private bool[] T00172_n234ContagemItem_FSQTDInm ;
      private String[] T00172_A235ContagemItem_FSCP ;
      private bool[] T00172_n235ContagemItem_FSCP ;
      private decimal[] T00172_A236ContagemItem_FSPontoFuncaoBruto ;
      private bool[] T00172_n236ContagemItem_FSPontoFuncaoBruto ;
      private decimal[] T00172_A237ContagemItem_FSPontoFuncaoLiquido ;
      private bool[] T00172_n237ContagemItem_FSPontoFuncaoLiquido ;
      private String[] T00172_A238ContagemItem_FSEvidencias ;
      private bool[] T00172_n238ContagemItem_FSEvidencias ;
      private int[] T00172_A239ContagemItem_FMQTDInm ;
      private bool[] T00172_n239ContagemItem_FMQTDInm ;
      private String[] T00172_A240ContagemItem_FMCP ;
      private bool[] T00172_n240ContagemItem_FMCP ;
      private decimal[] T00172_A241ContagemItem_FMPontoFuncaoBruto ;
      private bool[] T00172_n241ContagemItem_FMPontoFuncaoBruto ;
      private decimal[] T00172_A242ContagemItem_FMPontoFuncaoLiquido ;
      private bool[] T00172_n242ContagemItem_FMPontoFuncaoLiquido ;
      private int[] T00172_A165FuncaoAPF_Codigo ;
      private bool[] T00172_n165FuncaoAPF_Codigo ;
      private int[] T00172_A226ContagemItem_FSReferenciaTecnicaCod ;
      private bool[] T00172_n226ContagemItem_FSReferenciaTecnicaCod ;
      private int[] T00172_A255ContagemItem_FMReferenciaTecnicaCod ;
      private bool[] T00172_n255ContagemItem_FMReferenciaTecnicaCod ;
      private int[] T001726_A224ContagemItem_Lancamento ;
      private short[] T001730_A251ContagemItem_FMDER ;
      private bool[] T001730_n251ContagemItem_FMDER ;
      private String[] T001731_A166FuncaoAPF_Nome ;
      private String[] T001731_A184FuncaoAPF_Tipo ;
      private String[] T001732_A269ContagemItem_FSReferenciaTecnicaNom ;
      private bool[] T001732_n269ContagemItem_FSReferenciaTecnicaNom ;
      private decimal[] T001732_A227ContagemItem_FSReferenciaTecnicaVal ;
      private bool[] T001732_n227ContagemItem_FSReferenciaTecnicaVal ;
      private String[] T001733_A257ContagemItem_FMReferenciaTecnicaNom ;
      private bool[] T001733_n257ContagemItem_FMReferenciaTecnicaNom ;
      private decimal[] T001733_A256ContagemItem_FMReferenciaTecnicaVal ;
      private bool[] T001733_n256ContagemItem_FMReferenciaTecnicaVal ;
      private int[] T001734_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] T001734_A249ContagemItem_FMetricasAtributosCod ;
      private int[] T001735_A243ContagemItemParecer_Codigo ;
      private int[] T001736_A224ContagemItem_Lancamento ;
      private int[] T001736_A379ContagemItem_AtributosCod ;
      private int[] T001737_A224ContagemItem_Lancamento ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV15TrnContextAtt ;
   }

   public class contagemitem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new UpdateCursor(def[20])
         ,new UpdateCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00175 ;
          prmT00175 = new Object[] {
          } ;
          Object[] prmT00176 ;
          prmT00176 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001710 ;
          prmT001710 = new Object[] {
          new Object[] {"@Contagem_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001711 ;
          prmT001711 = new Object[] {
          new Object[] {"@Contagem_UsuarioContadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001712 ;
          prmT001712 = new Object[] {
          new Object[] {"@Contagem_UsuarioContadorPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001717 ;
          prmT001717 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT001717 ;
          cmdBufferT001717=" SELECT TM1.[Contagem_Codigo], TM1.[ContagemItem_Lancamento], TM1.[ContagemItem_FMValidacao], T2.[Contagem_DataCriacao], T3.[AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes, T2.[Contagem_Tecnica], T2.[Contagem_Tipo], TM1.[Contagem_FabricaSoftwareCod], TM1.[Contagem_FabricaSoftwarePessoaCod], TM1.[Contagem_FabricaSoftwarePessoaNom], T5.[Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom, TM1.[Contagem_UsuarioAuditorCod], TM1.[Contagem_UsuarioAuditorPessoaCod], TM1.[Contagem_UsuarioAuditorPessoaNom], TM1.[ContagemItem_Sequencial], TM1.[ContagemItem_Requisito], TM1.[ContagemItem_PFB], TM1.[ContagemItem_PFL], T8.[FuncaoAPF_Nome], T8.[FuncaoAPF_Tipo], TM1.[ContagemItem_TipoUnidade], TM1.[ContagemItem_Evidencias], TM1.[ContagemItem_QtdINM], TM1.[ContagemItem_CP], TM1.[ContagemItem_RA], TM1.[ContagemItem_DER], TM1.[ContagemItem_Funcao], TM1.[ContagemItem_FSTipoUnidade], TM1.[ContagemItem_FSValidacao], T9.[ReferenciaTecnica_Nome] AS ContagemItem_FSReferenciaTecnicaNom, T9.[ReferenciaTecnica_Valor] AS ContagemItem_FSReferenciaTecnicaVal, TM1.[ContagemItem_FSQTDInm], TM1.[ContagemItem_FSCP], TM1.[ContagemItem_FSPontoFuncaoBruto], TM1.[ContagemItem_FSPontoFuncaoLiquido], TM1.[ContagemItem_FSEvidencias], T10.[ReferenciaTecnica_Nome] AS ContagemItem_FMReferenciaTecni, T10.[ReferenciaTecnica_Valor] AS ContagemItem_FMReferenciaTecni, TM1.[ContagemItem_FMQTDInm], TM1.[ContagemItem_FMCP], TM1.[ContagemItem_FMPontoFuncaoBruto], TM1.[ContagemItem_FMPontoFuncaoLiquido], TM1.[FuncaoAPF_Codigo], TM1.[ContagemItem_FSReferenciaTecnicaCod] AS ContagemItem_FSReferenciaTecnicaCod, TM1.[ContagemItem_FMReferenciaTecnicaCod] AS ContagemItem_FMReferenciaTecni, T2.[Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, T2.[Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, T4.[Usuario_PessoaCod] "
          + " AS Contagem_UsuarioContadorPessoaCod, COALESCE( T6.[ContagemItem_FSDER], 0) AS ContagemItem_FSDER, COALESCE( T7.[ContagemItem_FMDER], 0) AS ContagemItem_FMDER FROM (((((((([ContagemItem] TM1 WITH (NOLOCK) INNER JOIN [Contagem] T2 WITH (NOLOCK) ON T2.[Contagem_Codigo] = TM1.[Contagem_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contagem_AreaTrabalhoCod]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T2.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) LEFT JOIN (SELECT COUNT(*) AS ContagemItem_FMDER, [ContagemItemAtributosFMetrica_ItemContagemLan] FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) GROUP BY [ContagemItemAtributosFMetrica_ItemContagemLan] ) T7 ON T7.[ContagemItemAtributosFMetrica_ItemContagemLan] = TM1.[ContagemItem_Lancamento]) LEFT JOIN [FuncoesAPF] T8 WITH (NOLOCK) ON T8.[FuncaoAPF_Codigo] = TM1.[FuncaoAPF_Codigo]) LEFT JOIN [ReferenciaTecnica] T9 WITH (NOLOCK) ON T9.[ReferenciaTecnica_Codigo] = TM1.[ContagemItem_FSReferenciaTecnicaCod]) LEFT JOIN [ReferenciaTecnica] T10 WITH (NOLOCK) ON T10.[ReferenciaTecnica_Codigo] = TM1.[ContagemItem_FMReferenciaTecnicaCod]),  (SELECT COUNT(*) AS ContagemItem_FSDER FROM [Atributos] WITH (NOLOCK) ) T6 WHERE TM1.[ContagemItem_Lancamento] = @ContagemItem_Lancamento and TM1.[Contagem_Codigo] = @Contagem_Codigo ORDER BY TM1.[ContagemItem_Lancamento]  OPTION (FAST 100)" ;
          Object[] prmT001714 ;
          prmT001714 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00177 ;
          prmT00177 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00178 ;
          prmT00178 = new Object[] {
          new Object[] {"@ContagemItem_FSReferenciaTecnicaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00179 ;
          prmT00179 = new Object[] {
          new Object[] {"@ContagemItem_FMReferenciaTecnicaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001719 ;
          prmT001719 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001720 ;
          prmT001720 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001721 ;
          prmT001721 = new Object[] {
          new Object[] {"@ContagemItem_FSReferenciaTecnicaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001722 ;
          prmT001722 = new Object[] {
          new Object[] {"@ContagemItem_FMReferenciaTecnicaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001723 ;
          prmT001723 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00173 ;
          prmT00173 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001724 ;
          prmT001724 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001725 ;
          prmT001725 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00172 ;
          prmT00172 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001726 ;
          prmT001726 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMValidacao",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@Contagem_FabricaSoftwareCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_FabricaSoftwarePessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_FabricaSoftwarePessoaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@Contagem_UsuarioAuditorCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_UsuarioAuditorPessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_UsuarioAuditorPessoaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemItem_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContagemItem_Requisito",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContagemItem_PFB",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemItem_PFL",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemItem_TipoUnidade",SqlDbType.VarChar,3,0} ,
          new Object[] {"@ContagemItem_Evidencias",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@ContagemItem_QtdINM",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemItem_CP",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemItem_RA",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContagemItem_DER",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContagemItem_Funcao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemItem_FSTipoUnidade",SqlDbType.VarChar,3,0} ,
          new Object[] {"@ContagemItem_FSValidacao",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@ContagemItem_FSQTDInm",SqlDbType.Int,9,0} ,
          new Object[] {"@ContagemItem_FSCP",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemItem_FSPontoFuncaoBruto",SqlDbType.Decimal,7,2} ,
          new Object[] {"@ContagemItem_FSPontoFuncaoLiquido",SqlDbType.Decimal,7,2} ,
          new Object[] {"@ContagemItem_FSEvidencias",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@ContagemItem_FMQTDInm",SqlDbType.Int,9,0} ,
          new Object[] {"@ContagemItem_FMCP",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemItem_FMPontoFuncaoBruto",SqlDbType.Decimal,7,2} ,
          new Object[] {"@ContagemItem_FMPontoFuncaoLiquido",SqlDbType.Decimal,7,2} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FSReferenciaTecnicaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMReferenciaTecnicaCod",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT001726 ;
          cmdBufferT001726=" INSERT INTO [ContagemItem]([Contagem_Codigo], [ContagemItem_FMValidacao], [Contagem_FabricaSoftwareCod], [Contagem_FabricaSoftwarePessoaCod], [Contagem_FabricaSoftwarePessoaNom], [Contagem_UsuarioAuditorCod], [Contagem_UsuarioAuditorPessoaCod], [Contagem_UsuarioAuditorPessoaNom], [ContagemItem_Sequencial], [ContagemItem_Requisito], [ContagemItem_PFB], [ContagemItem_PFL], [ContagemItem_TipoUnidade], [ContagemItem_Evidencias], [ContagemItem_QtdINM], [ContagemItem_CP], [ContagemItem_RA], [ContagemItem_DER], [ContagemItem_Funcao], [ContagemItem_FSTipoUnidade], [ContagemItem_FSValidacao], [ContagemItem_FSQTDInm], [ContagemItem_FSCP], [ContagemItem_FSPontoFuncaoBruto], [ContagemItem_FSPontoFuncaoLiquido], [ContagemItem_FSEvidencias], [ContagemItem_FMQTDInm], [ContagemItem_FMCP], [ContagemItem_FMPontoFuncaoBruto], [ContagemItem_FMPontoFuncaoLiquido], [FuncaoAPF_Codigo], [ContagemItem_FSReferenciaTecnicaCod], [ContagemItem_FMReferenciaTecnicaCod]) VALUES(@Contagem_Codigo, @ContagemItem_FMValidacao, @Contagem_FabricaSoftwareCod, @Contagem_FabricaSoftwarePessoaCod, @Contagem_FabricaSoftwarePessoaNom, @Contagem_UsuarioAuditorCod, @Contagem_UsuarioAuditorPessoaCod, @Contagem_UsuarioAuditorPessoaNom, @ContagemItem_Sequencial, @ContagemItem_Requisito, @ContagemItem_PFB, @ContagemItem_PFL, @ContagemItem_TipoUnidade, @ContagemItem_Evidencias, @ContagemItem_QtdINM, @ContagemItem_CP, @ContagemItem_RA, @ContagemItem_DER, @ContagemItem_Funcao, @ContagemItem_FSTipoUnidade, @ContagemItem_FSValidacao, @ContagemItem_FSQTDInm, @ContagemItem_FSCP, @ContagemItem_FSPontoFuncaoBruto, @ContagemItem_FSPontoFuncaoLiquido, @ContagemItem_FSEvidencias, @ContagemItem_FMQTDInm, @ContagemItem_FMCP, @ContagemItem_FMPontoFuncaoBruto, @ContagemItem_FMPontoFuncaoLiquido, @FuncaoAPF_Codigo, @ContagemItem_FSReferenciaTecnicaCod, "
          + " @ContagemItem_FMReferenciaTecnicaCod); SELECT SCOPE_IDENTITY()" ;
          Object[] prmT001727 ;
          prmT001727 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMValidacao",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@Contagem_FabricaSoftwareCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_FabricaSoftwarePessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_FabricaSoftwarePessoaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@Contagem_UsuarioAuditorCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_UsuarioAuditorPessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_UsuarioAuditorPessoaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemItem_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContagemItem_Requisito",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContagemItem_PFB",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemItem_PFL",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemItem_TipoUnidade",SqlDbType.VarChar,3,0} ,
          new Object[] {"@ContagemItem_Evidencias",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@ContagemItem_QtdINM",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemItem_CP",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemItem_RA",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContagemItem_DER",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContagemItem_Funcao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemItem_FSTipoUnidade",SqlDbType.VarChar,3,0} ,
          new Object[] {"@ContagemItem_FSValidacao",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@ContagemItem_FSQTDInm",SqlDbType.Int,9,0} ,
          new Object[] {"@ContagemItem_FSCP",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemItem_FSPontoFuncaoBruto",SqlDbType.Decimal,7,2} ,
          new Object[] {"@ContagemItem_FSPontoFuncaoLiquido",SqlDbType.Decimal,7,2} ,
          new Object[] {"@ContagemItem_FSEvidencias",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@ContagemItem_FMQTDInm",SqlDbType.Int,9,0} ,
          new Object[] {"@ContagemItem_FMCP",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemItem_FMPontoFuncaoBruto",SqlDbType.Decimal,7,2} ,
          new Object[] {"@ContagemItem_FMPontoFuncaoLiquido",SqlDbType.Decimal,7,2} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FSReferenciaTecnicaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMReferenciaTecnicaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT001727 ;
          cmdBufferT001727=" UPDATE [ContagemItem] SET [Contagem_Codigo]=@Contagem_Codigo, [ContagemItem_FMValidacao]=@ContagemItem_FMValidacao, [Contagem_FabricaSoftwareCod]=@Contagem_FabricaSoftwareCod, [Contagem_FabricaSoftwarePessoaCod]=@Contagem_FabricaSoftwarePessoaCod, [Contagem_FabricaSoftwarePessoaNom]=@Contagem_FabricaSoftwarePessoaNom, [Contagem_UsuarioAuditorCod]=@Contagem_UsuarioAuditorCod, [Contagem_UsuarioAuditorPessoaCod]=@Contagem_UsuarioAuditorPessoaCod, [Contagem_UsuarioAuditorPessoaNom]=@Contagem_UsuarioAuditorPessoaNom, [ContagemItem_Sequencial]=@ContagemItem_Sequencial, [ContagemItem_Requisito]=@ContagemItem_Requisito, [ContagemItem_PFB]=@ContagemItem_PFB, [ContagemItem_PFL]=@ContagemItem_PFL, [ContagemItem_TipoUnidade]=@ContagemItem_TipoUnidade, [ContagemItem_Evidencias]=@ContagemItem_Evidencias, [ContagemItem_QtdINM]=@ContagemItem_QtdINM, [ContagemItem_CP]=@ContagemItem_CP, [ContagemItem_RA]=@ContagemItem_RA, [ContagemItem_DER]=@ContagemItem_DER, [ContagemItem_Funcao]=@ContagemItem_Funcao, [ContagemItem_FSTipoUnidade]=@ContagemItem_FSTipoUnidade, [ContagemItem_FSValidacao]=@ContagemItem_FSValidacao, [ContagemItem_FSQTDInm]=@ContagemItem_FSQTDInm, [ContagemItem_FSCP]=@ContagemItem_FSCP, [ContagemItem_FSPontoFuncaoBruto]=@ContagemItem_FSPontoFuncaoBruto, [ContagemItem_FSPontoFuncaoLiquido]=@ContagemItem_FSPontoFuncaoLiquido, [ContagemItem_FSEvidencias]=@ContagemItem_FSEvidencias, [ContagemItem_FMQTDInm]=@ContagemItem_FMQTDInm, [ContagemItem_FMCP]=@ContagemItem_FMCP, [ContagemItem_FMPontoFuncaoBruto]=@ContagemItem_FMPontoFuncaoBruto, [ContagemItem_FMPontoFuncaoLiquido]=@ContagemItem_FMPontoFuncaoLiquido, [FuncaoAPF_Codigo]=@FuncaoAPF_Codigo, [ContagemItem_FSReferenciaTecnicaCod]=@ContagemItem_FSReferenciaTecnicaCod, [ContagemItem_FMReferenciaTecnicaCod]=@ContagemItem_FMReferenciaTecnicaCod "
          + "  WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento" ;
          Object[] prmT001728 ;
          prmT001728 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001734 ;
          prmT001734 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001735 ;
          prmT001735 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001736 ;
          prmT001736 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001737 ;
          prmT001737 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001730 ;
          prmT001730 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001731 ;
          prmT001731 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001732 ;
          prmT001732 = new Object[] {
          new Object[] {"@ContagemItem_FSReferenciaTecnicaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001733 ;
          prmT001733 = new Object[] {
          new Object[] {"@ContagemItem_FMReferenciaTecnicaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00172", "SELECT [Contagem_Codigo], [ContagemItem_Lancamento], [ContagemItem_FMValidacao], [Contagem_FabricaSoftwareCod], [Contagem_FabricaSoftwarePessoaCod], [Contagem_FabricaSoftwarePessoaNom], [Contagem_UsuarioAuditorCod], [Contagem_UsuarioAuditorPessoaCod], [Contagem_UsuarioAuditorPessoaNom], [ContagemItem_Sequencial], [ContagemItem_Requisito], [ContagemItem_PFB], [ContagemItem_PFL], [ContagemItem_TipoUnidade], [ContagemItem_Evidencias], [ContagemItem_QtdINM], [ContagemItem_CP], [ContagemItem_RA], [ContagemItem_DER], [ContagemItem_Funcao], [ContagemItem_FSTipoUnidade], [ContagemItem_FSValidacao], [ContagemItem_FSQTDInm], [ContagemItem_FSCP], [ContagemItem_FSPontoFuncaoBruto], [ContagemItem_FSPontoFuncaoLiquido], [ContagemItem_FSEvidencias], [ContagemItem_FMQTDInm], [ContagemItem_FMCP], [ContagemItem_FMPontoFuncaoBruto], [ContagemItem_FMPontoFuncaoLiquido], [FuncaoAPF_Codigo], [ContagemItem_FSReferenciaTecnicaCod] AS ContagemItem_FSReferenciaTecnicaCod, [ContagemItem_FMReferenciaTecnicaCod] AS ContagemItem_FMReferenciaTecni FROM [ContagemItem] WITH (UPDLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmT00172,1,0,true,false )
             ,new CursorDef("T00173", "SELECT [Contagem_Codigo], [ContagemItem_Lancamento], [ContagemItem_FMValidacao], [Contagem_FabricaSoftwareCod], [Contagem_FabricaSoftwarePessoaCod], [Contagem_FabricaSoftwarePessoaNom], [Contagem_UsuarioAuditorCod], [Contagem_UsuarioAuditorPessoaCod], [Contagem_UsuarioAuditorPessoaNom], [ContagemItem_Sequencial], [ContagemItem_Requisito], [ContagemItem_PFB], [ContagemItem_PFL], [ContagemItem_TipoUnidade], [ContagemItem_Evidencias], [ContagemItem_QtdINM], [ContagemItem_CP], [ContagemItem_RA], [ContagemItem_DER], [ContagemItem_Funcao], [ContagemItem_FSTipoUnidade], [ContagemItem_FSValidacao], [ContagemItem_FSQTDInm], [ContagemItem_FSCP], [ContagemItem_FSPontoFuncaoBruto], [ContagemItem_FSPontoFuncaoLiquido], [ContagemItem_FSEvidencias], [ContagemItem_FMQTDInm], [ContagemItem_FMCP], [ContagemItem_FMPontoFuncaoBruto], [ContagemItem_FMPontoFuncaoLiquido], [FuncaoAPF_Codigo], [ContagemItem_FSReferenciaTecnicaCod] AS ContagemItem_FSReferenciaTecnicaCod, [ContagemItem_FMReferenciaTecnicaCod] AS ContagemItem_FMReferenciaTecni FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmT00173,1,0,true,false )
             ,new CursorDef("T00175", "SELECT COALESCE( T1.[ContagemItem_FSDER], 0) AS ContagemItem_FSDER FROM (SELECT COUNT(*) AS ContagemItem_FSDER FROM [Atributos] WITH (NOLOCK) ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT00175,1,0,true,false )
             ,new CursorDef("T00176", "SELECT [Contagem_DataCriacao], [Contagem_Tecnica], [Contagem_Tipo], [Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, [Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00176,1,0,true,false )
             ,new CursorDef("T00177", "SELECT [FuncaoAPF_Nome], [FuncaoAPF_Tipo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00177,1,0,true,false )
             ,new CursorDef("T00178", "SELECT [ReferenciaTecnica_Nome] AS ContagemItem_FSReferenciaTecnicaNom, [ReferenciaTecnica_Valor] AS ContagemItem_FSReferenciaTecnicaVal FROM [ReferenciaTecnica] WITH (NOLOCK) WHERE [ReferenciaTecnica_Codigo] = @ContagemItem_FSReferenciaTecnicaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00178,1,0,true,false )
             ,new CursorDef("T00179", "SELECT [ReferenciaTecnica_Nome] AS ContagemItem_FMReferenciaTecni, [ReferenciaTecnica_Valor] AS ContagemItem_FMReferenciaTecni FROM [ReferenciaTecnica] WITH (NOLOCK) WHERE [ReferenciaTecnica_Codigo] = @ContagemItem_FMReferenciaTecnicaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00179,1,0,true,false )
             ,new CursorDef("T001710", "SELECT [AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contagem_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001710,1,0,true,false )
             ,new CursorDef("T001711", "SELECT [Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Contagem_UsuarioContadorCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001711,1,0,true,false )
             ,new CursorDef("T001712", "SELECT [Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contagem_UsuarioContadorPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001712,1,0,true,false )
             ,new CursorDef("T001714", "SELECT COALESCE( T1.[ContagemItem_FMDER], 0) AS ContagemItem_FMDER FROM (SELECT COUNT(*) AS ContagemItem_FMDER, [ContagemItemAtributosFMetrica_ItemContagemLan] FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) GROUP BY [ContagemItemAtributosFMetrica_ItemContagemLan] ) T1 WHERE T1.[ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmT001714,1,0,true,false )
             ,new CursorDef("T001717", cmdBufferT001717,true, GxErrorMask.GX_NOMASK, false, this,prmT001717,100,0,true,false )
             ,new CursorDef("T001719", "SELECT COALESCE( T1.[ContagemItem_FMDER], 0) AS ContagemItem_FMDER FROM (SELECT COUNT(*) AS ContagemItem_FMDER, [ContagemItemAtributosFMetrica_ItemContagemLan] FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) GROUP BY [ContagemItemAtributosFMetrica_ItemContagemLan] ) T1 WHERE T1.[ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmT001719,1,0,true,false )
             ,new CursorDef("T001720", "SELECT [FuncaoAPF_Nome], [FuncaoAPF_Tipo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001720,1,0,true,false )
             ,new CursorDef("T001721", "SELECT [ReferenciaTecnica_Nome] AS ContagemItem_FSReferenciaTecnicaNom, [ReferenciaTecnica_Valor] AS ContagemItem_FSReferenciaTecnicaVal FROM [ReferenciaTecnica] WITH (NOLOCK) WHERE [ReferenciaTecnica_Codigo] = @ContagemItem_FSReferenciaTecnicaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001721,1,0,true,false )
             ,new CursorDef("T001722", "SELECT [ReferenciaTecnica_Nome] AS ContagemItem_FMReferenciaTecni, [ReferenciaTecnica_Valor] AS ContagemItem_FMReferenciaTecni FROM [ReferenciaTecnica] WITH (NOLOCK) WHERE [ReferenciaTecnica_Codigo] = @ContagemItem_FMReferenciaTecnicaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001722,1,0,true,false )
             ,new CursorDef("T001723", "SELECT [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001723,1,0,true,false )
             ,new CursorDef("T001724", "SELECT TOP 1 [ContagemItem_Lancamento], [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) WHERE ( [ContagemItem_Lancamento] > @ContagemItem_Lancamento) and [Contagem_Codigo] = @Contagem_Codigo ORDER BY [ContagemItem_Lancamento]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001724,1,0,true,true )
             ,new CursorDef("T001725", "SELECT TOP 1 [ContagemItem_Lancamento], [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) WHERE ( [ContagemItem_Lancamento] < @ContagemItem_Lancamento) and [Contagem_Codigo] = @Contagem_Codigo ORDER BY [ContagemItem_Lancamento] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001725,1,0,true,true )
             ,new CursorDef("T001726", cmdBufferT001726, GxErrorMask.GX_NOMASK,prmT001726)
             ,new CursorDef("T001727", cmdBufferT001727, GxErrorMask.GX_NOMASK,prmT001727)
             ,new CursorDef("T001728", "DELETE FROM [ContagemItem]  WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento", GxErrorMask.GX_NOMASK,prmT001728)
             ,new CursorDef("T001730", "SELECT COALESCE( T1.[ContagemItem_FMDER], 0) AS ContagemItem_FMDER FROM (SELECT COUNT(*) AS ContagemItem_FMDER, [ContagemItemAtributosFMetrica_ItemContagemLan] FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) GROUP BY [ContagemItemAtributosFMetrica_ItemContagemLan] ) T1 WHERE T1.[ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmT001730,1,0,true,false )
             ,new CursorDef("T001731", "SELECT [FuncaoAPF_Nome], [FuncaoAPF_Tipo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001731,1,0,true,false )
             ,new CursorDef("T001732", "SELECT [ReferenciaTecnica_Nome] AS ContagemItem_FSReferenciaTecnicaNom, [ReferenciaTecnica_Valor] AS ContagemItem_FSReferenciaTecnicaVal FROM [ReferenciaTecnica] WITH (NOLOCK) WHERE [ReferenciaTecnica_Codigo] = @ContagemItem_FSReferenciaTecnicaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001732,1,0,true,false )
             ,new CursorDef("T001733", "SELECT [ReferenciaTecnica_Nome] AS ContagemItem_FMReferenciaTecni, [ReferenciaTecnica_Valor] AS ContagemItem_FMReferenciaTecni FROM [ReferenciaTecnica] WITH (NOLOCK) WHERE [ReferenciaTecnica_Codigo] = @ContagemItem_FMReferenciaTecnicaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001733,1,0,true,false )
             ,new CursorDef("T001734", "SELECT TOP 1 [ContagemItemAtributosFMetrica_ItemContagemLan], [ContagemItem_FMetricasAtributosCod] FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) WHERE [ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmT001734,1,0,true,true )
             ,new CursorDef("T001735", "SELECT TOP 1 [ContagemItemParecer_Codigo] FROM [ContagemItemParecer] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmT001735,1,0,true,true )
             ,new CursorDef("T001736", "SELECT TOP 1 [ContagemItem_Lancamento], [ContagemItem_AtributosCod] FROM [ContagemItemAtributosFSoftware] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmT001736,1,0,true,true )
             ,new CursorDef("T001737", "SELECT [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [Contagem_Codigo] = @Contagem_Codigo ORDER BY [ContagemItem_Lancamento]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001737,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((String[]) buf[9])[0] = rslt.getString(9, 50) ;
                ((short[]) buf[10])[0] = rslt.getShort(10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(10);
                ((short[]) buf[12])[0] = rslt.getShort(11) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(11);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(12);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(13);
                ((String[]) buf[18])[0] = rslt.getVarchar(14) ;
                ((String[]) buf[19])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(15);
                ((short[]) buf[21])[0] = rslt.getShort(16) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(16);
                ((String[]) buf[23])[0] = rslt.getString(17, 1) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(17);
                ((short[]) buf[25])[0] = rslt.getShort(18) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(18);
                ((short[]) buf[27])[0] = rslt.getShort(19) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(19);
                ((String[]) buf[29])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(20);
                ((String[]) buf[31])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(21);
                ((short[]) buf[33])[0] = rslt.getShort(22) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((String[]) buf[37])[0] = rslt.getString(24, 1) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(24);
                ((decimal[]) buf[39])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(25);
                ((decimal[]) buf[41])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(26);
                ((String[]) buf[43])[0] = rslt.getLongVarchar(27) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(27);
                ((int[]) buf[45])[0] = rslt.getInt(28) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(28);
                ((String[]) buf[47])[0] = rslt.getString(29, 1) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(29);
                ((decimal[]) buf[49])[0] = rslt.getDecimal(30) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(30);
                ((decimal[]) buf[51])[0] = rslt.getDecimal(31) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(31);
                ((int[]) buf[53])[0] = rslt.getInt(32) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(32);
                ((int[]) buf[55])[0] = rslt.getInt(33) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(33);
                ((int[]) buf[57])[0] = rslt.getInt(34) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(34);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((String[]) buf[9])[0] = rslt.getString(9, 50) ;
                ((short[]) buf[10])[0] = rslt.getShort(10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(10);
                ((short[]) buf[12])[0] = rslt.getShort(11) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(11);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(12);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(13);
                ((String[]) buf[18])[0] = rslt.getVarchar(14) ;
                ((String[]) buf[19])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(15);
                ((short[]) buf[21])[0] = rslt.getShort(16) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(16);
                ((String[]) buf[23])[0] = rslt.getString(17, 1) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(17);
                ((short[]) buf[25])[0] = rslt.getShort(18) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(18);
                ((short[]) buf[27])[0] = rslt.getShort(19) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(19);
                ((String[]) buf[29])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(20);
                ((String[]) buf[31])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(21);
                ((short[]) buf[33])[0] = rslt.getShort(22) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((String[]) buf[37])[0] = rslt.getString(24, 1) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(24);
                ((decimal[]) buf[39])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(25);
                ((decimal[]) buf[41])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(26);
                ((String[]) buf[43])[0] = rslt.getLongVarchar(27) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(27);
                ((int[]) buf[45])[0] = rslt.getInt(28) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(28);
                ((String[]) buf[47])[0] = rslt.getString(29, 1) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(29);
                ((decimal[]) buf[49])[0] = rslt.getDecimal(30) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(30);
                ((decimal[]) buf[51])[0] = rslt.getDecimal(31) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(31);
                ((int[]) buf[53])[0] = rslt.getInt(32) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(32);
                ((int[]) buf[55])[0] = rslt.getInt(33) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(33);
                ((int[]) buf[57])[0] = rslt.getInt(34) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(34);
                return;
             case 2 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 50) ;
                ((String[]) buf[14])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((int[]) buf[16])[0] = rslt.getInt(12) ;
                ((int[]) buf[17])[0] = rslt.getInt(13) ;
                ((String[]) buf[18])[0] = rslt.getString(14, 50) ;
                ((short[]) buf[19])[0] = rslt.getShort(15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(15);
                ((short[]) buf[21])[0] = rslt.getShort(16) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(16);
                ((decimal[]) buf[23])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(17);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(18);
                ((String[]) buf[27])[0] = rslt.getVarchar(19) ;
                ((String[]) buf[28])[0] = rslt.getString(20, 3) ;
                ((String[]) buf[29])[0] = rslt.getVarchar(21) ;
                ((String[]) buf[30])[0] = rslt.getLongVarchar(22) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(22);
                ((short[]) buf[32])[0] = rslt.getShort(23) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(23);
                ((String[]) buf[34])[0] = rslt.getString(24, 1) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(24);
                ((short[]) buf[36])[0] = rslt.getShort(25) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(25);
                ((short[]) buf[38])[0] = rslt.getShort(26) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(26);
                ((String[]) buf[40])[0] = rslt.getVarchar(27) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(27);
                ((String[]) buf[42])[0] = rslt.getVarchar(28) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(28);
                ((short[]) buf[44])[0] = rslt.getShort(29) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(29);
                ((String[]) buf[46])[0] = rslt.getString(30, 50) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(30);
                ((decimal[]) buf[48])[0] = rslt.getDecimal(31) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(31);
                ((int[]) buf[50])[0] = rslt.getInt(32) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(32);
                ((String[]) buf[52])[0] = rslt.getString(33, 1) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(33);
                ((decimal[]) buf[54])[0] = rslt.getDecimal(34) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(34);
                ((decimal[]) buf[56])[0] = rslt.getDecimal(35) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(35);
                ((String[]) buf[58])[0] = rslt.getLongVarchar(36) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(36);
                ((String[]) buf[60])[0] = rslt.getString(37, 50) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(37);
                ((decimal[]) buf[62])[0] = rslt.getDecimal(38) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(38);
                ((int[]) buf[64])[0] = rslt.getInt(39) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(39);
                ((String[]) buf[66])[0] = rslt.getString(40, 1) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(40);
                ((decimal[]) buf[68])[0] = rslt.getDecimal(41) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(41);
                ((decimal[]) buf[70])[0] = rslt.getDecimal(42) ;
                ((bool[]) buf[71])[0] = rslt.wasNull(42);
                ((int[]) buf[72])[0] = rslt.getInt(43) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(43);
                ((int[]) buf[74])[0] = rslt.getInt(44) ;
                ((bool[]) buf[75])[0] = rslt.wasNull(44);
                ((int[]) buf[76])[0] = rslt.getInt(45) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(45);
                ((int[]) buf[78])[0] = rslt.getInt(46) ;
                ((int[]) buf[79])[0] = rslt.getInt(47) ;
                ((bool[]) buf[80])[0] = rslt.wasNull(47);
                ((int[]) buf[81])[0] = rslt.getInt(48) ;
                ((bool[]) buf[82])[0] = rslt.wasNull(48);
                ((short[]) buf[83])[0] = rslt.getShort(49) ;
                ((bool[]) buf[84])[0] = rslt.wasNull(49);
                ((short[]) buf[85])[0] = rslt.getShort(50) ;
                ((bool[]) buf[86])[0] = rslt.wasNull(50);
                return;
             case 12 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 23 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                return;
             case 24 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                stmt.SetParameter(4, (int)parms[4]);
                stmt.SetParameter(5, (String)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (int)parms[7]);
                stmt.SetParameter(8, (String)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[16]);
                }
                stmt.SetParameter(13, (String)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(15, (short)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 17 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(17, (short)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 18 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(18, (short)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 19 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 20 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 21 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(21, (short)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 23 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(23, (String)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 24 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(24, (decimal)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 25 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(25, (decimal)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 26 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(26, (String)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 28 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(28, (String)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 29 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(29, (decimal)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 30 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(30, (decimal)parms[51]);
                }
                if ( (bool)parms[52] )
                {
                   stmt.setNull( 31 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(31, (int)parms[53]);
                }
                if ( (bool)parms[54] )
                {
                   stmt.setNull( 32 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(32, (int)parms[55]);
                }
                if ( (bool)parms[56] )
                {
                   stmt.setNull( 33 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(33, (int)parms[57]);
                }
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                stmt.SetParameter(4, (int)parms[4]);
                stmt.SetParameter(5, (String)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (int)parms[7]);
                stmt.SetParameter(8, (String)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[16]);
                }
                stmt.SetParameter(13, (String)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(15, (short)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 17 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(17, (short)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 18 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(18, (short)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 19 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 20 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 21 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(21, (short)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 23 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(23, (String)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 24 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(24, (decimal)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 25 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(25, (decimal)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 26 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(26, (String)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 28 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(28, (String)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 29 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(29, (decimal)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 30 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(30, (decimal)parms[51]);
                }
                if ( (bool)parms[52] )
                {
                   stmt.setNull( 31 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(31, (int)parms[53]);
                }
                if ( (bool)parms[54] )
                {
                   stmt.setNull( 32 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(32, (int)parms[55]);
                }
                if ( (bool)parms[56] )
                {
                   stmt.setNull( 33 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(33, (int)parms[57]);
                }
                stmt.SetParameter(34, (int)parms[58]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
