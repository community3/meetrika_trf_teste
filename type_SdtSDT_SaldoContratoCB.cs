/*
               File: type_SdtSDT_SaldoContratoCB
        Description: SDT_SaldoContratoCB
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:8.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_SaldoContratoCB" )]
   [XmlType(TypeName =  "SDT_SaldoContratoCB" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_SaldoContratoCB : GxUserType
   {
      public SdtSDT_SaldoContratoCB( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_descricao = "";
      }

      public SdtSDT_SaldoContratoCB( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_SaldoContratoCB deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_SaldoContratoCB)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_SaldoContratoCB obj ;
         obj = this;
         obj.gxTpr_Saldocontrato_codigo = deserialized.gxTpr_Saldocontrato_codigo;
         obj.gxTpr_Saldocontrato_descricao = deserialized.gxTpr_Saldocontrato_descricao;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "SaldoContrato_Codigo") )
               {
                  gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SaldoContrato_Descricao") )
               {
                  gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_SaldoContratoCB";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("SaldoContrato_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("SaldoContrato_Descricao", StringUtil.RTrim( gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("SaldoContrato_Codigo", gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_codigo, false);
         AddObjectProperty("SaldoContrato_Descricao", gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_descricao, false);
         return  ;
      }

      [  SoapElement( ElementName = "SaldoContrato_Codigo" )]
      [  XmlElement( ElementName = "SaldoContrato_Codigo"   )]
      public int gxTpr_Saldocontrato_codigo
      {
         get {
            return gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_codigo ;
         }

         set {
            gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "SaldoContrato_Descricao" )]
      [  XmlElement( ElementName = "SaldoContrato_Descricao"   )]
      public String gxTpr_Saldocontrato_descricao
      {
         get {
            return gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_descricao ;
         }

         set {
            gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_descricao = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_descricao = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_codigo ;
      protected String sTagName ;
      protected String gxTv_SdtSDT_SaldoContratoCB_Saldocontrato_descricao ;
   }

   [DataContract(Name = @"SDT_SaldoContratoCB", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_SaldoContratoCB_RESTInterface : GxGenericCollectionItem<SdtSDT_SaldoContratoCB>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_SaldoContratoCB_RESTInterface( ) : base()
      {
      }

      public SdtSDT_SaldoContratoCB_RESTInterface( SdtSDT_SaldoContratoCB psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "SaldoContrato_Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Saldocontrato_codigo
      {
         get {
            return sdt.gxTpr_Saldocontrato_codigo ;
         }

         set {
            sdt.gxTpr_Saldocontrato_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "SaldoContrato_Descricao" , Order = 1 )]
      public String gxTpr_Saldocontrato_descricao
      {
         get {
            return sdt.gxTpr_Saldocontrato_descricao ;
         }

         set {
            sdt.gxTpr_Saldocontrato_descricao = (String)(value);
         }

      }

      public SdtSDT_SaldoContratoCB sdt
      {
         get {
            return (SdtSDT_SaldoContratoCB)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_SaldoContratoCB() ;
         }
      }

   }

}
