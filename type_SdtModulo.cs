/*
               File: type_SdtModulo
        Description: Modulo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:47:36.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Modulo" )]
   [XmlType(TypeName =  "Modulo" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtModulo : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtModulo( )
      {
         /* Constructor for serialization */
         gxTv_SdtModulo_Modulo_nome = "";
         gxTv_SdtModulo_Modulo_sigla = "";
         gxTv_SdtModulo_Modulo_descricao = "";
         gxTv_SdtModulo_Sistema_nome = "";
         gxTv_SdtModulo_Mode = "";
         gxTv_SdtModulo_Modulo_nome_Z = "";
         gxTv_SdtModulo_Modulo_sigla_Z = "";
         gxTv_SdtModulo_Sistema_nome_Z = "";
      }

      public SdtModulo( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV146Modulo_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV146Modulo_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Modulo_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Modulo");
         metadata.Set("BT", "Modulo");
         metadata.Set("PK", "[ \"Modulo_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Modulo_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Sistema_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modulo_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modulo_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modulo_sigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modulo_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modulo_descricao_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtModulo deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtModulo)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtModulo obj ;
         obj = this;
         obj.gxTpr_Modulo_codigo = deserialized.gxTpr_Modulo_codigo;
         obj.gxTpr_Modulo_nome = deserialized.gxTpr_Modulo_nome;
         obj.gxTpr_Modulo_sigla = deserialized.gxTpr_Modulo_sigla;
         obj.gxTpr_Modulo_descricao = deserialized.gxTpr_Modulo_descricao;
         obj.gxTpr_Sistema_codigo = deserialized.gxTpr_Sistema_codigo;
         obj.gxTpr_Sistema_nome = deserialized.gxTpr_Sistema_nome;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Modulo_codigo_Z = deserialized.gxTpr_Modulo_codigo_Z;
         obj.gxTpr_Modulo_nome_Z = deserialized.gxTpr_Modulo_nome_Z;
         obj.gxTpr_Modulo_sigla_Z = deserialized.gxTpr_Modulo_sigla_Z;
         obj.gxTpr_Sistema_codigo_Z = deserialized.gxTpr_Sistema_codigo_Z;
         obj.gxTpr_Sistema_nome_Z = deserialized.gxTpr_Sistema_nome_Z;
         obj.gxTpr_Modulo_codigo_N = deserialized.gxTpr_Modulo_codigo_N;
         obj.gxTpr_Modulo_descricao_N = deserialized.gxTpr_Modulo_descricao_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Codigo") )
               {
                  gxTv_SdtModulo_Modulo_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Nome") )
               {
                  gxTv_SdtModulo_Modulo_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Sigla") )
               {
                  gxTv_SdtModulo_Modulo_sigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Descricao") )
               {
                  gxTv_SdtModulo_Modulo_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Codigo") )
               {
                  gxTv_SdtModulo_Sistema_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Nome") )
               {
                  gxTv_SdtModulo_Sistema_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtModulo_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtModulo_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Codigo_Z") )
               {
                  gxTv_SdtModulo_Modulo_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Nome_Z") )
               {
                  gxTv_SdtModulo_Modulo_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Sigla_Z") )
               {
                  gxTv_SdtModulo_Modulo_sigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Codigo_Z") )
               {
                  gxTv_SdtModulo_Sistema_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Nome_Z") )
               {
                  gxTv_SdtModulo_Sistema_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Codigo_N") )
               {
                  gxTv_SdtModulo_Modulo_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Descricao_N") )
               {
                  gxTv_SdtModulo_Modulo_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Modulo";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Modulo_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtModulo_Modulo_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Modulo_Nome", StringUtil.RTrim( gxTv_SdtModulo_Modulo_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Modulo_Sigla", StringUtil.RTrim( gxTv_SdtModulo_Modulo_sigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Modulo_Descricao", StringUtil.RTrim( gxTv_SdtModulo_Modulo_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtModulo_Sistema_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Nome", StringUtil.RTrim( gxTv_SdtModulo_Sistema_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtModulo_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtModulo_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Modulo_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtModulo_Modulo_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Modulo_Nome_Z", StringUtil.RTrim( gxTv_SdtModulo_Modulo_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Modulo_Sigla_Z", StringUtil.RTrim( gxTv_SdtModulo_Modulo_sigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtModulo_Sistema_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Nome_Z", StringUtil.RTrim( gxTv_SdtModulo_Sistema_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Modulo_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtModulo_Modulo_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Modulo_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtModulo_Modulo_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Modulo_Codigo", gxTv_SdtModulo_Modulo_codigo, false);
         AddObjectProperty("Modulo_Nome", gxTv_SdtModulo_Modulo_nome, false);
         AddObjectProperty("Modulo_Sigla", gxTv_SdtModulo_Modulo_sigla, false);
         AddObjectProperty("Modulo_Descricao", gxTv_SdtModulo_Modulo_descricao, false);
         AddObjectProperty("Sistema_Codigo", gxTv_SdtModulo_Sistema_codigo, false);
         AddObjectProperty("Sistema_Nome", gxTv_SdtModulo_Sistema_nome, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtModulo_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtModulo_Initialized, false);
            AddObjectProperty("Modulo_Codigo_Z", gxTv_SdtModulo_Modulo_codigo_Z, false);
            AddObjectProperty("Modulo_Nome_Z", gxTv_SdtModulo_Modulo_nome_Z, false);
            AddObjectProperty("Modulo_Sigla_Z", gxTv_SdtModulo_Modulo_sigla_Z, false);
            AddObjectProperty("Sistema_Codigo_Z", gxTv_SdtModulo_Sistema_codigo_Z, false);
            AddObjectProperty("Sistema_Nome_Z", gxTv_SdtModulo_Sistema_nome_Z, false);
            AddObjectProperty("Modulo_Codigo_N", gxTv_SdtModulo_Modulo_codigo_N, false);
            AddObjectProperty("Modulo_Descricao_N", gxTv_SdtModulo_Modulo_descricao_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Modulo_Codigo" )]
      [  XmlElement( ElementName = "Modulo_Codigo"   )]
      public int gxTpr_Modulo_codigo
      {
         get {
            return gxTv_SdtModulo_Modulo_codigo ;
         }

         set {
            if ( gxTv_SdtModulo_Modulo_codigo != value )
            {
               gxTv_SdtModulo_Mode = "INS";
               this.gxTv_SdtModulo_Modulo_codigo_Z_SetNull( );
               this.gxTv_SdtModulo_Modulo_nome_Z_SetNull( );
               this.gxTv_SdtModulo_Modulo_sigla_Z_SetNull( );
               this.gxTv_SdtModulo_Sistema_codigo_Z_SetNull( );
               this.gxTv_SdtModulo_Sistema_nome_Z_SetNull( );
            }
            gxTv_SdtModulo_Modulo_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Modulo_Nome" )]
      [  XmlElement( ElementName = "Modulo_Nome"   )]
      public String gxTpr_Modulo_nome
      {
         get {
            return gxTv_SdtModulo_Modulo_nome ;
         }

         set {
            gxTv_SdtModulo_Modulo_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Modulo_Sigla" )]
      [  XmlElement( ElementName = "Modulo_Sigla"   )]
      public String gxTpr_Modulo_sigla
      {
         get {
            return gxTv_SdtModulo_Modulo_sigla ;
         }

         set {
            gxTv_SdtModulo_Modulo_sigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Modulo_Descricao" )]
      [  XmlElement( ElementName = "Modulo_Descricao"   )]
      public String gxTpr_Modulo_descricao
      {
         get {
            return gxTv_SdtModulo_Modulo_descricao ;
         }

         set {
            gxTv_SdtModulo_Modulo_descricao_N = 0;
            gxTv_SdtModulo_Modulo_descricao = (String)(value);
         }

      }

      public void gxTv_SdtModulo_Modulo_descricao_SetNull( )
      {
         gxTv_SdtModulo_Modulo_descricao_N = 1;
         gxTv_SdtModulo_Modulo_descricao = "";
         return  ;
      }

      public bool gxTv_SdtModulo_Modulo_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Codigo" )]
      [  XmlElement( ElementName = "Sistema_Codigo"   )]
      public int gxTpr_Sistema_codigo
      {
         get {
            return gxTv_SdtModulo_Sistema_codigo ;
         }

         set {
            gxTv_SdtModulo_Sistema_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Sistema_Nome" )]
      [  XmlElement( ElementName = "Sistema_Nome"   )]
      public String gxTpr_Sistema_nome
      {
         get {
            return gxTv_SdtModulo_Sistema_nome ;
         }

         set {
            gxTv_SdtModulo_Sistema_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtModulo_Mode ;
         }

         set {
            gxTv_SdtModulo_Mode = (String)(value);
         }

      }

      public void gxTv_SdtModulo_Mode_SetNull( )
      {
         gxTv_SdtModulo_Mode = "";
         return  ;
      }

      public bool gxTv_SdtModulo_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtModulo_Initialized ;
         }

         set {
            gxTv_SdtModulo_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtModulo_Initialized_SetNull( )
      {
         gxTv_SdtModulo_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtModulo_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modulo_Codigo_Z" )]
      [  XmlElement( ElementName = "Modulo_Codigo_Z"   )]
      public int gxTpr_Modulo_codigo_Z
      {
         get {
            return gxTv_SdtModulo_Modulo_codigo_Z ;
         }

         set {
            gxTv_SdtModulo_Modulo_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtModulo_Modulo_codigo_Z_SetNull( )
      {
         gxTv_SdtModulo_Modulo_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtModulo_Modulo_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modulo_Nome_Z" )]
      [  XmlElement( ElementName = "Modulo_Nome_Z"   )]
      public String gxTpr_Modulo_nome_Z
      {
         get {
            return gxTv_SdtModulo_Modulo_nome_Z ;
         }

         set {
            gxTv_SdtModulo_Modulo_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtModulo_Modulo_nome_Z_SetNull( )
      {
         gxTv_SdtModulo_Modulo_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtModulo_Modulo_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modulo_Sigla_Z" )]
      [  XmlElement( ElementName = "Modulo_Sigla_Z"   )]
      public String gxTpr_Modulo_sigla_Z
      {
         get {
            return gxTv_SdtModulo_Modulo_sigla_Z ;
         }

         set {
            gxTv_SdtModulo_Modulo_sigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtModulo_Modulo_sigla_Z_SetNull( )
      {
         gxTv_SdtModulo_Modulo_sigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtModulo_Modulo_sigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Codigo_Z" )]
      [  XmlElement( ElementName = "Sistema_Codigo_Z"   )]
      public int gxTpr_Sistema_codigo_Z
      {
         get {
            return gxTv_SdtModulo_Sistema_codigo_Z ;
         }

         set {
            gxTv_SdtModulo_Sistema_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtModulo_Sistema_codigo_Z_SetNull( )
      {
         gxTv_SdtModulo_Sistema_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtModulo_Sistema_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Nome_Z" )]
      [  XmlElement( ElementName = "Sistema_Nome_Z"   )]
      public String gxTpr_Sistema_nome_Z
      {
         get {
            return gxTv_SdtModulo_Sistema_nome_Z ;
         }

         set {
            gxTv_SdtModulo_Sistema_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtModulo_Sistema_nome_Z_SetNull( )
      {
         gxTv_SdtModulo_Sistema_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtModulo_Sistema_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modulo_Codigo_N" )]
      [  XmlElement( ElementName = "Modulo_Codigo_N"   )]
      public short gxTpr_Modulo_codigo_N
      {
         get {
            return gxTv_SdtModulo_Modulo_codigo_N ;
         }

         set {
            gxTv_SdtModulo_Modulo_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtModulo_Modulo_codigo_N_SetNull( )
      {
         gxTv_SdtModulo_Modulo_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtModulo_Modulo_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modulo_Descricao_N" )]
      [  XmlElement( ElementName = "Modulo_Descricao_N"   )]
      public short gxTpr_Modulo_descricao_N
      {
         get {
            return gxTv_SdtModulo_Modulo_descricao_N ;
         }

         set {
            gxTv_SdtModulo_Modulo_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtModulo_Modulo_descricao_N_SetNull( )
      {
         gxTv_SdtModulo_Modulo_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtModulo_Modulo_descricao_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtModulo_Modulo_nome = "";
         gxTv_SdtModulo_Modulo_sigla = "";
         gxTv_SdtModulo_Modulo_descricao = "";
         gxTv_SdtModulo_Sistema_nome = "";
         gxTv_SdtModulo_Mode = "";
         gxTv_SdtModulo_Modulo_nome_Z = "";
         gxTv_SdtModulo_Modulo_sigla_Z = "";
         gxTv_SdtModulo_Sistema_nome_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "modulo", "GeneXus.Programs.modulo_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtModulo_Initialized ;
      private short gxTv_SdtModulo_Modulo_codigo_N ;
      private short gxTv_SdtModulo_Modulo_descricao_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtModulo_Modulo_codigo ;
      private int gxTv_SdtModulo_Sistema_codigo ;
      private int gxTv_SdtModulo_Modulo_codigo_Z ;
      private int gxTv_SdtModulo_Sistema_codigo_Z ;
      private String gxTv_SdtModulo_Modulo_nome ;
      private String gxTv_SdtModulo_Modulo_sigla ;
      private String gxTv_SdtModulo_Mode ;
      private String gxTv_SdtModulo_Modulo_nome_Z ;
      private String gxTv_SdtModulo_Modulo_sigla_Z ;
      private String sTagName ;
      private String gxTv_SdtModulo_Modulo_descricao ;
      private String gxTv_SdtModulo_Sistema_nome ;
      private String gxTv_SdtModulo_Sistema_nome_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Modulo", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtModulo_RESTInterface : GxGenericCollectionItem<SdtModulo>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtModulo_RESTInterface( ) : base()
      {
      }

      public SdtModulo_RESTInterface( SdtModulo psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Modulo_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Modulo_codigo
      {
         get {
            return sdt.gxTpr_Modulo_codigo ;
         }

         set {
            sdt.gxTpr_Modulo_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Modulo_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Modulo_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Modulo_nome) ;
         }

         set {
            sdt.gxTpr_Modulo_nome = (String)(value);
         }

      }

      [DataMember( Name = "Modulo_Sigla" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Modulo_sigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Modulo_sigla) ;
         }

         set {
            sdt.gxTpr_Modulo_sigla = (String)(value);
         }

      }

      [DataMember( Name = "Modulo_Descricao" , Order = 3 )]
      public String gxTpr_Modulo_descricao
      {
         get {
            return sdt.gxTpr_Modulo_descricao ;
         }

         set {
            sdt.gxTpr_Modulo_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Sistema_Codigo" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Sistema_codigo
      {
         get {
            return sdt.gxTpr_Sistema_codigo ;
         }

         set {
            sdt.gxTpr_Sistema_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sistema_Nome" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Sistema_nome
      {
         get {
            return sdt.gxTpr_Sistema_nome ;
         }

         set {
            sdt.gxTpr_Sistema_nome = (String)(value);
         }

      }

      public SdtModulo sdt
      {
         get {
            return (SdtModulo)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtModulo() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 15 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
