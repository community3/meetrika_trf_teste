/*
               File: PRC_ExisteContagem
        Description: Existe Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:51.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_existecontagem : GXProcedure
   {
      public prc_existecontagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_existecontagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           int aP1_ContagemResultado_ContadorFMCod ,
                           out bool aP2_Flag )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9ContagemResultado_ContadorFMCod = aP1_ContagemResultado_ContadorFMCod;
         this.AV10Flag = false ;
         initialize();
         executePrivate();
         aP2_Flag=this.AV10Flag;
      }

      public bool executeUdp( int aP0_ContagemResultado_Codigo ,
                              int aP1_ContagemResultado_ContadorFMCod )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9ContagemResultado_ContadorFMCod = aP1_ContagemResultado_ContadorFMCod;
         this.AV10Flag = false ;
         initialize();
         executePrivate();
         aP2_Flag=this.AV10Flag;
         return AV10Flag ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 int aP1_ContagemResultado_ContadorFMCod ,
                                 out bool aP2_Flag )
      {
         prc_existecontagem objprc_existecontagem;
         objprc_existecontagem = new prc_existecontagem();
         objprc_existecontagem.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_existecontagem.AV9ContagemResultado_ContadorFMCod = aP1_ContagemResultado_ContadorFMCod;
         objprc_existecontagem.AV10Flag = false ;
         objprc_existecontagem.context.SetSubmitInitialConfig(context);
         objprc_existecontagem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_existecontagem);
         aP2_Flag=this.AV10Flag;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_existecontagem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV10Flag = false;
         if ( (0==AV9ContagemResultado_ContadorFMCod) )
         {
            AV10Flag = true;
         }
         else
         {
            /* Using cursor P00372 */
            pr_default.execute(0, new Object[] {AV8ContagemResultado_Codigo, AV9ContagemResultado_ContadorFMCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A470ContagemResultado_ContadorFMCod = P00372_A470ContagemResultado_ContadorFMCod[0];
               A456ContagemResultado_Codigo = P00372_A456ContagemResultado_Codigo[0];
               A473ContagemResultado_DataCnt = P00372_A473ContagemResultado_DataCnt[0];
               A511ContagemResultado_HoraCnt = P00372_A511ContagemResultado_HoraCnt[0];
               AV10Flag = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(0);
            }
            pr_default.close(0);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00372_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P00372_A456ContagemResultado_Codigo = new int[1] ;
         P00372_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00372_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_existecontagem__default(),
            new Object[][] {
                new Object[] {
               P00372_A470ContagemResultado_ContadorFMCod, P00372_A456ContagemResultado_Codigo, P00372_A473ContagemResultado_DataCnt, P00372_A511ContagemResultado_HoraCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8ContagemResultado_Codigo ;
      private int AV9ContagemResultado_ContadorFMCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool AV10Flag ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00372_A470ContagemResultado_ContadorFMCod ;
      private int[] P00372_A456ContagemResultado_Codigo ;
      private DateTime[] P00372_A473ContagemResultado_DataCnt ;
      private String[] P00372_A511ContagemResultado_HoraCnt ;
      private bool aP2_Flag ;
   }

   public class prc_existecontagem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00372 ;
          prmP00372 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00372", "SELECT TOP 1 [ContagemResultado_ContadorFMCod], [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo) AND ([ContagemResultado_ContadorFMCod] = @AV9ContagemResultado_ContadorFMCod) ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00372,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
