/*
               File: ExportExtraWWContagemResultadoExtraSelection
        Description: Export Extra WWContagem Resultado Extra Selection
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/17/2020 1:56:56.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportextrawwcontagemresultadoextraselection : GXProcedure
   {
      public exportextrawwcontagemresultadoextraselection( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public exportextrawwcontagemresultadoextraselection( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           short aP1_ContagemResultado_StatusCnt ,
                           String aP2_TFContagemResultado_Agrupador ,
                           String aP3_TFContagemResultado_Agrupador_Sel ,
                           String aP4_TFContagemResultado_DemandaFM ,
                           String aP5_TFContagemResultado_DemandaFM_Sel ,
                           String aP6_TFContagemResultado_Demanda ,
                           String aP7_TFContagemResultado_Demanda_Sel ,
                           String aP8_TFContagemResultado_Descricao ,
                           String aP9_TFContagemResultado_Descricao_Sel ,
                           DateTime aP10_TFContagemResultado_DataPrevista ,
                           DateTime aP11_TFContagemResultado_DataPrevista_To ,
                           DateTime aP12_TFContagemResultado_DataDmn ,
                           DateTime aP13_TFContagemResultado_DataDmn_To ,
                           DateTime aP14_TFContagemResultado_DataUltCnt ,
                           DateTime aP15_TFContagemResultado_DataUltCnt_To ,
                           String aP16_TFContagemResultado_ContratadaSigla ,
                           String aP17_TFContagemResultado_ContratadaSigla_Sel ,
                           String aP18_TFContagemResultado_SistemaCoord ,
                           String aP19_TFContagemResultado_SistemaCoord_Sel ,
                           String aP20_TFContagemResultado_StatusDmn_SelsJson ,
                           short aP21_TFContagemResultado_Baseline_Sel ,
                           String aP22_TFContagemResultado_ServicoSigla ,
                           String aP23_TFContagemResultado_ServicoSigla_Sel ,
                           decimal aP24_TFContagemResultado_PFFinal ,
                           decimal aP25_TFContagemResultado_PFFinal_To ,
                           decimal aP26_TFContagemResultado_ValorPF ,
                           decimal aP27_TFContagemResultado_ValorPF_To ,
                           short aP28_OrderedBy ,
                           bool aP29_OrderedDsc ,
                           String aP30_GridStateXML ,
                           out String aP31_Filename ,
                           out String aP32_ErrorMessage )
      {
         this.AV18Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV19ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         this.AV157TFContagemResultado_Agrupador = aP2_TFContagemResultado_Agrupador;
         this.AV158TFContagemResultado_Agrupador_Sel = aP3_TFContagemResultado_Agrupador_Sel;
         this.AV161TFContagemResultado_DemandaFM = aP4_TFContagemResultado_DemandaFM;
         this.AV162TFContagemResultado_DemandaFM_Sel = aP5_TFContagemResultado_DemandaFM_Sel;
         this.AV159TFContagemResultado_Demanda = aP6_TFContagemResultado_Demanda;
         this.AV160TFContagemResultado_Demanda_Sel = aP7_TFContagemResultado_Demanda_Sel;
         this.AV163TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         this.AV164TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         this.AV203TFContagemResultado_DataPrevista = aP10_TFContagemResultado_DataPrevista;
         this.AV204TFContagemResultado_DataPrevista_To = aP11_TFContagemResultado_DataPrevista_To;
         this.AV201TFContagemResultado_DataDmn = aP12_TFContagemResultado_DataDmn;
         this.AV202TFContagemResultado_DataDmn_To = aP13_TFContagemResultado_DataDmn_To;
         this.AV165TFContagemResultado_DataUltCnt = aP14_TFContagemResultado_DataUltCnt;
         this.AV166TFContagemResultado_DataUltCnt_To = aP15_TFContagemResultado_DataUltCnt_To;
         this.AV167TFContagemResultado_ContratadaSigla = aP16_TFContagemResultado_ContratadaSigla;
         this.AV168TFContagemResultado_ContratadaSigla_Sel = aP17_TFContagemResultado_ContratadaSigla_Sel;
         this.AV169TFContagemResultado_SistemaCoord = aP18_TFContagemResultado_SistemaCoord;
         this.AV170TFContagemResultado_SistemaCoord_Sel = aP19_TFContagemResultado_SistemaCoord_Sel;
         this.AV171TFContagemResultado_StatusDmn_SelsJson = aP20_TFContagemResultado_StatusDmn_SelsJson;
         this.AV174TFContagemResultado_Baseline_Sel = aP21_TFContagemResultado_Baseline_Sel;
         this.AV175TFContagemResultado_ServicoSigla = aP22_TFContagemResultado_ServicoSigla;
         this.AV176TFContagemResultado_ServicoSigla_Sel = aP23_TFContagemResultado_ServicoSigla_Sel;
         this.AV177TFContagemResultado_PFFinal = aP24_TFContagemResultado_PFFinal;
         this.AV178TFContagemResultado_PFFinal_To = aP25_TFContagemResultado_PFFinal_To;
         this.AV179TFContagemResultado_ValorPF = aP26_TFContagemResultado_ValorPF;
         this.AV180TFContagemResultado_ValorPF_To = aP27_TFContagemResultado_ValorPF_To;
         this.AV16OrderedBy = aP28_OrderedBy;
         this.AV17OrderedDsc = aP29_OrderedDsc;
         this.AV53GridStateXML = aP30_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP31_Filename=this.AV11Filename;
         aP32_ErrorMessage=this.AV12ErrorMessage;
      }

      public String executeUdp( int aP0_Contratada_AreaTrabalhoCod ,
                                short aP1_ContagemResultado_StatusCnt ,
                                String aP2_TFContagemResultado_Agrupador ,
                                String aP3_TFContagemResultado_Agrupador_Sel ,
                                String aP4_TFContagemResultado_DemandaFM ,
                                String aP5_TFContagemResultado_DemandaFM_Sel ,
                                String aP6_TFContagemResultado_Demanda ,
                                String aP7_TFContagemResultado_Demanda_Sel ,
                                String aP8_TFContagemResultado_Descricao ,
                                String aP9_TFContagemResultado_Descricao_Sel ,
                                DateTime aP10_TFContagemResultado_DataPrevista ,
                                DateTime aP11_TFContagemResultado_DataPrevista_To ,
                                DateTime aP12_TFContagemResultado_DataDmn ,
                                DateTime aP13_TFContagemResultado_DataDmn_To ,
                                DateTime aP14_TFContagemResultado_DataUltCnt ,
                                DateTime aP15_TFContagemResultado_DataUltCnt_To ,
                                String aP16_TFContagemResultado_ContratadaSigla ,
                                String aP17_TFContagemResultado_ContratadaSigla_Sel ,
                                String aP18_TFContagemResultado_SistemaCoord ,
                                String aP19_TFContagemResultado_SistemaCoord_Sel ,
                                String aP20_TFContagemResultado_StatusDmn_SelsJson ,
                                short aP21_TFContagemResultado_Baseline_Sel ,
                                String aP22_TFContagemResultado_ServicoSigla ,
                                String aP23_TFContagemResultado_ServicoSigla_Sel ,
                                decimal aP24_TFContagemResultado_PFFinal ,
                                decimal aP25_TFContagemResultado_PFFinal_To ,
                                decimal aP26_TFContagemResultado_ValorPF ,
                                decimal aP27_TFContagemResultado_ValorPF_To ,
                                short aP28_OrderedBy ,
                                bool aP29_OrderedDsc ,
                                String aP30_GridStateXML ,
                                out String aP31_Filename )
      {
         this.AV18Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV19ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         this.AV157TFContagemResultado_Agrupador = aP2_TFContagemResultado_Agrupador;
         this.AV158TFContagemResultado_Agrupador_Sel = aP3_TFContagemResultado_Agrupador_Sel;
         this.AV161TFContagemResultado_DemandaFM = aP4_TFContagemResultado_DemandaFM;
         this.AV162TFContagemResultado_DemandaFM_Sel = aP5_TFContagemResultado_DemandaFM_Sel;
         this.AV159TFContagemResultado_Demanda = aP6_TFContagemResultado_Demanda;
         this.AV160TFContagemResultado_Demanda_Sel = aP7_TFContagemResultado_Demanda_Sel;
         this.AV163TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         this.AV164TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         this.AV203TFContagemResultado_DataPrevista = aP10_TFContagemResultado_DataPrevista;
         this.AV204TFContagemResultado_DataPrevista_To = aP11_TFContagemResultado_DataPrevista_To;
         this.AV201TFContagemResultado_DataDmn = aP12_TFContagemResultado_DataDmn;
         this.AV202TFContagemResultado_DataDmn_To = aP13_TFContagemResultado_DataDmn_To;
         this.AV165TFContagemResultado_DataUltCnt = aP14_TFContagemResultado_DataUltCnt;
         this.AV166TFContagemResultado_DataUltCnt_To = aP15_TFContagemResultado_DataUltCnt_To;
         this.AV167TFContagemResultado_ContratadaSigla = aP16_TFContagemResultado_ContratadaSigla;
         this.AV168TFContagemResultado_ContratadaSigla_Sel = aP17_TFContagemResultado_ContratadaSigla_Sel;
         this.AV169TFContagemResultado_SistemaCoord = aP18_TFContagemResultado_SistemaCoord;
         this.AV170TFContagemResultado_SistemaCoord_Sel = aP19_TFContagemResultado_SistemaCoord_Sel;
         this.AV171TFContagemResultado_StatusDmn_SelsJson = aP20_TFContagemResultado_StatusDmn_SelsJson;
         this.AV174TFContagemResultado_Baseline_Sel = aP21_TFContagemResultado_Baseline_Sel;
         this.AV175TFContagemResultado_ServicoSigla = aP22_TFContagemResultado_ServicoSigla;
         this.AV176TFContagemResultado_ServicoSigla_Sel = aP23_TFContagemResultado_ServicoSigla_Sel;
         this.AV177TFContagemResultado_PFFinal = aP24_TFContagemResultado_PFFinal;
         this.AV178TFContagemResultado_PFFinal_To = aP25_TFContagemResultado_PFFinal_To;
         this.AV179TFContagemResultado_ValorPF = aP26_TFContagemResultado_ValorPF;
         this.AV180TFContagemResultado_ValorPF_To = aP27_TFContagemResultado_ValorPF_To;
         this.AV16OrderedBy = aP28_OrderedBy;
         this.AV17OrderedDsc = aP29_OrderedDsc;
         this.AV53GridStateXML = aP30_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP31_Filename=this.AV11Filename;
         aP32_ErrorMessage=this.AV12ErrorMessage;
         return AV12ErrorMessage ;
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 short aP1_ContagemResultado_StatusCnt ,
                                 String aP2_TFContagemResultado_Agrupador ,
                                 String aP3_TFContagemResultado_Agrupador_Sel ,
                                 String aP4_TFContagemResultado_DemandaFM ,
                                 String aP5_TFContagemResultado_DemandaFM_Sel ,
                                 String aP6_TFContagemResultado_Demanda ,
                                 String aP7_TFContagemResultado_Demanda_Sel ,
                                 String aP8_TFContagemResultado_Descricao ,
                                 String aP9_TFContagemResultado_Descricao_Sel ,
                                 DateTime aP10_TFContagemResultado_DataPrevista ,
                                 DateTime aP11_TFContagemResultado_DataPrevista_To ,
                                 DateTime aP12_TFContagemResultado_DataDmn ,
                                 DateTime aP13_TFContagemResultado_DataDmn_To ,
                                 DateTime aP14_TFContagemResultado_DataUltCnt ,
                                 DateTime aP15_TFContagemResultado_DataUltCnt_To ,
                                 String aP16_TFContagemResultado_ContratadaSigla ,
                                 String aP17_TFContagemResultado_ContratadaSigla_Sel ,
                                 String aP18_TFContagemResultado_SistemaCoord ,
                                 String aP19_TFContagemResultado_SistemaCoord_Sel ,
                                 String aP20_TFContagemResultado_StatusDmn_SelsJson ,
                                 short aP21_TFContagemResultado_Baseline_Sel ,
                                 String aP22_TFContagemResultado_ServicoSigla ,
                                 String aP23_TFContagemResultado_ServicoSigla_Sel ,
                                 decimal aP24_TFContagemResultado_PFFinal ,
                                 decimal aP25_TFContagemResultado_PFFinal_To ,
                                 decimal aP26_TFContagemResultado_ValorPF ,
                                 decimal aP27_TFContagemResultado_ValorPF_To ,
                                 short aP28_OrderedBy ,
                                 bool aP29_OrderedDsc ,
                                 String aP30_GridStateXML ,
                                 out String aP31_Filename ,
                                 out String aP32_ErrorMessage )
      {
         exportextrawwcontagemresultadoextraselection objexportextrawwcontagemresultadoextraselection;
         objexportextrawwcontagemresultadoextraselection = new exportextrawwcontagemresultadoextraselection();
         objexportextrawwcontagemresultadoextraselection.AV18Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objexportextrawwcontagemresultadoextraselection.AV19ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         objexportextrawwcontagemresultadoextraselection.AV157TFContagemResultado_Agrupador = aP2_TFContagemResultado_Agrupador;
         objexportextrawwcontagemresultadoextraselection.AV158TFContagemResultado_Agrupador_Sel = aP3_TFContagemResultado_Agrupador_Sel;
         objexportextrawwcontagemresultadoextraselection.AV161TFContagemResultado_DemandaFM = aP4_TFContagemResultado_DemandaFM;
         objexportextrawwcontagemresultadoextraselection.AV162TFContagemResultado_DemandaFM_Sel = aP5_TFContagemResultado_DemandaFM_Sel;
         objexportextrawwcontagemresultadoextraselection.AV159TFContagemResultado_Demanda = aP6_TFContagemResultado_Demanda;
         objexportextrawwcontagemresultadoextraselection.AV160TFContagemResultado_Demanda_Sel = aP7_TFContagemResultado_Demanda_Sel;
         objexportextrawwcontagemresultadoextraselection.AV163TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         objexportextrawwcontagemresultadoextraselection.AV164TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         objexportextrawwcontagemresultadoextraselection.AV203TFContagemResultado_DataPrevista = aP10_TFContagemResultado_DataPrevista;
         objexportextrawwcontagemresultadoextraselection.AV204TFContagemResultado_DataPrevista_To = aP11_TFContagemResultado_DataPrevista_To;
         objexportextrawwcontagemresultadoextraselection.AV201TFContagemResultado_DataDmn = aP12_TFContagemResultado_DataDmn;
         objexportextrawwcontagemresultadoextraselection.AV202TFContagemResultado_DataDmn_To = aP13_TFContagemResultado_DataDmn_To;
         objexportextrawwcontagemresultadoextraselection.AV165TFContagemResultado_DataUltCnt = aP14_TFContagemResultado_DataUltCnt;
         objexportextrawwcontagemresultadoextraselection.AV166TFContagemResultado_DataUltCnt_To = aP15_TFContagemResultado_DataUltCnt_To;
         objexportextrawwcontagemresultadoextraselection.AV167TFContagemResultado_ContratadaSigla = aP16_TFContagemResultado_ContratadaSigla;
         objexportextrawwcontagemresultadoextraselection.AV168TFContagemResultado_ContratadaSigla_Sel = aP17_TFContagemResultado_ContratadaSigla_Sel;
         objexportextrawwcontagemresultadoextraselection.AV169TFContagemResultado_SistemaCoord = aP18_TFContagemResultado_SistemaCoord;
         objexportextrawwcontagemresultadoextraselection.AV170TFContagemResultado_SistemaCoord_Sel = aP19_TFContagemResultado_SistemaCoord_Sel;
         objexportextrawwcontagemresultadoextraselection.AV171TFContagemResultado_StatusDmn_SelsJson = aP20_TFContagemResultado_StatusDmn_SelsJson;
         objexportextrawwcontagemresultadoextraselection.AV174TFContagemResultado_Baseline_Sel = aP21_TFContagemResultado_Baseline_Sel;
         objexportextrawwcontagemresultadoextraselection.AV175TFContagemResultado_ServicoSigla = aP22_TFContagemResultado_ServicoSigla;
         objexportextrawwcontagemresultadoextraselection.AV176TFContagemResultado_ServicoSigla_Sel = aP23_TFContagemResultado_ServicoSigla_Sel;
         objexportextrawwcontagemresultadoextraselection.AV177TFContagemResultado_PFFinal = aP24_TFContagemResultado_PFFinal;
         objexportextrawwcontagemresultadoextraselection.AV178TFContagemResultado_PFFinal_To = aP25_TFContagemResultado_PFFinal_To;
         objexportextrawwcontagemresultadoextraselection.AV179TFContagemResultado_ValorPF = aP26_TFContagemResultado_ValorPF;
         objexportextrawwcontagemresultadoextraselection.AV180TFContagemResultado_ValorPF_To = aP27_TFContagemResultado_ValorPF_To;
         objexportextrawwcontagemresultadoextraselection.AV16OrderedBy = aP28_OrderedBy;
         objexportextrawwcontagemresultadoextraselection.AV17OrderedDsc = aP29_OrderedDsc;
         objexportextrawwcontagemresultadoextraselection.AV53GridStateXML = aP30_GridStateXML;
         objexportextrawwcontagemresultadoextraselection.AV11Filename = "" ;
         objexportextrawwcontagemresultadoextraselection.AV12ErrorMessage = "" ;
         objexportextrawwcontagemresultadoextraselection.context.SetSubmitInitialConfig(context);
         objexportextrawwcontagemresultadoextraselection.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportextrawwcontagemresultadoextraselection);
         aP31_Filename=this.AV11Filename;
         aP32_ErrorMessage=this.AV12ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportextrawwcontagemresultadoextraselection)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV142Contratadas.FromXml(AV143WebSession.Get("Contratadas"), "Collection");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'OPENDOCUMENT' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13CellRow = 1;
         AV14FirstColumn = 1;
         /* Execute user subroutine: 'WRITEMAINTITLE' */
         S131 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEFILTERS' */
         S141 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITECOLUMNTITLES' */
         S151 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEDATA' */
         S161 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'CLOSEDOCUMENT' */
         S191 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'OPENDOCUMENT' Routine */
         if ( false )
         {
            AV15Random = (int)(NumberUtil.Random( )*10000);
            AV11Filename = "ExportExtraWWContagemResultadoExtraSelection-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Random), 8, 0)) + ".xlsx";
            AV10ExcelDocument.Open(AV11Filename);
            /* Execute user subroutine: 'CHECKSTATUS' */
            S121 ();
            if (returnInSub) return;
            AV10ExcelDocument.Clear();
         }
         if ( AV9WWPContext.gxTpr_Userehgestor )
         {
            AV123i = 1;
            while ( true )
            {
               AV124p = (short)(StringUtil.StringSearch( AV9WWPContext.gxTpr_Servicosgeridos, ",", (int)(AV123i)));
               if ( AV124p == 0 )
               {
                  if (true) break;
               }
               AV124p = (short)(AV124p-1);
               AV125Servico = (int)(NumberUtil.Val( StringUtil.Substring( AV9WWPContext.gxTpr_Servicosgeridos, (int)(AV123i), AV124p), "."));
               AV126ServicosGeridos.Add(AV125Servico, 0);
               AV123i = (long)(AV124p+2);
            }
         }
         AV15Random = (int)(NumberUtil.Random( )*10000);
         AV11Filename = "PublicTempStorage\\ExportExtraWWContagemResultadoExtraSelection-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Random), 8, 0)) + ".xlsx";
         AV10ExcelDocument.Open(AV11Filename);
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Clear();
      }

      protected void S131( )
      {
         /* 'WRITEMAINTITLE' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Resultado das Contagens";
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S141( )
      {
         /* 'WRITEFILTERS' Routine */
         if ( ! ( (0==AV18Contratada_AreaTrabalhoCod) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "�rea de Trabalho";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV18Contratada_AreaTrabalhoCod;
         }
         if ( ! ( (0==AV19ContagemResultado_StatusCnt) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Status";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            if ( ! (0==AV19ContagemResultado_StatusCnt) )
            {
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV19ContagemResultado_StatusCnt);
            }
         }
         AV54GridState.gxTpr_Dynamicfilters.FromXml(AV53GridStateXML, "");
         if ( AV54GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV55GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV54GridState.gxTpr_Dynamicfilters.Item(1));
            AV21DynamicFiltersSelector1 = AV55GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 )
            {
               AV22ContagemResultado_DataCnt1 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
               AV23ContagemResultado_DataCnt_To1 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV22ContagemResultado_DataCnt1) || ! (DateTime.MinValue==AV23ContagemResultado_DataCnt_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data da Cnt";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV22ContagemResultado_DataCnt1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV23ContagemResultado_DataCnt_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV28ContagemResultado_StatusDmn1 = AV55GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28ContagemResultado_StatusDmn1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status da Dmn";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28ContagemResultado_StatusDmn1)) )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV28ContagemResultado_StatusDmn1);
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV137DynamicFiltersOperator1 = AV55GridStateDynamicFilter.gxTpr_Operator;
               AV24ContagemResultado_OsFsOsFm1 = AV55GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContagemResultado_OsFsOsFm1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  if ( AV137DynamicFiltersOperator1 == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                  }
                  else if ( AV137DynamicFiltersOperator1 == 1 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                  }
                  else if ( AV137DynamicFiltersOperator1 == 2 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV24ContagemResultado_OsFsOsFm1;
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV181ContagemResultado_DataDmn1 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
               AV182ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV181ContagemResultado_DataDmn1) || ! (DateTime.MinValue==AV182ContagemResultado_DataDmn_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data da Dmn";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV181ContagemResultado_DataDmn1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV182ContagemResultado_DataDmn_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
            {
               AV183ContagemResultado_DataPrevista1 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
               AV184ContagemResultado_DataPrevista_To1 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV183ContagemResultado_DataPrevista1) || ! (DateTime.MinValue==AV184ContagemResultado_DataPrevista_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Prevista";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV183ContagemResultado_DataPrevista1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV184ContagemResultado_DataPrevista_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV65ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV65ContagemResultado_Servico1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV65ContagemResultado_Servico1;
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
            {
               AV74ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV74ContagemResultado_ContadorFM1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV74ContagemResultado_ContadorFM1;
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV25ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV25ContagemResultado_SistemaCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV25ContagemResultado_SistemaCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV26ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV26ContagemResultado_ContratadaCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV26ContagemResultado_ContratadaCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
            {
               AV71ContagemResultado_ServicoGrupo1 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV71ContagemResultado_ServicoGrupo1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Grupo de Servi�os";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV71ContagemResultado_ServicoGrupo1;
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
            {
               AV59ContagemResultado_Baseline1 = AV55GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ContagemResultado_Baseline1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV59ContagemResultado_Baseline1), "/") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "(Todos)";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV59ContagemResultado_Baseline1), "S") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV59ContagemResultado_Baseline1), "N") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
            {
               AV27ContagemResultado_NaoCnfDmnCod1 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV27ContagemResultado_NaoCnfDmnCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV27ContagemResultado_NaoCnfDmnCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_PFBFM") == 0 )
            {
               AV29ContagemResultado_PFBFM1 = NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, ".");
               if ( ! (Convert.ToDecimal(0)==AV29ContagemResultado_PFBFM1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFB Diferentes";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV29ContagemResultado_PFBFM1);
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_PFBFS") == 0 )
            {
               AV30ContagemResultado_PFBFS1 = NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, ".");
               if ( ! (Convert.ToDecimal(0)==AV30ContagemResultado_PFBFS1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFL Diferentes";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV30ContagemResultado_PFBFS1);
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV117ContagemResultado_Agrupador1 = AV55GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117ContagemResultado_Agrupador1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV117ContagemResultado_Agrupador1;
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV132ContagemResultado_Descricao1 = AV55GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV132ContagemResultado_Descricao1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV132ContagemResultado_Descricao1;
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_OWNER") == 0 )
            {
               AV127ContagemResultado_Owner1 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV127ContagemResultado_Owner1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Solicitante";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV127ContagemResultado_Owner1;
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
            {
               AV145ContagemResultado_TemPndHmlg1 = AV55GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV145ContagemResultado_TemPndHmlg1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Pend�ncias vinculadas";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV145ContagemResultado_TemPndHmlg1), "S") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sem pend�ncias";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV145ContagemResultado_TemPndHmlg1), "C") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Com pend�ncias";
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_CNTCOD") == 0 )
            {
               AV207ContagemResultado_CntCod1 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV207ContagemResultado_CntCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contrato";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV207ContagemResultado_CntCod1;
               }
            }
            if ( AV54GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV31DynamicFiltersEnabled2 = true;
               AV55GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV54GridState.gxTpr_Dynamicfilters.Item(2));
               AV32DynamicFiltersSelector2 = AV55GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 )
               {
                  AV33ContagemResultado_DataCnt2 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
                  AV34ContagemResultado_DataCnt_To2 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV33ContagemResultado_DataCnt2) || ! (DateTime.MinValue==AV34ContagemResultado_DataCnt_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data da Cnt";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV33ContagemResultado_DataCnt2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV34ContagemResultado_DataCnt_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV39ContagemResultado_StatusDmn2 = AV55GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ContagemResultado_StatusDmn2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status da Dmn";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ContagemResultado_StatusDmn2)) )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV39ContagemResultado_StatusDmn2);
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV138DynamicFiltersOperator2 = AV55GridStateDynamicFilter.gxTpr_Operator;
                  AV35ContagemResultado_OsFsOsFm2 = AV55GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35ContagemResultado_OsFsOsFm2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     if ( AV138DynamicFiltersOperator2 == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                     }
                     else if ( AV138DynamicFiltersOperator2 == 1 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                     }
                     else if ( AV138DynamicFiltersOperator2 == 2 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                     }
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV35ContagemResultado_OsFsOsFm2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV185ContagemResultado_DataDmn2 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
                  AV186ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV185ContagemResultado_DataDmn2) || ! (DateTime.MinValue==AV186ContagemResultado_DataDmn_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data da Dmn";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV185ContagemResultado_DataDmn2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV186ContagemResultado_DataDmn_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
               {
                  AV187ContagemResultado_DataPrevista2 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
                  AV188ContagemResultado_DataPrevista_To2 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV187ContagemResultado_DataPrevista2) || ! (DateTime.MinValue==AV188ContagemResultado_DataPrevista_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Prevista";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV187ContagemResultado_DataPrevista2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV188ContagemResultado_DataPrevista_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV66ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV66ContagemResultado_Servico2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV66ContagemResultado_Servico2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
               {
                  AV75ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV75ContagemResultado_ContadorFM2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV75ContagemResultado_ContadorFM2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV36ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV36ContagemResultado_SistemaCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV36ContagemResultado_SistemaCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV37ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV37ContagemResultado_ContratadaCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV37ContagemResultado_ContratadaCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
               {
                  AV72ContagemResultado_ServicoGrupo2 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV72ContagemResultado_ServicoGrupo2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Grupo de Servi�os";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV72ContagemResultado_ServicoGrupo2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV60ContagemResultado_Baseline2 = AV55GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Baseline2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV60ContagemResultado_Baseline2), "/") == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "(Todos)";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV60ContagemResultado_Baseline2), "S") == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV60ContagemResultado_Baseline2), "N") == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
               {
                  AV38ContagemResultado_NaoCnfDmnCod2 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV38ContagemResultado_NaoCnfDmnCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV38ContagemResultado_NaoCnfDmnCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_PFBFM") == 0 )
               {
                  AV40ContagemResultado_PFBFM2 = NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, ".");
                  if ( ! (Convert.ToDecimal(0)==AV40ContagemResultado_PFBFM2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFB Diferentes";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV40ContagemResultado_PFBFM2);
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_PFBFS") == 0 )
               {
                  AV41ContagemResultado_PFBFS2 = NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, ".");
                  if ( ! (Convert.ToDecimal(0)==AV41ContagemResultado_PFBFS2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFL Diferentes";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV41ContagemResultado_PFBFS2);
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV118ContagemResultado_Agrupador2 = AV55GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118ContagemResultado_Agrupador2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV118ContagemResultado_Agrupador2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV133ContagemResultado_Descricao2 = AV55GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV133ContagemResultado_Descricao2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV133ContagemResultado_Descricao2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_OWNER") == 0 )
               {
                  AV128ContagemResultado_Owner2 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV128ContagemResultado_Owner2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Solicitante";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV128ContagemResultado_Owner2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
               {
                  AV147ContagemResultado_TemPndHmlg2 = AV55GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV147ContagemResultado_TemPndHmlg2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Pend�ncias vinculadas";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV147ContagemResultado_TemPndHmlg2), "S") == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sem pend�ncias";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV147ContagemResultado_TemPndHmlg2), "C") == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Com pend�ncias";
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_CNTCOD") == 0 )
               {
                  AV208ContagemResultado_CntCod2 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV208ContagemResultado_CntCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contrato";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV208ContagemResultado_CntCod2;
                  }
               }
               if ( AV54GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV42DynamicFiltersEnabled3 = true;
                  AV55GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV54GridState.gxTpr_Dynamicfilters.Item(3));
                  AV43DynamicFiltersSelector3 = AV55GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 )
                  {
                     AV44ContagemResultado_DataCnt3 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
                     AV45ContagemResultado_DataCnt_To3 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV44ContagemResultado_DataCnt3) || ! (DateTime.MinValue==AV45ContagemResultado_DataCnt_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data da Cnt";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV44ContagemResultado_DataCnt3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV45ContagemResultado_DataCnt_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV50ContagemResultado_StatusDmn3 = AV55GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContagemResultado_StatusDmn3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status da Dmn";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContagemResultado_StatusDmn3)) )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV50ContagemResultado_StatusDmn3);
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV139DynamicFiltersOperator3 = AV55GridStateDynamicFilter.gxTpr_Operator;
                     AV46ContagemResultado_OsFsOsFm3 = AV55GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_OsFsOsFm3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        if ( AV139DynamicFiltersOperator3 == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                        }
                        else if ( AV139DynamicFiltersOperator3 == 1 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                        }
                        else if ( AV139DynamicFiltersOperator3 == 2 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                        }
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV46ContagemResultado_OsFsOsFm3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV189ContagemResultado_DataDmn3 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
                     AV190ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV189ContagemResultado_DataDmn3) || ! (DateTime.MinValue==AV190ContagemResultado_DataDmn_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data da Dmn";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV189ContagemResultado_DataDmn3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV190ContagemResultado_DataDmn_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                  {
                     AV191ContagemResultado_DataPrevista3 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
                     AV192ContagemResultado_DataPrevista_To3 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV191ContagemResultado_DataPrevista3) || ! (DateTime.MinValue==AV192ContagemResultado_DataPrevista_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Prevista";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV191ContagemResultado_DataPrevista3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV192ContagemResultado_DataPrevista_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV67ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV67ContagemResultado_Servico3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV67ContagemResultado_Servico3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                  {
                     AV76ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV76ContagemResultado_ContadorFM3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV76ContagemResultado_ContadorFM3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV47ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV47ContagemResultado_SistemaCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV47ContagemResultado_SistemaCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV48ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV48ContagemResultado_ContratadaCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV48ContagemResultado_ContratadaCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                  {
                     AV73ContagemResultado_ServicoGrupo3 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV73ContagemResultado_ServicoGrupo3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Grupo de Servi�os";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV73ContagemResultado_ServicoGrupo3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV61ContagemResultado_Baseline3 = AV55GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemResultado_Baseline3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                        if ( StringUtil.StrCmp(StringUtil.Trim( AV61ContagemResultado_Baseline3), "/") == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "(Todos)";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV61ContagemResultado_Baseline3), "S") == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV61ContagemResultado_Baseline3), "N") == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                  {
                     AV49ContagemResultado_NaoCnfDmnCod3 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV49ContagemResultado_NaoCnfDmnCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV49ContagemResultado_NaoCnfDmnCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_PFBFM") == 0 )
                  {
                     AV51ContagemResultado_PFBFM3 = NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, ".");
                     if ( ! (Convert.ToDecimal(0)==AV51ContagemResultado_PFBFM3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFB Diferentes";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV51ContagemResultado_PFBFM3);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_PFBFS") == 0 )
                  {
                     AV52ContagemResultado_PFBFS3 = NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, ".");
                     if ( ! (Convert.ToDecimal(0)==AV52ContagemResultado_PFBFS3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFL Diferentes";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV52ContagemResultado_PFBFS3);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV119ContagemResultado_Agrupador3 = AV55GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119ContagemResultado_Agrupador3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV119ContagemResultado_Agrupador3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV134ContagemResultado_Descricao3 = AV55GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134ContagemResultado_Descricao3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV134ContagemResultado_Descricao3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_OWNER") == 0 )
                  {
                     AV129ContagemResultado_Owner3 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV129ContagemResultado_Owner3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Solicitante";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV129ContagemResultado_Owner3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                  {
                     AV149ContagemResultado_TemPndHmlg3 = AV55GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149ContagemResultado_TemPndHmlg3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Pend�ncias vinculadas";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                        if ( StringUtil.StrCmp(StringUtil.Trim( AV149ContagemResultado_TemPndHmlg3), "S") == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sem pend�ncias";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV149ContagemResultado_TemPndHmlg3), "C") == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Com pend�ncias";
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                  {
                     AV209ContagemResultado_CntCod3 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV209ContagemResultado_CntCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contrato";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV209ContagemResultado_CntCod3;
                     }
                  }
                  if ( AV54GridState.gxTpr_Dynamicfilters.Count >= 4 )
                  {
                     AV77DynamicFiltersEnabled4 = true;
                     AV55GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV54GridState.gxTpr_Dynamicfilters.Item(4));
                     AV78DynamicFiltersSelector4 = AV55GridStateDynamicFilter.gxTpr_Selected;
                     if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATACNT") == 0 )
                     {
                        AV79ContagemResultado_DataCnt4 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
                        AV80ContagemResultado_DataCnt_To4 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
                        if ( ! (DateTime.MinValue==AV79ContagemResultado_DataCnt4) || ! (DateTime.MinValue==AV80ContagemResultado_DataCnt_To4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data da Cnt";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           GXt_dtime1 = DateTimeUtil.ResetTime( AV79ContagemResultado_DataCnt4 ) ;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                           GXt_dtime1 = DateTimeUtil.ResetTime( AV80ContagemResultado_DataCnt_To4 ) ;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                     {
                        AV82ContagemResultado_StatusDmn4 = AV55GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82ContagemResultado_StatusDmn4)) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status da Dmn";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82ContagemResultado_StatusDmn4)) )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV82ContagemResultado_StatusDmn4);
                           }
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                     {
                        AV140DynamicFiltersOperator4 = AV55GridStateDynamicFilter.gxTpr_Operator;
                        AV81ContagemResultado_OsFsOsFm4 = AV55GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81ContagemResultado_OsFsOsFm4)) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           if ( AV140DynamicFiltersOperator4 == 0 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                           }
                           else if ( AV140DynamicFiltersOperator4 == 1 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                           }
                           else if ( AV140DynamicFiltersOperator4 == 2 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                           }
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV81ContagemResultado_OsFsOsFm4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATADMN") == 0 )
                     {
                        AV193ContagemResultado_DataDmn4 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
                        AV194ContagemResultado_DataDmn_To4 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
                        if ( ! (DateTime.MinValue==AV193ContagemResultado_DataDmn4) || ! (DateTime.MinValue==AV194ContagemResultado_DataDmn_To4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data da Dmn";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           GXt_dtime1 = DateTimeUtil.ResetTime( AV193ContagemResultado_DataDmn4 ) ;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                           GXt_dtime1 = DateTimeUtil.ResetTime( AV194ContagemResultado_DataDmn_To4 ) ;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                     {
                        AV195ContagemResultado_DataPrevista4 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
                        AV196ContagemResultado_DataPrevista_To4 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
                        if ( ! (DateTime.MinValue==AV195ContagemResultado_DataPrevista4) || ! (DateTime.MinValue==AV196ContagemResultado_DataPrevista_To4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Prevista";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           GXt_dtime1 = DateTimeUtil.ResetTime( AV195ContagemResultado_DataPrevista4 ) ;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                           GXt_dtime1 = DateTimeUtil.ResetTime( AV196ContagemResultado_DataPrevista_To4 ) ;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICO") == 0 )
                     {
                        AV83ContagemResultado_Servico4 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV83ContagemResultado_Servico4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV83ContagemResultado_Servico4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                     {
                        AV84ContagemResultado_ContadorFM4 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV84ContagemResultado_ContadorFM4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV84ContagemResultado_ContadorFM4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                     {
                        AV85ContagemResultado_SistemaCod4 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV85ContagemResultado_SistemaCod4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV85ContagemResultado_SistemaCod4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                     {
                        AV86ContagemResultado_ContratadaCod4 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV86ContagemResultado_ContratadaCod4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV86ContagemResultado_ContratadaCod4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                     {
                        AV87ContagemResultado_ServicoGrupo4 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV87ContagemResultado_ServicoGrupo4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Grupo de Servi�os";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV87ContagemResultado_ServicoGrupo4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_BASELINE") == 0 )
                     {
                        AV88ContagemResultado_Baseline4 = AV55GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88ContagemResultado_Baseline4)) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                           if ( StringUtil.StrCmp(StringUtil.Trim( AV88ContagemResultado_Baseline4), "/") == 0 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "(Todos)";
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Trim( AV88ContagemResultado_Baseline4), "S") == 0 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Trim( AV88ContagemResultado_Baseline4), "N") == 0 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                           }
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                     {
                        AV89ContagemResultado_NaoCnfDmnCod4 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV89ContagemResultado_NaoCnfDmnCod4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV89ContagemResultado_NaoCnfDmnCod4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_PFBFM") == 0 )
                     {
                        AV90ContagemResultado_PFBFM4 = NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, ".");
                        if ( ! (Convert.ToDecimal(0)==AV90ContagemResultado_PFBFM4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFB Diferentes";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV90ContagemResultado_PFBFM4);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_PFBFS") == 0 )
                     {
                        AV91ContagemResultado_PFBFS4 = NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, ".");
                        if ( ! (Convert.ToDecimal(0)==AV91ContagemResultado_PFBFS4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFL Diferentes";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV91ContagemResultado_PFBFS4);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                     {
                        AV120ContagemResultado_Agrupador4 = AV55GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120ContagemResultado_Agrupador4)) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV120ContagemResultado_Agrupador4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                     {
                        AV135ContagemResultado_Descricao4 = AV55GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV135ContagemResultado_Descricao4)) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV135ContagemResultado_Descricao4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_OWNER") == 0 )
                     {
                        AV130ContagemResultado_Owner4 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV130ContagemResultado_Owner4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Solicitante";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV130ContagemResultado_Owner4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                     {
                        AV151ContagemResultado_TemPndHmlg4 = AV55GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151ContagemResultado_TemPndHmlg4)) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Pend�ncias vinculadas";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                           if ( StringUtil.StrCmp(StringUtil.Trim( AV151ContagemResultado_TemPndHmlg4), "S") == 0 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sem pend�ncias";
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Trim( AV151ContagemResultado_TemPndHmlg4), "C") == 0 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Com pend�ncias";
                           }
                        }
                     }
                     else if ( StringUtil.StrCmp(AV78DynamicFiltersSelector4, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                     {
                        AV210ContagemResultado_CntCod4 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV210ContagemResultado_CntCod4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contrato";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV210ContagemResultado_CntCod4;
                        }
                     }
                     if ( AV54GridState.gxTpr_Dynamicfilters.Count >= 5 )
                     {
                        AV92DynamicFiltersEnabled5 = true;
                        AV55GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV54GridState.gxTpr_Dynamicfilters.Item(5));
                        AV93DynamicFiltersSelector5 = AV55GridStateDynamicFilter.gxTpr_Selected;
                        if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATACNT") == 0 )
                        {
                           AV94ContagemResultado_DataCnt5 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
                           AV95ContagemResultado_DataCnt_To5 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
                           if ( ! (DateTime.MinValue==AV94ContagemResultado_DataCnt5) || ! (DateTime.MinValue==AV95ContagemResultado_DataCnt_To5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data da Cnt";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV94ContagemResultado_DataCnt5 ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV95ContagemResultado_DataCnt_To5 ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                        {
                           AV97ContagemResultado_StatusDmn5 = AV55GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97ContagemResultado_StatusDmn5)) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status da Dmn";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97ContagemResultado_StatusDmn5)) )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV97ContagemResultado_StatusDmn5);
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                        {
                           AV141DynamicFiltersOperator5 = AV55GridStateDynamicFilter.gxTpr_Operator;
                           AV96ContagemResultado_OsFsOsFm5 = AV55GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96ContagemResultado_OsFsOsFm5)) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              if ( AV141DynamicFiltersOperator5 == 0 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                              }
                              else if ( AV141DynamicFiltersOperator5 == 1 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                              }
                              else if ( AV141DynamicFiltersOperator5 == 2 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                              }
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV96ContagemResultado_OsFsOsFm5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATADMN") == 0 )
                        {
                           AV197ContagemResultado_DataDmn5 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
                           AV198ContagemResultado_DataDmn_To5 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
                           if ( ! (DateTime.MinValue==AV197ContagemResultado_DataDmn5) || ! (DateTime.MinValue==AV198ContagemResultado_DataDmn_To5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data da Dmn";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV197ContagemResultado_DataDmn5 ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV198ContagemResultado_DataDmn_To5 ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                        {
                           AV199ContagemResultado_DataPrevista5 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Value, 2);
                           AV200ContagemResultado_DataPrevista_To5 = context.localUtil.CToD( AV55GridStateDynamicFilter.gxTpr_Valueto, 2);
                           if ( ! (DateTime.MinValue==AV199ContagemResultado_DataPrevista5) || ! (DateTime.MinValue==AV200ContagemResultado_DataPrevista_To5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Prevista";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV199ContagemResultado_DataPrevista5 ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV200ContagemResultado_DataPrevista_To5 ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICO") == 0 )
                        {
                           AV98ContagemResultado_Servico5 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV98ContagemResultado_Servico5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV98ContagemResultado_Servico5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                        {
                           AV99ContagemResultado_ContadorFM5 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV99ContagemResultado_ContadorFM5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV99ContagemResultado_ContadorFM5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                        {
                           AV100ContagemResultado_SistemaCod5 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV100ContagemResultado_SistemaCod5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV100ContagemResultado_SistemaCod5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                        {
                           AV101ContagemResultado_ContratadaCod5 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV101ContagemResultado_ContratadaCod5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV101ContagemResultado_ContratadaCod5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                        {
                           AV102ContagemResultado_ServicoGrupo5 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV102ContagemResultado_ServicoGrupo5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Grupo de Servi�os";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV102ContagemResultado_ServicoGrupo5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_BASELINE") == 0 )
                        {
                           AV103ContagemResultado_Baseline5 = AV55GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103ContagemResultado_Baseline5)) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                              if ( StringUtil.StrCmp(StringUtil.Trim( AV103ContagemResultado_Baseline5), "/") == 0 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "(Todos)";
                              }
                              else if ( StringUtil.StrCmp(StringUtil.Trim( AV103ContagemResultado_Baseline5), "S") == 0 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                              }
                              else if ( StringUtil.StrCmp(StringUtil.Trim( AV103ContagemResultado_Baseline5), "N") == 0 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                        {
                           AV104ContagemResultado_NaoCnfDmnCod5 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV104ContagemResultado_NaoCnfDmnCod5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV104ContagemResultado_NaoCnfDmnCod5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_PFBFM") == 0 )
                        {
                           AV105ContagemResultado_PFBFM5 = NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, ".");
                           if ( ! (Convert.ToDecimal(0)==AV105ContagemResultado_PFBFM5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFB Diferentes";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV105ContagemResultado_PFBFM5);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_PFBFS") == 0 )
                        {
                           AV106ContagemResultado_PFBFS5 = NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, ".");
                           if ( ! (Convert.ToDecimal(0)==AV106ContagemResultado_PFBFS5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFL Diferentes";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV106ContagemResultado_PFBFS5);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                        {
                           AV121ContagemResultado_Agrupador5 = AV55GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121ContagemResultado_Agrupador5)) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV121ContagemResultado_Agrupador5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                        {
                           AV136ContagemResultado_Descricao5 = AV55GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV136ContagemResultado_Descricao5)) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV136ContagemResultado_Descricao5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_OWNER") == 0 )
                        {
                           AV131ContagemResultado_Owner5 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV131ContagemResultado_Owner5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Solicitante";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV131ContagemResultado_Owner5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                        {
                           AV153ContagemResultado_TemPndHmlg5 = AV55GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV153ContagemResultado_TemPndHmlg5)) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Pend�ncias vinculadas";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                              if ( StringUtil.StrCmp(StringUtil.Trim( AV153ContagemResultado_TemPndHmlg5), "S") == 0 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sem pend�ncias";
                              }
                              else if ( StringUtil.StrCmp(StringUtil.Trim( AV153ContagemResultado_TemPndHmlg5), "C") == 0 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Com pend�ncias";
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector5, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                        {
                           AV211ContagemResultado_CntCod5 = (int)(NumberUtil.Val( AV55GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV211ContagemResultado_CntCod5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contrato";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV211ContagemResultado_CntCod5;
                           }
                        }
                     }
                  }
               }
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV158TFContagemResultado_Agrupador_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV158TFContagemResultado_Agrupador_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV157TFContagemResultado_Agrupador)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV157TFContagemResultado_Agrupador;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV162TFContagemResultado_DemandaFM_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N� OS";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV162TFContagemResultado_DemandaFM_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV161TFContagemResultado_DemandaFM)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N� OS";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV161TFContagemResultado_DemandaFM;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV160TFContagemResultado_Demanda_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N� OS Ref.";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV160TFContagemResultado_Demanda_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV159TFContagemResultado_Demanda)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N� OS Ref.";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV159TFContagemResultado_Demanda;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV164TFContagemResultado_Descricao_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV164TFContagemResultado_Descricao_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV163TFContagemResultado_Descricao)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV163TFContagemResultado_Descricao;
            }
         }
         if ( ! ( (DateTime.MinValue==AV203TFContagemResultado_DataPrevista) && (DateTime.MinValue==AV204TFContagemResultado_DataPrevista_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Previs�o";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = AV203TFContagemResultado_DataPrevista;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = AV204TFContagemResultado_DataPrevista_To;
         }
         if ( ! ( (DateTime.MinValue==AV201TFContagemResultado_DataDmn) && (DateTime.MinValue==AV202TFContagemResultado_DataDmn_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Demanda";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV201TFContagemResultado_DataDmn ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV202TFContagemResultado_DataDmn_To ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
         }
         if ( ! ( (DateTime.MinValue==AV165TFContagemResultado_DataUltCnt) && (DateTime.MinValue==AV166TFContagemResultado_DataUltCnt_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contagem";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV165TFContagemResultado_DataUltCnt ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV166TFContagemResultado_DataUltCnt_To ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV168TFContagemResultado_ContratadaSigla_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV168TFContagemResultado_ContratadaSigla_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV167TFContagemResultado_ContratadaSigla)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV167TFContagemResultado_ContratadaSigla;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV170TFContagemResultado_SistemaCoord_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Coordena��o";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV170TFContagemResultado_SistemaCoord_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV169TFContagemResultado_SistemaCoord)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Coordena��o";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV169TFContagemResultado_SistemaCoord;
            }
         }
         AV172TFContagemResultado_StatusDmn_Sels.FromJSonString(AV171TFContagemResultado_StatusDmn_SelsJson);
         if ( ! ( ( AV172TFContagemResultado_StatusDmn_Sels.Count == 0 ) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV123i = 1;
            AV216GXV1 = 1;
            while ( AV216GXV1 <= AV172TFContagemResultado_StatusDmn_Sels.Count )
            {
               AV173TFContagemResultado_StatusDmn_Sel = AV172TFContagemResultado_StatusDmn_Sels.GetString(AV216GXV1);
               if ( AV123i == 1 )
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
               }
               else
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+", ";
               }
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+gxdomainstatusdemanda.getDescription(context,AV173TFContagemResultado_StatusDmn_Sel);
               AV123i = (long)(AV123i+1);
               AV216GXV1 = (int)(AV216GXV1+1);
            }
         }
         if ( ! ( (0==AV174TFContagemResultado_Baseline_Sel) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "BS";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            if ( AV174TFContagemResultado_Baseline_Sel == 1 )
            {
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Marcado";
            }
            else if ( AV174TFContagemResultado_Baseline_Sel == 2 )
            {
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Desmarcado";
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV176TFContagemResultado_ServicoSigla_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV176TFContagemResultado_ServicoSigla_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV175TFContagemResultado_ServicoSigla)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV175TFContagemResultado_ServicoSigla;
            }
         }
         if ( ! ( (Convert.ToDecimal(0)==AV177TFContagemResultado_PFFinal) && (Convert.ToDecimal(0)==AV178TFContagemResultado_PFFinal_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Qtd Faturar";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV177TFContagemResultado_PFFinal);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV178TFContagemResultado_PFFinal_To);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV179TFContagemResultado_ValorPF) && (Convert.ToDecimal(0)==AV180TFContagemResultado_ValorPF_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Valor Unit�rio";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV179TFContagemResultado_ValorPF);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV180TFContagemResultado_ValorPF_To);
         }
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S151( )
      {
         /* 'WRITECOLUMNTITLES' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Agrupador";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N� OS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "N� OS Ref.";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Text = "Sistema";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Text = "Titulo";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Text = "Previs�o";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Text = "Demanda";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Text = "Contagem";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = "Solicitante";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Text = "Prestadora";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Text = "Contrato";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Text = "Coordena��o";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Text = "Status";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Text = "BS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Text = "Servi�o";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+15, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+15, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+15, 1, 1).Text = "Qtd Faturar";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+16, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+16, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+16, 1, 1).Text = "Valor Unit�rio";
      }

      protected void S161( )
      {
         /* 'WRITEDATA' Routine */
         AV218ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod = AV18Contratada_AreaTrabalhoCod;
         AV219ExtraWWContagemResultadoExtraSelectionDS_2_Contagemresultado_statuscnt = AV19ContagemResultado_StatusCnt;
         AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = AV21DynamicFiltersSelector1;
         AV221ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 = AV137DynamicFiltersOperator1;
         AV222ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = AV22ContagemResultado_DataCnt1;
         AV223ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = AV23ContagemResultado_DataCnt_To1;
         AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = AV28ContagemResultado_StatusDmn1;
         AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = AV24ContagemResultado_OsFsOsFm1;
         AV226ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 = AV181ContagemResultado_DataDmn1;
         AV227ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 = AV182ContagemResultado_DataDmn_To1;
         AV228ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 = AV183ContagemResultado_DataPrevista1;
         AV229ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 = AV184ContagemResultado_DataPrevista_To1;
         AV230ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 = AV65ContagemResultado_Servico1;
         AV231ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 = AV74ContagemResultado_ContadorFM1;
         AV232ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 = AV25ContagemResultado_SistemaCod1;
         AV233ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 = AV26ContagemResultado_ContratadaCod1;
         AV234ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 = AV71ContagemResultado_ServicoGrupo1;
         AV235ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 = AV59ContagemResultado_Baseline1;
         AV236ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 = AV27ContagemResultado_NaoCnfDmnCod1;
         AV237ExtraWWContagemResultadoExtraSelectionDS_20_Contagemresultado_pfbfm1 = AV29ContagemResultado_PFBFM1;
         AV238ExtraWWContagemResultadoExtraSelectionDS_21_Contagemresultado_pfbfs1 = AV30ContagemResultado_PFBFS1;
         AV239ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 = AV117ContagemResultado_Agrupador1;
         AV240ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = AV132ContagemResultado_Descricao1;
         AV241ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 = AV127ContagemResultado_Owner1;
         AV242ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 = AV145ContagemResultado_TemPndHmlg1;
         AV243ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 = AV207ContagemResultado_CntCod1;
         AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = AV31DynamicFiltersEnabled2;
         AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = AV32DynamicFiltersSelector2;
         AV246ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 = AV138DynamicFiltersOperator2;
         AV247ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = AV33ContagemResultado_DataCnt2;
         AV248ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = AV34ContagemResultado_DataCnt_To2;
         AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = AV39ContagemResultado_StatusDmn2;
         AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = AV35ContagemResultado_OsFsOsFm2;
         AV251ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 = AV185ContagemResultado_DataDmn2;
         AV252ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 = AV186ContagemResultado_DataDmn_To2;
         AV253ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 = AV187ContagemResultado_DataPrevista2;
         AV254ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 = AV188ContagemResultado_DataPrevista_To2;
         AV255ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 = AV66ContagemResultado_Servico2;
         AV256ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 = AV75ContagemResultado_ContadorFM2;
         AV257ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 = AV36ContagemResultado_SistemaCod2;
         AV258ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 = AV37ContagemResultado_ContratadaCod2;
         AV259ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 = AV72ContagemResultado_ServicoGrupo2;
         AV260ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 = AV60ContagemResultado_Baseline2;
         AV261ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 = AV38ContagemResultado_NaoCnfDmnCod2;
         AV262ExtraWWContagemResultadoExtraSelectionDS_45_Contagemresultado_pfbfm2 = AV40ContagemResultado_PFBFM2;
         AV263ExtraWWContagemResultadoExtraSelectionDS_46_Contagemresultado_pfbfs2 = AV41ContagemResultado_PFBFS2;
         AV264ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 = AV118ContagemResultado_Agrupador2;
         AV265ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = AV133ContagemResultado_Descricao2;
         AV266ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 = AV128ContagemResultado_Owner2;
         AV267ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 = AV147ContagemResultado_TemPndHmlg2;
         AV268ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 = AV208ContagemResultado_CntCod2;
         AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = AV42DynamicFiltersEnabled3;
         AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = AV43DynamicFiltersSelector3;
         AV271ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 = AV139DynamicFiltersOperator3;
         AV272ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = AV44ContagemResultado_DataCnt3;
         AV273ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = AV45ContagemResultado_DataCnt_To3;
         AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = AV50ContagemResultado_StatusDmn3;
         AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = AV46ContagemResultado_OsFsOsFm3;
         AV276ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 = AV189ContagemResultado_DataDmn3;
         AV277ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 = AV190ContagemResultado_DataDmn_To3;
         AV278ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 = AV191ContagemResultado_DataPrevista3;
         AV279ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 = AV192ContagemResultado_DataPrevista_To3;
         AV280ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 = AV67ContagemResultado_Servico3;
         AV281ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 = AV76ContagemResultado_ContadorFM3;
         AV282ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 = AV47ContagemResultado_SistemaCod3;
         AV283ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 = AV48ContagemResultado_ContratadaCod3;
         AV284ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 = AV73ContagemResultado_ServicoGrupo3;
         AV285ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 = AV61ContagemResultado_Baseline3;
         AV286ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 = AV49ContagemResultado_NaoCnfDmnCod3;
         AV287ExtraWWContagemResultadoExtraSelectionDS_70_Contagemresultado_pfbfm3 = AV51ContagemResultado_PFBFM3;
         AV288ExtraWWContagemResultadoExtraSelectionDS_71_Contagemresultado_pfbfs3 = AV52ContagemResultado_PFBFS3;
         AV289ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 = AV119ContagemResultado_Agrupador3;
         AV290ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = AV134ContagemResultado_Descricao3;
         AV291ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 = AV129ContagemResultado_Owner3;
         AV292ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 = AV149ContagemResultado_TemPndHmlg3;
         AV293ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 = AV209ContagemResultado_CntCod3;
         AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = AV77DynamicFiltersEnabled4;
         AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = AV78DynamicFiltersSelector4;
         AV296ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 = AV140DynamicFiltersOperator4;
         AV297ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = AV79ContagemResultado_DataCnt4;
         AV298ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = AV80ContagemResultado_DataCnt_To4;
         AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = AV82ContagemResultado_StatusDmn4;
         AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = AV81ContagemResultado_OsFsOsFm4;
         AV301ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 = AV193ContagemResultado_DataDmn4;
         AV302ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 = AV194ContagemResultado_DataDmn_To4;
         AV303ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 = AV195ContagemResultado_DataPrevista4;
         AV304ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 = AV196ContagemResultado_DataPrevista_To4;
         AV305ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 = AV83ContagemResultado_Servico4;
         AV306ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 = AV84ContagemResultado_ContadorFM4;
         AV307ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 = AV85ContagemResultado_SistemaCod4;
         AV308ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 = AV86ContagemResultado_ContratadaCod4;
         AV309ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 = AV87ContagemResultado_ServicoGrupo4;
         AV310ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 = AV88ContagemResultado_Baseline4;
         AV311ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 = AV89ContagemResultado_NaoCnfDmnCod4;
         AV312ExtraWWContagemResultadoExtraSelectionDS_95_Contagemresultado_pfbfm4 = AV90ContagemResultado_PFBFM4;
         AV313ExtraWWContagemResultadoExtraSelectionDS_96_Contagemresultado_pfbfs4 = AV91ContagemResultado_PFBFS4;
         AV314ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 = AV120ContagemResultado_Agrupador4;
         AV315ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = AV135ContagemResultado_Descricao4;
         AV316ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 = AV130ContagemResultado_Owner4;
         AV317ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 = AV151ContagemResultado_TemPndHmlg4;
         AV318ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 = AV210ContagemResultado_CntCod4;
         AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = AV92DynamicFiltersEnabled5;
         AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = AV93DynamicFiltersSelector5;
         AV321ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 = AV141DynamicFiltersOperator5;
         AV322ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = AV94ContagemResultado_DataCnt5;
         AV323ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = AV95ContagemResultado_DataCnt_To5;
         AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = AV97ContagemResultado_StatusDmn5;
         AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = AV96ContagemResultado_OsFsOsFm5;
         AV326ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 = AV197ContagemResultado_DataDmn5;
         AV327ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 = AV198ContagemResultado_DataDmn_To5;
         AV328ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 = AV199ContagemResultado_DataPrevista5;
         AV329ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 = AV200ContagemResultado_DataPrevista_To5;
         AV330ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 = AV98ContagemResultado_Servico5;
         AV331ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 = AV99ContagemResultado_ContadorFM5;
         AV332ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 = AV100ContagemResultado_SistemaCod5;
         AV333ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 = AV101ContagemResultado_ContratadaCod5;
         AV334ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 = AV102ContagemResultado_ServicoGrupo5;
         AV335ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 = AV103ContagemResultado_Baseline5;
         AV336ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 = AV104ContagemResultado_NaoCnfDmnCod5;
         AV337ExtraWWContagemResultadoExtraSelectionDS_120_Contagemresultado_pfbfm5 = AV105ContagemResultado_PFBFM5;
         AV338ExtraWWContagemResultadoExtraSelectionDS_121_Contagemresultado_pfbfs5 = AV106ContagemResultado_PFBFS5;
         AV339ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 = AV121ContagemResultado_Agrupador5;
         AV340ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = AV136ContagemResultado_Descricao5;
         AV341ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 = AV131ContagemResultado_Owner5;
         AV342ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 = AV153ContagemResultado_TemPndHmlg5;
         AV343ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 = AV211ContagemResultado_CntCod5;
         AV344ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = AV157TFContagemResultado_Agrupador;
         AV345ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel = AV158TFContagemResultado_Agrupador_Sel;
         AV346ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = AV161TFContagemResultado_DemandaFM;
         AV347ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel = AV162TFContagemResultado_DemandaFM_Sel;
         AV348ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = AV159TFContagemResultado_Demanda;
         AV349ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel = AV160TFContagemResultado_Demanda_Sel;
         AV350ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = AV163TFContagemResultado_Descricao;
         AV351ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel = AV164TFContagemResultado_Descricao_Sel;
         AV352ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista = AV203TFContagemResultado_DataPrevista;
         AV353ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to = AV204TFContagemResultado_DataPrevista_To;
         AV354ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn = AV201TFContagemResultado_DataDmn;
         AV355ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to = AV202TFContagemResultado_DataDmn_To;
         AV356ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = AV165TFContagemResultado_DataUltCnt;
         AV357ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = AV166TFContagemResultado_DataUltCnt_To;
         AV358ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = AV167TFContagemResultado_ContratadaSigla;
         AV359ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel = AV168TFContagemResultado_ContratadaSigla_Sel;
         AV360ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = AV169TFContagemResultado_SistemaCoord;
         AV361ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel = AV170TFContagemResultado_SistemaCoord_Sel;
         AV362ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels = AV172TFContagemResultado_StatusDmn_Sels;
         AV363ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel = AV174TFContagemResultado_Baseline_Sel;
         AV364ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = AV175TFContagemResultado_ServicoSigla;
         AV365ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel = AV176TFContagemResultado_ServicoSigla_Sel;
         AV366ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal = AV177TFContagemResultado_PFFinal;
         AV367ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to = AV178TFContagemResultado_PFFinal_To;
         AV368ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf = AV179TFContagemResultado_ValorPF;
         AV369ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to = AV180TFContagemResultado_ValorPF_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV362ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ,
                                              A490ContagemResultado_ContratadaCod ,
                                              AV142Contratadas ,
                                              A601ContagemResultado_Servico ,
                                              AV126ServicosGeridos ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ,
                                              AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ,
                                              AV221ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ,
                                              AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ,
                                              AV226ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ,
                                              AV227ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ,
                                              AV228ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ,
                                              AV229ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ,
                                              AV230ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ,
                                              AV232ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ,
                                              AV233ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ,
                                              AV218ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ,
                                              AV234ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ,
                                              AV235ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ,
                                              AV236ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ,
                                              AV239ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ,
                                              AV240ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ,
                                              AV241ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ,
                                              AV243ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ,
                                              AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ,
                                              AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ,
                                              AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ,
                                              AV246ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ,
                                              AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ,
                                              AV251ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ,
                                              AV252ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ,
                                              AV253ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ,
                                              AV254ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ,
                                              AV255ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ,
                                              AV257ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ,
                                              AV258ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ,
                                              AV259ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ,
                                              AV260ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ,
                                              AV261ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ,
                                              AV264ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ,
                                              AV265ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ,
                                              AV266ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ,
                                              AV268ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ,
                                              AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ,
                                              AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ,
                                              AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ,
                                              AV271ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ,
                                              AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ,
                                              AV276ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ,
                                              AV277ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ,
                                              AV278ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ,
                                              AV279ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ,
                                              AV280ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ,
                                              AV282ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ,
                                              AV283ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ,
                                              AV284ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ,
                                              AV285ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ,
                                              AV286ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ,
                                              AV289ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ,
                                              AV290ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ,
                                              AV291ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ,
                                              AV293ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ,
                                              AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ,
                                              AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ,
                                              AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ,
                                              AV296ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ,
                                              AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ,
                                              AV301ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ,
                                              AV302ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ,
                                              AV303ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ,
                                              AV304ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ,
                                              AV305ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ,
                                              AV307ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ,
                                              AV308ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ,
                                              AV309ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ,
                                              AV310ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ,
                                              AV311ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ,
                                              AV314ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ,
                                              AV315ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ,
                                              AV316ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ,
                                              AV318ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ,
                                              AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ,
                                              AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ,
                                              AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ,
                                              AV321ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ,
                                              AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ,
                                              AV326ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ,
                                              AV327ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ,
                                              AV328ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ,
                                              AV329ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ,
                                              AV330ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ,
                                              AV332ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ,
                                              AV333ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ,
                                              AV334ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ,
                                              AV335ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ,
                                              AV336ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ,
                                              AV339ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ,
                                              AV340ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ,
                                              AV341ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ,
                                              AV343ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ,
                                              AV345ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ,
                                              AV344ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ,
                                              AV347ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ,
                                              AV346ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ,
                                              AV349ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ,
                                              AV348ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ,
                                              AV351ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ,
                                              AV350ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ,
                                              AV352ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ,
                                              AV353ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ,
                                              AV354ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ,
                                              AV355ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ,
                                              AV359ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                              AV358ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ,
                                              AV361ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ,
                                              AV360ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ,
                                              AV362ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV363ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ,
                                              AV365ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ,
                                              AV364ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ,
                                              AV368ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ,
                                              AV369ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ,
                                              AV54GridState.gxTpr_Dynamicfilters.Count ,
                                              AV142Contratadas.Count ,
                                              AV9WWPContext.gxTpr_Userehfinanceiro ,
                                              AV9WWPContext.gxTpr_Userehgestor ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A1351ContagemResultado_DataPrevista ,
                                              A489ContagemResultado_SistemaCod ,
                                              A764ContagemResultado_ServicoGrupo ,
                                              A598ContagemResultado_Baseline ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A1046ContagemResultado_Agrupador ,
                                              A494ContagemResultado_Descricao ,
                                              A508ContagemResultado_Owner ,
                                              A1603ContagemResultado_CntCod ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A515ContagemResultado_SistemaCoord ,
                                              A801ContagemResultado_ServicoSigla ,
                                              A512ContagemResultado_ValorPF ,
                                              A456ContagemResultado_Codigo ,
                                              AV16OrderedBy ,
                                              AV17OrderedDsc ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A1854ContagemResultado_VlrCnc ,
                                              AV222ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV223ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ,
                                              AV231ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              A682ContagemResultado_PFBFMUltima ,
                                              A685ContagemResultado_PFLFSUltima ,
                                              A683ContagemResultado_PFLFMUltima ,
                                              AV242ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ,
                                              A1802ContagemResultado_TemPndHmlg ,
                                              AV247ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ,
                                              AV248ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ,
                                              AV256ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ,
                                              AV267ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ,
                                              AV272ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ,
                                              AV273ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ,
                                              AV281ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ,
                                              AV292ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ,
                                              AV297ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ,
                                              AV298ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ,
                                              AV306ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ,
                                              AV317ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ,
                                              AV322ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ,
                                              AV323ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ,
                                              AV331ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ,
                                              AV342ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ,
                                              AV356ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ,
                                              AV357ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ,
                                              AV366ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV367ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT
                                              }
         });
         lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV240ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV240ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1), "%", "");
         lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV265ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV265ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2), "%", "");
         lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV290ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV290ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3), "%", "");
         lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV315ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = StringUtil.Concat( StringUtil.RTrim( AV315ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4), "%", "");
         lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV340ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = StringUtil.Concat( StringUtil.RTrim( AV340ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5), "%", "");
         lV344ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = StringUtil.PadR( StringUtil.RTrim( AV344ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador), 15, "%");
         lV346ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV346ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm), "%", "");
         lV348ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV348ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda), "%", "");
         lV350ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV350ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao), "%", "");
         lV358ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV358ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla), 15, "%");
         lV360ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = StringUtil.Concat( StringUtil.RTrim( AV360ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord), "%", "");
         lV364ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV364ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P003U4 */
         pr_default.execute(0, new Object[] {AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV222ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1, AV222ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1, AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV223ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1, AV223ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1, AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV231ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV231ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV231ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV247ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2, AV247ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2, AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV248ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2, AV248ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2, AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV256ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV256ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV256ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV272ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3, AV272ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3, AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV273ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3, AV273ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3, AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV281ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV281ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV281ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV297ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4, AV297ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4, AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV298ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4, AV298ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4, AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV306ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV306ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV306ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV322ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5, AV322ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5, AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV323ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5, AV323ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5, AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV331ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV331ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV331ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV356ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt, AV356ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt, AV357ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to, AV357ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, AV226ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1, AV227ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1, AV228ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1, AV229ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1, AV230ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1, AV232ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1, AV233ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1, AV234ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1, AV236ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1, AV239ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1, lV240ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1, AV241ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1, AV243ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1, AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, AV251ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2, AV252ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2, AV253ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2, AV254ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2,
         AV255ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2, AV257ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2, AV258ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2, AV259ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2, AV261ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2, AV264ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2, lV265ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2, AV266ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2, AV268ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2, AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, AV276ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3, AV277ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3, AV278ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3, AV279ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3, AV280ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3, AV282ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3, AV283ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3, AV284ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3, AV286ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3, AV289ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3, lV290ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3, AV291ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3, AV293ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3, AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, AV301ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4, AV302ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4, AV303ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4, AV304ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4, AV305ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4, AV307ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4, AV308ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4, AV309ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4, AV311ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4, AV314ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4, lV315ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4, AV316ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4, AV318ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4, AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, AV326ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5, AV327ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5, AV328ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5, AV329ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5, AV330ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5, AV332ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5, AV333ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5, AV334ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5, AV336ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5, AV339ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5, lV340ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5, AV341ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5, AV343ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5, lV344ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador, AV345ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel, lV346ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm, AV347ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel, lV348ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda, AV349ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel, lV350ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao, AV351ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel, AV352ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista, AV353ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to, AV354ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn, AV355ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to, lV358ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla, AV359ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel, lV360ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord, AV361ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel, lV364ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla, AV365ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel, AV368ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf, AV369ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P003U4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003U4_n1553ContagemResultado_CntSrvCod[0];
            A1583ContagemResultado_TipoRegistro = P003U4_A1583ContagemResultado_TipoRegistro[0];
            A512ContagemResultado_ValorPF = P003U4_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P003U4_n512ContagemResultado_ValorPF[0];
            A801ContagemResultado_ServicoSigla = P003U4_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P003U4_n801ContagemResultado_ServicoSigla[0];
            A515ContagemResultado_SistemaCoord = P003U4_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P003U4_n515ContagemResultado_SistemaCoord[0];
            A803ContagemResultado_ContratadaSigla = P003U4_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P003U4_n803ContagemResultado_ContratadaSigla[0];
            A1603ContagemResultado_CntCod = P003U4_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P003U4_n1603ContagemResultado_CntCod[0];
            A494ContagemResultado_Descricao = P003U4_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P003U4_n494ContagemResultado_Descricao[0];
            A1046ContagemResultado_Agrupador = P003U4_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P003U4_n1046ContagemResultado_Agrupador[0];
            A468ContagemResultado_NaoCnfDmnCod = P003U4_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P003U4_n468ContagemResultado_NaoCnfDmnCod[0];
            A598ContagemResultado_Baseline = P003U4_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P003U4_n598ContagemResultado_Baseline[0];
            A764ContagemResultado_ServicoGrupo = P003U4_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P003U4_n764ContagemResultado_ServicoGrupo[0];
            A489ContagemResultado_SistemaCod = P003U4_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P003U4_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P003U4_A508ContagemResultado_Owner[0];
            A601ContagemResultado_Servico = P003U4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P003U4_n601ContagemResultado_Servico[0];
            A1351ContagemResultado_DataPrevista = P003U4_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P003U4_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P003U4_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P003U4_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P003U4_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P003U4_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P003U4_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = P003U4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P003U4_n484ContagemResultado_StatusDmn[0];
            A1854ContagemResultado_VlrCnc = P003U4_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P003U4_n1854ContagemResultado_VlrCnc[0];
            A490ContagemResultado_ContratadaCod = P003U4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P003U4_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P003U4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P003U4_n52Contratada_AreaTrabalhoCod[0];
            A495ContagemResultado_SistemaNom = P003U4_A495ContagemResultado_SistemaNom[0];
            n495ContagemResultado_SistemaNom = P003U4_n495ContagemResultado_SistemaNom[0];
            A1612ContagemResultado_CntNum = P003U4_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P003U4_n1612ContagemResultado_CntNum[0];
            A683ContagemResultado_PFLFMUltima = P003U4_A683ContagemResultado_PFLFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P003U4_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P003U4_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P003U4_A684ContagemResultado_PFBFSUltima[0];
            A584ContagemResultado_ContadorFM = P003U4_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P003U4_A566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P003U4_A531ContagemResultado_StatusUltCnt[0];
            A2118ContagemResultado_Owner_Identificao = P003U4_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P003U4_n2118ContagemResultado_Owner_Identificao[0];
            A456ContagemResultado_Codigo = P003U4_A456ContagemResultado_Codigo[0];
            A1603ContagemResultado_CntCod = P003U4_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P003U4_n1603ContagemResultado_CntCod[0];
            A601ContagemResultado_Servico = P003U4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P003U4_n601ContagemResultado_Servico[0];
            A1612ContagemResultado_CntNum = P003U4_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P003U4_n1612ContagemResultado_CntNum[0];
            A801ContagemResultado_ServicoSigla = P003U4_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P003U4_n801ContagemResultado_ServicoSigla[0];
            A764ContagemResultado_ServicoGrupo = P003U4_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P003U4_n764ContagemResultado_ServicoGrupo[0];
            A515ContagemResultado_SistemaCoord = P003U4_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P003U4_n515ContagemResultado_SistemaCoord[0];
            A495ContagemResultado_SistemaNom = P003U4_A495ContagemResultado_SistemaNom[0];
            n495ContagemResultado_SistemaNom = P003U4_n495ContagemResultado_SistemaNom[0];
            A2118ContagemResultado_Owner_Identificao = P003U4_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P003U4_n2118ContagemResultado_Owner_Identificao[0];
            A803ContagemResultado_ContratadaSigla = P003U4_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P003U4_n803ContagemResultado_ContratadaSigla[0];
            A52Contratada_AreaTrabalhoCod = P003U4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P003U4_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P003U4_A683ContagemResultado_PFLFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P003U4_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P003U4_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P003U4_A684ContagemResultado_PFBFSUltima[0];
            A584ContagemResultado_ContadorFM = P003U4_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P003U4_A566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P003U4_A531ContagemResultado_StatusUltCnt[0];
            GXt_boolean2 = A1802ContagemResultado_TemPndHmlg;
            new prc_tempndparahomologar(context ).execute( ref  A456ContagemResultado_Codigo, out  GXt_boolean2) ;
            A1802ContagemResultado_TemPndHmlg = GXt_boolean2;
            if ( ! ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
            {
               if ( ! ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
               {
                  if ( ! ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                  {
                     if ( ! ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                     {
                        if ( ! ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV292ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                        {
                           if ( ! ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV292ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                           {
                              if ( ! ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV317ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                              {
                                 if ( ! ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV317ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                                 {
                                    if ( ! ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV342ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                                    {
                                       if ( ! ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV342ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                                       {
                                          GXt_decimal3 = A574ContagemResultado_PFFinal;
                                          new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
                                          A574ContagemResultado_PFFinal = GXt_decimal3;
                                          if ( (Convert.ToDecimal(0)==AV366ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV366ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ) ) )
                                          {
                                             if ( (Convert.ToDecimal(0)==AV367ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV367ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ) ) )
                                             {
                                                AV13CellRow = (int)(AV13CellRow+1);
                                                /* Execute user subroutine: 'BEFOREWRITELINE' */
                                                S172 ();
                                                if ( returnInSub )
                                                {
                                                   pr_default.close(0);
                                                   returnInSub = true;
                                                   if (true) return;
                                                }
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = A1046ContagemResultado_Agrupador;
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = A493ContagemResultado_DemandaFM;
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = A457ContagemResultado_Demanda;
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Text = A495ContagemResultado_SistemaNom;
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Text = A494ContagemResultado_Descricao;
                                                AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Date = A1351ContagemResultado_DataPrevista;
                                                GXt_dtime1 = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
                                                AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Date = GXt_dtime1;
                                                GXt_dtime1 = DateTimeUtil.ResetTime( A566ContagemResultado_DataUltCnt ) ;
                                                AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Date = GXt_dtime1;
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = A2118ContagemResultado_Owner_Identificao;
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Text = A803ContagemResultado_ContratadaSigla;
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Text = A1612ContagemResultado_CntNum;
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Text = A515ContagemResultado_SistemaCoord;
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Text = "";
                                                if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "True") == 0 )
                                                {
                                                   AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Text = "*";
                                                }
                                                else if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "False") == 0 )
                                                {
                                                   AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Text = "";
                                                }
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Text = A801ContagemResultado_ServicoSigla;
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+15, 1, 1).Number = (double)(A574ContagemResultado_PFFinal);
                                                AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+16, 1, 1).Number = (double)(A512ContagemResultado_ValorPF);
                                                /* Execute user subroutine: 'AFTERWRITELINE' */
                                                S182 ();
                                                if ( returnInSub )
                                                {
                                                   pr_default.close(0);
                                                   returnInSub = true;
                                                   if (true) return;
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S191( )
      {
         /* 'CLOSEDOCUMENT' Routine */
         AV10ExcelDocument.Save();
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Close();
      }

      protected void S121( )
      {
         /* 'CHECKSTATUS' Routine */
         if ( AV10ExcelDocument.ErrCode != 0 )
         {
            AV11Filename = "";
            AV12ErrorMessage = AV10ExcelDocument.ErrDescription;
            AV10ExcelDocument.Close();
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'BEFOREWRITELINE' Routine */
      }

      protected void S182( )
      {
         /* 'AFTERWRITELINE' Routine */
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV142Contratadas = new GxSimpleCollection();
         AV143WebSession = context.GetSession();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10ExcelDocument = new ExcelDocumentI();
         AV126ServicosGeridos = new GxSimpleCollection();
         AV54GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV55GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV21DynamicFiltersSelector1 = "";
         AV22ContagemResultado_DataCnt1 = DateTime.MinValue;
         AV23ContagemResultado_DataCnt_To1 = DateTime.MinValue;
         AV28ContagemResultado_StatusDmn1 = "";
         AV24ContagemResultado_OsFsOsFm1 = "";
         AV181ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV182ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV183ContagemResultado_DataPrevista1 = DateTime.MinValue;
         AV184ContagemResultado_DataPrevista_To1 = DateTime.MinValue;
         AV59ContagemResultado_Baseline1 = "";
         AV117ContagemResultado_Agrupador1 = "";
         AV132ContagemResultado_Descricao1 = "";
         AV145ContagemResultado_TemPndHmlg1 = "";
         AV32DynamicFiltersSelector2 = "";
         AV33ContagemResultado_DataCnt2 = DateTime.MinValue;
         AV34ContagemResultado_DataCnt_To2 = DateTime.MinValue;
         AV39ContagemResultado_StatusDmn2 = "";
         AV35ContagemResultado_OsFsOsFm2 = "";
         AV185ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV186ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV187ContagemResultado_DataPrevista2 = DateTime.MinValue;
         AV188ContagemResultado_DataPrevista_To2 = DateTime.MinValue;
         AV60ContagemResultado_Baseline2 = "";
         AV118ContagemResultado_Agrupador2 = "";
         AV133ContagemResultado_Descricao2 = "";
         AV147ContagemResultado_TemPndHmlg2 = "";
         AV43DynamicFiltersSelector3 = "";
         AV44ContagemResultado_DataCnt3 = DateTime.MinValue;
         AV45ContagemResultado_DataCnt_To3 = DateTime.MinValue;
         AV50ContagemResultado_StatusDmn3 = "";
         AV46ContagemResultado_OsFsOsFm3 = "";
         AV189ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV190ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV191ContagemResultado_DataPrevista3 = DateTime.MinValue;
         AV192ContagemResultado_DataPrevista_To3 = DateTime.MinValue;
         AV61ContagemResultado_Baseline3 = "";
         AV119ContagemResultado_Agrupador3 = "";
         AV134ContagemResultado_Descricao3 = "";
         AV149ContagemResultado_TemPndHmlg3 = "";
         AV78DynamicFiltersSelector4 = "";
         AV79ContagemResultado_DataCnt4 = DateTime.MinValue;
         AV80ContagemResultado_DataCnt_To4 = DateTime.MinValue;
         AV82ContagemResultado_StatusDmn4 = "";
         AV81ContagemResultado_OsFsOsFm4 = "";
         AV193ContagemResultado_DataDmn4 = DateTime.MinValue;
         AV194ContagemResultado_DataDmn_To4 = DateTime.MinValue;
         AV195ContagemResultado_DataPrevista4 = DateTime.MinValue;
         AV196ContagemResultado_DataPrevista_To4 = DateTime.MinValue;
         AV88ContagemResultado_Baseline4 = "";
         AV120ContagemResultado_Agrupador4 = "";
         AV135ContagemResultado_Descricao4 = "";
         AV151ContagemResultado_TemPndHmlg4 = "";
         AV93DynamicFiltersSelector5 = "";
         AV94ContagemResultado_DataCnt5 = DateTime.MinValue;
         AV95ContagemResultado_DataCnt_To5 = DateTime.MinValue;
         AV97ContagemResultado_StatusDmn5 = "";
         AV96ContagemResultado_OsFsOsFm5 = "";
         AV197ContagemResultado_DataDmn5 = DateTime.MinValue;
         AV198ContagemResultado_DataDmn_To5 = DateTime.MinValue;
         AV199ContagemResultado_DataPrevista5 = DateTime.MinValue;
         AV200ContagemResultado_DataPrevista_To5 = DateTime.MinValue;
         AV103ContagemResultado_Baseline5 = "";
         AV121ContagemResultado_Agrupador5 = "";
         AV136ContagemResultado_Descricao5 = "";
         AV153ContagemResultado_TemPndHmlg5 = "";
         AV172TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV173TFContagemResultado_StatusDmn_Sel = "";
         AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = "";
         AV222ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = DateTime.MinValue;
         AV223ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = DateTime.MinValue;
         AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = "";
         AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = "";
         AV226ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV227ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV228ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 = DateTime.MinValue;
         AV229ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 = DateTime.MinValue;
         AV235ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 = "";
         AV239ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 = "";
         AV240ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = "";
         AV242ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 = "";
         AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = "";
         AV247ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = DateTime.MinValue;
         AV248ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = DateTime.MinValue;
         AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = "";
         AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = "";
         AV251ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV252ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV253ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 = DateTime.MinValue;
         AV254ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 = DateTime.MinValue;
         AV260ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 = "";
         AV264ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 = "";
         AV265ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = "";
         AV267ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 = "";
         AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = "";
         AV272ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = DateTime.MinValue;
         AV273ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = DateTime.MinValue;
         AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = "";
         AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = "";
         AV276ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV277ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV278ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 = DateTime.MinValue;
         AV279ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 = DateTime.MinValue;
         AV285ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 = "";
         AV289ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 = "";
         AV290ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = "";
         AV292ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 = "";
         AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = "";
         AV297ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = DateTime.MinValue;
         AV298ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = DateTime.MinValue;
         AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = "";
         AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = "";
         AV301ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 = DateTime.MinValue;
         AV302ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 = DateTime.MinValue;
         AV303ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 = DateTime.MinValue;
         AV304ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 = DateTime.MinValue;
         AV310ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 = "";
         AV314ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 = "";
         AV315ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = "";
         AV317ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 = "";
         AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = "";
         AV322ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = DateTime.MinValue;
         AV323ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = DateTime.MinValue;
         AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = "";
         AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = "";
         AV326ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 = DateTime.MinValue;
         AV327ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 = DateTime.MinValue;
         AV328ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 = DateTime.MinValue;
         AV329ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 = DateTime.MinValue;
         AV335ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 = "";
         AV339ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 = "";
         AV340ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = "";
         AV342ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 = "";
         AV344ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = "";
         AV345ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel = "";
         AV346ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = "";
         AV347ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel = "";
         AV348ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = "";
         AV349ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel = "";
         AV350ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = "";
         AV351ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel = "";
         AV352ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
         AV353ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to = (DateTime)(DateTime.MinValue);
         AV354ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV355ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV356ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = DateTime.MinValue;
         AV357ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = DateTime.MinValue;
         AV358ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = "";
         AV359ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel = "";
         AV360ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = "";
         AV361ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel = "";
         AV362ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels = new GxSimpleCollection();
         AV364ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = "";
         AV365ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel = "";
         scmdbuf = "";
         lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = "";
         lV240ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = "";
         lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = "";
         lV265ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = "";
         lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = "";
         lV290ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = "";
         lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = "";
         lV315ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = "";
         lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = "";
         lV340ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = "";
         lV344ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = "";
         lV346ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = "";
         lV348ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = "";
         lV350ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = "";
         lV358ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = "";
         lV360ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = "";
         lV364ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1046ContagemResultado_Agrupador = "";
         A494ContagemResultado_Descricao = "";
         A803ContagemResultado_ContratadaSigla = "";
         A515ContagemResultado_SistemaCoord = "";
         A801ContagemResultado_ServicoSigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P003U4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P003U4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P003U4_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P003U4_A512ContagemResultado_ValorPF = new decimal[1] ;
         P003U4_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P003U4_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P003U4_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P003U4_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P003U4_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P003U4_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P003U4_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P003U4_A1603ContagemResultado_CntCod = new int[1] ;
         P003U4_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P003U4_A494ContagemResultado_Descricao = new String[] {""} ;
         P003U4_n494ContagemResultado_Descricao = new bool[] {false} ;
         P003U4_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P003U4_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P003U4_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P003U4_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P003U4_A598ContagemResultado_Baseline = new bool[] {false} ;
         P003U4_n598ContagemResultado_Baseline = new bool[] {false} ;
         P003U4_A764ContagemResultado_ServicoGrupo = new int[1] ;
         P003U4_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         P003U4_A489ContagemResultado_SistemaCod = new int[1] ;
         P003U4_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P003U4_A508ContagemResultado_Owner = new int[1] ;
         P003U4_A601ContagemResultado_Servico = new int[1] ;
         P003U4_n601ContagemResultado_Servico = new bool[] {false} ;
         P003U4_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P003U4_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P003U4_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P003U4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P003U4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P003U4_A457ContagemResultado_Demanda = new String[] {""} ;
         P003U4_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003U4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003U4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003U4_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P003U4_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P003U4_A490ContagemResultado_ContratadaCod = new int[1] ;
         P003U4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P003U4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003U4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003U4_A495ContagemResultado_SistemaNom = new String[] {""} ;
         P003U4_n495ContagemResultado_SistemaNom = new bool[] {false} ;
         P003U4_A1612ContagemResultado_CntNum = new String[] {""} ;
         P003U4_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P003U4_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P003U4_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P003U4_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P003U4_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P003U4_A584ContagemResultado_ContadorFM = new int[1] ;
         P003U4_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P003U4_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P003U4_A2118ContagemResultado_Owner_Identificao = new String[] {""} ;
         P003U4_n2118ContagemResultado_Owner_Identificao = new bool[] {false} ;
         P003U4_A456ContagemResultado_Codigo = new int[1] ;
         A495ContagemResultado_SistemaNom = "";
         A1612ContagemResultado_CntNum = "";
         A2118ContagemResultado_Owner_Identificao = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.exportextrawwcontagemresultadoextraselection__default(),
            new Object[][] {
                new Object[] {
               P003U4_A1553ContagemResultado_CntSrvCod, P003U4_n1553ContagemResultado_CntSrvCod, P003U4_A1583ContagemResultado_TipoRegistro, P003U4_A512ContagemResultado_ValorPF, P003U4_n512ContagemResultado_ValorPF, P003U4_A801ContagemResultado_ServicoSigla, P003U4_n801ContagemResultado_ServicoSigla, P003U4_A515ContagemResultado_SistemaCoord, P003U4_n515ContagemResultado_SistemaCoord, P003U4_A803ContagemResultado_ContratadaSigla,
               P003U4_n803ContagemResultado_ContratadaSigla, P003U4_A1603ContagemResultado_CntCod, P003U4_n1603ContagemResultado_CntCod, P003U4_A494ContagemResultado_Descricao, P003U4_n494ContagemResultado_Descricao, P003U4_A1046ContagemResultado_Agrupador, P003U4_n1046ContagemResultado_Agrupador, P003U4_A468ContagemResultado_NaoCnfDmnCod, P003U4_n468ContagemResultado_NaoCnfDmnCod, P003U4_A598ContagemResultado_Baseline,
               P003U4_n598ContagemResultado_Baseline, P003U4_A764ContagemResultado_ServicoGrupo, P003U4_n764ContagemResultado_ServicoGrupo, P003U4_A489ContagemResultado_SistemaCod, P003U4_n489ContagemResultado_SistemaCod, P003U4_A508ContagemResultado_Owner, P003U4_A601ContagemResultado_Servico, P003U4_n601ContagemResultado_Servico, P003U4_A1351ContagemResultado_DataPrevista, P003U4_n1351ContagemResultado_DataPrevista,
               P003U4_A471ContagemResultado_DataDmn, P003U4_A493ContagemResultado_DemandaFM, P003U4_n493ContagemResultado_DemandaFM, P003U4_A457ContagemResultado_Demanda, P003U4_n457ContagemResultado_Demanda, P003U4_A484ContagemResultado_StatusDmn, P003U4_n484ContagemResultado_StatusDmn, P003U4_A1854ContagemResultado_VlrCnc, P003U4_n1854ContagemResultado_VlrCnc, P003U4_A490ContagemResultado_ContratadaCod,
               P003U4_n490ContagemResultado_ContratadaCod, P003U4_A52Contratada_AreaTrabalhoCod, P003U4_n52Contratada_AreaTrabalhoCod, P003U4_A495ContagemResultado_SistemaNom, P003U4_n495ContagemResultado_SistemaNom, P003U4_A1612ContagemResultado_CntNum, P003U4_n1612ContagemResultado_CntNum, P003U4_A683ContagemResultado_PFLFMUltima, P003U4_A685ContagemResultado_PFLFSUltima, P003U4_A682ContagemResultado_PFBFMUltima,
               P003U4_A684ContagemResultado_PFBFSUltima, P003U4_A584ContagemResultado_ContadorFM, P003U4_A566ContagemResultado_DataUltCnt, P003U4_A531ContagemResultado_StatusUltCnt, P003U4_A2118ContagemResultado_Owner_Identificao, P003U4_n2118ContagemResultado_Owner_Identificao, P003U4_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV19ContagemResultado_StatusCnt ;
      private short AV174TFContagemResultado_Baseline_Sel ;
      private short AV16OrderedBy ;
      private short AV124p ;
      private short AV137DynamicFiltersOperator1 ;
      private short AV138DynamicFiltersOperator2 ;
      private short AV139DynamicFiltersOperator3 ;
      private short AV140DynamicFiltersOperator4 ;
      private short AV141DynamicFiltersOperator5 ;
      private short AV219ExtraWWContagemResultadoExtraSelectionDS_2_Contagemresultado_statuscnt ;
      private short AV221ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ;
      private short AV246ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ;
      private short AV271ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ;
      private short AV296ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ;
      private short AV321ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ;
      private short AV363ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short A1583ContagemResultado_TipoRegistro ;
      private int AV18Contratada_AreaTrabalhoCod ;
      private int AV13CellRow ;
      private int AV14FirstColumn ;
      private int AV15Random ;
      private int AV125Servico ;
      private int AV65ContagemResultado_Servico1 ;
      private int AV74ContagemResultado_ContadorFM1 ;
      private int AV25ContagemResultado_SistemaCod1 ;
      private int AV26ContagemResultado_ContratadaCod1 ;
      private int AV71ContagemResultado_ServicoGrupo1 ;
      private int AV27ContagemResultado_NaoCnfDmnCod1 ;
      private int AV127ContagemResultado_Owner1 ;
      private int AV207ContagemResultado_CntCod1 ;
      private int AV66ContagemResultado_Servico2 ;
      private int AV75ContagemResultado_ContadorFM2 ;
      private int AV36ContagemResultado_SistemaCod2 ;
      private int AV37ContagemResultado_ContratadaCod2 ;
      private int AV72ContagemResultado_ServicoGrupo2 ;
      private int AV38ContagemResultado_NaoCnfDmnCod2 ;
      private int AV128ContagemResultado_Owner2 ;
      private int AV208ContagemResultado_CntCod2 ;
      private int AV67ContagemResultado_Servico3 ;
      private int AV76ContagemResultado_ContadorFM3 ;
      private int AV47ContagemResultado_SistemaCod3 ;
      private int AV48ContagemResultado_ContratadaCod3 ;
      private int AV73ContagemResultado_ServicoGrupo3 ;
      private int AV49ContagemResultado_NaoCnfDmnCod3 ;
      private int AV129ContagemResultado_Owner3 ;
      private int AV209ContagemResultado_CntCod3 ;
      private int AV83ContagemResultado_Servico4 ;
      private int AV84ContagemResultado_ContadorFM4 ;
      private int AV85ContagemResultado_SistemaCod4 ;
      private int AV86ContagemResultado_ContratadaCod4 ;
      private int AV87ContagemResultado_ServicoGrupo4 ;
      private int AV89ContagemResultado_NaoCnfDmnCod4 ;
      private int AV130ContagemResultado_Owner4 ;
      private int AV210ContagemResultado_CntCod4 ;
      private int AV98ContagemResultado_Servico5 ;
      private int AV99ContagemResultado_ContadorFM5 ;
      private int AV100ContagemResultado_SistemaCod5 ;
      private int AV101ContagemResultado_ContratadaCod5 ;
      private int AV102ContagemResultado_ServicoGrupo5 ;
      private int AV104ContagemResultado_NaoCnfDmnCod5 ;
      private int AV131ContagemResultado_Owner5 ;
      private int AV211ContagemResultado_CntCod5 ;
      private int AV216GXV1 ;
      private int AV218ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ;
      private int AV230ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ;
      private int AV231ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ;
      private int AV232ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ;
      private int AV233ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ;
      private int AV234ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ;
      private int AV236ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ;
      private int AV241ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ;
      private int AV243ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ;
      private int AV255ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ;
      private int AV256ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ;
      private int AV257ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ;
      private int AV258ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ;
      private int AV259ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ;
      private int AV261ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ;
      private int AV266ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ;
      private int AV268ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ;
      private int AV280ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ;
      private int AV281ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ;
      private int AV282ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ;
      private int AV283ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ;
      private int AV284ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ;
      private int AV286ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ;
      private int AV291ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ;
      private int AV293ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ;
      private int AV305ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ;
      private int AV306ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ;
      private int AV307ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ;
      private int AV308ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ;
      private int AV309ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ;
      private int AV311ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ;
      private int AV316ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ;
      private int AV318ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ;
      private int AV330ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ;
      private int AV331ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ;
      private int AV332ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ;
      private int AV333ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ;
      private int AV334ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ;
      private int AV336ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ;
      private int AV341ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ;
      private int AV343ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV362ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count ;
      private int AV54GridState_gxTpr_Dynamicfilters_Count ;
      private int AV142Contratadas_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A489ContagemResultado_SistemaCod ;
      private int A764ContagemResultado_ServicoGrupo ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A508ContagemResultado_Owner ;
      private int A1603ContagemResultado_CntCod ;
      private int A456ContagemResultado_Codigo ;
      private int A584ContagemResultado_ContadorFM ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private long AV123i ;
      private decimal AV177TFContagemResultado_PFFinal ;
      private decimal AV178TFContagemResultado_PFFinal_To ;
      private decimal AV179TFContagemResultado_ValorPF ;
      private decimal AV180TFContagemResultado_ValorPF_To ;
      private decimal AV29ContagemResultado_PFBFM1 ;
      private decimal AV30ContagemResultado_PFBFS1 ;
      private decimal AV40ContagemResultado_PFBFM2 ;
      private decimal AV41ContagemResultado_PFBFS2 ;
      private decimal AV51ContagemResultado_PFBFM3 ;
      private decimal AV52ContagemResultado_PFBFS3 ;
      private decimal AV90ContagemResultado_PFBFM4 ;
      private decimal AV91ContagemResultado_PFBFS4 ;
      private decimal AV105ContagemResultado_PFBFM5 ;
      private decimal AV106ContagemResultado_PFBFS5 ;
      private decimal AV237ExtraWWContagemResultadoExtraSelectionDS_20_Contagemresultado_pfbfm1 ;
      private decimal AV238ExtraWWContagemResultadoExtraSelectionDS_21_Contagemresultado_pfbfs1 ;
      private decimal AV262ExtraWWContagemResultadoExtraSelectionDS_45_Contagemresultado_pfbfm2 ;
      private decimal AV263ExtraWWContagemResultadoExtraSelectionDS_46_Contagemresultado_pfbfs2 ;
      private decimal AV287ExtraWWContagemResultadoExtraSelectionDS_70_Contagemresultado_pfbfm3 ;
      private decimal AV288ExtraWWContagemResultadoExtraSelectionDS_71_Contagemresultado_pfbfs3 ;
      private decimal AV312ExtraWWContagemResultadoExtraSelectionDS_95_Contagemresultado_pfbfm4 ;
      private decimal AV313ExtraWWContagemResultadoExtraSelectionDS_96_Contagemresultado_pfbfs4 ;
      private decimal AV337ExtraWWContagemResultadoExtraSelectionDS_120_Contagemresultado_pfbfm5 ;
      private decimal AV338ExtraWWContagemResultadoExtraSelectionDS_121_Contagemresultado_pfbfs5 ;
      private decimal AV366ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ;
      private decimal AV367ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ;
      private decimal AV368ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ;
      private decimal AV369ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal3 ;
      private String AV157TFContagemResultado_Agrupador ;
      private String AV158TFContagemResultado_Agrupador_Sel ;
      private String AV167TFContagemResultado_ContratadaSigla ;
      private String AV168TFContagemResultado_ContratadaSigla_Sel ;
      private String AV175TFContagemResultado_ServicoSigla ;
      private String AV176TFContagemResultado_ServicoSigla_Sel ;
      private String AV28ContagemResultado_StatusDmn1 ;
      private String AV59ContagemResultado_Baseline1 ;
      private String AV117ContagemResultado_Agrupador1 ;
      private String AV145ContagemResultado_TemPndHmlg1 ;
      private String AV39ContagemResultado_StatusDmn2 ;
      private String AV60ContagemResultado_Baseline2 ;
      private String AV118ContagemResultado_Agrupador2 ;
      private String AV147ContagemResultado_TemPndHmlg2 ;
      private String AV50ContagemResultado_StatusDmn3 ;
      private String AV61ContagemResultado_Baseline3 ;
      private String AV119ContagemResultado_Agrupador3 ;
      private String AV149ContagemResultado_TemPndHmlg3 ;
      private String AV82ContagemResultado_StatusDmn4 ;
      private String AV88ContagemResultado_Baseline4 ;
      private String AV120ContagemResultado_Agrupador4 ;
      private String AV151ContagemResultado_TemPndHmlg4 ;
      private String AV97ContagemResultado_StatusDmn5 ;
      private String AV103ContagemResultado_Baseline5 ;
      private String AV121ContagemResultado_Agrupador5 ;
      private String AV153ContagemResultado_TemPndHmlg5 ;
      private String AV173TFContagemResultado_StatusDmn_Sel ;
      private String AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ;
      private String AV235ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ;
      private String AV239ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ;
      private String AV242ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ;
      private String AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ;
      private String AV260ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ;
      private String AV264ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ;
      private String AV267ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ;
      private String AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ;
      private String AV285ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ;
      private String AV289ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ;
      private String AV292ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ;
      private String AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ;
      private String AV310ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ;
      private String AV314ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ;
      private String AV317ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ;
      private String AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ;
      private String AV335ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ;
      private String AV339ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ;
      private String AV342ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ;
      private String AV344ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ;
      private String AV345ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ;
      private String AV358ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ;
      private String AV359ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ;
      private String AV364ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ;
      private String AV365ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ;
      private String scmdbuf ;
      private String lV344ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ;
      private String lV358ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ;
      private String lV364ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1046ContagemResultado_Agrupador ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A1612ContagemResultado_CntNum ;
      private DateTime AV203TFContagemResultado_DataPrevista ;
      private DateTime AV204TFContagemResultado_DataPrevista_To ;
      private DateTime AV352ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ;
      private DateTime AV353ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime GXt_dtime1 ;
      private DateTime AV201TFContagemResultado_DataDmn ;
      private DateTime AV202TFContagemResultado_DataDmn_To ;
      private DateTime AV165TFContagemResultado_DataUltCnt ;
      private DateTime AV166TFContagemResultado_DataUltCnt_To ;
      private DateTime AV22ContagemResultado_DataCnt1 ;
      private DateTime AV23ContagemResultado_DataCnt_To1 ;
      private DateTime AV181ContagemResultado_DataDmn1 ;
      private DateTime AV182ContagemResultado_DataDmn_To1 ;
      private DateTime AV183ContagemResultado_DataPrevista1 ;
      private DateTime AV184ContagemResultado_DataPrevista_To1 ;
      private DateTime AV33ContagemResultado_DataCnt2 ;
      private DateTime AV34ContagemResultado_DataCnt_To2 ;
      private DateTime AV185ContagemResultado_DataDmn2 ;
      private DateTime AV186ContagemResultado_DataDmn_To2 ;
      private DateTime AV187ContagemResultado_DataPrevista2 ;
      private DateTime AV188ContagemResultado_DataPrevista_To2 ;
      private DateTime AV44ContagemResultado_DataCnt3 ;
      private DateTime AV45ContagemResultado_DataCnt_To3 ;
      private DateTime AV189ContagemResultado_DataDmn3 ;
      private DateTime AV190ContagemResultado_DataDmn_To3 ;
      private DateTime AV191ContagemResultado_DataPrevista3 ;
      private DateTime AV192ContagemResultado_DataPrevista_To3 ;
      private DateTime AV79ContagemResultado_DataCnt4 ;
      private DateTime AV80ContagemResultado_DataCnt_To4 ;
      private DateTime AV193ContagemResultado_DataDmn4 ;
      private DateTime AV194ContagemResultado_DataDmn_To4 ;
      private DateTime AV195ContagemResultado_DataPrevista4 ;
      private DateTime AV196ContagemResultado_DataPrevista_To4 ;
      private DateTime AV94ContagemResultado_DataCnt5 ;
      private DateTime AV95ContagemResultado_DataCnt_To5 ;
      private DateTime AV197ContagemResultado_DataDmn5 ;
      private DateTime AV198ContagemResultado_DataDmn_To5 ;
      private DateTime AV199ContagemResultado_DataPrevista5 ;
      private DateTime AV200ContagemResultado_DataPrevista_To5 ;
      private DateTime AV222ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ;
      private DateTime AV223ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ;
      private DateTime AV226ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ;
      private DateTime AV227ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ;
      private DateTime AV228ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ;
      private DateTime AV229ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ;
      private DateTime AV247ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ;
      private DateTime AV248ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ;
      private DateTime AV251ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ;
      private DateTime AV252ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ;
      private DateTime AV253ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ;
      private DateTime AV254ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ;
      private DateTime AV272ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ;
      private DateTime AV273ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ;
      private DateTime AV276ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ;
      private DateTime AV277ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ;
      private DateTime AV278ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ;
      private DateTime AV279ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ;
      private DateTime AV297ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ;
      private DateTime AV298ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ;
      private DateTime AV301ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ;
      private DateTime AV302ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ;
      private DateTime AV303ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ;
      private DateTime AV304ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ;
      private DateTime AV322ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ;
      private DateTime AV323ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ;
      private DateTime AV326ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ;
      private DateTime AV327ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ;
      private DateTime AV328ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ;
      private DateTime AV329ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ;
      private DateTime AV354ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ;
      private DateTime AV355ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ;
      private DateTime AV356ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ;
      private DateTime AV357ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private bool AV17OrderedDsc ;
      private bool returnInSub ;
      private bool AV31DynamicFiltersEnabled2 ;
      private bool AV42DynamicFiltersEnabled3 ;
      private bool AV77DynamicFiltersEnabled4 ;
      private bool AV92DynamicFiltersEnabled5 ;
      private bool AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ;
      private bool AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ;
      private bool AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ;
      private bool AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ;
      private bool AV9WWPContext_gxTpr_Userehfinanceiro ;
      private bool AV9WWPContext_gxTpr_Userehgestor ;
      private bool A598ContagemResultado_Baseline ;
      private bool A1802ContagemResultado_TemPndHmlg ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n494ContagemResultado_Descricao ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n598ContagemResultado_Baseline ;
      private bool n764ContagemResultado_ServicoGrupo ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n495ContagemResultado_SistemaNom ;
      private bool n1612ContagemResultado_CntNum ;
      private bool n2118ContagemResultado_Owner_Identificao ;
      private bool GXt_boolean2 ;
      private String AV171TFContagemResultado_StatusDmn_SelsJson ;
      private String AV53GridStateXML ;
      private String AV161TFContagemResultado_DemandaFM ;
      private String AV162TFContagemResultado_DemandaFM_Sel ;
      private String AV159TFContagemResultado_Demanda ;
      private String AV160TFContagemResultado_Demanda_Sel ;
      private String AV163TFContagemResultado_Descricao ;
      private String AV164TFContagemResultado_Descricao_Sel ;
      private String AV169TFContagemResultado_SistemaCoord ;
      private String AV170TFContagemResultado_SistemaCoord_Sel ;
      private String AV12ErrorMessage ;
      private String AV11Filename ;
      private String AV21DynamicFiltersSelector1 ;
      private String AV24ContagemResultado_OsFsOsFm1 ;
      private String AV132ContagemResultado_Descricao1 ;
      private String AV32DynamicFiltersSelector2 ;
      private String AV35ContagemResultado_OsFsOsFm2 ;
      private String AV133ContagemResultado_Descricao2 ;
      private String AV43DynamicFiltersSelector3 ;
      private String AV46ContagemResultado_OsFsOsFm3 ;
      private String AV134ContagemResultado_Descricao3 ;
      private String AV78DynamicFiltersSelector4 ;
      private String AV81ContagemResultado_OsFsOsFm4 ;
      private String AV135ContagemResultado_Descricao4 ;
      private String AV93DynamicFiltersSelector5 ;
      private String AV96ContagemResultado_OsFsOsFm5 ;
      private String AV136ContagemResultado_Descricao5 ;
      private String AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ;
      private String AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ;
      private String AV240ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ;
      private String AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ;
      private String AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ;
      private String AV265ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ;
      private String AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ;
      private String AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ;
      private String AV290ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ;
      private String AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ;
      private String AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ;
      private String AV315ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ;
      private String AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ;
      private String AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ;
      private String AV340ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ;
      private String AV346ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ;
      private String AV347ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ;
      private String AV348ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ;
      private String AV349ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ;
      private String AV350ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ;
      private String AV351ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ;
      private String AV360ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ;
      private String AV361ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ;
      private String lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ;
      private String lV240ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ;
      private String lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ;
      private String lV265ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ;
      private String lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ;
      private String lV290ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ;
      private String lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ;
      private String lV315ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ;
      private String lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ;
      private String lV340ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ;
      private String lV346ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ;
      private String lV348ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ;
      private String lV350ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ;
      private String lV360ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A515ContagemResultado_SistemaCoord ;
      private String A495ContagemResultado_SistemaNom ;
      private String A2118ContagemResultado_Owner_Identificao ;
      private IGxSession AV143WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P003U4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P003U4_n1553ContagemResultado_CntSrvCod ;
      private short[] P003U4_A1583ContagemResultado_TipoRegistro ;
      private decimal[] P003U4_A512ContagemResultado_ValorPF ;
      private bool[] P003U4_n512ContagemResultado_ValorPF ;
      private String[] P003U4_A801ContagemResultado_ServicoSigla ;
      private bool[] P003U4_n801ContagemResultado_ServicoSigla ;
      private String[] P003U4_A515ContagemResultado_SistemaCoord ;
      private bool[] P003U4_n515ContagemResultado_SistemaCoord ;
      private String[] P003U4_A803ContagemResultado_ContratadaSigla ;
      private bool[] P003U4_n803ContagemResultado_ContratadaSigla ;
      private int[] P003U4_A1603ContagemResultado_CntCod ;
      private bool[] P003U4_n1603ContagemResultado_CntCod ;
      private String[] P003U4_A494ContagemResultado_Descricao ;
      private bool[] P003U4_n494ContagemResultado_Descricao ;
      private String[] P003U4_A1046ContagemResultado_Agrupador ;
      private bool[] P003U4_n1046ContagemResultado_Agrupador ;
      private int[] P003U4_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P003U4_n468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P003U4_A598ContagemResultado_Baseline ;
      private bool[] P003U4_n598ContagemResultado_Baseline ;
      private int[] P003U4_A764ContagemResultado_ServicoGrupo ;
      private bool[] P003U4_n764ContagemResultado_ServicoGrupo ;
      private int[] P003U4_A489ContagemResultado_SistemaCod ;
      private bool[] P003U4_n489ContagemResultado_SistemaCod ;
      private int[] P003U4_A508ContagemResultado_Owner ;
      private int[] P003U4_A601ContagemResultado_Servico ;
      private bool[] P003U4_n601ContagemResultado_Servico ;
      private DateTime[] P003U4_A1351ContagemResultado_DataPrevista ;
      private bool[] P003U4_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P003U4_A471ContagemResultado_DataDmn ;
      private String[] P003U4_A493ContagemResultado_DemandaFM ;
      private bool[] P003U4_n493ContagemResultado_DemandaFM ;
      private String[] P003U4_A457ContagemResultado_Demanda ;
      private bool[] P003U4_n457ContagemResultado_Demanda ;
      private String[] P003U4_A484ContagemResultado_StatusDmn ;
      private bool[] P003U4_n484ContagemResultado_StatusDmn ;
      private decimal[] P003U4_A1854ContagemResultado_VlrCnc ;
      private bool[] P003U4_n1854ContagemResultado_VlrCnc ;
      private int[] P003U4_A490ContagemResultado_ContratadaCod ;
      private bool[] P003U4_n490ContagemResultado_ContratadaCod ;
      private int[] P003U4_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003U4_n52Contratada_AreaTrabalhoCod ;
      private String[] P003U4_A495ContagemResultado_SistemaNom ;
      private bool[] P003U4_n495ContagemResultado_SistemaNom ;
      private String[] P003U4_A1612ContagemResultado_CntNum ;
      private bool[] P003U4_n1612ContagemResultado_CntNum ;
      private decimal[] P003U4_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P003U4_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P003U4_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P003U4_A684ContagemResultado_PFBFSUltima ;
      private int[] P003U4_A584ContagemResultado_ContadorFM ;
      private DateTime[] P003U4_A566ContagemResultado_DataUltCnt ;
      private short[] P003U4_A531ContagemResultado_StatusUltCnt ;
      private String[] P003U4_A2118ContagemResultado_Owner_Identificao ;
      private bool[] P003U4_n2118ContagemResultado_Owner_Identificao ;
      private int[] P003U4_A456ContagemResultado_Codigo ;
      private String aP31_Filename ;
      private String aP32_ErrorMessage ;
      private ExcelDocumentI AV10ExcelDocument ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV142Contratadas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV126ServicosGeridos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV172TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV362ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV54GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV55GridStateDynamicFilter ;
   }

   public class exportextrawwcontagemresultadoextraselection__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P003U4( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV362ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV142Contratadas ,
                                             int A601ContagemResultado_Servico ,
                                             IGxCollection AV126ServicosGeridos ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ,
                                             String AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ,
                                             short AV221ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ,
                                             String AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ,
                                             DateTime AV226ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ,
                                             DateTime AV227ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ,
                                             DateTime AV228ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ,
                                             DateTime AV229ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ,
                                             int AV230ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ,
                                             int AV232ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ,
                                             int AV233ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ,
                                             int AV218ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ,
                                             int AV234ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ,
                                             String AV235ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ,
                                             int AV236ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ,
                                             String AV239ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ,
                                             String AV240ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ,
                                             int AV241ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ,
                                             int AV243ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ,
                                             bool AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ,
                                             String AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ,
                                             String AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ,
                                             short AV246ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ,
                                             String AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ,
                                             DateTime AV251ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ,
                                             DateTime AV252ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ,
                                             DateTime AV253ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ,
                                             DateTime AV254ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ,
                                             int AV255ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ,
                                             int AV257ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ,
                                             int AV258ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ,
                                             int AV259ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ,
                                             String AV260ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ,
                                             int AV261ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ,
                                             String AV264ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ,
                                             String AV265ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ,
                                             int AV266ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ,
                                             int AV268ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ,
                                             bool AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ,
                                             String AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ,
                                             String AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ,
                                             short AV271ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ,
                                             String AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ,
                                             DateTime AV276ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ,
                                             DateTime AV277ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ,
                                             DateTime AV278ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ,
                                             DateTime AV279ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ,
                                             int AV280ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ,
                                             int AV282ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ,
                                             int AV283ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ,
                                             int AV284ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ,
                                             String AV285ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ,
                                             int AV286ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ,
                                             String AV289ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ,
                                             String AV290ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ,
                                             int AV291ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ,
                                             int AV293ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ,
                                             bool AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ,
                                             String AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ,
                                             String AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ,
                                             short AV296ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ,
                                             String AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ,
                                             DateTime AV301ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ,
                                             DateTime AV302ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ,
                                             DateTime AV303ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ,
                                             DateTime AV304ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ,
                                             int AV305ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ,
                                             int AV307ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ,
                                             int AV308ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ,
                                             int AV309ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ,
                                             String AV310ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ,
                                             int AV311ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ,
                                             String AV314ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ,
                                             String AV315ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ,
                                             int AV316ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ,
                                             int AV318ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ,
                                             bool AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ,
                                             String AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ,
                                             String AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ,
                                             short AV321ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ,
                                             String AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ,
                                             DateTime AV326ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ,
                                             DateTime AV327ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ,
                                             DateTime AV328ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ,
                                             DateTime AV329ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ,
                                             int AV330ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ,
                                             int AV332ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ,
                                             int AV333ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ,
                                             int AV334ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ,
                                             String AV335ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ,
                                             int AV336ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ,
                                             String AV339ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ,
                                             String AV340ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ,
                                             int AV341ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ,
                                             int AV343ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ,
                                             String AV345ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ,
                                             String AV344ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ,
                                             String AV347ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ,
                                             String AV346ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ,
                                             String AV349ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ,
                                             String AV348ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ,
                                             String AV351ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ,
                                             String AV350ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ,
                                             DateTime AV352ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ,
                                             DateTime AV353ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ,
                                             DateTime AV354ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ,
                                             DateTime AV355ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ,
                                             String AV359ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV358ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ,
                                             String AV361ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ,
                                             String AV360ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ,
                                             int AV362ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count ,
                                             short AV363ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ,
                                             String AV365ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ,
                                             String AV364ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ,
                                             decimal AV368ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ,
                                             decimal AV369ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ,
                                             int AV54GridState_gxTpr_Dynamicfilters_Count ,
                                             int AV142Contratadas_Count ,
                                             bool AV9WWPContext_gxTpr_Userehfinanceiro ,
                                             bool AV9WWPContext_gxTpr_Userehgestor ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A1351ContagemResultado_DataPrevista ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A764ContagemResultado_ServicoGrupo ,
                                             bool A598ContagemResultado_Baseline ,
                                             int A468ContagemResultado_NaoCnfDmnCod ,
                                             String A1046ContagemResultado_Agrupador ,
                                             String A494ContagemResultado_Descricao ,
                                             int A508ContagemResultado_Owner ,
                                             int A1603ContagemResultado_CntCod ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A515ContagemResultado_SistemaCoord ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             decimal A512ContagemResultado_ValorPF ,
                                             int A456ContagemResultado_Codigo ,
                                             short AV16OrderedBy ,
                                             bool AV17OrderedDsc ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             decimal A1854ContagemResultado_VlrCnc ,
                                             DateTime AV222ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV223ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ,
                                             int AV231ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ,
                                             int A584ContagemResultado_ContadorFM ,
                                             decimal A684ContagemResultado_PFBFSUltima ,
                                             decimal A682ContagemResultado_PFBFMUltima ,
                                             decimal A685ContagemResultado_PFLFSUltima ,
                                             decimal A683ContagemResultado_PFLFMUltima ,
                                             String AV242ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ,
                                             bool A1802ContagemResultado_TemPndHmlg ,
                                             DateTime AV247ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ,
                                             DateTime AV248ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ,
                                             int AV256ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ,
                                             String AV267ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ,
                                             DateTime AV272ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ,
                                             DateTime AV273ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ,
                                             int AV281ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ,
                                             String AV292ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ,
                                             DateTime AV297ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ,
                                             DateTime AV298ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ,
                                             int AV306ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ,
                                             String AV317ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ,
                                             DateTime AV322ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ,
                                             DateTime AV323ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ,
                                             int AV331ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ,
                                             String AV342ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ,
                                             DateTime AV356ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV357ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ,
                                             decimal AV366ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ,
                                             decimal A574ContagemResultado_PFFinal ,
                                             decimal AV367ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [211] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_TipoRegistro], T1.[ContagemResultado_ValorPF], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T5.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T7.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Descricao], T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_Baseline], T4.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Owner], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T7.[Contratada_AreaTrabalhoCod], T5.[Sistema_Nome] AS ContagemResultado_SistemaNom, T3.[Contrato_Numero] AS ContagemResultado_CntNum, COALESCE( T8.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T8.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T8.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T8.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T8.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T8.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T6.[ContagemResultado_Owner_Identificao], '') AS ContagemResultado_Owner_Identificao,";
         scmdbuf = scmdbuf + " T1.[ContagemResultado_Codigo] FROM ((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN (SELECT T10.[Pessoa_Nome] AS ContagemResultado_Owner_Identificao, T9.[Usuario_Codigo] FROM ([Usuario] T9 WITH (NOLOCK) INNER JOIN [Pessoa] T10 WITH (NOLOCK) ON T10.[Pessoa_Codigo] = T9.[Usuario_PessoaCod]) ) T6 ON T6.[Usuario_Codigo] = T1.[ContagemResultado_Owner]) LEFT JOIN [Contratada] T7 WITH (NOLOCK) ON T7.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( Not ( @AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = 'N' or @AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = 'N' or @AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = 'N' or @AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = 'N' or @AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = 'N')) or ( COALESCE( T8.[ContagemResultado_StatusUltCnt], 0) = 5 or T1.[ContagemResultado_VlrCnc] > 0 or COALESCE( T8.[ContagemResultado_StatusUltCnt], 0) = 8))";
         scmdbuf = scmdbuf + " and (Not ( @AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV222ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV222ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV223ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV223ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV231ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T8.[ContagemResultado_ContadorFM], 0) = @AV231ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 or ( (COALESCE( T8.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV231ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (@AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 <> 'CONTAGEMRESULTADO_PFBFM' or ( COALESCE( T8.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T8.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T8.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (@AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 <> 'CONTAGEMRESULTADO_PFBFS' or ( COALESCE( T8.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T8.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T8.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV247ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV247ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV248ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV248ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV256ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T8.[ContagemResultado_ContadorFM], 0) = @AV256ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 or ( (COALESCE( T8.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV256ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T8.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T8.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T8.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T8.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T8.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T8.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV272ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV272ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV273ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV273ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV281ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T8.[ContagemResultado_ContadorFM], 0) = @AV281ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 or ( (COALESCE( T8.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV281ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and (Not ( @AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T8.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T8.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T8.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T8.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T8.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T8.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV297ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV297ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV298ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV298ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4))";
         scmdbuf = scmdbuf + " and (Not ( @AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV306ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 = convert(int, 0)))) or ( COALESCE( T8.[ContagemResultado_ContadorFM], 0) = @AV306ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 or ( (COALESCE( T8.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV306ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4)))";
         scmdbuf = scmdbuf + " and (Not ( @AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T8.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T8.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T8.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T8.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T8.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T8.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV322ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV322ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV323ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV323ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5))";
         scmdbuf = scmdbuf + " and (Not ( @AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV331ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 = convert(int, 0)))) or ( COALESCE( T8.[ContagemResultado_ContadorFM], 0) = @AV331ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 or ( (COALESCE( T8.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV331ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5)))";
         scmdbuf = scmdbuf + " and (Not ( @AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T8.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T8.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T8.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T8.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T8.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T8.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and ((@AV356ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV356ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV357ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV357ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (T7.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int4[90] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1)) && ! ( StringUtil.StrCmp(AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int4[91] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV221ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int4[92] = 1;
            GXv_int4[93] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV221ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int4[94] = 1;
            GXv_int4[95] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV221ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int4[96] = 1;
            GXv_int4[97] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV226ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV226ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int4[98] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV227ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV227ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int4[99] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV228ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV228ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1)";
         }
         else
         {
            GXv_int4[100] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV229ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV229ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1))";
         }
         else
         {
            GXv_int4[101] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV230ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV230ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int4[102] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV232ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV232ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int4[103] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV233ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 > 0 ) && ( AV218ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV233ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int4[104] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV234ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV234ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1)";
         }
         else
         {
            GXv_int4[105] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV235ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV235ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV236ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV236ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int4[106] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV239ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV239ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int4[107] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV240ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV240ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int4[108] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV241ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV241ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1)";
         }
         else
         {
            GXv_int4[109] = 1;
         }
         if ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV243ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV243ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1)";
         }
         else
         {
            GXv_int4[110] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2)) && ! ( StringUtil.StrCmp(AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int4[111] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV246ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int4[112] = 1;
            GXv_int4[113] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV246ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int4[114] = 1;
            GXv_int4[115] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV246ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int4[116] = 1;
            GXv_int4[117] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV251ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV251ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int4[118] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV252ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV252ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int4[119] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV253ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV253ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2)";
         }
         else
         {
            GXv_int4[120] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV254ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV254ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2))";
         }
         else
         {
            GXv_int4[121] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV255ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV255ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int4[122] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV257ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV257ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int4[123] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV258ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 > 0 ) && ( AV218ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV258ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int4[124] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV259ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV259ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2)";
         }
         else
         {
            GXv_int4[125] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV260ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV260ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV261ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV261ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int4[126] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV264ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV264ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int4[127] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV265ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV265ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int4[128] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV266ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV266ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2)";
         }
         else
         {
            GXv_int4[129] = 1;
         }
         if ( AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV268ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV268ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2)";
         }
         else
         {
            GXv_int4[130] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3)) && ! ( StringUtil.StrCmp(AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int4[131] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV271ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int4[132] = 1;
            GXv_int4[133] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV271ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int4[134] = 1;
            GXv_int4[135] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV271ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int4[136] = 1;
            GXv_int4[137] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV276ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV276ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int4[138] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV277ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV277ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int4[139] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV278ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV278ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3)";
         }
         else
         {
            GXv_int4[140] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV279ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV279ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3))";
         }
         else
         {
            GXv_int4[141] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV280ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV280ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int4[142] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV282ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV282ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int4[143] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV283ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 > 0 ) && ( AV218ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV283ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int4[144] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV284ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV284ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3)";
         }
         else
         {
            GXv_int4[145] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV285ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV285ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV286ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV286ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int4[146] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV289ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV289ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int4[147] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV290ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV290ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int4[148] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV291ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV291ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3)";
         }
         else
         {
            GXv_int4[149] = 1;
         }
         if ( AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV293ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV293ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3)";
         }
         else
         {
            GXv_int4[150] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4)) && ! ( StringUtil.StrCmp(AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4)";
         }
         else
         {
            GXv_int4[151] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV296ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] = @AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int4[152] = 1;
            GXv_int4[153] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV296ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like @lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int4[154] = 1;
            GXv_int4[155] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV296ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like '%' + @lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int4[156] = 1;
            GXv_int4[157] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV301ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV301ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4)";
         }
         else
         {
            GXv_int4[158] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV302ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV302ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4)";
         }
         else
         {
            GXv_int4[159] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV303ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV303ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4)";
         }
         else
         {
            GXv_int4[160] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV304ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV304ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4))";
         }
         else
         {
            GXv_int4[161] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV305ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV305ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4)";
         }
         else
         {
            GXv_int4[162] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV307ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV307ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4)";
         }
         else
         {
            GXv_int4[163] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV308ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 > 0 ) && ( AV218ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV308ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4)";
         }
         else
         {
            GXv_int4[164] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV309ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV309ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4)";
         }
         else
         {
            GXv_int4[165] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV310ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV310ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV311ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV311ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4)";
         }
         else
         {
            GXv_int4[166] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV314ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV314ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4)";
         }
         else
         {
            GXv_int4[167] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV315ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV315ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 + '%')";
         }
         else
         {
            GXv_int4[168] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV316ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV316ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4)";
         }
         else
         {
            GXv_int4[169] = 1;
         }
         if ( AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV318ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV318ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4)";
         }
         else
         {
            GXv_int4[170] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5)) && ! ( StringUtil.StrCmp(AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5)";
         }
         else
         {
            GXv_int4[171] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV321ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] = @AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int4[172] = 1;
            GXv_int4[173] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV321ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like @lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int4[174] = 1;
            GXv_int4[175] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV321ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like '%' + @lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int4[176] = 1;
            GXv_int4[177] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV326ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV326ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5)";
         }
         else
         {
            GXv_int4[178] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV327ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV327ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5)";
         }
         else
         {
            GXv_int4[179] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV328ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV328ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5)";
         }
         else
         {
            GXv_int4[180] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV329ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV329ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5))";
         }
         else
         {
            GXv_int4[181] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV330ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV330ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5)";
         }
         else
         {
            GXv_int4[182] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV332ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV332ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5)";
         }
         else
         {
            GXv_int4[183] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV333ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 > 0 ) && ( AV218ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV333ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5)";
         }
         else
         {
            GXv_int4[184] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV334ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV334ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5)";
         }
         else
         {
            GXv_int4[185] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV335ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV335ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV336ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV336ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5)";
         }
         else
         {
            GXv_int4[186] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV339ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV339ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5)";
         }
         else
         {
            GXv_int4[187] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV340ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV340ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 + '%')";
         }
         else
         {
            GXv_int4[188] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV341ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV341ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5)";
         }
         else
         {
            GXv_int4[189] = 1;
         }
         if ( AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV343ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV343ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5)";
         }
         else
         {
            GXv_int4[190] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV345ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV344ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV344ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador)";
         }
         else
         {
            GXv_int4[191] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV345ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV345ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)";
         }
         else
         {
            GXv_int4[192] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV347ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV346ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV346ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int4[193] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV347ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV347ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int4[194] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV349ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV348ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV348ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int4[195] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV349ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV349ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int4[196] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV351ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV350ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV350ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int4[197] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV351ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV351ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int4[198] = 1;
         }
         if ( ! (DateTime.MinValue==AV352ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV352ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista)";
         }
         else
         {
            GXv_int4[199] = 1;
         }
         if ( ! (DateTime.MinValue==AV353ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV353ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to)";
         }
         else
         {
            GXv_int4[200] = 1;
         }
         if ( ! (DateTime.MinValue==AV354ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV354ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int4[201] = 1;
         }
         if ( ! (DateTime.MinValue==AV355ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV355ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int4[202] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV358ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T7.[Contratada_Sigla] like @lV358ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int4[203] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T7.[Contratada_Sigla] = @AV359ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int4[204] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV361ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV360ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Coordenacao] like @lV360ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord)";
         }
         else
         {
            GXv_int4[205] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV361ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Coordenacao] = @AV361ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)";
         }
         else
         {
            GXv_int4[206] = 1;
         }
         if ( AV362ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV362ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV363ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV363ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 0)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV365ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV364ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] like @lV364ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int4[207] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV365ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] = @AV365ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int4[208] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV368ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ValorPF] >= @AV368ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf)";
         }
         else
         {
            GXv_int4[209] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV369ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ValorPF] <= @AV369ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to)";
         }
         else
         {
            GXv_int4[210] = 1;
         }
         if ( AV54GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = 0)";
         }
         if ( AV142Contratadas_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142Contratadas, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         if ( ( AV9WWPContext_gxTpr_Contratada_codigo > 0 ) && ! AV9WWPContext_gxTpr_Userehfinanceiro && AV9WWPContext_gxTpr_Userehgestor )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV126ServicosGeridos, "T2.[Servico_Codigo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV16OrderedBy == 1 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataDmn]";
         }
         else if ( ( AV16OrderedBy == 1 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataDmn] DESC";
         }
         else if ( ( AV16OrderedBy == 2 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DemandaFM]";
         }
         else if ( ( AV16OrderedBy == 2 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DemandaFM] DESC";
         }
         else if ( ( AV16OrderedBy == 3 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         }
         else if ( ( AV16OrderedBy == 3 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda] DESC";
         }
         else if ( ( AV16OrderedBy == 4 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Descricao]";
         }
         else if ( ( AV16OrderedBy == 4 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Descricao] DESC";
         }
         else if ( ( AV16OrderedBy == 5 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataPrevista]";
         }
         else if ( ( AV16OrderedBy == 5 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataPrevista] DESC";
         }
         else if ( ( AV16OrderedBy == 6 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_StatusDmn]";
         }
         else if ( ( AV16OrderedBy == 6 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_StatusDmn] DESC";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P003U4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (bool)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] , (String)dynConstraints[39] , (int)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (bool)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (short)dynConstraints[48] , (String)dynConstraints[49] , (DateTime)dynConstraints[50] , (DateTime)dynConstraints[51] , (DateTime)dynConstraints[52] , (DateTime)dynConstraints[53] , (int)dynConstraints[54] , (int)dynConstraints[55] , (int)dynConstraints[56] , (int)dynConstraints[57] , (String)dynConstraints[58] , (int)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (int)dynConstraints[62] , (int)dynConstraints[63] , (bool)dynConstraints[64] , (String)dynConstraints[65] , (String)dynConstraints[66] , (short)dynConstraints[67] , (String)dynConstraints[68] , (DateTime)dynConstraints[69] , (DateTime)dynConstraints[70] , (DateTime)dynConstraints[71] , (DateTime)dynConstraints[72] , (int)dynConstraints[73] , (int)dynConstraints[74] , (int)dynConstraints[75] , (int)dynConstraints[76] , (String)dynConstraints[77] , (int)dynConstraints[78] , (String)dynConstraints[79] , (String)dynConstraints[80] , (int)dynConstraints[81] , (int)dynConstraints[82] , (bool)dynConstraints[83] , (String)dynConstraints[84] , (String)dynConstraints[85] , (short)dynConstraints[86] , (String)dynConstraints[87] , (DateTime)dynConstraints[88] , (DateTime)dynConstraints[89] , (DateTime)dynConstraints[90] , (DateTime)dynConstraints[91] , (int)dynConstraints[92] , (int)dynConstraints[93] , (int)dynConstraints[94] , (int)dynConstraints[95] , (String)dynConstraints[96] , (int)dynConstraints[97] , (String)dynConstraints[98] , (String)dynConstraints[99] , (int)dynConstraints[100] , (int)dynConstraints[101] , (String)dynConstraints[102] , (String)dynConstraints[103] , (String)dynConstraints[104] , (String)dynConstraints[105] , (String)dynConstraints[106] , (String)dynConstraints[107] , (String)dynConstraints[108] , (String)dynConstraints[109] , (DateTime)dynConstraints[110] , (DateTime)dynConstraints[111] , (DateTime)dynConstraints[112] , (DateTime)dynConstraints[113] , (String)dynConstraints[114] , (String)dynConstraints[115] , (String)dynConstraints[116] , (String)dynConstraints[117] , (int)dynConstraints[118] , (short)dynConstraints[119] , (String)dynConstraints[120] , (String)dynConstraints[121] , (decimal)dynConstraints[122] , (decimal)dynConstraints[123] , (int)dynConstraints[124] , (int)dynConstraints[125] , (bool)dynConstraints[126] , (bool)dynConstraints[127] , (String)dynConstraints[128] , (String)dynConstraints[129] , (DateTime)dynConstraints[130] , (DateTime)dynConstraints[131] , (int)dynConstraints[132] , (int)dynConstraints[133] , (bool)dynConstraints[134] , (int)dynConstraints[135] , (String)dynConstraints[136] , (String)dynConstraints[137] , (int)dynConstraints[138] , (int)dynConstraints[139] , (String)dynConstraints[140] , (String)dynConstraints[141] , (String)dynConstraints[142] , (decimal)dynConstraints[143] , (int)dynConstraints[144] , (short)dynConstraints[145] , (bool)dynConstraints[146] , (short)dynConstraints[147] , (decimal)dynConstraints[148] , (DateTime)dynConstraints[149] , (DateTime)dynConstraints[150] , (DateTime)dynConstraints[151] , (int)dynConstraints[152] , (int)dynConstraints[153] , (decimal)dynConstraints[154] , (decimal)dynConstraints[155] , (decimal)dynConstraints[156] , (decimal)dynConstraints[157] , (String)dynConstraints[158] , (bool)dynConstraints[159] , (DateTime)dynConstraints[160] , (DateTime)dynConstraints[161] , (int)dynConstraints[162] , (String)dynConstraints[163] , (DateTime)dynConstraints[164] , (DateTime)dynConstraints[165] , (int)dynConstraints[166] , (String)dynConstraints[167] , (DateTime)dynConstraints[168] , (DateTime)dynConstraints[169] , (int)dynConstraints[170] , (String)dynConstraints[171] , (DateTime)dynConstraints[172] , (DateTime)dynConstraints[173] , (int)dynConstraints[174] , (String)dynConstraints[175] , (DateTime)dynConstraints[176] , (DateTime)dynConstraints[177] , (decimal)dynConstraints[178] , (decimal)dynConstraints[179] , (decimal)dynConstraints[180] , (int)dynConstraints[181] , (int)dynConstraints[182] , (short)dynConstraints[183] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP003U4 ;
          prmP003U4 = new Object[] {
          new Object[] {"@AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV222ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV222ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV223ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV223ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV231ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV231ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV231ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV220ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV247ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV247ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV248ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV248ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV256ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV256ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV256ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV244ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV272ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV272ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV273ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV273ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV281ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV281ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV281ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV269ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV270ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV297ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV297ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV298ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV298ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV306ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV306ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV306ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV294ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV295ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV322ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV322ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV323ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV323ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV331ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV331ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV331ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV319ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV320ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV356ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV356ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV357ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV357ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV224ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV225ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV226ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV227ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV228ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV229ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV230ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV232ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV233ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV234ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV236ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV239ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV240ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV241ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV243ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV249ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV250ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV251ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV252ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV253ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV254ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV255ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV257ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV258ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV259ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV261ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV264ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV265ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV268ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV274ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV275ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV276ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV277ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV278ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV279ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV280ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV282ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV283ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV284ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV286ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV289ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV290ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV291ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV293ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV299ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV300ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV301ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV302ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV303ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV304ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV305ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV307ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV308ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV309ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV311ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV314ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4",SqlDbType.Char,15,0} ,
          new Object[] {"@lV315ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV316ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV318ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV324ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV325ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV326ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV327ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV328ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV330ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV332ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV333ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV334ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV336ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV339ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5",SqlDbType.Char,15,0} ,
          new Object[] {"@lV340ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV341ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV343ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV344ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV345ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV346ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV347ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV348ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV349ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV350ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV351ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV352ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV355ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV358ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV359ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV360ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV361ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV364ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV365ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV368ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV369ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P003U4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003U4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[28])[0] = rslt.getGXDateTime(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(17) ;
                ((String[]) buf[31])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                ((String[]) buf[33])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((String[]) buf[35])[0] = rslt.getString(20, 1) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((decimal[]) buf[37])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                ((int[]) buf[39])[0] = rslt.getInt(22) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(22);
                ((int[]) buf[41])[0] = rslt.getInt(23) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(23);
                ((String[]) buf[43])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(24);
                ((String[]) buf[45])[0] = rslt.getString(25, 20) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(25);
                ((decimal[]) buf[47])[0] = rslt.getDecimal(26) ;
                ((decimal[]) buf[48])[0] = rslt.getDecimal(27) ;
                ((decimal[]) buf[49])[0] = rslt.getDecimal(28) ;
                ((decimal[]) buf[50])[0] = rslt.getDecimal(29) ;
                ((int[]) buf[51])[0] = rslt.getInt(30) ;
                ((DateTime[]) buf[52])[0] = rslt.getGXDate(31) ;
                ((short[]) buf[53])[0] = rslt.getShort(32) ;
                ((String[]) buf[54])[0] = rslt.getVarchar(33) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(33);
                ((int[]) buf[56])[0] = rslt.getInt(34) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[211]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[212]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[213]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[214]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[217]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[218]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[219]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[220]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[221]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[223]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[224]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[225]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[227]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[228]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[229]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[230]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[231]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[232]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[234]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[235]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[236]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[237]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[238]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[239]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[240]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[241]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[243]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[245]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[247]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[248]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[249]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[250]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[251]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[252]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[253]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[254]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[255]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[256]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[257]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[258]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[259]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[260]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[261]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[262]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[263]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[264]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[265]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[266]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[267]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[268]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[269]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[270]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[271]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[272]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[273]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[274]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[275]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[276]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[277]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[278]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[279]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[280]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[281]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[282]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[283]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[284]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[285]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[286]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[287]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[288]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[289]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[290]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[291]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[292]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[293]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[294]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[295]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[296]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[297]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[298]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[299]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[300]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[301]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[302]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[303]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[304]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[305]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[306]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[307]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[308]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[309]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[310]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[311]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[312]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[313]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[314]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[315]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[316]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[317]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[318]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[319]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[320]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[321]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[322]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[323]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[324]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[325]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[326]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[327]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[328]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[329]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[330]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[331]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[332]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[333]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[334]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[335]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[336]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[337]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[338]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[339]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[340]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[341]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[342]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[343]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[344]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[345]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[346]);
                }
                if ( (short)parms[136] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[347]);
                }
                if ( (short)parms[137] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[348]);
                }
                if ( (short)parms[138] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[349]);
                }
                if ( (short)parms[139] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[350]);
                }
                if ( (short)parms[140] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[351]);
                }
                if ( (short)parms[141] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[352]);
                }
                if ( (short)parms[142] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[353]);
                }
                if ( (short)parms[143] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[354]);
                }
                if ( (short)parms[144] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[355]);
                }
                if ( (short)parms[145] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[356]);
                }
                if ( (short)parms[146] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[357]);
                }
                if ( (short)parms[147] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[358]);
                }
                if ( (short)parms[148] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[359]);
                }
                if ( (short)parms[149] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[360]);
                }
                if ( (short)parms[150] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[361]);
                }
                if ( (short)parms[151] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[362]);
                }
                if ( (short)parms[152] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[363]);
                }
                if ( (short)parms[153] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[364]);
                }
                if ( (short)parms[154] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[365]);
                }
                if ( (short)parms[155] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[366]);
                }
                if ( (short)parms[156] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[367]);
                }
                if ( (short)parms[157] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[368]);
                }
                if ( (short)parms[158] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[369]);
                }
                if ( (short)parms[159] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[370]);
                }
                if ( (short)parms[160] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[371]);
                }
                if ( (short)parms[161] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[372]);
                }
                if ( (short)parms[162] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[373]);
                }
                if ( (short)parms[163] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[374]);
                }
                if ( (short)parms[164] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[375]);
                }
                if ( (short)parms[165] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[376]);
                }
                if ( (short)parms[166] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[377]);
                }
                if ( (short)parms[167] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[378]);
                }
                if ( (short)parms[168] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[379]);
                }
                if ( (short)parms[169] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[380]);
                }
                if ( (short)parms[170] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[381]);
                }
                if ( (short)parms[171] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[382]);
                }
                if ( (short)parms[172] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[383]);
                }
                if ( (short)parms[173] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[384]);
                }
                if ( (short)parms[174] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[385]);
                }
                if ( (short)parms[175] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[386]);
                }
                if ( (short)parms[176] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[387]);
                }
                if ( (short)parms[177] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[388]);
                }
                if ( (short)parms[178] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[389]);
                }
                if ( (short)parms[179] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[390]);
                }
                if ( (short)parms[180] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[391]);
                }
                if ( (short)parms[181] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[392]);
                }
                if ( (short)parms[182] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[393]);
                }
                if ( (short)parms[183] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[394]);
                }
                if ( (short)parms[184] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[395]);
                }
                if ( (short)parms[185] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[396]);
                }
                if ( (short)parms[186] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[397]);
                }
                if ( (short)parms[187] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[398]);
                }
                if ( (short)parms[188] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[399]);
                }
                if ( (short)parms[189] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[400]);
                }
                if ( (short)parms[190] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[401]);
                }
                if ( (short)parms[191] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[402]);
                }
                if ( (short)parms[192] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[403]);
                }
                if ( (short)parms[193] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[404]);
                }
                if ( (short)parms[194] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[405]);
                }
                if ( (short)parms[195] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[406]);
                }
                if ( (short)parms[196] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[407]);
                }
                if ( (short)parms[197] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[408]);
                }
                if ( (short)parms[198] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[409]);
                }
                if ( (short)parms[199] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[410]);
                }
                if ( (short)parms[200] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[411]);
                }
                if ( (short)parms[201] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[412]);
                }
                if ( (short)parms[202] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[413]);
                }
                if ( (short)parms[203] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[414]);
                }
                if ( (short)parms[204] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[415]);
                }
                if ( (short)parms[205] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[416]);
                }
                if ( (short)parms[206] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[417]);
                }
                if ( (short)parms[207] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[418]);
                }
                if ( (short)parms[208] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[419]);
                }
                if ( (short)parms[209] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[420]);
                }
                if ( (short)parms[210] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[421]);
                }
                return;
       }
    }

 }

}
