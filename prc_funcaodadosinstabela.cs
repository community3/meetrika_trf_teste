/*
               File: PRC_FuncaoDadosInsTabela
        Description: Funcao Dados Ins Tabela
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:0:53.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_funcaodadosinstabela : GXProcedure
   {
      public prc_funcaodadosinstabela( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_funcaodadosinstabela( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoDados_Codigo ,
                           int aP1_Tabela_CoDIGO )
      {
         this.AV10FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.AV8Tabela_CoDIGO = aP1_Tabela_CoDIGO;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_FuncaoDados_Codigo ,
                                 int aP1_Tabela_CoDIGO )
      {
         prc_funcaodadosinstabela objprc_funcaodadosinstabela;
         objprc_funcaodadosinstabela = new prc_funcaodadosinstabela();
         objprc_funcaodadosinstabela.AV10FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         objprc_funcaodadosinstabela.AV8Tabela_CoDIGO = aP1_Tabela_CoDIGO;
         objprc_funcaodadosinstabela.context.SetSubmitInitialConfig(context);
         objprc_funcaodadosinstabela.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_funcaodadosinstabela);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_funcaodadosinstabela)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9FuncaoDadosTabela = new SdtFuncaoDadosTabela(context);
         AV9FuncaoDadosTabela.gxTpr_Funcaodados_codigo = AV10FuncaoDados_Codigo;
         AV9FuncaoDadosTabela.gxTpr_Tabela_codigo = AV8Tabela_CoDIGO;
         AV9FuncaoDadosTabela.Save();
         if ( AV9FuncaoDadosTabela.Success() )
         {
            context.CommitDataStores( "PRC_FuncaoDadosInsTabela");
         }
         else
         {
            context.RollbackDataStores( "PRC_FuncaoDadosInsTabela");
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9FuncaoDadosTabela = new SdtFuncaoDadosTabela(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_funcaodadosinstabela__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV10FuncaoDados_Codigo ;
      private int AV8Tabela_CoDIGO ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private SdtFuncaoDadosTabela AV9FuncaoDadosTabela ;
   }

   public class prc_funcaodadosinstabela__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
