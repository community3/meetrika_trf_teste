/*
               File: FuncoesUsuarioFuncoesAPF_BC
        Description: Funcoes Usuario Funcoes APF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:38:30.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcoesusuariofuncoesapf_bc : GXHttpHandler, IGxSilentTrn
   {
      public funcoesusuariofuncoesapf_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public funcoesusuariofuncoesapf_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1542( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1542( ) ;
         standaloneModal( ) ;
         AddRow1542( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
               Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_150( )
      {
         BeforeValidate1542( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1542( ) ;
            }
            else
            {
               CheckExtendedTable1542( ) ;
               if ( AnyError == 0 )
               {
                  ZM1542( 2) ;
                  ZM1542( 3) ;
               }
               CloseExtendedTableCursors1542( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM1542( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z162FuncaoUsuario_Nome = A162FuncaoUsuario_Nome;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z166FuncaoAPF_Nome = A166FuncaoAPF_Nome;
         }
         if ( GX_JID == -1 )
         {
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
            Z162FuncaoUsuario_Nome = A162FuncaoUsuario_Nome;
            Z166FuncaoAPF_Nome = A166FuncaoAPF_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load1542( )
      {
         /* Using cursor BC00156 */
         pr_default.execute(4, new Object[] {A161FuncaoUsuario_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound42 = 1;
            A162FuncaoUsuario_Nome = BC00156_A162FuncaoUsuario_Nome[0];
            A166FuncaoAPF_Nome = BC00156_A166FuncaoAPF_Nome[0];
            ZM1542( -1) ;
         }
         pr_default.close(4);
         OnLoadActions1542( ) ;
      }

      protected void OnLoadActions1542( )
      {
      }

      protected void CheckExtendedTable1542( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00154 */
         pr_default.execute(2, new Object[] {A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
            AnyError = 1;
         }
         A162FuncaoUsuario_Nome = BC00154_A162FuncaoUsuario_Nome[0];
         pr_default.close(2);
         /* Using cursor BC00155 */
         pr_default.execute(3, new Object[] {A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Fun��es APF - An�lise de Ponto de Fun��o'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_CODIGO");
            AnyError = 1;
         }
         A166FuncaoAPF_Nome = BC00155_A166FuncaoAPF_Nome[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1542( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1542( )
      {
         /* Using cursor BC00157 */
         pr_default.execute(5, new Object[] {A161FuncaoUsuario_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound42 = 1;
         }
         else
         {
            RcdFound42 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00153 */
         pr_default.execute(1, new Object[] {A161FuncaoUsuario_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1542( 1) ;
            RcdFound42 = 1;
            A161FuncaoUsuario_Codigo = BC00153_A161FuncaoUsuario_Codigo[0];
            A165FuncaoAPF_Codigo = BC00153_A165FuncaoAPF_Codigo[0];
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
            sMode42 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1542( ) ;
            if ( AnyError == 1 )
            {
               RcdFound42 = 0;
               InitializeNonKey1542( ) ;
            }
            Gx_mode = sMode42;
         }
         else
         {
            RcdFound42 = 0;
            InitializeNonKey1542( ) ;
            sMode42 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode42;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1542( ) ;
         if ( RcdFound42 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_150( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1542( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00152 */
            pr_default.execute(0, new Object[] {A161FuncaoUsuario_Codigo, A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncoesUsuarioFuncoesAPF"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"FuncoesUsuarioFuncoesAPF"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1542( )
      {
         BeforeValidate1542( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1542( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1542( 0) ;
            CheckOptimisticConcurrency1542( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1542( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1542( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00158 */
                     pr_default.execute(6, new Object[] {A161FuncaoUsuario_Codigo, A165FuncaoAPF_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncoesUsuarioFuncoesAPF") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1542( ) ;
            }
            EndLevel1542( ) ;
         }
         CloseExtendedTableCursors1542( ) ;
      }

      protected void Update1542( )
      {
         BeforeValidate1542( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1542( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1542( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1542( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1542( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [FuncoesUsuarioFuncoesAPF] */
                     DeferredUpdate1542( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1542( ) ;
         }
         CloseExtendedTableCursors1542( ) ;
      }

      protected void DeferredUpdate1542( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1542( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1542( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1542( ) ;
            AfterConfirm1542( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1542( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC00159 */
                  pr_default.execute(7, new Object[] {A161FuncaoUsuario_Codigo, A165FuncaoAPF_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncoesUsuarioFuncoesAPF") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode42 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1542( ) ;
         Gx_mode = sMode42;
      }

      protected void OnDeleteControls1542( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC001510 */
            pr_default.execute(8, new Object[] {A161FuncaoUsuario_Codigo});
            A162FuncaoUsuario_Nome = BC001510_A162FuncaoUsuario_Nome[0];
            pr_default.close(8);
            /* Using cursor BC001511 */
            pr_default.execute(9, new Object[] {A165FuncaoAPF_Codigo});
            A166FuncaoAPF_Nome = BC001511_A166FuncaoAPF_Nome[0];
            pr_default.close(9);
         }
      }

      protected void EndLevel1542( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1542( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1542( )
      {
         /* Using cursor BC001512 */
         pr_default.execute(10, new Object[] {A161FuncaoUsuario_Codigo, A165FuncaoAPF_Codigo});
         RcdFound42 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound42 = 1;
            A162FuncaoUsuario_Nome = BC001512_A162FuncaoUsuario_Nome[0];
            A166FuncaoAPF_Nome = BC001512_A166FuncaoAPF_Nome[0];
            A161FuncaoUsuario_Codigo = BC001512_A161FuncaoUsuario_Codigo[0];
            A165FuncaoAPF_Codigo = BC001512_A165FuncaoAPF_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1542( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound42 = 0;
         ScanKeyLoad1542( ) ;
      }

      protected void ScanKeyLoad1542( )
      {
         sMode42 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound42 = 1;
            A162FuncaoUsuario_Nome = BC001512_A162FuncaoUsuario_Nome[0];
            A166FuncaoAPF_Nome = BC001512_A166FuncaoAPF_Nome[0];
            A161FuncaoUsuario_Codigo = BC001512_A161FuncaoUsuario_Codigo[0];
            A165FuncaoAPF_Codigo = BC001512_A165FuncaoAPF_Codigo[0];
         }
         Gx_mode = sMode42;
      }

      protected void ScanKeyEnd1542( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm1542( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1542( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1542( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1542( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1542( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1542( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1542( )
      {
      }

      protected void AddRow1542( )
      {
         VarsToRow42( bcFuncoesUsuarioFuncoesAPF) ;
      }

      protected void ReadRow1542( )
      {
         RowToVars42( bcFuncoesUsuarioFuncoesAPF, 1) ;
      }

      protected void InitializeNonKey1542( )
      {
         A162FuncaoUsuario_Nome = "";
         A166FuncaoAPF_Nome = "";
      }

      protected void InitAll1542( )
      {
         A161FuncaoUsuario_Codigo = 0;
         A165FuncaoAPF_Codigo = 0;
         InitializeNonKey1542( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow42( SdtFuncoesUsuarioFuncoesAPF obj42 )
      {
         obj42.gxTpr_Mode = Gx_mode;
         obj42.gxTpr_Funcaousuario_nome = A162FuncaoUsuario_Nome;
         obj42.gxTpr_Funcaoapf_nome = A166FuncaoAPF_Nome;
         obj42.gxTpr_Funcaousuario_codigo = A161FuncaoUsuario_Codigo;
         obj42.gxTpr_Funcaoapf_codigo = A165FuncaoAPF_Codigo;
         obj42.gxTpr_Funcaousuario_codigo_Z = Z161FuncaoUsuario_Codigo;
         obj42.gxTpr_Funcaousuario_nome_Z = Z162FuncaoUsuario_Nome;
         obj42.gxTpr_Funcaoapf_codigo_Z = Z165FuncaoAPF_Codigo;
         obj42.gxTpr_Funcaoapf_nome_Z = Z166FuncaoAPF_Nome;
         obj42.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow42( SdtFuncoesUsuarioFuncoesAPF obj42 )
      {
         obj42.gxTpr_Funcaousuario_codigo = A161FuncaoUsuario_Codigo;
         obj42.gxTpr_Funcaoapf_codigo = A165FuncaoAPF_Codigo;
         return  ;
      }

      public void RowToVars42( SdtFuncoesUsuarioFuncoesAPF obj42 ,
                               int forceLoad )
      {
         Gx_mode = obj42.gxTpr_Mode;
         A162FuncaoUsuario_Nome = obj42.gxTpr_Funcaousuario_nome;
         A166FuncaoAPF_Nome = obj42.gxTpr_Funcaoapf_nome;
         A161FuncaoUsuario_Codigo = obj42.gxTpr_Funcaousuario_codigo;
         A165FuncaoAPF_Codigo = obj42.gxTpr_Funcaoapf_codigo;
         Z161FuncaoUsuario_Codigo = obj42.gxTpr_Funcaousuario_codigo_Z;
         Z162FuncaoUsuario_Nome = obj42.gxTpr_Funcaousuario_nome_Z;
         Z165FuncaoAPF_Codigo = obj42.gxTpr_Funcaoapf_codigo_Z;
         Z166FuncaoAPF_Nome = obj42.gxTpr_Funcaoapf_nome_Z;
         Gx_mode = obj42.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A161FuncaoUsuario_Codigo = (int)getParm(obj,0);
         A165FuncaoAPF_Codigo = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1542( ) ;
         ScanKeyStart1542( ) ;
         if ( RcdFound42 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001510 */
            pr_default.execute(8, new Object[] {A161FuncaoUsuario_Codigo});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
               AnyError = 1;
            }
            A162FuncaoUsuario_Nome = BC001510_A162FuncaoUsuario_Nome[0];
            pr_default.close(8);
            /* Using cursor BC001511 */
            pr_default.execute(9, new Object[] {A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Fun��es APF - An�lise de Ponto de Fun��o'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_CODIGO");
               AnyError = 1;
            }
            A166FuncaoAPF_Nome = BC001511_A166FuncaoAPF_Nome[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
         }
         ZM1542( -1) ;
         OnLoadActions1542( ) ;
         AddRow1542( ) ;
         ScanKeyEnd1542( ) ;
         if ( RcdFound42 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars42( bcFuncoesUsuarioFuncoesAPF, 0) ;
         ScanKeyStart1542( ) ;
         if ( RcdFound42 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001510 */
            pr_default.execute(8, new Object[] {A161FuncaoUsuario_Codigo});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
               AnyError = 1;
            }
            A162FuncaoUsuario_Nome = BC001510_A162FuncaoUsuario_Nome[0];
            pr_default.close(8);
            /* Using cursor BC001511 */
            pr_default.execute(9, new Object[] {A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Fun��es APF - An�lise de Ponto de Fun��o'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_CODIGO");
               AnyError = 1;
            }
            A166FuncaoAPF_Nome = BC001511_A166FuncaoAPF_Nome[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
         }
         ZM1542( -1) ;
         OnLoadActions1542( ) ;
         AddRow1542( ) ;
         ScanKeyEnd1542( ) ;
         if ( RcdFound42 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars42( bcFuncoesUsuarioFuncoesAPF, 0) ;
         nKeyPressed = 1;
         GetKey1542( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1542( ) ;
         }
         else
         {
            if ( RcdFound42 == 1 )
            {
               if ( ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo ) || ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo ) )
               {
                  A161FuncaoUsuario_Codigo = Z161FuncaoUsuario_Codigo;
                  A165FuncaoAPF_Codigo = Z165FuncaoAPF_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1542( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo ) || ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1542( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1542( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow42( bcFuncoesUsuarioFuncoesAPF) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars42( bcFuncoesUsuarioFuncoesAPF, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1542( ) ;
         if ( RcdFound42 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo ) || ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo ) )
            {
               A161FuncaoUsuario_Codigo = Z161FuncaoUsuario_Codigo;
               A165FuncaoAPF_Codigo = Z165FuncaoAPF_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo ) || ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(8);
         pr_default.close(9);
         context.RollbackDataStores( "FuncoesUsuarioFuncoesAPF_BC");
         VarsToRow42( bcFuncoesUsuarioFuncoesAPF) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcFuncoesUsuarioFuncoesAPF.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcFuncoesUsuarioFuncoesAPF.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcFuncoesUsuarioFuncoesAPF )
         {
            bcFuncoesUsuarioFuncoesAPF = (SdtFuncoesUsuarioFuncoesAPF)(sdt);
            if ( StringUtil.StrCmp(bcFuncoesUsuarioFuncoesAPF.gxTpr_Mode, "") == 0 )
            {
               bcFuncoesUsuarioFuncoesAPF.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow42( bcFuncoesUsuarioFuncoesAPF) ;
            }
            else
            {
               RowToVars42( bcFuncoesUsuarioFuncoesAPF, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcFuncoesUsuarioFuncoesAPF.gxTpr_Mode, "") == 0 )
            {
               bcFuncoesUsuarioFuncoesAPF.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars42( bcFuncoesUsuarioFuncoesAPF, 1) ;
         return  ;
      }

      public SdtFuncoesUsuarioFuncoesAPF FuncoesUsuarioFuncoesAPF_BC
      {
         get {
            return bcFuncoesUsuarioFuncoesAPF ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(8);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z162FuncaoUsuario_Nome = "";
         A162FuncaoUsuario_Nome = "";
         Z166FuncaoAPF_Nome = "";
         A166FuncaoAPF_Nome = "";
         BC00156_A162FuncaoUsuario_Nome = new String[] {""} ;
         BC00156_A166FuncaoAPF_Nome = new String[] {""} ;
         BC00156_A161FuncaoUsuario_Codigo = new int[1] ;
         BC00156_A165FuncaoAPF_Codigo = new int[1] ;
         BC00154_A162FuncaoUsuario_Nome = new String[] {""} ;
         BC00155_A166FuncaoAPF_Nome = new String[] {""} ;
         BC00157_A161FuncaoUsuario_Codigo = new int[1] ;
         BC00157_A165FuncaoAPF_Codigo = new int[1] ;
         BC00153_A161FuncaoUsuario_Codigo = new int[1] ;
         BC00153_A165FuncaoAPF_Codigo = new int[1] ;
         sMode42 = "";
         BC00152_A161FuncaoUsuario_Codigo = new int[1] ;
         BC00152_A165FuncaoAPF_Codigo = new int[1] ;
         BC001510_A162FuncaoUsuario_Nome = new String[] {""} ;
         BC001511_A166FuncaoAPF_Nome = new String[] {""} ;
         BC001512_A162FuncaoUsuario_Nome = new String[] {""} ;
         BC001512_A166FuncaoAPF_Nome = new String[] {""} ;
         BC001512_A161FuncaoUsuario_Codigo = new int[1] ;
         BC001512_A165FuncaoAPF_Codigo = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcoesusuariofuncoesapf_bc__default(),
            new Object[][] {
                new Object[] {
               BC00152_A161FuncaoUsuario_Codigo, BC00152_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               BC00153_A161FuncaoUsuario_Codigo, BC00153_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               BC00154_A162FuncaoUsuario_Nome
               }
               , new Object[] {
               BC00155_A166FuncaoAPF_Nome
               }
               , new Object[] {
               BC00156_A162FuncaoUsuario_Nome, BC00156_A166FuncaoAPF_Nome, BC00156_A161FuncaoUsuario_Codigo, BC00156_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               BC00157_A161FuncaoUsuario_Codigo, BC00157_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001510_A162FuncaoUsuario_Nome
               }
               , new Object[] {
               BC001511_A166FuncaoAPF_Nome
               }
               , new Object[] {
               BC001512_A162FuncaoUsuario_Nome, BC001512_A166FuncaoAPF_Nome, BC001512_A161FuncaoUsuario_Codigo, BC001512_A165FuncaoAPF_Codigo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound42 ;
      private int trnEnded ;
      private int Z161FuncaoUsuario_Codigo ;
      private int A161FuncaoUsuario_Codigo ;
      private int Z165FuncaoAPF_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode42 ;
      private String Z162FuncaoUsuario_Nome ;
      private String A162FuncaoUsuario_Nome ;
      private String Z166FuncaoAPF_Nome ;
      private String A166FuncaoAPF_Nome ;
      private SdtFuncoesUsuarioFuncoesAPF bcFuncoesUsuarioFuncoesAPF ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC00156_A162FuncaoUsuario_Nome ;
      private String[] BC00156_A166FuncaoAPF_Nome ;
      private int[] BC00156_A161FuncaoUsuario_Codigo ;
      private int[] BC00156_A165FuncaoAPF_Codigo ;
      private String[] BC00154_A162FuncaoUsuario_Nome ;
      private String[] BC00155_A166FuncaoAPF_Nome ;
      private int[] BC00157_A161FuncaoUsuario_Codigo ;
      private int[] BC00157_A165FuncaoAPF_Codigo ;
      private int[] BC00153_A161FuncaoUsuario_Codigo ;
      private int[] BC00153_A165FuncaoAPF_Codigo ;
      private int[] BC00152_A161FuncaoUsuario_Codigo ;
      private int[] BC00152_A165FuncaoAPF_Codigo ;
      private String[] BC001510_A162FuncaoUsuario_Nome ;
      private String[] BC001511_A166FuncaoAPF_Nome ;
      private String[] BC001512_A162FuncaoUsuario_Nome ;
      private String[] BC001512_A166FuncaoAPF_Nome ;
      private int[] BC001512_A161FuncaoUsuario_Codigo ;
      private int[] BC001512_A165FuncaoAPF_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class funcoesusuariofuncoesapf_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00156 ;
          prmBC00156 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00154 ;
          prmBC00154 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00155 ;
          prmBC00155 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00157 ;
          prmBC00157 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00153 ;
          prmBC00153 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00152 ;
          prmBC00152 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00158 ;
          prmBC00158 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00159 ;
          prmBC00159 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001512 ;
          prmBC001512 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001510 ;
          prmBC001510 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001511 ;
          prmBC001511 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00152", "SELECT [FuncaoUsuario_Codigo], [FuncaoAPF_Codigo] FROM [FuncoesUsuarioFuncoesAPF] WITH (UPDLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo AND [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00152,1,0,true,false )
             ,new CursorDef("BC00153", "SELECT [FuncaoUsuario_Codigo], [FuncaoAPF_Codigo] FROM [FuncoesUsuarioFuncoesAPF] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo AND [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00153,1,0,true,false )
             ,new CursorDef("BC00154", "SELECT [FuncaoUsuario_Nome] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00154,1,0,true,false )
             ,new CursorDef("BC00155", "SELECT [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00155,1,0,true,false )
             ,new CursorDef("BC00156", "SELECT T2.[FuncaoUsuario_Nome], T3.[FuncaoAPF_Nome], TM1.[FuncaoUsuario_Codigo], TM1.[FuncaoAPF_Codigo] FROM (([FuncoesUsuarioFuncoesAPF] TM1 WITH (NOLOCK) INNER JOIN [ModuloFuncoes] T2 WITH (NOLOCK) ON T2.[FuncaoUsuario_Codigo] = TM1.[FuncaoUsuario_Codigo]) INNER JOIN [FuncoesAPF] T3 WITH (NOLOCK) ON T3.[FuncaoAPF_Codigo] = TM1.[FuncaoAPF_Codigo]) WHERE TM1.[FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo and TM1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY TM1.[FuncaoUsuario_Codigo], TM1.[FuncaoAPF_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00156,100,0,true,false )
             ,new CursorDef("BC00157", "SELECT [FuncaoUsuario_Codigo], [FuncaoAPF_Codigo] FROM [FuncoesUsuarioFuncoesAPF] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo AND [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00157,1,0,true,false )
             ,new CursorDef("BC00158", "INSERT INTO [FuncoesUsuarioFuncoesAPF]([FuncaoUsuario_Codigo], [FuncaoAPF_Codigo]) VALUES(@FuncaoUsuario_Codigo, @FuncaoAPF_Codigo)", GxErrorMask.GX_NOMASK,prmBC00158)
             ,new CursorDef("BC00159", "DELETE FROM [FuncoesUsuarioFuncoesAPF]  WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo AND [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK,prmBC00159)
             ,new CursorDef("BC001510", "SELECT [FuncaoUsuario_Nome] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001510,1,0,true,false )
             ,new CursorDef("BC001511", "SELECT [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001511,1,0,true,false )
             ,new CursorDef("BC001512", "SELECT T2.[FuncaoUsuario_Nome], T3.[FuncaoAPF_Nome], TM1.[FuncaoUsuario_Codigo], TM1.[FuncaoAPF_Codigo] FROM (([FuncoesUsuarioFuncoesAPF] TM1 WITH (NOLOCK) INNER JOIN [ModuloFuncoes] T2 WITH (NOLOCK) ON T2.[FuncaoUsuario_Codigo] = TM1.[FuncaoUsuario_Codigo]) INNER JOIN [FuncoesAPF] T3 WITH (NOLOCK) ON T3.[FuncaoAPF_Codigo] = TM1.[FuncaoAPF_Codigo]) WHERE TM1.[FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo and TM1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY TM1.[FuncaoUsuario_Codigo], TM1.[FuncaoAPF_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001512,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
