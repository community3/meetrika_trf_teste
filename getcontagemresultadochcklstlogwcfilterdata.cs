/*
               File: GetContagemResultadoChckLstLogWCFilterData
        Description: Get Contagem Resultado Chck Lst Log WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 5:20:5.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontagemresultadochcklstlogwcfilterdata : GXProcedure
   {
      public getcontagemresultadochcklstlogwcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontagemresultadochcklstlogwcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontagemresultadochcklstlogwcfilterdata objgetcontagemresultadochcklstlogwcfilterdata;
         objgetcontagemresultadochcklstlogwcfilterdata = new getcontagemresultadochcklstlogwcfilterdata();
         objgetcontagemresultadochcklstlogwcfilterdata.AV16DDOName = aP0_DDOName;
         objgetcontagemresultadochcklstlogwcfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetcontagemresultadochcklstlogwcfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetcontagemresultadochcklstlogwcfilterdata.AV20OptionsJson = "" ;
         objgetcontagemresultadochcklstlogwcfilterdata.AV23OptionsDescJson = "" ;
         objgetcontagemresultadochcklstlogwcfilterdata.AV25OptionIndexesJson = "" ;
         objgetcontagemresultadochcklstlogwcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontagemresultadochcklstlogwcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontagemresultadochcklstlogwcfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontagemresultadochcklstlogwcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("ContagemResultadoChckLstLogWCGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContagemResultadoChckLstLogWCGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("ContagemResultadoChckLstLogWCGridState"), "");
         }
         AV36GXV1 = 1;
         while ( AV36GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV36GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
            {
               AV10TFContagemResultadoChckLstLog_DataHora = context.localUtil.CToT( AV30GridStateFilterValue.gxTpr_Value, 2);
               AV11TFContagemResultadoChckLstLog_DataHora_To = context.localUtil.CToT( AV30GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 )
            {
               AV12TFContagemResultadoChckLstLog_UsuarioNome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL") == 0 )
            {
               AV13TFContagemResultadoChckLstLog_UsuarioNome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "PARM_&CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO") == 0 )
            {
               AV33ContagemResultadoChckLstLog_OSCodigo = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
            }
            AV36GXV1 = (int)(AV36GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEOPTIONS' Routine */
         AV12TFContagemResultadoChckLstLog_UsuarioNome = AV14SearchTxt;
         AV13TFContagemResultadoChckLstLog_UsuarioNome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV10TFContagemResultadoChckLstLog_DataHora ,
                                              AV11TFContagemResultadoChckLstLog_DataHora_To ,
                                              AV13TFContagemResultadoChckLstLog_UsuarioNome_Sel ,
                                              AV12TFContagemResultadoChckLstLog_UsuarioNome ,
                                              A814ContagemResultadoChckLstLog_DataHora ,
                                              A817ContagemResultadoChckLstLog_UsuarioNome ,
                                              AV33ContagemResultadoChckLstLog_OSCodigo ,
                                              A1853ContagemResultadoChckLstLog_OSCodigo },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV12TFContagemResultadoChckLstLog_UsuarioNome = StringUtil.PadR( StringUtil.RTrim( AV12TFContagemResultadoChckLstLog_UsuarioNome), 100, "%");
         /* Using cursor P00S62 */
         pr_default.execute(0, new Object[] {AV33ContagemResultadoChckLstLog_OSCodigo, AV10TFContagemResultadoChckLstLog_DataHora, AV11TFContagemResultadoChckLstLog_DataHora_To, lV12TFContagemResultadoChckLstLog_UsuarioNome, AV13TFContagemResultadoChckLstLog_UsuarioNome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKS62 = false;
            A822ContagemResultadoChckLstLog_UsuarioCod = P00S62_A822ContagemResultadoChckLstLog_UsuarioCod[0];
            A823ContagemResultadoChckLstLog_PessoaCod = P00S62_A823ContagemResultadoChckLstLog_PessoaCod[0];
            n823ContagemResultadoChckLstLog_PessoaCod = P00S62_n823ContagemResultadoChckLstLog_PessoaCod[0];
            A1853ContagemResultadoChckLstLog_OSCodigo = P00S62_A1853ContagemResultadoChckLstLog_OSCodigo[0];
            A817ContagemResultadoChckLstLog_UsuarioNome = P00S62_A817ContagemResultadoChckLstLog_UsuarioNome[0];
            n817ContagemResultadoChckLstLog_UsuarioNome = P00S62_n817ContagemResultadoChckLstLog_UsuarioNome[0];
            A814ContagemResultadoChckLstLog_DataHora = P00S62_A814ContagemResultadoChckLstLog_DataHora[0];
            A820ContagemResultadoChckLstLog_Codigo = P00S62_A820ContagemResultadoChckLstLog_Codigo[0];
            A823ContagemResultadoChckLstLog_PessoaCod = P00S62_A823ContagemResultadoChckLstLog_PessoaCod[0];
            n823ContagemResultadoChckLstLog_PessoaCod = P00S62_n823ContagemResultadoChckLstLog_PessoaCod[0];
            A817ContagemResultadoChckLstLog_UsuarioNome = P00S62_A817ContagemResultadoChckLstLog_UsuarioNome[0];
            n817ContagemResultadoChckLstLog_UsuarioNome = P00S62_n817ContagemResultadoChckLstLog_UsuarioNome[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00S62_A1853ContagemResultadoChckLstLog_OSCodigo[0] == A1853ContagemResultadoChckLstLog_OSCodigo ) && ( StringUtil.StrCmp(P00S62_A817ContagemResultadoChckLstLog_UsuarioNome[0], A817ContagemResultadoChckLstLog_UsuarioNome) == 0 ) )
            {
               BRKS62 = false;
               A822ContagemResultadoChckLstLog_UsuarioCod = P00S62_A822ContagemResultadoChckLstLog_UsuarioCod[0];
               A823ContagemResultadoChckLstLog_PessoaCod = P00S62_A823ContagemResultadoChckLstLog_PessoaCod[0];
               n823ContagemResultadoChckLstLog_PessoaCod = P00S62_n823ContagemResultadoChckLstLog_PessoaCod[0];
               A820ContagemResultadoChckLstLog_Codigo = P00S62_A820ContagemResultadoChckLstLog_Codigo[0];
               A823ContagemResultadoChckLstLog_PessoaCod = P00S62_A823ContagemResultadoChckLstLog_PessoaCod[0];
               n823ContagemResultadoChckLstLog_PessoaCod = P00S62_n823ContagemResultadoChckLstLog_PessoaCod[0];
               AV26count = (long)(AV26count+1);
               BRKS62 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A817ContagemResultadoChckLstLog_UsuarioNome)) )
            {
               AV18Option = A817ContagemResultadoChckLstLog_UsuarioNome;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKS62 )
            {
               BRKS62 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         AV11TFContagemResultadoChckLstLog_DataHora_To = (DateTime)(DateTime.MinValue);
         AV12TFContagemResultadoChckLstLog_UsuarioNome = "";
         AV13TFContagemResultadoChckLstLog_UsuarioNome_Sel = "";
         scmdbuf = "";
         lV12TFContagemResultadoChckLstLog_UsuarioNome = "";
         A814ContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         A817ContagemResultadoChckLstLog_UsuarioNome = "";
         P00S62_A822ContagemResultadoChckLstLog_UsuarioCod = new int[1] ;
         P00S62_A823ContagemResultadoChckLstLog_PessoaCod = new int[1] ;
         P00S62_n823ContagemResultadoChckLstLog_PessoaCod = new bool[] {false} ;
         P00S62_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         P00S62_A817ContagemResultadoChckLstLog_UsuarioNome = new String[] {""} ;
         P00S62_n817ContagemResultadoChckLstLog_UsuarioNome = new bool[] {false} ;
         P00S62_A814ContagemResultadoChckLstLog_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00S62_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         AV18Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontagemresultadochcklstlogwcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00S62_A822ContagemResultadoChckLstLog_UsuarioCod, P00S62_A823ContagemResultadoChckLstLog_PessoaCod, P00S62_n823ContagemResultadoChckLstLog_PessoaCod, P00S62_A1853ContagemResultadoChckLstLog_OSCodigo, P00S62_A817ContagemResultadoChckLstLog_UsuarioNome, P00S62_n817ContagemResultadoChckLstLog_UsuarioNome, P00S62_A814ContagemResultadoChckLstLog_DataHora, P00S62_A820ContagemResultadoChckLstLog_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV36GXV1 ;
      private int AV33ContagemResultadoChckLstLog_OSCodigo ;
      private int A1853ContagemResultadoChckLstLog_OSCodigo ;
      private int A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int A823ContagemResultadoChckLstLog_PessoaCod ;
      private int A820ContagemResultadoChckLstLog_Codigo ;
      private long AV26count ;
      private String AV12TFContagemResultadoChckLstLog_UsuarioNome ;
      private String AV13TFContagemResultadoChckLstLog_UsuarioNome_Sel ;
      private String scmdbuf ;
      private String lV12TFContagemResultadoChckLstLog_UsuarioNome ;
      private String A817ContagemResultadoChckLstLog_UsuarioNome ;
      private DateTime AV10TFContagemResultadoChckLstLog_DataHora ;
      private DateTime AV11TFContagemResultadoChckLstLog_DataHora_To ;
      private DateTime A814ContagemResultadoChckLstLog_DataHora ;
      private bool returnInSub ;
      private bool BRKS62 ;
      private bool n823ContagemResultadoChckLstLog_PessoaCod ;
      private bool n817ContagemResultadoChckLstLog_UsuarioNome ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00S62_A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int[] P00S62_A823ContagemResultadoChckLstLog_PessoaCod ;
      private bool[] P00S62_n823ContagemResultadoChckLstLog_PessoaCod ;
      private int[] P00S62_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private String[] P00S62_A817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool[] P00S62_n817ContagemResultadoChckLstLog_UsuarioNome ;
      private DateTime[] P00S62_A814ContagemResultadoChckLstLog_DataHora ;
      private int[] P00S62_A820ContagemResultadoChckLstLog_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
   }

   public class getcontagemresultadochcklstlogwcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00S62( IGxContext context ,
                                             DateTime AV10TFContagemResultadoChckLstLog_DataHora ,
                                             DateTime AV11TFContagemResultadoChckLstLog_DataHora_To ,
                                             String AV13TFContagemResultadoChckLstLog_UsuarioNome_Sel ,
                                             String AV12TFContagemResultadoChckLstLog_UsuarioNome ,
                                             DateTime A814ContagemResultadoChckLstLog_DataHora ,
                                             String A817ContagemResultadoChckLstLog_UsuarioNome ,
                                             int AV33ContagemResultadoChckLstLog_OSCodigo ,
                                             int A1853ContagemResultadoChckLstLog_OSCodigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [5] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultadoChckLstLog_UsuarioCod] AS ContagemResultadoChckLstLog_UsuarioCod, T2.[Usuario_PessoaCod] AS ContagemResultadoChckLstLog_PessoaCod, T1.[ContagemResultadoChckLstLog_OSCodigo], T3.[Pessoa_Nome] AS ContagemResultadoChckLstLog_UsuarioNome, T1.[ContagemResultadoChckLstLog_DataHora], T1.[ContagemResultadoChckLstLog_Codigo] FROM (([ContagemResultadoChckLstLog] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoChckLstLog_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultadoChckLstLog_OSCodigo] = @AV33ContagemResultadoChckLstLog_OSCodigo)";
         if ( ! (DateTime.MinValue==AV10TFContagemResultadoChckLstLog_DataHora) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] >= @AV10TFContagemResultadoChckLstLog_DataHora)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFContagemResultadoChckLstLog_DataHora_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] <= @AV11TFContagemResultadoChckLstLog_DataHora_To)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultadoChckLstLog_UsuarioNome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContagemResultadoChckLstLog_UsuarioNome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV12TFContagemResultadoChckLstLog_UsuarioNome)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultadoChckLstLog_UsuarioNome_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV13TFContagemResultadoChckLstLog_UsuarioNome_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultadoChckLstLog_OSCodigo], T3.[Pessoa_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00S62(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00S62 ;
          prmP00S62 = new Object[] {
          new Object[] {"@AV33ContagemResultadoChckLstLog_OSCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10TFContagemResultadoChckLstLog_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV11TFContagemResultadoChckLstLog_DataHora_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV12TFContagemResultadoChckLstLog_UsuarioNome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContagemResultadoChckLstLog_UsuarioNome_Sel",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00S62", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00S62,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontagemresultadochcklstlogwcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontagemresultadochcklstlogwcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontagemresultadochcklstlogwcfilterdata") )
          {
             return  ;
          }
          getcontagemresultadochcklstlogwcfilterdata worker = new getcontagemresultadochcklstlogwcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
