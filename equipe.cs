/*
               File: Equipe
        Description: Equipe
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:32:45.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class equipe : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"USUARIO_CODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAUSUARIO_CODIGO5D238( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A2177Equipe_AreaTrabalhoCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2177Equipe_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2177Equipe_AreaTrabalhoCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A2177Equipe_AreaTrabalhoCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A1Usuario_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_17") == 0 )
         {
            A57Usuario_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_17( A57Usuario_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_16") == 0 )
         {
            A621Funcao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_16( A621Funcao_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridlevel_usuario") == 0 )
         {
            nRC_GXsfl_30 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_30_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_30_idx = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridlevel_usuario_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Equipe_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Equipe_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Equipe_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vEQUIPE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Equipe_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         GXCCtl = "USUARIO_CODIGO_" + sGXsfl_30_idx;
         dynUsuario_Codigo.Name = GXCCtl;
         dynUsuario_Codigo.WebTags = "";
         GXCCtl = "FUNCAO_CODIGO_" + sGXsfl_30_idx;
         dynFuncao_Codigo.Name = GXCCtl;
         dynFuncao_Codigo.WebTags = "";
         dynFuncao_Codigo.removeAllItems();
         /* Using cursor T005D10 */
         pr_default.execute(8);
         while ( (pr_default.getStatus(8) != 101) )
         {
            dynFuncao_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T005D10_A621Funcao_Codigo[0]), 6, 0)), T005D10_A622Funcao_Nome[0], 0);
            pr_default.readNext(8);
         }
         pr_default.close(8);
         if ( dynFuncao_Codigo.ItemCount > 0 )
         {
            A621Funcao_Codigo = (int)(NumberUtil.Val( dynFuncao_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0))), "."));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Equipe", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtEquipe_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public equipe( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public equipe( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Equipe_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Equipe_Codigo = aP1_Equipe_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynUsuario_Codigo = new GXCombobox();
         dynFuncao_Codigo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_5D237( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_5D237e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEquipe_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2175Equipe_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A2175Equipe_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEquipe_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtEquipe_Codigo_Visible, edtEquipe_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Equipe.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_5D237( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblEquipetitle_Internalname, "Equipe", "", "", lblEquipetitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_Equipe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_5D237( true) ;
         }
         return  ;
      }

      protected void wb_table2_8_5D237e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_38_5D237( true) ;
         }
         return  ;
      }

      protected void wb_table3_38_5D237e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_5D237e( true) ;
         }
         else
         {
            wb_table1_2_5D237e( false) ;
         }
      }

      protected void wb_table3_38_5D237( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Equipe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Equipe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Equipe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_38_5D237e( true) ;
         }
         else
         {
            wb_table3_38_5D237e( false) ;
         }
      }

      protected void wb_table2_8_5D237( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_5D237( true) ;
         }
         return  ;
      }

      protected void wb_table4_16_5D237e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_5D237e( true) ;
         }
         else
         {
            wb_table2_8_5D237e( false) ;
         }
      }

      protected void wb_table4_16_5D237( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_19_5D237( true) ;
         }
         return  ;
      }

      protected void wb_table5_19_5D237e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_27_5D237( true) ;
         }
         return  ;
      }

      protected void wb_table6_27_5D237e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_5D237e( true) ;
         }
         else
         {
            wb_table4_16_5D237e( false) ;
         }
      }

      protected void wb_table6_27_5D237( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableleaflevel_usuario_Internalname, tblTableleaflevel_usuario_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            Gridlevel_usuarioContainer.AddObjectProperty("GridName", "Gridlevel_usuario");
            Gridlevel_usuarioContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
            Gridlevel_usuarioContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            Gridlevel_usuarioContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            Gridlevel_usuarioContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_usuario_Backcolorstyle), 1, 0, ".", "")));
            Gridlevel_usuarioContainer.AddObjectProperty("CmpContext", "");
            Gridlevel_usuarioContainer.AddObjectProperty("InMasterPage", "false");
            Gridlevel_usuarioColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_usuarioColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", "")));
            Gridlevel_usuarioColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynUsuario_Codigo.Enabled), 5, 0, ".", "")));
            Gridlevel_usuarioContainer.AddColumnProperties(Gridlevel_usuarioColumn);
            Gridlevel_usuarioColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_usuarioColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ".", "")));
            Gridlevel_usuarioColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaCod_Enabled), 5, 0, ".", "")));
            Gridlevel_usuarioContainer.AddColumnProperties(Gridlevel_usuarioColumn);
            Gridlevel_usuarioColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_usuarioColumn.AddObjectProperty("Value", StringUtil.RTrim( A58Usuario_PessoaNom));
            Gridlevel_usuarioColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaNom_Enabled), 5, 0, ".", "")));
            Gridlevel_usuarioContainer.AddColumnProperties(Gridlevel_usuarioColumn);
            Gridlevel_usuarioColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_usuarioColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A621Funcao_Codigo), 6, 0, ".", "")));
            Gridlevel_usuarioColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynFuncao_Codigo.Enabled), 5, 0, ".", "")));
            Gridlevel_usuarioContainer.AddColumnProperties(Gridlevel_usuarioColumn);
            Gridlevel_usuarioColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_usuarioColumn.AddObjectProperty("Value", A622Funcao_Nome);
            Gridlevel_usuarioColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncao_Nome_Enabled), 5, 0, ".", "")));
            Gridlevel_usuarioContainer.AddColumnProperties(Gridlevel_usuarioColumn);
            Gridlevel_usuarioContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_usuario_Allowselection), 1, 0, ".", "")));
            Gridlevel_usuarioContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_usuario_Selectioncolor), 9, 0, ".", "")));
            Gridlevel_usuarioContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_usuario_Allowhovering), 1, 0, ".", "")));
            Gridlevel_usuarioContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_usuario_Hoveringcolor), 9, 0, ".", "")));
            Gridlevel_usuarioContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_usuario_Allowcollapsing), 1, 0, ".", "")));
            Gridlevel_usuarioContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_usuario_Collapsed), 1, 0, ".", "")));
            nGXsfl_30_idx = 0;
            if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
            {
               /* Enter key processing. */
               nBlankRcdCount238 = 5;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  /* Display confirmed (stored) records */
                  nRcdExists_238 = 1;
                  ScanStart5D238( ) ;
                  while ( RcdFound238 != 0 )
                  {
                     init_level_properties238( ) ;
                     getByPrimaryKey5D238( ) ;
                     AddRow5D238( ) ;
                     ScanNext5D238( ) ;
                  }
                  ScanEnd5D238( ) ;
                  nBlankRcdCount238 = 5;
               }
            }
            else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
            {
               /* Button check  or addlines. */
               standaloneNotModal5D238( ) ;
               standaloneModal5D238( ) ;
               sMode238 = Gx_mode;
               while ( nGXsfl_30_idx < nRC_GXsfl_30 )
               {
                  ReadRow5D238( ) ;
                  dynUsuario_Codigo.Enabled = (int)(context.localUtil.CToN( cgiGet( "USUARIO_CODIGO_"+sGXsfl_30_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynUsuario_Codigo.Enabled), 5, 0)));
                  edtUsuario_PessoaCod_Enabled = (int)(context.localUtil.CToN( cgiGet( "USUARIO_PESSOACOD_"+sGXsfl_30_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaCod_Enabled), 5, 0)));
                  edtUsuario_PessoaNom_Enabled = (int)(context.localUtil.CToN( cgiGet( "USUARIO_PESSOANOM_"+sGXsfl_30_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaNom_Enabled), 5, 0)));
                  dynFuncao_Codigo.Enabled = (int)(context.localUtil.CToN( cgiGet( "FUNCAO_CODIGO_"+sGXsfl_30_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynFuncao_Codigo.Enabled), 5, 0)));
                  edtFuncao_Nome_Enabled = (int)(context.localUtil.CToN( cgiGet( "FUNCAO_NOME_"+sGXsfl_30_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncao_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncao_Nome_Enabled), 5, 0)));
                  if ( ( nRcdExists_238 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     standaloneModal5D238( ) ;
                  }
                  SendRow5D238( ) ;
               }
               Gx_mode = sMode238;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            }
            else
            {
               /* Get or get-alike key processing. */
               nBlankRcdCount238 = 5;
               nRcdExists_238 = 1;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  ScanStart5D238( ) ;
                  while ( RcdFound238 != 0 )
                  {
                     sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx+1), 4, 0)), 4, "0");
                     SubsflControlProps_30238( ) ;
                     init_level_properties238( ) ;
                     standaloneNotModal5D238( ) ;
                     getByPrimaryKey5D238( ) ;
                     standaloneModal5D238( ) ;
                     AddRow5D238( ) ;
                     ScanNext5D238( ) ;
                  }
                  ScanEnd5D238( ) ;
               }
            }
            /* Initialize fields for 'new' records and send them. */
            if ( ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
            {
               sMode238 = Gx_mode;
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx+1), 4, 0)), 4, "0");
               SubsflControlProps_30238( ) ;
               InitAll5D238( ) ;
               init_level_properties238( ) ;
               standaloneNotModal5D238( ) ;
               standaloneModal5D238( ) ;
               nRcdExists_238 = 0;
               nIsMod_238 = 0;
               nRcdDeleted_238 = 0;
               nBlankRcdCount238 = (short)(nBlankRcdUsr238+nBlankRcdCount238);
               fRowAdded = 0;
               while ( nBlankRcdCount238 > 0 )
               {
                  AddRow5D238( ) ;
                  if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
                  {
                     fRowAdded = 1;
                     GX_FocusControl = dynUsuario_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  nBlankRcdCount238 = (short)(nBlankRcdCount238-1);
               }
               Gx_mode = sMode238;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            }
            sStyleString = "";
            context.WriteHtmlText( "<div id=\""+"Gridlevel_usuarioContainer"+"Div\" "+sStyleString+">"+"</div>") ;
            context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridlevel_usuario", Gridlevel_usuarioContainer);
            if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Gridlevel_usuarioContainerData", Gridlevel_usuarioContainer.ToJavascriptSource());
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Gridlevel_usuarioContainerData"+"V", Gridlevel_usuarioContainer.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridlevel_usuarioContainerData"+"V"+"\" value='"+Gridlevel_usuarioContainer.GridValuesHidden()+"'/>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_27_5D237e( true) ;
         }
         else
         {
            wb_table6_27_5D237e( false) ;
         }
      }

      protected void wb_table5_19_5D237( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockequipe_nome_Internalname, "Equipe", "", "", lblTextblockequipe_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Equipe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEquipe_Nome_Internalname, StringUtil.RTrim( A2176Equipe_Nome), StringUtil.RTrim( context.localUtil.Format( A2176Equipe_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,24);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEquipe_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtEquipe_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Equipe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_19_5D237e( true) ;
         }
         else
         {
            wb_table5_19_5D237e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E115D2 */
         E115D2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A2176Equipe_Nome = StringUtil.Upper( cgiGet( edtEquipe_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2176Equipe_Nome", A2176Equipe_Nome);
               if ( ( ( context.localUtil.CToN( cgiGet( edtEquipe_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtEquipe_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "EQUIPE_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtEquipe_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2175Equipe_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2175Equipe_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2175Equipe_Codigo), 6, 0)));
               }
               else
               {
                  A2175Equipe_Codigo = (int)(context.localUtil.CToN( cgiGet( edtEquipe_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2175Equipe_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2175Equipe_Codigo), 6, 0)));
               }
               /* Read saved values. */
               Z2175Equipe_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z2175Equipe_Codigo"), ",", "."));
               Z2176Equipe_Nome = cgiGet( "Z2176Equipe_Nome");
               Z2177Equipe_AreaTrabalhoCodigo = (int)(context.localUtil.CToN( cgiGet( "Z2177Equipe_AreaTrabalhoCodigo"), ",", "."));
               A2177Equipe_AreaTrabalhoCodigo = (int)(context.localUtil.CToN( cgiGet( "Z2177Equipe_AreaTrabalhoCodigo"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               nRC_GXsfl_30 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_30"), ",", "."));
               N2177Equipe_AreaTrabalhoCodigo = (int)(context.localUtil.CToN( cgiGet( "N2177Equipe_AreaTrabalhoCodigo"), ",", "."));
               AV7Equipe_Codigo = (int)(context.localUtil.CToN( cgiGet( "vEQUIPE_CODIGO"), ",", "."));
               AV11Insert_Equipe_AreaTrabalhoCodigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_EQUIPE_AREATRABALHOCODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A2177Equipe_AreaTrabalhoCodigo = (int)(context.localUtil.CToN( cgiGet( "EQUIPE_AREATRABALHOCODIGO"), ",", "."));
               A2178Equipe_AreaTrabalhoDescricao = cgiGet( "EQUIPE_AREATRABALHODESCRICAO");
               n2178Equipe_AreaTrabalhoDescricao = false;
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Equipe";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2177Equipe_AreaTrabalhoCodigo), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A2175Equipe_Codigo != Z2175Equipe_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("equipe:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("equipe:[SecurityCheckFailed value for]"+"Equipe_AreaTrabalhoCodigo:"+context.localUtil.Format( (decimal)(A2177Equipe_AreaTrabalhoCodigo), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               /* Check if conditions changed and reset current page numbers */
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A2175Equipe_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2175Equipe_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2175Equipe_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode237 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode237;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound237 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_5D0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "EQUIPE_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtEquipe_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E115D2 */
                           E115D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E125D2 */
                           E125D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E125D2 */
            E125D2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll5D237( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes5D237( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_5D0( )
      {
         BeforeValidate5D237( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls5D237( ) ;
            }
            else
            {
               CheckExtendedTable5D237( ) ;
               CloseExtendedTableCursors5D237( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode237 = Gx_mode;
            CONFIRM_5D238( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode237;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               IsConfirmed = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
            }
            /* Restore parent mode. */
            Gx_mode = sMode237;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void CONFIRM_5D238( )
      {
         nGXsfl_30_idx = 0;
         while ( nGXsfl_30_idx < nRC_GXsfl_30 )
         {
            ReadRow5D238( ) ;
            if ( ( nRcdExists_238 != 0 ) || ( nIsMod_238 != 0 ) )
            {
               GetKey5D238( ) ;
               if ( ( nRcdExists_238 == 0 ) && ( nRcdDeleted_238 == 0 ) )
               {
                  if ( RcdFound238 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     BeforeValidate5D238( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable5D238( ) ;
                        CloseExtendedTableCursors5D238( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                     }
                  }
                  else
                  {
                     GXCCtl = "USUARIO_CODIGO_" + sGXsfl_30_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = dynUsuario_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound238 != 0 )
                  {
                     if ( nRcdDeleted_238 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        getByPrimaryKey5D238( ) ;
                        Load5D238( ) ;
                        BeforeValidate5D238( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls5D238( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_238 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           BeforeValidate5D238( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable5D238( ) ;
                              CloseExtendedTableCursors5D238( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_238 == 0 )
                     {
                        GXCCtl = "USUARIO_CODIGO_" + sGXsfl_30_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = dynUsuario_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( dynUsuario_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", ""))) ;
            ChangePostValue( edtUsuario_PessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ",", ""))) ;
            ChangePostValue( edtUsuario_PessoaNom_Internalname, StringUtil.RTrim( A58Usuario_PessoaNom)) ;
            ChangePostValue( dynFuncao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A621Funcao_Codigo), 6, 0, ".", ""))) ;
            ChangePostValue( edtFuncao_Nome_Internalname, A622Funcao_Nome) ;
            ChangePostValue( "ZT_"+"Z1Usuario_Codigo_"+sGXsfl_30_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1Usuario_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z621Funcao_Codigo_"+sGXsfl_30_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z621Funcao_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_238_"+sGXsfl_30_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_238), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_238_"+sGXsfl_30_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_238), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_238_"+sGXsfl_30_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_238), 4, 0, ",", ""))) ;
            if ( nIsMod_238 != 0 )
            {
               ChangePostValue( "USUARIO_CODIGO_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynUsuario_Codigo.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "USUARIO_PESSOACOD_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaCod_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "USUARIO_PESSOANOM_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaNom_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "FUNCAO_CODIGO_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynFuncao_Codigo.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "FUNCAO_NOME_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncao_Nome_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption5D0( )
      {
      }

      protected void E115D2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Equipe_AreaTrabalhoCodigo") == 0 )
               {
                  AV11Insert_Equipe_AreaTrabalhoCodigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Equipe_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Equipe_AreaTrabalhoCodigo), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtEquipe_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEquipe_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEquipe_Codigo_Visible), 5, 0)));
      }

      protected void E125D2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwequipe.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM5D237( short GX_JID )
      {
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2176Equipe_Nome = T005D8_A2176Equipe_Nome[0];
               Z2177Equipe_AreaTrabalhoCodigo = T005D8_A2177Equipe_AreaTrabalhoCodigo[0];
            }
            else
            {
               Z2176Equipe_Nome = A2176Equipe_Nome;
               Z2177Equipe_AreaTrabalhoCodigo = A2177Equipe_AreaTrabalhoCodigo;
            }
         }
         if ( GX_JID == -12 )
         {
            Z2175Equipe_Codigo = A2175Equipe_Codigo;
            Z2176Equipe_Nome = A2176Equipe_Nome;
            Z2177Equipe_AreaTrabalhoCodigo = A2177Equipe_AreaTrabalhoCodigo;
            Z2178Equipe_AreaTrabalhoDescricao = A2178Equipe_AreaTrabalhoDescricao;
         }
      }

      protected void standaloneNotModal( )
      {
         AV14Pgmname = "Equipe";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Equipe_Codigo) )
         {
            A2175Equipe_Codigo = AV7Equipe_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2175Equipe_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2175Equipe_Codigo), 6, 0)));
         }
         if ( ! (0==AV7Equipe_Codigo) )
         {
            edtEquipe_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEquipe_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEquipe_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtEquipe_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEquipe_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEquipe_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV7Equipe_Codigo) )
         {
            edtEquipe_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEquipe_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEquipe_Codigo_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Equipe_AreaTrabalhoCodigo) )
         {
            A2177Equipe_AreaTrabalhoCodigo = AV11Insert_Equipe_AreaTrabalhoCodigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2177Equipe_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2177Equipe_AreaTrabalhoCodigo), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A2177Equipe_AreaTrabalhoCodigo) && ( Gx_BScreen == 0 ) )
            {
               A2177Equipe_AreaTrabalhoCodigo = AV8WWPContext.gxTpr_Areatrabalho_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2177Equipe_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2177Equipe_AreaTrabalhoCodigo), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T005D9 */
            pr_default.execute(7, new Object[] {A2177Equipe_AreaTrabalhoCodigo});
            A2178Equipe_AreaTrabalhoDescricao = T005D9_A2178Equipe_AreaTrabalhoDescricao[0];
            n2178Equipe_AreaTrabalhoDescricao = T005D9_n2178Equipe_AreaTrabalhoDescricao[0];
            pr_default.close(7);
         }
      }

      protected void Load5D237( )
      {
         /* Using cursor T005D11 */
         pr_default.execute(9, new Object[] {A2175Equipe_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound237 = 1;
            A2176Equipe_Nome = T005D11_A2176Equipe_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2176Equipe_Nome", A2176Equipe_Nome);
            A2178Equipe_AreaTrabalhoDescricao = T005D11_A2178Equipe_AreaTrabalhoDescricao[0];
            n2178Equipe_AreaTrabalhoDescricao = T005D11_n2178Equipe_AreaTrabalhoDescricao[0];
            A2177Equipe_AreaTrabalhoCodigo = T005D11_A2177Equipe_AreaTrabalhoCodigo[0];
            ZM5D237( -12) ;
         }
         pr_default.close(9);
         OnLoadActions5D237( ) ;
      }

      protected void OnLoadActions5D237( )
      {
      }

      protected void CheckExtendedTable5D237( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T005D9 */
         pr_default.execute(7, new Object[] {A2177Equipe_AreaTrabalhoCodigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A2178Equipe_AreaTrabalhoDescricao = T005D9_A2178Equipe_AreaTrabalhoDescricao[0];
         n2178Equipe_AreaTrabalhoDescricao = T005D9_n2178Equipe_AreaTrabalhoDescricao[0];
         pr_default.close(7);
      }

      protected void CloseExtendedTableCursors5D237( )
      {
         pr_default.close(7);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_13( int A2177Equipe_AreaTrabalhoCodigo )
      {
         /* Using cursor T005D12 */
         pr_default.execute(10, new Object[] {A2177Equipe_AreaTrabalhoCodigo});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A2178Equipe_AreaTrabalhoDescricao = T005D12_A2178Equipe_AreaTrabalhoDescricao[0];
         n2178Equipe_AreaTrabalhoDescricao = T005D12_n2178Equipe_AreaTrabalhoDescricao[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A2178Equipe_AreaTrabalhoDescricao)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void GetKey5D237( )
      {
         /* Using cursor T005D13 */
         pr_default.execute(11, new Object[] {A2175Equipe_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound237 = 1;
         }
         else
         {
            RcdFound237 = 0;
         }
         pr_default.close(11);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T005D8 */
         pr_default.execute(6, new Object[] {A2175Equipe_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            ZM5D237( 12) ;
            RcdFound237 = 1;
            A2175Equipe_Codigo = T005D8_A2175Equipe_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2175Equipe_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2175Equipe_Codigo), 6, 0)));
            A2176Equipe_Nome = T005D8_A2176Equipe_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2176Equipe_Nome", A2176Equipe_Nome);
            A2177Equipe_AreaTrabalhoCodigo = T005D8_A2177Equipe_AreaTrabalhoCodigo[0];
            Z2175Equipe_Codigo = A2175Equipe_Codigo;
            sMode237 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load5D237( ) ;
            if ( AnyError == 1 )
            {
               RcdFound237 = 0;
               InitializeNonKey5D237( ) ;
            }
            Gx_mode = sMode237;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound237 = 0;
            InitializeNonKey5D237( ) ;
            sMode237 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode237;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(6);
      }

      protected void getEqualNoModal( )
      {
         GetKey5D237( ) ;
         if ( RcdFound237 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound237 = 0;
         /* Using cursor T005D14 */
         pr_default.execute(12, new Object[] {A2175Equipe_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T005D14_A2175Equipe_Codigo[0] < A2175Equipe_Codigo ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T005D14_A2175Equipe_Codigo[0] > A2175Equipe_Codigo ) ) )
            {
               A2175Equipe_Codigo = T005D14_A2175Equipe_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2175Equipe_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2175Equipe_Codigo), 6, 0)));
               RcdFound237 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void move_previous( )
      {
         RcdFound237 = 0;
         /* Using cursor T005D15 */
         pr_default.execute(13, new Object[] {A2175Equipe_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            while ( (pr_default.getStatus(13) != 101) && ( ( T005D15_A2175Equipe_Codigo[0] > A2175Equipe_Codigo ) ) )
            {
               pr_default.readNext(13);
            }
            if ( (pr_default.getStatus(13) != 101) && ( ( T005D15_A2175Equipe_Codigo[0] < A2175Equipe_Codigo ) ) )
            {
               A2175Equipe_Codigo = T005D15_A2175Equipe_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2175Equipe_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2175Equipe_Codigo), 6, 0)));
               RcdFound237 = 1;
            }
         }
         pr_default.close(13);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey5D237( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtEquipe_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert5D237( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound237 == 1 )
            {
               if ( A2175Equipe_Codigo != Z2175Equipe_Codigo )
               {
                  A2175Equipe_Codigo = Z2175Equipe_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2175Equipe_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2175Equipe_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "EQUIPE_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtEquipe_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtEquipe_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update5D237( ) ;
                  GX_FocusControl = edtEquipe_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A2175Equipe_Codigo != Z2175Equipe_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtEquipe_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert5D237( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "EQUIPE_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtEquipe_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtEquipe_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert5D237( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A2175Equipe_Codigo != Z2175Equipe_Codigo )
         {
            A2175Equipe_Codigo = Z2175Equipe_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2175Equipe_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2175Equipe_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "EQUIPE_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtEquipe_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtEquipe_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency5D237( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T005D7 */
            pr_default.execute(5, new Object[] {A2175Equipe_Codigo});
            if ( (pr_default.getStatus(5) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Equipe"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(5) == 101) || ( StringUtil.StrCmp(Z2176Equipe_Nome, T005D7_A2176Equipe_Nome[0]) != 0 ) || ( Z2177Equipe_AreaTrabalhoCodigo != T005D7_A2177Equipe_AreaTrabalhoCodigo[0] ) )
            {
               if ( StringUtil.StrCmp(Z2176Equipe_Nome, T005D7_A2176Equipe_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("equipe:[seudo value changed for attri]"+"Equipe_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z2176Equipe_Nome);
                  GXUtil.WriteLogRaw("Current: ",T005D7_A2176Equipe_Nome[0]);
               }
               if ( Z2177Equipe_AreaTrabalhoCodigo != T005D7_A2177Equipe_AreaTrabalhoCodigo[0] )
               {
                  GXUtil.WriteLog("equipe:[seudo value changed for attri]"+"Equipe_AreaTrabalhoCodigo");
                  GXUtil.WriteLogRaw("Old: ",Z2177Equipe_AreaTrabalhoCodigo);
                  GXUtil.WriteLogRaw("Current: ",T005D7_A2177Equipe_AreaTrabalhoCodigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Equipe"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert5D237( )
      {
         BeforeValidate5D237( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable5D237( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM5D237( 0) ;
            CheckOptimisticConcurrency5D237( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm5D237( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert5D237( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005D16 */
                     pr_default.execute(14, new Object[] {A2175Equipe_Codigo, A2176Equipe_Nome, A2177Equipe_AreaTrabalhoCodigo});
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("Equipe") ;
                     if ( (pr_default.getStatus(14) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel5D237( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                              ResetCaption5D0( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load5D237( ) ;
            }
            EndLevel5D237( ) ;
         }
         CloseExtendedTableCursors5D237( ) ;
      }

      protected void Update5D237( )
      {
         BeforeValidate5D237( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable5D237( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency5D237( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm5D237( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate5D237( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005D17 */
                     pr_default.execute(15, new Object[] {A2176Equipe_Nome, A2177Equipe_AreaTrabalhoCodigo, A2175Equipe_Codigo});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("Equipe") ;
                     if ( (pr_default.getStatus(15) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Equipe"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate5D237( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel5D237( ) ;
                           if ( AnyError == 0 )
                           {
                              if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                              {
                                 if ( AnyError == 0 )
                                 {
                                    context.nUserReturn = 1;
                                 }
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel5D237( ) ;
         }
         CloseExtendedTableCursors5D237( ) ;
      }

      protected void DeferredUpdate5D237( )
      {
      }

      protected void delete( )
      {
         BeforeValidate5D237( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency5D237( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls5D237( ) ;
            AfterConfirm5D237( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete5D237( ) ;
               if ( AnyError == 0 )
               {
                  ScanStart5D238( ) ;
                  while ( RcdFound238 != 0 )
                  {
                     getByPrimaryKey5D238( ) ;
                     Delete5D238( ) ;
                     ScanNext5D238( ) ;
                  }
                  ScanEnd5D238( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005D18 */
                     pr_default.execute(16, new Object[] {A2175Equipe_Codigo});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("Equipe") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode237 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel5D237( ) ;
         Gx_mode = sMode237;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls5D237( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T005D19 */
            pr_default.execute(17, new Object[] {A2177Equipe_AreaTrabalhoCodigo});
            A2178Equipe_AreaTrabalhoDescricao = T005D19_A2178Equipe_AreaTrabalhoDescricao[0];
            n2178Equipe_AreaTrabalhoDescricao = T005D19_n2178Equipe_AreaTrabalhoDescricao[0];
            pr_default.close(17);
         }
      }

      protected void ProcessNestedLevel5D238( )
      {
         nGXsfl_30_idx = 0;
         while ( nGXsfl_30_idx < nRC_GXsfl_30 )
         {
            ReadRow5D238( ) ;
            if ( ( nRcdExists_238 != 0 ) || ( nIsMod_238 != 0 ) )
            {
               standaloneNotModal5D238( ) ;
               GetKey5D238( ) ;
               if ( ( nRcdExists_238 == 0 ) && ( nRcdDeleted_238 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  Insert5D238( ) ;
               }
               else
               {
                  if ( RcdFound238 != 0 )
                  {
                     if ( ( nRcdDeleted_238 != 0 ) && ( nRcdExists_238 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        Delete5D238( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_238 != 0 ) && ( nRcdExists_238 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           Update5D238( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_238 == 0 )
                     {
                        GXCCtl = "USUARIO_CODIGO_" + sGXsfl_30_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = dynUsuario_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( dynUsuario_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", ""))) ;
            ChangePostValue( edtUsuario_PessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ",", ""))) ;
            ChangePostValue( edtUsuario_PessoaNom_Internalname, StringUtil.RTrim( A58Usuario_PessoaNom)) ;
            ChangePostValue( dynFuncao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A621Funcao_Codigo), 6, 0, ".", ""))) ;
            ChangePostValue( edtFuncao_Nome_Internalname, A622Funcao_Nome) ;
            ChangePostValue( "ZT_"+"Z1Usuario_Codigo_"+sGXsfl_30_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1Usuario_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z621Funcao_Codigo_"+sGXsfl_30_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z621Funcao_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_238_"+sGXsfl_30_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_238), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_238_"+sGXsfl_30_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_238), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_238_"+sGXsfl_30_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_238), 4, 0, ",", ""))) ;
            if ( nIsMod_238 != 0 )
            {
               ChangePostValue( "USUARIO_CODIGO_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynUsuario_Codigo.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "USUARIO_PESSOACOD_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaCod_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "USUARIO_PESSOANOM_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaNom_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "FUNCAO_CODIGO_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynFuncao_Codigo.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "FUNCAO_NOME_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncao_Nome_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll5D238( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_238 = 0;
         nIsMod_238 = 0;
         nRcdDeleted_238 = 0;
      }

      protected void ProcessLevel5D237( )
      {
         /* Save parent mode. */
         sMode237 = Gx_mode;
         ProcessNestedLevel5D238( ) ;
         if ( AnyError != 0 )
         {
         }
         /* Restore parent mode. */
         Gx_mode = sMode237;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         /* ' Update level parameters */
      }

      protected void EndLevel5D237( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(5);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete5D237( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(6);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(17);
            pr_default.close(2);
            pr_default.close(3);
            pr_default.close(4);
            context.CommitDataStores( "Equipe");
            if ( AnyError == 0 )
            {
               ConfirmValues5D0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(6);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(17);
            pr_default.close(2);
            pr_default.close(3);
            pr_default.close(4);
            context.RollbackDataStores( "Equipe");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart5D237( )
      {
         /* Scan By routine */
         /* Using cursor T005D20 */
         pr_default.execute(18);
         RcdFound237 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound237 = 1;
            A2175Equipe_Codigo = T005D20_A2175Equipe_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2175Equipe_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2175Equipe_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext5D237( )
      {
         /* Scan next routine */
         pr_default.readNext(18);
         RcdFound237 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound237 = 1;
            A2175Equipe_Codigo = T005D20_A2175Equipe_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2175Equipe_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2175Equipe_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd5D237( )
      {
         pr_default.close(18);
      }

      protected void AfterConfirm5D237( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert5D237( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate5D237( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete5D237( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete5D237( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate5D237( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes5D237( )
      {
         edtEquipe_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEquipe_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEquipe_Nome_Enabled), 5, 0)));
         edtEquipe_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEquipe_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEquipe_Codigo_Enabled), 5, 0)));
      }

      protected void ZM5D238( short GX_JID )
      {
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z621Funcao_Codigo = T005D3_A621Funcao_Codigo[0];
            }
            else
            {
               Z621Funcao_Codigo = A621Funcao_Codigo;
            }
         }
         if ( GX_JID == -14 )
         {
            Z2175Equipe_Codigo = A2175Equipe_Codigo;
            Z1Usuario_Codigo = A1Usuario_Codigo;
            Z621Funcao_Codigo = A621Funcao_Codigo;
            Z57Usuario_PessoaCod = A57Usuario_PessoaCod;
            Z58Usuario_PessoaNom = A58Usuario_PessoaNom;
            Z622Funcao_Nome = A622Funcao_Nome;
         }
      }

      protected void standaloneNotModal5D238( )
      {
         GXAUSUARIO_CODIGO_html5D238( ) ;
         edtUsuario_PessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaCod_Enabled), 5, 0)));
         edtUsuario_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaNom_Enabled), 5, 0)));
         edtFuncao_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncao_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncao_Nome_Enabled), 5, 0)));
      }

      protected void standaloneModal5D238( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            dynUsuario_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynUsuario_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynUsuario_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynUsuario_Codigo.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T005D4 */
            pr_default.execute(2, new Object[] {A1Usuario_Codigo});
            A57Usuario_PessoaCod = T005D4_A57Usuario_PessoaCod[0];
            pr_default.close(2);
            /* Using cursor T005D6 */
            pr_default.execute(4, new Object[] {A57Usuario_PessoaCod});
            A58Usuario_PessoaNom = T005D6_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = T005D6_n58Usuario_PessoaNom[0];
            pr_default.close(4);
         }
      }

      protected void Load5D238( )
      {
         /* Using cursor T005D21 */
         pr_default.execute(19, new Object[] {A2175Equipe_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound238 = 1;
            A58Usuario_PessoaNom = T005D21_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = T005D21_n58Usuario_PessoaNom[0];
            A622Funcao_Nome = T005D21_A622Funcao_Nome[0];
            A621Funcao_Codigo = T005D21_A621Funcao_Codigo[0];
            A57Usuario_PessoaCod = T005D21_A57Usuario_PessoaCod[0];
            ZM5D238( -14) ;
         }
         pr_default.close(19);
         OnLoadActions5D238( ) ;
      }

      protected void OnLoadActions5D238( )
      {
      }

      protected void CheckExtendedTable5D238( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal5D238( ) ;
         /* Using cursor T005D4 */
         pr_default.execute(2, new Object[] {A1Usuario_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GXCCtl = "USUARIO_CODIGO_" + sGXsfl_30_idx;
            GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = dynUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A57Usuario_PessoaCod = T005D4_A57Usuario_PessoaCod[0];
         pr_default.close(2);
         /* Using cursor T005D6 */
         pr_default.execute(4, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A58Usuario_PessoaNom = T005D6_A58Usuario_PessoaNom[0];
         n58Usuario_PessoaNom = T005D6_n58Usuario_PessoaNom[0];
         pr_default.close(4);
         /* Using cursor T005D5 */
         pr_default.execute(3, new Object[] {A621Funcao_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GXCCtl = "FUNCAO_CODIGO_" + sGXsfl_30_idx;
            GX_msglist.addItem("N�o existe 'Funcao'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = dynFuncao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A622Funcao_Nome = T005D5_A622Funcao_Nome[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors5D238( )
      {
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(3);
      }

      protected void enableDisable5D238( )
      {
      }

      protected void gxLoad_15( int A1Usuario_Codigo )
      {
         /* Using cursor T005D22 */
         pr_default.execute(20, new Object[] {A1Usuario_Codigo});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GXCCtl = "USUARIO_CODIGO_" + sGXsfl_30_idx;
            GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = dynUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A57Usuario_PessoaCod = T005D22_A57Usuario_PessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(20) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(20);
      }

      protected void gxLoad_17( int A57Usuario_PessoaCod )
      {
         /* Using cursor T005D23 */
         pr_default.execute(21, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A58Usuario_PessoaNom = T005D23_A58Usuario_PessoaNom[0];
         n58Usuario_PessoaNom = T005D23_n58Usuario_PessoaNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A58Usuario_PessoaNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(21) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(21);
      }

      protected void gxLoad_16( int A621Funcao_Codigo )
      {
         /* Using cursor T005D24 */
         pr_default.execute(22, new Object[] {A621Funcao_Codigo});
         if ( (pr_default.getStatus(22) == 101) )
         {
            GXCCtl = "FUNCAO_CODIGO_" + sGXsfl_30_idx;
            GX_msglist.addItem("N�o existe 'Funcao'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = dynFuncao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A622Funcao_Nome = T005D24_A622Funcao_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A622Funcao_Nome)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(22) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(22);
      }

      protected void GetKey5D238( )
      {
         /* Using cursor T005D25 */
         pr_default.execute(23, new Object[] {A2175Equipe_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(23) != 101) )
         {
            RcdFound238 = 1;
         }
         else
         {
            RcdFound238 = 0;
         }
         pr_default.close(23);
      }

      protected void getByPrimaryKey5D238( )
      {
         /* Using cursor T005D3 */
         pr_default.execute(1, new Object[] {A2175Equipe_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM5D238( 14) ;
            RcdFound238 = 1;
            InitializeNonKey5D238( ) ;
            A1Usuario_Codigo = T005D3_A1Usuario_Codigo[0];
            A621Funcao_Codigo = T005D3_A621Funcao_Codigo[0];
            Z2175Equipe_Codigo = A2175Equipe_Codigo;
            Z1Usuario_Codigo = A1Usuario_Codigo;
            sMode238 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load5D238( ) ;
            Gx_mode = sMode238;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound238 = 0;
            InitializeNonKey5D238( ) ;
            sMode238 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal5D238( ) ;
            Gx_mode = sMode238;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes5D238( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency5D238( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T005D2 */
            pr_default.execute(0, new Object[] {A2175Equipe_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"EquipeUsuario"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z621Funcao_Codigo != T005D2_A621Funcao_Codigo[0] ) )
            {
               if ( Z621Funcao_Codigo != T005D2_A621Funcao_Codigo[0] )
               {
                  GXUtil.WriteLog("equipe:[seudo value changed for attri]"+"Funcao_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z621Funcao_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T005D2_A621Funcao_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"EquipeUsuario"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert5D238( )
      {
         BeforeValidate5D238( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable5D238( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM5D238( 0) ;
            CheckOptimisticConcurrency5D238( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm5D238( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert5D238( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005D26 */
                     pr_default.execute(24, new Object[] {A2175Equipe_Codigo, A1Usuario_Codigo, A621Funcao_Codigo});
                     pr_default.close(24);
                     dsDefault.SmartCacheProvider.SetUpdated("EquipeUsuario") ;
                     if ( (pr_default.getStatus(24) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load5D238( ) ;
            }
            EndLevel5D238( ) ;
         }
         CloseExtendedTableCursors5D238( ) ;
      }

      protected void Update5D238( )
      {
         BeforeValidate5D238( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable5D238( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency5D238( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm5D238( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate5D238( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005D27 */
                     pr_default.execute(25, new Object[] {A621Funcao_Codigo, A2175Equipe_Codigo, A1Usuario_Codigo});
                     pr_default.close(25);
                     dsDefault.SmartCacheProvider.SetUpdated("EquipeUsuario") ;
                     if ( (pr_default.getStatus(25) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"EquipeUsuario"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate5D238( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey5D238( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel5D238( ) ;
         }
         CloseExtendedTableCursors5D238( ) ;
      }

      protected void DeferredUpdate5D238( )
      {
      }

      protected void Delete5D238( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         BeforeValidate5D238( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency5D238( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls5D238( ) ;
            AfterConfirm5D238( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete5D238( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T005D28 */
                  pr_default.execute(26, new Object[] {A2175Equipe_Codigo, A1Usuario_Codigo});
                  pr_default.close(26);
                  dsDefault.SmartCacheProvider.SetUpdated("EquipeUsuario") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode238 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel5D238( ) ;
         Gx_mode = sMode238;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls5D238( )
      {
         standaloneModal5D238( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T005D29 */
            pr_default.execute(27, new Object[] {A1Usuario_Codigo});
            A57Usuario_PessoaCod = T005D29_A57Usuario_PessoaCod[0];
            pr_default.close(27);
            /* Using cursor T005D30 */
            pr_default.execute(28, new Object[] {A57Usuario_PessoaCod});
            A58Usuario_PessoaNom = T005D30_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = T005D30_n58Usuario_PessoaNom[0];
            pr_default.close(28);
            /* Using cursor T005D31 */
            pr_default.execute(29, new Object[] {A621Funcao_Codigo});
            A622Funcao_Nome = T005D31_A622Funcao_Nome[0];
            pr_default.close(29);
         }
      }

      protected void EndLevel5D238( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart5D238( )
      {
         /* Scan By routine */
         /* Using cursor T005D32 */
         pr_default.execute(30, new Object[] {A2175Equipe_Codigo});
         RcdFound238 = 0;
         if ( (pr_default.getStatus(30) != 101) )
         {
            RcdFound238 = 1;
            A1Usuario_Codigo = T005D32_A1Usuario_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext5D238( )
      {
         /* Scan next routine */
         pr_default.readNext(30);
         RcdFound238 = 0;
         if ( (pr_default.getStatus(30) != 101) )
         {
            RcdFound238 = 1;
            A1Usuario_Codigo = T005D32_A1Usuario_Codigo[0];
         }
      }

      protected void ScanEnd5D238( )
      {
         pr_default.close(30);
      }

      protected void AfterConfirm5D238( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert5D238( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate5D238( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete5D238( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete5D238( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate5D238( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes5D238( )
      {
         dynUsuario_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynUsuario_Codigo.Enabled), 5, 0)));
         edtUsuario_PessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaCod_Enabled), 5, 0)));
         edtUsuario_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaNom_Enabled), 5, 0)));
         dynFuncao_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynFuncao_Codigo.Enabled), 5, 0)));
         edtFuncao_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncao_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncao_Nome_Enabled), 5, 0)));
      }

      protected void SubsflControlProps_30238( )
      {
         dynUsuario_Codigo_Internalname = "USUARIO_CODIGO_"+sGXsfl_30_idx;
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD_"+sGXsfl_30_idx;
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM_"+sGXsfl_30_idx;
         dynFuncao_Codigo_Internalname = "FUNCAO_CODIGO_"+sGXsfl_30_idx;
         edtFuncao_Nome_Internalname = "FUNCAO_NOME_"+sGXsfl_30_idx;
      }

      protected void SubsflControlProps_fel_30238( )
      {
         dynUsuario_Codigo_Internalname = "USUARIO_CODIGO_"+sGXsfl_30_fel_idx;
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD_"+sGXsfl_30_fel_idx;
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM_"+sGXsfl_30_fel_idx;
         dynFuncao_Codigo_Internalname = "FUNCAO_CODIGO_"+sGXsfl_30_fel_idx;
         edtFuncao_Nome_Internalname = "FUNCAO_NOME_"+sGXsfl_30_fel_idx;
      }

      protected void AddRow5D238( )
      {
         nGXsfl_30_idx = (short)(nGXsfl_30_idx+1);
         sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
         SubsflControlProps_30238( ) ;
         SendRow5D238( ) ;
      }

      protected void SendRow5D238( )
      {
         Gridlevel_usuarioRow = GXWebRow.GetNew(context);
         if ( subGridlevel_usuario_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridlevel_usuario_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridlevel_usuario_Class, "") != 0 )
            {
               subGridlevel_usuario_Linesclass = subGridlevel_usuario_Class+"Odd";
            }
         }
         else if ( subGridlevel_usuario_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridlevel_usuario_Backstyle = 0;
            subGridlevel_usuario_Backcolor = subGridlevel_usuario_Allbackcolor;
            if ( StringUtil.StrCmp(subGridlevel_usuario_Class, "") != 0 )
            {
               subGridlevel_usuario_Linesclass = subGridlevel_usuario_Class+"Uniform";
            }
         }
         else if ( subGridlevel_usuario_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridlevel_usuario_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridlevel_usuario_Class, "") != 0 )
            {
               subGridlevel_usuario_Linesclass = subGridlevel_usuario_Class+"Odd";
            }
            subGridlevel_usuario_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGridlevel_usuario_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridlevel_usuario_Backstyle = 1;
            if ( ((int)((nGXsfl_30_idx) % (2))) == 0 )
            {
               subGridlevel_usuario_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridlevel_usuario_Class, "") != 0 )
               {
                  subGridlevel_usuario_Linesclass = subGridlevel_usuario_Class+"Even";
               }
            }
            else
            {
               subGridlevel_usuario_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridlevel_usuario_Class, "") != 0 )
               {
                  subGridlevel_usuario_Linesclass = subGridlevel_usuario_Class+"Odd";
               }
            }
         }
         GXAUSUARIO_CODIGO_html5D238( ) ;
         /* Subfile cell */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_238_" + sGXsfl_30_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_30_idx + "',30)\"";
         GXCCtl = "USUARIO_CODIGO_" + sGXsfl_30_idx;
         dynUsuario_Codigo.Name = GXCCtl;
         dynUsuario_Codigo.WebTags = "";
         /* ComboBox */
         Gridlevel_usuarioRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynUsuario_Codigo,(String)dynUsuario_Codigo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)),(short)1,(String)dynUsuario_Codigo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,dynUsuario_Codigo.Enabled,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"",(String)"",(bool)true});
         dynUsuario_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUsuario_Codigo_Internalname, "Values", (String)(dynUsuario_Codigo.ToJavascriptSource()));
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Gridlevel_usuarioRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ",", "")),((edtUsuario_PessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtUsuario_PessoaCod_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Gridlevel_usuarioRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_PessoaNom_Internalname,StringUtil.RTrim( A58Usuario_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A58Usuario_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtUsuario_PessoaNom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
         /* Subfile cell */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_238_" + sGXsfl_30_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_30_idx + "',30)\"";
         if ( ( nGXsfl_30_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "FUNCAO_CODIGO_" + sGXsfl_30_idx;
            dynFuncao_Codigo.Name = GXCCtl;
            dynFuncao_Codigo.WebTags = "";
            dynFuncao_Codigo.removeAllItems();
            /* Using cursor T005D33 */
            pr_default.execute(31);
            while ( (pr_default.getStatus(31) != 101) )
            {
               dynFuncao_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T005D33_A621Funcao_Codigo[0]), 6, 0)), T005D33_A622Funcao_Nome[0], 0);
               pr_default.readNext(31);
            }
            pr_default.close(31);
            if ( dynFuncao_Codigo.ItemCount > 0 )
            {
               A621Funcao_Codigo = (int)(NumberUtil.Val( dynFuncao_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0))), "."));
            }
         }
         /* ComboBox */
         Gridlevel_usuarioRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynFuncao_Codigo,(String)dynFuncao_Codigo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0)),(short)1,(String)dynFuncao_Codigo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,dynFuncao_Codigo.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"",(String)"",(bool)true});
         dynFuncao_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncao_Codigo_Internalname, "Values", (String)(dynFuncao_Codigo.ToJavascriptSource()));
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Gridlevel_usuarioRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncao_Nome_Internalname,(String)A622Funcao_Nome,StringUtil.RTrim( context.localUtil.Format( A622Funcao_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncao_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtFuncao_Nome_Enabled,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         context.httpAjaxContext.ajax_sending_grid_row(Gridlevel_usuarioRow);
         GXCCtl = "Z1Usuario_Codigo_" + sGXsfl_30_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1Usuario_Codigo), 6, 0, ",", "")));
         GXCCtl = "Z621Funcao_Codigo_" + sGXsfl_30_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z621Funcao_Codigo), 6, 0, ",", "")));
         GXCCtl = "nRcdDeleted_238_" + sGXsfl_30_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_238), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_238_" + sGXsfl_30_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_238), 4, 0, ",", "")));
         GXCCtl = "nIsMod_238_" + sGXsfl_30_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_238), 4, 0, ",", "")));
         GXCCtl = "vMODE_" + sGXsfl_30_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Gx_mode));
         GXCCtl = "vTRNCONTEXT_" + sGXsfl_30_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV9TrnContext);
         }
         GXCCtl = "vEQUIPE_CODIGO_" + sGXsfl_30_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Equipe_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynUsuario_Codigo.Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOACOD_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaCod_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOANOM_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaNom_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAO_CODIGO_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynFuncao_Codigo.Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAO_NOME_"+sGXsfl_30_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncao_Nome_Enabled), 5, 0, ".", "")));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridlevel_usuarioContainer.AddRow(Gridlevel_usuarioRow);
      }

      protected void ReadRow5D238( )
      {
         nGXsfl_30_idx = (short)(nGXsfl_30_idx+1);
         sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
         SubsflControlProps_30238( ) ;
         dynUsuario_Codigo.Enabled = (int)(context.localUtil.CToN( cgiGet( "USUARIO_CODIGO_"+sGXsfl_30_idx+"Enabled"), ",", "."));
         edtUsuario_PessoaCod_Enabled = (int)(context.localUtil.CToN( cgiGet( "USUARIO_PESSOACOD_"+sGXsfl_30_idx+"Enabled"), ",", "."));
         edtUsuario_PessoaNom_Enabled = (int)(context.localUtil.CToN( cgiGet( "USUARIO_PESSOANOM_"+sGXsfl_30_idx+"Enabled"), ",", "."));
         dynFuncao_Codigo.Enabled = (int)(context.localUtil.CToN( cgiGet( "FUNCAO_CODIGO_"+sGXsfl_30_idx+"Enabled"), ",", "."));
         edtFuncao_Nome_Enabled = (int)(context.localUtil.CToN( cgiGet( "FUNCAO_NOME_"+sGXsfl_30_idx+"Enabled"), ",", "."));
         dynUsuario_Codigo.Name = dynUsuario_Codigo_Internalname;
         dynUsuario_Codigo.CurrentValue = cgiGet( dynUsuario_Codigo_Internalname);
         A1Usuario_Codigo = (int)(NumberUtil.Val( cgiGet( dynUsuario_Codigo_Internalname), "."));
         A57Usuario_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtUsuario_PessoaCod_Internalname), ",", "."));
         A58Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtUsuario_PessoaNom_Internalname));
         n58Usuario_PessoaNom = false;
         dynFuncao_Codigo.Name = dynFuncao_Codigo_Internalname;
         dynFuncao_Codigo.CurrentValue = cgiGet( dynFuncao_Codigo_Internalname);
         A621Funcao_Codigo = (int)(NumberUtil.Val( cgiGet( dynFuncao_Codigo_Internalname), "."));
         A622Funcao_Nome = StringUtil.Upper( cgiGet( edtFuncao_Nome_Internalname));
         GXCCtl = "Z1Usuario_Codigo_" + sGXsfl_30_idx;
         Z1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z621Funcao_Codigo_" + sGXsfl_30_idx;
         Z621Funcao_Codigo = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdDeleted_238_" + sGXsfl_30_idx;
         nRcdDeleted_238 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_238_" + sGXsfl_30_idx;
         nRcdExists_238 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_238_" + sGXsfl_30_idx;
         nIsMod_238 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
      }

      protected void assign_properties_default( )
      {
         defedtFuncao_Nome_Enabled = edtFuncao_Nome_Enabled;
         defedtUsuario_PessoaNom_Enabled = edtUsuario_PessoaNom_Enabled;
         defedtUsuario_PessoaCod_Enabled = edtUsuario_PessoaCod_Enabled;
         defdynUsuario_Codigo_Enabled = dynUsuario_Codigo.Enabled;
      }

      protected void ConfirmValues5D0( )
      {
         nGXsfl_30_idx = 0;
         sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
         SubsflControlProps_30238( ) ;
         while ( nGXsfl_30_idx < nRC_GXsfl_30 )
         {
            nGXsfl_30_idx = (short)(nGXsfl_30_idx+1);
            sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
            SubsflControlProps_30238( ) ;
            ChangePostValue( "Z1Usuario_Codigo_"+sGXsfl_30_idx, cgiGet( "ZT_"+"Z1Usuario_Codigo_"+sGXsfl_30_idx)) ;
            DeletePostValue( "ZT_"+"Z1Usuario_Codigo_"+sGXsfl_30_idx) ;
            ChangePostValue( "Z621Funcao_Codigo_"+sGXsfl_30_idx, cgiGet( "ZT_"+"Z621Funcao_Codigo_"+sGXsfl_30_idx)) ;
            DeletePostValue( "ZT_"+"Z621Funcao_Codigo_"+sGXsfl_30_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299324729");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("equipe.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Equipe_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z2175Equipe_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2175Equipe_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2176Equipe_Nome", StringUtil.RTrim( Z2176Equipe_Nome));
         GxWebStd.gx_hidden_field( context, "Z2177Equipe_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2177Equipe_AreaTrabalhoCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_30", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_30_idx), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N2177Equipe_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2177Equipe_AreaTrabalhoCodigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vEQUIPE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Equipe_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_EQUIPE_AREATRABALHOCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Equipe_AreaTrabalhoCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "EQUIPE_AREATRABALHOCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2177Equipe_AreaTrabalhoCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "EQUIPE_AREATRABALHODESCRICAO", A2178Equipe_AreaTrabalhoDescricao);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vEQUIPE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Equipe_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Equipe";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2177Equipe_AreaTrabalhoCodigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("equipe:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("equipe:[SendSecurityCheck value for]"+"Equipe_AreaTrabalhoCodigo:"+context.localUtil.Format( (decimal)(A2177Equipe_AreaTrabalhoCodigo), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("equipe.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Equipe_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Equipe" ;
      }

      public override String GetPgmdesc( )
      {
         return "Equipe" ;
      }

      protected void InitializeNonKey5D237( )
      {
         A2176Equipe_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2176Equipe_Nome", A2176Equipe_Nome);
         A2178Equipe_AreaTrabalhoDescricao = "";
         n2178Equipe_AreaTrabalhoDescricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2178Equipe_AreaTrabalhoDescricao", A2178Equipe_AreaTrabalhoDescricao);
         A2177Equipe_AreaTrabalhoCodigo = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2177Equipe_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2177Equipe_AreaTrabalhoCodigo), 6, 0)));
         Z2176Equipe_Nome = "";
         Z2177Equipe_AreaTrabalhoCodigo = 0;
      }

      protected void InitAll5D237( )
      {
         A2175Equipe_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2175Equipe_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2175Equipe_Codigo), 6, 0)));
         InitializeNonKey5D237( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2177Equipe_AreaTrabalhoCodigo = i2177Equipe_AreaTrabalhoCodigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2177Equipe_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2177Equipe_AreaTrabalhoCodigo), 6, 0)));
      }

      protected void InitializeNonKey5D238( )
      {
         A57Usuario_PessoaCod = 0;
         A58Usuario_PessoaNom = "";
         n58Usuario_PessoaNom = false;
         A621Funcao_Codigo = 0;
         A622Funcao_Nome = "";
         Z621Funcao_Codigo = 0;
      }

      protected void InitAll5D238( )
      {
         A1Usuario_Codigo = 0;
         InitializeNonKey5D238( ) ;
      }

      protected void StandaloneModalInsert5D238( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299324749");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("equipe.js", "?20205299324749");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_level_properties238( )
      {
         edtFuncao_Nome_Enabled = defedtFuncao_Nome_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncao_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncao_Nome_Enabled), 5, 0)));
         edtUsuario_PessoaNom_Enabled = defedtUsuario_PessoaNom_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaNom_Enabled), 5, 0)));
         edtUsuario_PessoaCod_Enabled = defedtUsuario_PessoaCod_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaCod_Enabled), 5, 0)));
         dynUsuario_Codigo.Enabled = defdynUsuario_Codigo_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynUsuario_Codigo.Enabled), 5, 0)));
      }

      protected void init_default_properties( )
      {
         lblEquipetitle_Internalname = "EQUIPETITLE";
         lblTextblockequipe_nome_Internalname = "TEXTBLOCKEQUIPE_NOME";
         edtEquipe_Nome_Internalname = "EQUIPE_NOME";
         tblTable1_Internalname = "TABLE1";
         dynUsuario_Codigo_Internalname = "USUARIO_CODIGO";
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD";
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM";
         dynFuncao_Codigo_Internalname = "FUNCAO_CODIGO";
         edtFuncao_Nome_Internalname = "FUNCAO_NOME";
         tblTableleaflevel_usuario_Internalname = "TABLELEAFLEVEL_USUARIO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtEquipe_Codigo_Internalname = "EQUIPE_CODIGO";
         Form.Internalname = "FORM";
         subGridlevel_usuario_Internalname = "GRIDLEVEL_USUARIO";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Informa��es Gerais";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Equipe";
         edtFuncao_Nome_Jsonclick = "";
         dynFuncao_Codigo_Jsonclick = "";
         edtUsuario_PessoaNom_Jsonclick = "";
         edtUsuario_PessoaCod_Jsonclick = "";
         dynUsuario_Codigo_Jsonclick = "";
         subGridlevel_usuario_Class = "WorkWithBorder WorkWith";
         edtEquipe_Nome_Jsonclick = "";
         edtEquipe_Nome_Enabled = 1;
         subGridlevel_usuario_Allowcollapsing = 0;
         subGridlevel_usuario_Allowselection = 0;
         edtFuncao_Nome_Enabled = 0;
         dynFuncao_Codigo.Enabled = 1;
         edtUsuario_PessoaNom_Enabled = 0;
         edtUsuario_PessoaCod_Enabled = 0;
         dynUsuario_Codigo.Enabled = 1;
         subGridlevel_usuario_Backcolorstyle = 3;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtEquipe_Codigo_Jsonclick = "";
         edtEquipe_Codigo_Enabled = 1;
         edtEquipe_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAUSUARIO_CODIGO5D238( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAUSUARIO_CODIGO_data5D238( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAUSUARIO_CODIGO_html5D238( )
      {
         int gxdynajaxvalue ;
         GXDLAUSUARIO_CODIGO_data5D238( ) ;
         gxdynajaxindex = 1;
         dynUsuario_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynUsuario_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAUSUARIO_CODIGO_data5D238( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T005D34 */
         pr_default.execute(32);
         while ( (pr_default.getStatus(32) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T005D34_A1Usuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T005D34_A58Usuario_PessoaNom[0]));
            pr_default.readNext(32);
         }
         pr_default.close(32);
      }

      protected void gxnrGridlevel_usuario_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         SubsflControlProps_30238( ) ;
         while ( nGXsfl_30_idx <= nRC_GXsfl_30 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal5D238( ) ;
            standaloneModal5D238( ) ;
            GXCCtl = "USUARIO_CODIGO_" + sGXsfl_30_idx;
            dynUsuario_Codigo.Name = GXCCtl;
            dynUsuario_Codigo.WebTags = "";
            GXCCtl = "FUNCAO_CODIGO_" + sGXsfl_30_idx;
            dynFuncao_Codigo.Name = GXCCtl;
            dynFuncao_Codigo.WebTags = "";
            dynFuncao_Codigo.removeAllItems();
            /* Using cursor T005D35 */
            pr_default.execute(33);
            while ( (pr_default.getStatus(33) != 101) )
            {
               dynFuncao_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T005D35_A621Funcao_Codigo[0]), 6, 0)), T005D35_A622Funcao_Nome[0], 0);
               pr_default.readNext(33);
            }
            pr_default.close(33);
            if ( dynFuncao_Codigo.ItemCount > 0 )
            {
               A621Funcao_Codigo = (int)(NumberUtil.Val( dynFuncao_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A621Funcao_Codigo), 6, 0))), "."));
            }
            dynload_actions( ) ;
            SendRow5D238( ) ;
            nGXsfl_30_idx = (short)(nGXsfl_30_idx+1);
            sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
            SubsflControlProps_30238( ) ;
         }
         context.GX_webresponse.AddString(Gridlevel_usuarioContainer.ToJavascriptSource());
         /* End function gxnrGridlevel_usuario_newrow */
      }

      public void Valid_Usuario_codigo( GXCombobox dynGX_Parm1 ,
                                        int GX_Parm2 ,
                                        String GX_Parm3 )
      {
         dynUsuario_Codigo = dynGX_Parm1;
         A1Usuario_Codigo = (int)(NumberUtil.Val( dynUsuario_Codigo.CurrentValue, "."));
         A57Usuario_PessoaCod = GX_Parm2;
         A58Usuario_PessoaNom = GX_Parm3;
         n58Usuario_PessoaNom = false;
         /* Using cursor T005D29 */
         pr_default.execute(27, new Object[] {A1Usuario_Codigo});
         if ( (pr_default.getStatus(27) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynUsuario_Codigo_Internalname;
         }
         A57Usuario_PessoaCod = T005D29_A57Usuario_PessoaCod[0];
         pr_default.close(27);
         /* Using cursor T005D30 */
         pr_default.execute(28, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(28) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A58Usuario_PessoaNom = T005D30_A58Usuario_PessoaNom[0];
         n58Usuario_PessoaNom = T005D30_n58Usuario_PessoaNom[0];
         pr_default.close(28);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A57Usuario_PessoaCod = 0;
            A58Usuario_PessoaNom = "";
            n58Usuario_PessoaNom = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A58Usuario_PessoaNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Funcao_codigo( GXCombobox dynGX_Parm1 ,
                                       String GX_Parm2 )
      {
         dynFuncao_Codigo = dynGX_Parm1;
         A621Funcao_Codigo = (int)(NumberUtil.Val( dynFuncao_Codigo.CurrentValue, "."));
         A622Funcao_Nome = GX_Parm2;
         /* Using cursor T005D31 */
         pr_default.execute(29, new Object[] {A621Funcao_Codigo});
         if ( (pr_default.getStatus(29) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao'.", "ForeignKeyNotFound", 1, "FUNCAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynFuncao_Codigo_Internalname;
         }
         A622Funcao_Nome = T005D31_A622Funcao_Nome[0];
         pr_default.close(29);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A622Funcao_Nome = "";
         }
         isValidOutput.Add(A622Funcao_Nome);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Equipe_Codigo',fld:'vEQUIPE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E125D2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(27);
         pr_default.close(29);
         pr_default.close(28);
         pr_default.close(6);
         pr_default.close(17);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z2176Equipe_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         GXCCtl = "";
         T005D10_A621Funcao_Codigo = new int[1] ;
         T005D10_A622Funcao_Nome = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         lblEquipetitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         Gridlevel_usuarioContainer = new GXWebGrid( context);
         Gridlevel_usuarioColumn = new GXWebColumn();
         A58Usuario_PessoaNom = "";
         A622Funcao_Nome = "";
         sMode238 = "";
         lblTextblockequipe_nome_Jsonclick = "";
         A2176Equipe_Nome = "";
         A2178Equipe_AreaTrabalhoDescricao = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode237 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z2178Equipe_AreaTrabalhoDescricao = "";
         T005D9_A2178Equipe_AreaTrabalhoDescricao = new String[] {""} ;
         T005D9_n2178Equipe_AreaTrabalhoDescricao = new bool[] {false} ;
         T005D11_A2175Equipe_Codigo = new int[1] ;
         T005D11_A2176Equipe_Nome = new String[] {""} ;
         T005D11_A2178Equipe_AreaTrabalhoDescricao = new String[] {""} ;
         T005D11_n2178Equipe_AreaTrabalhoDescricao = new bool[] {false} ;
         T005D11_A2177Equipe_AreaTrabalhoCodigo = new int[1] ;
         T005D12_A2178Equipe_AreaTrabalhoDescricao = new String[] {""} ;
         T005D12_n2178Equipe_AreaTrabalhoDescricao = new bool[] {false} ;
         T005D13_A2175Equipe_Codigo = new int[1] ;
         T005D8_A2175Equipe_Codigo = new int[1] ;
         T005D8_A2176Equipe_Nome = new String[] {""} ;
         T005D8_A2177Equipe_AreaTrabalhoCodigo = new int[1] ;
         T005D14_A2175Equipe_Codigo = new int[1] ;
         T005D15_A2175Equipe_Codigo = new int[1] ;
         T005D7_A2175Equipe_Codigo = new int[1] ;
         T005D7_A2176Equipe_Nome = new String[] {""} ;
         T005D7_A2177Equipe_AreaTrabalhoCodigo = new int[1] ;
         T005D19_A2178Equipe_AreaTrabalhoDescricao = new String[] {""} ;
         T005D19_n2178Equipe_AreaTrabalhoDescricao = new bool[] {false} ;
         T005D20_A2175Equipe_Codigo = new int[1] ;
         Z58Usuario_PessoaNom = "";
         Z622Funcao_Nome = "";
         T005D4_A57Usuario_PessoaCod = new int[1] ;
         T005D6_A58Usuario_PessoaNom = new String[] {""} ;
         T005D6_n58Usuario_PessoaNom = new bool[] {false} ;
         T005D21_A2175Equipe_Codigo = new int[1] ;
         T005D21_A58Usuario_PessoaNom = new String[] {""} ;
         T005D21_n58Usuario_PessoaNom = new bool[] {false} ;
         T005D21_A622Funcao_Nome = new String[] {""} ;
         T005D21_A1Usuario_Codigo = new int[1] ;
         T005D21_A621Funcao_Codigo = new int[1] ;
         T005D21_A57Usuario_PessoaCod = new int[1] ;
         T005D5_A622Funcao_Nome = new String[] {""} ;
         T005D22_A57Usuario_PessoaCod = new int[1] ;
         T005D23_A58Usuario_PessoaNom = new String[] {""} ;
         T005D23_n58Usuario_PessoaNom = new bool[] {false} ;
         T005D24_A622Funcao_Nome = new String[] {""} ;
         T005D25_A2175Equipe_Codigo = new int[1] ;
         T005D25_A1Usuario_Codigo = new int[1] ;
         T005D3_A2175Equipe_Codigo = new int[1] ;
         T005D3_A1Usuario_Codigo = new int[1] ;
         T005D3_A621Funcao_Codigo = new int[1] ;
         T005D2_A2175Equipe_Codigo = new int[1] ;
         T005D2_A1Usuario_Codigo = new int[1] ;
         T005D2_A621Funcao_Codigo = new int[1] ;
         T005D29_A57Usuario_PessoaCod = new int[1] ;
         T005D30_A58Usuario_PessoaNom = new String[] {""} ;
         T005D30_n58Usuario_PessoaNom = new bool[] {false} ;
         T005D31_A622Funcao_Nome = new String[] {""} ;
         T005D32_A2175Equipe_Codigo = new int[1] ;
         T005D32_A1Usuario_Codigo = new int[1] ;
         Gridlevel_usuarioRow = new GXWebRow();
         subGridlevel_usuario_Linesclass = "";
         ROClassString = "";
         T005D33_A621Funcao_Codigo = new int[1] ;
         T005D33_A622Funcao_Nome = new String[] {""} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T005D34_A57Usuario_PessoaCod = new int[1] ;
         T005D34_A1Usuario_Codigo = new int[1] ;
         T005D34_A58Usuario_PessoaNom = new String[] {""} ;
         T005D34_n58Usuario_PessoaNom = new bool[] {false} ;
         T005D35_A621Funcao_Codigo = new int[1] ;
         T005D35_A622Funcao_Nome = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.equipe__default(),
            new Object[][] {
                new Object[] {
               T005D2_A2175Equipe_Codigo, T005D2_A1Usuario_Codigo, T005D2_A621Funcao_Codigo
               }
               , new Object[] {
               T005D3_A2175Equipe_Codigo, T005D3_A1Usuario_Codigo, T005D3_A621Funcao_Codigo
               }
               , new Object[] {
               T005D4_A57Usuario_PessoaCod
               }
               , new Object[] {
               T005D5_A622Funcao_Nome
               }
               , new Object[] {
               T005D6_A58Usuario_PessoaNom, T005D6_n58Usuario_PessoaNom
               }
               , new Object[] {
               T005D7_A2175Equipe_Codigo, T005D7_A2176Equipe_Nome, T005D7_A2177Equipe_AreaTrabalhoCodigo
               }
               , new Object[] {
               T005D8_A2175Equipe_Codigo, T005D8_A2176Equipe_Nome, T005D8_A2177Equipe_AreaTrabalhoCodigo
               }
               , new Object[] {
               T005D9_A2178Equipe_AreaTrabalhoDescricao, T005D9_n2178Equipe_AreaTrabalhoDescricao
               }
               , new Object[] {
               T005D10_A621Funcao_Codigo, T005D10_A622Funcao_Nome
               }
               , new Object[] {
               T005D11_A2175Equipe_Codigo, T005D11_A2176Equipe_Nome, T005D11_A2178Equipe_AreaTrabalhoDescricao, T005D11_n2178Equipe_AreaTrabalhoDescricao, T005D11_A2177Equipe_AreaTrabalhoCodigo
               }
               , new Object[] {
               T005D12_A2178Equipe_AreaTrabalhoDescricao, T005D12_n2178Equipe_AreaTrabalhoDescricao
               }
               , new Object[] {
               T005D13_A2175Equipe_Codigo
               }
               , new Object[] {
               T005D14_A2175Equipe_Codigo
               }
               , new Object[] {
               T005D15_A2175Equipe_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T005D19_A2178Equipe_AreaTrabalhoDescricao, T005D19_n2178Equipe_AreaTrabalhoDescricao
               }
               , new Object[] {
               T005D20_A2175Equipe_Codigo
               }
               , new Object[] {
               T005D21_A2175Equipe_Codigo, T005D21_A58Usuario_PessoaNom, T005D21_n58Usuario_PessoaNom, T005D21_A622Funcao_Nome, T005D21_A1Usuario_Codigo, T005D21_A621Funcao_Codigo, T005D21_A57Usuario_PessoaCod
               }
               , new Object[] {
               T005D22_A57Usuario_PessoaCod
               }
               , new Object[] {
               T005D23_A58Usuario_PessoaNom, T005D23_n58Usuario_PessoaNom
               }
               , new Object[] {
               T005D24_A622Funcao_Nome
               }
               , new Object[] {
               T005D25_A2175Equipe_Codigo, T005D25_A1Usuario_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T005D29_A57Usuario_PessoaCod
               }
               , new Object[] {
               T005D30_A58Usuario_PessoaNom, T005D30_n58Usuario_PessoaNom
               }
               , new Object[] {
               T005D31_A622Funcao_Nome
               }
               , new Object[] {
               T005D32_A2175Equipe_Codigo, T005D32_A1Usuario_Codigo
               }
               , new Object[] {
               T005D33_A621Funcao_Codigo, T005D33_A622Funcao_Nome
               }
               , new Object[] {
               T005D34_A57Usuario_PessoaCod, T005D34_A1Usuario_Codigo, T005D34_A58Usuario_PessoaNom, T005D34_n58Usuario_PessoaNom
               }
               , new Object[] {
               T005D35_A621Funcao_Codigo, T005D35_A622Funcao_Nome
               }
            }
         );
         AV14Pgmname = "Equipe";
         Z2177Equipe_AreaTrabalhoCodigo = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         N2177Equipe_AreaTrabalhoCodigo = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         i2177Equipe_AreaTrabalhoCodigo = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A2177Equipe_AreaTrabalhoCodigo = AV8WWPContext.gxTpr_Areatrabalho_codigo;
      }

      private short nRC_GXsfl_30 ;
      private short nGXsfl_30_idx=1 ;
      private short nRcdDeleted_238 ;
      private short nRcdExists_238 ;
      private short nIsMod_238 ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short subGridlevel_usuario_Backcolorstyle ;
      private short subGridlevel_usuario_Allowselection ;
      private short subGridlevel_usuario_Allowhovering ;
      private short subGridlevel_usuario_Allowcollapsing ;
      private short subGridlevel_usuario_Collapsed ;
      private short nBlankRcdCount238 ;
      private short RcdFound238 ;
      private short nBlankRcdUsr238 ;
      private short Gx_BScreen ;
      private short RcdFound237 ;
      private short GX_JID ;
      private short subGridlevel_usuario_Backstyle ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Equipe_Codigo ;
      private int Z2175Equipe_Codigo ;
      private int Z2177Equipe_AreaTrabalhoCodigo ;
      private int N2177Equipe_AreaTrabalhoCodigo ;
      private int Z1Usuario_Codigo ;
      private int Z621Funcao_Codigo ;
      private int A2177Equipe_AreaTrabalhoCodigo ;
      private int A1Usuario_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int A621Funcao_Codigo ;
      private int AV7Equipe_Codigo ;
      private int trnEnded ;
      private int A2175Equipe_Codigo ;
      private int edtEquipe_Codigo_Visible ;
      private int edtEquipe_Codigo_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtUsuario_PessoaCod_Enabled ;
      private int edtUsuario_PessoaNom_Enabled ;
      private int edtFuncao_Nome_Enabled ;
      private int subGridlevel_usuario_Selectioncolor ;
      private int subGridlevel_usuario_Hoveringcolor ;
      private int fRowAdded ;
      private int edtEquipe_Nome_Enabled ;
      private int AV11Insert_Equipe_AreaTrabalhoCodigo ;
      private int AV15GXV1 ;
      private int Z57Usuario_PessoaCod ;
      private int subGridlevel_usuario_Backcolor ;
      private int subGridlevel_usuario_Allbackcolor ;
      private int defedtFuncao_Nome_Enabled ;
      private int defedtUsuario_PessoaNom_Enabled ;
      private int defedtUsuario_PessoaCod_Enabled ;
      private int defdynUsuario_Codigo_Enabled ;
      private int i2177Equipe_AreaTrabalhoCodigo ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private long GRIDLEVEL_USUARIO_nFirstRecordOnPage ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z2176Equipe_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_30_idx="0001" ;
      private String Gx_mode ;
      private String GXKey ;
      private String GXCCtl ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtEquipe_Nome_Internalname ;
      private String TempTags ;
      private String edtEquipe_Codigo_Internalname ;
      private String edtEquipe_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblEquipetitle_Internalname ;
      private String lblEquipetitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String tblTableleaflevel_usuario_Internalname ;
      private String A58Usuario_PessoaNom ;
      private String sMode238 ;
      private String dynUsuario_Codigo_Internalname ;
      private String edtUsuario_PessoaCod_Internalname ;
      private String edtUsuario_PessoaNom_Internalname ;
      private String dynFuncao_Codigo_Internalname ;
      private String edtFuncao_Nome_Internalname ;
      private String tblTable1_Internalname ;
      private String lblTextblockequipe_nome_Internalname ;
      private String lblTextblockequipe_nome_Jsonclick ;
      private String A2176Equipe_Nome ;
      private String edtEquipe_Nome_Jsonclick ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode237 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z58Usuario_PessoaNom ;
      private String sGXsfl_30_fel_idx="0001" ;
      private String subGridlevel_usuario_Class ;
      private String subGridlevel_usuario_Linesclass ;
      private String dynUsuario_Codigo_Jsonclick ;
      private String ROClassString ;
      private String edtUsuario_PessoaCod_Jsonclick ;
      private String edtUsuario_PessoaNom_Jsonclick ;
      private String dynFuncao_Codigo_Jsonclick ;
      private String edtFuncao_Nome_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String subGridlevel_usuario_Internalname ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n2178Equipe_AreaTrabalhoDescricao ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool n58Usuario_PessoaNom ;
      private String A622Funcao_Nome ;
      private String A2178Equipe_AreaTrabalhoDescricao ;
      private String Z2178Equipe_AreaTrabalhoDescricao ;
      private String Z622Funcao_Nome ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid Gridlevel_usuarioContainer ;
      private GXWebRow Gridlevel_usuarioRow ;
      private GXWebColumn Gridlevel_usuarioColumn ;
      private IDataStoreProvider pr_default ;
      private int[] T005D10_A621Funcao_Codigo ;
      private String[] T005D10_A622Funcao_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynUsuario_Codigo ;
      private GXCombobox dynFuncao_Codigo ;
      private String[] T005D9_A2178Equipe_AreaTrabalhoDescricao ;
      private bool[] T005D9_n2178Equipe_AreaTrabalhoDescricao ;
      private int[] T005D11_A2175Equipe_Codigo ;
      private String[] T005D11_A2176Equipe_Nome ;
      private String[] T005D11_A2178Equipe_AreaTrabalhoDescricao ;
      private bool[] T005D11_n2178Equipe_AreaTrabalhoDescricao ;
      private int[] T005D11_A2177Equipe_AreaTrabalhoCodigo ;
      private String[] T005D12_A2178Equipe_AreaTrabalhoDescricao ;
      private bool[] T005D12_n2178Equipe_AreaTrabalhoDescricao ;
      private int[] T005D13_A2175Equipe_Codigo ;
      private int[] T005D8_A2175Equipe_Codigo ;
      private String[] T005D8_A2176Equipe_Nome ;
      private int[] T005D8_A2177Equipe_AreaTrabalhoCodigo ;
      private int[] T005D14_A2175Equipe_Codigo ;
      private int[] T005D15_A2175Equipe_Codigo ;
      private int[] T005D7_A2175Equipe_Codigo ;
      private String[] T005D7_A2176Equipe_Nome ;
      private int[] T005D7_A2177Equipe_AreaTrabalhoCodigo ;
      private String[] T005D19_A2178Equipe_AreaTrabalhoDescricao ;
      private bool[] T005D19_n2178Equipe_AreaTrabalhoDescricao ;
      private int[] T005D20_A2175Equipe_Codigo ;
      private int[] T005D4_A57Usuario_PessoaCod ;
      private String[] T005D6_A58Usuario_PessoaNom ;
      private bool[] T005D6_n58Usuario_PessoaNom ;
      private int[] T005D21_A2175Equipe_Codigo ;
      private String[] T005D21_A58Usuario_PessoaNom ;
      private bool[] T005D21_n58Usuario_PessoaNom ;
      private String[] T005D21_A622Funcao_Nome ;
      private int[] T005D21_A1Usuario_Codigo ;
      private int[] T005D21_A621Funcao_Codigo ;
      private int[] T005D21_A57Usuario_PessoaCod ;
      private String[] T005D5_A622Funcao_Nome ;
      private int[] T005D22_A57Usuario_PessoaCod ;
      private String[] T005D23_A58Usuario_PessoaNom ;
      private bool[] T005D23_n58Usuario_PessoaNom ;
      private String[] T005D24_A622Funcao_Nome ;
      private int[] T005D25_A2175Equipe_Codigo ;
      private int[] T005D25_A1Usuario_Codigo ;
      private int[] T005D3_A2175Equipe_Codigo ;
      private int[] T005D3_A1Usuario_Codigo ;
      private int[] T005D3_A621Funcao_Codigo ;
      private int[] T005D2_A2175Equipe_Codigo ;
      private int[] T005D2_A1Usuario_Codigo ;
      private int[] T005D2_A621Funcao_Codigo ;
      private int[] T005D29_A57Usuario_PessoaCod ;
      private String[] T005D30_A58Usuario_PessoaNom ;
      private bool[] T005D30_n58Usuario_PessoaNom ;
      private String[] T005D31_A622Funcao_Nome ;
      private int[] T005D32_A2175Equipe_Codigo ;
      private int[] T005D32_A1Usuario_Codigo ;
      private int[] T005D33_A621Funcao_Codigo ;
      private String[] T005D33_A622Funcao_Nome ;
      private int[] T005D34_A57Usuario_PessoaCod ;
      private int[] T005D34_A1Usuario_Codigo ;
      private String[] T005D34_A58Usuario_PessoaNom ;
      private bool[] T005D34_n58Usuario_PessoaNom ;
      private int[] T005D35_A621Funcao_Codigo ;
      private String[] T005D35_A622Funcao_Nome ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class equipe__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new UpdateCursor(def[24])
         ,new UpdateCursor(def[25])
         ,new UpdateCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT005D10 ;
          prmT005D10 = new Object[] {
          } ;
          Object[] prmT005D11 ;
          prmT005D11 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D9 ;
          prmT005D9 = new Object[] {
          new Object[] {"@Equipe_AreaTrabalhoCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D12 ;
          prmT005D12 = new Object[] {
          new Object[] {"@Equipe_AreaTrabalhoCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D13 ;
          prmT005D13 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D8 ;
          prmT005D8 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D14 ;
          prmT005D14 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D15 ;
          prmT005D15 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D7 ;
          prmT005D7 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D16 ;
          prmT005D16 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Equipe_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Equipe_AreaTrabalhoCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D17 ;
          prmT005D17 = new Object[] {
          new Object[] {"@Equipe_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Equipe_AreaTrabalhoCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D18 ;
          prmT005D18 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D19 ;
          prmT005D19 = new Object[] {
          new Object[] {"@Equipe_AreaTrabalhoCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D20 ;
          prmT005D20 = new Object[] {
          } ;
          Object[] prmT005D21 ;
          prmT005D21 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D4 ;
          prmT005D4 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D6 ;
          prmT005D6 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D5 ;
          prmT005D5 = new Object[] {
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D22 ;
          prmT005D22 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D23 ;
          prmT005D23 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D24 ;
          prmT005D24 = new Object[] {
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D25 ;
          prmT005D25 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D3 ;
          prmT005D3 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D2 ;
          prmT005D2 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D26 ;
          prmT005D26 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D27 ;
          prmT005D27 = new Object[] {
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D28 ;
          prmT005D28 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D32 ;
          prmT005D32 = new Object[] {
          new Object[] {"@Equipe_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D33 ;
          prmT005D33 = new Object[] {
          } ;
          Object[] prmT005D34 ;
          prmT005D34 = new Object[] {
          } ;
          Object[] prmT005D35 ;
          prmT005D35 = new Object[] {
          } ;
          Object[] prmT005D29 ;
          prmT005D29 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D30 ;
          prmT005D30 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005D31 ;
          prmT005D31 = new Object[] {
          new Object[] {"@Funcao_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T005D2", "SELECT [Equipe_Codigo], [Usuario_Codigo], [Funcao_Codigo] FROM [EquipeUsuario] WITH (UPDLOCK) WHERE [Equipe_Codigo] = @Equipe_Codigo AND [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D2,1,0,true,false )
             ,new CursorDef("T005D3", "SELECT [Equipe_Codigo], [Usuario_Codigo], [Funcao_Codigo] FROM [EquipeUsuario] WITH (NOLOCK) WHERE [Equipe_Codigo] = @Equipe_Codigo AND [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D3,1,0,true,false )
             ,new CursorDef("T005D4", "SELECT [Usuario_PessoaCod] AS Usuario_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D4,1,0,true,false )
             ,new CursorDef("T005D5", "SELECT [Funcao_Nome] FROM [Geral_Funcao] WITH (NOLOCK) WHERE [Funcao_Codigo] = @Funcao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D5,1,0,true,false )
             ,new CursorDef("T005D6", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D6,1,0,true,false )
             ,new CursorDef("T005D7", "SELECT [Equipe_Codigo], [Equipe_Nome], [Equipe_AreaTrabalhoCodigo] AS Equipe_AreaTrabalhoCodigo FROM [Equipe] WITH (UPDLOCK) WHERE [Equipe_Codigo] = @Equipe_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D7,1,0,true,false )
             ,new CursorDef("T005D8", "SELECT [Equipe_Codigo], [Equipe_Nome], [Equipe_AreaTrabalhoCodigo] AS Equipe_AreaTrabalhoCodigo FROM [Equipe] WITH (NOLOCK) WHERE [Equipe_Codigo] = @Equipe_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D8,1,0,true,false )
             ,new CursorDef("T005D9", "SELECT [AreaTrabalho_Descricao] AS Equipe_AreaTrabalhoDescricao FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Equipe_AreaTrabalhoCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D9,1,0,true,false )
             ,new CursorDef("T005D10", "SELECT [Funcao_Codigo], [Funcao_Nome] FROM [Geral_Funcao] WITH (NOLOCK) ORDER BY [Funcao_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D10,0,0,true,false )
             ,new CursorDef("T005D11", "SELECT TM1.[Equipe_Codigo], TM1.[Equipe_Nome], T2.[AreaTrabalho_Descricao] AS Equipe_AreaTrabalhoDescricao, TM1.[Equipe_AreaTrabalhoCodigo] AS Equipe_AreaTrabalhoCodigo FROM ([Equipe] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Equipe_AreaTrabalhoCodigo]) WHERE TM1.[Equipe_Codigo] = @Equipe_Codigo ORDER BY TM1.[Equipe_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005D11,100,0,true,false )
             ,new CursorDef("T005D12", "SELECT [AreaTrabalho_Descricao] AS Equipe_AreaTrabalhoDescricao FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Equipe_AreaTrabalhoCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D12,1,0,true,false )
             ,new CursorDef("T005D13", "SELECT [Equipe_Codigo] FROM [Equipe] WITH (NOLOCK) WHERE [Equipe_Codigo] = @Equipe_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005D13,1,0,true,false )
             ,new CursorDef("T005D14", "SELECT TOP 1 [Equipe_Codigo] FROM [Equipe] WITH (NOLOCK) WHERE ( [Equipe_Codigo] > @Equipe_Codigo) ORDER BY [Equipe_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005D14,1,0,true,true )
             ,new CursorDef("T005D15", "SELECT TOP 1 [Equipe_Codigo] FROM [Equipe] WITH (NOLOCK) WHERE ( [Equipe_Codigo] < @Equipe_Codigo) ORDER BY [Equipe_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005D15,1,0,true,true )
             ,new CursorDef("T005D16", "INSERT INTO [Equipe]([Equipe_Codigo], [Equipe_Nome], [Equipe_AreaTrabalhoCodigo]) VALUES(@Equipe_Codigo, @Equipe_Nome, @Equipe_AreaTrabalhoCodigo)", GxErrorMask.GX_NOMASK,prmT005D16)
             ,new CursorDef("T005D17", "UPDATE [Equipe] SET [Equipe_Nome]=@Equipe_Nome, [Equipe_AreaTrabalhoCodigo]=@Equipe_AreaTrabalhoCodigo  WHERE [Equipe_Codigo] = @Equipe_Codigo", GxErrorMask.GX_NOMASK,prmT005D17)
             ,new CursorDef("T005D18", "DELETE FROM [Equipe]  WHERE [Equipe_Codigo] = @Equipe_Codigo", GxErrorMask.GX_NOMASK,prmT005D18)
             ,new CursorDef("T005D19", "SELECT [AreaTrabalho_Descricao] AS Equipe_AreaTrabalhoDescricao FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Equipe_AreaTrabalhoCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D19,1,0,true,false )
             ,new CursorDef("T005D20", "SELECT [Equipe_Codigo] FROM [Equipe] WITH (NOLOCK) ORDER BY [Equipe_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005D20,100,0,true,false )
             ,new CursorDef("T005D21", "SELECT T1.[Equipe_Codigo], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T4.[Funcao_Nome], T1.[Usuario_Codigo], T1.[Funcao_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod FROM ((([EquipeUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Geral_Funcao] T4 WITH (NOLOCK) ON T4.[Funcao_Codigo] = T1.[Funcao_Codigo]) WHERE T1.[Equipe_Codigo] = @Equipe_Codigo and T1.[Usuario_Codigo] = @Usuario_Codigo ORDER BY T1.[Equipe_Codigo], T1.[Usuario_Codigo] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D21,11,0,true,false )
             ,new CursorDef("T005D22", "SELECT [Usuario_PessoaCod] AS Usuario_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D22,1,0,true,false )
             ,new CursorDef("T005D23", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D23,1,0,true,false )
             ,new CursorDef("T005D24", "SELECT [Funcao_Nome] FROM [Geral_Funcao] WITH (NOLOCK) WHERE [Funcao_Codigo] = @Funcao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D24,1,0,true,false )
             ,new CursorDef("T005D25", "SELECT [Equipe_Codigo], [Usuario_Codigo] FROM [EquipeUsuario] WITH (NOLOCK) WHERE [Equipe_Codigo] = @Equipe_Codigo AND [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D25,1,0,true,false )
             ,new CursorDef("T005D26", "INSERT INTO [EquipeUsuario]([Equipe_Codigo], [Usuario_Codigo], [Funcao_Codigo]) VALUES(@Equipe_Codigo, @Usuario_Codigo, @Funcao_Codigo)", GxErrorMask.GX_NOMASK,prmT005D26)
             ,new CursorDef("T005D27", "UPDATE [EquipeUsuario] SET [Funcao_Codigo]=@Funcao_Codigo  WHERE [Equipe_Codigo] = @Equipe_Codigo AND [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK,prmT005D27)
             ,new CursorDef("T005D28", "DELETE FROM [EquipeUsuario]  WHERE [Equipe_Codigo] = @Equipe_Codigo AND [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK,prmT005D28)
             ,new CursorDef("T005D29", "SELECT [Usuario_PessoaCod] AS Usuario_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D29,1,0,true,false )
             ,new CursorDef("T005D30", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D30,1,0,true,false )
             ,new CursorDef("T005D31", "SELECT [Funcao_Nome] FROM [Geral_Funcao] WITH (NOLOCK) WHERE [Funcao_Codigo] = @Funcao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D31,1,0,true,false )
             ,new CursorDef("T005D32", "SELECT [Equipe_Codigo], [Usuario_Codigo] FROM [EquipeUsuario] WITH (NOLOCK) WHERE [Equipe_Codigo] = @Equipe_Codigo ORDER BY [Equipe_Codigo], [Usuario_Codigo] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D32,11,0,true,false )
             ,new CursorDef("T005D33", "SELECT [Funcao_Codigo], [Funcao_Nome] FROM [Geral_Funcao] WITH (NOLOCK) ORDER BY [Funcao_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D33,0,0,true,false )
             ,new CursorDef("T005D34", "SELECT T2.[Pessoa_Codigo] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) ORDER BY T2.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D34,0,0,true,false )
             ,new CursorDef("T005D35", "SELECT [Funcao_Codigo], [Funcao_Nome] FROM [Geral_Funcao] WITH (NOLOCK) ORDER BY [Funcao_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005D35,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 29 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 15 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
