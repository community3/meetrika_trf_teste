/*
               File: type_SdtSDT_RedmineUsers_user
        Description: SDT_RedmineUsers
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:7.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_RedmineUsers.user" )]
   [XmlType(TypeName =  "SDT_RedmineUsers.user" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineUsers_user_custom_fields ))]
   [Serializable]
   public class SdtSDT_RedmineUsers_user : GxUserType
   {
      public SdtSDT_RedmineUsers_user( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_RedmineUsers_user_Login = "";
         gxTv_SdtSDT_RedmineUsers_user_Firstname = "";
         gxTv_SdtSDT_RedmineUsers_user_Lastname = "";
         gxTv_SdtSDT_RedmineUsers_user_Mail = "";
         gxTv_SdtSDT_RedmineUsers_user_Created_on = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_RedmineUsers_user_Last_login_on = "";
      }

      public SdtSDT_RedmineUsers_user( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_RedmineUsers_user deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_RedmineUsers_user)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_RedmineUsers_user obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Login = deserialized.gxTpr_Login;
         obj.gxTpr_Firstname = deserialized.gxTpr_Firstname;
         obj.gxTpr_Lastname = deserialized.gxTpr_Lastname;
         obj.gxTpr_Mail = deserialized.gxTpr_Mail;
         obj.gxTpr_Created_on = deserialized.gxTpr_Created_on;
         obj.gxTpr_Last_login_on = deserialized.gxTpr_Last_login_on;
         obj.gxTpr_Custom_fields = deserialized.gxTpr_Custom_fields;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "id") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineUsers_user_Id = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "login") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineUsers_user_Login = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "firstname") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineUsers_user_Firstname = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "lastname") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineUsers_user_Lastname = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "mail") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineUsers_user_Mail = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "created_on") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_RedmineUsers_user_Created_on = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_RedmineUsers_user_Created_on = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "last_login_on") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineUsers_user_Last_login_on = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "custom_fields") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_RedmineUsers_user_Custom_fields == null )
                  {
                     gxTv_SdtSDT_RedmineUsers_user_Custom_fields = new SdtSDT_RedmineUsers_user_custom_fields(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_RedmineUsers_user_Custom_fields.readxml(oReader, "custom_fields");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_RedmineUsers.user";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RedmineUsers_user_Id), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("login", StringUtil.RTrim( gxTv_SdtSDT_RedmineUsers_user_Login));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("firstname", StringUtil.RTrim( gxTv_SdtSDT_RedmineUsers_user_Firstname));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("lastname", StringUtil.RTrim( gxTv_SdtSDT_RedmineUsers_user_Lastname));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("mail", StringUtil.RTrim( gxTv_SdtSDT_RedmineUsers_user_Mail));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_RedmineUsers_user_Created_on) )
         {
            oWriter.WriteStartElement("created_on");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_RedmineUsers_user_Created_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_RedmineUsers_user_Created_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_RedmineUsers_user_Created_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_RedmineUsers_user_Created_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_RedmineUsers_user_Created_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_RedmineUsers_user_Created_on)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("created_on", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "");
            }
         }
         oWriter.WriteElement("last_login_on", StringUtil.RTrim( gxTv_SdtSDT_RedmineUsers_user_Last_login_on));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         if ( gxTv_SdtSDT_RedmineUsers_user_Custom_fields != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_RedmineUsers_user_Custom_fields.writexml(oWriter, "custom_fields", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("id", gxTv_SdtSDT_RedmineUsers_user_Id, false);
         AddObjectProperty("login", gxTv_SdtSDT_RedmineUsers_user_Login, false);
         AddObjectProperty("firstname", gxTv_SdtSDT_RedmineUsers_user_Firstname, false);
         AddObjectProperty("lastname", gxTv_SdtSDT_RedmineUsers_user_Lastname, false);
         AddObjectProperty("mail", gxTv_SdtSDT_RedmineUsers_user_Mail, false);
         datetime_STZ = gxTv_SdtSDT_RedmineUsers_user_Created_on;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("created_on", sDateCnv, false);
         AddObjectProperty("last_login_on", gxTv_SdtSDT_RedmineUsers_user_Last_login_on, false);
         if ( gxTv_SdtSDT_RedmineUsers_user_Custom_fields != null )
         {
            AddObjectProperty("custom_fields", gxTv_SdtSDT_RedmineUsers_user_Custom_fields, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "id" )]
      [  XmlElement( ElementName = "id" , Namespace = ""  )]
      public short gxTpr_Id
      {
         get {
            return gxTv_SdtSDT_RedmineUsers_user_Id ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_user_Id = (short)(value);
         }

      }

      [  SoapElement( ElementName = "login" )]
      [  XmlElement( ElementName = "login" , Namespace = ""  )]
      public String gxTpr_Login
      {
         get {
            return gxTv_SdtSDT_RedmineUsers_user_Login ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_user_Login = (String)(value);
         }

      }

      [  SoapElement( ElementName = "firstname" )]
      [  XmlElement( ElementName = "firstname" , Namespace = ""  )]
      public String gxTpr_Firstname
      {
         get {
            return gxTv_SdtSDT_RedmineUsers_user_Firstname ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_user_Firstname = (String)(value);
         }

      }

      [  SoapElement( ElementName = "lastname" )]
      [  XmlElement( ElementName = "lastname" , Namespace = ""  )]
      public String gxTpr_Lastname
      {
         get {
            return gxTv_SdtSDT_RedmineUsers_user_Lastname ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_user_Lastname = (String)(value);
         }

      }

      [  SoapElement( ElementName = "mail" )]
      [  XmlElement( ElementName = "mail" , Namespace = ""  )]
      public String gxTpr_Mail
      {
         get {
            return gxTv_SdtSDT_RedmineUsers_user_Mail ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_user_Mail = (String)(value);
         }

      }

      [  SoapElement( ElementName = "created_on" )]
      [  XmlElement( ElementName = "created_on" , Namespace = "" , IsNullable=true )]
      public string gxTpr_Created_on_Nullable
      {
         get {
            if ( gxTv_SdtSDT_RedmineUsers_user_Created_on == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_RedmineUsers_user_Created_on).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_RedmineUsers_user_Created_on = DateTime.MinValue;
            else
               gxTv_SdtSDT_RedmineUsers_user_Created_on = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Created_on
      {
         get {
            return gxTv_SdtSDT_RedmineUsers_user_Created_on ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_user_Created_on = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "last_login_on" )]
      [  XmlElement( ElementName = "last_login_on" , Namespace = ""  )]
      public String gxTpr_Last_login_on
      {
         get {
            return gxTv_SdtSDT_RedmineUsers_user_Last_login_on ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_user_Last_login_on = (String)(value);
         }

      }

      [  SoapElement( ElementName = "custom_fields" )]
      [  XmlElement( ElementName = "custom_fields"   )]
      public SdtSDT_RedmineUsers_user_custom_fields gxTpr_Custom_fields
      {
         get {
            if ( gxTv_SdtSDT_RedmineUsers_user_Custom_fields == null )
            {
               gxTv_SdtSDT_RedmineUsers_user_Custom_fields = new SdtSDT_RedmineUsers_user_custom_fields(context);
            }
            return gxTv_SdtSDT_RedmineUsers_user_Custom_fields ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_user_Custom_fields = value;
         }

      }

      public void gxTv_SdtSDT_RedmineUsers_user_Custom_fields_SetNull( )
      {
         gxTv_SdtSDT_RedmineUsers_user_Custom_fields = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineUsers_user_Custom_fields_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineUsers_user_Custom_fields == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_RedmineUsers_user_Login = "";
         gxTv_SdtSDT_RedmineUsers_user_Firstname = "";
         gxTv_SdtSDT_RedmineUsers_user_Lastname = "";
         gxTv_SdtSDT_RedmineUsers_user_Mail = "";
         gxTv_SdtSDT_RedmineUsers_user_Created_on = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_RedmineUsers_user_Last_login_on = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      protected short gxTv_SdtSDT_RedmineUsers_user_Id ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_RedmineUsers_user_Login ;
      protected String gxTv_SdtSDT_RedmineUsers_user_Firstname ;
      protected String gxTv_SdtSDT_RedmineUsers_user_Lastname ;
      protected String gxTv_SdtSDT_RedmineUsers_user_Mail ;
      protected String gxTv_SdtSDT_RedmineUsers_user_Last_login_on ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtSDT_RedmineUsers_user_Created_on ;
      protected DateTime datetime_STZ ;
      protected SdtSDT_RedmineUsers_user_custom_fields gxTv_SdtSDT_RedmineUsers_user_Custom_fields=null ;
   }

   [DataContract(Name = @"SDT_RedmineUsers.user", Namespace = "")]
   public class SdtSDT_RedmineUsers_user_RESTInterface : GxGenericCollectionItem<SdtSDT_RedmineUsers_user>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_RedmineUsers_user_RESTInterface( ) : base()
      {
      }

      public SdtSDT_RedmineUsers_user_RESTInterface( SdtSDT_RedmineUsers_user psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "id" , Order = 0 )]
      public Nullable<short> gxTpr_Id
      {
         get {
            return sdt.gxTpr_Id ;
         }

         set {
            sdt.gxTpr_Id = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "login" , Order = 1 )]
      public String gxTpr_Login
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Login) ;
         }

         set {
            sdt.gxTpr_Login = (String)(value);
         }

      }

      [DataMember( Name = "firstname" , Order = 2 )]
      public String gxTpr_Firstname
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Firstname) ;
         }

         set {
            sdt.gxTpr_Firstname = (String)(value);
         }

      }

      [DataMember( Name = "lastname" , Order = 3 )]
      public String gxTpr_Lastname
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Lastname) ;
         }

         set {
            sdt.gxTpr_Lastname = (String)(value);
         }

      }

      [DataMember( Name = "mail" , Order = 4 )]
      public String gxTpr_Mail
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Mail) ;
         }

         set {
            sdt.gxTpr_Mail = (String)(value);
         }

      }

      [DataMember( Name = "created_on" , Order = 5 )]
      public String gxTpr_Created_on
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Created_on) ;
         }

         set {
            sdt.gxTpr_Created_on = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "last_login_on" , Order = 6 )]
      public String gxTpr_Last_login_on
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Last_login_on) ;
         }

         set {
            sdt.gxTpr_Last_login_on = (String)(value);
         }

      }

      [DataMember( Name = "custom_fields" , Order = 7 )]
      public SdtSDT_RedmineUsers_user_custom_fields_RESTInterface gxTpr_Custom_fields
      {
         get {
            return new SdtSDT_RedmineUsers_user_custom_fields_RESTInterface(sdt.gxTpr_Custom_fields) ;
         }

         set {
            sdt.gxTpr_Custom_fields = value.sdt;
         }

      }

      public SdtSDT_RedmineUsers_user sdt
      {
         get {
            return (SdtSDT_RedmineUsers_user)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_RedmineUsers_user() ;
         }
      }

   }

}
