/*
               File: GetWC_PerfisUsuarioFilterData
        Description: Get WC_Perfis Usuario Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:2.69
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwc_perfisusuariofilterdata : GXProcedure
   {
      public getwc_perfisusuariofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwc_perfisusuariofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwc_perfisusuariofilterdata objgetwc_perfisusuariofilterdata;
         objgetwc_perfisusuariofilterdata = new getwc_perfisusuariofilterdata();
         objgetwc_perfisusuariofilterdata.AV14DDOName = aP0_DDOName;
         objgetwc_perfisusuariofilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetwc_perfisusuariofilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetwc_perfisusuariofilterdata.AV18OptionsJson = "" ;
         objgetwc_perfisusuariofilterdata.AV21OptionsDescJson = "" ;
         objgetwc_perfisusuariofilterdata.AV23OptionIndexesJson = "" ;
         objgetwc_perfisusuariofilterdata.context.SetSubmitInitialConfig(context);
         objgetwc_perfisusuariofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwc_perfisusuariofilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwc_perfisusuariofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_PERFIL_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADPERFIL_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("WC_PerfisUsuarioGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WC_PerfisUsuarioGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("WC_PerfisUsuarioGridState"), "");
         }
         AV34GXV1 = 1;
         while ( AV34GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV34GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "PERFIL_AREATRABALHOCOD") == 0 )
            {
               AV30Perfil_AreaTrabalhoCod = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME") == 0 )
            {
               AV10TFPerfil_Nome = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME_SEL") == 0 )
            {
               AV11TFPerfil_Nome_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "PARM_&USUARIO_CODIGO") == 0 )
            {
               AV31Usuario_Codigo = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            AV34GXV1 = (int)(AV34GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADPERFIL_NOMEOPTIONS' Routine */
         AV10TFPerfil_Nome = AV12SearchTxt;
         AV11TFPerfil_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFPerfil_Nome_Sel ,
                                              AV10TFPerfil_Nome ,
                                              A4Perfil_Nome ,
                                              A7Perfil_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              AV31Usuario_Codigo ,
                                              A1Usuario_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFPerfil_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFPerfil_Nome), 50, "%");
         /* Using cursor P00W02 */
         pr_default.execute(0, new Object[] {AV31Usuario_Codigo, AV9WWPContext.gxTpr_Areatrabalho_codigo, lV10TFPerfil_Nome, AV11TFPerfil_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKW02 = false;
            A3Perfil_Codigo = P00W02_A3Perfil_Codigo[0];
            A1Usuario_Codigo = P00W02_A1Usuario_Codigo[0];
            A7Perfil_AreaTrabalhoCod = P00W02_A7Perfil_AreaTrabalhoCod[0];
            A4Perfil_Nome = P00W02_A4Perfil_Nome[0];
            A7Perfil_AreaTrabalhoCod = P00W02_A7Perfil_AreaTrabalhoCod[0];
            A4Perfil_Nome = P00W02_A4Perfil_Nome[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00W02_A1Usuario_Codigo[0] == A1Usuario_Codigo ) && ( StringUtil.StrCmp(P00W02_A4Perfil_Nome[0], A4Perfil_Nome) == 0 ) )
            {
               BRKW02 = false;
               A3Perfil_Codigo = P00W02_A3Perfil_Codigo[0];
               AV24count = (long)(AV24count+1);
               BRKW02 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A4Perfil_Nome)) )
            {
               AV16Option = A4Perfil_Nome;
               AV17Options.Add(AV16Option, 0);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKW02 )
            {
               BRKW02 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFPerfil_Nome = "";
         AV11TFPerfil_Nome_Sel = "";
         scmdbuf = "";
         lV10TFPerfil_Nome = "";
         A4Perfil_Nome = "";
         P00W02_A3Perfil_Codigo = new int[1] ;
         P00W02_A1Usuario_Codigo = new int[1] ;
         P00W02_A7Perfil_AreaTrabalhoCod = new int[1] ;
         P00W02_A4Perfil_Nome = new String[] {""} ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwc_perfisusuariofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00W02_A3Perfil_Codigo, P00W02_A1Usuario_Codigo, P00W02_A7Perfil_AreaTrabalhoCod, P00W02_A4Perfil_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV34GXV1 ;
      private int AV30Perfil_AreaTrabalhoCod ;
      private int AV31Usuario_Codigo ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A7Perfil_AreaTrabalhoCod ;
      private int A1Usuario_Codigo ;
      private int A3Perfil_Codigo ;
      private long AV24count ;
      private String AV10TFPerfil_Nome ;
      private String AV11TFPerfil_Nome_Sel ;
      private String scmdbuf ;
      private String lV10TFPerfil_Nome ;
      private String A4Perfil_Nome ;
      private bool returnInSub ;
      private bool BRKW02 ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00W02_A3Perfil_Codigo ;
      private int[] P00W02_A1Usuario_Codigo ;
      private int[] P00W02_A7Perfil_AreaTrabalhoCod ;
      private String[] P00W02_A4Perfil_Nome ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
   }

   public class getwc_perfisusuariofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00W02( IGxContext context ,
                                             String AV11TFPerfil_Nome_Sel ,
                                             String AV10TFPerfil_Nome ,
                                             String A4Perfil_Nome ,
                                             int A7Perfil_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             int AV31Usuario_Codigo ,
                                             int A1Usuario_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [4] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Perfil_Codigo], T1.[Usuario_Codigo], T2.[Perfil_AreaTrabalhoCod], T2.[Perfil_Nome] FROM ([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Usuario_Codigo] = @AV31Usuario_Codigo)";
         scmdbuf = scmdbuf + " and (T2.[Perfil_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFPerfil_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFPerfil_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Perfil_Nome] like @lV10TFPerfil_Nome)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFPerfil_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Perfil_Nome] = @AV11TFPerfil_Nome_Sel)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Usuario_Codigo], T2.[Perfil_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00W02(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00W02 ;
          prmP00W02 = new Object[] {
          new Object[] {"@AV31Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFPerfil_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFPerfil_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00W02", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W02,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwc_perfisusuariofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwc_perfisusuariofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwc_perfisusuariofilterdata") )
          {
             return  ;
          }
          getwc_perfisusuariofilterdata worker = new getwc_perfisusuariofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
