/*
               File: GetPromptContratadaUsuarioFilterData
        Description: Get Prompt Contratada Usuario Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:44.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratadausuariofilterdata : GXProcedure
   {
      public getpromptcontratadausuariofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratadausuariofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratadausuariofilterdata objgetpromptcontratadausuariofilterdata;
         objgetpromptcontratadausuariofilterdata = new getpromptcontratadausuariofilterdata();
         objgetpromptcontratadausuariofilterdata.AV18DDOName = aP0_DDOName;
         objgetpromptcontratadausuariofilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetpromptcontratadausuariofilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratadausuariofilterdata.AV22OptionsJson = "" ;
         objgetpromptcontratadausuariofilterdata.AV25OptionsDescJson = "" ;
         objgetpromptcontratadausuariofilterdata.AV27OptionIndexesJson = "" ;
         objgetpromptcontratadausuariofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratadausuariofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratadausuariofilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratadausuariofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_USUARIO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADUSUARIO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADAUSUARIO_USUARIOPESSOANOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADAUSUARIO_CONTRATADAPESSOANOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("PromptContratadaUsuarioGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratadaUsuarioGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("PromptContratadaUsuarioGridState"), "");
         }
         AV53GXV1 = 1;
         while ( AV53GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV53GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME") == 0 )
            {
               AV10TFUsuario_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME_SEL") == 0 )
            {
               AV11TFUsuario_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV12TFContratadaUsuario_UsuarioPessoaNom = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL") == 0 )
            {
               AV13TFContratadaUsuario_UsuarioPessoaNom_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
            {
               AV14TFContratadaUsuario_ContratadaPessoaNom = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL") == 0 )
            {
               AV15TFContratadaUsuario_ContratadaPessoaNom_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            AV53GXV1 = (int)(AV53GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "USUARIO_NOME") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV36Usuario_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV37ContratadaUsuario_UsuarioPessoaNom1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV38ContratadaUsuario_ContratadaPessoaNom1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV39DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV40DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "USUARIO_NOME") == 0 )
               {
                  AV41DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV42Usuario_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
               {
                  AV41DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV43ContratadaUsuario_UsuarioPessoaNom2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
               {
                  AV41DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV44ContratadaUsuario_ContratadaPessoaNom2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV45DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV46DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "USUARIO_NOME") == 0 )
                  {
                     AV47DynamicFiltersOperator3 = AV33GridStateDynamicFilter.gxTpr_Operator;
                     AV48Usuario_Nome3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
                  {
                     AV47DynamicFiltersOperator3 = AV33GridStateDynamicFilter.gxTpr_Operator;
                     AV49ContratadaUsuario_UsuarioPessoaNom3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
                  {
                     AV47DynamicFiltersOperator3 = AV33GridStateDynamicFilter.gxTpr_Operator;
                     AV50ContratadaUsuario_ContratadaPessoaNom3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADUSUARIO_NOMEOPTIONS' Routine */
         AV10TFUsuario_Nome = AV16SearchTxt;
         AV11TFUsuario_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV36Usuario_Nome1 ,
                                              AV37ContratadaUsuario_UsuarioPessoaNom1 ,
                                              AV38ContratadaUsuario_ContratadaPessoaNom1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41DynamicFiltersOperator2 ,
                                              AV42Usuario_Nome2 ,
                                              AV43ContratadaUsuario_UsuarioPessoaNom2 ,
                                              AV44ContratadaUsuario_ContratadaPessoaNom2 ,
                                              AV45DynamicFiltersEnabled3 ,
                                              AV46DynamicFiltersSelector3 ,
                                              AV47DynamicFiltersOperator3 ,
                                              AV48Usuario_Nome3 ,
                                              AV49ContratadaUsuario_UsuarioPessoaNom3 ,
                                              AV50ContratadaUsuario_ContratadaPessoaNom3 ,
                                              AV11TFUsuario_Nome_Sel ,
                                              AV10TFUsuario_Nome ,
                                              AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                              AV12TFContratadaUsuario_UsuarioPessoaNom ,
                                              AV15TFContratadaUsuario_ContratadaPessoaNom_Sel ,
                                              AV14TFContratadaUsuario_ContratadaPessoaNom ,
                                              A2Usuario_Nome ,
                                              A71ContratadaUsuario_UsuarioPessoaNom ,
                                              A68ContratadaUsuario_ContratadaPessoaNom },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV36Usuario_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36Usuario_Nome1), 50, "%");
         lV36Usuario_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36Usuario_Nome1), 50, "%");
         lV37ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
         lV37ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
         lV38ContratadaUsuario_ContratadaPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV38ContratadaUsuario_ContratadaPessoaNom1), 100, "%");
         lV38ContratadaUsuario_ContratadaPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV38ContratadaUsuario_ContratadaPessoaNom1), 100, "%");
         lV42Usuario_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV42Usuario_Nome2), 50, "%");
         lV42Usuario_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV42Usuario_Nome2), 50, "%");
         lV43ContratadaUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV43ContratadaUsuario_UsuarioPessoaNom2), 100, "%");
         lV43ContratadaUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV43ContratadaUsuario_UsuarioPessoaNom2), 100, "%");
         lV44ContratadaUsuario_ContratadaPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV44ContratadaUsuario_ContratadaPessoaNom2), 100, "%");
         lV44ContratadaUsuario_ContratadaPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV44ContratadaUsuario_ContratadaPessoaNom2), 100, "%");
         lV48Usuario_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV48Usuario_Nome3), 50, "%");
         lV48Usuario_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV48Usuario_Nome3), 50, "%");
         lV49ContratadaUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV49ContratadaUsuario_UsuarioPessoaNom3), 100, "%");
         lV49ContratadaUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV49ContratadaUsuario_UsuarioPessoaNom3), 100, "%");
         lV50ContratadaUsuario_ContratadaPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV50ContratadaUsuario_ContratadaPessoaNom3), 100, "%");
         lV50ContratadaUsuario_ContratadaPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV50ContratadaUsuario_ContratadaPessoaNom3), 100, "%");
         lV10TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUsuario_Nome), 50, "%");
         lV12TFContratadaUsuario_UsuarioPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV12TFContratadaUsuario_UsuarioPessoaNom), 100, "%");
         lV14TFContratadaUsuario_ContratadaPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV14TFContratadaUsuario_ContratadaPessoaNom), 100, "%");
         /* Using cursor P00LC2 */
         pr_default.execute(0, new Object[] {lV36Usuario_Nome1, lV36Usuario_Nome1, lV37ContratadaUsuario_UsuarioPessoaNom1, lV37ContratadaUsuario_UsuarioPessoaNom1, lV38ContratadaUsuario_ContratadaPessoaNom1, lV38ContratadaUsuario_ContratadaPessoaNom1, lV42Usuario_Nome2, lV42Usuario_Nome2, lV43ContratadaUsuario_UsuarioPessoaNom2, lV43ContratadaUsuario_UsuarioPessoaNom2, lV44ContratadaUsuario_ContratadaPessoaNom2, lV44ContratadaUsuario_ContratadaPessoaNom2, lV48Usuario_Nome3, lV48Usuario_Nome3, lV49ContratadaUsuario_UsuarioPessoaNom3, lV49ContratadaUsuario_UsuarioPessoaNom3, lV50ContratadaUsuario_ContratadaPessoaNom3, lV50ContratadaUsuario_ContratadaPessoaNom3, lV10TFUsuario_Nome, AV11TFUsuario_Nome_Sel, lV12TFContratadaUsuario_UsuarioPessoaNom, AV13TFContratadaUsuario_UsuarioPessoaNom_Sel, lV14TFContratadaUsuario_ContratadaPessoaNom, AV15TFContratadaUsuario_ContratadaPessoaNom_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKLC2 = false;
            A66ContratadaUsuario_ContratadaCod = P00LC2_A66ContratadaUsuario_ContratadaCod[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00LC2_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00LC2_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A69ContratadaUsuario_UsuarioCod = P00LC2_A69ContratadaUsuario_UsuarioCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00LC2_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00LC2_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A2Usuario_Nome = P00LC2_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LC2_n2Usuario_Nome[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00LC2_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00LC2_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00LC2_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00LC2_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00LC2_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00LC2_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00LC2_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00LC2_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00LC2_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00LC2_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A2Usuario_Nome = P00LC2_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LC2_n2Usuario_Nome[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00LC2_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00LC2_n71ContratadaUsuario_UsuarioPessoaNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00LC2_A2Usuario_Nome[0], A2Usuario_Nome) == 0 ) )
            {
               BRKLC2 = false;
               A66ContratadaUsuario_ContratadaCod = P00LC2_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = P00LC2_A69ContratadaUsuario_UsuarioCod[0];
               AV28count = (long)(AV28count+1);
               BRKLC2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2Usuario_Nome)) )
            {
               AV20Option = A2Usuario_Nome;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLC2 )
            {
               BRKLC2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATADAUSUARIO_USUARIOPESSOANOMOPTIONS' Routine */
         AV12TFContratadaUsuario_UsuarioPessoaNom = AV16SearchTxt;
         AV13TFContratadaUsuario_UsuarioPessoaNom_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV36Usuario_Nome1 ,
                                              AV37ContratadaUsuario_UsuarioPessoaNom1 ,
                                              AV38ContratadaUsuario_ContratadaPessoaNom1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41DynamicFiltersOperator2 ,
                                              AV42Usuario_Nome2 ,
                                              AV43ContratadaUsuario_UsuarioPessoaNom2 ,
                                              AV44ContratadaUsuario_ContratadaPessoaNom2 ,
                                              AV45DynamicFiltersEnabled3 ,
                                              AV46DynamicFiltersSelector3 ,
                                              AV47DynamicFiltersOperator3 ,
                                              AV48Usuario_Nome3 ,
                                              AV49ContratadaUsuario_UsuarioPessoaNom3 ,
                                              AV50ContratadaUsuario_ContratadaPessoaNom3 ,
                                              AV11TFUsuario_Nome_Sel ,
                                              AV10TFUsuario_Nome ,
                                              AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                              AV12TFContratadaUsuario_UsuarioPessoaNom ,
                                              AV15TFContratadaUsuario_ContratadaPessoaNom_Sel ,
                                              AV14TFContratadaUsuario_ContratadaPessoaNom ,
                                              A2Usuario_Nome ,
                                              A71ContratadaUsuario_UsuarioPessoaNom ,
                                              A68ContratadaUsuario_ContratadaPessoaNom },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV36Usuario_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36Usuario_Nome1), 50, "%");
         lV36Usuario_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36Usuario_Nome1), 50, "%");
         lV37ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
         lV37ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
         lV38ContratadaUsuario_ContratadaPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV38ContratadaUsuario_ContratadaPessoaNom1), 100, "%");
         lV38ContratadaUsuario_ContratadaPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV38ContratadaUsuario_ContratadaPessoaNom1), 100, "%");
         lV42Usuario_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV42Usuario_Nome2), 50, "%");
         lV42Usuario_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV42Usuario_Nome2), 50, "%");
         lV43ContratadaUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV43ContratadaUsuario_UsuarioPessoaNom2), 100, "%");
         lV43ContratadaUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV43ContratadaUsuario_UsuarioPessoaNom2), 100, "%");
         lV44ContratadaUsuario_ContratadaPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV44ContratadaUsuario_ContratadaPessoaNom2), 100, "%");
         lV44ContratadaUsuario_ContratadaPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV44ContratadaUsuario_ContratadaPessoaNom2), 100, "%");
         lV48Usuario_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV48Usuario_Nome3), 50, "%");
         lV48Usuario_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV48Usuario_Nome3), 50, "%");
         lV49ContratadaUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV49ContratadaUsuario_UsuarioPessoaNom3), 100, "%");
         lV49ContratadaUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV49ContratadaUsuario_UsuarioPessoaNom3), 100, "%");
         lV50ContratadaUsuario_ContratadaPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV50ContratadaUsuario_ContratadaPessoaNom3), 100, "%");
         lV50ContratadaUsuario_ContratadaPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV50ContratadaUsuario_ContratadaPessoaNom3), 100, "%");
         lV10TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUsuario_Nome), 50, "%");
         lV12TFContratadaUsuario_UsuarioPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV12TFContratadaUsuario_UsuarioPessoaNom), 100, "%");
         lV14TFContratadaUsuario_ContratadaPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV14TFContratadaUsuario_ContratadaPessoaNom), 100, "%");
         /* Using cursor P00LC3 */
         pr_default.execute(1, new Object[] {lV36Usuario_Nome1, lV36Usuario_Nome1, lV37ContratadaUsuario_UsuarioPessoaNom1, lV37ContratadaUsuario_UsuarioPessoaNom1, lV38ContratadaUsuario_ContratadaPessoaNom1, lV38ContratadaUsuario_ContratadaPessoaNom1, lV42Usuario_Nome2, lV42Usuario_Nome2, lV43ContratadaUsuario_UsuarioPessoaNom2, lV43ContratadaUsuario_UsuarioPessoaNom2, lV44ContratadaUsuario_ContratadaPessoaNom2, lV44ContratadaUsuario_ContratadaPessoaNom2, lV48Usuario_Nome3, lV48Usuario_Nome3, lV49ContratadaUsuario_UsuarioPessoaNom3, lV49ContratadaUsuario_UsuarioPessoaNom3, lV50ContratadaUsuario_ContratadaPessoaNom3, lV50ContratadaUsuario_ContratadaPessoaNom3, lV10TFUsuario_Nome, AV11TFUsuario_Nome_Sel, lV12TFContratadaUsuario_UsuarioPessoaNom, AV13TFContratadaUsuario_UsuarioPessoaNom_Sel, lV14TFContratadaUsuario_ContratadaPessoaNom, AV15TFContratadaUsuario_ContratadaPessoaNom_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKLC4 = false;
            A66ContratadaUsuario_ContratadaCod = P00LC3_A66ContratadaUsuario_ContratadaCod[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00LC3_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00LC3_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A69ContratadaUsuario_UsuarioCod = P00LC3_A69ContratadaUsuario_UsuarioCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00LC3_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00LC3_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00LC3_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00LC3_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00LC3_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00LC3_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A2Usuario_Nome = P00LC3_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LC3_n2Usuario_Nome[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00LC3_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00LC3_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00LC3_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00LC3_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00LC3_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00LC3_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A2Usuario_Nome = P00LC3_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LC3_n2Usuario_Nome[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00LC3_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00LC3_n71ContratadaUsuario_UsuarioPessoaNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00LC3_A71ContratadaUsuario_UsuarioPessoaNom[0], A71ContratadaUsuario_UsuarioPessoaNom) == 0 ) )
            {
               BRKLC4 = false;
               A66ContratadaUsuario_ContratadaCod = P00LC3_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = P00LC3_A69ContratadaUsuario_UsuarioCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = P00LC3_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = P00LC3_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = P00LC3_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = P00LC3_n70ContratadaUsuario_UsuarioPessoaCod[0];
               AV28count = (long)(AV28count+1);
               BRKLC4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom)) )
            {
               AV20Option = A71ContratadaUsuario_UsuarioPessoaNom;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLC4 )
            {
               BRKLC4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATADAUSUARIO_CONTRATADAPESSOANOMOPTIONS' Routine */
         AV14TFContratadaUsuario_ContratadaPessoaNom = AV16SearchTxt;
         AV15TFContratadaUsuario_ContratadaPessoaNom_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV36Usuario_Nome1 ,
                                              AV37ContratadaUsuario_UsuarioPessoaNom1 ,
                                              AV38ContratadaUsuario_ContratadaPessoaNom1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41DynamicFiltersOperator2 ,
                                              AV42Usuario_Nome2 ,
                                              AV43ContratadaUsuario_UsuarioPessoaNom2 ,
                                              AV44ContratadaUsuario_ContratadaPessoaNom2 ,
                                              AV45DynamicFiltersEnabled3 ,
                                              AV46DynamicFiltersSelector3 ,
                                              AV47DynamicFiltersOperator3 ,
                                              AV48Usuario_Nome3 ,
                                              AV49ContratadaUsuario_UsuarioPessoaNom3 ,
                                              AV50ContratadaUsuario_ContratadaPessoaNom3 ,
                                              AV11TFUsuario_Nome_Sel ,
                                              AV10TFUsuario_Nome ,
                                              AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                              AV12TFContratadaUsuario_UsuarioPessoaNom ,
                                              AV15TFContratadaUsuario_ContratadaPessoaNom_Sel ,
                                              AV14TFContratadaUsuario_ContratadaPessoaNom ,
                                              A2Usuario_Nome ,
                                              A71ContratadaUsuario_UsuarioPessoaNom ,
                                              A68ContratadaUsuario_ContratadaPessoaNom },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV36Usuario_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36Usuario_Nome1), 50, "%");
         lV36Usuario_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36Usuario_Nome1), 50, "%");
         lV37ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
         lV37ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
         lV38ContratadaUsuario_ContratadaPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV38ContratadaUsuario_ContratadaPessoaNom1), 100, "%");
         lV38ContratadaUsuario_ContratadaPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV38ContratadaUsuario_ContratadaPessoaNom1), 100, "%");
         lV42Usuario_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV42Usuario_Nome2), 50, "%");
         lV42Usuario_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV42Usuario_Nome2), 50, "%");
         lV43ContratadaUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV43ContratadaUsuario_UsuarioPessoaNom2), 100, "%");
         lV43ContratadaUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV43ContratadaUsuario_UsuarioPessoaNom2), 100, "%");
         lV44ContratadaUsuario_ContratadaPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV44ContratadaUsuario_ContratadaPessoaNom2), 100, "%");
         lV44ContratadaUsuario_ContratadaPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV44ContratadaUsuario_ContratadaPessoaNom2), 100, "%");
         lV48Usuario_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV48Usuario_Nome3), 50, "%");
         lV48Usuario_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV48Usuario_Nome3), 50, "%");
         lV49ContratadaUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV49ContratadaUsuario_UsuarioPessoaNom3), 100, "%");
         lV49ContratadaUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV49ContratadaUsuario_UsuarioPessoaNom3), 100, "%");
         lV50ContratadaUsuario_ContratadaPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV50ContratadaUsuario_ContratadaPessoaNom3), 100, "%");
         lV50ContratadaUsuario_ContratadaPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV50ContratadaUsuario_ContratadaPessoaNom3), 100, "%");
         lV10TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUsuario_Nome), 50, "%");
         lV12TFContratadaUsuario_UsuarioPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV12TFContratadaUsuario_UsuarioPessoaNom), 100, "%");
         lV14TFContratadaUsuario_ContratadaPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV14TFContratadaUsuario_ContratadaPessoaNom), 100, "%");
         /* Using cursor P00LC4 */
         pr_default.execute(2, new Object[] {lV36Usuario_Nome1, lV36Usuario_Nome1, lV37ContratadaUsuario_UsuarioPessoaNom1, lV37ContratadaUsuario_UsuarioPessoaNom1, lV38ContratadaUsuario_ContratadaPessoaNom1, lV38ContratadaUsuario_ContratadaPessoaNom1, lV42Usuario_Nome2, lV42Usuario_Nome2, lV43ContratadaUsuario_UsuarioPessoaNom2, lV43ContratadaUsuario_UsuarioPessoaNom2, lV44ContratadaUsuario_ContratadaPessoaNom2, lV44ContratadaUsuario_ContratadaPessoaNom2, lV48Usuario_Nome3, lV48Usuario_Nome3, lV49ContratadaUsuario_UsuarioPessoaNom3, lV49ContratadaUsuario_UsuarioPessoaNom3, lV50ContratadaUsuario_ContratadaPessoaNom3, lV50ContratadaUsuario_ContratadaPessoaNom3, lV10TFUsuario_Nome, AV11TFUsuario_Nome_Sel, lV12TFContratadaUsuario_UsuarioPessoaNom, AV13TFContratadaUsuario_UsuarioPessoaNom_Sel, lV14TFContratadaUsuario_ContratadaPessoaNom, AV15TFContratadaUsuario_ContratadaPessoaNom_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKLC6 = false;
            A66ContratadaUsuario_ContratadaCod = P00LC4_A66ContratadaUsuario_ContratadaCod[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00LC4_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00LC4_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A69ContratadaUsuario_UsuarioCod = P00LC4_A69ContratadaUsuario_UsuarioCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00LC4_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00LC4_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00LC4_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00LC4_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00LC4_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00LC4_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A2Usuario_Nome = P00LC4_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LC4_n2Usuario_Nome[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00LC4_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00LC4_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00LC4_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00LC4_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00LC4_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00LC4_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A2Usuario_Nome = P00LC4_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LC4_n2Usuario_Nome[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00LC4_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00LC4_n71ContratadaUsuario_UsuarioPessoaNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00LC4_A68ContratadaUsuario_ContratadaPessoaNom[0], A68ContratadaUsuario_ContratadaPessoaNom) == 0 ) )
            {
               BRKLC6 = false;
               A66ContratadaUsuario_ContratadaCod = P00LC4_A66ContratadaUsuario_ContratadaCod[0];
               A67ContratadaUsuario_ContratadaPessoaCod = P00LC4_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = P00LC4_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A69ContratadaUsuario_UsuarioCod = P00LC4_A69ContratadaUsuario_UsuarioCod[0];
               A67ContratadaUsuario_ContratadaPessoaCod = P00LC4_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = P00LC4_n67ContratadaUsuario_ContratadaPessoaCod[0];
               AV28count = (long)(AV28count+1);
               BRKLC6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A68ContratadaUsuario_ContratadaPessoaNom)) )
            {
               AV20Option = A68ContratadaUsuario_ContratadaPessoaNom;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLC6 )
            {
               BRKLC6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFUsuario_Nome = "";
         AV11TFUsuario_Nome_Sel = "";
         AV12TFContratadaUsuario_UsuarioPessoaNom = "";
         AV13TFContratadaUsuario_UsuarioPessoaNom_Sel = "";
         AV14TFContratadaUsuario_ContratadaPessoaNom = "";
         AV15TFContratadaUsuario_ContratadaPessoaNom_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV36Usuario_Nome1 = "";
         AV37ContratadaUsuario_UsuarioPessoaNom1 = "";
         AV38ContratadaUsuario_ContratadaPessoaNom1 = "";
         AV40DynamicFiltersSelector2 = "";
         AV42Usuario_Nome2 = "";
         AV43ContratadaUsuario_UsuarioPessoaNom2 = "";
         AV44ContratadaUsuario_ContratadaPessoaNom2 = "";
         AV46DynamicFiltersSelector3 = "";
         AV48Usuario_Nome3 = "";
         AV49ContratadaUsuario_UsuarioPessoaNom3 = "";
         AV50ContratadaUsuario_ContratadaPessoaNom3 = "";
         scmdbuf = "";
         lV36Usuario_Nome1 = "";
         lV37ContratadaUsuario_UsuarioPessoaNom1 = "";
         lV38ContratadaUsuario_ContratadaPessoaNom1 = "";
         lV42Usuario_Nome2 = "";
         lV43ContratadaUsuario_UsuarioPessoaNom2 = "";
         lV44ContratadaUsuario_ContratadaPessoaNom2 = "";
         lV48Usuario_Nome3 = "";
         lV49ContratadaUsuario_UsuarioPessoaNom3 = "";
         lV50ContratadaUsuario_ContratadaPessoaNom3 = "";
         lV10TFUsuario_Nome = "";
         lV12TFContratadaUsuario_UsuarioPessoaNom = "";
         lV14TFContratadaUsuario_ContratadaPessoaNom = "";
         A2Usuario_Nome = "";
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         P00LC2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00LC2_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         P00LC2_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         P00LC2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00LC2_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         P00LC2_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00LC2_A2Usuario_Nome = new String[] {""} ;
         P00LC2_n2Usuario_Nome = new bool[] {false} ;
         P00LC2_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         P00LC2_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         P00LC2_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00LC2_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         AV20Option = "";
         P00LC3_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00LC3_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         P00LC3_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         P00LC3_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00LC3_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         P00LC3_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00LC3_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00LC3_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P00LC3_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         P00LC3_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         P00LC3_A2Usuario_Nome = new String[] {""} ;
         P00LC3_n2Usuario_Nome = new bool[] {false} ;
         P00LC4_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00LC4_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         P00LC4_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         P00LC4_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00LC4_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         P00LC4_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00LC4_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         P00LC4_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         P00LC4_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00LC4_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P00LC4_A2Usuario_Nome = new String[] {""} ;
         P00LC4_n2Usuario_Nome = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratadausuariofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00LC2_A66ContratadaUsuario_ContratadaCod, P00LC2_A67ContratadaUsuario_ContratadaPessoaCod, P00LC2_n67ContratadaUsuario_ContratadaPessoaCod, P00LC2_A69ContratadaUsuario_UsuarioCod, P00LC2_A70ContratadaUsuario_UsuarioPessoaCod, P00LC2_n70ContratadaUsuario_UsuarioPessoaCod, P00LC2_A2Usuario_Nome, P00LC2_n2Usuario_Nome, P00LC2_A68ContratadaUsuario_ContratadaPessoaNom, P00LC2_n68ContratadaUsuario_ContratadaPessoaNom,
               P00LC2_A71ContratadaUsuario_UsuarioPessoaNom, P00LC2_n71ContratadaUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               P00LC3_A66ContratadaUsuario_ContratadaCod, P00LC3_A67ContratadaUsuario_ContratadaPessoaCod, P00LC3_n67ContratadaUsuario_ContratadaPessoaCod, P00LC3_A69ContratadaUsuario_UsuarioCod, P00LC3_A70ContratadaUsuario_UsuarioPessoaCod, P00LC3_n70ContratadaUsuario_UsuarioPessoaCod, P00LC3_A71ContratadaUsuario_UsuarioPessoaNom, P00LC3_n71ContratadaUsuario_UsuarioPessoaNom, P00LC3_A68ContratadaUsuario_ContratadaPessoaNom, P00LC3_n68ContratadaUsuario_ContratadaPessoaNom,
               P00LC3_A2Usuario_Nome, P00LC3_n2Usuario_Nome
               }
               , new Object[] {
               P00LC4_A66ContratadaUsuario_ContratadaCod, P00LC4_A67ContratadaUsuario_ContratadaPessoaCod, P00LC4_n67ContratadaUsuario_ContratadaPessoaCod, P00LC4_A69ContratadaUsuario_UsuarioCod, P00LC4_A70ContratadaUsuario_UsuarioPessoaCod, P00LC4_n70ContratadaUsuario_UsuarioPessoaCod, P00LC4_A68ContratadaUsuario_ContratadaPessoaNom, P00LC4_n68ContratadaUsuario_ContratadaPessoaNom, P00LC4_A71ContratadaUsuario_UsuarioPessoaNom, P00LC4_n71ContratadaUsuario_UsuarioPessoaNom,
               P00LC4_A2Usuario_Nome, P00LC4_n2Usuario_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV35DynamicFiltersOperator1 ;
      private short AV41DynamicFiltersOperator2 ;
      private short AV47DynamicFiltersOperator3 ;
      private int AV53GXV1 ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private long AV28count ;
      private String AV10TFUsuario_Nome ;
      private String AV11TFUsuario_Nome_Sel ;
      private String AV12TFContratadaUsuario_UsuarioPessoaNom ;
      private String AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ;
      private String AV14TFContratadaUsuario_ContratadaPessoaNom ;
      private String AV15TFContratadaUsuario_ContratadaPessoaNom_Sel ;
      private String AV36Usuario_Nome1 ;
      private String AV37ContratadaUsuario_UsuarioPessoaNom1 ;
      private String AV38ContratadaUsuario_ContratadaPessoaNom1 ;
      private String AV42Usuario_Nome2 ;
      private String AV43ContratadaUsuario_UsuarioPessoaNom2 ;
      private String AV44ContratadaUsuario_ContratadaPessoaNom2 ;
      private String AV48Usuario_Nome3 ;
      private String AV49ContratadaUsuario_UsuarioPessoaNom3 ;
      private String AV50ContratadaUsuario_ContratadaPessoaNom3 ;
      private String scmdbuf ;
      private String lV36Usuario_Nome1 ;
      private String lV37ContratadaUsuario_UsuarioPessoaNom1 ;
      private String lV38ContratadaUsuario_ContratadaPessoaNom1 ;
      private String lV42Usuario_Nome2 ;
      private String lV43ContratadaUsuario_UsuarioPessoaNom2 ;
      private String lV44ContratadaUsuario_ContratadaPessoaNom2 ;
      private String lV48Usuario_Nome3 ;
      private String lV49ContratadaUsuario_UsuarioPessoaNom3 ;
      private String lV50ContratadaUsuario_ContratadaPessoaNom3 ;
      private String lV10TFUsuario_Nome ;
      private String lV12TFContratadaUsuario_UsuarioPessoaNom ;
      private String lV14TFContratadaUsuario_ContratadaPessoaNom ;
      private String A2Usuario_Nome ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool returnInSub ;
      private bool AV39DynamicFiltersEnabled2 ;
      private bool AV45DynamicFiltersEnabled3 ;
      private bool BRKLC2 ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n2Usuario_Nome ;
      private bool n68ContratadaUsuario_ContratadaPessoaNom ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool BRKLC4 ;
      private bool BRKLC6 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV40DynamicFiltersSelector2 ;
      private String AV46DynamicFiltersSelector3 ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00LC2_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00LC2_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] P00LC2_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] P00LC2_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00LC2_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] P00LC2_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] P00LC2_A2Usuario_Nome ;
      private bool[] P00LC2_n2Usuario_Nome ;
      private String[] P00LC2_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] P00LC2_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] P00LC2_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P00LC2_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] P00LC3_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00LC3_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] P00LC3_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] P00LC3_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00LC3_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] P00LC3_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] P00LC3_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P00LC3_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] P00LC3_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] P00LC3_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] P00LC3_A2Usuario_Nome ;
      private bool[] P00LC3_n2Usuario_Nome ;
      private int[] P00LC4_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00LC4_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] P00LC4_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] P00LC4_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00LC4_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] P00LC4_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] P00LC4_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] P00LC4_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] P00LC4_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P00LC4_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] P00LC4_A2Usuario_Nome ;
      private bool[] P00LC4_n2Usuario_Nome ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getpromptcontratadausuariofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00LC2( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV36Usuario_Nome1 ,
                                             String AV37ContratadaUsuario_UsuarioPessoaNom1 ,
                                             String AV38ContratadaUsuario_ContratadaPessoaNom1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             short AV41DynamicFiltersOperator2 ,
                                             String AV42Usuario_Nome2 ,
                                             String AV43ContratadaUsuario_UsuarioPessoaNom2 ,
                                             String AV44ContratadaUsuario_ContratadaPessoaNom2 ,
                                             bool AV45DynamicFiltersEnabled3 ,
                                             String AV46DynamicFiltersSelector3 ,
                                             short AV47DynamicFiltersOperator3 ,
                                             String AV48Usuario_Nome3 ,
                                             String AV49ContratadaUsuario_UsuarioPessoaNom3 ,
                                             String AV50ContratadaUsuario_ContratadaPessoaNom3 ,
                                             String AV11TFUsuario_Nome_Sel ,
                                             String AV10TFUsuario_Nome ,
                                             String AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                             String AV12TFContratadaUsuario_UsuarioPessoaNom ,
                                             String AV15TFContratadaUsuario_ContratadaPessoaNom_Sel ,
                                             String AV14TFContratadaUsuario_ContratadaPessoaNom ,
                                             String A2Usuario_Nome ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String A68ContratadaUsuario_ContratadaPessoaNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [24] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T4.[Usuario_Nome], T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, T5.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso FROM (((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "USUARIO_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Usuario_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV36Usuario_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV36Usuario_Nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "USUARIO_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Usuario_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV36Usuario_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV36Usuario_Nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV37ContratadaUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV37ContratadaUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV37ContratadaUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV37ContratadaUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratadaUsuario_ContratadaPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV38ContratadaUsuario_ContratadaPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV38ContratadaUsuario_ContratadaPessoaNom1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratadaUsuario_ContratadaPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV38ContratadaUsuario_ContratadaPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV38ContratadaUsuario_ContratadaPessoaNom1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "USUARIO_NOME") == 0 ) && ( AV41DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Usuario_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV42Usuario_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV42Usuario_Nome2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "USUARIO_NOME") == 0 ) && ( AV41DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Usuario_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV42Usuario_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV42Usuario_Nome2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContratadaUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV43ContratadaUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV43ContratadaUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContratadaUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV43ContratadaUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV43ContratadaUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratadaUsuario_ContratadaPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV44ContratadaUsuario_ContratadaPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV44ContratadaUsuario_ContratadaPessoaNom2)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratadaUsuario_ContratadaPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV44ContratadaUsuario_ContratadaPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV44ContratadaUsuario_ContratadaPessoaNom2)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "USUARIO_NOME") == 0 ) && ( AV47DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Usuario_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV48Usuario_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV48Usuario_Nome3)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "USUARIO_NOME") == 0 ) && ( AV47DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Usuario_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV48Usuario_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV48Usuario_Nome3)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV47DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContratadaUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV49ContratadaUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV49ContratadaUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV47DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContratadaUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV49ContratadaUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV49ContratadaUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV47DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContratadaUsuario_ContratadaPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV50ContratadaUsuario_ContratadaPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV50ContratadaUsuario_ContratadaPessoaNom3)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV47DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContratadaUsuario_ContratadaPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV50ContratadaUsuario_ContratadaPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV50ContratadaUsuario_ContratadaPessoaNom3)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUsuario_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV10TFUsuario_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV10TFUsuario_Nome)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratadaUsuario_UsuarioPessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV12TFContratadaUsuario_UsuarioPessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV12TFContratadaUsuario_UsuarioPessoaNom)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratadaUsuario_ContratadaPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratadaUsuario_ContratadaPessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV14TFContratadaUsuario_ContratadaPessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV14TFContratadaUsuario_ContratadaPessoaNom)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratadaUsuario_ContratadaPessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV15TFContratadaUsuario_ContratadaPessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV15TFContratadaUsuario_ContratadaPessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Usuario_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00LC3( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV36Usuario_Nome1 ,
                                             String AV37ContratadaUsuario_UsuarioPessoaNom1 ,
                                             String AV38ContratadaUsuario_ContratadaPessoaNom1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             short AV41DynamicFiltersOperator2 ,
                                             String AV42Usuario_Nome2 ,
                                             String AV43ContratadaUsuario_UsuarioPessoaNom2 ,
                                             String AV44ContratadaUsuario_ContratadaPessoaNom2 ,
                                             bool AV45DynamicFiltersEnabled3 ,
                                             String AV46DynamicFiltersSelector3 ,
                                             short AV47DynamicFiltersOperator3 ,
                                             String AV48Usuario_Nome3 ,
                                             String AV49ContratadaUsuario_UsuarioPessoaNom3 ,
                                             String AV50ContratadaUsuario_ContratadaPessoaNom3 ,
                                             String AV11TFUsuario_Nome_Sel ,
                                             String AV10TFUsuario_Nome ,
                                             String AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                             String AV12TFContratadaUsuario_UsuarioPessoaNom ,
                                             String AV15TFContratadaUsuario_ContratadaPessoaNom_Sel ,
                                             String AV14TFContratadaUsuario_ContratadaPessoaNom ,
                                             String A2Usuario_Nome ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String A68ContratadaUsuario_ContratadaPessoaNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [24] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T5.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, T4.[Usuario_Nome] FROM (((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "USUARIO_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Usuario_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV36Usuario_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV36Usuario_Nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "USUARIO_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Usuario_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV36Usuario_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV36Usuario_Nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV37ContratadaUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV37ContratadaUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV37ContratadaUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV37ContratadaUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratadaUsuario_ContratadaPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV38ContratadaUsuario_ContratadaPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV38ContratadaUsuario_ContratadaPessoaNom1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratadaUsuario_ContratadaPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV38ContratadaUsuario_ContratadaPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV38ContratadaUsuario_ContratadaPessoaNom1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "USUARIO_NOME") == 0 ) && ( AV41DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Usuario_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV42Usuario_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV42Usuario_Nome2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "USUARIO_NOME") == 0 ) && ( AV41DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Usuario_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV42Usuario_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV42Usuario_Nome2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContratadaUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV43ContratadaUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV43ContratadaUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContratadaUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV43ContratadaUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV43ContratadaUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratadaUsuario_ContratadaPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV44ContratadaUsuario_ContratadaPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV44ContratadaUsuario_ContratadaPessoaNom2)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratadaUsuario_ContratadaPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV44ContratadaUsuario_ContratadaPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV44ContratadaUsuario_ContratadaPessoaNom2)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "USUARIO_NOME") == 0 ) && ( AV47DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Usuario_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV48Usuario_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV48Usuario_Nome3)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "USUARIO_NOME") == 0 ) && ( AV47DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Usuario_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV48Usuario_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV48Usuario_Nome3)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV47DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContratadaUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV49ContratadaUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV49ContratadaUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV47DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContratadaUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV49ContratadaUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV49ContratadaUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV47DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContratadaUsuario_ContratadaPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV50ContratadaUsuario_ContratadaPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV50ContratadaUsuario_ContratadaPessoaNom3)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV47DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContratadaUsuario_ContratadaPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV50ContratadaUsuario_ContratadaPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV50ContratadaUsuario_ContratadaPessoaNom3)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUsuario_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV10TFUsuario_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV10TFUsuario_Nome)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratadaUsuario_UsuarioPessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV12TFContratadaUsuario_UsuarioPessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV12TFContratadaUsuario_UsuarioPessoaNom)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratadaUsuario_ContratadaPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratadaUsuario_ContratadaPessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV14TFContratadaUsuario_ContratadaPessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV14TFContratadaUsuario_ContratadaPessoaNom)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratadaUsuario_ContratadaPessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV15TFContratadaUsuario_ContratadaPessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV15TFContratadaUsuario_ContratadaPessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T5.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00LC4( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV36Usuario_Nome1 ,
                                             String AV37ContratadaUsuario_UsuarioPessoaNom1 ,
                                             String AV38ContratadaUsuario_ContratadaPessoaNom1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             short AV41DynamicFiltersOperator2 ,
                                             String AV42Usuario_Nome2 ,
                                             String AV43ContratadaUsuario_UsuarioPessoaNom2 ,
                                             String AV44ContratadaUsuario_ContratadaPessoaNom2 ,
                                             bool AV45DynamicFiltersEnabled3 ,
                                             String AV46DynamicFiltersSelector3 ,
                                             short AV47DynamicFiltersOperator3 ,
                                             String AV48Usuario_Nome3 ,
                                             String AV49ContratadaUsuario_UsuarioPessoaNom3 ,
                                             String AV50ContratadaUsuario_ContratadaPessoaNom3 ,
                                             String AV11TFUsuario_Nome_Sel ,
                                             String AV10TFUsuario_Nome ,
                                             String AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                             String AV12TFContratadaUsuario_UsuarioPessoaNom ,
                                             String AV15TFContratadaUsuario_ContratadaPessoaNom_Sel ,
                                             String AV14TFContratadaUsuario_ContratadaPessoaNom ,
                                             String A2Usuario_Nome ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String A68ContratadaUsuario_ContratadaPessoaNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [24] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, T5.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T4.[Usuario_Nome] FROM (((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "USUARIO_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Usuario_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV36Usuario_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV36Usuario_Nome1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "USUARIO_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Usuario_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV36Usuario_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV36Usuario_Nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV37ContratadaUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV37ContratadaUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV37ContratadaUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV37ContratadaUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratadaUsuario_ContratadaPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV38ContratadaUsuario_ContratadaPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV38ContratadaUsuario_ContratadaPessoaNom1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratadaUsuario_ContratadaPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV38ContratadaUsuario_ContratadaPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV38ContratadaUsuario_ContratadaPessoaNom1)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "USUARIO_NOME") == 0 ) && ( AV41DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Usuario_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV42Usuario_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV42Usuario_Nome2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "USUARIO_NOME") == 0 ) && ( AV41DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Usuario_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV42Usuario_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV42Usuario_Nome2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContratadaUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV43ContratadaUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV43ContratadaUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContratadaUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV43ContratadaUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV43ContratadaUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratadaUsuario_ContratadaPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV44ContratadaUsuario_ContratadaPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV44ContratadaUsuario_ContratadaPessoaNom2)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratadaUsuario_ContratadaPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV44ContratadaUsuario_ContratadaPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV44ContratadaUsuario_ContratadaPessoaNom2)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "USUARIO_NOME") == 0 ) && ( AV47DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Usuario_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV48Usuario_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV48Usuario_Nome3)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "USUARIO_NOME") == 0 ) && ( AV47DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Usuario_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV48Usuario_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV48Usuario_Nome3)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV47DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContratadaUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV49ContratadaUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV49ContratadaUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV47DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContratadaUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV49ContratadaUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV49ContratadaUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV47DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContratadaUsuario_ContratadaPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV50ContratadaUsuario_ContratadaPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV50ContratadaUsuario_ContratadaPessoaNom3)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV47DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContratadaUsuario_ContratadaPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV50ContratadaUsuario_ContratadaPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV50ContratadaUsuario_ContratadaPessoaNom3)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUsuario_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV10TFUsuario_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV10TFUsuario_Nome)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratadaUsuario_UsuarioPessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV12TFContratadaUsuario_UsuarioPessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV12TFContratadaUsuario_UsuarioPessoaNom)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratadaUsuario_ContratadaPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratadaUsuario_ContratadaPessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV14TFContratadaUsuario_ContratadaPessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV14TFContratadaUsuario_ContratadaPessoaNom)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratadaUsuario_ContratadaPessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV15TFContratadaUsuario_ContratadaPessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV15TFContratadaUsuario_ContratadaPessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00LC2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
               case 1 :
                     return conditional_P00LC3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
               case 2 :
                     return conditional_P00LC4(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00LC2 ;
          prmP00LC2 = new Object[] {
          new Object[] {"@lV36Usuario_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV36Usuario_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV37ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV37ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV38ContratadaUsuario_ContratadaPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV38ContratadaUsuario_ContratadaPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV42Usuario_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Usuario_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV43ContratadaUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV43ContratadaUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV44ContratadaUsuario_ContratadaPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV44ContratadaUsuario_ContratadaPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV48Usuario_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48Usuario_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49ContratadaUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV49ContratadaUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV50ContratadaUsuario_ContratadaPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV50ContratadaUsuario_ContratadaPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV10TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFContratadaUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContratadaUsuario_UsuarioPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV14TFContratadaUsuario_ContratadaPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV15TFContratadaUsuario_ContratadaPessoaNom_Sel",SqlDbType.Char,100,0}
          } ;
          Object[] prmP00LC3 ;
          prmP00LC3 = new Object[] {
          new Object[] {"@lV36Usuario_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV36Usuario_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV37ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV37ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV38ContratadaUsuario_ContratadaPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV38ContratadaUsuario_ContratadaPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV42Usuario_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Usuario_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV43ContratadaUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV43ContratadaUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV44ContratadaUsuario_ContratadaPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV44ContratadaUsuario_ContratadaPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV48Usuario_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48Usuario_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49ContratadaUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV49ContratadaUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV50ContratadaUsuario_ContratadaPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV50ContratadaUsuario_ContratadaPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV10TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFContratadaUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContratadaUsuario_UsuarioPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV14TFContratadaUsuario_ContratadaPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV15TFContratadaUsuario_ContratadaPessoaNom_Sel",SqlDbType.Char,100,0}
          } ;
          Object[] prmP00LC4 ;
          prmP00LC4 = new Object[] {
          new Object[] {"@lV36Usuario_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV36Usuario_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV37ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV37ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV38ContratadaUsuario_ContratadaPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV38ContratadaUsuario_ContratadaPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV42Usuario_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Usuario_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV43ContratadaUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV43ContratadaUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV44ContratadaUsuario_ContratadaPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV44ContratadaUsuario_ContratadaPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV48Usuario_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48Usuario_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49ContratadaUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV49ContratadaUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV50ContratadaUsuario_ContratadaPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV50ContratadaUsuario_ContratadaPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV10TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFContratadaUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContratadaUsuario_UsuarioPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV14TFContratadaUsuario_ContratadaPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV15TFContratadaUsuario_ContratadaPessoaNom_Sel",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00LC2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LC2,100,0,true,false )
             ,new CursorDef("P00LC3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LC3,100,0,true,false )
             ,new CursorDef("P00LC4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LC4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratadausuariofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratadausuariofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratadausuariofilterdata") )
          {
             return  ;
          }
          getpromptcontratadausuariofilterdata worker = new getpromptcontratadausuariofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
