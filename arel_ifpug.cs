/*
               File: REL_IFPUG
        Description: IFPUG
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:28:15.51
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_ifpug : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV30User_Nome = gxfirstwebparm;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV19Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV9Area = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV16Colaborador = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV28Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV8Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV23Mes = (long)(NumberUtil.Val( GetNextPar( ), "."));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_ifpug( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_ifpug( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_User_Nome ,
                           int aP1_Contratada_Codigo ,
                           int aP2_Area ,
                           int aP3_Colaborador ,
                           int aP4_Servico ,
                           short aP5_Ano ,
                           long aP6_Mes )
      {
         this.AV30User_Nome = aP0_User_Nome;
         this.AV19Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV9Area = aP2_Area;
         this.AV16Colaborador = aP3_Colaborador;
         this.AV28Servico = aP4_Servico;
         this.AV8Ano = aP5_Ano;
         this.AV23Mes = aP6_Mes;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_User_Nome ,
                                 int aP1_Contratada_Codigo ,
                                 int aP2_Area ,
                                 int aP3_Colaborador ,
                                 int aP4_Servico ,
                                 short aP5_Ano ,
                                 long aP6_Mes )
      {
         arel_ifpug objarel_ifpug;
         objarel_ifpug = new arel_ifpug();
         objarel_ifpug.AV30User_Nome = aP0_User_Nome;
         objarel_ifpug.AV19Contratada_Codigo = aP1_Contratada_Codigo;
         objarel_ifpug.AV9Area = aP2_Area;
         objarel_ifpug.AV16Colaborador = aP3_Colaborador;
         objarel_ifpug.AV28Servico = aP4_Servico;
         objarel_ifpug.AV8Ano = aP5_Ano;
         objarel_ifpug.AV23Mes = aP6_Mes;
         objarel_ifpug.context.SetSubmitInitialConfig(context);
         objarel_ifpug.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_ifpug);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_ifpug)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            AV68SDT_Codigos.FromXml(AV31WebSession.Get("Codigos"), "SDT_CodigosCollection");
            AV73GXV1 = 1;
            while ( AV73GXV1 <= AV68SDT_Codigos.Count )
            {
               AV69Codigo = ((SdtSDT_Codigos)AV68SDT_Codigos.Item(AV73GXV1));
               AV15Codigos.Add(AV69Codigo.gxTpr_Codigo, 0);
               AV73GXV1 = (int)(AV73GXV1+1);
            }
            /* Execute user subroutine: 'FILTROS' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Using cursor P00VW2 */
            pr_default.execute(0, new Object[] {AV19Contratada_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A40Contratada_PessoaCod = P00VW2_A40Contratada_PessoaCod[0];
               A503Pessoa_MunicipioCod = P00VW2_A503Pessoa_MunicipioCod[0];
               n503Pessoa_MunicipioCod = P00VW2_n503Pessoa_MunicipioCod[0];
               A39Contratada_Codigo = P00VW2_A39Contratada_Codigo[0];
               A41Contratada_PessoaNom = P00VW2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P00VW2_n41Contratada_PessoaNom[0];
               A519Pessoa_Endereco = P00VW2_A519Pessoa_Endereco[0];
               n519Pessoa_Endereco = P00VW2_n519Pessoa_Endereco[0];
               A26Municipio_Nome = P00VW2_A26Municipio_Nome[0];
               n26Municipio_Nome = P00VW2_n26Municipio_Nome[0];
               A520Pessoa_UF = P00VW2_A520Pessoa_UF[0];
               n520Pessoa_UF = P00VW2_n520Pessoa_UF[0];
               A521Pessoa_CEP = P00VW2_A521Pessoa_CEP[0];
               n521Pessoa_CEP = P00VW2_n521Pessoa_CEP[0];
               A522Pessoa_Telefone = P00VW2_A522Pessoa_Telefone[0];
               n522Pessoa_Telefone = P00VW2_n522Pessoa_Telefone[0];
               A503Pessoa_MunicipioCod = P00VW2_A503Pessoa_MunicipioCod[0];
               n503Pessoa_MunicipioCod = P00VW2_n503Pessoa_MunicipioCod[0];
               A41Contratada_PessoaNom = P00VW2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P00VW2_n41Contratada_PessoaNom[0];
               A519Pessoa_Endereco = P00VW2_A519Pessoa_Endereco[0];
               n519Pessoa_Endereco = P00VW2_n519Pessoa_Endereco[0];
               A521Pessoa_CEP = P00VW2_A521Pessoa_CEP[0];
               n521Pessoa_CEP = P00VW2_n521Pessoa_CEP[0];
               A522Pessoa_Telefone = P00VW2_A522Pessoa_Telefone[0];
               n522Pessoa_Telefone = P00VW2_n522Pessoa_Telefone[0];
               A26Municipio_Nome = P00VW2_A26Municipio_Nome[0];
               n26Municipio_Nome = P00VW2_n26Municipio_Nome[0];
               A520Pessoa_UF = P00VW2_A520Pessoa_UF[0];
               n520Pessoa_UF = P00VW2_n520Pessoa_UF[0];
               AV33Contratada_PessoaNom = A41Contratada_PessoaNom;
               AV34Pessoa_Endereco = A519Pessoa_Endereco;
               AV38Municipio_Nome = A26Municipio_Nome;
               AV36Pessoa_UF = A520Pessoa_UF;
               AV39Pessoa_CEP = A521Pessoa_CEP;
               AV40Pessoa_Telefone = A522Pessoa_Telefone;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 A456ContagemResultado_Codigo ,
                                                 AV15Codigos },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P00VW3 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               A490ContagemResultado_ContratadaCod = P00VW3_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00VW3_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = P00VW3_A456ContagemResultado_Codigo[0];
               A52Contratada_AreaTrabalhoCod = P00VW3_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00VW3_n52Contratada_AreaTrabalhoCod[0];
               A52Contratada_AreaTrabalhoCod = P00VW3_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00VW3_n52Contratada_AreaTrabalhoCod[0];
               AV11Area_Codigos.Add(A52Contratada_AreaTrabalhoCod, 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 A456ContagemResultado_Codigo ,
                                                 AV15Codigos },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P00VW4 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P00VW4_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00VW4_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = P00VW4_A456ContagemResultado_Codigo[0];
               A1603ContagemResultado_CntCod = P00VW4_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00VW4_n1603ContagemResultado_CntCod[0];
               A1603ContagemResultado_CntCod = P00VW4_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00VW4_n1603ContagemResultado_CntCod[0];
               AV61Contrato_Codigos.Add(A1603ContagemResultado_CntCod, 0);
               pr_default.readNext(2);
            }
            pr_default.close(2);
            HVW0( false, 950) ;
            getPrinter().GxDrawLine(167, Gx_line+667, 500, Gx_line+667, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(642, Gx_line+667, 767, Gx_line+667, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(650, Gx_line+767, 775, Gx_line+767, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(650, Gx_line+800, 775, Gx_line+800, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(33, Gx_line+771, 43, Gx_line+781, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(33, Gx_line+804, 43, Gx_line+814, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(125, Gx_line+804, 135, Gx_line+814, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(225, Gx_line+804, 235, Gx_line+814, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(283, Gx_line+804, 293, Gx_line+814, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(150, Gx_line+850, 483, Gx_line+850, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(150, Gx_line+883, 483, Gx_line+883, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(183, Gx_line+917, 483, Gx_line+917, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(25, Gx_line+700, 808, Gx_line+933, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(600, Gx_line+17, 783, Gx_line+67, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("FOR OFFICE USE ONLY", 600, Gx_line+0, 758, Gx_line+18, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("IFPUG CERTIFIED FUNCTION POINT SPECIALIST", 133, Gx_line+74, 683, Gx_line+102, 1, 0, 0, 0) ;
            getPrinter().GxDrawText("CERTIFICATION EXTENSION APPLICATION", 133, Gx_line+100, 683, Gx_line+128, 1, 0, 0, 2) ;
            getPrinter().GxDrawText("INSTRUCTIONS:", 25, Gx_line+133, 183, Gx_line+161, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Date Received:", 608, Gx_line+17, 695, Gx_line+33, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Date Approved:", 608, Gx_line+50, 695, Gx_line+66, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("1.", 25, Gx_line+167, 42, Gx_line+185, 3, 0, 0, 0) ;
            getPrinter().GxDrawText("Type or print entire application clearly.", 50, Gx_line+167, 286, Gx_line+185, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Additional Certification Extension Activity Credit Documentation pages may be attached to the application as needed.", 50, Gx_line+183, 769, Gx_line+201, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("2.", 25, Gx_line+183, 42, Gx_line+201, 3, 0, 0, 0) ;
            getPrinter().GxDrawText("3.", 25, Gx_line+200, 42, Gx_line+218, 3, 0, 0, 0) ;
            getPrinter().GxDrawText("Send application and Certification Extension Activity Credit Documentation in its entirety, along with fee to IFPUG, 191 Clarksville Road, Princeton Junction, NJ 08550 USA Phone: 609/799-4900 Fax:609/799-7032", 50, Gx_line+200, 708, Gx_line+253, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Note: As of July 1, 2003 all Certified Function Point Specialists must be IFPUG Members.", 25, Gx_line+250, 564, Gx_line+268, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Name:", 25, Gx_line+283, 68, Gx_line+301, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Date:", 450, Gx_line+283, 484, Gx_line+301, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Company Name:", 25, Gx_line+317, 131, Gx_line+335, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Mailing Address:", 25, Gx_line+350, 130, Gx_line+368, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("City/Province:", 25, Gx_line+383, 111, Gx_line+401, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Zip/Postal Code:", 484, Gx_line+383, 589, Gx_line+401, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Country:   BRASIL", 25, Gx_line+417, 134, Gx_line+435, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Daytime Telephone Number:", 258, Gx_line+417, 439, Gx_line+435, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("State:", 367, Gx_line+383, 403, Gx_line+401, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Email address:", 25, Gx_line+450, 120, Gx_line+468, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("CFPS Expiration Date:", 475, Gx_line+450, 614, Gx_line+468, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("In signing below, I certify that:", 25, Gx_line+517, 202, Gx_line+535, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("I understand that falsification of any kind may be sufficient for rejection or withdrawal of certification and forfeiture of all fees.", 83, Gx_line+567, 808, Gx_line+600, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("3.", 58, Gx_line+567, 75, Gx_line+585, 3, 0, 0, 0) ;
            getPrinter().GxDrawText("2.", 58, Gx_line+550, 75, Gx_line+568, 3, 0, 0, 0) ;
            getPrinter().GxDrawText("All of the information on this application is accurate to the best of my knowledge.", 83, Gx_line+550, 570, Gx_line+568, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("I have read CFPS Certification Extension Program document and understand the program�s requirements.", 83, Gx_line+533, 730, Gx_line+551, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("1.", 58, Gx_line+533, 75, Gx_line+551, 3, 0, 0, 0) ;
            getPrinter().GxDrawText("I have read and agree to abide by and uphold the Certified Function Point Specialist Code of Ethics.", 83, Gx_line+600, 693, Gx_line+618, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("4.", 58, Gx_line+600, 75, Gx_line+618, 3, 0, 0, 0) ;
            getPrinter().GxDrawText("Applicant�s Signature:", 33, Gx_line+650, 168, Gx_line+668, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Date:", 600, Gx_line+650, 634, Gx_line+668, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Fees: $100 per documented Certification Extension Activity or Activity Occurrence", 42, Gx_line+700, 539, Gx_line+718, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("(Application accepted by fax only when paying by credit card.)", 192, Gx_line+733, 574, Gx_line+751, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Mbr #", 608, Gx_line+750, 643, Gx_line+768, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Application Fee enclosed.", 50, Gx_line+767, 214, Gx_line+785, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Make checks or money", 467, Gx_line+767, 613, Gx_line+785, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("orders payable to IFPUG in U.S. funds only and drawn on a U.S. bank.", 50, Gx_line+783, 479, Gx_line+801, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Affiliate Type", 558, Gx_line+783, 640, Gx_line+801, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Charge To:", 50, Gx_line+800, 121, Gx_line+818, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("MasterCard", 142, Gx_line+800, 216, Gx_line+818, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("VISA", 242, Gx_line+800, 274, Gx_line+818, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("American Express", 300, Gx_line+800, 415, Gx_line+818, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Account Number", 33, Gx_line+833, 137, Gx_line+851, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Expiration Date", 33, Gx_line+867, 129, Gx_line+885, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Authorization Signature", 33, Gx_line+900, 176, Gx_line+918, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Payments to IFPUG are not deductible as charitable contributions for Federal Income Tax purposes. However, they may be deductible under other provisions of the Internal Revenue Code", 508, Gx_line+833, 806, Gx_line+933, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV17Colaborador_Nome, "@!")), 75, Gx_line+283, 389, Gx_line+301, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 492, Gx_line+283, 545, Gx_line+301, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV33Contratada_PessoaNom, "@!")), 142, Gx_line+317, 768, Gx_line+335, 0+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV34Pessoa_Endereco, "")), 133, Gx_line+350, 759, Gx_line+368, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV38Municipio_Nome, "@!")), 117, Gx_line+383, 359, Gx_line+401, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV36Pessoa_UF, "@!")), 408, Gx_line+383, 436, Gx_line+401, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV39Pessoa_CEP, "")), 592, Gx_line+383, 656, Gx_line+401, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV40Pessoa_Telefone, "")), 450, Gx_line+417, 545, Gx_line+435, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV41Usuario_Email, "")), 125, Gx_line+450, 467, Gx_line+468, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV57CFPS_Validade, "99/99/99"), 617, Gx_line+450, 670, Gx_line+468, 2+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Method of Payment:", 42, Gx_line+733, 185, Gx_line+751, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Purchase Orders are not accepted.", 217, Gx_line+767, 469, Gx_line+785, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+950);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 A5AreaTrabalho_Codigo ,
                                                 AV11Area_Codigos },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P00VW5 */
            pr_default.execute(3);
            while ( (pr_default.getStatus(3) != 101) )
            {
               A29Contratante_Codigo = P00VW5_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00VW5_n29Contratante_Codigo[0];
               A335Contratante_PessoaCod = P00VW5_A335Contratante_PessoaCod[0];
               A5AreaTrabalho_Codigo = P00VW5_A5AreaTrabalho_Codigo[0];
               A9Contratante_RazaoSocial = P00VW5_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = P00VW5_n9Contratante_RazaoSocial[0];
               A12Contratante_CNPJ = P00VW5_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = P00VW5_n12Contratante_CNPJ[0];
               A6AreaTrabalho_Descricao = P00VW5_A6AreaTrabalho_Descricao[0];
               A335Contratante_PessoaCod = P00VW5_A335Contratante_PessoaCod[0];
               A9Contratante_RazaoSocial = P00VW5_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = P00VW5_n9Contratante_RazaoSocial[0];
               A12Contratante_CNPJ = P00VW5_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = P00VW5_n12Contratante_CNPJ[0];
               AV55AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
               AV13AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
               AV47Contratante_RazaoSocial = A9Contratante_RazaoSocial;
               AV48CNPJ = A12Contratante_CNPJ;
               pr_default.dynParam(4, new Object[]{ new Object[]{
                                                    A1603ContagemResultado_CntCod ,
                                                    AV61Contrato_Codigos ,
                                                    A456ContagemResultado_Codigo ,
                                                    AV15Codigos ,
                                                    A598ContagemResultado_Baseline ,
                                                    A517ContagemResultado_Ultima ,
                                                    A5AreaTrabalho_Codigo ,
                                                    A52Contratada_AreaTrabalhoCod },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                    }
               });
               /* Using cursor P00VW6 */
               pr_default.execute(4, new Object[] {A5AreaTrabalho_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  BRKVW7 = false;
                  A490ContagemResultado_ContratadaCod = P00VW6_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00VW6_n490ContagemResultado_ContratadaCod[0];
                  A1553ContagemResultado_CntSrvCod = P00VW6_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00VW6_n1553ContagemResultado_CntSrvCod[0];
                  A1603ContagemResultado_CntCod = P00VW6_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P00VW6_n1603ContagemResultado_CntCod[0];
                  A52Contratada_AreaTrabalhoCod = P00VW6_A52Contratada_AreaTrabalhoCod[0];
                  n52Contratada_AreaTrabalhoCod = P00VW6_n52Contratada_AreaTrabalhoCod[0];
                  A1326ContagemResultado_ContratadaTipoFab = P00VW6_A1326ContagemResultado_ContratadaTipoFab[0];
                  n1326ContagemResultado_ContratadaTipoFab = P00VW6_n1326ContagemResultado_ContratadaTipoFab[0];
                  A460ContagemResultado_PFBFM = P00VW6_A460ContagemResultado_PFBFM[0];
                  n460ContagemResultado_PFBFM = P00VW6_n460ContagemResultado_PFBFM[0];
                  A458ContagemResultado_PFBFS = P00VW6_A458ContagemResultado_PFBFS[0];
                  n458ContagemResultado_PFBFS = P00VW6_n458ContagemResultado_PFBFS[0];
                  A456ContagemResultado_Codigo = P00VW6_A456ContagemResultado_Codigo[0];
                  A517ContagemResultado_Ultima = P00VW6_A517ContagemResultado_Ultima[0];
                  A598ContagemResultado_Baseline = P00VW6_A598ContagemResultado_Baseline[0];
                  n598ContagemResultado_Baseline = P00VW6_n598ContagemResultado_Baseline[0];
                  A1612ContagemResultado_CntNum = P00VW6_A1612ContagemResultado_CntNum[0];
                  n1612ContagemResultado_CntNum = P00VW6_n1612ContagemResultado_CntNum[0];
                  A473ContagemResultado_DataCnt = P00VW6_A473ContagemResultado_DataCnt[0];
                  A511ContagemResultado_HoraCnt = P00VW6_A511ContagemResultado_HoraCnt[0];
                  A490ContagemResultado_ContratadaCod = P00VW6_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00VW6_n490ContagemResultado_ContratadaCod[0];
                  A1553ContagemResultado_CntSrvCod = P00VW6_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00VW6_n1553ContagemResultado_CntSrvCod[0];
                  A598ContagemResultado_Baseline = P00VW6_A598ContagemResultado_Baseline[0];
                  n598ContagemResultado_Baseline = P00VW6_n598ContagemResultado_Baseline[0];
                  A52Contratada_AreaTrabalhoCod = P00VW6_A52Contratada_AreaTrabalhoCod[0];
                  n52Contratada_AreaTrabalhoCod = P00VW6_n52Contratada_AreaTrabalhoCod[0];
                  A1326ContagemResultado_ContratadaTipoFab = P00VW6_A1326ContagemResultado_ContratadaTipoFab[0];
                  n1326ContagemResultado_ContratadaTipoFab = P00VW6_n1326ContagemResultado_ContratadaTipoFab[0];
                  A1603ContagemResultado_CntCod = P00VW6_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P00VW6_n1603ContagemResultado_CntCod[0];
                  A1612ContagemResultado_CntNum = P00VW6_A1612ContagemResultado_CntNum[0];
                  n1612ContagemResultado_CntNum = P00VW6_n1612ContagemResultado_CntNum[0];
                  W52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
                  n52Contratada_AreaTrabalhoCod = false;
                  AV60Contrato_Codigo = A1603ContagemResultado_CntCod;
                  AV59Contrato_Numero = A1612ContagemResultado_CntNum;
                  /* Execute user subroutine: 'GESTORES' */
                  S121 ();
                  if ( returnInSub )
                  {
                     pr_default.close(4);
                     this.cleanup();
                     if (true) return;
                  }
                  AV46Total = 0;
                  while ( (pr_default.getStatus(4) != 101) && ( P00VW6_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( P00VW6_A1603ContagemResultado_CntCod[0] == A1603ContagemResultado_CntCod ) )
                  {
                     BRKVW7 = false;
                     A490ContagemResultado_ContratadaCod = P00VW6_A490ContagemResultado_ContratadaCod[0];
                     n490ContagemResultado_ContratadaCod = P00VW6_n490ContagemResultado_ContratadaCod[0];
                     A1553ContagemResultado_CntSrvCod = P00VW6_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = P00VW6_n1553ContagemResultado_CntSrvCod[0];
                     A1326ContagemResultado_ContratadaTipoFab = P00VW6_A1326ContagemResultado_ContratadaTipoFab[0];
                     n1326ContagemResultado_ContratadaTipoFab = P00VW6_n1326ContagemResultado_ContratadaTipoFab[0];
                     A460ContagemResultado_PFBFM = P00VW6_A460ContagemResultado_PFBFM[0];
                     n460ContagemResultado_PFBFM = P00VW6_n460ContagemResultado_PFBFM[0];
                     A458ContagemResultado_PFBFS = P00VW6_A458ContagemResultado_PFBFS[0];
                     n458ContagemResultado_PFBFS = P00VW6_n458ContagemResultado_PFBFS[0];
                     A456ContagemResultado_Codigo = P00VW6_A456ContagemResultado_Codigo[0];
                     A473ContagemResultado_DataCnt = P00VW6_A473ContagemResultado_DataCnt[0];
                     A511ContagemResultado_HoraCnt = P00VW6_A511ContagemResultado_HoraCnt[0];
                     A490ContagemResultado_ContratadaCod = P00VW6_A490ContagemResultado_ContratadaCod[0];
                     n490ContagemResultado_ContratadaCod = P00VW6_n490ContagemResultado_ContratadaCod[0];
                     A1553ContagemResultado_CntSrvCod = P00VW6_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = P00VW6_n1553ContagemResultado_CntSrvCod[0];
                     A1326ContagemResultado_ContratadaTipoFab = P00VW6_A1326ContagemResultado_ContratadaTipoFab[0];
                     n1326ContagemResultado_ContratadaTipoFab = P00VW6_n1326ContagemResultado_ContratadaTipoFab[0];
                     if ( (AV61Contrato_Codigos.IndexOf(A1603ContagemResultado_CntCod)>0) )
                     {
                        if ( (AV15Codigos.IndexOf(A456ContagemResultado_Codigo)>0) )
                        {
                           if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 )
                           {
                              AV46Total = (decimal)(AV46Total+A460ContagemResultado_PFBFM);
                           }
                           else if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S") == 0 )
                           {
                              AV46Total = (decimal)(AV46Total+A458ContagemResultado_PFBFS);
                           }
                        }
                     }
                     BRKVW7 = true;
                     pr_default.readNext(4);
                  }
                  if ( ! AV56PaginaImpressa )
                  {
                     AV56PaginaImpressa = true;
                     HVW0( false, 125) ;
                     getPrinter().GxDrawLine(133, Gx_line+67, 516, Gx_line+67, 1, 0, 0, 0, 0) ;
                     getPrinter().GxDrawLine(700, Gx_line+67, 792, Gx_line+67, 1, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("List CEA eligible FP Count(s) that were performed by the applicant Minimum 2000 UFP Maximum 6000 UFP", 42, Gx_line+83, 759, Gx_line+99, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("Attach FP Count Summary Form(s)", 25, Gx_line+100, 254, Gx_line+116, 0+256, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Applicant Name", 25, Gx_line+50, 126, Gx_line+68, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("Date", 658, Gx_line+50, 689, Gx_line+68, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("6.", 25, Gx_line+83, 42, Gx_line+101, 3, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 12, true, true, true, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("IFPUG CERTIFIED FUNCTION POINT SPECIALIST", 100, Gx_line+0, 725, Gx_line+17, 1, 0, 0, 0) ;
                     getPrinter().GxDrawText("CERTIFICATION EXTENSION ACTIVITY CREDIT DOCUMENTATION", 100, Gx_line+17, 725, Gx_line+45, 1, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+125);
                     HVW0( false, 59) ;
                     getPrinter().GxDrawRect(33, Gx_line+9, 800, Gx_line+59, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxDrawRect(33, Gx_line+9, 175, Gx_line+59, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxDrawRect(325, Gx_line+9, 800, Gx_line+59, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Date(s) of FP Count", 50, Gx_line+17, 125, Gx_line+50, 1+16, 0, 0, 1) ;
                     getPrinter().GxDrawText("UFP of FP Count", 208, Gx_line+17, 291, Gx_line+50, 1+16, 0, 0, 1) ;
                     getPrinter().GxDrawText("Client/Manager Name", 492, Gx_line+17, 626, Gx_line+50, 1+16, 0, 0, 1) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+59);
                  }
                  HVW0( false, 200) ;
                  getPrinter().GxDrawRect(33, Gx_line+0, 800, Gx_line+200, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxDrawRect(325, Gx_line+0, 800, Gx_line+200, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxDrawRect(33, Gx_line+0, 175, Gx_line+200, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV51Gestor_Email, "")), 442, Gx_line+100, 784, Gx_line+115, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54Fiscal_Email, "")), 442, Gx_line+167, 784, Gx_line+182, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV52Gestor_Telefone, "")), 442, Gx_line+117, 547, Gx_line+132, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV53Fiscal_Telefone, "")), 442, Gx_line+183, 547, Gx_line+198, 0+256, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV45Periodo, "")), 42, Gx_line+67, 168, Gx_line+85, 1+256, 0, 0, 1) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV46Total, "ZZ,ZZZ,ZZ9.999")), 183, Gx_line+67, 316, Gx_line+85, 1, 0, 0, 1) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV47Contratante_RazaoSocial, "@!")), 442, Gx_line+17, 784, Gx_line+35, 0, 0, 0, 1) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV48CNPJ, "")), 558, Gx_line+33, 647, Gx_line+51, 0+256, 0, 0, 1) ;
                  getPrinter().GxDrawText("Manager:", 350, Gx_line+83, 410, Gx_line+101, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Supervisor:", 350, Gx_line+150, 422, Gx_line+168, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Client:", 350, Gx_line+17, 389, Gx_line+35, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV49Gestor_Nome, "@!")), 442, Gx_line+83, 784, Gx_line+101, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV50Fiscal_Nome, "@!")), 442, Gx_line+150, 784, Gx_line+168, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Agreement N�:", 442, Gx_line+50, 534, Gx_line+68, 0+256, 0, 0, 1) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV59Contrato_Numero, "")), 558, Gx_line+50, 684, Gx_line+68, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("National registry:", 442, Gx_line+33, 547, Gx_line+51, 0+256, 0, 0, 1) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+200);
                  A52Contratada_AreaTrabalhoCod = W52Contratada_AreaTrabalhoCod;
                  n52Contratada_AreaTrabalhoCod = false;
                  if ( ! BRKVW7 )
                  {
                     BRKVW7 = true;
                     pr_default.readNext(4);
                  }
               }
               pr_default.close(4);
               pr_default.readNext(3);
            }
            pr_default.close(3);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            AV56PaginaImpressa = false;
            pr_default.dynParam(5, new Object[]{ new Object[]{
                                                 A5AreaTrabalho_Codigo ,
                                                 AV11Area_Codigos },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P00VW7 */
            pr_default.execute(5);
            while ( (pr_default.getStatus(5) != 101) )
            {
               A29Contratante_Codigo = P00VW7_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00VW7_n29Contratante_Codigo[0];
               A335Contratante_PessoaCod = P00VW7_A335Contratante_PessoaCod[0];
               A5AreaTrabalho_Codigo = P00VW7_A5AreaTrabalho_Codigo[0];
               A9Contratante_RazaoSocial = P00VW7_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = P00VW7_n9Contratante_RazaoSocial[0];
               A12Contratante_CNPJ = P00VW7_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = P00VW7_n12Contratante_CNPJ[0];
               A6AreaTrabalho_Descricao = P00VW7_A6AreaTrabalho_Descricao[0];
               A335Contratante_PessoaCod = P00VW7_A335Contratante_PessoaCod[0];
               A9Contratante_RazaoSocial = P00VW7_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = P00VW7_n9Contratante_RazaoSocial[0];
               A12Contratante_CNPJ = P00VW7_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = P00VW7_n12Contratante_CNPJ[0];
               AV55AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
               AV13AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
               AV47Contratante_RazaoSocial = A9Contratante_RazaoSocial;
               AV48CNPJ = A12Contratante_CNPJ;
               pr_default.dynParam(6, new Object[]{ new Object[]{
                                                    A1603ContagemResultado_CntCod ,
                                                    AV61Contrato_Codigos ,
                                                    A456ContagemResultado_Codigo ,
                                                    AV15Codigos ,
                                                    A598ContagemResultado_Baseline ,
                                                    A517ContagemResultado_Ultima ,
                                                    A5AreaTrabalho_Codigo ,
                                                    A52Contratada_AreaTrabalhoCod },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                    }
               });
               /* Using cursor P00VW8 */
               pr_default.execute(6, new Object[] {A5AreaTrabalho_Codigo});
               while ( (pr_default.getStatus(6) != 101) )
               {
                  BRKVW10 = false;
                  A490ContagemResultado_ContratadaCod = P00VW8_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00VW8_n490ContagemResultado_ContratadaCod[0];
                  A1553ContagemResultado_CntSrvCod = P00VW8_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00VW8_n1553ContagemResultado_CntSrvCod[0];
                  A1603ContagemResultado_CntCod = P00VW8_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P00VW8_n1603ContagemResultado_CntCod[0];
                  A52Contratada_AreaTrabalhoCod = P00VW8_A52Contratada_AreaTrabalhoCod[0];
                  n52Contratada_AreaTrabalhoCod = P00VW8_n52Contratada_AreaTrabalhoCod[0];
                  A1326ContagemResultado_ContratadaTipoFab = P00VW8_A1326ContagemResultado_ContratadaTipoFab[0];
                  n1326ContagemResultado_ContratadaTipoFab = P00VW8_n1326ContagemResultado_ContratadaTipoFab[0];
                  A460ContagemResultado_PFBFM = P00VW8_A460ContagemResultado_PFBFM[0];
                  n460ContagemResultado_PFBFM = P00VW8_n460ContagemResultado_PFBFM[0];
                  A458ContagemResultado_PFBFS = P00VW8_A458ContagemResultado_PFBFS[0];
                  n458ContagemResultado_PFBFS = P00VW8_n458ContagemResultado_PFBFS[0];
                  A456ContagemResultado_Codigo = P00VW8_A456ContagemResultado_Codigo[0];
                  A517ContagemResultado_Ultima = P00VW8_A517ContagemResultado_Ultima[0];
                  A598ContagemResultado_Baseline = P00VW8_A598ContagemResultado_Baseline[0];
                  n598ContagemResultado_Baseline = P00VW8_n598ContagemResultado_Baseline[0];
                  A1612ContagemResultado_CntNum = P00VW8_A1612ContagemResultado_CntNum[0];
                  n1612ContagemResultado_CntNum = P00VW8_n1612ContagemResultado_CntNum[0];
                  A473ContagemResultado_DataCnt = P00VW8_A473ContagemResultado_DataCnt[0];
                  A511ContagemResultado_HoraCnt = P00VW8_A511ContagemResultado_HoraCnt[0];
                  A490ContagemResultado_ContratadaCod = P00VW8_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00VW8_n490ContagemResultado_ContratadaCod[0];
                  A1553ContagemResultado_CntSrvCod = P00VW8_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00VW8_n1553ContagemResultado_CntSrvCod[0];
                  A598ContagemResultado_Baseline = P00VW8_A598ContagemResultado_Baseline[0];
                  n598ContagemResultado_Baseline = P00VW8_n598ContagemResultado_Baseline[0];
                  A52Contratada_AreaTrabalhoCod = P00VW8_A52Contratada_AreaTrabalhoCod[0];
                  n52Contratada_AreaTrabalhoCod = P00VW8_n52Contratada_AreaTrabalhoCod[0];
                  A1326ContagemResultado_ContratadaTipoFab = P00VW8_A1326ContagemResultado_ContratadaTipoFab[0];
                  n1326ContagemResultado_ContratadaTipoFab = P00VW8_n1326ContagemResultado_ContratadaTipoFab[0];
                  A1603ContagemResultado_CntCod = P00VW8_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P00VW8_n1603ContagemResultado_CntCod[0];
                  A1612ContagemResultado_CntNum = P00VW8_A1612ContagemResultado_CntNum[0];
                  n1612ContagemResultado_CntNum = P00VW8_n1612ContagemResultado_CntNum[0];
                  W52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
                  n52Contratada_AreaTrabalhoCod = false;
                  AV60Contrato_Codigo = A1603ContagemResultado_CntCod;
                  AV59Contrato_Numero = A1612ContagemResultado_CntNum;
                  /* Execute user subroutine: 'GESTORES' */
                  S121 ();
                  if ( returnInSub )
                  {
                     pr_default.close(6);
                     this.cleanup();
                     if (true) return;
                  }
                  AV46Total = 0;
                  while ( (pr_default.getStatus(6) != 101) && ( P00VW8_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( P00VW8_A1603ContagemResultado_CntCod[0] == A1603ContagemResultado_CntCod ) )
                  {
                     BRKVW10 = false;
                     A490ContagemResultado_ContratadaCod = P00VW8_A490ContagemResultado_ContratadaCod[0];
                     n490ContagemResultado_ContratadaCod = P00VW8_n490ContagemResultado_ContratadaCod[0];
                     A1553ContagemResultado_CntSrvCod = P00VW8_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = P00VW8_n1553ContagemResultado_CntSrvCod[0];
                     A1326ContagemResultado_ContratadaTipoFab = P00VW8_A1326ContagemResultado_ContratadaTipoFab[0];
                     n1326ContagemResultado_ContratadaTipoFab = P00VW8_n1326ContagemResultado_ContratadaTipoFab[0];
                     A460ContagemResultado_PFBFM = P00VW8_A460ContagemResultado_PFBFM[0];
                     n460ContagemResultado_PFBFM = P00VW8_n460ContagemResultado_PFBFM[0];
                     A458ContagemResultado_PFBFS = P00VW8_A458ContagemResultado_PFBFS[0];
                     n458ContagemResultado_PFBFS = P00VW8_n458ContagemResultado_PFBFS[0];
                     A456ContagemResultado_Codigo = P00VW8_A456ContagemResultado_Codigo[0];
                     A473ContagemResultado_DataCnt = P00VW8_A473ContagemResultado_DataCnt[0];
                     A511ContagemResultado_HoraCnt = P00VW8_A511ContagemResultado_HoraCnt[0];
                     A490ContagemResultado_ContratadaCod = P00VW8_A490ContagemResultado_ContratadaCod[0];
                     n490ContagemResultado_ContratadaCod = P00VW8_n490ContagemResultado_ContratadaCod[0];
                     A1553ContagemResultado_CntSrvCod = P00VW8_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = P00VW8_n1553ContagemResultado_CntSrvCod[0];
                     A1326ContagemResultado_ContratadaTipoFab = P00VW8_A1326ContagemResultado_ContratadaTipoFab[0];
                     n1326ContagemResultado_ContratadaTipoFab = P00VW8_n1326ContagemResultado_ContratadaTipoFab[0];
                     if ( (AV61Contrato_Codigos.IndexOf(A1603ContagemResultado_CntCod)>0) )
                     {
                        if ( (AV15Codigos.IndexOf(A456ContagemResultado_Codigo)>0) )
                        {
                           if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 )
                           {
                              AV46Total = (decimal)(AV46Total+A460ContagemResultado_PFBFM);
                           }
                           else if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S") == 0 )
                           {
                              AV46Total = (decimal)(AV46Total+A458ContagemResultado_PFBFS);
                           }
                        }
                     }
                     BRKVW10 = true;
                     pr_default.readNext(6);
                  }
                  if ( ! AV56PaginaImpressa )
                  {
                     AV56PaginaImpressa = true;
                     HVW0( false, 125) ;
                     getPrinter().GxDrawLine(142, Gx_line+67, 525, Gx_line+67, 1, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Attach FP Count Summary Form(s)", 33, Gx_line+100, 262, Gx_line+116, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("List CEA eligible FP count(s) validated by the applicant  - Minimum 10 and Maximum of 30", 50, Gx_line+83, 652, Gx_line+99, 0+256, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("7.", 33, Gx_line+83, 50, Gx_line+101, 3, 0, 0, 0) ;
                     getPrinter().GxDrawText("Applicant Name", 33, Gx_line+50, 134, Gx_line+68, 0+256, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 12, true, true, true, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("CERTIFICATION EXTENSION ACTIVITY CREDIT DOCUMENTATION", 100, Gx_line+17, 725, Gx_line+45, 1, 0, 0, 0) ;
                     getPrinter().GxDrawText("IFPUG CERTIFIED FUNCTION POINT SPECIALIST", 100, Gx_line+0, 725, Gx_line+17, 1, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+125);
                     HVW0( false, 59) ;
                     getPrinter().GxDrawRect(33, Gx_line+9, 800, Gx_line+59, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxDrawRect(33, Gx_line+9, 175, Gx_line+59, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxDrawRect(325, Gx_line+9, 800, Gx_line+59, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Date(s) of FP Count", 50, Gx_line+17, 125, Gx_line+50, 1+16, 0, 0, 1) ;
                     getPrinter().GxDrawText("UFP of FP Count", 208, Gx_line+17, 291, Gx_line+50, 1+16, 0, 0, 1) ;
                     getPrinter().GxDrawText("Client/Manager Name", 492, Gx_line+17, 626, Gx_line+50, 1+16, 0, 0, 1) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+59);
                  }
                  HVW0( false, 200) ;
                  getPrinter().GxDrawRect(33, Gx_line+0, 800, Gx_line+200, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxDrawRect(325, Gx_line+0, 800, Gx_line+200, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxDrawRect(33, Gx_line+0, 175, Gx_line+200, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV51Gestor_Email, "")), 442, Gx_line+100, 784, Gx_line+115, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54Fiscal_Email, "")), 442, Gx_line+167, 784, Gx_line+182, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV52Gestor_Telefone, "")), 442, Gx_line+117, 547, Gx_line+132, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV53Fiscal_Telefone, "")), 442, Gx_line+183, 547, Gx_line+198, 0+256, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV45Periodo, "")), 42, Gx_line+67, 168, Gx_line+85, 1+256, 0, 0, 1) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV46Total, "ZZ,ZZZ,ZZ9.999")), 183, Gx_line+67, 316, Gx_line+85, 1, 0, 0, 1) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV47Contratante_RazaoSocial, "@!")), 442, Gx_line+17, 784, Gx_line+35, 0, 0, 0, 1) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV48CNPJ, "")), 558, Gx_line+33, 647, Gx_line+51, 0+256, 0, 0, 1) ;
                  getPrinter().GxDrawText("Manager:", 350, Gx_line+83, 410, Gx_line+101, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Supervisor:", 350, Gx_line+150, 422, Gx_line+168, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Client:", 350, Gx_line+17, 389, Gx_line+35, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV49Gestor_Nome, "@!")), 442, Gx_line+83, 784, Gx_line+101, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV50Fiscal_Nome, "@!")), 442, Gx_line+150, 784, Gx_line+168, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Agreement N�:", 442, Gx_line+50, 534, Gx_line+68, 0+256, 0, 0, 1) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV59Contrato_Numero, "")), 558, Gx_line+50, 684, Gx_line+68, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("National registry:", 442, Gx_line+33, 547, Gx_line+51, 0+256, 0, 0, 1) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+200);
                  A52Contratada_AreaTrabalhoCod = W52Contratada_AreaTrabalhoCod;
                  n52Contratada_AreaTrabalhoCod = false;
                  if ( ! BRKVW10 )
                  {
                     BRKVW10 = true;
                     pr_default.readNext(6);
                  }
               }
               pr_default.close(6);
               pr_default.readNext(5);
            }
            pr_default.close(5);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            HVW0( false, 633) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("As an IFPUG Certified Function Point Specialist:", 25, Gx_line+83, 318, Gx_line+101, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("I will promote the understanding of Function Point Counting Practices, methods and procedures.", 50, Gx_line+117, 633, Gx_line+135, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("I have an obligation to the FP community to uphold the high ideals of personal knowledge as evidence by the certification held.", 50, Gx_line+150, 775, Gx_line+185, 3+16, 0, 0, 0) ;
            getPrinter().GxDrawText("2.", 25, Gx_line+150, 42, Gx_line+168, 3, 0, 0, 0) ;
            getPrinter().GxDrawText("1.", 25, Gx_line+117, 42, Gx_line+135, 3, 0, 0, 0) ;
            getPrinter().GxDrawText("3.", 25, Gx_line+200, 42, Gx_line+218, 3, 0, 0, 0) ;
            getPrinter().GxDrawText("I have an obligation to serve the interest of my employers and/or clients loyally, diligently and honestly.", 50, Gx_line+200, 678, Gx_line+218, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("4.", 25, Gx_line+233, 42, Gx_line+251, 3, 0, 0, 0) ;
            getPrinter().GxDrawText("I will not engage in any conduct or commit any act, which is a discredit to the reputation or integrity of the CFPS program, IFPUG, or the information system community.", 58, Gx_line+233, 786, Gx_line+268, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("5.", 25, Gx_line+283, 42, Gx_line+301, 3, 0, 0, 0) ;
            getPrinter().GxDrawText("I will not imply or otherwise convey that the CFPS designation is my sole claim to professional competence.  I will continuously strive for professional knowledge and growth.", 58, Gx_line+283, 786, Gx_line+318, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("6.", 25, Gx_line+333, 75, Gx_line+351, 3, 0, 0, 0) ;
            getPrinter().GxDrawText("I will not engage in any activity during the administration of the exam, which could provide any of the participants, including myself, with an unfair advantage for successful completion of the exam.", 58, Gx_line+333, 786, Gx_line+368, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("By accepting their certificates, Certified Function Point Specialists agree to:  (1) hold IFPUG harmless from any and all liability arising out of their professional activities, and (2) abide by and uphold the IFPUG Code of Ethics.", 25, Gx_line+383, 758, Gx_line+436, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Please contact the IFPUG Executive Office with any questions you may have.", 25, Gx_line+433, 497, Gx_line+451, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("IFPUG", 392, Gx_line+517, 435, Gx_line+535, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("609-799-4900 Voice, 609-799-7032 Fax", 300, Gx_line+567, 535, Gx_line+585, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("http://www.ifpug.org", 350, Gx_line+583, 470, Gx_line+601, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("ifpug@ifpug.org", 367, Gx_line+600, 467, Gx_line+618, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("191 Clarksville Rd.", 350, Gx_line+533, 485, Gx_line+551, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Princeton Junction, New Jersey USA  08550", 258, Gx_line+550, 569, Gx_line+568, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("IFPUG Certified Function Point Specialist I.Code of Ethics", 183, Gx_line+0, 650, Gx_line+83, 1+16, 0, 0, 1) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+633);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HVW0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'FILTROS' Routine */
         if ( (0==AV9Area) )
         {
            AV12Area_Nome = "Todas";
         }
         else
         {
            /* Using cursor P00VW9 */
            pr_default.execute(7, new Object[] {AV9Area});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A5AreaTrabalho_Codigo = P00VW9_A5AreaTrabalho_Codigo[0];
               A6AreaTrabalho_Descricao = P00VW9_A6AreaTrabalho_Descricao[0];
               AV12Area_Nome = A6AreaTrabalho_Descricao;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(7);
         }
         if ( (0==AV16Colaborador) )
         {
            AV17Colaborador_Nome = "Todos";
         }
         else
         {
            /* Using cursor P00VW10 */
            pr_default.execute(8, new Object[] {AV16Colaborador});
            while ( (pr_default.getStatus(8) != 101) )
            {
               A57Usuario_PessoaCod = P00VW10_A57Usuario_PessoaCod[0];
               A1Usuario_Codigo = P00VW10_A1Usuario_Codigo[0];
               A58Usuario_PessoaNom = P00VW10_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = P00VW10_n58Usuario_PessoaNom[0];
               A1647Usuario_Email = P00VW10_A1647Usuario_Email[0];
               n1647Usuario_Email = P00VW10_n1647Usuario_Email[0];
               A341Usuario_UserGamGuid = P00VW10_A341Usuario_UserGamGuid[0];
               A58Usuario_PessoaNom = P00VW10_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = P00VW10_n58Usuario_PessoaNom[0];
               AV17Colaborador_Nome = A58Usuario_PessoaNom;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Trim( A1647Usuario_Email))) )
               {
                  AV43GamUser.load( A341Usuario_UserGamGuid);
                  AV41Usuario_Email = AV43GamUser.gxTpr_Email;
               }
               else
               {
                  AV41Usuario_Email = A1647Usuario_Email;
               }
               /* Using cursor P00VW11 */
               pr_default.execute(9, new Object[] {A57Usuario_PessoaCod});
               while ( (pr_default.getStatus(9) != 101) )
               {
                  A2011PessoaCertificado_PesCod = P00VW11_A2011PessoaCertificado_PesCod[0];
                  A2014PessoaCertificado_Validade = P00VW11_A2014PessoaCertificado_Validade[0];
                  n2014PessoaCertificado_Validade = P00VW11_n2014PessoaCertificado_Validade[0];
                  A2010PessoaCertificado_Codigo = P00VW11_A2010PessoaCertificado_Codigo[0];
                  AV57CFPS_Validade = A2014PessoaCertificado_Validade;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(9);
               }
               pr_default.close(9);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(8);
         }
         if ( (0==AV28Servico) )
         {
            AV29Servico_Sigla = "Todos";
         }
         else
         {
            /* Using cursor P00VW12 */
            pr_default.execute(10, new Object[] {AV28Servico});
            while ( (pr_default.getStatus(10) != 101) )
            {
               A155Servico_Codigo = P00VW12_A155Servico_Codigo[0];
               A605Servico_Sigla = P00VW12_A605Servico_Sigla[0];
               AV29Servico_Sigla = A605Servico_Sigla;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(10);
         }
         if ( (0==AV23Mes) )
         {
            AV24Mes_Nome = "Todos";
            AV45Periodo = StringUtil.Trim( StringUtil.Str( (decimal)(AV8Ano), 4, 0));
         }
         else
         {
            AV21Date = context.localUtil.CToD( "01/01/01", 2);
            AV32m = (short)(AV23Mes-1);
            AV21Date = DateTimeUtil.AddMth( AV21Date, AV32m);
            AV24Mes_Nome = DateTimeUtil.CMonth( AV21Date, "por");
            AV45Periodo = AV24Mes_Nome + " " + StringUtil.Trim( StringUtil.Str( (decimal)(AV8Ano), 4, 0));
         }
      }

      protected void S121( )
      {
         /* 'GESTORES' Routine */
         AV70Usuario_Codigo = 0;
         AV52Gestor_Telefone = "";
         AV88GXLvl180 = 0;
         /* Using cursor P00VW13 */
         pr_default.execute(11, new Object[] {AV60Contrato_Codigo});
         while ( (pr_default.getStatus(11) != 101) )
         {
            A1136ContratoGestor_ContratadaCod = P00VW13_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = P00VW13_n1136ContratoGestor_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00VW13_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00VW13_n52Contratada_AreaTrabalhoCod[0];
            A29Contratante_Codigo = P00VW13_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00VW13_n29Contratante_Codigo[0];
            A2009ContratoGestor_Tipo = P00VW13_A2009ContratoGestor_Tipo[0];
            n2009ContratoGestor_Tipo = P00VW13_n2009ContratoGestor_Tipo[0];
            A1078ContratoGestor_ContratoCod = P00VW13_A1078ContratoGestor_ContratoCod[0];
            A31Contratante_Telefone = P00VW13_A31Contratante_Telefone[0];
            A1079ContratoGestor_UsuarioCod = P00VW13_A1079ContratoGestor_UsuarioCod[0];
            A1446ContratoGestor_ContratadaAreaCod = P00VW13_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = P00VW13_n1446ContratoGestor_ContratadaAreaCod[0];
            A1136ContratoGestor_ContratadaCod = P00VW13_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = P00VW13_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = P00VW13_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = P00VW13_n1446ContratoGestor_ContratadaAreaCod[0];
            A52Contratada_AreaTrabalhoCod = P00VW13_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00VW13_n52Contratada_AreaTrabalhoCod[0];
            A29Contratante_Codigo = P00VW13_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00VW13_n29Contratante_Codigo[0];
            A31Contratante_Telefone = P00VW13_A31Contratante_Telefone[0];
            GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
            new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
            A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
            if ( A1135ContratoGestor_UsuarioEhContratante )
            {
               AV88GXLvl180 = 1;
               AV52Gestor_Telefone = A31Contratante_Telefone;
               AV70Usuario_Codigo = A1079ContratoGestor_UsuarioCod;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(11);
         }
         pr_default.close(11);
         if ( AV88GXLvl180 == 0 )
         {
            /* Using cursor P00VW14 */
            pr_default.execute(12, new Object[] {AV60Contrato_Codigo});
            while ( (pr_default.getStatus(12) != 101) )
            {
               A1136ContratoGestor_ContratadaCod = P00VW14_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = P00VW14_n1136ContratoGestor_ContratadaCod[0];
               A52Contratada_AreaTrabalhoCod = P00VW14_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00VW14_n52Contratada_AreaTrabalhoCod[0];
               A29Contratante_Codigo = P00VW14_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00VW14_n29Contratante_Codigo[0];
               A2009ContratoGestor_Tipo = P00VW14_A2009ContratoGestor_Tipo[0];
               n2009ContratoGestor_Tipo = P00VW14_n2009ContratoGestor_Tipo[0];
               A1078ContratoGestor_ContratoCod = P00VW14_A1078ContratoGestor_ContratoCod[0];
               A31Contratante_Telefone = P00VW14_A31Contratante_Telefone[0];
               A1079ContratoGestor_UsuarioCod = P00VW14_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P00VW14_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P00VW14_n1446ContratoGestor_ContratadaAreaCod[0];
               A1136ContratoGestor_ContratadaCod = P00VW14_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = P00VW14_n1136ContratoGestor_ContratadaCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P00VW14_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P00VW14_n1446ContratoGestor_ContratadaAreaCod[0];
               A52Contratada_AreaTrabalhoCod = P00VW14_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00VW14_n52Contratada_AreaTrabalhoCod[0];
               A29Contratante_Codigo = P00VW14_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00VW14_n29Contratante_Codigo[0];
               A31Contratante_Telefone = P00VW14_A31Contratante_Telefone[0];
               GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
               new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
               A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
               if ( A1135ContratoGestor_UsuarioEhContratante )
               {
                  AV52Gestor_Telefone = A31Contratante_Telefone;
                  AV70Usuario_Codigo = A1079ContratoGestor_UsuarioCod;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(12);
            }
            pr_default.close(12);
         }
         if ( AV70Usuario_Codigo > 0 )
         {
            AV49Gestor_Nome = "";
            AV51Gestor_Email = "";
            /* Using cursor P00VW15 */
            pr_default.execute(13, new Object[] {AV70Usuario_Codigo});
            while ( (pr_default.getStatus(13) != 101) )
            {
               A57Usuario_PessoaCod = P00VW15_A57Usuario_PessoaCod[0];
               A1Usuario_Codigo = P00VW15_A1Usuario_Codigo[0];
               A58Usuario_PessoaNom = P00VW15_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = P00VW15_n58Usuario_PessoaNom[0];
               A1647Usuario_Email = P00VW15_A1647Usuario_Email[0];
               n1647Usuario_Email = P00VW15_n1647Usuario_Email[0];
               A341Usuario_UserGamGuid = P00VW15_A341Usuario_UserGamGuid[0];
               A58Usuario_PessoaNom = P00VW15_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = P00VW15_n58Usuario_PessoaNom[0];
               AV49Gestor_Nome = A58Usuario_PessoaNom;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Trim( A1647Usuario_Email))) )
               {
                  AV43GamUser.load( A341Usuario_UserGamGuid);
                  AV51Gestor_Email = AV43GamUser.gxTpr_Email;
               }
               else
               {
                  AV51Gestor_Email = A1647Usuario_Email;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(13);
         }
         AV70Usuario_Codigo = 0;
         AV52Gestor_Telefone = "";
         /* Using cursor P00VW16 */
         pr_default.execute(14, new Object[] {AV60Contrato_Codigo});
         while ( (pr_default.getStatus(14) != 101) )
         {
            A1136ContratoGestor_ContratadaCod = P00VW16_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = P00VW16_n1136ContratoGestor_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00VW16_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00VW16_n52Contratada_AreaTrabalhoCod[0];
            A29Contratante_Codigo = P00VW16_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00VW16_n29Contratante_Codigo[0];
            A2009ContratoGestor_Tipo = P00VW16_A2009ContratoGestor_Tipo[0];
            n2009ContratoGestor_Tipo = P00VW16_n2009ContratoGestor_Tipo[0];
            A1078ContratoGestor_ContratoCod = P00VW16_A1078ContratoGestor_ContratoCod[0];
            A31Contratante_Telefone = P00VW16_A31Contratante_Telefone[0];
            A1079ContratoGestor_UsuarioCod = P00VW16_A1079ContratoGestor_UsuarioCod[0];
            A1446ContratoGestor_ContratadaAreaCod = P00VW16_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = P00VW16_n1446ContratoGestor_ContratadaAreaCod[0];
            A1136ContratoGestor_ContratadaCod = P00VW16_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = P00VW16_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = P00VW16_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = P00VW16_n1446ContratoGestor_ContratadaAreaCod[0];
            A52Contratada_AreaTrabalhoCod = P00VW16_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00VW16_n52Contratada_AreaTrabalhoCod[0];
            A29Contratante_Codigo = P00VW16_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00VW16_n29Contratante_Codigo[0];
            A31Contratante_Telefone = P00VW16_A31Contratante_Telefone[0];
            GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
            new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
            A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
            if ( A1135ContratoGestor_UsuarioEhContratante )
            {
               AV53Fiscal_Telefone = A31Contratante_Telefone;
               AV70Usuario_Codigo = A1079ContratoGestor_UsuarioCod;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(14);
         }
         pr_default.close(14);
         if ( AV70Usuario_Codigo > 0 )
         {
            AV49Gestor_Nome = "";
            AV51Gestor_Email = "";
            /* Using cursor P00VW17 */
            pr_default.execute(15, new Object[] {AV70Usuario_Codigo});
            while ( (pr_default.getStatus(15) != 101) )
            {
               A57Usuario_PessoaCod = P00VW17_A57Usuario_PessoaCod[0];
               A1Usuario_Codigo = P00VW17_A1Usuario_Codigo[0];
               A58Usuario_PessoaNom = P00VW17_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = P00VW17_n58Usuario_PessoaNom[0];
               A1647Usuario_Email = P00VW17_A1647Usuario_Email[0];
               n1647Usuario_Email = P00VW17_n1647Usuario_Email[0];
               A341Usuario_UserGamGuid = P00VW17_A341Usuario_UserGamGuid[0];
               A58Usuario_PessoaNom = P00VW17_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = P00VW17_n58Usuario_PessoaNom[0];
               AV50Fiscal_Nome = A58Usuario_PessoaNom;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Trim( A1647Usuario_Email))) )
               {
                  AV43GamUser.load( A341Usuario_UserGamGuid);
                  AV54Fiscal_Email = AV43GamUser.gxTpr_Email;
               }
               else
               {
                  AV54Fiscal_Email = A1647Usuario_Email;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(15);
         }
      }

      protected void HVW0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("IFPUG-CERTIFICATION EXTENSION APPLICATION_V4.1_20130301", 0, Gx_line+0, 351, Gx_line+14, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+14);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
         add_metrics3( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics3( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, true, 58, 14, 72, 123,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 30, 35, 35, 55, 45, 14, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 21, 21, 37, 37, 37, 38, 61, 45, 45, 45, 45, 42, 38, 49, 45, 17, 35, 45, 38, 52, 45, 49, 42, 49, 45, 42, 38, 45, 42, 59, 42, 42, 38, 21, 18, 23, 37, 35, 21, 35, 38, 35, 38, 35, 21, 38, 38, 18, 18, 35, 18, 56, 38, 38, 38, 38, 25, 35, 21, 38, 35, 49, 35, 35, 32, 25, 17, 25, 37, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 36, 35, 35, 35, 17, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 21, 21, 36, 35, 21, 21, 21, 23, 35, 53, 53, 53, 38, 45, 45, 45, 45, 45, 45, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 35, 35, 35, 35, 35, 18, 18, 18, 18, 38, 38, 38, 38, 38, 38, 38, 35, 38, 38, 38, 38, 38, 35, 38, 35}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV68SDT_Codigos = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs");
         AV31WebSession = context.GetSession();
         AV69Codigo = new SdtSDT_Codigos(context);
         AV15Codigos = new GxSimpleCollection();
         scmdbuf = "";
         P00VW2_A40Contratada_PessoaCod = new int[1] ;
         P00VW2_A503Pessoa_MunicipioCod = new int[1] ;
         P00VW2_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P00VW2_A39Contratada_Codigo = new int[1] ;
         P00VW2_A41Contratada_PessoaNom = new String[] {""} ;
         P00VW2_n41Contratada_PessoaNom = new bool[] {false} ;
         P00VW2_A519Pessoa_Endereco = new String[] {""} ;
         P00VW2_n519Pessoa_Endereco = new bool[] {false} ;
         P00VW2_A26Municipio_Nome = new String[] {""} ;
         P00VW2_n26Municipio_Nome = new bool[] {false} ;
         P00VW2_A520Pessoa_UF = new String[] {""} ;
         P00VW2_n520Pessoa_UF = new bool[] {false} ;
         P00VW2_A521Pessoa_CEP = new String[] {""} ;
         P00VW2_n521Pessoa_CEP = new bool[] {false} ;
         P00VW2_A522Pessoa_Telefone = new String[] {""} ;
         P00VW2_n522Pessoa_Telefone = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         A519Pessoa_Endereco = "";
         A26Municipio_Nome = "";
         A520Pessoa_UF = "";
         A521Pessoa_CEP = "";
         A522Pessoa_Telefone = "";
         AV33Contratada_PessoaNom = "";
         AV34Pessoa_Endereco = "";
         AV38Municipio_Nome = "";
         AV36Pessoa_UF = "";
         AV39Pessoa_CEP = "";
         AV40Pessoa_Telefone = "";
         P00VW3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00VW3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00VW3_A456ContagemResultado_Codigo = new int[1] ;
         P00VW3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00VW3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         AV11Area_Codigos = new GxSimpleCollection();
         P00VW4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00VW4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00VW4_A456ContagemResultado_Codigo = new int[1] ;
         P00VW4_A1603ContagemResultado_CntCod = new int[1] ;
         P00VW4_n1603ContagemResultado_CntCod = new bool[] {false} ;
         AV61Contrato_Codigos = new GxSimpleCollection();
         AV17Colaborador_Nome = "";
         Gx_date = DateTime.MinValue;
         AV41Usuario_Email = "";
         AV57CFPS_Validade = DateTime.MinValue;
         P00VW5_A29Contratante_Codigo = new int[1] ;
         P00VW5_n29Contratante_Codigo = new bool[] {false} ;
         P00VW5_A335Contratante_PessoaCod = new int[1] ;
         P00VW5_A5AreaTrabalho_Codigo = new int[1] ;
         P00VW5_A9Contratante_RazaoSocial = new String[] {""} ;
         P00VW5_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00VW5_A12Contratante_CNPJ = new String[] {""} ;
         P00VW5_n12Contratante_CNPJ = new bool[] {false} ;
         P00VW5_A6AreaTrabalho_Descricao = new String[] {""} ;
         A9Contratante_RazaoSocial = "";
         A12Contratante_CNPJ = "";
         A6AreaTrabalho_Descricao = "";
         AV13AreaTrabalho_Descricao = "";
         AV47Contratante_RazaoSocial = "";
         AV48CNPJ = "";
         P00VW6_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00VW6_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00VW6_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00VW6_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00VW6_A1603ContagemResultado_CntCod = new int[1] ;
         P00VW6_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00VW6_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00VW6_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00VW6_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00VW6_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00VW6_A460ContagemResultado_PFBFM = new decimal[1] ;
         P00VW6_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P00VW6_A458ContagemResultado_PFBFS = new decimal[1] ;
         P00VW6_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P00VW6_A456ContagemResultado_Codigo = new int[1] ;
         P00VW6_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00VW6_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00VW6_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00VW6_A1612ContagemResultado_CntNum = new String[] {""} ;
         P00VW6_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P00VW6_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00VW6_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A1326ContagemResultado_ContratadaTipoFab = "";
         A1612ContagemResultado_CntNum = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         AV59Contrato_Numero = "";
         AV51Gestor_Email = "";
         AV54Fiscal_Email = "";
         AV52Gestor_Telefone = "";
         AV53Fiscal_Telefone = "";
         AV45Periodo = "";
         AV49Gestor_Nome = "";
         AV50Fiscal_Nome = "";
         P00VW7_A29Contratante_Codigo = new int[1] ;
         P00VW7_n29Contratante_Codigo = new bool[] {false} ;
         P00VW7_A335Contratante_PessoaCod = new int[1] ;
         P00VW7_A5AreaTrabalho_Codigo = new int[1] ;
         P00VW7_A9Contratante_RazaoSocial = new String[] {""} ;
         P00VW7_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00VW7_A12Contratante_CNPJ = new String[] {""} ;
         P00VW7_n12Contratante_CNPJ = new bool[] {false} ;
         P00VW7_A6AreaTrabalho_Descricao = new String[] {""} ;
         P00VW8_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00VW8_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00VW8_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00VW8_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00VW8_A1603ContagemResultado_CntCod = new int[1] ;
         P00VW8_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00VW8_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00VW8_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00VW8_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00VW8_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00VW8_A460ContagemResultado_PFBFM = new decimal[1] ;
         P00VW8_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P00VW8_A458ContagemResultado_PFBFS = new decimal[1] ;
         P00VW8_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P00VW8_A456ContagemResultado_Codigo = new int[1] ;
         P00VW8_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00VW8_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00VW8_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00VW8_A1612ContagemResultado_CntNum = new String[] {""} ;
         P00VW8_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P00VW8_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00VW8_A511ContagemResultado_HoraCnt = new String[] {""} ;
         AV12Area_Nome = "";
         P00VW9_A5AreaTrabalho_Codigo = new int[1] ;
         P00VW9_A6AreaTrabalho_Descricao = new String[] {""} ;
         P00VW10_A57Usuario_PessoaCod = new int[1] ;
         P00VW10_A1Usuario_Codigo = new int[1] ;
         P00VW10_A58Usuario_PessoaNom = new String[] {""} ;
         P00VW10_n58Usuario_PessoaNom = new bool[] {false} ;
         P00VW10_A1647Usuario_Email = new String[] {""} ;
         P00VW10_n1647Usuario_Email = new bool[] {false} ;
         P00VW10_A341Usuario_UserGamGuid = new String[] {""} ;
         A58Usuario_PessoaNom = "";
         A1647Usuario_Email = "";
         A341Usuario_UserGamGuid = "";
         AV43GamUser = new SdtGAMUser(context);
         P00VW11_A2011PessoaCertificado_PesCod = new int[1] ;
         P00VW11_A2014PessoaCertificado_Validade = new DateTime[] {DateTime.MinValue} ;
         P00VW11_n2014PessoaCertificado_Validade = new bool[] {false} ;
         P00VW11_A2010PessoaCertificado_Codigo = new int[1] ;
         A2014PessoaCertificado_Validade = DateTime.MinValue;
         AV29Servico_Sigla = "";
         P00VW12_A155Servico_Codigo = new int[1] ;
         P00VW12_A605Servico_Sigla = new String[] {""} ;
         A605Servico_Sigla = "";
         AV24Mes_Nome = "";
         AV21Date = DateTime.MinValue;
         P00VW13_A1136ContratoGestor_ContratadaCod = new int[1] ;
         P00VW13_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         P00VW13_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00VW13_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00VW13_A29Contratante_Codigo = new int[1] ;
         P00VW13_n29Contratante_Codigo = new bool[] {false} ;
         P00VW13_A2009ContratoGestor_Tipo = new short[1] ;
         P00VW13_n2009ContratoGestor_Tipo = new bool[] {false} ;
         P00VW13_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00VW13_A31Contratante_Telefone = new String[] {""} ;
         P00VW13_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00VW13_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00VW13_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         A31Contratante_Telefone = "";
         P00VW14_A1136ContratoGestor_ContratadaCod = new int[1] ;
         P00VW14_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         P00VW14_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00VW14_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00VW14_A29Contratante_Codigo = new int[1] ;
         P00VW14_n29Contratante_Codigo = new bool[] {false} ;
         P00VW14_A2009ContratoGestor_Tipo = new short[1] ;
         P00VW14_n2009ContratoGestor_Tipo = new bool[] {false} ;
         P00VW14_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00VW14_A31Contratante_Telefone = new String[] {""} ;
         P00VW14_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00VW14_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00VW14_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P00VW15_A57Usuario_PessoaCod = new int[1] ;
         P00VW15_A1Usuario_Codigo = new int[1] ;
         P00VW15_A58Usuario_PessoaNom = new String[] {""} ;
         P00VW15_n58Usuario_PessoaNom = new bool[] {false} ;
         P00VW15_A1647Usuario_Email = new String[] {""} ;
         P00VW15_n1647Usuario_Email = new bool[] {false} ;
         P00VW15_A341Usuario_UserGamGuid = new String[] {""} ;
         P00VW16_A1136ContratoGestor_ContratadaCod = new int[1] ;
         P00VW16_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         P00VW16_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00VW16_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00VW16_A29Contratante_Codigo = new int[1] ;
         P00VW16_n29Contratante_Codigo = new bool[] {false} ;
         P00VW16_A2009ContratoGestor_Tipo = new short[1] ;
         P00VW16_n2009ContratoGestor_Tipo = new bool[] {false} ;
         P00VW16_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00VW16_A31Contratante_Telefone = new String[] {""} ;
         P00VW16_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00VW16_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00VW16_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P00VW17_A57Usuario_PessoaCod = new int[1] ;
         P00VW17_A1Usuario_Codigo = new int[1] ;
         P00VW17_A58Usuario_PessoaNom = new String[] {""} ;
         P00VW17_n58Usuario_PessoaNom = new bool[] {false} ;
         P00VW17_A1647Usuario_Email = new String[] {""} ;
         P00VW17_n1647Usuario_Email = new bool[] {false} ;
         P00VW17_A341Usuario_UserGamGuid = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_ifpug__default(),
            new Object[][] {
                new Object[] {
               P00VW2_A40Contratada_PessoaCod, P00VW2_A503Pessoa_MunicipioCod, P00VW2_n503Pessoa_MunicipioCod, P00VW2_A39Contratada_Codigo, P00VW2_A41Contratada_PessoaNom, P00VW2_n41Contratada_PessoaNom, P00VW2_A519Pessoa_Endereco, P00VW2_n519Pessoa_Endereco, P00VW2_A26Municipio_Nome, P00VW2_n26Municipio_Nome,
               P00VW2_A520Pessoa_UF, P00VW2_n520Pessoa_UF, P00VW2_A521Pessoa_CEP, P00VW2_n521Pessoa_CEP, P00VW2_A522Pessoa_Telefone, P00VW2_n522Pessoa_Telefone
               }
               , new Object[] {
               P00VW3_A490ContagemResultado_ContratadaCod, P00VW3_n490ContagemResultado_ContratadaCod, P00VW3_A456ContagemResultado_Codigo, P00VW3_A52Contratada_AreaTrabalhoCod, P00VW3_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               P00VW4_A1553ContagemResultado_CntSrvCod, P00VW4_n1553ContagemResultado_CntSrvCod, P00VW4_A456ContagemResultado_Codigo, P00VW4_A1603ContagemResultado_CntCod, P00VW4_n1603ContagemResultado_CntCod
               }
               , new Object[] {
               P00VW5_A29Contratante_Codigo, P00VW5_n29Contratante_Codigo, P00VW5_A335Contratante_PessoaCod, P00VW5_A5AreaTrabalho_Codigo, P00VW5_A9Contratante_RazaoSocial, P00VW5_n9Contratante_RazaoSocial, P00VW5_A12Contratante_CNPJ, P00VW5_n12Contratante_CNPJ, P00VW5_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00VW6_A490ContagemResultado_ContratadaCod, P00VW6_n490ContagemResultado_ContratadaCod, P00VW6_A1553ContagemResultado_CntSrvCod, P00VW6_n1553ContagemResultado_CntSrvCod, P00VW6_A1603ContagemResultado_CntCod, P00VW6_n1603ContagemResultado_CntCod, P00VW6_A52Contratada_AreaTrabalhoCod, P00VW6_n52Contratada_AreaTrabalhoCod, P00VW6_A1326ContagemResultado_ContratadaTipoFab, P00VW6_n1326ContagemResultado_ContratadaTipoFab,
               P00VW6_A460ContagemResultado_PFBFM, P00VW6_n460ContagemResultado_PFBFM, P00VW6_A458ContagemResultado_PFBFS, P00VW6_n458ContagemResultado_PFBFS, P00VW6_A456ContagemResultado_Codigo, P00VW6_A517ContagemResultado_Ultima, P00VW6_A598ContagemResultado_Baseline, P00VW6_n598ContagemResultado_Baseline, P00VW6_A1612ContagemResultado_CntNum, P00VW6_n1612ContagemResultado_CntNum,
               P00VW6_A473ContagemResultado_DataCnt, P00VW6_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               P00VW7_A29Contratante_Codigo, P00VW7_n29Contratante_Codigo, P00VW7_A335Contratante_PessoaCod, P00VW7_A5AreaTrabalho_Codigo, P00VW7_A9Contratante_RazaoSocial, P00VW7_n9Contratante_RazaoSocial, P00VW7_A12Contratante_CNPJ, P00VW7_n12Contratante_CNPJ, P00VW7_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00VW8_A490ContagemResultado_ContratadaCod, P00VW8_n490ContagemResultado_ContratadaCod, P00VW8_A1553ContagemResultado_CntSrvCod, P00VW8_n1553ContagemResultado_CntSrvCod, P00VW8_A1603ContagemResultado_CntCod, P00VW8_n1603ContagemResultado_CntCod, P00VW8_A52Contratada_AreaTrabalhoCod, P00VW8_n52Contratada_AreaTrabalhoCod, P00VW8_A1326ContagemResultado_ContratadaTipoFab, P00VW8_n1326ContagemResultado_ContratadaTipoFab,
               P00VW8_A460ContagemResultado_PFBFM, P00VW8_n460ContagemResultado_PFBFM, P00VW8_A458ContagemResultado_PFBFS, P00VW8_n458ContagemResultado_PFBFS, P00VW8_A456ContagemResultado_Codigo, P00VW8_A517ContagemResultado_Ultima, P00VW8_A598ContagemResultado_Baseline, P00VW8_n598ContagemResultado_Baseline, P00VW8_A1612ContagemResultado_CntNum, P00VW8_n1612ContagemResultado_CntNum,
               P00VW8_A473ContagemResultado_DataCnt, P00VW8_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               P00VW9_A5AreaTrabalho_Codigo, P00VW9_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00VW10_A57Usuario_PessoaCod, P00VW10_A1Usuario_Codigo, P00VW10_A58Usuario_PessoaNom, P00VW10_n58Usuario_PessoaNom, P00VW10_A1647Usuario_Email, P00VW10_n1647Usuario_Email, P00VW10_A341Usuario_UserGamGuid
               }
               , new Object[] {
               P00VW11_A2011PessoaCertificado_PesCod, P00VW11_A2014PessoaCertificado_Validade, P00VW11_n2014PessoaCertificado_Validade, P00VW11_A2010PessoaCertificado_Codigo
               }
               , new Object[] {
               P00VW12_A155Servico_Codigo, P00VW12_A605Servico_Sigla
               }
               , new Object[] {
               P00VW13_A1136ContratoGestor_ContratadaCod, P00VW13_n1136ContratoGestor_ContratadaCod, P00VW13_A52Contratada_AreaTrabalhoCod, P00VW13_n52Contratada_AreaTrabalhoCod, P00VW13_A29Contratante_Codigo, P00VW13_n29Contratante_Codigo, P00VW13_A2009ContratoGestor_Tipo, P00VW13_n2009ContratoGestor_Tipo, P00VW13_A1078ContratoGestor_ContratoCod, P00VW13_A31Contratante_Telefone,
               P00VW13_A1079ContratoGestor_UsuarioCod, P00VW13_A1446ContratoGestor_ContratadaAreaCod, P00VW13_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P00VW14_A1136ContratoGestor_ContratadaCod, P00VW14_n1136ContratoGestor_ContratadaCod, P00VW14_A52Contratada_AreaTrabalhoCod, P00VW14_n52Contratada_AreaTrabalhoCod, P00VW14_A29Contratante_Codigo, P00VW14_n29Contratante_Codigo, P00VW14_A2009ContratoGestor_Tipo, P00VW14_n2009ContratoGestor_Tipo, P00VW14_A1078ContratoGestor_ContratoCod, P00VW14_A31Contratante_Telefone,
               P00VW14_A1079ContratoGestor_UsuarioCod, P00VW14_A1446ContratoGestor_ContratadaAreaCod, P00VW14_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P00VW15_A57Usuario_PessoaCod, P00VW15_A1Usuario_Codigo, P00VW15_A58Usuario_PessoaNom, P00VW15_n58Usuario_PessoaNom, P00VW15_A1647Usuario_Email, P00VW15_n1647Usuario_Email, P00VW15_A341Usuario_UserGamGuid
               }
               , new Object[] {
               P00VW16_A1136ContratoGestor_ContratadaCod, P00VW16_n1136ContratoGestor_ContratadaCod, P00VW16_A52Contratada_AreaTrabalhoCod, P00VW16_n52Contratada_AreaTrabalhoCod, P00VW16_A29Contratante_Codigo, P00VW16_n29Contratante_Codigo, P00VW16_A2009ContratoGestor_Tipo, P00VW16_n2009ContratoGestor_Tipo, P00VW16_A1078ContratoGestor_ContratoCod, P00VW16_A31Contratante_Telefone,
               P00VW16_A1079ContratoGestor_UsuarioCod, P00VW16_A1446ContratoGestor_ContratadaAreaCod, P00VW16_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P00VW17_A57Usuario_PessoaCod, P00VW17_A1Usuario_Codigo, P00VW17_A58Usuario_PessoaNom, P00VW17_n58Usuario_PessoaNom, P00VW17_A1647Usuario_Email, P00VW17_n1647Usuario_Email, P00VW17_A341Usuario_UserGamGuid
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV8Ano ;
      private short GxWebError ;
      private short AV32m ;
      private short AV88GXLvl180 ;
      private short A2009ContratoGestor_Tipo ;
      private int AV19Contratada_Codigo ;
      private int AV9Area ;
      private int AV16Colaborador ;
      private int AV28Servico ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV73GXV1 ;
      private int A40Contratada_PessoaCod ;
      private int A503Pessoa_MunicipioCod ;
      private int A39Contratada_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int Gx_OldLine ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int AV55AreaTrabalho_Codigo ;
      private int W52Contratada_AreaTrabalhoCod ;
      private int AV60Contrato_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private int A2011PessoaCertificado_PesCod ;
      private int A2010PessoaCertificado_Codigo ;
      private int A155Servico_Codigo ;
      private int AV70Usuario_Codigo ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private long AV23Mes ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal AV46Total ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV30User_Nome ;
      private String scmdbuf ;
      private String A41Contratada_PessoaNom ;
      private String A26Municipio_Nome ;
      private String A520Pessoa_UF ;
      private String A521Pessoa_CEP ;
      private String A522Pessoa_Telefone ;
      private String AV33Contratada_PessoaNom ;
      private String AV38Municipio_Nome ;
      private String AV36Pessoa_UF ;
      private String AV39Pessoa_CEP ;
      private String AV40Pessoa_Telefone ;
      private String AV17Colaborador_Nome ;
      private String A9Contratante_RazaoSocial ;
      private String AV47Contratante_RazaoSocial ;
      private String AV48CNPJ ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String A1612ContagemResultado_CntNum ;
      private String A511ContagemResultado_HoraCnt ;
      private String AV59Contrato_Numero ;
      private String AV52Gestor_Telefone ;
      private String AV53Fiscal_Telefone ;
      private String AV45Periodo ;
      private String AV49Gestor_Nome ;
      private String AV50Fiscal_Nome ;
      private String AV12Area_Nome ;
      private String A58Usuario_PessoaNom ;
      private String A341Usuario_UserGamGuid ;
      private String AV29Servico_Sigla ;
      private String A605Servico_Sigla ;
      private String AV24Mes_Nome ;
      private String A31Contratante_Telefone ;
      private DateTime Gx_date ;
      private DateTime AV57CFPS_Validade ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime A2014PessoaCertificado_Validade ;
      private DateTime AV21Date ;
      private bool entryPointCalled ;
      private bool returnInSub ;
      private bool n503Pessoa_MunicipioCod ;
      private bool n41Contratada_PessoaNom ;
      private bool n519Pessoa_Endereco ;
      private bool n26Municipio_Nome ;
      private bool n520Pessoa_UF ;
      private bool n521Pessoa_CEP ;
      private bool n522Pessoa_Telefone ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n29Contratante_Codigo ;
      private bool n9Contratante_RazaoSocial ;
      private bool n12Contratante_CNPJ ;
      private bool A598ContagemResultado_Baseline ;
      private bool A517ContagemResultado_Ultima ;
      private bool BRKVW7 ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n598ContagemResultado_Baseline ;
      private bool n1612ContagemResultado_CntNum ;
      private bool AV56PaginaImpressa ;
      private bool BRKVW10 ;
      private bool n58Usuario_PessoaNom ;
      private bool n1647Usuario_Email ;
      private bool n2014PessoaCertificado_Validade ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n2009ContratoGestor_Tipo ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool GXt_boolean1 ;
      private String A519Pessoa_Endereco ;
      private String AV34Pessoa_Endereco ;
      private String AV41Usuario_Email ;
      private String A12Contratante_CNPJ ;
      private String A6AreaTrabalho_Descricao ;
      private String AV13AreaTrabalho_Descricao ;
      private String AV51Gestor_Email ;
      private String AV54Fiscal_Email ;
      private String A1647Usuario_Email ;
      private IGxSession AV31WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00VW2_A40Contratada_PessoaCod ;
      private int[] P00VW2_A503Pessoa_MunicipioCod ;
      private bool[] P00VW2_n503Pessoa_MunicipioCod ;
      private int[] P00VW2_A39Contratada_Codigo ;
      private String[] P00VW2_A41Contratada_PessoaNom ;
      private bool[] P00VW2_n41Contratada_PessoaNom ;
      private String[] P00VW2_A519Pessoa_Endereco ;
      private bool[] P00VW2_n519Pessoa_Endereco ;
      private String[] P00VW2_A26Municipio_Nome ;
      private bool[] P00VW2_n26Municipio_Nome ;
      private String[] P00VW2_A520Pessoa_UF ;
      private bool[] P00VW2_n520Pessoa_UF ;
      private String[] P00VW2_A521Pessoa_CEP ;
      private bool[] P00VW2_n521Pessoa_CEP ;
      private String[] P00VW2_A522Pessoa_Telefone ;
      private bool[] P00VW2_n522Pessoa_Telefone ;
      private int[] P00VW3_A490ContagemResultado_ContratadaCod ;
      private bool[] P00VW3_n490ContagemResultado_ContratadaCod ;
      private int[] P00VW3_A456ContagemResultado_Codigo ;
      private int[] P00VW3_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00VW3_n52Contratada_AreaTrabalhoCod ;
      private int[] P00VW4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00VW4_n1553ContagemResultado_CntSrvCod ;
      private int[] P00VW4_A456ContagemResultado_Codigo ;
      private int[] P00VW4_A1603ContagemResultado_CntCod ;
      private bool[] P00VW4_n1603ContagemResultado_CntCod ;
      private int[] P00VW5_A29Contratante_Codigo ;
      private bool[] P00VW5_n29Contratante_Codigo ;
      private int[] P00VW5_A335Contratante_PessoaCod ;
      private int[] P00VW5_A5AreaTrabalho_Codigo ;
      private String[] P00VW5_A9Contratante_RazaoSocial ;
      private bool[] P00VW5_n9Contratante_RazaoSocial ;
      private String[] P00VW5_A12Contratante_CNPJ ;
      private bool[] P00VW5_n12Contratante_CNPJ ;
      private String[] P00VW5_A6AreaTrabalho_Descricao ;
      private int[] P00VW6_A490ContagemResultado_ContratadaCod ;
      private bool[] P00VW6_n490ContagemResultado_ContratadaCod ;
      private int[] P00VW6_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00VW6_n1553ContagemResultado_CntSrvCod ;
      private int[] P00VW6_A1603ContagemResultado_CntCod ;
      private bool[] P00VW6_n1603ContagemResultado_CntCod ;
      private int[] P00VW6_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00VW6_n52Contratada_AreaTrabalhoCod ;
      private String[] P00VW6_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00VW6_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] P00VW6_A460ContagemResultado_PFBFM ;
      private bool[] P00VW6_n460ContagemResultado_PFBFM ;
      private decimal[] P00VW6_A458ContagemResultado_PFBFS ;
      private bool[] P00VW6_n458ContagemResultado_PFBFS ;
      private int[] P00VW6_A456ContagemResultado_Codigo ;
      private bool[] P00VW6_A517ContagemResultado_Ultima ;
      private bool[] P00VW6_A598ContagemResultado_Baseline ;
      private bool[] P00VW6_n598ContagemResultado_Baseline ;
      private String[] P00VW6_A1612ContagemResultado_CntNum ;
      private bool[] P00VW6_n1612ContagemResultado_CntNum ;
      private DateTime[] P00VW6_A473ContagemResultado_DataCnt ;
      private String[] P00VW6_A511ContagemResultado_HoraCnt ;
      private int[] P00VW7_A29Contratante_Codigo ;
      private bool[] P00VW7_n29Contratante_Codigo ;
      private int[] P00VW7_A335Contratante_PessoaCod ;
      private int[] P00VW7_A5AreaTrabalho_Codigo ;
      private String[] P00VW7_A9Contratante_RazaoSocial ;
      private bool[] P00VW7_n9Contratante_RazaoSocial ;
      private String[] P00VW7_A12Contratante_CNPJ ;
      private bool[] P00VW7_n12Contratante_CNPJ ;
      private String[] P00VW7_A6AreaTrabalho_Descricao ;
      private int[] P00VW8_A490ContagemResultado_ContratadaCod ;
      private bool[] P00VW8_n490ContagemResultado_ContratadaCod ;
      private int[] P00VW8_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00VW8_n1553ContagemResultado_CntSrvCod ;
      private int[] P00VW8_A1603ContagemResultado_CntCod ;
      private bool[] P00VW8_n1603ContagemResultado_CntCod ;
      private int[] P00VW8_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00VW8_n52Contratada_AreaTrabalhoCod ;
      private String[] P00VW8_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00VW8_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] P00VW8_A460ContagemResultado_PFBFM ;
      private bool[] P00VW8_n460ContagemResultado_PFBFM ;
      private decimal[] P00VW8_A458ContagemResultado_PFBFS ;
      private bool[] P00VW8_n458ContagemResultado_PFBFS ;
      private int[] P00VW8_A456ContagemResultado_Codigo ;
      private bool[] P00VW8_A517ContagemResultado_Ultima ;
      private bool[] P00VW8_A598ContagemResultado_Baseline ;
      private bool[] P00VW8_n598ContagemResultado_Baseline ;
      private String[] P00VW8_A1612ContagemResultado_CntNum ;
      private bool[] P00VW8_n1612ContagemResultado_CntNum ;
      private DateTime[] P00VW8_A473ContagemResultado_DataCnt ;
      private String[] P00VW8_A511ContagemResultado_HoraCnt ;
      private int[] P00VW9_A5AreaTrabalho_Codigo ;
      private String[] P00VW9_A6AreaTrabalho_Descricao ;
      private int[] P00VW10_A57Usuario_PessoaCod ;
      private int[] P00VW10_A1Usuario_Codigo ;
      private String[] P00VW10_A58Usuario_PessoaNom ;
      private bool[] P00VW10_n58Usuario_PessoaNom ;
      private String[] P00VW10_A1647Usuario_Email ;
      private bool[] P00VW10_n1647Usuario_Email ;
      private String[] P00VW10_A341Usuario_UserGamGuid ;
      private int[] P00VW11_A2011PessoaCertificado_PesCod ;
      private DateTime[] P00VW11_A2014PessoaCertificado_Validade ;
      private bool[] P00VW11_n2014PessoaCertificado_Validade ;
      private int[] P00VW11_A2010PessoaCertificado_Codigo ;
      private int[] P00VW12_A155Servico_Codigo ;
      private String[] P00VW12_A605Servico_Sigla ;
      private int[] P00VW13_A1136ContratoGestor_ContratadaCod ;
      private bool[] P00VW13_n1136ContratoGestor_ContratadaCod ;
      private int[] P00VW13_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00VW13_n52Contratada_AreaTrabalhoCod ;
      private int[] P00VW13_A29Contratante_Codigo ;
      private bool[] P00VW13_n29Contratante_Codigo ;
      private short[] P00VW13_A2009ContratoGestor_Tipo ;
      private bool[] P00VW13_n2009ContratoGestor_Tipo ;
      private int[] P00VW13_A1078ContratoGestor_ContratoCod ;
      private String[] P00VW13_A31Contratante_Telefone ;
      private int[] P00VW13_A1079ContratoGestor_UsuarioCod ;
      private int[] P00VW13_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00VW13_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P00VW14_A1136ContratoGestor_ContratadaCod ;
      private bool[] P00VW14_n1136ContratoGestor_ContratadaCod ;
      private int[] P00VW14_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00VW14_n52Contratada_AreaTrabalhoCod ;
      private int[] P00VW14_A29Contratante_Codigo ;
      private bool[] P00VW14_n29Contratante_Codigo ;
      private short[] P00VW14_A2009ContratoGestor_Tipo ;
      private bool[] P00VW14_n2009ContratoGestor_Tipo ;
      private int[] P00VW14_A1078ContratoGestor_ContratoCod ;
      private String[] P00VW14_A31Contratante_Telefone ;
      private int[] P00VW14_A1079ContratoGestor_UsuarioCod ;
      private int[] P00VW14_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00VW14_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P00VW15_A57Usuario_PessoaCod ;
      private int[] P00VW15_A1Usuario_Codigo ;
      private String[] P00VW15_A58Usuario_PessoaNom ;
      private bool[] P00VW15_n58Usuario_PessoaNom ;
      private String[] P00VW15_A1647Usuario_Email ;
      private bool[] P00VW15_n1647Usuario_Email ;
      private String[] P00VW15_A341Usuario_UserGamGuid ;
      private int[] P00VW16_A1136ContratoGestor_ContratadaCod ;
      private bool[] P00VW16_n1136ContratoGestor_ContratadaCod ;
      private int[] P00VW16_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00VW16_n52Contratada_AreaTrabalhoCod ;
      private int[] P00VW16_A29Contratante_Codigo ;
      private bool[] P00VW16_n29Contratante_Codigo ;
      private short[] P00VW16_A2009ContratoGestor_Tipo ;
      private bool[] P00VW16_n2009ContratoGestor_Tipo ;
      private int[] P00VW16_A1078ContratoGestor_ContratoCod ;
      private String[] P00VW16_A31Contratante_Telefone ;
      private int[] P00VW16_A1079ContratoGestor_UsuarioCod ;
      private int[] P00VW16_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00VW16_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P00VW17_A57Usuario_PessoaCod ;
      private int[] P00VW17_A1Usuario_Codigo ;
      private String[] P00VW17_A58Usuario_PessoaNom ;
      private bool[] P00VW17_n58Usuario_PessoaNom ;
      private String[] P00VW17_A1647Usuario_Email ;
      private bool[] P00VW17_n1647Usuario_Email ;
      private String[] P00VW17_A341Usuario_UserGamGuid ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV15Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV11Area_Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV61Contrato_Codigos ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection AV68SDT_Codigos ;
      private SdtSDT_Codigos AV69Codigo ;
      private SdtGAMUser AV43GamUser ;
   }

   public class arel_ifpug__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00VW3( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV15Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT NULL AS [ContagemResultado_ContratadaCod], NULL AS [ContagemResultado_Codigo], [Contratada_AreaTrabalhoCod] FROM ( SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T2.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV15Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object2[0] = scmdbuf;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00VW4( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV15Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT NULL AS [ContagemResultado_CntSrvCod], NULL AS [ContagemResultado_Codigo], [ContagemResultado_CntCod] FROM ( SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T2.[Contrato_Codigo] AS ContagemResultado_CntCod FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV15Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object4[0] = scmdbuf;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00VW5( IGxContext context ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV11Area_Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_Codigo], T3.[Pessoa_Nome] AS Contratante_RazaoSocial, T3.[Pessoa_Docto] AS Contratante_CNPJ, T1.[AreaTrabalho_Descricao] FROM (([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV11Area_Codigos, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[AreaTrabalho_Descricao]";
         GXv_Object6[0] = scmdbuf;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00VW6( IGxContext context ,
                                             int A1603ContagemResultado_CntCod ,
                                             IGxCollection AV61Contrato_Codigos ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV15Codigos ,
                                             bool A598ContagemResultado_Baseline ,
                                             bool A517ContagemResultado_Ultima ,
                                             int A5AreaTrabalho_Codigo ,
                                             int A52Contratada_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int8 ;
         GXv_int8 = new short [1] ;
         Object[] GXv_Object9 ;
         GXv_Object9 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Contrato_Codigo] AS ContagemResultado_CntCod, T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T3.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Ultima], T2.[ContagemResultado_Baseline], T5.[Contrato_Numero] AS ContagemResultado_CntNum, T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM (((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T3.[Contratada_AreaTrabalhoCod] = @AreaTrabalho_Codigo)";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV61Contrato_Codigos, "T4.[Contrato_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV15Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T2.[ContagemResultado_Baseline] IS NULL or Not T2.[ContagemResultado_Baseline] = 1)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Ultima] = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Contratada_AreaTrabalhoCod], T4.[Contrato_Codigo], T1.[ContagemResultado_Codigo]";
         GXv_Object9[0] = scmdbuf;
         GXv_Object9[1] = GXv_int8;
         return GXv_Object9 ;
      }

      protected Object[] conditional_P00VW7( IGxContext context ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV11Area_Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_Codigo], T3.[Pessoa_Nome] AS Contratante_RazaoSocial, T3.[Pessoa_Docto] AS Contratante_CNPJ, T1.[AreaTrabalho_Descricao] FROM (([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV11Area_Codigos, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[AreaTrabalho_Descricao]";
         GXv_Object10[0] = scmdbuf;
         return GXv_Object10 ;
      }

      protected Object[] conditional_P00VW8( IGxContext context ,
                                             int A1603ContagemResultado_CntCod ,
                                             IGxCollection AV61Contrato_Codigos ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV15Codigos ,
                                             bool A598ContagemResultado_Baseline ,
                                             bool A517ContagemResultado_Ultima ,
                                             int A5AreaTrabalho_Codigo ,
                                             int A52Contratada_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int12 ;
         GXv_int12 = new short [1] ;
         Object[] GXv_Object13 ;
         GXv_Object13 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Contrato_Codigo] AS ContagemResultado_CntCod, T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T3.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Ultima], T2.[ContagemResultado_Baseline], T5.[Contrato_Numero] AS ContagemResultado_CntNum, T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM (((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T3.[Contratada_AreaTrabalhoCod] = @AreaTrabalho_Codigo)";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV61Contrato_Codigos, "T4.[Contrato_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV15Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T2.[ContagemResultado_Baseline] = 1)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Ultima] = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Contratada_AreaTrabalhoCod], T4.[Contrato_Codigo], T1.[ContagemResultado_Codigo]";
         GXv_Object13[0] = scmdbuf;
         GXv_Object13[1] = GXv_int12;
         return GXv_Object13 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P00VW3(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 2 :
                     return conditional_P00VW4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 3 :
                     return conditional_P00VW5(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 4 :
                     return conditional_P00VW6(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
               case 5 :
                     return conditional_P00VW7(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 6 :
                     return conditional_P00VW8(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VW2 ;
          prmP00VW2 = new Object[] {
          new Object[] {"@AV19Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VW9 ;
          prmP00VW9 = new Object[] {
          new Object[] {"@AV9Area",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VW10 ;
          prmP00VW10 = new Object[] {
          new Object[] {"@AV16Colaborador",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VW11 ;
          prmP00VW11 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VW12 ;
          prmP00VW12 = new Object[] {
          new Object[] {"@AV28Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VW13 ;
          prmP00VW13 = new Object[] {
          new Object[] {"@AV60Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VW14 ;
          prmP00VW14 = new Object[] {
          new Object[] {"@AV60Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VW15 ;
          prmP00VW15 = new Object[] {
          new Object[] {"@AV70Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VW16 ;
          prmP00VW16 = new Object[] {
          new Object[] {"@AV60Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VW17 ;
          prmP00VW17 = new Object[] {
          new Object[] {"@AV70Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VW3 ;
          prmP00VW3 = new Object[] {
          } ;
          Object[] prmP00VW4 ;
          prmP00VW4 = new Object[] {
          } ;
          Object[] prmP00VW5 ;
          prmP00VW5 = new Object[] {
          } ;
          Object[] prmP00VW6 ;
          prmP00VW6 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VW7 ;
          prmP00VW7 = new Object[] {
          } ;
          Object[] prmP00VW8 ;
          prmP00VW8 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VW2", "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Pessoa_Endereco], T3.[Municipio_Nome], T3.[Estado_UF] AS Pessoa_UF, T2.[Pessoa_CEP], T2.[Pessoa_Telefone] FROM (([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Pessoa_MunicipioCod]) WHERE T1.[Contratada_Codigo] = @AV19Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW2,1,0,false,true )
             ,new CursorDef("P00VW3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW3,100,0,false,false )
             ,new CursorDef("P00VW4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW4,100,0,false,false )
             ,new CursorDef("P00VW5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW5,100,0,true,false )
             ,new CursorDef("P00VW6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW6,100,0,true,false )
             ,new CursorDef("P00VW7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW7,100,0,true,false )
             ,new CursorDef("P00VW8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW8,100,0,true,false )
             ,new CursorDef("P00VW9", "SELECT TOP 1 [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV9Area ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW9,1,0,false,true )
             ,new CursorDef("P00VW10", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Email], T1.[Usuario_UserGamGuid] FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV16Colaborador ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW10,1,0,true,true )
             ,new CursorDef("P00VW11", "SELECT TOP 1 [PessoaCertificado_PesCod], [PessoaCertificado_Validade], [PessoaCertificado_Codigo] FROM [PessoaCertificado] WITH (NOLOCK) WHERE [PessoaCertificado_PesCod] = @Usuario_PessoaCod ORDER BY [PessoaCertificado_PesCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW11,1,0,false,true )
             ,new CursorDef("P00VW12", "SELECT TOP 1 [Servico_Codigo], [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AV28Servico ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW12,1,0,false,true )
             ,new CursorDef("P00VW13", "SELECT T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[Contratante_Codigo], T1.[ContratoGestor_Tipo], T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T5.[Contratante_Telefone], T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM (((([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Contratante] T5 WITH (NOLOCK) ON T5.[Contratante_Codigo] = T4.[Contratante_Codigo]) WHERE (T1.[ContratoGestor_ContratoCod] = @AV60Contrato_Codigo) AND (T1.[ContratoGestor_Tipo] = 1) ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW13,100,0,true,false )
             ,new CursorDef("P00VW14", "SELECT T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[Contratante_Codigo], T1.[ContratoGestor_Tipo], T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T5.[Contratante_Telefone], T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM (((([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Contratante] T5 WITH (NOLOCK) ON T5.[Contratante_Codigo] = T4.[Contratante_Codigo]) WHERE (T1.[ContratoGestor_ContratoCod] = @AV60Contrato_Codigo) AND (T1.[ContratoGestor_Tipo] IS NULL or (T1.[ContratoGestor_Tipo] = convert(int, 0))) ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW14,100,0,true,false )
             ,new CursorDef("P00VW15", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Email], T1.[Usuario_UserGamGuid] FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV70Usuario_Codigo ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW15,1,0,true,true )
             ,new CursorDef("P00VW16", "SELECT T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[Contratante_Codigo], T1.[ContratoGestor_Tipo], T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T5.[Contratante_Telefone], T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM (((([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Contratante] T5 WITH (NOLOCK) ON T5.[Contratante_Codigo] = T4.[Contratante_Codigo]) WHERE (T1.[ContratoGestor_ContratoCod] = @AV60Contrato_Codigo) AND (T1.[ContratoGestor_Tipo] = 2) ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW16,100,0,true,false )
             ,new CursorDef("P00VW17", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Email], T1.[Usuario_UserGamGuid] FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV70Usuario_Codigo ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VW17,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 2) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.getBool(9) ;
                ((bool[]) buf[16])[0] = rslt.getBool(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 20) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(12) ;
                ((String[]) buf[21])[0] = rslt.getString(13, 5) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.getBool(9) ;
                ((bool[]) buf[16])[0] = rslt.getBool(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 20) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(12) ;
                ((String[]) buf[21])[0] = rslt.getString(13, 5) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 40) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 20) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 20) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 40) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 20) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 40) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
