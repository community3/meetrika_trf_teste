/*
               File: PRC_FDComplexidade
        Description: Complexidade da Fun��o de Dados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:49.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_fdcomplexidade : GXProcedure
   {
      public prc_fdcomplexidade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_fdcomplexidade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_FuncaoDados_Codigo ,
                           ref String aP1_Complexidade )
      {
         this.AV8FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.AV9Complexidade = aP1_Complexidade;
         initialize();
         executePrivate();
         aP0_FuncaoDados_Codigo=this.AV8FuncaoDados_Codigo;
         aP1_Complexidade=this.AV9Complexidade;
      }

      public String executeUdp( ref int aP0_FuncaoDados_Codigo )
      {
         this.AV8FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.AV9Complexidade = aP1_Complexidade;
         initialize();
         executePrivate();
         aP0_FuncaoDados_Codigo=this.AV8FuncaoDados_Codigo;
         aP1_Complexidade=this.AV9Complexidade;
         return AV9Complexidade ;
      }

      public void executeSubmit( ref int aP0_FuncaoDados_Codigo ,
                                 ref String aP1_Complexidade )
      {
         prc_fdcomplexidade objprc_fdcomplexidade;
         objprc_fdcomplexidade = new prc_fdcomplexidade();
         objprc_fdcomplexidade.AV8FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         objprc_fdcomplexidade.AV9Complexidade = aP1_Complexidade;
         objprc_fdcomplexidade.context.SetSubmitInitialConfig(context);
         objprc_fdcomplexidade.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_fdcomplexidade);
         aP0_FuncaoDados_Codigo=this.AV8FuncaoDados_Codigo;
         aP1_Complexidade=this.AV9Complexidade;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_fdcomplexidade)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9Complexidade = "E";
         /* Using cursor P001V2 */
         pr_default.execute(0, new Object[] {AV8FuncaoDados_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A391FuncaoDados_FuncaoDadosCod = P001V2_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = P001V2_n391FuncaoDados_FuncaoDadosCod[0];
            A373FuncaoDados_Tipo = P001V2_A373FuncaoDados_Tipo[0];
            A368FuncaoDados_Codigo = P001V2_A368FuncaoDados_Codigo[0];
            GXt_int1 = A374FuncaoDados_DER;
            new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
            A374FuncaoDados_DER = GXt_int1;
            GXt_int1 = A375FuncaoDados_RLR;
            new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
            A375FuncaoDados_RLR = GXt_int1;
            if ( (0==A391FuncaoDados_FuncaoDadosCod) )
            {
               if ( ( A375FuncaoDados_RLR == 0 ) || ( StringUtil.StrCmp(A373FuncaoDados_Tipo, "DC") == 0 ) )
               {
               }
               else if ( A375FuncaoDados_RLR == 1 )
               {
                  if ( A374FuncaoDados_DER == 0 )
                  {
                  }
                  else if ( A374FuncaoDados_DER < 51 )
                  {
                     AV9Complexidade = "B";
                  }
                  else if ( A374FuncaoDados_DER > 50 )
                  {
                     AV9Complexidade = "M";
                  }
               }
               else if ( A375FuncaoDados_RLR < 6 )
               {
                  if ( A374FuncaoDados_DER == 0 )
                  {
                  }
                  else if ( A374FuncaoDados_DER < 20 )
                  {
                     AV9Complexidade = "B";
                  }
                  else if ( A374FuncaoDados_DER > 50 )
                  {
                     AV9Complexidade = "A";
                  }
                  else
                  {
                     AV9Complexidade = "M";
                  }
               }
               else if ( A375FuncaoDados_RLR > 5 )
               {
                  if ( A374FuncaoDados_DER == 0 )
                  {
                  }
                  else if ( A374FuncaoDados_DER < 20 )
                  {
                     AV9Complexidade = "M";
                  }
                  else if ( A374FuncaoDados_DER > 19 )
                  {
                     AV9Complexidade = "A";
                  }
               }
            }
            else
            {
               AV10FuncaoDados_FuncaoDadosCod = A391FuncaoDados_FuncaoDadosCod;
               /* Execute user subroutine: 'FUNCAOEXTERNA' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'FUNCAOEXTERNA' Routine */
         /* Using cursor P001V3 */
         pr_default.execute(1, new Object[] {AV10FuncaoDados_FuncaoDadosCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A368FuncaoDados_Codigo = P001V3_A368FuncaoDados_Codigo[0];
            GXt_char2 = A376FuncaoDados_Complexidade;
            new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
            A376FuncaoDados_Complexidade = GXt_char2;
            AV9Complexidade = A376FuncaoDados_Complexidade;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001V2_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         P001V2_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         P001V2_A373FuncaoDados_Tipo = new String[] {""} ;
         P001V2_A368FuncaoDados_Codigo = new int[1] ;
         A373FuncaoDados_Tipo = "";
         P001V3_A368FuncaoDados_Codigo = new int[1] ;
         A376FuncaoDados_Complexidade = "";
         GXt_char2 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_fdcomplexidade__default(),
            new Object[][] {
                new Object[] {
               P001V2_A391FuncaoDados_FuncaoDadosCod, P001V2_n391FuncaoDados_FuncaoDadosCod, P001V2_A373FuncaoDados_Tipo, P001V2_A368FuncaoDados_Codigo
               }
               , new Object[] {
               P001V3_A368FuncaoDados_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A374FuncaoDados_DER ;
      private short A375FuncaoDados_RLR ;
      private short GXt_int1 ;
      private int AV8FuncaoDados_Codigo ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int A368FuncaoDados_Codigo ;
      private int AV10FuncaoDados_FuncaoDadosCod ;
      private String AV9Complexidade ;
      private String scmdbuf ;
      private String A373FuncaoDados_Tipo ;
      private String A376FuncaoDados_Complexidade ;
      private String GXt_char2 ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool returnInSub ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_FuncaoDados_Codigo ;
      private String aP1_Complexidade ;
      private IDataStoreProvider pr_default ;
      private int[] P001V2_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] P001V2_n391FuncaoDados_FuncaoDadosCod ;
      private String[] P001V2_A373FuncaoDados_Tipo ;
      private int[] P001V2_A368FuncaoDados_Codigo ;
      private int[] P001V3_A368FuncaoDados_Codigo ;
   }

   public class prc_fdcomplexidade__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001V2 ;
          prmP001V2 = new Object[] {
          new Object[] {"@AV8FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP001V3 ;
          prmP001V3 = new Object[] {
          new Object[] {"@AV10FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001V2", "SELECT [FuncaoDados_FuncaoDadosCod], [FuncaoDados_Tipo], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV8FuncaoDados_Codigo ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001V2,1,0,true,true )
             ,new CursorDef("P001V3", "SELECT [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV10FuncaoDados_FuncaoDadosCod ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001V3,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
