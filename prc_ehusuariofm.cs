/*
               File: PRC_EhUsuarioFM
        Description: Eh Usuario de Fabrica de Metricas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:57.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_ehusuariofm : GXProcedure
   {
      public prc_ehusuariofm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_ehusuariofm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratadaUsuario_UsuarioCod ,
                           ref int aP1_Contratada_AreaTrabalhoCod ,
                           ref bool aP2_Flag )
      {
         this.A69ContratadaUsuario_UsuarioCod = aP0_ContratadaUsuario_UsuarioCod;
         this.A52Contratada_AreaTrabalhoCod = aP1_Contratada_AreaTrabalhoCod;
         this.AV8Flag = aP2_Flag;
         initialize();
         executePrivate();
         aP0_ContratadaUsuario_UsuarioCod=this.A69ContratadaUsuario_UsuarioCod;
         aP1_Contratada_AreaTrabalhoCod=this.A52Contratada_AreaTrabalhoCod;
         aP2_Flag=this.AV8Flag;
      }

      public bool executeUdp( ref int aP0_ContratadaUsuario_UsuarioCod ,
                              ref int aP1_Contratada_AreaTrabalhoCod )
      {
         this.A69ContratadaUsuario_UsuarioCod = aP0_ContratadaUsuario_UsuarioCod;
         this.A52Contratada_AreaTrabalhoCod = aP1_Contratada_AreaTrabalhoCod;
         this.AV8Flag = aP2_Flag;
         initialize();
         executePrivate();
         aP0_ContratadaUsuario_UsuarioCod=this.A69ContratadaUsuario_UsuarioCod;
         aP1_Contratada_AreaTrabalhoCod=this.A52Contratada_AreaTrabalhoCod;
         aP2_Flag=this.AV8Flag;
         return AV8Flag ;
      }

      public void executeSubmit( ref int aP0_ContratadaUsuario_UsuarioCod ,
                                 ref int aP1_Contratada_AreaTrabalhoCod ,
                                 ref bool aP2_Flag )
      {
         prc_ehusuariofm objprc_ehusuariofm;
         objprc_ehusuariofm = new prc_ehusuariofm();
         objprc_ehusuariofm.A69ContratadaUsuario_UsuarioCod = aP0_ContratadaUsuario_UsuarioCod;
         objprc_ehusuariofm.A52Contratada_AreaTrabalhoCod = aP1_Contratada_AreaTrabalhoCod;
         objprc_ehusuariofm.AV8Flag = aP2_Flag;
         objprc_ehusuariofm.context.SetSubmitInitialConfig(context);
         objprc_ehusuariofm.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_ehusuariofm);
         aP0_ContratadaUsuario_UsuarioCod=this.A69ContratadaUsuario_UsuarioCod;
         aP1_Contratada_AreaTrabalhoCod=this.A52Contratada_AreaTrabalhoCod;
         aP2_Flag=this.AV8Flag;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_ehusuariofm)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11GXLvl2 = 0;
         /* Using cursor P003K2 */
         pr_default.execute(0, new Object[] {A69ContratadaUsuario_UsuarioCod, A52Contratada_AreaTrabalhoCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A66ContratadaUsuario_ContratadaCod = P003K2_A66ContratadaUsuario_ContratadaCod[0];
            A516Contratada_TipoFabrica = P003K2_A516Contratada_TipoFabrica[0];
            n516Contratada_TipoFabrica = P003K2_n516Contratada_TipoFabrica[0];
            A516Contratada_TipoFabrica = P003K2_A516Contratada_TipoFabrica[0];
            n516Contratada_TipoFabrica = P003K2_n516Contratada_TipoFabrica[0];
            AV11GXLvl2 = 1;
            AV8Flag = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV11GXLvl2 == 0 )
         {
            AV8Flag = false;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P003K2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P003K2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P003K2_A516Contratada_TipoFabrica = new String[] {""} ;
         P003K2_n516Contratada_TipoFabrica = new bool[] {false} ;
         A516Contratada_TipoFabrica = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_ehusuariofm__default(),
            new Object[][] {
                new Object[] {
               P003K2_A66ContratadaUsuario_ContratadaCod, P003K2_A69ContratadaUsuario_UsuarioCod, P003K2_A516Contratada_TipoFabrica, P003K2_n516Contratada_TipoFabrica
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV11GXLvl2 ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private String scmdbuf ;
      private String A516Contratada_TipoFabrica ;
      private bool AV8Flag ;
      private bool n516Contratada_TipoFabrica ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratadaUsuario_UsuarioCod ;
      private int aP1_Contratada_AreaTrabalhoCod ;
      private bool aP2_Flag ;
      private IDataStoreProvider pr_default ;
      private int[] P003K2_A66ContratadaUsuario_ContratadaCod ;
      private int[] P003K2_A69ContratadaUsuario_UsuarioCod ;
      private String[] P003K2_A516Contratada_TipoFabrica ;
      private bool[] P003K2_n516Contratada_TipoFabrica ;
   }

   public class prc_ehusuariofm__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP003K2 ;
          prmP003K2 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P003K2", "SELECT TOP 1 T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_TipoFabrica] FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod) AND (@Contratada_AreaTrabalhoCod = @Contratada_AreaTrabalhoCod) AND (T2.[Contratada_TipoFabrica] = 'M') ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003K2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
