/*
               File: type_SdtGeoRegions_GeoRegionsItem
        Description: GeoRegions
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:56.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GeoRegions.GeoRegionsItem" )]
   [XmlType(TypeName =  "GeoRegions.GeoRegionsItem" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtGeoRegions_GeoRegionsItem : GxUserType
   {
      public SdtGeoRegions_GeoRegionsItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtGeoRegions_GeoRegionsItem_Rvaltext = "";
         gxTv_SdtGeoRegions_GeoRegionsItem_Rcountry = "";
      }

      public SdtGeoRegions_GeoRegionsItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtGeoRegions_GeoRegionsItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtGeoRegions_GeoRegionsItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtGeoRegions_GeoRegionsItem obj ;
         obj = this;
         obj.gxTpr_Rvalnum = deserialized.gxTpr_Rvalnum;
         obj.gxTpr_Rvaltext = deserialized.gxTpr_Rvaltext;
         obj.gxTpr_Rcountry = deserialized.gxTpr_Rcountry;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "RValnum") )
               {
                  gxTv_SdtGeoRegions_GeoRegionsItem_Rvalnum = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RValtext") )
               {
                  gxTv_SdtGeoRegions_GeoRegionsItem_Rvaltext = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RCountry") )
               {
                  gxTv_SdtGeoRegions_GeoRegionsItem_Rcountry = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "GeoRegions.GeoRegionsItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("RValnum", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGeoRegions_GeoRegionsItem_Rvalnum), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("RValtext", StringUtil.RTrim( gxTv_SdtGeoRegions_GeoRegionsItem_Rvaltext));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("RCountry", StringUtil.RTrim( gxTv_SdtGeoRegions_GeoRegionsItem_Rcountry));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("RValnum", gxTv_SdtGeoRegions_GeoRegionsItem_Rvalnum, false);
         AddObjectProperty("RValtext", gxTv_SdtGeoRegions_GeoRegionsItem_Rvaltext, false);
         AddObjectProperty("RCountry", gxTv_SdtGeoRegions_GeoRegionsItem_Rcountry, false);
         return  ;
      }

      [  SoapElement( ElementName = "RValnum" )]
      [  XmlElement( ElementName = "RValnum"   )]
      public int gxTpr_Rvalnum
      {
         get {
            return gxTv_SdtGeoRegions_GeoRegionsItem_Rvalnum ;
         }

         set {
            gxTv_SdtGeoRegions_GeoRegionsItem_Rvalnum = (int)(value);
         }

      }

      [  SoapElement( ElementName = "RValtext" )]
      [  XmlElement( ElementName = "RValtext"   )]
      public String gxTpr_Rvaltext
      {
         get {
            return gxTv_SdtGeoRegions_GeoRegionsItem_Rvaltext ;
         }

         set {
            gxTv_SdtGeoRegions_GeoRegionsItem_Rvaltext = (String)(value);
         }

      }

      [  SoapElement( ElementName = "RCountry" )]
      [  XmlElement( ElementName = "RCountry"   )]
      public String gxTpr_Rcountry
      {
         get {
            return gxTv_SdtGeoRegions_GeoRegionsItem_Rcountry ;
         }

         set {
            gxTv_SdtGeoRegions_GeoRegionsItem_Rcountry = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtGeoRegions_GeoRegionsItem_Rvaltext = "";
         gxTv_SdtGeoRegions_GeoRegionsItem_Rcountry = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtGeoRegions_GeoRegionsItem_Rvalnum ;
      protected String gxTv_SdtGeoRegions_GeoRegionsItem_Rvaltext ;
      protected String gxTv_SdtGeoRegions_GeoRegionsItem_Rcountry ;
      protected String sTagName ;
   }

   [DataContract(Name = @"GeoRegions.GeoRegionsItem", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtGeoRegions_GeoRegionsItem_RESTInterface : GxGenericCollectionItem<SdtGeoRegions_GeoRegionsItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGeoRegions_GeoRegionsItem_RESTInterface( ) : base()
      {
      }

      public SdtGeoRegions_GeoRegionsItem_RESTInterface( SdtGeoRegions_GeoRegionsItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "RValnum" , Order = 0 )]
      public Nullable<int> gxTpr_Rvalnum
      {
         get {
            return sdt.gxTpr_Rvalnum ;
         }

         set {
            sdt.gxTpr_Rvalnum = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "RValtext" , Order = 1 )]
      public String gxTpr_Rvaltext
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Rvaltext) ;
         }

         set {
            sdt.gxTpr_Rvaltext = (String)(value);
         }

      }

      [DataMember( Name = "RCountry" , Order = 2 )]
      public String gxTpr_Rcountry
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Rcountry) ;
         }

         set {
            sdt.gxTpr_Rcountry = (String)(value);
         }

      }

      public SdtGeoRegions_GeoRegionsItem sdt
      {
         get {
            return (SdtGeoRegions_GeoRegionsItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtGeoRegions_GeoRegionsItem() ;
         }
      }

   }

}
