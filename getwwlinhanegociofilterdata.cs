/*
               File: GetWWLinhaNegocioFilterData
        Description: Get WWLinha Negocio Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:12.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwlinhanegociofilterdata : GXProcedure
   {
      public getwwlinhanegociofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwlinhanegociofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwlinhanegociofilterdata objgetwwlinhanegociofilterdata;
         objgetwwlinhanegociofilterdata = new getwwlinhanegociofilterdata();
         objgetwwlinhanegociofilterdata.AV18DDOName = aP0_DDOName;
         objgetwwlinhanegociofilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwlinhanegociofilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwlinhanegociofilterdata.AV22OptionsJson = "" ;
         objgetwwlinhanegociofilterdata.AV25OptionsDescJson = "" ;
         objgetwwlinhanegociofilterdata.AV27OptionIndexesJson = "" ;
         objgetwwlinhanegociofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwlinhanegociofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwlinhanegociofilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwlinhanegociofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_LINHANEGOCIO_IDENTIFICADOR") == 0 )
         {
            /* Execute user subroutine: 'LOADLINHANEGOCIO_IDENTIFICADOROPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_LINHANEGOCIO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADLINHANEGOCIO_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWLinhaNegocioGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWLinhaNegocioGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWLinhaNegocioGridState"), "");
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFLINHANEGOCIO_IDENTIFICADOR") == 0 )
            {
               AV10TFLinhaNegocio_Identificador = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFLINHANEGOCIO_IDENTIFICADOR_SEL") == 0 )
            {
               AV11TFLinhaNegocio_Identificador_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFLINHANEGOCIO_DESCRICAO") == 0 )
            {
               AV12TFLinhaNegocio_Descricao = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFLINHANEGOCIO_DESCRICAO_SEL") == 0 )
            {
               AV13TFLinhaNegocio_Descricao_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFLINHANEGOCIO_GPOOBJCTRLCOD") == 0 )
            {
               AV14TFLinhaNegocio_GpoObjCtrlCod = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
               AV15TFLinhaNegocio_GpoObjCtrlCod_To = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, "."));
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "LINHANEGOCIO_IDENTIFICADOR") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV36LinhaNegocio_Identificador1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV37DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV38DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "LINHANEGOCIO_IDENTIFICADOR") == 0 )
               {
                  AV39DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV40LinhaNegocio_Identificador2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV41DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV42DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "LINHANEGOCIO_IDENTIFICADOR") == 0 )
                  {
                     AV43DynamicFiltersOperator3 = AV33GridStateDynamicFilter.gxTpr_Operator;
                     AV44LinhaNegocio_Identificador3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADLINHANEGOCIO_IDENTIFICADOROPTIONS' Routine */
         AV10TFLinhaNegocio_Identificador = AV16SearchTxt;
         AV11TFLinhaNegocio_Identificador_Sel = "";
         AV49WWLinhaNegocioDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV50WWLinhaNegocioDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 = AV36LinhaNegocio_Identificador1;
         AV52WWLinhaNegocioDS_4_Dynamicfiltersenabled2 = AV37DynamicFiltersEnabled2;
         AV53WWLinhaNegocioDS_5_Dynamicfiltersselector2 = AV38DynamicFiltersSelector2;
         AV54WWLinhaNegocioDS_6_Dynamicfiltersoperator2 = AV39DynamicFiltersOperator2;
         AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 = AV40LinhaNegocio_Identificador2;
         AV56WWLinhaNegocioDS_8_Dynamicfiltersenabled3 = AV41DynamicFiltersEnabled3;
         AV57WWLinhaNegocioDS_9_Dynamicfiltersselector3 = AV42DynamicFiltersSelector3;
         AV58WWLinhaNegocioDS_10_Dynamicfiltersoperator3 = AV43DynamicFiltersOperator3;
         AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 = AV44LinhaNegocio_Identificador3;
         AV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador = AV10TFLinhaNegocio_Identificador;
         AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel = AV11TFLinhaNegocio_Identificador_Sel;
         AV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao = AV12TFLinhaNegocio_Descricao;
         AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel = AV13TFLinhaNegocio_Descricao_Sel;
         AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod = AV14TFLinhaNegocio_GpoObjCtrlCod;
         AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to = AV15TFLinhaNegocio_GpoObjCtrlCod_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV49WWLinhaNegocioDS_1_Dynamicfiltersselector1 ,
                                              AV50WWLinhaNegocioDS_2_Dynamicfiltersoperator1 ,
                                              AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 ,
                                              AV52WWLinhaNegocioDS_4_Dynamicfiltersenabled2 ,
                                              AV53WWLinhaNegocioDS_5_Dynamicfiltersselector2 ,
                                              AV54WWLinhaNegocioDS_6_Dynamicfiltersoperator2 ,
                                              AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 ,
                                              AV56WWLinhaNegocioDS_8_Dynamicfiltersenabled3 ,
                                              AV57WWLinhaNegocioDS_9_Dynamicfiltersselector3 ,
                                              AV58WWLinhaNegocioDS_10_Dynamicfiltersoperator3 ,
                                              AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 ,
                                              AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel ,
                                              AV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador ,
                                              AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel ,
                                              AV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao ,
                                              AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod ,
                                              AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to ,
                                              A2037LinhaNegocio_Identificador ,
                                              A2038LinhaNegocio_Descricao ,
                                              A2035LinhaNegocio_GpoObjCtrlCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT
                                              }
         });
         lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 = StringUtil.Concat( StringUtil.RTrim( AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1), "%", "");
         lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 = StringUtil.Concat( StringUtil.RTrim( AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1), "%", "");
         lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 = StringUtil.Concat( StringUtil.RTrim( AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2), "%", "");
         lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 = StringUtil.Concat( StringUtil.RTrim( AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2), "%", "");
         lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 = StringUtil.Concat( StringUtil.RTrim( AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3), "%", "");
         lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 = StringUtil.Concat( StringUtil.RTrim( AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3), "%", "");
         lV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador = StringUtil.Concat( StringUtil.RTrim( AV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador), "%", "");
         lV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao = StringUtil.Concat( StringUtil.RTrim( AV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao), "%", "");
         /* Using cursor P00WM2 */
         pr_default.execute(0, new Object[] {lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1, lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1, lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2, lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2, lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3, lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3, lV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador, AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel, lV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao, AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel, AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod, AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKWM2 = false;
            A2037LinhaNegocio_Identificador = P00WM2_A2037LinhaNegocio_Identificador[0];
            A2035LinhaNegocio_GpoObjCtrlCod = P00WM2_A2035LinhaNegocio_GpoObjCtrlCod[0];
            A2038LinhaNegocio_Descricao = P00WM2_A2038LinhaNegocio_Descricao[0];
            n2038LinhaNegocio_Descricao = P00WM2_n2038LinhaNegocio_Descricao[0];
            A2036LinhadeNegocio_Codigo = P00WM2_A2036LinhadeNegocio_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00WM2_A2037LinhaNegocio_Identificador[0], A2037LinhaNegocio_Identificador) == 0 ) )
            {
               BRKWM2 = false;
               A2036LinhadeNegocio_Codigo = P00WM2_A2036LinhadeNegocio_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKWM2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2037LinhaNegocio_Identificador)) )
            {
               AV20Option = A2037LinhaNegocio_Identificador;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKWM2 )
            {
               BRKWM2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADLINHANEGOCIO_DESCRICAOOPTIONS' Routine */
         AV12TFLinhaNegocio_Descricao = AV16SearchTxt;
         AV13TFLinhaNegocio_Descricao_Sel = "";
         AV49WWLinhaNegocioDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV50WWLinhaNegocioDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 = AV36LinhaNegocio_Identificador1;
         AV52WWLinhaNegocioDS_4_Dynamicfiltersenabled2 = AV37DynamicFiltersEnabled2;
         AV53WWLinhaNegocioDS_5_Dynamicfiltersselector2 = AV38DynamicFiltersSelector2;
         AV54WWLinhaNegocioDS_6_Dynamicfiltersoperator2 = AV39DynamicFiltersOperator2;
         AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 = AV40LinhaNegocio_Identificador2;
         AV56WWLinhaNegocioDS_8_Dynamicfiltersenabled3 = AV41DynamicFiltersEnabled3;
         AV57WWLinhaNegocioDS_9_Dynamicfiltersselector3 = AV42DynamicFiltersSelector3;
         AV58WWLinhaNegocioDS_10_Dynamicfiltersoperator3 = AV43DynamicFiltersOperator3;
         AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 = AV44LinhaNegocio_Identificador3;
         AV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador = AV10TFLinhaNegocio_Identificador;
         AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel = AV11TFLinhaNegocio_Identificador_Sel;
         AV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao = AV12TFLinhaNegocio_Descricao;
         AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel = AV13TFLinhaNegocio_Descricao_Sel;
         AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod = AV14TFLinhaNegocio_GpoObjCtrlCod;
         AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to = AV15TFLinhaNegocio_GpoObjCtrlCod_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV49WWLinhaNegocioDS_1_Dynamicfiltersselector1 ,
                                              AV50WWLinhaNegocioDS_2_Dynamicfiltersoperator1 ,
                                              AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 ,
                                              AV52WWLinhaNegocioDS_4_Dynamicfiltersenabled2 ,
                                              AV53WWLinhaNegocioDS_5_Dynamicfiltersselector2 ,
                                              AV54WWLinhaNegocioDS_6_Dynamicfiltersoperator2 ,
                                              AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 ,
                                              AV56WWLinhaNegocioDS_8_Dynamicfiltersenabled3 ,
                                              AV57WWLinhaNegocioDS_9_Dynamicfiltersselector3 ,
                                              AV58WWLinhaNegocioDS_10_Dynamicfiltersoperator3 ,
                                              AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 ,
                                              AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel ,
                                              AV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador ,
                                              AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel ,
                                              AV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao ,
                                              AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod ,
                                              AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to ,
                                              A2037LinhaNegocio_Identificador ,
                                              A2038LinhaNegocio_Descricao ,
                                              A2035LinhaNegocio_GpoObjCtrlCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT
                                              }
         });
         lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 = StringUtil.Concat( StringUtil.RTrim( AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1), "%", "");
         lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 = StringUtil.Concat( StringUtil.RTrim( AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1), "%", "");
         lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 = StringUtil.Concat( StringUtil.RTrim( AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2), "%", "");
         lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 = StringUtil.Concat( StringUtil.RTrim( AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2), "%", "");
         lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 = StringUtil.Concat( StringUtil.RTrim( AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3), "%", "");
         lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 = StringUtil.Concat( StringUtil.RTrim( AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3), "%", "");
         lV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador = StringUtil.Concat( StringUtil.RTrim( AV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador), "%", "");
         lV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao = StringUtil.Concat( StringUtil.RTrim( AV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao), "%", "");
         /* Using cursor P00WM3 */
         pr_default.execute(1, new Object[] {lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1, lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1, lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2, lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2, lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3, lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3, lV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador, AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel, lV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao, AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel, AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod, AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKWM4 = false;
            A2038LinhaNegocio_Descricao = P00WM3_A2038LinhaNegocio_Descricao[0];
            n2038LinhaNegocio_Descricao = P00WM3_n2038LinhaNegocio_Descricao[0];
            A2035LinhaNegocio_GpoObjCtrlCod = P00WM3_A2035LinhaNegocio_GpoObjCtrlCod[0];
            A2037LinhaNegocio_Identificador = P00WM3_A2037LinhaNegocio_Identificador[0];
            A2036LinhadeNegocio_Codigo = P00WM3_A2036LinhadeNegocio_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00WM3_A2038LinhaNegocio_Descricao[0], A2038LinhaNegocio_Descricao) == 0 ) )
            {
               BRKWM4 = false;
               A2036LinhadeNegocio_Codigo = P00WM3_A2036LinhadeNegocio_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKWM4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2038LinhaNegocio_Descricao)) )
            {
               AV20Option = A2038LinhaNegocio_Descricao;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKWM4 )
            {
               BRKWM4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFLinhaNegocio_Identificador = "";
         AV11TFLinhaNegocio_Identificador_Sel = "";
         AV12TFLinhaNegocio_Descricao = "";
         AV13TFLinhaNegocio_Descricao_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV36LinhaNegocio_Identificador1 = "";
         AV38DynamicFiltersSelector2 = "";
         AV40LinhaNegocio_Identificador2 = "";
         AV42DynamicFiltersSelector3 = "";
         AV44LinhaNegocio_Identificador3 = "";
         AV49WWLinhaNegocioDS_1_Dynamicfiltersselector1 = "";
         AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 = "";
         AV53WWLinhaNegocioDS_5_Dynamicfiltersselector2 = "";
         AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 = "";
         AV57WWLinhaNegocioDS_9_Dynamicfiltersselector3 = "";
         AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 = "";
         AV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador = "";
         AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel = "";
         AV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao = "";
         AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel = "";
         scmdbuf = "";
         lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 = "";
         lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 = "";
         lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 = "";
         lV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador = "";
         lV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao = "";
         A2037LinhaNegocio_Identificador = "";
         A2038LinhaNegocio_Descricao = "";
         P00WM2_A2037LinhaNegocio_Identificador = new String[] {""} ;
         P00WM2_A2035LinhaNegocio_GpoObjCtrlCod = new int[1] ;
         P00WM2_A2038LinhaNegocio_Descricao = new String[] {""} ;
         P00WM2_n2038LinhaNegocio_Descricao = new bool[] {false} ;
         P00WM2_A2036LinhadeNegocio_Codigo = new int[1] ;
         AV20Option = "";
         P00WM3_A2038LinhaNegocio_Descricao = new String[] {""} ;
         P00WM3_n2038LinhaNegocio_Descricao = new bool[] {false} ;
         P00WM3_A2035LinhaNegocio_GpoObjCtrlCod = new int[1] ;
         P00WM3_A2037LinhaNegocio_Identificador = new String[] {""} ;
         P00WM3_A2036LinhadeNegocio_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwlinhanegociofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00WM2_A2037LinhaNegocio_Identificador, P00WM2_A2035LinhaNegocio_GpoObjCtrlCod, P00WM2_A2038LinhaNegocio_Descricao, P00WM2_n2038LinhaNegocio_Descricao, P00WM2_A2036LinhadeNegocio_Codigo
               }
               , new Object[] {
               P00WM3_A2038LinhaNegocio_Descricao, P00WM3_n2038LinhaNegocio_Descricao, P00WM3_A2035LinhaNegocio_GpoObjCtrlCod, P00WM3_A2037LinhaNegocio_Identificador, P00WM3_A2036LinhadeNegocio_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV35DynamicFiltersOperator1 ;
      private short AV39DynamicFiltersOperator2 ;
      private short AV43DynamicFiltersOperator3 ;
      private short AV50WWLinhaNegocioDS_2_Dynamicfiltersoperator1 ;
      private short AV54WWLinhaNegocioDS_6_Dynamicfiltersoperator2 ;
      private short AV58WWLinhaNegocioDS_10_Dynamicfiltersoperator3 ;
      private int AV47GXV1 ;
      private int AV14TFLinhaNegocio_GpoObjCtrlCod ;
      private int AV15TFLinhaNegocio_GpoObjCtrlCod_To ;
      private int AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod ;
      private int AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to ;
      private int A2035LinhaNegocio_GpoObjCtrlCod ;
      private int A2036LinhadeNegocio_Codigo ;
      private long AV28count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV37DynamicFiltersEnabled2 ;
      private bool AV41DynamicFiltersEnabled3 ;
      private bool AV52WWLinhaNegocioDS_4_Dynamicfiltersenabled2 ;
      private bool AV56WWLinhaNegocioDS_8_Dynamicfiltersenabled3 ;
      private bool BRKWM2 ;
      private bool n2038LinhaNegocio_Descricao ;
      private bool BRKWM4 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV10TFLinhaNegocio_Identificador ;
      private String AV11TFLinhaNegocio_Identificador_Sel ;
      private String AV12TFLinhaNegocio_Descricao ;
      private String AV13TFLinhaNegocio_Descricao_Sel ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV36LinhaNegocio_Identificador1 ;
      private String AV38DynamicFiltersSelector2 ;
      private String AV40LinhaNegocio_Identificador2 ;
      private String AV42DynamicFiltersSelector3 ;
      private String AV44LinhaNegocio_Identificador3 ;
      private String AV49WWLinhaNegocioDS_1_Dynamicfiltersselector1 ;
      private String AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 ;
      private String AV53WWLinhaNegocioDS_5_Dynamicfiltersselector2 ;
      private String AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 ;
      private String AV57WWLinhaNegocioDS_9_Dynamicfiltersselector3 ;
      private String AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 ;
      private String AV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador ;
      private String AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel ;
      private String AV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao ;
      private String AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel ;
      private String lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 ;
      private String lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 ;
      private String lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 ;
      private String lV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador ;
      private String lV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao ;
      private String A2037LinhaNegocio_Identificador ;
      private String A2038LinhaNegocio_Descricao ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00WM2_A2037LinhaNegocio_Identificador ;
      private int[] P00WM2_A2035LinhaNegocio_GpoObjCtrlCod ;
      private String[] P00WM2_A2038LinhaNegocio_Descricao ;
      private bool[] P00WM2_n2038LinhaNegocio_Descricao ;
      private int[] P00WM2_A2036LinhadeNegocio_Codigo ;
      private String[] P00WM3_A2038LinhaNegocio_Descricao ;
      private bool[] P00WM3_n2038LinhaNegocio_Descricao ;
      private int[] P00WM3_A2035LinhaNegocio_GpoObjCtrlCod ;
      private String[] P00WM3_A2037LinhaNegocio_Identificador ;
      private int[] P00WM3_A2036LinhadeNegocio_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getwwlinhanegociofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00WM2( IGxContext context ,
                                             String AV49WWLinhaNegocioDS_1_Dynamicfiltersselector1 ,
                                             short AV50WWLinhaNegocioDS_2_Dynamicfiltersoperator1 ,
                                             String AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 ,
                                             bool AV52WWLinhaNegocioDS_4_Dynamicfiltersenabled2 ,
                                             String AV53WWLinhaNegocioDS_5_Dynamicfiltersselector2 ,
                                             short AV54WWLinhaNegocioDS_6_Dynamicfiltersoperator2 ,
                                             String AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 ,
                                             bool AV56WWLinhaNegocioDS_8_Dynamicfiltersenabled3 ,
                                             String AV57WWLinhaNegocioDS_9_Dynamicfiltersselector3 ,
                                             short AV58WWLinhaNegocioDS_10_Dynamicfiltersoperator3 ,
                                             String AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 ,
                                             String AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel ,
                                             String AV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador ,
                                             String AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel ,
                                             String AV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao ,
                                             int AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod ,
                                             int AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to ,
                                             String A2037LinhaNegocio_Identificador ,
                                             String A2038LinhaNegocio_Descricao ,
                                             int A2035LinhaNegocio_GpoObjCtrlCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [LinhaNegocio_Identificador], [LinhaNegocio_GpoObjCtrlCod], [LinhaNegocio_Descricao], [LinhadeNegocio_Codigo] FROM [LinhaNegocio] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV49WWLinhaNegocioDS_1_Dynamicfiltersselector1, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV50WWLinhaNegocioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49WWLinhaNegocioDS_1_Dynamicfiltersselector1, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV50WWLinhaNegocioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like '%' + @lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like '%' + @lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV52WWLinhaNegocioDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWLinhaNegocioDS_5_Dynamicfiltersselector2, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV54WWLinhaNegocioDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV52WWLinhaNegocioDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWLinhaNegocioDS_5_Dynamicfiltersselector2, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV54WWLinhaNegocioDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like '%' + @lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like '%' + @lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV56WWLinhaNegocioDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWLinhaNegocioDS_9_Dynamicfiltersselector3, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV58WWLinhaNegocioDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV56WWLinhaNegocioDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWLinhaNegocioDS_9_Dynamicfiltersselector3, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV58WWLinhaNegocioDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like '%' + @lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like '%' + @lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] = @AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] = @AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Descricao] like @lV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Descricao] like @lV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Descricao] = @AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Descricao] = @AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (0==AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_GpoObjCtrlCod] >= @AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_GpoObjCtrlCod] >= @AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (0==AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_GpoObjCtrlCod] <= @AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_GpoObjCtrlCod] <= @AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [LinhaNegocio_Identificador]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00WM3( IGxContext context ,
                                             String AV49WWLinhaNegocioDS_1_Dynamicfiltersselector1 ,
                                             short AV50WWLinhaNegocioDS_2_Dynamicfiltersoperator1 ,
                                             String AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1 ,
                                             bool AV52WWLinhaNegocioDS_4_Dynamicfiltersenabled2 ,
                                             String AV53WWLinhaNegocioDS_5_Dynamicfiltersselector2 ,
                                             short AV54WWLinhaNegocioDS_6_Dynamicfiltersoperator2 ,
                                             String AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2 ,
                                             bool AV56WWLinhaNegocioDS_8_Dynamicfiltersenabled3 ,
                                             String AV57WWLinhaNegocioDS_9_Dynamicfiltersselector3 ,
                                             short AV58WWLinhaNegocioDS_10_Dynamicfiltersoperator3 ,
                                             String AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3 ,
                                             String AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel ,
                                             String AV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador ,
                                             String AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel ,
                                             String AV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao ,
                                             int AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod ,
                                             int AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to ,
                                             String A2037LinhaNegocio_Identificador ,
                                             String A2038LinhaNegocio_Descricao ,
                                             int A2035LinhaNegocio_GpoObjCtrlCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [LinhaNegocio_Descricao], [LinhaNegocio_GpoObjCtrlCod], [LinhaNegocio_Identificador], [LinhadeNegocio_Codigo] FROM [LinhaNegocio] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV49WWLinhaNegocioDS_1_Dynamicfiltersselector1, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV50WWLinhaNegocioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49WWLinhaNegocioDS_1_Dynamicfiltersselector1, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV50WWLinhaNegocioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWLinhaNegocioDS_3_Linhanegocio_identificador1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like '%' + @lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like '%' + @lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV52WWLinhaNegocioDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWLinhaNegocioDS_5_Dynamicfiltersselector2, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV54WWLinhaNegocioDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV52WWLinhaNegocioDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWLinhaNegocioDS_5_Dynamicfiltersselector2, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV54WWLinhaNegocioDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWLinhaNegocioDS_7_Linhanegocio_identificador2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like '%' + @lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like '%' + @lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV56WWLinhaNegocioDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWLinhaNegocioDS_9_Dynamicfiltersselector3, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV58WWLinhaNegocioDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV56WWLinhaNegocioDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWLinhaNegocioDS_9_Dynamicfiltersselector3, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV58WWLinhaNegocioDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWLinhaNegocioDS_11_Linhanegocio_identificador3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like '%' + @lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like '%' + @lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] = @AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] = @AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Descricao] like @lV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Descricao] like @lV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Descricao] = @AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Descricao] = @AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (0==AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_GpoObjCtrlCod] >= @AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_GpoObjCtrlCod] >= @AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (0==AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_GpoObjCtrlCod] <= @AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_GpoObjCtrlCod] <= @AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [LinhaNegocio_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00WM2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] );
               case 1 :
                     return conditional_P00WM3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WM2 ;
          prmP00WM2 = new Object[] {
          new Object[] {"@lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WM3 ;
          prmP00WM3 = new Object[] {
          new Object[] {"@lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV51WWLinhaNegocioDS_3_Linhanegocio_identificador1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV55WWLinhaNegocioDS_7_Linhanegocio_identificador2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV59WWLinhaNegocioDS_11_Linhanegocio_identificador3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV60WWLinhaNegocioDS_12_Tflinhanegocio_identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV61WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV62WWLinhaNegocioDS_14_Tflinhanegocio_descricao",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV63WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV64WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WM2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WM2,100,0,true,false )
             ,new CursorDef("P00WM3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WM3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwlinhanegociofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwlinhanegociofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwlinhanegociofilterdata") )
          {
             return  ;
          }
          getwwlinhanegociofilterdata worker = new getwwlinhanegociofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
