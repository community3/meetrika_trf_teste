/*
               File: PRC_RenumeraOSFM
        Description: Renumera OSFM
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:33.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_renumeraosfm : GXProcedure
   {
      public prc_renumeraosfm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_renumeraosfm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_LoteAceiteCod ,
                           ref short aP1_Contratada_NumeroOS )
      {
         this.AV9ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         this.AV8Contratada_NumeroOS = aP1_Contratada_NumeroOS;
         initialize();
         executePrivate();
         aP0_ContagemResultado_LoteAceiteCod=this.AV9ContagemResultado_LoteAceiteCod;
         aP1_Contratada_NumeroOS=this.AV8Contratada_NumeroOS;
      }

      public short executeUdp( ref int aP0_ContagemResultado_LoteAceiteCod )
      {
         this.AV9ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         this.AV8Contratada_NumeroOS = aP1_Contratada_NumeroOS;
         initialize();
         executePrivate();
         aP0_ContagemResultado_LoteAceiteCod=this.AV9ContagemResultado_LoteAceiteCod;
         aP1_Contratada_NumeroOS=this.AV8Contratada_NumeroOS;
         return AV8Contratada_NumeroOS ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_LoteAceiteCod ,
                                 ref short aP1_Contratada_NumeroOS )
      {
         prc_renumeraosfm objprc_renumeraosfm;
         objprc_renumeraosfm = new prc_renumeraosfm();
         objprc_renumeraosfm.AV9ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         objprc_renumeraosfm.AV8Contratada_NumeroOS = aP1_Contratada_NumeroOS;
         objprc_renumeraosfm.context.SetSubmitInitialConfig(context);
         objprc_renumeraosfm.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_renumeraosfm);
         aP0_ContagemResultado_LoteAceiteCod=this.AV9ContagemResultado_LoteAceiteCod;
         aP1_Contratada_NumeroOS=this.AV8Contratada_NumeroOS;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_renumeraosfm)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P008H2 */
         pr_default.execute(0, new Object[] {AV9ContagemResultado_LoteAceiteCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A489ContagemResultado_SistemaCod = P008H2_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P008H2_n489ContagemResultado_SistemaCod[0];
            A597ContagemResultado_LoteAceiteCod = P008H2_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P008H2_n597ContagemResultado_LoteAceiteCod[0];
            A509ContagemrResultado_SistemaSigla = P008H2_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P008H2_n509ContagemrResultado_SistemaSigla[0];
            A515ContagemResultado_SistemaCoord = P008H2_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P008H2_n515ContagemResultado_SistemaCoord[0];
            A509ContagemrResultado_SistemaSigla = P008H2_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P008H2_n509ContagemrResultado_SistemaSigla[0];
            A515ContagemResultado_SistemaCoord = P008H2_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P008H2_n515ContagemResultado_SistemaCoord[0];
            AV8Contratada_NumeroOS = (short)(AV8Contratada_NumeroOS+1);
            AV10ContagemrResultado_SistemaSigla = A509ContagemrResultado_SistemaSigla;
            /* Execute user subroutine: 'NUMERA' */
            S111 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               this.cleanup();
               if (true) return;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'NUMERA' Routine */
         /* Using cursor P008H3 */
         pr_default.execute(1, new Object[] {AV9ContagemResultado_LoteAceiteCod, AV10ContagemrResultado_SistemaSigla});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A489ContagemResultado_SistemaCod = P008H3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P008H3_n489ContagemResultado_SistemaCod[0];
            A509ContagemrResultado_SistemaSigla = P008H3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P008H3_n509ContagemrResultado_SistemaSigla[0];
            A597ContagemResultado_LoteAceiteCod = P008H3_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P008H3_n597ContagemResultado_LoteAceiteCod[0];
            A493ContagemResultado_DemandaFM = P008H3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P008H3_n493ContagemResultado_DemandaFM[0];
            A456ContagemResultado_Codigo = P008H3_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = P008H3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P008H3_n509ContagemrResultado_SistemaSigla[0];
            A493ContagemResultado_DemandaFM = StringUtil.Trim( StringUtil.Str( (decimal)(AV8Contratada_NumeroOS), 4, 0));
            n493ContagemResultado_DemandaFM = false;
            BatchSize = 100;
            pr_default.initializeBatch( 2, BatchSize, this, "Executebatchp008h4");
            /* Using cursor P008H4 */
            pr_default.addRecord(2, new Object[] {n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, A456ContagemResultado_Codigo});
            if ( pr_default.recordCount(2) == pr_default.getBatchSize(2) )
            {
               Executebatchp008h4( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(1);
         }
         if ( pr_default.getBatchSize(2) > 0 )
         {
            Executebatchp008h4( ) ;
         }
         pr_default.close(1);
      }

      protected void Executebatchp008h4( )
      {
         /* Using cursor P008H4 */
         pr_default.executeBatch(2);
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_RenumeraOSFM");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(2);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P008H2_A489ContagemResultado_SistemaCod = new int[1] ;
         P008H2_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P008H2_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P008H2_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P008H2_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P008H2_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P008H2_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P008H2_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         A509ContagemrResultado_SistemaSigla = "";
         A515ContagemResultado_SistemaCoord = "";
         AV10ContagemrResultado_SistemaSigla = "";
         P008H3_A489ContagemResultado_SistemaCod = new int[1] ;
         P008H3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P008H3_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P008H3_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P008H3_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P008H3_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P008H3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P008H3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P008H3_A456ContagemResultado_Codigo = new int[1] ;
         A493ContagemResultado_DemandaFM = "";
         P008H4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P008H4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P008H4_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_renumeraosfm__default(),
            new Object[][] {
                new Object[] {
               P008H2_A489ContagemResultado_SistemaCod, P008H2_n489ContagemResultado_SistemaCod, P008H2_A597ContagemResultado_LoteAceiteCod, P008H2_n597ContagemResultado_LoteAceiteCod, P008H2_A509ContagemrResultado_SistemaSigla, P008H2_n509ContagemrResultado_SistemaSigla, P008H2_A515ContagemResultado_SistemaCoord, P008H2_n515ContagemResultado_SistemaCoord
               }
               , new Object[] {
               P008H3_A489ContagemResultado_SistemaCod, P008H3_n489ContagemResultado_SistemaCod, P008H3_A509ContagemrResultado_SistemaSigla, P008H3_n509ContagemrResultado_SistemaSigla, P008H3_A597ContagemResultado_LoteAceiteCod, P008H3_n597ContagemResultado_LoteAceiteCod, P008H3_A493ContagemResultado_DemandaFM, P008H3_n493ContagemResultado_DemandaFM, P008H3_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8Contratada_NumeroOS ;
      private int AV9ContagemResultado_LoteAceiteCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A456ContagemResultado_Codigo ;
      private int BatchSize ;
      private String scmdbuf ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String AV10ContagemrResultado_SistemaSigla ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool returnInSub ;
      private bool n493ContagemResultado_DemandaFM ;
      private String A515ContagemResultado_SistemaCoord ;
      private String A493ContagemResultado_DemandaFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_LoteAceiteCod ;
      private short aP1_Contratada_NumeroOS ;
      private IDataStoreProvider pr_default ;
      private int[] P008H2_A489ContagemResultado_SistemaCod ;
      private bool[] P008H2_n489ContagemResultado_SistemaCod ;
      private int[] P008H2_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P008H2_n597ContagemResultado_LoteAceiteCod ;
      private String[] P008H2_A509ContagemrResultado_SistemaSigla ;
      private bool[] P008H2_n509ContagemrResultado_SistemaSigla ;
      private String[] P008H2_A515ContagemResultado_SistemaCoord ;
      private bool[] P008H2_n515ContagemResultado_SistemaCoord ;
      private int[] P008H3_A489ContagemResultado_SistemaCod ;
      private bool[] P008H3_n489ContagemResultado_SistemaCod ;
      private String[] P008H3_A509ContagemrResultado_SistemaSigla ;
      private bool[] P008H3_n509ContagemrResultado_SistemaSigla ;
      private int[] P008H3_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P008H3_n597ContagemResultado_LoteAceiteCod ;
      private String[] P008H3_A493ContagemResultado_DemandaFM ;
      private bool[] P008H3_n493ContagemResultado_DemandaFM ;
      private int[] P008H3_A456ContagemResultado_Codigo ;
      private String[] P008H4_A493ContagemResultado_DemandaFM ;
      private bool[] P008H4_n493ContagemResultado_DemandaFM ;
      private int[] P008H4_A456ContagemResultado_Codigo ;
   }

   public class prc_renumeraosfm__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new BatchUpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008H2 ;
          prmP008H2 = new Object[] {
          new Object[] {"@AV9ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008H3 ;
          prmP008H3 = new Object[] {
          new Object[] {"@AV9ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10ContagemrResultado_SistemaSigla",SqlDbType.Char,25,0}
          } ;
          Object[] prmP008H4 ;
          prmP008H4 = new Object[] {
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008H2", "SELECT DISTINCT NULL AS [ContagemResultado_SistemaCod], NULL AS [ContagemResultado_LoteAceiteCod], [ContagemrResultado_SistemaSigla], [ContagemResultado_SistemaCoord] FROM ( SELECT TOP(100) PERCENT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_LoteAceiteCod], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV9ContagemResultado_LoteAceiteCod ORDER BY T2.[Sistema_Coordenacao], T2.[Sistema_Sigla]) DistinctT ORDER BY [ContagemResultado_SistemaCoord], [ContagemrResultado_SistemaSigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008H2,100,0,true,false )
             ,new CursorDef("P008H3", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE (T1.[ContagemResultado_LoteAceiteCod] = @AV9ContagemResultado_LoteAceiteCod) AND (T2.[Sistema_Sigla] = @AV10ContagemrResultado_SistemaSigla) ORDER BY T1.[ContagemResultado_LoteAceiteCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008H3,1,0,true,false )
             ,new CursorDef("P008H4", "UPDATE [ContagemResultado] SET [ContagemResultado_DemandaFM]=@ContagemResultado_DemandaFM  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008H4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 25) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
