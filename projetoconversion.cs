/*
               File: ProjetoConversion
        Description: Conversion for table Projeto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/23/2020 23:35:4.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Reorg;
using System.Threading;
using GeneXus.Programs;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
namespace GeneXus.Programs {
   public class projetoconversion : GXProcedure
   {
      public projetoconversion( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public projetoconversion( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         projetoconversion objprojetoconversion;
         objprojetoconversion = new projetoconversion();
         objprojetoconversion.context.SetSubmitInitialConfig(context);
         objprojetoconversion.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprojetoconversion);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((projetoconversion)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         cmdBuffer=" SET IDENTITY_INSERT [GXA0086] ON "
         ;
         RGZ = new GxCommand(dsDefault.Db, cmdBuffer, dsDefault,0,true,false,null);
         RGZ.ErrorMask = GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK;
         RGZ.ExecuteStmt() ;
         RGZ.Drop();
         /* Using cursor PROJETOCON2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2161Projeto_AreaTrabalhoCodigo = PROJETOCON2_A2161Projeto_AreaTrabalhoCodigo[0];
            n2161Projeto_AreaTrabalhoCodigo = PROJETOCON2_n2161Projeto_AreaTrabalhoCodigo[0];
            A2160Projeto_CustoPrevisto = PROJETOCON2_A2160Projeto_CustoPrevisto[0];
            n2160Projeto_CustoPrevisto = PROJETOCON2_n2160Projeto_CustoPrevisto[0];
            A2159Projeto_EsforcoPrevisto = PROJETOCON2_A2159Projeto_EsforcoPrevisto[0];
            n2159Projeto_EsforcoPrevisto = PROJETOCON2_n2159Projeto_EsforcoPrevisto[0];
            A2158Projeto_PrazoPrevisto = PROJETOCON2_A2158Projeto_PrazoPrevisto[0];
            n2158Projeto_PrazoPrevisto = PROJETOCON2_n2158Projeto_PrazoPrevisto[0];
            A2157Projeto_DTFim = PROJETOCON2_A2157Projeto_DTFim[0];
            n2157Projeto_DTFim = PROJETOCON2_n2157Projeto_DTFim[0];
            A2156Projeto_DTInicio = PROJETOCON2_A2156Projeto_DTInicio[0];
            n2156Projeto_DTInicio = PROJETOCON2_n2156Projeto_DTInicio[0];
            A2155Projeto_Objetivo = PROJETOCON2_A2155Projeto_Objetivo[0];
            n2155Projeto_Objetivo = PROJETOCON2_n2155Projeto_Objetivo[0];
            A2154Projeto_Identificador = PROJETOCON2_A2154Projeto_Identificador[0];
            n2154Projeto_Identificador = PROJETOCON2_n2154Projeto_Identificador[0];
            A1543Projeto_ServicoCod = PROJETOCON2_A1543Projeto_ServicoCod[0];
            n1543Projeto_ServicoCod = PROJETOCON2_n1543Projeto_ServicoCod[0];
            A1542Projeto_GerenteCod = PROJETOCON2_A1542Projeto_GerenteCod[0];
            n1542Projeto_GerenteCod = PROJETOCON2_n1542Projeto_GerenteCod[0];
            A1541Projeto_Previsao = PROJETOCON2_A1541Projeto_Previsao[0];
            n1541Projeto_Previsao = PROJETOCON2_n1541Projeto_Previsao[0];
            A1540Projeto_Introducao = PROJETOCON2_A1540Projeto_Introducao[0];
            n1540Projeto_Introducao = PROJETOCON2_n1540Projeto_Introducao[0];
            A1232Projeto_Incremental = PROJETOCON2_A1232Projeto_Incremental[0];
            n1232Projeto_Incremental = PROJETOCON2_n1232Projeto_Incremental[0];
            A990Projeto_ConstACocomo = PROJETOCON2_A990Projeto_ConstACocomo[0];
            n990Projeto_ConstACocomo = PROJETOCON2_n990Projeto_ConstACocomo[0];
            A989Projeto_FatorMultiplicador = PROJETOCON2_A989Projeto_FatorMultiplicador[0];
            n989Projeto_FatorMultiplicador = PROJETOCON2_n989Projeto_FatorMultiplicador[0];
            A985Projeto_FatorEscala = PROJETOCON2_A985Projeto_FatorEscala[0];
            n985Projeto_FatorEscala = PROJETOCON2_n985Projeto_FatorEscala[0];
            A983Projeto_TipoProjetoCod = PROJETOCON2_A983Projeto_TipoProjetoCod[0];
            n983Projeto_TipoProjetoCod = PROJETOCON2_n983Projeto_TipoProjetoCod[0];
            A658Projeto_Status = PROJETOCON2_A658Projeto_Status[0];
            A653Projeto_Escopo = PROJETOCON2_A653Projeto_Escopo[0];
            A652Projeto_TecnicaContagem = PROJETOCON2_A652Projeto_TecnicaContagem[0];
            A650Projeto_Sigla = PROJETOCON2_A650Projeto_Sigla[0];
            A649Projeto_Nome = PROJETOCON2_A649Projeto_Nome[0];
            A648Projeto_Codigo = PROJETOCON2_A648Projeto_Codigo[0];
            /*
               INSERT RECORD ON TABLE GXA0086

            */
            AV2Projeto_Codigo = A648Projeto_Codigo;
            AV3Projeto_Nome = A649Projeto_Nome;
            AV4Projeto_Sigla = A650Projeto_Sigla;
            AV5Projeto_TecnicaContagem = A652Projeto_TecnicaContagem;
            AV6Projeto_Escopo = A653Projeto_Escopo;
            AV7Projeto_Status = A658Projeto_Status;
            if ( PROJETOCON2_n983Projeto_TipoProjetoCod[0] )
            {
               AV8Projeto_TipoProjetoCod = 0;
               nV8Projeto_TipoProjetoCod = false;
               nV8Projeto_TipoProjetoCod = true;
            }
            else
            {
               AV8Projeto_TipoProjetoCod = A983Projeto_TipoProjetoCod;
               nV8Projeto_TipoProjetoCod = false;
            }
            if ( PROJETOCON2_n985Projeto_FatorEscala[0] )
            {
               AV9Projeto_FatorEscala = 0;
               nV9Projeto_FatorEscala = false;
               nV9Projeto_FatorEscala = true;
            }
            else
            {
               AV9Projeto_FatorEscala = A985Projeto_FatorEscala;
               nV9Projeto_FatorEscala = false;
            }
            if ( PROJETOCON2_n989Projeto_FatorMultiplicador[0] )
            {
               AV10Projeto_FatorMultiplicador = 0;
               nV10Projeto_FatorMultiplicador = false;
               nV10Projeto_FatorMultiplicador = true;
            }
            else
            {
               AV10Projeto_FatorMultiplicador = A989Projeto_FatorMultiplicador;
               nV10Projeto_FatorMultiplicador = false;
            }
            if ( PROJETOCON2_n990Projeto_ConstACocomo[0] )
            {
               AV11Projeto_ConstACocomo = 0;
               nV11Projeto_ConstACocomo = false;
               nV11Projeto_ConstACocomo = true;
            }
            else
            {
               AV11Projeto_ConstACocomo = A990Projeto_ConstACocomo;
               nV11Projeto_ConstACocomo = false;
            }
            if ( PROJETOCON2_n1232Projeto_Incremental[0] )
            {
               AV12Projeto_Incremental = false;
               nV12Projeto_Incremental = false;
               nV12Projeto_Incremental = true;
            }
            else
            {
               AV12Projeto_Incremental = A1232Projeto_Incremental;
               nV12Projeto_Incremental = false;
            }
            if ( PROJETOCON2_n1540Projeto_Introducao[0] )
            {
               AV13Projeto_Introducao = "";
               nV13Projeto_Introducao = false;
               nV13Projeto_Introducao = true;
            }
            else
            {
               AV13Projeto_Introducao = A1540Projeto_Introducao;
               nV13Projeto_Introducao = false;
            }
            if ( PROJETOCON2_n1541Projeto_Previsao[0] )
            {
               AV14Projeto_Previsao = DateTime.MinValue;
               nV14Projeto_Previsao = false;
               nV14Projeto_Previsao = true;
            }
            else
            {
               AV14Projeto_Previsao = A1541Projeto_Previsao;
               nV14Projeto_Previsao = false;
            }
            if ( PROJETOCON2_n1542Projeto_GerenteCod[0] )
            {
               AV15Projeto_GerenteCod = 0;
               nV15Projeto_GerenteCod = false;
               nV15Projeto_GerenteCod = true;
            }
            else
            {
               AV15Projeto_GerenteCod = A1542Projeto_GerenteCod;
               nV15Projeto_GerenteCod = false;
            }
            if ( PROJETOCON2_n1543Projeto_ServicoCod[0] )
            {
               AV16Projeto_ServicoCod = 0;
               nV16Projeto_ServicoCod = false;
               nV16Projeto_ServicoCod = true;
            }
            else
            {
               AV16Projeto_ServicoCod = A1543Projeto_ServicoCod;
               nV16Projeto_ServicoCod = false;
            }
            if ( PROJETOCON2_n2154Projeto_Identificador[0] )
            {
               AV17Projeto_Identificador = "";
               nV17Projeto_Identificador = false;
               nV17Projeto_Identificador = true;
            }
            else
            {
               AV17Projeto_Identificador = A2154Projeto_Identificador;
               nV17Projeto_Identificador = false;
            }
            if ( PROJETOCON2_n2155Projeto_Objetivo[0] )
            {
               AV18Projeto_Objetivo = "";
               nV18Projeto_Objetivo = false;
               nV18Projeto_Objetivo = true;
            }
            else
            {
               AV18Projeto_Objetivo = A2155Projeto_Objetivo;
               nV18Projeto_Objetivo = false;
            }
            if ( PROJETOCON2_n2156Projeto_DTInicio[0] )
            {
               AV19Projeto_DTInicio = DateTime.MinValue;
               nV19Projeto_DTInicio = false;
               nV19Projeto_DTInicio = true;
            }
            else
            {
               AV19Projeto_DTInicio = A2156Projeto_DTInicio;
               nV19Projeto_DTInicio = false;
            }
            if ( PROJETOCON2_n2157Projeto_DTFim[0] )
            {
               AV20Projeto_DTFim = DateTime.MinValue;
               nV20Projeto_DTFim = false;
               nV20Projeto_DTFim = true;
            }
            else
            {
               AV20Projeto_DTFim = A2157Projeto_DTFim;
               nV20Projeto_DTFim = false;
            }
            if ( PROJETOCON2_n2158Projeto_PrazoPrevisto[0] )
            {
               AV21Projeto_PrazoPrevisto = 0;
               nV21Projeto_PrazoPrevisto = false;
               nV21Projeto_PrazoPrevisto = true;
            }
            else
            {
               AV21Projeto_PrazoPrevisto = A2158Projeto_PrazoPrevisto;
               nV21Projeto_PrazoPrevisto = false;
            }
            if ( PROJETOCON2_n2159Projeto_EsforcoPrevisto[0] )
            {
               AV22Projeto_EsforcoPrevisto = 0;
               nV22Projeto_EsforcoPrevisto = false;
               nV22Projeto_EsforcoPrevisto = true;
            }
            else
            {
               AV22Projeto_EsforcoPrevisto = A2159Projeto_EsforcoPrevisto;
               nV22Projeto_EsforcoPrevisto = false;
            }
            if ( PROJETOCON2_n2160Projeto_CustoPrevisto[0] )
            {
               AV23Projeto_CustoPrevisto = 0;
               nV23Projeto_CustoPrevisto = false;
               nV23Projeto_CustoPrevisto = true;
            }
            else
            {
               AV23Projeto_CustoPrevisto = A2160Projeto_CustoPrevisto;
               nV23Projeto_CustoPrevisto = false;
            }
            if ( PROJETOCON2_n2161Projeto_AreaTrabalhoCodigo[0] )
            {
               AV24Projeto_AreaTrabalhoCodigo = 0;
               nV24Projeto_AreaTrabalhoCodigo = false;
               nV24Projeto_AreaTrabalhoCodigo = true;
            }
            else
            {
               AV24Projeto_AreaTrabalhoCodigo = A2161Projeto_AreaTrabalhoCodigo;
               nV24Projeto_AreaTrabalhoCodigo = false;
            }
            if ( (0==A1826GpoObjCtrl_Codigo) )
            {
               AV25Projeto_GpoObjCtrlCodigo = 0;
            }
            else
            {
               AV25Projeto_GpoObjCtrlCodigo = A1826GpoObjCtrl_Codigo;
            }
            AV26Projeto_SistemaCodigo = 0;
            AV27Projeto_ModuloCodigo = 0;
            /* Using cursor PROJETOCON3 */
            pr_default.execute(1, new Object[] {AV2Projeto_Codigo, AV3Projeto_Nome, AV4Projeto_Sigla, AV5Projeto_TecnicaContagem, AV6Projeto_Escopo, AV7Projeto_Status, nV8Projeto_TipoProjetoCod, AV8Projeto_TipoProjetoCod, nV9Projeto_FatorEscala, AV9Projeto_FatorEscala, nV10Projeto_FatorMultiplicador, AV10Projeto_FatorMultiplicador, nV11Projeto_ConstACocomo, AV11Projeto_ConstACocomo, nV12Projeto_Incremental, AV12Projeto_Incremental, nV13Projeto_Introducao, AV13Projeto_Introducao, nV14Projeto_Previsao, AV14Projeto_Previsao, nV15Projeto_GerenteCod, AV15Projeto_GerenteCod, nV16Projeto_ServicoCod, AV16Projeto_ServicoCod, nV17Projeto_Identificador, AV17Projeto_Identificador, nV18Projeto_Objetivo, AV18Projeto_Objetivo, nV19Projeto_DTInicio, AV19Projeto_DTInicio, nV20Projeto_DTFim, AV20Projeto_DTFim, nV21Projeto_PrazoPrevisto, AV21Projeto_PrazoPrevisto, nV22Projeto_EsforcoPrevisto, AV22Projeto_EsforcoPrevisto, nV23Projeto_CustoPrevisto, AV23Projeto_CustoPrevisto, nV24Projeto_AreaTrabalhoCodigo, AV24Projeto_AreaTrabalhoCodigo, AV25Projeto_GpoObjCtrlCodigo, AV26Projeto_SistemaCodigo, AV27Projeto_ModuloCodigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("GXA0086") ;
            if ( (pr_default.getStatus(1) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(GXResourceManager.GetMessage("GXM_noupdate"));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            pr_default.readNext(0);
         }
         pr_default.close(0);
         cmdBuffer=" SET IDENTITY_INSERT [GXA0086] OFF "
         ;
         RGZ = new GxCommand(dsDefault.Db, cmdBuffer, dsDefault,0,true,false,null);
         RGZ.ErrorMask = GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK;
         RGZ.ExecuteStmt() ;
         RGZ.Drop();
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         cmdBuffer = "";
         scmdbuf = "";
         PROJETOCON2_A2161Projeto_AreaTrabalhoCodigo = new int[1] ;
         PROJETOCON2_n2161Projeto_AreaTrabalhoCodigo = new bool[] {false} ;
         PROJETOCON2_A2160Projeto_CustoPrevisto = new decimal[1] ;
         PROJETOCON2_n2160Projeto_CustoPrevisto = new bool[] {false} ;
         PROJETOCON2_A2159Projeto_EsforcoPrevisto = new short[1] ;
         PROJETOCON2_n2159Projeto_EsforcoPrevisto = new bool[] {false} ;
         PROJETOCON2_A2158Projeto_PrazoPrevisto = new short[1] ;
         PROJETOCON2_n2158Projeto_PrazoPrevisto = new bool[] {false} ;
         PROJETOCON2_A2157Projeto_DTFim = new DateTime[] {DateTime.MinValue} ;
         PROJETOCON2_n2157Projeto_DTFim = new bool[] {false} ;
         PROJETOCON2_A2156Projeto_DTInicio = new DateTime[] {DateTime.MinValue} ;
         PROJETOCON2_n2156Projeto_DTInicio = new bool[] {false} ;
         PROJETOCON2_A2155Projeto_Objetivo = new String[] {""} ;
         PROJETOCON2_n2155Projeto_Objetivo = new bool[] {false} ;
         PROJETOCON2_A2154Projeto_Identificador = new String[] {""} ;
         PROJETOCON2_n2154Projeto_Identificador = new bool[] {false} ;
         PROJETOCON2_A1543Projeto_ServicoCod = new int[1] ;
         PROJETOCON2_n1543Projeto_ServicoCod = new bool[] {false} ;
         PROJETOCON2_A1542Projeto_GerenteCod = new int[1] ;
         PROJETOCON2_n1542Projeto_GerenteCod = new bool[] {false} ;
         PROJETOCON2_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         PROJETOCON2_n1541Projeto_Previsao = new bool[] {false} ;
         PROJETOCON2_A1540Projeto_Introducao = new String[] {""} ;
         PROJETOCON2_n1540Projeto_Introducao = new bool[] {false} ;
         PROJETOCON2_A1232Projeto_Incremental = new bool[] {false} ;
         PROJETOCON2_n1232Projeto_Incremental = new bool[] {false} ;
         PROJETOCON2_A990Projeto_ConstACocomo = new decimal[1] ;
         PROJETOCON2_n990Projeto_ConstACocomo = new bool[] {false} ;
         PROJETOCON2_A989Projeto_FatorMultiplicador = new decimal[1] ;
         PROJETOCON2_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         PROJETOCON2_A985Projeto_FatorEscala = new decimal[1] ;
         PROJETOCON2_n985Projeto_FatorEscala = new bool[] {false} ;
         PROJETOCON2_A983Projeto_TipoProjetoCod = new int[1] ;
         PROJETOCON2_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         PROJETOCON2_A658Projeto_Status = new String[] {""} ;
         PROJETOCON2_A653Projeto_Escopo = new String[] {""} ;
         PROJETOCON2_A652Projeto_TecnicaContagem = new String[] {""} ;
         PROJETOCON2_A650Projeto_Sigla = new String[] {""} ;
         PROJETOCON2_A649Projeto_Nome = new String[] {""} ;
         PROJETOCON2_A648Projeto_Codigo = new int[1] ;
         A2157Projeto_DTFim = DateTime.MinValue;
         A2156Projeto_DTInicio = DateTime.MinValue;
         A2155Projeto_Objetivo = "";
         A2154Projeto_Identificador = "";
         A1541Projeto_Previsao = DateTime.MinValue;
         A1540Projeto_Introducao = "";
         A658Projeto_Status = "";
         A653Projeto_Escopo = "";
         A652Projeto_TecnicaContagem = "";
         A650Projeto_Sigla = "";
         A649Projeto_Nome = "";
         AV3Projeto_Nome = "";
         AV4Projeto_Sigla = "";
         AV5Projeto_TecnicaContagem = "";
         AV6Projeto_Escopo = "";
         AV7Projeto_Status = "";
         AV13Projeto_Introducao = "";
         AV14Projeto_Previsao = DateTime.MinValue;
         AV17Projeto_Identificador = "";
         AV18Projeto_Objetivo = "";
         AV19Projeto_DTInicio = DateTime.MinValue;
         AV20Projeto_DTFim = DateTime.MinValue;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.projetoconversion__default(),
            new Object[][] {
                new Object[] {
               PROJETOCON2_A2161Projeto_AreaTrabalhoCodigo, PROJETOCON2_n2161Projeto_AreaTrabalhoCodigo, PROJETOCON2_A2160Projeto_CustoPrevisto, PROJETOCON2_n2160Projeto_CustoPrevisto, PROJETOCON2_A2159Projeto_EsforcoPrevisto, PROJETOCON2_n2159Projeto_EsforcoPrevisto, PROJETOCON2_A2158Projeto_PrazoPrevisto, PROJETOCON2_n2158Projeto_PrazoPrevisto, PROJETOCON2_A2157Projeto_DTFim, PROJETOCON2_n2157Projeto_DTFim,
               PROJETOCON2_A2156Projeto_DTInicio, PROJETOCON2_n2156Projeto_DTInicio, PROJETOCON2_A2155Projeto_Objetivo, PROJETOCON2_n2155Projeto_Objetivo, PROJETOCON2_A2154Projeto_Identificador, PROJETOCON2_n2154Projeto_Identificador, PROJETOCON2_A1543Projeto_ServicoCod, PROJETOCON2_n1543Projeto_ServicoCod, PROJETOCON2_A1542Projeto_GerenteCod, PROJETOCON2_n1542Projeto_GerenteCod,
               PROJETOCON2_A1541Projeto_Previsao, PROJETOCON2_n1541Projeto_Previsao, PROJETOCON2_A1540Projeto_Introducao, PROJETOCON2_n1540Projeto_Introducao, PROJETOCON2_A1232Projeto_Incremental, PROJETOCON2_n1232Projeto_Incremental, PROJETOCON2_A990Projeto_ConstACocomo, PROJETOCON2_n990Projeto_ConstACocomo, PROJETOCON2_A989Projeto_FatorMultiplicador, PROJETOCON2_n989Projeto_FatorMultiplicador,
               PROJETOCON2_A985Projeto_FatorEscala, PROJETOCON2_n985Projeto_FatorEscala, PROJETOCON2_A983Projeto_TipoProjetoCod, PROJETOCON2_n983Projeto_TipoProjetoCod, PROJETOCON2_A658Projeto_Status, PROJETOCON2_A653Projeto_Escopo, PROJETOCON2_A652Projeto_TecnicaContagem, PROJETOCON2_A650Projeto_Sigla, PROJETOCON2_A649Projeto_Nome, PROJETOCON2_A648Projeto_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A2159Projeto_EsforcoPrevisto ;
      private short A2158Projeto_PrazoPrevisto ;
      private short AV21Projeto_PrazoPrevisto ;
      private short AV22Projeto_EsforcoPrevisto ;
      private int A2161Projeto_AreaTrabalhoCodigo ;
      private int A1543Projeto_ServicoCod ;
      private int A1542Projeto_GerenteCod ;
      private int A983Projeto_TipoProjetoCod ;
      private int A648Projeto_Codigo ;
      private int GIGXA0086 ;
      private int AV2Projeto_Codigo ;
      private int AV8Projeto_TipoProjetoCod ;
      private int AV15Projeto_GerenteCod ;
      private int AV16Projeto_ServicoCod ;
      private int AV24Projeto_AreaTrabalhoCodigo ;
      private int A1826GpoObjCtrl_Codigo ;
      private int AV25Projeto_GpoObjCtrlCodigo ;
      private int AV26Projeto_SistemaCodigo ;
      private int AV27Projeto_ModuloCodigo ;
      private decimal A2160Projeto_CustoPrevisto ;
      private decimal A990Projeto_ConstACocomo ;
      private decimal A989Projeto_FatorMultiplicador ;
      private decimal A985Projeto_FatorEscala ;
      private decimal AV9Projeto_FatorEscala ;
      private decimal AV10Projeto_FatorMultiplicador ;
      private decimal AV11Projeto_ConstACocomo ;
      private decimal AV23Projeto_CustoPrevisto ;
      private String cmdBuffer ;
      private String scmdbuf ;
      private String A658Projeto_Status ;
      private String A652Projeto_TecnicaContagem ;
      private String A650Projeto_Sigla ;
      private String A649Projeto_Nome ;
      private String AV3Projeto_Nome ;
      private String AV4Projeto_Sigla ;
      private String AV5Projeto_TecnicaContagem ;
      private String AV7Projeto_Status ;
      private String Gx_emsg ;
      private DateTime A2157Projeto_DTFim ;
      private DateTime A2156Projeto_DTInicio ;
      private DateTime A1541Projeto_Previsao ;
      private DateTime AV14Projeto_Previsao ;
      private DateTime AV19Projeto_DTInicio ;
      private DateTime AV20Projeto_DTFim ;
      private bool n2161Projeto_AreaTrabalhoCodigo ;
      private bool n2160Projeto_CustoPrevisto ;
      private bool n2159Projeto_EsforcoPrevisto ;
      private bool n2158Projeto_PrazoPrevisto ;
      private bool n2157Projeto_DTFim ;
      private bool n2156Projeto_DTInicio ;
      private bool n2155Projeto_Objetivo ;
      private bool n2154Projeto_Identificador ;
      private bool n1543Projeto_ServicoCod ;
      private bool n1542Projeto_GerenteCod ;
      private bool n1541Projeto_Previsao ;
      private bool n1540Projeto_Introducao ;
      private bool A1232Projeto_Incremental ;
      private bool n1232Projeto_Incremental ;
      private bool n990Projeto_ConstACocomo ;
      private bool n989Projeto_FatorMultiplicador ;
      private bool n985Projeto_FatorEscala ;
      private bool n983Projeto_TipoProjetoCod ;
      private bool nV8Projeto_TipoProjetoCod ;
      private bool nV9Projeto_FatorEscala ;
      private bool nV10Projeto_FatorMultiplicador ;
      private bool nV11Projeto_ConstACocomo ;
      private bool AV12Projeto_Incremental ;
      private bool nV12Projeto_Incremental ;
      private bool nV13Projeto_Introducao ;
      private bool nV14Projeto_Previsao ;
      private bool nV15Projeto_GerenteCod ;
      private bool nV16Projeto_ServicoCod ;
      private bool nV17Projeto_Identificador ;
      private bool nV18Projeto_Objetivo ;
      private bool nV19Projeto_DTInicio ;
      private bool nV20Projeto_DTFim ;
      private bool nV21Projeto_PrazoPrevisto ;
      private bool nV22Projeto_EsforcoPrevisto ;
      private bool nV23Projeto_CustoPrevisto ;
      private bool nV24Projeto_AreaTrabalhoCodigo ;
      private String A2155Projeto_Objetivo ;
      private String A1540Projeto_Introducao ;
      private String A653Projeto_Escopo ;
      private String AV6Projeto_Escopo ;
      private String AV13Projeto_Introducao ;
      private String AV18Projeto_Objetivo ;
      private String A2154Projeto_Identificador ;
      private String AV17Projeto_Identificador ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GxCommand RGZ ;
      private IDataStoreProvider pr_default ;
      private int[] PROJETOCON2_A2161Projeto_AreaTrabalhoCodigo ;
      private bool[] PROJETOCON2_n2161Projeto_AreaTrabalhoCodigo ;
      private decimal[] PROJETOCON2_A2160Projeto_CustoPrevisto ;
      private bool[] PROJETOCON2_n2160Projeto_CustoPrevisto ;
      private short[] PROJETOCON2_A2159Projeto_EsforcoPrevisto ;
      private bool[] PROJETOCON2_n2159Projeto_EsforcoPrevisto ;
      private short[] PROJETOCON2_A2158Projeto_PrazoPrevisto ;
      private bool[] PROJETOCON2_n2158Projeto_PrazoPrevisto ;
      private DateTime[] PROJETOCON2_A2157Projeto_DTFim ;
      private bool[] PROJETOCON2_n2157Projeto_DTFim ;
      private DateTime[] PROJETOCON2_A2156Projeto_DTInicio ;
      private bool[] PROJETOCON2_n2156Projeto_DTInicio ;
      private String[] PROJETOCON2_A2155Projeto_Objetivo ;
      private bool[] PROJETOCON2_n2155Projeto_Objetivo ;
      private String[] PROJETOCON2_A2154Projeto_Identificador ;
      private bool[] PROJETOCON2_n2154Projeto_Identificador ;
      private int[] PROJETOCON2_A1543Projeto_ServicoCod ;
      private bool[] PROJETOCON2_n1543Projeto_ServicoCod ;
      private int[] PROJETOCON2_A1542Projeto_GerenteCod ;
      private bool[] PROJETOCON2_n1542Projeto_GerenteCod ;
      private DateTime[] PROJETOCON2_A1541Projeto_Previsao ;
      private bool[] PROJETOCON2_n1541Projeto_Previsao ;
      private String[] PROJETOCON2_A1540Projeto_Introducao ;
      private bool[] PROJETOCON2_n1540Projeto_Introducao ;
      private bool[] PROJETOCON2_A1232Projeto_Incremental ;
      private bool[] PROJETOCON2_n1232Projeto_Incremental ;
      private decimal[] PROJETOCON2_A990Projeto_ConstACocomo ;
      private bool[] PROJETOCON2_n990Projeto_ConstACocomo ;
      private decimal[] PROJETOCON2_A989Projeto_FatorMultiplicador ;
      private bool[] PROJETOCON2_n989Projeto_FatorMultiplicador ;
      private decimal[] PROJETOCON2_A985Projeto_FatorEscala ;
      private bool[] PROJETOCON2_n985Projeto_FatorEscala ;
      private int[] PROJETOCON2_A983Projeto_TipoProjetoCod ;
      private bool[] PROJETOCON2_n983Projeto_TipoProjetoCod ;
      private String[] PROJETOCON2_A658Projeto_Status ;
      private String[] PROJETOCON2_A653Projeto_Escopo ;
      private String[] PROJETOCON2_A652Projeto_TecnicaContagem ;
      private String[] PROJETOCON2_A650Projeto_Sigla ;
      private String[] PROJETOCON2_A649Projeto_Nome ;
      private int[] PROJETOCON2_A648Projeto_Codigo ;
   }

   public class projetoconversion__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmPROJETOCON2 ;
          prmPROJETOCON2 = new Object[] {
          } ;
          Object[] prmPROJETOCON3 ;
          prmPROJETOCON3 = new Object[] {
          new Object[] {"@AV2Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV3Projeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV4Projeto_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV5Projeto_TecnicaContagem",SqlDbType.Char,1,0} ,
          new Object[] {"@AV6Projeto_Escopo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV7Projeto_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@AV8Projeto_TipoProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Projeto_FatorEscala",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV10Projeto_FatorMultiplicador",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV11Projeto_ConstACocomo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV12Projeto_Incremental",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV13Projeto_Introducao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV14Projeto_Previsao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15Projeto_GerenteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16Projeto_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17Projeto_Identificador",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV18Projeto_Objetivo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV19Projeto_DTInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20Projeto_DTFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21Projeto_PrazoPrevisto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV22Projeto_EsforcoPrevisto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV23Projeto_CustoPrevisto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV24Projeto_AreaTrabalhoCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25Projeto_GpoObjCtrlCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26Projeto_SistemaCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27Projeto_ModuloCodigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("PROJETOCON2", "SELECT [Projeto_AreaTrabalhoCodigo], [Projeto_CustoPrevisto], [Projeto_EsforcoPrevisto], [Projeto_PrazoPrevisto], [Projeto_DTFim], [Projeto_DTInicio], [Projeto_Objetivo], [Projeto_Identificador], [Projeto_ServicoCod], [Projeto_GerenteCod], [Projeto_Previsao], [Projeto_Introducao], [Projeto_Incremental], [Projeto_ConstACocomo], [Projeto_FatorMultiplicador], [Projeto_FatorEscala], [Projeto_TipoProjetoCod], [Projeto_Status], [Projeto_Escopo], [Projeto_TecnicaContagem], [Projeto_Sigla], [Projeto_Nome], [Projeto_Codigo] FROM [Projeto] ORDER BY [Projeto_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmPROJETOCON2,100,0,true,false )
             ,new CursorDef("PROJETOCON3", "INSERT INTO [GXA0086]([Projeto_Codigo], [Projeto_Nome], [Projeto_Sigla], [Projeto_TecnicaContagem], [Projeto_Escopo], [Projeto_Status], [Projeto_TipoProjetoCod], [Projeto_FatorEscala], [Projeto_FatorMultiplicador], [Projeto_ConstACocomo], [Projeto_Incremental], [Projeto_Introducao], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Identificador], [Projeto_Objetivo], [Projeto_DTInicio], [Projeto_DTFim], [Projeto_PrazoPrevisto], [Projeto_EsforcoPrevisto], [Projeto_CustoPrevisto], [Projeto_AreaTrabalhoCodigo], [Projeto_GpoObjCtrlCodigo], [Projeto_SistemaCodigo], [Projeto_ModuloCodigo]) VALUES(@AV2Projeto_Codigo, @AV3Projeto_Nome, @AV4Projeto_Sigla, @AV5Projeto_TecnicaContagem, @AV6Projeto_Escopo, @AV7Projeto_Status, @AV8Projeto_TipoProjetoCod, @AV9Projeto_FatorEscala, @AV10Projeto_FatorMultiplicador, @AV11Projeto_ConstACocomo, @AV12Projeto_Incremental, @AV13Projeto_Introducao, @AV14Projeto_Previsao, @AV15Projeto_GerenteCod, @AV16Projeto_ServicoCod, @AV17Projeto_Identificador, @AV18Projeto_Objetivo, @AV19Projeto_DTInicio, @AV20Projeto_DTFim, @AV21Projeto_PrazoPrevisto, @AV22Projeto_EsforcoPrevisto, @AV23Projeto_CustoPrevisto, @AV24Projeto_AreaTrabalhoCodigo, @AV25Projeto_GpoObjCtrlCodigo, @AV26Projeto_SistemaCodigo, @AV27Projeto_ModuloCodigo)", GxErrorMask.GX_NOMASK,prmPROJETOCON3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((String[]) buf[22])[0] = rslt.getLongVarchar(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((bool[]) buf[24])[0] = rslt.getBool(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((decimal[]) buf[28])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((decimal[]) buf[30])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(16);
                ((int[]) buf[32])[0] = rslt.getInt(17) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(17);
                ((String[]) buf[34])[0] = rslt.getString(18, 1) ;
                ((String[]) buf[35])[0] = rslt.getLongVarchar(19) ;
                ((String[]) buf[36])[0] = rslt.getString(20, 1) ;
                ((String[]) buf[37])[0] = rslt.getString(21, 15) ;
                ((String[]) buf[38])[0] = rslt.getString(22, 50) ;
                ((int[]) buf[39])[0] = rslt.getInt(23) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 11 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(11, (bool)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 13 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(13, (DateTime)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 16 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 17 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 18 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(18, (DateTime)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 19 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(19, (DateTime)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 20 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(20, (short)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 21 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(21, (short)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 22 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(22, (decimal)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[39]);
                }
                stmt.SetParameter(24, (int)parms[40]);
                stmt.SetParameter(25, (int)parms[41]);
                stmt.SetParameter(26, (int)parms[42]);
                return;
       }
    }

 }

}
