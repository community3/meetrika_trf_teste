/*
               File: GetSistemaVersaoWCFilterData
        Description: Get Sistema Versao WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:6.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getsistemaversaowcfilterdata : GXProcedure
   {
      public getsistemaversaowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getsistemaversaowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getsistemaversaowcfilterdata objgetsistemaversaowcfilterdata;
         objgetsistemaversaowcfilterdata = new getsistemaversaowcfilterdata();
         objgetsistemaversaowcfilterdata.AV18DDOName = aP0_DDOName;
         objgetsistemaversaowcfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetsistemaversaowcfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetsistemaversaowcfilterdata.AV22OptionsJson = "" ;
         objgetsistemaversaowcfilterdata.AV25OptionsDescJson = "" ;
         objgetsistemaversaowcfilterdata.AV27OptionIndexesJson = "" ;
         objgetsistemaversaowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetsistemaversaowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetsistemaversaowcfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getsistemaversaowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_SISTEMAVERSAO_ID") == 0 )
         {
            /* Execute user subroutine: 'LOADSISTEMAVERSAO_IDOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_SISTEMAVERSAO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADSISTEMAVERSAO_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("SistemaVersaoWCGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "SistemaVersaoWCGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("SistemaVersaoWCGridState"), "");
         }
         AV37GXV1 = 1;
         while ( AV37GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV37GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_DATA") == 0 )
            {
               AV10TFSistemaVersao_Data = context.localUtil.CToT( AV32GridStateFilterValue.gxTpr_Value, 2);
               AV11TFSistemaVersao_Data_To = context.localUtil.CToT( AV32GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_ID") == 0 )
            {
               AV12TFSistemaVersao_Id = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_ID_SEL") == 0 )
            {
               AV13TFSistemaVersao_Id_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_DESCRICAO") == 0 )
            {
               AV14TFSistemaVersao_Descricao = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_DESCRICAO_SEL") == 0 )
            {
               AV15TFSistemaVersao_Descricao_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&SISTEMAVERSAO_SISTEMACOD") == 0 )
            {
               AV34SistemaVersao_SistemaCod = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            AV37GXV1 = (int)(AV37GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADSISTEMAVERSAO_IDOPTIONS' Routine */
         AV12TFSistemaVersao_Id = AV16SearchTxt;
         AV13TFSistemaVersao_Id_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV10TFSistemaVersao_Data ,
                                              AV11TFSistemaVersao_Data_To ,
                                              AV13TFSistemaVersao_Id_Sel ,
                                              AV12TFSistemaVersao_Id ,
                                              AV15TFSistemaVersao_Descricao_Sel ,
                                              AV14TFSistemaVersao_Descricao ,
                                              A1865SistemaVersao_Data ,
                                              A1860SistemaVersao_Id ,
                                              A1861SistemaVersao_Descricao },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV12TFSistemaVersao_Id = StringUtil.PadR( StringUtil.RTrim( AV12TFSistemaVersao_Id), 20, "%");
         lV14TFSistemaVersao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFSistemaVersao_Descricao), "%", "");
         /* Using cursor P00TU2 */
         pr_default.execute(0, new Object[] {AV10TFSistemaVersao_Data, AV11TFSistemaVersao_Data_To, lV12TFSistemaVersao_Id, AV13TFSistemaVersao_Id_Sel, lV14TFSistemaVersao_Descricao, AV15TFSistemaVersao_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKTU2 = false;
            A1860SistemaVersao_Id = P00TU2_A1860SistemaVersao_Id[0];
            A1861SistemaVersao_Descricao = P00TU2_A1861SistemaVersao_Descricao[0];
            A1865SistemaVersao_Data = P00TU2_A1865SistemaVersao_Data[0];
            A1859SistemaVersao_Codigo = P00TU2_A1859SistemaVersao_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00TU2_A1860SistemaVersao_Id[0], A1860SistemaVersao_Id) == 0 ) )
            {
               BRKTU2 = false;
               A1859SistemaVersao_Codigo = P00TU2_A1859SistemaVersao_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKTU2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1860SistemaVersao_Id)) )
            {
               AV20Option = A1860SistemaVersao_Id;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKTU2 )
            {
               BRKTU2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSISTEMAVERSAO_DESCRICAOOPTIONS' Routine */
         AV14TFSistemaVersao_Descricao = AV16SearchTxt;
         AV15TFSistemaVersao_Descricao_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV10TFSistemaVersao_Data ,
                                              AV11TFSistemaVersao_Data_To ,
                                              AV13TFSistemaVersao_Id_Sel ,
                                              AV12TFSistemaVersao_Id ,
                                              AV15TFSistemaVersao_Descricao_Sel ,
                                              AV14TFSistemaVersao_Descricao ,
                                              A1865SistemaVersao_Data ,
                                              A1860SistemaVersao_Id ,
                                              A1861SistemaVersao_Descricao },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV12TFSistemaVersao_Id = StringUtil.PadR( StringUtil.RTrim( AV12TFSistemaVersao_Id), 20, "%");
         lV14TFSistemaVersao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFSistemaVersao_Descricao), "%", "");
         /* Using cursor P00TU3 */
         pr_default.execute(1, new Object[] {AV10TFSistemaVersao_Data, AV11TFSistemaVersao_Data_To, lV12TFSistemaVersao_Id, AV13TFSistemaVersao_Id_Sel, lV14TFSistemaVersao_Descricao, AV15TFSistemaVersao_Descricao_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKTU4 = false;
            A1861SistemaVersao_Descricao = P00TU3_A1861SistemaVersao_Descricao[0];
            A1860SistemaVersao_Id = P00TU3_A1860SistemaVersao_Id[0];
            A1865SistemaVersao_Data = P00TU3_A1865SistemaVersao_Data[0];
            A1859SistemaVersao_Codigo = P00TU3_A1859SistemaVersao_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00TU3_A1861SistemaVersao_Descricao[0], A1861SistemaVersao_Descricao) == 0 ) )
            {
               BRKTU4 = false;
               A1859SistemaVersao_Codigo = P00TU3_A1859SistemaVersao_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKTU4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1861SistemaVersao_Descricao)) )
            {
               AV20Option = A1861SistemaVersao_Descricao;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKTU4 )
            {
               BRKTU4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFSistemaVersao_Data = (DateTime)(DateTime.MinValue);
         AV11TFSistemaVersao_Data_To = (DateTime)(DateTime.MinValue);
         AV12TFSistemaVersao_Id = "";
         AV13TFSistemaVersao_Id_Sel = "";
         AV14TFSistemaVersao_Descricao = "";
         AV15TFSistemaVersao_Descricao_Sel = "";
         scmdbuf = "";
         lV12TFSistemaVersao_Id = "";
         lV14TFSistemaVersao_Descricao = "";
         A1865SistemaVersao_Data = (DateTime)(DateTime.MinValue);
         A1860SistemaVersao_Id = "";
         A1861SistemaVersao_Descricao = "";
         P00TU2_A1860SistemaVersao_Id = new String[] {""} ;
         P00TU2_A1861SistemaVersao_Descricao = new String[] {""} ;
         P00TU2_A1865SistemaVersao_Data = new DateTime[] {DateTime.MinValue} ;
         P00TU2_A1859SistemaVersao_Codigo = new int[1] ;
         AV20Option = "";
         P00TU3_A1861SistemaVersao_Descricao = new String[] {""} ;
         P00TU3_A1860SistemaVersao_Id = new String[] {""} ;
         P00TU3_A1865SistemaVersao_Data = new DateTime[] {DateTime.MinValue} ;
         P00TU3_A1859SistemaVersao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getsistemaversaowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00TU2_A1860SistemaVersao_Id, P00TU2_A1861SistemaVersao_Descricao, P00TU2_A1865SistemaVersao_Data, P00TU2_A1859SistemaVersao_Codigo
               }
               , new Object[] {
               P00TU3_A1861SistemaVersao_Descricao, P00TU3_A1860SistemaVersao_Id, P00TU3_A1865SistemaVersao_Data, P00TU3_A1859SistemaVersao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV37GXV1 ;
      private int AV34SistemaVersao_SistemaCod ;
      private int A1859SistemaVersao_Codigo ;
      private long AV28count ;
      private String AV12TFSistemaVersao_Id ;
      private String AV13TFSistemaVersao_Id_Sel ;
      private String scmdbuf ;
      private String lV12TFSistemaVersao_Id ;
      private String A1860SistemaVersao_Id ;
      private DateTime AV10TFSistemaVersao_Data ;
      private DateTime AV11TFSistemaVersao_Data_To ;
      private DateTime A1865SistemaVersao_Data ;
      private bool returnInSub ;
      private bool BRKTU2 ;
      private bool BRKTU4 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String A1861SistemaVersao_Descricao ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV14TFSistemaVersao_Descricao ;
      private String AV15TFSistemaVersao_Descricao_Sel ;
      private String lV14TFSistemaVersao_Descricao ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00TU2_A1860SistemaVersao_Id ;
      private String[] P00TU2_A1861SistemaVersao_Descricao ;
      private DateTime[] P00TU2_A1865SistemaVersao_Data ;
      private int[] P00TU2_A1859SistemaVersao_Codigo ;
      private String[] P00TU3_A1861SistemaVersao_Descricao ;
      private String[] P00TU3_A1860SistemaVersao_Id ;
      private DateTime[] P00TU3_A1865SistemaVersao_Data ;
      private int[] P00TU3_A1859SistemaVersao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
   }

   public class getsistemaversaowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00TU2( IGxContext context ,
                                             DateTime AV10TFSistemaVersao_Data ,
                                             DateTime AV11TFSistemaVersao_Data_To ,
                                             String AV13TFSistemaVersao_Id_Sel ,
                                             String AV12TFSistemaVersao_Id ,
                                             String AV15TFSistemaVersao_Descricao_Sel ,
                                             String AV14TFSistemaVersao_Descricao ,
                                             DateTime A1865SistemaVersao_Data ,
                                             String A1860SistemaVersao_Id ,
                                             String A1861SistemaVersao_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [SistemaVersao_Id], [SistemaVersao_Descricao], [SistemaVersao_Data], [SistemaVersao_Codigo] FROM [SistemaVersao] WITH (NOLOCK)";
         if ( ! (DateTime.MinValue==AV10TFSistemaVersao_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] >= @AV10TFSistemaVersao_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] >= @AV10TFSistemaVersao_Data)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFSistemaVersao_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] <= @AV11TFSistemaVersao_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] <= @AV11TFSistemaVersao_Data_To)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistemaVersao_Id_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFSistemaVersao_Id)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV12TFSistemaVersao_Id)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV12TFSistemaVersao_Id)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistemaVersao_Id_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] = @AV13TFSistemaVersao_Id_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] = @AV13TFSistemaVersao_Id_Sel)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFSistemaVersao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFSistemaVersao_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] like @lV14TFSistemaVersao_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] like @lV14TFSistemaVersao_Descricao)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFSistemaVersao_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] = @AV15TFSistemaVersao_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] = @AV15TFSistemaVersao_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [SistemaVersao_Id]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00TU3( IGxContext context ,
                                             DateTime AV10TFSistemaVersao_Data ,
                                             DateTime AV11TFSistemaVersao_Data_To ,
                                             String AV13TFSistemaVersao_Id_Sel ,
                                             String AV12TFSistemaVersao_Id ,
                                             String AV15TFSistemaVersao_Descricao_Sel ,
                                             String AV14TFSistemaVersao_Descricao ,
                                             DateTime A1865SistemaVersao_Data ,
                                             String A1860SistemaVersao_Id ,
                                             String A1861SistemaVersao_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [6] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [SistemaVersao_Descricao], [SistemaVersao_Id], [SistemaVersao_Data], [SistemaVersao_Codigo] FROM [SistemaVersao] WITH (NOLOCK)";
         if ( ! (DateTime.MinValue==AV10TFSistemaVersao_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] >= @AV10TFSistemaVersao_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] >= @AV10TFSistemaVersao_Data)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFSistemaVersao_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] <= @AV11TFSistemaVersao_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] <= @AV11TFSistemaVersao_Data_To)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistemaVersao_Id_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFSistemaVersao_Id)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV12TFSistemaVersao_Id)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV12TFSistemaVersao_Id)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistemaVersao_Id_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] = @AV13TFSistemaVersao_Id_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] = @AV13TFSistemaVersao_Id_Sel)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFSistemaVersao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFSistemaVersao_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] like @lV14TFSistemaVersao_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] like @lV14TFSistemaVersao_Descricao)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFSistemaVersao_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] = @AV15TFSistemaVersao_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] = @AV15TFSistemaVersao_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [SistemaVersao_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00TU2(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] );
               case 1 :
                     return conditional_P00TU3(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00TU2 ;
          prmP00TU2 = new Object[] {
          new Object[] {"@AV10TFSistemaVersao_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV11TFSistemaVersao_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV12TFSistemaVersao_Id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV13TFSistemaVersao_Id_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV14TFSistemaVersao_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV15TFSistemaVersao_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00TU3 ;
          prmP00TU3 = new Object[] {
          new Object[] {"@AV10TFSistemaVersao_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV11TFSistemaVersao_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV12TFSistemaVersao_Id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV13TFSistemaVersao_Id_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV14TFSistemaVersao_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV15TFSistemaVersao_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00TU2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TU2,100,0,true,false )
             ,new CursorDef("P00TU3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TU3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getsistemaversaowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getsistemaversaowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getsistemaversaowcfilterdata") )
          {
             return  ;
          }
          getsistemaversaowcfilterdata worker = new getsistemaversaowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
