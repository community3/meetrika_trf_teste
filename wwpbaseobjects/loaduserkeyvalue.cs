/*
               File: WWPBaseObjects.LoadUserKeyValue
        Description: Load User Key Value
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:43.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   public class loaduserkeyvalue : GXProcedure
   {
      public loaduserkeyvalue( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loaduserkeyvalue( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_UserCustomKey ,
                           out String aP1_UserCustomValue )
      {
         this.AV9UserCustomKey = aP0_UserCustomKey;
         this.AV10UserCustomValue = "" ;
         initialize();
         executePrivate();
         aP1_UserCustomValue=this.AV10UserCustomValue;
      }

      public String executeUdp( String aP0_UserCustomKey )
      {
         this.AV9UserCustomKey = aP0_UserCustomKey;
         this.AV10UserCustomValue = "" ;
         initialize();
         executePrivate();
         aP1_UserCustomValue=this.AV10UserCustomValue;
         return AV10UserCustomValue ;
      }

      public void executeSubmit( String aP0_UserCustomKey ,
                                 out String aP1_UserCustomValue )
      {
         loaduserkeyvalue objloaduserkeyvalue;
         objloaduserkeyvalue = new loaduserkeyvalue();
         objloaduserkeyvalue.AV9UserCustomKey = aP0_UserCustomKey;
         objloaduserkeyvalue.AV10UserCustomValue = "" ;
         objloaduserkeyvalue.context.SetSubmitInitialConfig(context);
         objloaduserkeyvalue.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloaduserkeyvalue);
         aP1_UserCustomValue=this.AV10UserCustomValue;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loaduserkeyvalue)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV10UserCustomValue = AV8Session.Get(AV9UserCustomKey);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8Session = context.GetSession();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV9UserCustomKey ;
      private String AV10UserCustomValue ;
      private IGxSession AV8Session ;
      private String aP1_UserCustomValue ;
   }

}
