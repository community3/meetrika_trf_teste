/*
               File: wwpbaseobjects.type_SdtWWPGridState_DynamicFilter
        Description: WWPBaseObjects\WWPGridState
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/17/2020 15:47:40.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "WWPGridState.DynamicFilter" )]
   [XmlType(TypeName =  "WWPGridState.DynamicFilter" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtWWPGridState_DynamicFilter : GxUserType
   {
      public SdtWWPGridState_DynamicFilter( )
      {
         /* Constructor for serialization */
         gxTv_SdtWWPGridState_DynamicFilter_Selected = "";
         gxTv_SdtWWPGridState_DynamicFilter_Value = "";
         gxTv_SdtWWPGridState_DynamicFilter_Valueto = "";
      }

      public SdtWWPGridState_DynamicFilter( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtWWPGridState_DynamicFilter deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtWWPGridState_DynamicFilter)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtWWPGridState_DynamicFilter obj ;
         obj = this;
         obj.gxTpr_Selected = deserialized.gxTpr_Selected;
         obj.gxTpr_Value = deserialized.gxTpr_Value;
         obj.gxTpr_Operator = deserialized.gxTpr_Operator;
         obj.gxTpr_Valueto = deserialized.gxTpr_Valueto;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Selected") )
               {
                  gxTv_SdtWWPGridState_DynamicFilter_Selected = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Value") )
               {
                  gxTv_SdtWWPGridState_DynamicFilter_Value = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Operator") )
               {
                  gxTv_SdtWWPGridState_DynamicFilter_Operator = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ValueTo") )
               {
                  gxTv_SdtWWPGridState_DynamicFilter_Valueto = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPBaseObjects\\WWPGridState.DynamicFilter";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Selected", StringUtil.RTrim( gxTv_SdtWWPGridState_DynamicFilter_Selected));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Value", StringUtil.RTrim( gxTv_SdtWWPGridState_DynamicFilter_Value));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Operator", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPGridState_DynamicFilter_Operator), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ValueTo", StringUtil.RTrim( gxTv_SdtWWPGridState_DynamicFilter_Valueto));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Selected", gxTv_SdtWWPGridState_DynamicFilter_Selected, false);
         AddObjectProperty("Value", gxTv_SdtWWPGridState_DynamicFilter_Value, false);
         AddObjectProperty("Operator", gxTv_SdtWWPGridState_DynamicFilter_Operator, false);
         AddObjectProperty("ValueTo", gxTv_SdtWWPGridState_DynamicFilter_Valueto, false);
         return  ;
      }

      [  SoapElement( ElementName = "Selected" )]
      [  XmlElement( ElementName = "Selected"   )]
      public String gxTpr_Selected
      {
         get {
            return gxTv_SdtWWPGridState_DynamicFilter_Selected ;
         }

         set {
            gxTv_SdtWWPGridState_DynamicFilter_Selected = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Value" )]
      [  XmlElement( ElementName = "Value"   )]
      public String gxTpr_Value
      {
         get {
            return gxTv_SdtWWPGridState_DynamicFilter_Value ;
         }

         set {
            gxTv_SdtWWPGridState_DynamicFilter_Value = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Operator" )]
      [  XmlElement( ElementName = "Operator"   )]
      public short gxTpr_Operator
      {
         get {
            return gxTv_SdtWWPGridState_DynamicFilter_Operator ;
         }

         set {
            gxTv_SdtWWPGridState_DynamicFilter_Operator = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ValueTo" )]
      [  XmlElement( ElementName = "ValueTo"   )]
      public String gxTpr_Valueto
      {
         get {
            return gxTv_SdtWWPGridState_DynamicFilter_Valueto ;
         }

         set {
            gxTv_SdtWWPGridState_DynamicFilter_Valueto = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtWWPGridState_DynamicFilter_Selected = "";
         gxTv_SdtWWPGridState_DynamicFilter_Value = "";
         gxTv_SdtWWPGridState_DynamicFilter_Valueto = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtWWPGridState_DynamicFilter_Operator ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String gxTv_SdtWWPGridState_DynamicFilter_Selected ;
      protected String gxTv_SdtWWPGridState_DynamicFilter_Value ;
      protected String gxTv_SdtWWPGridState_DynamicFilter_Valueto ;
   }

   [DataContract(Name = @"WWPBaseObjects\WWPGridState.DynamicFilter", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtWWPGridState_DynamicFilter_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtWWPGridState_DynamicFilter>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtWWPGridState_DynamicFilter_RESTInterface( ) : base()
      {
      }

      public SdtWWPGridState_DynamicFilter_RESTInterface( wwpbaseobjects.SdtWWPGridState_DynamicFilter psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Selected" , Order = 0 )]
      public String gxTpr_Selected
      {
         get {
            return sdt.gxTpr_Selected ;
         }

         set {
            sdt.gxTpr_Selected = (String)(value);
         }

      }

      [DataMember( Name = "Value" , Order = 1 )]
      public String gxTpr_Value
      {
         get {
            return sdt.gxTpr_Value ;
         }

         set {
            sdt.gxTpr_Value = (String)(value);
         }

      }

      [DataMember( Name = "Operator" , Order = 2 )]
      public Nullable<short> gxTpr_Operator
      {
         get {
            return sdt.gxTpr_Operator ;
         }

         set {
            sdt.gxTpr_Operator = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ValueTo" , Order = 3 )]
      public String gxTpr_Valueto
      {
         get {
            return sdt.gxTpr_Valueto ;
         }

         set {
            sdt.gxTpr_Valueto = (String)(value);
         }

      }

      public wwpbaseobjects.SdtWWPGridState_DynamicFilter sdt
      {
         get {
            return (wwpbaseobjects.SdtWWPGridState_DynamicFilter)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtWWPGridState_DynamicFilter() ;
         }
      }

   }

}
