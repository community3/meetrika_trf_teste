/*
               File: wwpbaseobjects.type_SdtWWPTabOptions_TabOptionsItem
        Description: WWPBaseObjects\WWPTabOptions
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/13/2020 23:30:34.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "WWPTabOptions.TabOptionsItem" )]
   [XmlType(TypeName =  "WWPTabOptions.TabOptionsItem" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtWWPTabOptions_TabOptionsItem : GxUserType
   {
      public SdtWWPTabOptions_TabOptionsItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtWWPTabOptions_TabOptionsItem_Code = "";
         gxTv_SdtWWPTabOptions_TabOptionsItem_Description = "";
         gxTv_SdtWWPTabOptions_TabOptionsItem_Link = "";
         gxTv_SdtWWPTabOptions_TabOptionsItem_Webcomponent = "";
      }

      public SdtWWPTabOptions_TabOptionsItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem obj ;
         obj = this;
         obj.gxTpr_Code = deserialized.gxTpr_Code;
         obj.gxTpr_Description = deserialized.gxTpr_Description;
         obj.gxTpr_Link = deserialized.gxTpr_Link;
         obj.gxTpr_Webcomponent = deserialized.gxTpr_Webcomponent;
         obj.gxTpr_Includeinpanel = deserialized.gxTpr_Includeinpanel;
         obj.gxTpr_Collapsedbydefault = deserialized.gxTpr_Collapsedbydefault;
         obj.gxTpr_Collapsable = deserialized.gxTpr_Collapsable;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Code") )
               {
                  gxTv_SdtWWPTabOptions_TabOptionsItem_Code = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Description") )
               {
                  gxTv_SdtWWPTabOptions_TabOptionsItem_Description = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Link") )
               {
                  gxTv_SdtWWPTabOptions_TabOptionsItem_Link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "WebComponent") )
               {
                  gxTv_SdtWWPTabOptions_TabOptionsItem_Webcomponent = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "IncludeInPanel") )
               {
                  gxTv_SdtWWPTabOptions_TabOptionsItem_Includeinpanel = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "CollapsedByDefault") )
               {
                  gxTv_SdtWWPTabOptions_TabOptionsItem_Collapsedbydefault = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Collapsable") )
               {
                  gxTv_SdtWWPTabOptions_TabOptionsItem_Collapsable = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPBaseObjects\\WWPTabOptions.TabOptionsItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Code", StringUtil.RTrim( gxTv_SdtWWPTabOptions_TabOptionsItem_Code));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Description", StringUtil.RTrim( gxTv_SdtWWPTabOptions_TabOptionsItem_Description));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Link", StringUtil.RTrim( gxTv_SdtWWPTabOptions_TabOptionsItem_Link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("WebComponent", StringUtil.RTrim( gxTv_SdtWWPTabOptions_TabOptionsItem_Webcomponent));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("IncludeInPanel", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPTabOptions_TabOptionsItem_Includeinpanel), 1, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("CollapsedByDefault", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPTabOptions_TabOptionsItem_Collapsedbydefault)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Collapsable", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPTabOptions_TabOptionsItem_Collapsable)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Code", gxTv_SdtWWPTabOptions_TabOptionsItem_Code, false);
         AddObjectProperty("Description", gxTv_SdtWWPTabOptions_TabOptionsItem_Description, false);
         AddObjectProperty("Link", gxTv_SdtWWPTabOptions_TabOptionsItem_Link, false);
         AddObjectProperty("WebComponent", gxTv_SdtWWPTabOptions_TabOptionsItem_Webcomponent, false);
         AddObjectProperty("IncludeInPanel", gxTv_SdtWWPTabOptions_TabOptionsItem_Includeinpanel, false);
         AddObjectProperty("CollapsedByDefault", gxTv_SdtWWPTabOptions_TabOptionsItem_Collapsedbydefault, false);
         AddObjectProperty("Collapsable", gxTv_SdtWWPTabOptions_TabOptionsItem_Collapsable, false);
         return  ;
      }

      [  SoapElement( ElementName = "Code" )]
      [  XmlElement( ElementName = "Code"   )]
      public String gxTpr_Code
      {
         get {
            return gxTv_SdtWWPTabOptions_TabOptionsItem_Code ;
         }

         set {
            gxTv_SdtWWPTabOptions_TabOptionsItem_Code = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Description" )]
      [  XmlElement( ElementName = "Description"   )]
      public String gxTpr_Description
      {
         get {
            return gxTv_SdtWWPTabOptions_TabOptionsItem_Description ;
         }

         set {
            gxTv_SdtWWPTabOptions_TabOptionsItem_Description = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Link" )]
      [  XmlElement( ElementName = "Link"   )]
      public String gxTpr_Link
      {
         get {
            return gxTv_SdtWWPTabOptions_TabOptionsItem_Link ;
         }

         set {
            gxTv_SdtWWPTabOptions_TabOptionsItem_Link = (String)(value);
         }

      }

      [  SoapElement( ElementName = "WebComponent" )]
      [  XmlElement( ElementName = "WebComponent"   )]
      public String gxTpr_Webcomponent
      {
         get {
            return gxTv_SdtWWPTabOptions_TabOptionsItem_Webcomponent ;
         }

         set {
            gxTv_SdtWWPTabOptions_TabOptionsItem_Webcomponent = (String)(value);
         }

      }

      [  SoapElement( ElementName = "IncludeInPanel" )]
      [  XmlElement( ElementName = "IncludeInPanel"   )]
      public short gxTpr_Includeinpanel
      {
         get {
            return gxTv_SdtWWPTabOptions_TabOptionsItem_Includeinpanel ;
         }

         set {
            gxTv_SdtWWPTabOptions_TabOptionsItem_Includeinpanel = (short)(value);
         }

      }

      [  SoapElement( ElementName = "CollapsedByDefault" )]
      [  XmlElement( ElementName = "CollapsedByDefault"   )]
      public bool gxTpr_Collapsedbydefault
      {
         get {
            return gxTv_SdtWWPTabOptions_TabOptionsItem_Collapsedbydefault ;
         }

         set {
            gxTv_SdtWWPTabOptions_TabOptionsItem_Collapsedbydefault = value;
         }

      }

      [  SoapElement( ElementName = "Collapsable" )]
      [  XmlElement( ElementName = "Collapsable"   )]
      public bool gxTpr_Collapsable
      {
         get {
            return gxTv_SdtWWPTabOptions_TabOptionsItem_Collapsable ;
         }

         set {
            gxTv_SdtWWPTabOptions_TabOptionsItem_Collapsable = value;
         }

      }

      public void initialize( )
      {
         gxTv_SdtWWPTabOptions_TabOptionsItem_Code = "";
         gxTv_SdtWWPTabOptions_TabOptionsItem_Description = "";
         gxTv_SdtWWPTabOptions_TabOptionsItem_Link = "";
         gxTv_SdtWWPTabOptions_TabOptionsItem_Webcomponent = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtWWPTabOptions_TabOptionsItem_Includeinpanel ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtWWPTabOptions_TabOptionsItem_Code ;
      protected String sTagName ;
      protected bool gxTv_SdtWWPTabOptions_TabOptionsItem_Collapsedbydefault ;
      protected bool gxTv_SdtWWPTabOptions_TabOptionsItem_Collapsable ;
      protected String gxTv_SdtWWPTabOptions_TabOptionsItem_Description ;
      protected String gxTv_SdtWWPTabOptions_TabOptionsItem_Link ;
      protected String gxTv_SdtWWPTabOptions_TabOptionsItem_Webcomponent ;
   }

   [DataContract(Name = @"WWPBaseObjects\WWPTabOptions.TabOptionsItem", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtWWPTabOptions_TabOptionsItem_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtWWPTabOptions_TabOptionsItem_RESTInterface( ) : base()
      {
      }

      public SdtWWPTabOptions_TabOptionsItem_RESTInterface( wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Code" , Order = 0 )]
      public String gxTpr_Code
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Code) ;
         }

         set {
            sdt.gxTpr_Code = (String)(value);
         }

      }

      [DataMember( Name = "Description" , Order = 1 )]
      public String gxTpr_Description
      {
         get {
            return sdt.gxTpr_Description ;
         }

         set {
            sdt.gxTpr_Description = (String)(value);
         }

      }

      [DataMember( Name = "Link" , Order = 2 )]
      public String gxTpr_Link
      {
         get {
            return sdt.gxTpr_Link ;
         }

         set {
            sdt.gxTpr_Link = (String)(value);
         }

      }

      [DataMember( Name = "WebComponent" , Order = 3 )]
      public String gxTpr_Webcomponent
      {
         get {
            return sdt.gxTpr_Webcomponent ;
         }

         set {
            sdt.gxTpr_Webcomponent = (String)(value);
         }

      }

      [DataMember( Name = "IncludeInPanel" , Order = 4 )]
      public Nullable<short> gxTpr_Includeinpanel
      {
         get {
            return sdt.gxTpr_Includeinpanel ;
         }

         set {
            sdt.gxTpr_Includeinpanel = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "CollapsedByDefault" , Order = 5 )]
      public bool gxTpr_Collapsedbydefault
      {
         get {
            return sdt.gxTpr_Collapsedbydefault ;
         }

         set {
            sdt.gxTpr_Collapsedbydefault = value;
         }

      }

      [DataMember( Name = "Collapsable" , Order = 6 )]
      public bool gxTpr_Collapsable
      {
         get {
            return sdt.gxTpr_Collapsable ;
         }

         set {
            sdt.gxTpr_Collapsable = value;
         }

      }

      public wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem sdt
      {
         get {
            return (wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem() ;
         }
      }

   }

}
