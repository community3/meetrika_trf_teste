/**@preserve  GeneXus C# 10_3_14-114418 on 5/18/2020 13:35:40.34
*/
gx.evt.autoSkip = false;
gx.define('wwpbaseobjects.wwpbootstrapresponsivemasterpage', false, function () {
   this.ServerClass =  "wwpbaseobjects.wwpbootstrapresponsivemasterpage" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.IsMasterPage=true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
   };
   this.e11ni1_client=function()
   {
      this.clearMessages();
      this.SIDEBARMENU1_MPAGEContainer.CollapseExpand() ;
      this.refreshOutputs([]);
   };
   this.e14ni1_client=function()
   {
      this.clearMessages();
      if ( this.DDO_ADMINAG_MPAGEContainer.ActiveEventKey == "ActionLogOut" )
      {
         this.s112_client();
      }
      this.refreshOutputs([]);
   };
   this.s112_client=function()
   {
   };
   this.e15ni2_client=function()
   {
      this.executeServerEvent("ENTER_MPAGE", true, null, false, false);
   };
   this.e16ni2_client=function()
   {
      this.executeServerEvent("CANCEL_MPAGE", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,3,4,5,6,7,8,9,10,11,12,15,17,18,19,20,21,22,23,24,25,26,27,29,30,31,32,33,35,36,37,38];
   this.GXLastCtrlId =38;
   this.SIDEBARMENU1_MPAGEContainer = gx.uc.getNew(this, 28, 0, "BootstrapSidebarMenu", "SIDEBARMENU1_MPAGEContainer", "Sidebarmenu1");
   var SIDEBARMENU1_MPAGEContainer = this.SIDEBARMENU1_MPAGEContainer;
   SIDEBARMENU1_MPAGEContainer.setProp("SelectedItem", "Selecteditem", "", "char");
   SIDEBARMENU1_MPAGEContainer.setProp("SearchServiceUrl", "Searchserviceurl", "", "str");
   SIDEBARMENU1_MPAGEContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   SIDEBARMENU1_MPAGEContainer.setProp("SearchMinChars", "Searchminchars", 3, "num");
   SIDEBARMENU1_MPAGEContainer.setProp("SearchHelperDescription", "Searchhelperdescription", "", "str");
   SIDEBARMENU1_MPAGEContainer.setProp("SidebarMainClass", "Sidebarmainclass", "page-sidebar sidebar-fixed", "str");
   SIDEBARMENU1_MPAGEContainer.setProp("HeaderClass", "Headerclass", "sidebar-header-wrapper", "str");
   SIDEBARMENU1_MPAGEContainer.setProp("SearchInputClass", "Searchinputclass", "searchinput", "str");
   SIDEBARMENU1_MPAGEContainer.setProp("SearchIconClass", "Searchiconclass", "searchicon fa fa-search", "str");
   SIDEBARMENU1_MPAGEContainer.setProp("SearchHelperClass", "Searchhelperclass", "searchhelper", "str");
   SIDEBARMENU1_MPAGEContainer.setProp("SidebarMenuClass", "Sidebarmenuclass", "nav sidebar-menu", "str");
   SIDEBARMENU1_MPAGEContainer.addV2CFunction('AV9SidebarMenuOptionsData', "vSIDEBARMENUOPTIONSDATA_MPAGE", 'SetSidebarMenuOptionsDataOptionsData');
   SIDEBARMENU1_MPAGEContainer.addC2VFunction(function(UC) { UC.ParentObject.AV9SidebarMenuOptionsData=UC.GetSidebarMenuOptionsDataOptionsData();gx.fn.setControlValue("vSIDEBARMENUOPTIONSDATA_MPAGE",UC.ParentObject.AV9SidebarMenuOptionsData); });
   SIDEBARMENU1_MPAGEContainer.setProp("SidebarMenuUserData", "Sidebarmenuuserdata", '', "str");
   SIDEBARMENU1_MPAGEContainer.setProp("AllMenuItemsVisibleAtLoad", "Allmenuitemsvisibleatload", false, "bool");
   SIDEBARMENU1_MPAGEContainer.setProp("Visible", "Visible", true, "bool");
   SIDEBARMENU1_MPAGEContainer.setProp("Enabled", "Enabled", true, "boolean");
   SIDEBARMENU1_MPAGEContainer.setProp("Class", "Class", "", "char");
   SIDEBARMENU1_MPAGEContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(SIDEBARMENU1_MPAGEContainer);
   this.DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer = gx.uc.getNew(this, 41, 0, "BootstrapTooltip", "DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer", "Dvelop_gxbootstrap_tooltip1");
   var DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer = this.DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer;
   DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer.setProp("ClassSelector", "Classselector", "BootstrapTooltip", "str");
   DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer.setProp("DefaultPosition", "Defaultposition", "bottom", "str");
   DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer.setProp("LabelsShowDelay", "Labelsshowdelay", 300, "num");
   DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer.setProp("ButtonsShowDelay", "Buttonsshowdelay", 300, "num");
   DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer.setProp("InputsShowDelay", "Inputsshowdelay", 300, "num");
   DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer.setProp("ImagesShowDelay", "Imagesshowdelay", 0, "num");
   DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer.setProp("HideDelay", "Hidedelay", 0, "num");
   DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer.setProp("Visible", "Visible", true, "bool");
   DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer.setProp("Class", "Class", "", "char");
   DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVELOP_GXBOOTSTRAP_TOOLTIP1_MPAGEContainer);
   this.DDO_ADMINAG_MPAGEContainer = gx.uc.getNew(this, 16, 0, "BootstrapDropDownOptions", "DDO_ADMINAG_MPAGEContainer", "Ddo_adminag");
   var DDO_ADMINAG_MPAGEContainer = this.DDO_ADMINAG_MPAGEContainer;
   DDO_ADMINAG_MPAGEContainer.setDynProp("Icon", "Icon", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("Caption", "Caption", "", "str");
   DDO_ADMINAG_MPAGEContainer.setProp("Tooltip", "Tooltip", "", "str");
   DDO_ADMINAG_MPAGEContainer.setProp("Cls", "Cls", "ActionGroupHeader", "str");
   DDO_ADMINAG_MPAGEContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_ADMINAG_MPAGEContainer.setDynProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_ADMINAG_MPAGEContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "Regular", "str");
   DDO_ADMINAG_MPAGEContainer.setProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_ADMINAG_MPAGEContainer.setProp("IncludeSortASC", "Includesortasc", false, "boolean");
   DDO_ADMINAG_MPAGEContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "boolean");
   DDO_ADMINAG_MPAGEContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("IncludeFilter", "Includefilter", false, "boolean");
   DDO_ADMINAG_MPAGEContainer.setProp("FilterType", "Filtertype", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("FilterIsRange", "Filterisrange", false, "boolean");
   DDO_ADMINAG_MPAGEContainer.setProp("IncludeDataList", "Includedatalist", false, "boolean");
   DDO_ADMINAG_MPAGEContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_ADMINAG_MPAGEContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_ADMINAG_MPAGEContainer.setProp("FixedFilters", "Fixedfilters", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("CleanFilter", "Cleanfilter", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("SearchButtonText", "Searchbuttontext", "", "char");
   DDO_ADMINAG_MPAGEContainer.setProp("UpdateButtonText", "Updatebuttontext", "", "char");
   DDO_ADMINAG_MPAGEContainer.DropDownOptionsTitleSettingsIcons = '';
   DDO_ADMINAG_MPAGEContainer.addV2CFunction('AV5AdminAGData', "vADMINAGDATA_MPAGE", 'SetDropDownOptionsData');
   DDO_ADMINAG_MPAGEContainer.addC2VFunction(function(UC) { UC.ParentObject.AV5AdminAGData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vADMINAGDATA_MPAGE",UC.ParentObject.AV5AdminAGData); });
   DDO_ADMINAG_MPAGEContainer.setProp("Visible", "Visible", true, "bool");
   DDO_ADMINAG_MPAGEContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_ADMINAG_MPAGEContainer.setProp("Class", "Class", "", "char");
   DDO_ADMINAG_MPAGEContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_ADMINAG_MPAGEContainer.addEventHandler("OnOptionClicked", this.e14ni1_client);
   this.setUserControl(DDO_ADMINAG_MPAGEContainer);
   GXValidFnc[2]={fld:"LAYOUT_TABLEMAIN",grid:0};
   GXValidFnc[3]={fld:"",grid:0};
   GXValidFnc[4]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"",grid:0};
   GXValidFnc[6]={fld:"",grid:0};
   GXValidFnc[7]={fld:"TABLEHEADER",grid:0};
   GXValidFnc[8]={fld:"",grid:0};
   GXValidFnc[9]={fld:"",grid:0};
   GXValidFnc[10]={fld:"HEADER",grid:0};
   GXValidFnc[11]={fld:"",grid:0};
   GXValidFnc[12]={fld:"TABLEUSERROLE",grid:0};
   GXValidFnc[15]={fld:"HTML_USERTABLE_ADMINAG",grid:0};
   GXValidFnc[17]={fld:"",grid:0};
   GXValidFnc[18]={fld:"",grid:0};
   GXValidFnc[19]={fld:"SHOWMENU",grid:0};
   GXValidFnc[20]={fld:"",grid:0};
   GXValidFnc[21]={fld:"",grid:0};
   GXValidFnc[22]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[23]={fld:"",grid:0};
   GXValidFnc[24]={fld:"",grid:0};
   GXValidFnc[25]={fld:"MENU",grid:0};
   GXValidFnc[26]={fld:"",grid:0};
   GXValidFnc[27]={fld:"",grid:0};
   GXValidFnc[29]={fld:"",grid:0};
   GXValidFnc[30]={fld:"",grid:0};
   GXValidFnc[31]={fld:"TABLECONTENTPLACEHOLDER",grid:0};
   GXValidFnc[32]={fld:"",grid:0};
   GXValidFnc[33]={fld:"",grid:0};
   GXValidFnc[35]={fld:"",grid:0};
   GXValidFnc[36]={fld:"",grid:0};
   GXValidFnc[37]={fld:"HTML_USERTABLE_UTTOOLTIP",grid:0};
   GXValidFnc[38]={fld:"UTTOOLTIP",grid:0};
   this.AV5AdminAGData = [ ] ;
   this.AV9SidebarMenuOptionsData = [ ] ;
   this.Events = {"e15ni2_client": ["ENTER_MPAGE", true] ,"e16ni2_client": ["CANCEL_MPAGE", true] ,"e11ni1_client": ["DOSHOWMENU_MPAGE", false] ,"e14ni1_client": ["DDO_ADMINAG_MPAGE.ONOPTIONCLICKED_MPAGE", false]};
   this.EvtParms["REFRESH_MPAGE"] = [[],[]];
   this.EvtParms["DOSHOWMENU_MPAGE"] = [[],[]];
   this.EvtParms["DDO_ADMINAG_MPAGE.ONOPTIONCLICKED_MPAGE"] = [[{av:'this.DDO_ADMINAG_MPAGEContainer.ActiveEventKey',ctrl:'DDO_ADMINAG_MPAGE',prop:'ActiveEventKey'}],[]];
   this.InitStandaloneVars( );
});
gx.createMasterPage(wwpbaseobjects.wwpbootstrapresponsivemasterpage);
