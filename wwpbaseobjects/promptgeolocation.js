/**@preserve  GeneXus C# 10_3_14-114418 on 5/18/2020 12:56:47.3
*/
gx.evt.autoSkip = false;
gx.define('wwpbaseobjects.promptgeolocation', false, function () {
   this.ServerClass =  "wwpbaseobjects.promptgeolocation" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV5Geolocation=gx.fn.getControlValue("vGEOLOCATION") ;
   };
   this.e120f2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e140f1_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,14];
   this.GXLastCtrlId =14;
   this.GOOGLEMAPCONTROL1Container = gx.uc.getNew(this, 11, 0, "gxMap", "GOOGLEMAPCONTROL1Container", "Googlemapcontrol1");
   var GOOGLEMAPCONTROL1Container = this.GOOGLEMAPCONTROL1Container;
   GOOGLEMAPCONTROL1Container.setProp("Title", "Title", "Map Title", "str");
   GOOGLEMAPCONTROL1Container.setProp("Width", "Width", "500px", "str");
   GOOGLEMAPCONTROL1Container.setProp("Height", "Height", "350px", "str");
   GOOGLEMAPCONTROL1Container.setProp("Provider", "Provider", "GOOGLE", "str");
   GOOGLEMAPCONTROL1Container.setProp("Type", "Type", "G_NORMAL_MAP", "str");
   GOOGLEMAPCONTROL1Container.setProp("City", "City", "0,0", "str");
   GOOGLEMAPCONTROL1Container.setDynProp("Latitude", "Latitude", "-34.906275829530244", "str");
   GOOGLEMAPCONTROL1Container.setDynProp("Longitude", "Longitude", "-56.199703216552734", "str");
   GOOGLEMAPCONTROL1Container.setProp("Precision", "Precision", 15, "num");
   GOOGLEMAPCONTROL1Container.setProp("Small_Control", "Gsmall", "GSmall_True", "str");
   GOOGLEMAPCONTROL1Container.setProp("Type_Control", "Gtype", "GType_False", "str");
   GOOGLEMAPCONTROL1Container.setProp("Scale_Control", "Scale", "GScale_False", "str");
   GOOGLEMAPCONTROL1Container.setProp("OverView_Control", "Overview", "GOverviewMap_False", "str");
   GOOGLEMAPCONTROL1Container.setProp("Small_Zoom_Control", "Smallzoom", "GSmallZoom_False", "str");
   GOOGLEMAPCONTROL1Container.setProp("Large_Control", "Large", "GLarge_False", "str");
   GOOGLEMAPCONTROL1Container.setProp("MapType_Control_Style", "Maptype_control_style", "DEFAULT", "str");
   GOOGLEMAPCONTROL1Container.setProp("Navigation_Control_Style", "Navigation_control_style", "DEFAULT", "str");
   GOOGLEMAPCONTROL1Container.setProp("ScrollWheel", "Scrollwheel", true, "bool");
   GOOGLEMAPCONTROL1Container.addV2CFunction('GxMapData', "vGXMAPDATA", 'SetData');
   GOOGLEMAPCONTROL1Container.addC2VFunction(function(UC) { UC.ParentObject.GxMapData=UC.GetData();gx.fn.setControlValue("vGXMAPDATA",UC.ParentObject.GxMapData); });
   GOOGLEMAPCONTROL1Container.addV2CFunction('AV8Latitude', "vLATITUDE", 'SetClickLatitude');
   GOOGLEMAPCONTROL1Container.addC2VFunction(function(UC) { UC.ParentObject.AV8Latitude=UC.GetClickLatitude();gx.fn.setControlValue("vLATITUDE",UC.ParentObject.AV8Latitude); });
   GOOGLEMAPCONTROL1Container.addV2CFunction('AV9Longitude', "vLONGITUDE", 'SetClickLongitude');
   GOOGLEMAPCONTROL1Container.addC2VFunction(function(UC) { UC.ParentObject.AV9Longitude=UC.GetClickLongitude();gx.fn.setControlValue("vLONGITUDE",UC.ParentObject.AV9Longitude); });
   GOOGLEMAPCONTROL1Container.setProp("InformationControl", "Informationcontrol", "ControlName", "str");
   GOOGLEMAPCONTROL1Container.setProp("Icon", "Icon", "Default", "str");
   GOOGLEMAPCONTROL1Container.setProp("GoogleApiKey", "Googleapikey", "", "str");
   GOOGLEMAPCONTROL1Container.setProp("OpenLinksInNewWindow", "Openlinksinnewwindow", "OpenNew_True", "str");
   GOOGLEMAPCONTROL1Container.setProp("KML", "Kml", false, "bool");
   GOOGLEMAPCONTROL1Container.setProp("KMLURL", "Kmlurl", "", "str");
   GOOGLEMAPCONTROL1Container.setProp("getIcon", "Geticon", "Orange", "str");
   GOOGLEMAPCONTROL1Container.setProp("Onclick", "Onclick", "getvalue", "str");
   GOOGLEMAPCONTROL1Container.setProp("CenterWhenClick", "Centerwhenclick", true, "bool");
   GOOGLEMAPCONTROL1Container.setProp("Clear_Overlay", "Clear_overlay", false, "bool");
   GOOGLEMAPCONTROL1Container.setProp("BaiduKey", "Baidukey", "41e051de8c41e0a4532e367c6b0e12fc", "str");
   GOOGLEMAPCONTROL1Container.setProp("Travel_Mode", "Travel_mode", "", "str");
   GOOGLEMAPCONTROL1Container.setProp("Visible", "Visible", true, "bool");
   GOOGLEMAPCONTROL1Container.setProp("Enabled", "Enabled", true, "boolean");
   GOOGLEMAPCONTROL1Container.setProp("Class", "Class", "", "char");
   GOOGLEMAPCONTROL1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(GOOGLEMAPCONTROL1Container);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"UTGEOLOCATION",grid:0};
   GXValidFnc[14]={fld:"ACTIONGROUP_ACTIONS",grid:0};
   this.GxMapData = {} ;
   this.AV5Geolocation = "" ;
   this.Events = {"e120f2_client": ["ENTER", true] ,"e140f1_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["ENTER"] = [[{av:'this.GOOGLEMAPCONTROL1Container.Latitude',ctrl:'GOOGLEMAPCONTROL1',prop:'Latitude'},{av:'AV5Geolocation',fld:'vGEOLOCATION',pic:'',nv:''},{av:'this.GOOGLEMAPCONTROL1Container.Longitude',ctrl:'GOOGLEMAPCONTROL1',prop:'Longitude'}],[{av:'AV8Latitude',fld:'vLATITUDE',pic:'',nv:''},{av:'AV9Longitude',fld:'vLONGITUDE',pic:'',nv:''},{av:'AV5Geolocation',fld:'vGEOLOCATION',pic:'',nv:''}]];
   this.EnterCtrl = ["BTNENTER"];
   this.setVCMap("AV5Geolocation", "vGEOLOCATION", 0, "svchar");
   this.InitStandaloneVars( );
});
gx.createParentObj(wwpbaseobjects.promptgeolocation);
