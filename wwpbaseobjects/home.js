/**@preserve  GeneXus C# 10_3_14-114418 on 4/28/2020 23:10:5.9
*/
gx.evt.autoSkip = false;
gx.define('wwpbaseobjects.home', false, function () {
   this.ServerClass =  "wwpbaseobjects.home" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
   };
   this.e130c2_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e140c2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,3,4,5,6,7,8,9,11,12,13,14,15,17,20,21];
   this.GXLastCtrlId =21;
   this.FsgridContainer = new gx.grid.grid(this, 2,"WbpLvl2",16,"Fsgrid","Fsgrid","FsgridContainer",this.CmpContext,this.IsMasterPage,"wwpbaseobjects.home",[],true,1,true,true,0,false,false,false,"",0,"px","Novo registro",false,false,false,null,null,false,"",true,[1,1,1,1]);
   var FsgridContainer = this.FsgridContainer;
   FsgridContainer.startTable("Unnamedtablefsfsgrid",17,"0px");
   FsgridContainer.startRow("","","","","","");
   FsgridContainer.startCell("","","","","","","","","","");
   FsgridContainer.startDiv("","0px","0px");
   FsgridContainer.addLabel();
   FsgridContainer.addMultipleLineEdit("Objectname",21,"vOBJECTNAME","","ObjectName","svchar",80,"chr",4,"row","256",256,"left",null,true,false,0,"");
   FsgridContainer.endDiv();
   FsgridContainer.endCell();
   FsgridContainer.endRow();
   FsgridContainer.endTable();
   this.setGrid(FsgridContainer);
   GXValidFnc[2]={fld:"LAYOUT_TABLEMAIN",grid:0};
   GXValidFnc[3]={fld:"",grid:0};
   GXValidFnc[4]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"",grid:0};
   GXValidFnc[6]={fld:"",grid:0};
   GXValidFnc[7]={fld:"TEXTBLOCKTITLE", format:0,grid:0};
   GXValidFnc[8]={fld:"",grid:0};
   GXValidFnc[9]={fld:"",grid:0};
   GXValidFnc[11]={fld:"",grid:0};
   GXValidFnc[12]={fld:"",grid:0};
   GXValidFnc[13]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[14]={fld:"",grid:0};
   GXValidFnc[15]={fld:"",grid:0};
   GXValidFnc[17]={fld:"UNNAMEDTABLEFSFSGRID",grid:16};
   GXValidFnc[20]={fld:"",grid:16};
   GXValidFnc[21]={lvl:2,type:"svchar",len:256,dec:0,sign:false,ro:1,isacc:0, multiline:true,grid:16,gxgrid:this.FsgridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vOBJECTNAME",gxz:"ZV5ObjectName",gxold:"OV5ObjectName",gxvar:"AV5ObjectName",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.AV5ObjectName=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV5ObjectName=Value},v2c:function(row){gx.fn.setGridControlValue("vOBJECTNAME",row || gx.fn.currentGridRowImpl(16),gx.O.AV5ObjectName,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV5ObjectName=this.val()},val:function(row){return gx.fn.getGridControlValue("vOBJECTNAME",row || gx.fn.currentGridRowImpl(16))},nac:gx.falseFn};
   this.AV5ObjectName = "" ;
   this.Events = {"e130c2_client": ["ENTER", true] ,"e140c2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'FSGRID_nFirstRecordOnPage',nv:0},{av:'FSGRID_nEOF',nv:0}],[]];
   this.EvtParms["FSGRID.LOAD"] = [[],[{av:'AV5ObjectName',fld:'vOBJECTNAME',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vOBJECTNAME","Link")',ctrl:'vOBJECTNAME',prop:'Link'}]];
   this.InitStandaloneVars( );
});
gx.createParentObj(wwpbaseobjects.home);
