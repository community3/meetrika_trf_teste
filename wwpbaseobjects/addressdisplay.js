/**@preserve  GeneXus C# 10_3_14-114418 on 4/28/2020 23:10:4.73
*/
gx.evt.autoSkip = false;
gx.define('wwpbaseobjects.addressdisplay', false, function () {
   this.ServerClass =  "wwpbaseobjects.addressdisplay" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV5Address=gx.fn.getControlValue("vADDRESS") ;
      this.AV6Geolocation=gx.fn.getControlValue("vGEOLOCATION") ;
   };
   this.e130a2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e140a2_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5];
   this.GXLastCtrlId =5;
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"UTEMBEDDEDPAGE",grid:0};
   this.AV5Address = "" ;
   this.AV6Geolocation = "" ;
   this.Events = {"e130a2_client": ["ENTER", true] ,"e140a2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.setVCMap("AV5Address", "vADDRESS", 0, "svchar");
   this.setVCMap("AV6Geolocation", "vGEOLOCATION", 0, "svchar");
   this.InitStandaloneVars( );
});
gx.createParentObj(wwpbaseobjects.addressdisplay);
