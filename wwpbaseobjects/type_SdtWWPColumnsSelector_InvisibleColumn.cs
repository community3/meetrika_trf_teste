/*
               File: wwpbaseobjects.type_SdtWWPColumnsSelector_InvisibleColumn
        Description: WWPBaseObjects\WWPColumnsSelector
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/4/2020 0:18:43.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "WWPColumnsSelector.InvisibleColumn" )]
   [XmlType(TypeName =  "WWPColumnsSelector.InvisibleColumn" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtWWPColumnsSelector_InvisibleColumn : GxUserType
   {
      public SdtWWPColumnsSelector_InvisibleColumn( )
      {
         /* Constructor for serialization */
         gxTv_SdtWWPColumnsSelector_InvisibleColumn_Columnname = "";
      }

      public SdtWWPColumnsSelector_InvisibleColumn( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn obj ;
         obj = this;
         obj.gxTpr_Columnname = deserialized.gxTpr_Columnname;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ColumnName") )
               {
                  gxTv_SdtWWPColumnsSelector_InvisibleColumn_Columnname = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPBaseObjects\\WWPColumnsSelector.InvisibleColumn";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ColumnName", StringUtil.RTrim( gxTv_SdtWWPColumnsSelector_InvisibleColumn_Columnname));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ColumnName", gxTv_SdtWWPColumnsSelector_InvisibleColumn_Columnname, false);
         return  ;
      }

      [  SoapElement( ElementName = "ColumnName" )]
      [  XmlElement( ElementName = "ColumnName"   )]
      public String gxTpr_Columnname
      {
         get {
            return gxTv_SdtWWPColumnsSelector_InvisibleColumn_Columnname ;
         }

         set {
            gxTv_SdtWWPColumnsSelector_InvisibleColumn_Columnname = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtWWPColumnsSelector_InvisibleColumn_Columnname = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String gxTv_SdtWWPColumnsSelector_InvisibleColumn_Columnname ;
   }

   [DataContract(Name = @"WWPBaseObjects\WWPColumnsSelector.InvisibleColumn", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtWWPColumnsSelector_InvisibleColumn_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtWWPColumnsSelector_InvisibleColumn_RESTInterface( ) : base()
      {
      }

      public SdtWWPColumnsSelector_InvisibleColumn_RESTInterface( wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ColumnName" , Order = 0 )]
      public String gxTpr_Columnname
      {
         get {
            return sdt.gxTpr_Columnname ;
         }

         set {
            sdt.gxTpr_Columnname = (String)(value);
         }

      }

      public wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn sdt
      {
         get {
            return (wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn() ;
         }
      }

   }

}
