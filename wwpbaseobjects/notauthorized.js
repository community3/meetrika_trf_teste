/**@preserve  GeneXus C# 10_3_14-114418 on 4/28/2020 23:10:5.96
*/
gx.evt.autoSkip = false;
gx.define('wwpbaseobjects.notauthorized', false, function () {
   this.ServerClass =  "wwpbaseobjects.notauthorized" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.GxObject=gx.fn.getControlValue("vGXOBJECT") ;
   };
   this.e130e2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e140e2_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,3,4,5,6,7,8,9];
   this.GXLastCtrlId =9;
   GXValidFnc[2]={fld:"LAYOUT_TABLEMAIN",grid:0};
   GXValidFnc[3]={fld:"",grid:0};
   GXValidFnc[4]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"",grid:0};
   GXValidFnc[6]={fld:"",grid:0};
   GXValidFnc[7]={fld:"TEXTBLOCKTITLE", format:0,grid:0};
   GXValidFnc[8]={fld:"",grid:0};
   GXValidFnc[9]={fld:"",grid:0};
   this.GxObject = "" ;
   this.Events = {"e130e2_client": ["ENTER", true] ,"e140e2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.setVCMap("GxObject", "vGXOBJECT", 0, "svchar");
   this.InitStandaloneVars( );
});
gx.createParentObj(wwpbaseobjects.notauthorized);
