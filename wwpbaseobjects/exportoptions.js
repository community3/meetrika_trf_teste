/**@preserve  GeneXus C# 10_3_14-114418 on 5/18/2020 12:56:46.36
*/
gx.evt.autoSkip = false;
gx.define('wwpbaseobjects.exportoptions', false, function () {
   this.ServerClass =  "wwpbaseobjects.exportoptions" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV11GoogleDocResultXML=gx.fn.getControlValue("vGOOGLEDOCRESULTXML") ;
      this.AV9ExcelFileName=gx.fn.getControlValue("vEXCELFILENAME") ;
      this.AV7DefaultTitle=gx.fn.getControlValue("vDEFAULTTITLE") ;
   };
   this.Validv_Exporttype=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vEXPORTTYPE");
         this.AnyError  = 0;
         if ( ! ( ( this.AV10ExportType == 1 ) || ( this.AV10ExportType == 2 ) ) )
         {
            try {
               gxballoon.setError("Campo Export Type fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e15091_client=function()
   {
      this.clearMessages();
      if ( this.AV10ExportType == 2 )
      {
         gx.fn.setCtrlProperty("TABLEGOOGLEDRIVEINFO","Visible", true );
         gx.fn.setCtrlProperty("BTNDOWNLOADTOFILE","Visible", false );
         gx.fn.setCtrlProperty("BTNSAVEGOOGLEDRIVE","Visible", true );
      }
      else
      {
         gx.fn.setCtrlProperty("TABLEGOOGLEDRIVEINFO","Visible", false );
         gx.fn.setCtrlProperty("BTNDOWNLOADTOFILE","Visible", true );
         gx.fn.setCtrlProperty("BTNSAVEGOOGLEDRIVE","Visible", false );
      }
      this.refreshOutputs([{av:'gx.fn.getCtrlProperty("TABLEGOOGLEDRIVEINFO","Visible")',ctrl:'TABLEGOOGLEDRIVEINFO',prop:'Visible'},{ctrl:'BTNDOWNLOADTOFILE',prop:'Visible'},{ctrl:'BTNSAVEGOOGLEDRIVE',prop:'Visible'}]);
   };
   this.e12092_client=function()
   {
      this.executeServerEvent("'DODOWNLOADTOFILE'", false, null, false, false);
   };
   this.e13092_client=function()
   {
      this.executeServerEvent("'DOSAVEGOOGLEDRIVE'", false, null, false, false);
   };
   this.e16092_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e17091_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,11,16,17,18,19,20,21,22,25,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,47,53];
   this.GXLastCtrlId =53;
   this.INNEWWINDOW1Container = gx.uc.getNew(this, 50, 22, "InNewWindow", "INNEWWINDOW1Container", "Innewwindow1");
   var INNEWWINDOW1Container = this.INNEWWINDOW1Container;
   INNEWWINDOW1Container.setDynProp("Width", "Width", "50", "str");
   INNEWWINDOW1Container.setDynProp("Height", "Height", "50", "str");
   INNEWWINDOW1Container.setProp("Name", "Name", "", "str");
   INNEWWINDOW1Container.setDynProp("Target", "Target", "", "str");
   INNEWWINDOW1Container.setProp("Fullscreen", "Fullscreen", false, "bool");
   INNEWWINDOW1Container.setProp("Location", "Location", true, "bool");
   INNEWWINDOW1Container.setProp("MenuBar", "Menubar", true, "bool");
   INNEWWINDOW1Container.setProp("Resizable", "Resizable", true, "bool");
   INNEWWINDOW1Container.setProp("Scrollbars", "Scrollbars", true, "bool");
   INNEWWINDOW1Container.setProp("TitleBar", "Titlebar", true, "bool");
   INNEWWINDOW1Container.setProp("ToolBar", "Toolbar", true, "bool");
   INNEWWINDOW1Container.setProp("directories", "Directories", true, "bool");
   INNEWWINDOW1Container.setProp("status", "Status", true, "bool");
   INNEWWINDOW1Container.setProp("copyhistory", "Copyhistory", true, "bool");
   INNEWWINDOW1Container.setProp("top", "Top", "5", "str");
   INNEWWINDOW1Container.setProp("left", "Left", "5", "str");
   INNEWWINDOW1Container.setProp("fitscreen", "Fitscreen", false, "bool");
   INNEWWINDOW1Container.setProp("RefreshParentOnClose", "Refreshparentonclose", false, "bool");
   INNEWWINDOW1Container.setProp("Targets", "Targets", '', "str");
   INNEWWINDOW1Container.setProp("Visible", "Visible", true, "bool");
   INNEWWINDOW1Container.setProp("Enabled", "Enabled", true, "boolean");
   INNEWWINDOW1Container.setProp("Class", "Class", "", "char");
   INNEWWINDOW1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(INNEWWINDOW1Container);
   this.DVPANEL_TABLEEXPORTContainer = gx.uc.getNew(this, 14, 0, "BootstrapPanel", "DVPANEL_TABLEEXPORTContainer", "Dvpanel_tableexport");
   var DVPANEL_TABLEEXPORTContainer = this.DVPANEL_TABLEEXPORTContainer;
   DVPANEL_TABLEEXPORTContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_TABLEEXPORTContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_TABLEEXPORTContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_TABLEEXPORTContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_TABLEEXPORTContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_TABLEEXPORTContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_TABLEEXPORTContainer.setProp("Title", "Title", "Onde exportar?", "str");
   DVPANEL_TABLEEXPORTContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_TABLEEXPORTContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_TABLEEXPORTContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_TABLEEXPORTContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_TABLEEXPORTContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_TABLEEXPORTContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_TABLEEXPORTContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_TABLEEXPORTContainer.setProp("Class", "Class", "", "char");
   DVPANEL_TABLEEXPORTContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_TABLEEXPORTContainer);
   this.DVPANEL_TABLEATTRIBUTESContainer = gx.uc.getNew(this, 28, 22, "BootstrapPanel", "DVPANEL_TABLEATTRIBUTESContainer", "Dvpanel_tableattributes");
   var DVPANEL_TABLEATTRIBUTESContainer = this.DVPANEL_TABLEATTRIBUTESContainer;
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Title", "Title", "Informação de Google Drive", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Class", "Class", "", "char");
   DVPANEL_TABLEATTRIBUTESContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_TABLEATTRIBUTESContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"JS", format:2,grid:0};
   GXValidFnc[11]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[16]={fld:"LAYOUT_TABLEEXPORT",grid:0};
   GXValidFnc[17]={fld:"",grid:0};
   GXValidFnc[18]={fld:"TABLEEXPORT",grid:0};
   GXValidFnc[19]={fld:"",grid:0};
   GXValidFnc[20]={fld:"",grid:0};
   GXValidFnc[21]={fld:"",grid:0};
   GXValidFnc[22]={lvl:0,type:"int",len:1,dec:0,sign:false,pic:"9",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Exporttype,isvalid:null,rgrid:[],fld:"vEXPORTTYPE",gxz:"ZV10ExportType",gxold:"OV10ExportType",gxvar:"AV10ExportType",ucs:[],op:[22],ip:[22],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV10ExportType=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV10ExportType=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vEXPORTTYPE",gx.O.AV10ExportType);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV10ExportType=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vEXPORTTYPE",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 22 , function() {
   });
   GXValidFnc[25]={fld:"TABLEGOOGLEDRIVEINFO",grid:0};
   GXValidFnc[30]={fld:"LAYOUT_TABLEATTRIBUTES",grid:0};
   GXValidFnc[31]={fld:"",grid:0};
   GXValidFnc[32]={fld:"TABLEATTRIBUTES",grid:0};
   GXValidFnc[33]={fld:"",grid:0};
   GXValidFnc[34]={fld:"",grid:0};
   GXValidFnc[35]={fld:"",grid:0};
   GXValidFnc[36]={lvl:0,type:"char",len:40,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vUSER",gxz:"ZV19User",gxold:"OV19User",gxvar:"AV19User",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV19User=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV19User=Value},v2c:function(){gx.fn.setControlValue("vUSER",gx.O.AV19User,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV19User=this.val()},val:function(){return gx.fn.getControlValue("vUSER")},nac:gx.falseFn};
   this.declareDomainHdlr( 36 , function() {
   });
   GXValidFnc[37]={fld:"",grid:0};
   GXValidFnc[38]={fld:"",grid:0};
   GXValidFnc[39]={fld:"",grid:0};
   GXValidFnc[40]={lvl:0,type:"char",len:40,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDOCTITLE",gxz:"ZV5DocTitle",gxold:"OV5DocTitle",gxvar:"AV5DocTitle",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV5DocTitle=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV5DocTitle=Value},v2c:function(){gx.fn.setControlValue("vDOCTITLE",gx.O.AV5DocTitle,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV5DocTitle=this.val()},val:function(){return gx.fn.getControlValue("vDOCTITLE")},nac:gx.falseFn};
   this.declareDomainHdlr( 40 , function() {
   });
   GXValidFnc[41]={fld:"",grid:0};
   GXValidFnc[42]={fld:"",grid:0};
   GXValidFnc[43]={fld:"",grid:0};
   GXValidFnc[44]={lvl:0,type:"char",len:40,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vPASSWORD",gxz:"ZV17Password",gxold:"OV17Password",gxvar:"AV17Password",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV17Password=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV17Password=Value},v2c:function(){gx.fn.setControlValue("vPASSWORD",gx.O.AV17Password,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV17Password=this.val()},val:function(){return gx.fn.getControlValue("vPASSWORD")},nac:gx.falseFn};
   this.declareDomainHdlr( 44 , function() {
   });
   GXValidFnc[47]={fld:"UT",grid:0};
   GXValidFnc[53]={fld:"ACTIONGROUP_ACTIONS",grid:0};
   this.AV10ExportType = 0 ;
   this.ZV10ExportType = 0 ;
   this.OV10ExportType = 0 ;
   this.AV19User = "" ;
   this.ZV19User = "" ;
   this.OV19User = "" ;
   this.AV5DocTitle = "" ;
   this.ZV5DocTitle = "" ;
   this.OV5DocTitle = "" ;
   this.AV17Password = "" ;
   this.ZV17Password = "" ;
   this.OV17Password = "" ;
   this.AV10ExportType = 0 ;
   this.AV19User = "" ;
   this.AV5DocTitle = "" ;
   this.AV17Password = "" ;
   this.AV9ExcelFileName = "" ;
   this.AV7DefaultTitle = "" ;
   this.AV11GoogleDocResultXML = "" ;
   this.Events = {"e12092_client": ["'DODOWNLOADTOFILE'", true] ,"e13092_client": ["'DOSAVEGOOGLEDRIVE'", true] ,"e16092_client": ["ENTER", true] ,"e17091_client": ["CANCEL", true] ,"e15091_client": ["VEXPORTTYPE.CLICK", false]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["'DODOWNLOADTOFILE'"] = [[],[]];
   this.EvtParms["'DOSAVEGOOGLEDRIVE'"] = [[{av:'AV11GoogleDocResultXML',fld:'vGOOGLEDOCRESULTXML',pic:'',nv:''}],[{av:'this.INNEWWINDOW1Container.Target',ctrl:'INNEWWINDOW1',prop:'Target'},{av:'this.INNEWWINDOW1Container.Height',ctrl:'INNEWWINDOW1',prop:'Height'},{av:'this.INNEWWINDOW1Container.Width',ctrl:'INNEWWINDOW1',prop:'Width'},{ctrl:'BTNCANCEL',prop:'Caption'},{av:'gx.fn.getCtrlProperty("TABLECONTENT","Visible")',ctrl:'TABLECONTENT',prop:'Visible'},{ctrl:'BTNDOWNLOADTOFILE',prop:'Visible'},{ctrl:'BTNSAVEGOOGLEDRIVE',prop:'Visible'}]];
   this.EvtParms["VEXPORTTYPE.CLICK"] = [[{av:'AV10ExportType',fld:'vEXPORTTYPE',pic:'9',nv:0}],[{av:'gx.fn.getCtrlProperty("TABLEGOOGLEDRIVEINFO","Visible")',ctrl:'TABLEGOOGLEDRIVEINFO',prop:'Visible'},{ctrl:'BTNDOWNLOADTOFILE',prop:'Visible'},{ctrl:'BTNSAVEGOOGLEDRIVE',prop:'Visible'}]];
   this.setVCMap("AV11GoogleDocResultXML", "vGOOGLEDOCRESULTXML", 0, "vchar");
   this.setVCMap("AV9ExcelFileName", "vEXCELFILENAME", 0, "svchar");
   this.setVCMap("AV7DefaultTitle", "vDEFAULTTITLE", 0, "char");
   this.InitStandaloneVars( );
});
gx.createParentObj(wwpbaseobjects.exportoptions);
