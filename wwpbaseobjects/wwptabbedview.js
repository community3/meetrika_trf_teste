/**@preserve  GeneXus C# 10_3_14-114418 on 4/28/2020 22:50:39.85
*/
gx.evt.autoSkip = false;
gx.define('wwpbaseobjects.wwptabbedview', true, function (CmpContext) {
   this.ServerClass =  "wwpbaseobjects.wwptabbedview" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV9FirstTab=gx.fn.getIntegerValue("vFIRSTTAB",'.') ;
      this.AV10LastTab=gx.fn.getIntegerValue("vLASTTAB",'.') ;
      this.AV5Tabs=gx.fn.getControlValue("vTABS") ;
      this.AV13IsSelectedTab=gx.fn.getControlValue("vISSELECTEDTAB") ;
      this.AV7TabCode=gx.fn.getControlValue("vTABCODE") ;
      this.AV6Tab=gx.fn.getControlValue("vTAB") ;
      this.AV11Index=gx.fn.getIntegerValue("vINDEX",'.') ;
      this.AV8SelectedTab=gx.fn.getIntegerValue("vSELECTEDTAB",'.') ;
      this.AV12IsFirstTab=gx.fn.getControlValue("vISFIRSTTAB") ;
   };
   this.s122_client=function()
   {
      this.AV9FirstTab = gx.num.trunc( 1 ,0) ;
      this.AV10LastTab = gx.num.trunc( this.AV5Tabs.length ,0) ;
      gx.fn.setCtrlProperty("TABPREVIOUS","Visible", 0 );
      gx.fn.setCtrlProperty("TABNEXT","Visible", 0 );
   };
   this.e120g2_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e130g2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,8,11,19,22,24,26,28,31,34];
   this.GXLastCtrlId =34;
   this.TabsgridContainer = new gx.grid.grid(this, 2,"WbpLvl2",14,"Tabsgrid","Tabsgrid","TabsgridContainer",this.CmpContext,this.IsMasterPage,"wwpbaseobjects.wwptabbedview",[],true,0,true,true,0,false,false,false,"",0,"px","Novo registro",false,false,false,null,null,false,"",false,[1,1,1,1]);
   var TabsgridContainer = this.TabsgridContainer;
   TabsgridContainer.startRow("","","top","","","");
   TabsgridContainer.startCell("","","bottom","","","","","","","");
   TabsgridContainer.addBitmap("Begintab","BEGINTAB",17,0,"px",0,"px",null,"","","Image","");
   TabsgridContainer.endCell();
   TabsgridContainer.startCell("","","top","","","","63px","","","");
   TabsgridContainer.startTable("Tabletab",19,"0px");
   TabsgridContainer.addHtmlCode("<tbody>");
   TabsgridContainer.startRow("","","","","","");
   TabsgridContainer.startCell("","","","","","","","","","");
   TabsgridContainer.addTextBlock('TAB',null);
   TabsgridContainer.endCell();
   TabsgridContainer.endRow();
   TabsgridContainer.addHtmlCode("</tbody>");
   TabsgridContainer.endTable();
   TabsgridContainer.endCell();
   TabsgridContainer.endRow();
   this.setGrid(TabsgridContainer);
   GXValidFnc[2]={fld:"TABLE4",grid:0};
   GXValidFnc[5]={fld:"TABLE3",grid:0};
   GXValidFnc[8]={fld:"TABLE1",grid:0};
   GXValidFnc[11]={fld:"TABLETABS",grid:0};
   GXValidFnc[19]={fld:"TABLETAB",grid:14};
   GXValidFnc[22]={fld:"TAB", format:0,grid:14};
   GXValidFnc[24]={fld:"ENDTAB",grid:0};
   GXValidFnc[26]={fld:"TABPREVIOUS",grid:0};
   GXValidFnc[28]={fld:"TABNEXT",grid:0};
   GXValidFnc[31]={fld:"TABLE2",grid:0};
   GXValidFnc[34]={fld:"TABLECOMPONENT",grid:0};
   this.AV5Tabs = [ ] ;
   this.AV7TabCode = "" ;
   this.AV9FirstTab = 0 ;
   this.AV10LastTab = 0 ;
   this.AV13IsSelectedTab = false ;
   this.AV6Tab = {} ;
   this.AV11Index = 0 ;
   this.AV8SelectedTab = 0 ;
   this.AV12IsFirstTab = false ;
   this.Events = {"e120g2_client": ["ENTER", true] ,"e130g2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'TABSGRID_nFirstRecordOnPage',nv:0},{av:'TABSGRID_nEOF',nv:0},{av:'AV9FirstTab',fld:'vFIRSTTAB',pic:'ZZZ9',nv:0},{av:'AV10LastTab',fld:'vLASTTAB',pic:'ZZZ9',nv:0},{av:'AV5Tabs',fld:'vTABS',pic:'',nv:null},{av:'AV13IsSelectedTab',fld:'vISSELECTEDTAB',pic:'',nv:false},{av:'AV7TabCode',fld:'vTABCODE',pic:'',nv:''},{av:'AV6Tab',fld:'vTAB',pic:'',nv:null},{av:'AV11Index',fld:'vINDEX',pic:'ZZZ9',nv:0},{av:'AV8SelectedTab',fld:'vSELECTEDTAB',pic:'ZZZ9',nv:0},{av:'AV12IsFirstTab',fld:'vISFIRSTTAB',pic:'',nv:false},{av:'sPrefix',nv:''}],[]];
   this.EvtParms["TABSGRID.LOAD"] = [[{av:'AV9FirstTab',fld:'vFIRSTTAB',pic:'ZZZ9',nv:0},{av:'AV10LastTab',fld:'vLASTTAB',pic:'ZZZ9',nv:0},{av:'AV5Tabs',fld:'vTABS',pic:'',nv:null},{av:'AV13IsSelectedTab',fld:'vISSELECTEDTAB',pic:'',nv:false},{av:'AV7TabCode',fld:'vTABCODE',pic:'',nv:''},{av:'AV6Tab',fld:'vTAB',pic:'',nv:null},{av:'AV11Index',fld:'vINDEX',pic:'ZZZ9',nv:0},{av:'AV8SelectedTab',fld:'vSELECTEDTAB',pic:'ZZZ9',nv:0},{av:'AV12IsFirstTab',fld:'vISFIRSTTAB',pic:'',nv:false}],[{av:'AV12IsFirstTab',fld:'vISFIRSTTAB',pic:'',nv:false},{av:'AV11Index',fld:'vINDEX',pic:'ZZZ9',nv:0},{av:'AV6Tab',fld:'vTAB',pic:'',nv:null},{av:'gx.fn.getCtrlProperty("ENDTAB","Bitmap")',ctrl:'ENDTAB',prop:'Bitmap'},{av:'gx.fn.getCtrlProperty("ENDTAB","Visible")',ctrl:'ENDTAB',prop:'Visible'},{av:'AV8SelectedTab',fld:'vSELECTEDTAB',pic:'ZZZ9',nv:0},{av:'AV9FirstTab',fld:'vFIRSTTAB',pic:'ZZZ9',nv:0},{av:'AV10LastTab',fld:'vLASTTAB',pic:'ZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("TABPREVIOUS","Visible")',ctrl:'TABPREVIOUS',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABNEXT","Visible")',ctrl:'TABNEXT',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TAB","Caption")',ctrl:'TAB',prop:'Caption'},{ctrl:'COMPONENT'},{av:'AV13IsSelectedTab',fld:'vISSELECTEDTAB',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("TABLETAB","Class")',ctrl:'TABLETAB',prop:'Class'},{av:'gx.fn.getCtrlProperty("TAB","Class")',ctrl:'TAB',prop:'Class'},{av:'Tab.Link',ctrl:'TAB',prop:'Link'},{av:'gx.fn.getCtrlProperty("BEGINTAB","Bitmap")',ctrl:'BEGINTAB',prop:'Bitmap'}]];
   this.setVCMap("AV9FirstTab", "vFIRSTTAB", 0, "int");
   this.setVCMap("AV10LastTab", "vLASTTAB", 0, "int");
   this.setVCMap("AV5Tabs", "vTABS", 0, "CollWWPBaseObjects\WWPTabOptions.TabOptionsItem");
   this.setVCMap("AV13IsSelectedTab", "vISSELECTEDTAB", 0, "boolean");
   this.setVCMap("AV7TabCode", "vTABCODE", 0, "char");
   this.setVCMap("AV6Tab", "TAB", 0, "WWPBaseObjects\WWPTabOptions.TabOptionsItem");
   this.setVCMap("AV11Index", "vINDEX", 0, "int");
   this.setVCMap("AV8SelectedTab", "vSELECTEDTAB", 0, "int");
   this.setVCMap("AV12IsFirstTab", "vISFIRSTTAB", 0, "boolean");
   this.setVCMap("AV9FirstTab", "vFIRSTTAB", 0, "int");
   this.setVCMap("AV10LastTab", "vLASTTAB", 0, "int");
   this.setVCMap("AV5Tabs", "vTABS", 0, "CollWWPBaseObjects\WWPTabOptions.TabOptionsItem");
   this.setVCMap("AV13IsSelectedTab", "vISSELECTEDTAB", 0, "boolean");
   this.setVCMap("AV7TabCode", "vTABCODE", 0, "char");
   this.setVCMap("AV6Tab", "TAB", 0, "WWPBaseObjects\WWPTabOptions.TabOptionsItem");
   this.setVCMap("AV11Index", "vINDEX", 0, "int");
   this.setVCMap("AV8SelectedTab", "vSELECTEDTAB", 0, "int");
   this.setVCMap("AV12IsFirstTab", "vISFIRSTTAB", 0, "boolean");
   TabsgridContainer.addRefreshingVar({rfrVar:"AV9FirstTab"});
   TabsgridContainer.addRefreshingVar({rfrVar:"AV10LastTab"});
   TabsgridContainer.addRefreshingVar({rfrVar:"AV5Tabs"});
   TabsgridContainer.addRefreshingVar({rfrVar:"AV13IsSelectedTab"});
   TabsgridContainer.addRefreshingVar({rfrVar:"AV7TabCode"});
   TabsgridContainer.addRefreshingVar({rfrVar:"AV6Tab"});
   TabsgridContainer.addRefreshingVar({rfrVar:"AV11Index"});
   TabsgridContainer.addRefreshingVar({rfrVar:"AV8SelectedTab"});
   TabsgridContainer.addRefreshingVar({rfrVar:"AV12IsFirstTab"});
   this.InitStandaloneVars( );
   this.setComponent({id: "COMPONENT" ,GXClass: null , Prefix: "W0037" , lvl: 1 });
});
