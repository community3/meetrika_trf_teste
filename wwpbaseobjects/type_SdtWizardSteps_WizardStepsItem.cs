/*
               File: wwpbaseobjects.type_SdtWizardSteps_WizardStepsItem
        Description: WWPBaseObjects\WizardSteps
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:9.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "WizardSteps.WizardStepsItem" )]
   [XmlType(TypeName =  "WizardSteps.WizardStepsItem" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtWizardSteps_WizardStepsItem : GxUserType
   {
      public SdtWizardSteps_WizardStepsItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtWizardSteps_WizardStepsItem_Code = "";
         gxTv_SdtWizardSteps_WizardStepsItem_Title = "";
         gxTv_SdtWizardSteps_WizardStepsItem_Description = "";
      }

      public SdtWizardSteps_WizardStepsItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtWizardSteps_WizardStepsItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtWizardSteps_WizardStepsItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtWizardSteps_WizardStepsItem obj ;
         obj = this;
         obj.gxTpr_Code = deserialized.gxTpr_Code;
         obj.gxTpr_Title = deserialized.gxTpr_Title;
         obj.gxTpr_Description = deserialized.gxTpr_Description;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Code") )
               {
                  gxTv_SdtWizardSteps_WizardStepsItem_Code = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Title") )
               {
                  gxTv_SdtWizardSteps_WizardStepsItem_Title = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Description") )
               {
                  gxTv_SdtWizardSteps_WizardStepsItem_Description = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPBaseObjects\\WizardSteps.WizardStepsItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Code", StringUtil.RTrim( gxTv_SdtWizardSteps_WizardStepsItem_Code));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Title", StringUtil.RTrim( gxTv_SdtWizardSteps_WizardStepsItem_Title));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Description", StringUtil.RTrim( gxTv_SdtWizardSteps_WizardStepsItem_Description));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Code", gxTv_SdtWizardSteps_WizardStepsItem_Code, false);
         AddObjectProperty("Title", gxTv_SdtWizardSteps_WizardStepsItem_Title, false);
         AddObjectProperty("Description", gxTv_SdtWizardSteps_WizardStepsItem_Description, false);
         return  ;
      }

      [  SoapElement( ElementName = "Code" )]
      [  XmlElement( ElementName = "Code"   )]
      public String gxTpr_Code
      {
         get {
            return gxTv_SdtWizardSteps_WizardStepsItem_Code ;
         }

         set {
            gxTv_SdtWizardSteps_WizardStepsItem_Code = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Title" )]
      [  XmlElement( ElementName = "Title"   )]
      public String gxTpr_Title
      {
         get {
            return gxTv_SdtWizardSteps_WizardStepsItem_Title ;
         }

         set {
            gxTv_SdtWizardSteps_WizardStepsItem_Title = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Description" )]
      [  XmlElement( ElementName = "Description"   )]
      public String gxTpr_Description
      {
         get {
            return gxTv_SdtWizardSteps_WizardStepsItem_Description ;
         }

         set {
            gxTv_SdtWizardSteps_WizardStepsItem_Description = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtWizardSteps_WizardStepsItem_Code = "";
         gxTv_SdtWizardSteps_WizardStepsItem_Title = "";
         gxTv_SdtWizardSteps_WizardStepsItem_Description = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String gxTv_SdtWizardSteps_WizardStepsItem_Code ;
      protected String gxTv_SdtWizardSteps_WizardStepsItem_Title ;
      protected String gxTv_SdtWizardSteps_WizardStepsItem_Description ;
   }

   [DataContract(Name = @"WWPBaseObjects\WizardSteps.WizardStepsItem", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtWizardSteps_WizardStepsItem_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtWizardSteps_WizardStepsItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtWizardSteps_WizardStepsItem_RESTInterface( ) : base()
      {
      }

      public SdtWizardSteps_WizardStepsItem_RESTInterface( wwpbaseobjects.SdtWizardSteps_WizardStepsItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Code" , Order = 0 )]
      public String gxTpr_Code
      {
         get {
            return sdt.gxTpr_Code ;
         }

         set {
            sdt.gxTpr_Code = (String)(value);
         }

      }

      [DataMember( Name = "Title" , Order = 1 )]
      public String gxTpr_Title
      {
         get {
            return sdt.gxTpr_Title ;
         }

         set {
            sdt.gxTpr_Title = (String)(value);
         }

      }

      [DataMember( Name = "Description" , Order = 2 )]
      public String gxTpr_Description
      {
         get {
            return sdt.gxTpr_Description ;
         }

         set {
            sdt.gxTpr_Description = (String)(value);
         }

      }

      public wwpbaseobjects.SdtWizardSteps_WizardStepsItem sdt
      {
         get {
            return (wwpbaseobjects.SdtWizardSteps_WizardStepsItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtWizardSteps_WizardStepsItem() ;
         }
      }

   }

}
