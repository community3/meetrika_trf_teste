/*
               File: WWPBaseObjects.SidebarMenuOptionsData
        Description: Sidebar Menu Options Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:4:26.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   public class sidebarmenuoptionsdata : GXProcedure
   {
      public sidebarmenuoptionsdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public sidebarmenuoptionsdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out IGxCollection aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "DVB_SidebarMenuOptionsData.Item", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "DVB_SidebarMenuOptionsData.Item", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out IGxCollection aP0_Gxm2rootcol )
      {
         sidebarmenuoptionsdata objsidebarmenuoptionsdata;
         objsidebarmenuoptionsdata = new sidebarmenuoptionsdata();
         objsidebarmenuoptionsdata.Gxm2rootcol = new GxObjectCollection( context, "DVB_SidebarMenuOptionsData.Item", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item", "GeneXus.Programs") ;
         objsidebarmenuoptionsdata.context.SetSubmitInitialConfig(context);
         objsidebarmenuoptionsdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objsidebarmenuoptionsdata);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((sidebarmenuoptionsdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV5id = 0;
         Gxm1dvb_sidebarmenuoptionsdata = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm2rootcol.Add(Gxm1dvb_sidebarmenuoptionsdata, 0);
         AV5id = (short)(AV5id+1);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Tooltip = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Link = "wwpbaseobjects.home.aspx";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Linktarget = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Iconclass = "menu-icon glyphicon glyphicon-home";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Caption = "Inicio";
         Gxm1dvb_sidebarmenuoptionsdata = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm2rootcol.Add(Gxm1dvb_sidebarmenuoptionsdata, 0);
         AV5id = (short)(AV5id+1);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Tooltip = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Link = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Linktarget = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Iconclass = "menu-icon glyphicon glyphicon-tasks";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Caption = "Op��o de Menu 1";
         Gxm3dvb_sidebarmenuoptionsdata_subitems = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Subitems.Add(Gxm3dvb_sidebarmenuoptionsdata_subitems, 0);
         AV5id = (short)(AV5id+1);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Tooltip = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Link = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Linktarget = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Caption = "Op��o de Menu 1.1";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Authorizationkey = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Subitems.Add(Gxm3dvb_sidebarmenuoptionsdata_subitems, 0);
         AV5id = (short)(AV5id+1);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Tooltip = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Link = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Linktarget = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Caption = "Op��o de Menu 1.2";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Authorizationkey = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Subitems.Add(Gxm3dvb_sidebarmenuoptionsdata_subitems, 0);
         AV5id = (short)(AV5id+1);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Tooltip = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Link = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Linktarget = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Caption = "Op��o de Menu 1.3";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Authorizationkey = "";
         Gxm1dvb_sidebarmenuoptionsdata = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm2rootcol.Add(Gxm1dvb_sidebarmenuoptionsdata, 0);
         AV5id = (short)(AV5id+1);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Tooltip = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Link = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Linktarget = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Iconclass = "menu-icon fa fa-key";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Caption = "Op��o de Menu 2";
         Gxm3dvb_sidebarmenuoptionsdata_subitems = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Subitems.Add(Gxm3dvb_sidebarmenuoptionsdata_subitems, 0);
         AV5id = (short)(AV5id+1);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Tooltip = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Link = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Linktarget = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Iconclass = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Caption = "Op��o de Menu 2.1";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Subitems.Add(Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems, 0);
         AV5id = (short)(AV5id+1);
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Tooltip = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Link = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Linktarget = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Caption = "Op��o de Menu 2.1.1";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Authorizationkey = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Subitems.Add(Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems, 0);
         AV5id = (short)(AV5id+1);
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Tooltip = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Link = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Linktarget = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Caption = "Op��o de Menu 2.1.2";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Authorizationkey = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Subitems.Add(Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems, 0);
         AV5id = (short)(AV5id+1);
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Tooltip = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Link = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Linktarget = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Caption = "Op��o de Menu 2.1.3";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Authorizationkey = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Subitems.Add(Gxm3dvb_sidebarmenuoptionsdata_subitems, 0);
         AV5id = (short)(AV5id+1);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Tooltip = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Link = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Linktarget = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Iconclass = "";
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Caption = "Op��o de Menu 2.2";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Subitems.Add(Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems, 0);
         AV5id = (short)(AV5id+1);
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Tooltip = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Link = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Linktarget = "";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Caption = "Op��o de Menu 2.2.1";
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems.gxTpr_Authorizationkey = "";
         Gxm1dvb_sidebarmenuoptionsdata = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm2rootcol.Add(Gxm1dvb_sidebarmenuoptionsdata, 0);
         AV5id = (short)(AV5id+1);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Tooltip = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Link = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Linktarget = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Iconclass = "menu-icon fa fa-briefcase";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Caption = "Op��o de Menu 3";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Authorizationkey = "";
         Gxm1dvb_sidebarmenuoptionsdata = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm2rootcol.Add(Gxm1dvb_sidebarmenuoptionsdata, 0);
         AV5id = (short)(AV5id+1);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Tooltip = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Link = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Linktarget = "";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Iconclass = "menu-icon fa fa-pencil-square-o";
         Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Caption = "Menu de Desenvolvimento";
         Gxv5skipcount = 0;
         AV15GXV2 = 1;
         GXt_objcol_SdtProgramNames_ProgramName1 = AV14GXV1;
         new listwwpprograms(context ).execute( out  GXt_objcol_SdtProgramNames_ProgramName1) ;
         AV14GXV1 = GXt_objcol_SdtProgramNames_ProgramName1;
         while ( AV15GXV2 <= AV14GXV1.Count )
         {
            AV6ProgramName = ((wwpbaseobjects.SdtProgramNames_ProgramName)AV14GXV1.Item(AV15GXV2));
            Gxv5skipcount = (int)(Gxv5skipcount+1);
            Gxm3dvb_sidebarmenuoptionsdata_subitems = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
            Gxm1dvb_sidebarmenuoptionsdata.gxTpr_Subitems.Add(Gxm3dvb_sidebarmenuoptionsdata_subitems, 0);
            AV5id = (short)(AV5id+1);
            Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Id = StringUtil.Str( (decimal)(AV5id), 4, 0);
            Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Tooltip = "";
            Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Link = AV6ProgramName.gxTpr_Link;
            Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Linktarget = "";
            Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Iconclass = "";
            Gxm3dvb_sidebarmenuoptionsdata_subitems.gxTpr_Caption = AV6ProgramName.gxTpr_Description;
            if ( ( 20 != 0 ) && ( Gxv5skipcount >= 20 ) )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            AV15GXV2 = (int)(AV15GXV2+1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gxm1dvb_sidebarmenuoptionsdata = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm3dvb_sidebarmenuoptionsdata_subitems = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems = new wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item(context);
         AV14GXV1 = new GxObjectCollection( context, "ProgramNames.ProgramName", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtProgramNames_ProgramName", "GeneXus.Programs");
         GXt_objcol_SdtProgramNames_ProgramName1 = new GxObjectCollection( context, "ProgramNames.ProgramName", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtProgramNames_ProgramName", "GeneXus.Programs");
         AV6ProgramName = new wwpbaseobjects.SdtProgramNames_ProgramName(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV5id ;
      private int Gxv5skipcount ;
      private int AV15GXV2 ;
      private IGxCollection aP0_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtProgramNames_ProgramName ))]
      private IGxCollection AV14GXV1 ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtProgramNames_ProgramName ))]
      private IGxCollection GXt_objcol_SdtProgramNames_ProgramName1 ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item ))]
      private IGxCollection Gxm2rootcol ;
      private wwpbaseobjects.SdtProgramNames_ProgramName AV6ProgramName ;
      private wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item Gxm1dvb_sidebarmenuoptionsdata ;
      private wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item Gxm3dvb_sidebarmenuoptionsdata_subitems ;
      private wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item Gxm4dvb_sidebarmenuoptionsdata_subitems_subitems ;
   }

}
