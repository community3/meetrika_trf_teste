/*
               File: WWPBaseObjects.GetWWPTabImage
        Description: Get Tab Image Name
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:43.76
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   public class getwwptabimage : GXProcedure
   {
      public getwwptabimage( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwptabimage( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( bool aP0_IsFirstTab ,
                           bool aP1_IsSelected ,
                           bool aP2_IsBeforeTabSelected ,
                           bool aP3_IsLastTab ,
                           out String aP4_Bitmap )
      {
         this.AV9IsFirstTab = aP0_IsFirstTab;
         this.AV10IsSelected = aP1_IsSelected;
         this.AV11IsBeforeTabSelected = aP2_IsBeforeTabSelected;
         this.AV12IsLastTab = aP3_IsLastTab;
         this.AV8Bitmap = "" ;
         initialize();
         executePrivate();
         aP4_Bitmap=this.AV8Bitmap;
      }

      public String executeUdp( bool aP0_IsFirstTab ,
                                bool aP1_IsSelected ,
                                bool aP2_IsBeforeTabSelected ,
                                bool aP3_IsLastTab )
      {
         this.AV9IsFirstTab = aP0_IsFirstTab;
         this.AV10IsSelected = aP1_IsSelected;
         this.AV11IsBeforeTabSelected = aP2_IsBeforeTabSelected;
         this.AV12IsLastTab = aP3_IsLastTab;
         this.AV8Bitmap = "" ;
         initialize();
         executePrivate();
         aP4_Bitmap=this.AV8Bitmap;
         return AV8Bitmap ;
      }

      public void executeSubmit( bool aP0_IsFirstTab ,
                                 bool aP1_IsSelected ,
                                 bool aP2_IsBeforeTabSelected ,
                                 bool aP3_IsLastTab ,
                                 out String aP4_Bitmap )
      {
         getwwptabimage objgetwwptabimage;
         objgetwwptabimage = new getwwptabimage();
         objgetwwptabimage.AV9IsFirstTab = aP0_IsFirstTab;
         objgetwwptabimage.AV10IsSelected = aP1_IsSelected;
         objgetwwptabimage.AV11IsBeforeTabSelected = aP2_IsBeforeTabSelected;
         objgetwwptabimage.AV12IsLastTab = aP3_IsLastTab;
         objgetwwptabimage.AV8Bitmap = "" ;
         objgetwwptabimage.context.SetSubmitInitialConfig(context);
         objgetwwptabimage.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwptabimage);
         aP4_Bitmap=this.AV8Bitmap;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwptabimage)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV12IsLastTab )
         {
            if ( AV10IsSelected )
            {
               AV8Bitmap = context.GetImagePath( "0424c12e-f1b1-4123-889e-b2966eb27e93", "", context.GetTheme( ));
               AV15Bitmap_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0424c12e-f1b1-4123-889e-b2966eb27e93", "", context.GetTheme( )));
            }
            else
            {
               AV8Bitmap = context.GetImagePath( "a46a2602-a799-4dab-a329-130bd24fbbb4", "", context.GetTheme( ));
               AV15Bitmap_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "a46a2602-a799-4dab-a329-130bd24fbbb4", "", context.GetTheme( )));
            }
         }
         else
         {
            if ( AV10IsSelected )
            {
               if ( AV9IsFirstTab )
               {
                  AV8Bitmap = context.GetImagePath( "f4a0965f-129d-45fc-bdfe-107944e516c0", "", context.GetTheme( ));
                  AV15Bitmap_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f4a0965f-129d-45fc-bdfe-107944e516c0", "", context.GetTheme( )));
               }
               else
               {
                  AV8Bitmap = context.GetImagePath( "a908a75c-9ebf-4d01-bcda-9711089e0493", "", context.GetTheme( ));
                  AV15Bitmap_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "a908a75c-9ebf-4d01-bcda-9711089e0493", "", context.GetTheme( )));
               }
            }
            else
            {
               if ( AV9IsFirstTab )
               {
                  AV8Bitmap = context.GetImagePath( "014ad39b-4d50-4343-92ef-0117c86e1b16", "", context.GetTheme( ));
                  AV15Bitmap_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "014ad39b-4d50-4343-92ef-0117c86e1b16", "", context.GetTheme( )));
               }
               else
               {
                  if ( AV11IsBeforeTabSelected )
                  {
                     AV8Bitmap = context.GetImagePath( "3e28b5a8-f17c-43b3-8850-2e932e706b48", "", context.GetTheme( ));
                     AV15Bitmap_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3e28b5a8-f17c-43b3-8850-2e932e706b48", "", context.GetTheme( )));
                  }
                  else
                  {
                     AV8Bitmap = context.GetImagePath( "ce463bdd-5a79-4a33-a6e5-8b0b7cb85bcd", "", context.GetTheme( ));
                     AV15Bitmap_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "ce463bdd-5a79-4a33-a6e5-8b0b7cb85bcd", "", context.GetTheme( )));
                  }
               }
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV15Bitmap_GXI = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private bool AV9IsFirstTab ;
      private bool AV10IsSelected ;
      private bool AV11IsBeforeTabSelected ;
      private bool AV12IsLastTab ;
      private String AV15Bitmap_GXI ;
      private String AV8Bitmap ;
      private String aP4_Bitmap ;
   }

}
