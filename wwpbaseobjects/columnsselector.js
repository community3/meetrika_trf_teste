/**@preserve  GeneXus C# 10_3_14-114418 on 5/18/2020 12:56:44.94
*/
gx.evt.autoSkip = false;
gx.define('wwpbaseobjects.columnsselector', false, function () {
   this.ServerClass =  "wwpbaseobjects.columnsselector" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV16ColumnsSelectorSDT=gx.fn.getControlValue("vCOLUMNSSELECTORSDT") ;
      this.AV12SessionKey=gx.fn.getControlValue("vSESSIONKEY") ;
   };
   this.e110b2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e140b2_client=function()
   {
      this.executeServerEvent("'DOMOVEDOWN'", true, arguments[0], false, false);
   };
   this.e150b2_client=function()
   {
      this.executeServerEvent("'DOMOVEUP'", true, arguments[0], false, false);
   };
   this.e170b1_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,14,21,23,24,27,33];
   this.GXLastCtrlId =33;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",11,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"wwpbaseobjects.columnsselector",[],true,1,true,true,0,false,false,false,"",0,"px","Novo registro",true,false,true,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.startRow("","","","","","");
   GridContainer.startCell("","","","","","","","","","FSLineSeparatorCell");
   GridContainer.startTable("Unnamedtable1",14,"0px");
   GridContainer.addHtmlCode("<tbody>");
   GridContainer.startRow("","","","","","");
   GridContainer.startCell("","","","","","","","","","");
   GridContainer.addBitmap("Movedown","MOVEDOWN",17,0,"px",0,"px","e140b2_client","","","Image","");
   GridContainer.endCell();
   GridContainer.startCell("","","","","","","","","","");
   GridContainer.addBitmap("Moveup","MOVEUP",19,0,"px",0,"px","e150b2_client","","","Image","");
   GridContainer.endCell();
   GridContainer.startCell("","","","","","","","","","");
   GridContainer.addCheckBox("Visible",21,"vVISIBLE","","","Visible","boolean","true","false",null,true,false,4,"chr","");
   GridContainer.endCell();
   GridContainer.startCell("","","","","","","","","","");
   GridContainer.addSingleLineEdit("Columndisplayname",23,"vCOLUMNDISPLAYNAME","","","ColumnDisplayName","svchar",80,"chr",100,80,"left",null,[],"Columndisplayname","ColumnDisplayName",true,0,false,false,"AttributeNoWrap",1,"");
   GridContainer.endCell();
   GridContainer.endRow();
   GridContainer.addHtmlCode("</tbody>");
   GridContainer.endTable();
   GridContainer.addSingleLineEdit("Columnname",24,"vCOLUMNNAME","","","ColumnName","svchar",80,"chr",100,80,"left",null,[],"Columnname","ColumnName",true,0,false,false,"Attribute",1,"");
   GridContainer.endCell();
   GridContainer.endRow();
   this.setGrid(GridContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[14]={fld:"UNNAMEDTABLE1",grid:11};
   GXValidFnc[21]={lvl:2,type:"boolean",len:4,dec:0,sign:false,ro:0,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vVISIBLE",gxz:"ZV23Visible",gxold:"OV23Visible",gxvar:"AV23Visible",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV23Visible=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV23Visible=gx.lang.booleanValue(Value)},v2c:function(row){gx.fn.setGridCheckBoxValue("vVISIBLE",row || gx.fn.currentGridRowImpl(11),gx.O.AV23Visible,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV23Visible=gx.lang.booleanValue(this.val())},val:function(row){return gx.fn.getGridControlValue("vVISIBLE",row || gx.fn.currentGridRowImpl(11))},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[23]={lvl:2,type:"svchar",len:100,dec:0,sign:false,ro:0,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vCOLUMNDISPLAYNAME",gxz:"ZV27ColumnDisplayName",gxold:"OV27ColumnDisplayName",gxvar:"AV27ColumnDisplayName",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.AV27ColumnDisplayName=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV27ColumnDisplayName=Value},v2c:function(row){gx.fn.setGridControlValue("vCOLUMNDISPLAYNAME",row || gx.fn.currentGridRowImpl(11),gx.O.AV27ColumnDisplayName,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV27ColumnDisplayName=this.val()},val:function(row){return gx.fn.getGridControlValue("vCOLUMNDISPLAYNAME",row || gx.fn.currentGridRowImpl(11))},nac:gx.falseFn};
   GXValidFnc[24]={lvl:2,type:"svchar",len:100,dec:0,sign:false,ro:1,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vCOLUMNNAME",gxz:"ZV5ColumnName",gxold:"OV5ColumnName",gxvar:"AV5ColumnName",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.AV5ColumnName=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV5ColumnName=Value},v2c:function(row){gx.fn.setGridControlValue("vCOLUMNNAME",row || gx.fn.currentGridRowImpl(11),gx.O.AV5ColumnName,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV5ColumnName=this.val()},val:function(row){return gx.fn.getGridControlValue("vCOLUMNNAME",row || gx.fn.currentGridRowImpl(11))},nac:gx.falseFn};
   GXValidFnc[27]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[33]={lvl:0,type:"svchar",len:1000,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCOLUMNSSELECTORXML",gxz:"ZV8ColumnsSelectorXML",gxold:"OV8ColumnsSelectorXML",gxvar:"AV8ColumnsSelectorXML",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV8ColumnsSelectorXML=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV8ColumnsSelectorXML=Value},v2c:function(){gx.fn.setControlValue("vCOLUMNSSELECTORXML",gx.O.AV8ColumnsSelectorXML,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV8ColumnsSelectorXML=this.val()},val:function(){return gx.fn.getControlValue("vCOLUMNSSELECTORXML")},nac:gx.falseFn};
   this.ZV23Visible = false ;
   this.OV23Visible = false ;
   this.ZV27ColumnDisplayName = "" ;
   this.OV27ColumnDisplayName = "" ;
   this.ZV5ColumnName = "" ;
   this.OV5ColumnName = "" ;
   this.AV8ColumnsSelectorXML = "" ;
   this.ZV8ColumnsSelectorXML = "" ;
   this.OV8ColumnsSelectorXML = "" ;
   this.AV8ColumnsSelectorXML = "" ;
   this.AV12SessionKey = "" ;
   this.AV23Visible = false ;
   this.AV27ColumnDisplayName = "" ;
   this.AV5ColumnName = "" ;
   this.AV16ColumnsSelectorSDT = [ ] ;
   this.Events = {"e110b2_client": ["ENTER", true] ,"e140b2_client": ["'DOMOVEDOWN'", true] ,"e150b2_client": ["'DOMOVEUP'", true] ,"e170b1_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'gx.fn.getCtrlProperty("vCOLUMNNAME","Visible")',ctrl:'vCOLUMNNAME',prop:'Visible'},{av:'AV16ColumnsSelectorSDT',fld:'vCOLUMNSSELECTORSDT',pic:'',nv:null},{av:'AV8ColumnsSelectorXML',fld:'vCOLUMNSSELECTORXML',pic:'',nv:''}],[{av:'AV16ColumnsSelectorSDT',fld:'vCOLUMNSSELECTORSDT',pic:'',nv:null}]];
   this.EvtParms["GRID.LOAD"] = [[{av:'AV16ColumnsSelectorSDT',fld:'vCOLUMNSSELECTORSDT',pic:'',nv:null}],[{av:'AV5ColumnName',fld:'vCOLUMNNAME',pic:'',nv:''},{av:'AV27ColumnDisplayName',fld:'vCOLUMNDISPLAYNAME',pic:'',nv:''},{av:'AV23Visible',fld:'vVISIBLE',pic:'',nv:false}]];
   this.EvtParms["ENTER"] = [[{av:'AV16ColumnsSelectorSDT',fld:'vCOLUMNSSELECTORSDT',pic:'',nv:null},{av:'AV12SessionKey',fld:'vSESSIONKEY',pic:'',hsh:true,nv:''},{av:'AV5ColumnName',fld:'vCOLUMNNAME',grid:11,pic:'',nv:''},{av:'AV23Visible',fld:'vVISIBLE',grid:11,pic:'',nv:false}],[{av:'AV16ColumnsSelectorSDT',fld:'vCOLUMNSSELECTORSDT',pic:'',nv:null},{av:'AV8ColumnsSelectorXML',fld:'vCOLUMNSSELECTORXML',pic:'',nv:''}]];
   this.EvtParms["'DOMOVEDOWN'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'gx.fn.getCtrlProperty("vCOLUMNNAME","Visible")',ctrl:'vCOLUMNNAME',prop:'Visible'},{av:'AV8ColumnsSelectorXML',fld:'vCOLUMNSSELECTORXML',pic:'',nv:''},{av:'AV16ColumnsSelectorSDT',fld:'vCOLUMNSSELECTORSDT',pic:'',nv:null},{av:'AV5ColumnName',fld:'vCOLUMNNAME',grid:11,pic:'',nv:''},{av:'AV23Visible',fld:'vVISIBLE',grid:11,pic:'',nv:false}],[{av:'AV16ColumnsSelectorSDT',fld:'vCOLUMNSSELECTORSDT',pic:'',nv:null},{av:'AV8ColumnsSelectorXML',fld:'vCOLUMNSSELECTORXML',pic:'',nv:''}]];
   this.EvtParms["'DOMOVEUP'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'gx.fn.getCtrlProperty("vCOLUMNNAME","Visible")',ctrl:'vCOLUMNNAME',prop:'Visible'},{av:'AV8ColumnsSelectorXML',fld:'vCOLUMNSSELECTORXML',pic:'',nv:''},{av:'AV16ColumnsSelectorSDT',fld:'vCOLUMNSSELECTORSDT',pic:'',nv:null},{av:'AV5ColumnName',fld:'vCOLUMNNAME',grid:11,pic:'',nv:''},{av:'AV23Visible',fld:'vVISIBLE',grid:11,pic:'',nv:false}],[{av:'AV16ColumnsSelectorSDT',fld:'vCOLUMNSSELECTORSDT',pic:'',nv:null},{av:'AV8ColumnsSelectorXML',fld:'vCOLUMNSSELECTORXML',pic:'',nv:''}]];
   this.EnterCtrl = ["BTNENTER"];
   this.setVCMap("AV16ColumnsSelectorSDT", "vCOLUMNSSELECTORSDT", 0, "CollWWPBaseObjects\ColumnsSelectorSDT.ColumnsSelectorSDTItem");
   this.setVCMap("AV12SessionKey", "vSESSIONKEY", 0, "svchar");
   this.setVCMap("AV16ColumnsSelectorSDT", "vCOLUMNSSELECTORSDT", 0, "CollWWPBaseObjects\ColumnsSelectorSDT.ColumnsSelectorSDTItem");
   GridContainer.addRefreshingVar({rfrVar:"AV5ColumnName", rfrProp:"Visible", gxAttId:"Columnname"});
   GridContainer.addRefreshingVar(this.GXValidFnc[33]);
   GridContainer.addRefreshingVar({rfrVar:"AV16ColumnsSelectorSDT"});
   this.InitStandaloneVars( );
});
gx.createParentObj(wwpbaseobjects.columnsselector);
