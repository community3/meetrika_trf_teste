/**@preserve  GeneXus C# 10_3_14-114418 on 5/18/2020 12:56:43.68
*/
gx.evt.autoSkip = false;
gx.define('wwpbaseobjects.managefilters', false, function () {
   this.ServerClass =  "wwpbaseobjects.managefilters" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV7GridStateCollection=gx.fn.getControlValue("vGRIDSTATECOLLECTION") ;
      this.AV17UserKey=gx.fn.getControlValue("vUSERKEY") ;
   };
   this.e11072_client=function()
   {
      this.executeServerEvent("GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e12072_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e16072_client=function()
   {
      this.executeServerEvent("VMOVEUP.CLICK", true, arguments[0], false, false);
   };
   this.e17072_client=function()
   {
      this.executeServerEvent("VMOVEDOWN.CLICK", true, arguments[0], false, false);
   };
   this.e18072_client=function()
   {
      this.executeServerEvent("VUDELETE.CLICK", true, arguments[0], false, false);
   };
   this.e19071_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,15,16,17,18,19,25,33,38,41];
   this.GXLastCtrlId =41;
   this.GridgridstatecollectionsContainer = new gx.grid.grid(this, 2,"WbpLvl2",14,"Gridgridstatecollections","Gridgridstatecollections","GridgridstatecollectionsContainer",this.CmpContext,this.IsMasterPage,"wwpbaseobjects.managefilters",[],false,1,false,true,0,false,false,false,"CollWWPBaseObjects\GridStateCollection.Item",0,"px","Novo registro",true,false,false,null,null,false,"AV7GridStateCollection",false,[1,1,1,1]);
   var GridgridstatecollectionsContainer = this.GridgridstatecollectionsContainer;
   GridgridstatecollectionsContainer.addBitmap("&Moveup","vMOVEUP",15,15,"px",17,"px","e16072_client","","","Image","");
   GridgridstatecollectionsContainer.addBitmap("&Movedown","vMOVEDOWN",16,15,"px",17,"px","e17072_client","","","Image","");
   GridgridstatecollectionsContainer.addSingleLineEdit("GXV10H",17,"GRIDSTATECOLLECTION__TITLE","Filtros","","Title","svchar",0,"px",100,80,"left",null,[],"GXV10H","GXV10H",true,0,false,false,"BootstrapAttribute",1,"");
   GridgridstatecollectionsContainer.addSingleLineEdit("GXV10I",18,"GRIDSTATECOLLECTION__GRIDSTATEXML","Grid State XML","","GridStateXML","vchar",0,"px",2097152,80,"left",null,[],"GXV10I","GXV10I",false,0,false,false,"BootstrapAttribute",1,"");
   GridgridstatecollectionsContainer.addBitmap("&Udelete","vUDELETE",19,36,"px",17,"px","e18072_client","","","Image","");
   this.setGrid(GridgridstatecollectionsContainer);
   this.DVPANEL_UNNAMEDTABLE1Container = gx.uc.getNew(this, 36, 15, "BootstrapPanel", "DVPANEL_UNNAMEDTABLE1Container", "Dvpanel_unnamedtable1");
   var DVPANEL_UNNAMEDTABLE1Container = this.DVPANEL_UNNAMEDTABLE1Container;
   DVPANEL_UNNAMEDTABLE1Container.setProp("Width", "Width", "100%", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Height", "Height", "100", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Title", "Title", "", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Collapsible", "Collapsible", true, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Visible", "Visible", true, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Class", "Class", "", "char");
   DVPANEL_UNNAMEDTABLE1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_UNNAMEDTABLE1Container);
   this.GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer = gx.uc.getNew(this, 22, 15, "DVelop_DVPaginationBar", "GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer", "Gridgridstatecollectionspaginationbar");
   var GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer = this.GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer;
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.addV2CFunction('AV20GridGridStateCollectionsCurrentPage', "vGRIDGRIDSTATECOLLECTIONSCURRENTPAGE", 'SetCurrentPage');
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV20GridGridStateCollectionsCurrentPage=UC.GetCurrentPage();gx.fn.setControlValue("vGRIDGRIDSTATECOLLECTIONSCURRENTPAGE",UC.ParentObject.AV20GridGridStateCollectionsCurrentPage); });
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.addV2CFunction('AV21GridGridStateCollectionsPageCount', "vGRIDGRIDSTATECOLLECTIONSPAGECOUNT", 'SetPageCount');
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV21GridGridStateCollectionsPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDGRIDSTATECOLLECTIONSPAGECOUNT",UC.ParentObject.AV21GridGridStateCollectionsPageCount); });
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.addEventHandler("ChangePage", this.e11072_client);
   this.setUserControl(GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[11]={fld:"GRIDGRIDSTATECOLLECTIONSTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[15]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:14,gxgrid:this.GridgridstatecollectionsContainer,fnc:null,isvalid:null,rgrid:[],fld:"vMOVEUP",gxz:"ZV16MoveUp",gxold:"OV16MoveUp",gxvar:"AV16MoveUp",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV16MoveUp=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV16MoveUp=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vMOVEUP",row || gx.fn.currentGridRowImpl(14),gx.O.AV16MoveUp,gx.O.AV27Moveup_GXI)},c2v:function(){gx.O.AV27Moveup_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV16MoveUp=this.val()},val:function(row){return gx.fn.getGridControlValue("vMOVEUP",row || gx.fn.currentGridRowImpl(14))},val_GXI:function(row){return gx.fn.getGridControlValue("vMOVEUP_GXI",row || gx.fn.currentGridRowImpl(14))}, gxvar_GXI:'AV27Moveup_GXI',nac:gx.falseFn};
   GXValidFnc[16]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:14,gxgrid:this.GridgridstatecollectionsContainer,fnc:null,isvalid:null,rgrid:[],fld:"vMOVEDOWN",gxz:"ZV15MoveDown",gxold:"OV15MoveDown",gxvar:"AV15MoveDown",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV15MoveDown=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV15MoveDown=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vMOVEDOWN",row || gx.fn.currentGridRowImpl(14),gx.O.AV15MoveDown,gx.O.AV28Movedown_GXI)},c2v:function(){gx.O.AV28Movedown_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV15MoveDown=this.val()},val:function(row){return gx.fn.getGridControlValue("vMOVEDOWN",row || gx.fn.currentGridRowImpl(14))},val_GXI:function(row){return gx.fn.getGridControlValue("vMOVEDOWN_GXI",row || gx.fn.currentGridRowImpl(14))}, gxvar_GXI:'AV28Movedown_GXI',nac:gx.falseFn};
   GXValidFnc[17]={lvl:2,type:"svchar",len:100,dec:0,sign:false,ro:0,isacc:0,grid:14,gxgrid:this.GridgridstatecollectionsContainer,fnc:null,isvalid:null,rgrid:[],fld:"GRIDSTATECOLLECTION__TITLE",gxz:"ZV25GXV10H",gxold:"OV25GXV10H",gxvar:"GXV10H",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.GXV10H=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV25GXV10H=Value},v2c:function(row){gx.fn.setGridControlValue("GRIDSTATECOLLECTION__TITLE",row || gx.fn.currentGridRowImpl(14),gx.O.GXV10H,0)},c2v:function(){if(this.val()!==undefined)gx.O.GXV10H=this.val()},val:function(row){return gx.fn.getGridControlValue("GRIDSTATECOLLECTION__TITLE",row || gx.fn.currentGridRowImpl(14))},nac:gx.falseFn};
   GXValidFnc[18]={lvl:2,type:"vchar",len:2097152,dec:0,sign:false,ro:0,isacc:0,grid:14,gxgrid:this.GridgridstatecollectionsContainer,fnc:null,isvalid:null,rgrid:[],fld:"GRIDSTATECOLLECTION__GRIDSTATEXML",gxz:"ZV26GXV10I",gxold:"OV26GXV10I",gxvar:"GXV10I",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.GXV10I=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV26GXV10I=Value},v2c:function(row){gx.fn.setGridControlValue("GRIDSTATECOLLECTION__GRIDSTATEXML",row || gx.fn.currentGridRowImpl(14),gx.O.GXV10I,0)},c2v:function(){if(this.val()!==undefined)gx.O.GXV10I=this.val()},val:function(row){return gx.fn.getGridControlValue("GRIDSTATECOLLECTION__GRIDSTATEXML",row || gx.fn.currentGridRowImpl(14))},nac:gx.falseFn};
   GXValidFnc[19]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:14,gxgrid:this.GridgridstatecollectionsContainer,fnc:null,isvalid:null,rgrid:[],fld:"vUDELETE",gxz:"ZV6UDelete",gxold:"OV6UDelete",gxvar:"AV6UDelete",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV6UDelete=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV6UDelete=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vUDELETE",row || gx.fn.currentGridRowImpl(14),gx.O.AV6UDelete,gx.O.AV29Udelete_GXI)},c2v:function(){gx.O.AV29Udelete_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV6UDelete=this.val()},val:function(row){return gx.fn.getGridControlValue("vUDELETE",row || gx.fn.currentGridRowImpl(14))},val_GXI:function(row){return gx.fn.getGridControlValue("vUDELETE_GXI",row || gx.fn.currentGridRowImpl(14))}, gxvar_GXI:'AV29Udelete_GXI',nac:gx.falseFn};
   GXValidFnc[25]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[33]={fld:"DUMMYTABLETOIMPORTBOOTSTRAPCSS",grid:0};
   GXValidFnc[38]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[41]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCOLLECTIONISEMPTY",gxz:"ZV18CollectionIsEmpty",gxold:"OV18CollectionIsEmpty",gxvar:"AV18CollectionIsEmpty",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV18CollectionIsEmpty=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV18CollectionIsEmpty=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vCOLLECTIONISEMPTY",gx.O.AV18CollectionIsEmpty,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV18CollectionIsEmpty=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vCOLLECTIONISEMPTY")},nac:gx.falseFn,values:['true','false']};
   this.ZV16MoveUp = "" ;
   this.OV16MoveUp = "" ;
   this.ZV15MoveDown = "" ;
   this.OV15MoveDown = "" ;
   this.ZV25GXV10H = "" ;
   this.OV25GXV10H = "" ;
   this.ZV26GXV10I = "" ;
   this.OV26GXV10I = "" ;
   this.ZV6UDelete = "" ;
   this.OV6UDelete = "" ;
   this.AV18CollectionIsEmpty = false ;
   this.ZV18CollectionIsEmpty = false ;
   this.OV18CollectionIsEmpty = false ;
   this.AV20GridGridStateCollectionsCurrentPage = 0 ;
   this.AV18CollectionIsEmpty = false ;
   this.AV17UserKey = "" ;
   this.AV16MoveUp = "" ;
   this.AV15MoveDown = "" ;
   this.GXV10H = "" ;
   this.GXV10I = "" ;
   this.AV6UDelete = "" ;
   this.AV7GridStateCollection = [ ] ;
   this.Events = {"e11072_client": ["GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR.CHANGEPAGE", true] ,"e12072_client": ["ENTER", true] ,"e16072_client": ["VMOVEUP.CLICK", true] ,"e17072_client": ["VMOVEDOWN.CLICK", true] ,"e18072_client": ["VUDELETE.CLICK", true] ,"e19071_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage',nv:0},{av:'GRIDGRIDSTATECOLLECTIONS_nEOF',nv:0},{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'subGridgridstatecollections_Rows',nv:0}],[{av:'AV16MoveUp',fld:'vMOVEUP',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vMOVEUP","Tooltiptext")',ctrl:'vMOVEUP',prop:'Tooltiptext'},{av:'AV15MoveDown',fld:'vMOVEDOWN',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vMOVEDOWN","Tooltiptext")',ctrl:'vMOVEDOWN',prop:'Tooltiptext'},{av:'AV6UDelete',fld:'vUDELETE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vUDELETE","Tooltiptext")',ctrl:'vUDELETE',prop:'Tooltiptext'},{av:'AV20GridGridStateCollectionsCurrentPage',fld:'vGRIDGRIDSTATECOLLECTIONSCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV21GridGridStateCollectionsPageCount',fld:'vGRIDGRIDSTATECOLLECTIONSPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]];
   this.EvtParms["GRIDGRIDSTATECOLLECTIONS.LOAD"] = [[],[{av:'AV16MoveUp',fld:'vMOVEUP',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vMOVEUP","Tooltiptext")',ctrl:'vMOVEUP',prop:'Tooltiptext'},{av:'AV15MoveDown',fld:'vMOVEDOWN',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vMOVEDOWN","Tooltiptext")',ctrl:'vMOVEDOWN',prop:'Tooltiptext'},{av:'AV6UDelete',fld:'vUDELETE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vUDELETE","Tooltiptext")',ctrl:'vUDELETE',prop:'Tooltiptext'}]];
   this.EvtParms["GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage',nv:0},{av:'GRIDGRIDSTATECOLLECTIONS_nEOF',nv:0},{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'subGridgridstatecollections_Rows',nv:0},{av:'this.GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR',prop:'SelectedPage'}],[]];
   this.EvtParms["ENTER"] = [[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'AV18CollectionIsEmpty',fld:'vCOLLECTIONISEMPTY',pic:'',nv:false},{av:'AV17UserKey',fld:'vUSERKEY',pic:'',hsh:true,nv:''},{av:'GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage',nv:0},{av:'GRIDGRIDSTATECOLLECTIONS_nEOF',nv:0},{av:'subGridgridstatecollections_Rows',nv:0}],[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null}]];
   this.EvtParms["VMOVEUP.CLICK"] = [[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage',nv:0},{av:'GRIDGRIDSTATECOLLECTIONS_nEOF',nv:0},{av:'subGridgridstatecollections_Rows',nv:0}],[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null}]];
   this.EvtParms["VMOVEDOWN.CLICK"] = [[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage',nv:0},{av:'GRIDGRIDSTATECOLLECTIONS_nEOF',nv:0},{av:'subGridgridstatecollections_Rows',nv:0}],[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null}]];
   this.EvtParms["VUDELETE.CLICK"] = [[{av:'GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage',nv:0},{av:'GRIDGRIDSTATECOLLECTIONS_nEOF',nv:0},{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'subGridgridstatecollections_Rows',nv:0}],[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'AV18CollectionIsEmpty',fld:'vCOLLECTIONISEMPTY',pic:'',nv:false}]];
   this.EnterCtrl = ["BTNENTER"];
   this.setVCMap("AV7GridStateCollection", "vGRIDSTATECOLLECTION", 0, "CollWWPBaseObjects\GridStateCollection.Item");
   this.setVCMap("AV17UserKey", "vUSERKEY", 0, "svchar");
   this.addGridBCProperty("Gridstatecollection", ["Title"], this.GXValidFnc[17], "AV7GridStateCollection");
   this.addGridBCProperty("Gridstatecollection", ["GridStateXML"], this.GXValidFnc[18], "AV7GridStateCollection");
   this.InitStandaloneVars( );
});
gx.createParentObj(wwpbaseobjects.managefilters);
