/**@preserve  GeneXus C# 10_3_14-114418 on 4/28/2020 23:54:18.47
*/
gx.evt.autoSkip = false;
gx.define('wwpbaseobjects.workwithpluspromptmasterpage', false, function () {
   this.ServerClass =  "wwpbaseobjects.workwithpluspromptmasterpage" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.IsMasterPage=true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
   };
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("TABLEINVISIBLE_MPAGE","Visible", ((1==0)) );
   };
   this.e13nk2_client=function()
   {
      this.executeServerEvent("ENTER_MPAGE", true, null, false, false);
   };
   this.e14nk2_client=function()
   {
      this.executeServerEvent("CANCEL_MPAGE", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,11,16];
   this.GXLastCtrlId =16;
   this.DVPANEL_UNNAMEDTABLE2_MPAGEContainer = gx.uc.getNew(this, 14, 0, "BootstrapPanel", "DVPANEL_UNNAMEDTABLE2_MPAGEContainer", "Dvpanel_unnamedtable2");
   var DVPANEL_UNNAMEDTABLE2_MPAGEContainer = this.DVPANEL_UNNAMEDTABLE2_MPAGEContainer;
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("Title", "Title", "", "str");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("Collapsible", "Collapsible", true, "bool");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setProp("Class", "Class", "", "char");
   DVPANEL_UNNAMEDTABLE2_MPAGEContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_UNNAMEDTABLE2_MPAGEContainer);
   GXValidFnc[2]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[5]={fld:"UTCONTENTHOLDER",grid:0};
   GXValidFnc[11]={fld:"TABLEINVISIBLE",grid:0};
   GXValidFnc[16]={fld:"UNNAMEDTABLE2",grid:0};
   this.Events = {"e13nk2_client": ["ENTER_MPAGE", true] ,"e14nk2_client": ["CANCEL_MPAGE", true]};
   this.EvtParms["REFRESH_MPAGE"] = [[],[]];
   this.InitStandaloneVars( );
});
gx.createMasterPage(wwpbaseobjects.workwithpluspromptmasterpage);
