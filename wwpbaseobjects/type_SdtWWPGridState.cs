/*
               File: wwpbaseobjects.type_SdtWWPGridState
        Description: WWPBaseObjects\WWPGridState
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/17/2020 15:47:40.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "WWPGridState" )]
   [XmlType(TypeName =  "WWPGridState" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( wwpbaseobjects.SdtWWPGridState_FilterValue ))]
   [System.Xml.Serialization.XmlInclude( typeof( wwpbaseobjects.SdtWWPGridState_DynamicFilter ))]
   [Serializable]
   public class SdtWWPGridState : GxUserType
   {
      public SdtWWPGridState( )
      {
         /* Constructor for serialization */
      }

      public SdtWWPGridState( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtWWPGridState deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtWWPGridState)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtWWPGridState obj ;
         obj = this;
         obj.gxTpr_Currentpage = deserialized.gxTpr_Currentpage;
         obj.gxTpr_Orderedby = deserialized.gxTpr_Orderedby;
         obj.gxTpr_Ordereddsc = deserialized.gxTpr_Ordereddsc;
         obj.gxTpr_Hidingsearch = deserialized.gxTpr_Hidingsearch;
         obj.gxTpr_Filtervalues = deserialized.gxTpr_Filtervalues;
         obj.gxTpr_Dynamicfilters = deserialized.gxTpr_Dynamicfilters;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "CurrentPage") )
               {
                  gxTv_SdtWWPGridState_Currentpage = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OrderedBy") )
               {
                  gxTv_SdtWWPGridState_Orderedby = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OrderedDsc") )
               {
                  gxTv_SdtWWPGridState_Ordereddsc = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "HidingSearch") )
               {
                  gxTv_SdtWWPGridState_Hidingsearch = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FilterValues") )
               {
                  if ( gxTv_SdtWWPGridState_Filtervalues == null )
                  {
                     gxTv_SdtWWPGridState_Filtervalues = new GxObjectCollection( context, "WWPGridState.FilterValue", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPGridState_FilterValue", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtWWPGridState_Filtervalues.readxmlcollection(oReader, "FilterValues", "FilterValue");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DynamicFilters") )
               {
                  if ( gxTv_SdtWWPGridState_Dynamicfilters == null )
                  {
                     gxTv_SdtWWPGridState_Dynamicfilters = new GxObjectCollection( context, "WWPGridState.DynamicFilter", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPGridState_DynamicFilter", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtWWPGridState_Dynamicfilters.readxmlcollection(oReader, "DynamicFilters", "DynamicFilter");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPGridState";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("CurrentPage", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPGridState_Currentpage), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("OrderedBy", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPGridState_Orderedby), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("OrderedDsc", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPGridState_Ordereddsc)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("HidingSearch", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPGridState_Hidingsearch), 1, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( gxTv_SdtWWPGridState_Filtervalues != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtWWPGridState_Filtervalues.writexmlcollection(oWriter, "FilterValues", sNameSpace1, "FilterValue", sNameSpace1);
         }
         if ( gxTv_SdtWWPGridState_Dynamicfilters != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtWWPGridState_Dynamicfilters.writexmlcollection(oWriter, "DynamicFilters", sNameSpace1, "DynamicFilter", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("CurrentPage", gxTv_SdtWWPGridState_Currentpage, false);
         AddObjectProperty("OrderedBy", gxTv_SdtWWPGridState_Orderedby, false);
         AddObjectProperty("OrderedDsc", gxTv_SdtWWPGridState_Ordereddsc, false);
         AddObjectProperty("HidingSearch", gxTv_SdtWWPGridState_Hidingsearch, false);
         if ( gxTv_SdtWWPGridState_Filtervalues != null )
         {
            AddObjectProperty("FilterValues", gxTv_SdtWWPGridState_Filtervalues, false);
         }
         if ( gxTv_SdtWWPGridState_Dynamicfilters != null )
         {
            AddObjectProperty("DynamicFilters", gxTv_SdtWWPGridState_Dynamicfilters, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "CurrentPage" )]
      [  XmlElement( ElementName = "CurrentPage"   )]
      public short gxTpr_Currentpage
      {
         get {
            return gxTv_SdtWWPGridState_Currentpage ;
         }

         set {
            gxTv_SdtWWPGridState_Currentpage = (short)(value);
         }

      }

      [  SoapElement( ElementName = "OrderedBy" )]
      [  XmlElement( ElementName = "OrderedBy"   )]
      public short gxTpr_Orderedby
      {
         get {
            return gxTv_SdtWWPGridState_Orderedby ;
         }

         set {
            gxTv_SdtWWPGridState_Orderedby = (short)(value);
         }

      }

      [  SoapElement( ElementName = "OrderedDsc" )]
      [  XmlElement( ElementName = "OrderedDsc"   )]
      public bool gxTpr_Ordereddsc
      {
         get {
            return gxTv_SdtWWPGridState_Ordereddsc ;
         }

         set {
            gxTv_SdtWWPGridState_Ordereddsc = value;
         }

      }

      [  SoapElement( ElementName = "HidingSearch" )]
      [  XmlElement( ElementName = "HidingSearch"   )]
      public short gxTpr_Hidingsearch
      {
         get {
            return gxTv_SdtWWPGridState_Hidingsearch ;
         }

         set {
            gxTv_SdtWWPGridState_Hidingsearch = (short)(value);
         }

      }

      public class gxTv_SdtWWPGridState_Filtervalues_wwpbaseobjects_SdtWWPGridState_FilterValue_80compatibility:wwpbaseobjects.SdtWWPGridState_FilterValue {}
      [  SoapElement( ElementName = "FilterValues" )]
      [  XmlArray( ElementName = "FilterValues"  )]
      [  XmlArrayItemAttribute( Type= typeof( wwpbaseobjects.SdtWWPGridState_FilterValue ), ElementName= "FilterValue"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtWWPGridState_Filtervalues_wwpbaseobjects_SdtWWPGridState_FilterValue_80compatibility ), ElementName= "WWPGridState.FilterValue"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Filtervalues_GxObjectCollection
      {
         get {
            if ( gxTv_SdtWWPGridState_Filtervalues == null )
            {
               gxTv_SdtWWPGridState_Filtervalues = new GxObjectCollection( context, "WWPGridState.FilterValue", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPGridState_FilterValue", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtWWPGridState_Filtervalues ;
         }

         set {
            if ( gxTv_SdtWWPGridState_Filtervalues == null )
            {
               gxTv_SdtWWPGridState_Filtervalues = new GxObjectCollection( context, "WWPGridState.FilterValue", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPGridState_FilterValue", "GeneXus.Programs");
            }
            gxTv_SdtWWPGridState_Filtervalues = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Filtervalues
      {
         get {
            if ( gxTv_SdtWWPGridState_Filtervalues == null )
            {
               gxTv_SdtWWPGridState_Filtervalues = new GxObjectCollection( context, "WWPGridState.FilterValue", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPGridState_FilterValue", "GeneXus.Programs");
            }
            return gxTv_SdtWWPGridState_Filtervalues ;
         }

         set {
            gxTv_SdtWWPGridState_Filtervalues = value;
         }

      }

      public void gxTv_SdtWWPGridState_Filtervalues_SetNull( )
      {
         gxTv_SdtWWPGridState_Filtervalues = null;
         return  ;
      }

      public bool gxTv_SdtWWPGridState_Filtervalues_IsNull( )
      {
         if ( gxTv_SdtWWPGridState_Filtervalues == null )
         {
            return true ;
         }
         return false ;
      }

      public class gxTv_SdtWWPGridState_Dynamicfilters_wwpbaseobjects_SdtWWPGridState_DynamicFilter_80compatibility:wwpbaseobjects.SdtWWPGridState_DynamicFilter {}
      [  SoapElement( ElementName = "DynamicFilters" )]
      [  XmlArray( ElementName = "DynamicFilters"  )]
      [  XmlArrayItemAttribute( Type= typeof( wwpbaseobjects.SdtWWPGridState_DynamicFilter ), ElementName= "DynamicFilter"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtWWPGridState_Dynamicfilters_wwpbaseobjects_SdtWWPGridState_DynamicFilter_80compatibility ), ElementName= "WWPGridState.DynamicFilter"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Dynamicfilters_GxObjectCollection
      {
         get {
            if ( gxTv_SdtWWPGridState_Dynamicfilters == null )
            {
               gxTv_SdtWWPGridState_Dynamicfilters = new GxObjectCollection( context, "WWPGridState.DynamicFilter", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPGridState_DynamicFilter", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtWWPGridState_Dynamicfilters ;
         }

         set {
            if ( gxTv_SdtWWPGridState_Dynamicfilters == null )
            {
               gxTv_SdtWWPGridState_Dynamicfilters = new GxObjectCollection( context, "WWPGridState.DynamicFilter", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPGridState_DynamicFilter", "GeneXus.Programs");
            }
            gxTv_SdtWWPGridState_Dynamicfilters = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Dynamicfilters
      {
         get {
            if ( gxTv_SdtWWPGridState_Dynamicfilters == null )
            {
               gxTv_SdtWWPGridState_Dynamicfilters = new GxObjectCollection( context, "WWPGridState.DynamicFilter", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPGridState_DynamicFilter", "GeneXus.Programs");
            }
            return gxTv_SdtWWPGridState_Dynamicfilters ;
         }

         set {
            gxTv_SdtWWPGridState_Dynamicfilters = value;
         }

      }

      public void gxTv_SdtWWPGridState_Dynamicfilters_SetNull( )
      {
         gxTv_SdtWWPGridState_Dynamicfilters = null;
         return  ;
      }

      public bool gxTv_SdtWWPGridState_Dynamicfilters_IsNull( )
      {
         if ( gxTv_SdtWWPGridState_Dynamicfilters == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtWWPGridState_Currentpage ;
      protected short gxTv_SdtWWPGridState_Orderedby ;
      protected short gxTv_SdtWWPGridState_Hidingsearch ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected bool gxTv_SdtWWPGridState_Ordereddsc ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPGridState_FilterValue ))]
      protected IGxCollection gxTv_SdtWWPGridState_Filtervalues=null ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPGridState_DynamicFilter ))]
      protected IGxCollection gxTv_SdtWWPGridState_Dynamicfilters=null ;
   }

   [DataContract(Name = @"WWPBaseObjects\WWPGridState", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtWWPGridState_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtWWPGridState>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtWWPGridState_RESTInterface( ) : base()
      {
      }

      public SdtWWPGridState_RESTInterface( wwpbaseobjects.SdtWWPGridState psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "CurrentPage" , Order = 0 )]
      public Nullable<short> gxTpr_Currentpage
      {
         get {
            return sdt.gxTpr_Currentpage ;
         }

         set {
            sdt.gxTpr_Currentpage = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "OrderedBy" , Order = 1 )]
      public Nullable<short> gxTpr_Orderedby
      {
         get {
            return sdt.gxTpr_Orderedby ;
         }

         set {
            sdt.gxTpr_Orderedby = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "OrderedDsc" , Order = 2 )]
      public bool gxTpr_Ordereddsc
      {
         get {
            return sdt.gxTpr_Ordereddsc ;
         }

         set {
            sdt.gxTpr_Ordereddsc = value;
         }

      }

      [DataMember( Name = "HidingSearch" , Order = 3 )]
      public Nullable<short> gxTpr_Hidingsearch
      {
         get {
            return sdt.gxTpr_Hidingsearch ;
         }

         set {
            sdt.gxTpr_Hidingsearch = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "FilterValues" , Order = 4 )]
      public GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtWWPGridState_FilterValue_RESTInterface> gxTpr_Filtervalues
      {
         get {
            return new GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtWWPGridState_FilterValue_RESTInterface>(sdt.gxTpr_Filtervalues) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Filtervalues);
         }

      }

      [DataMember( Name = "DynamicFilters" , Order = 5 )]
      public GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtWWPGridState_DynamicFilter_RESTInterface> gxTpr_Dynamicfilters
      {
         get {
            return new GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtWWPGridState_DynamicFilter_RESTInterface>(sdt.gxTpr_Dynamicfilters) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Dynamicfilters);
         }

      }

      public wwpbaseobjects.SdtWWPGridState sdt
      {
         get {
            return (wwpbaseobjects.SdtWWPGridState)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtWWPGridState() ;
         }
      }

   }

}
