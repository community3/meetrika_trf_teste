/*
               File: wwpbaseobjects.type_SdtWWPColumnsSelector
        Description: WWPBaseObjects\WWPColumnsSelector
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/4/2020 0:18:43.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "WWPColumnsSelector" )]
   [XmlType(TypeName =  "WWPColumnsSelector" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( wwpbaseobjects.SdtWWPColumnsSelector_VisibleColumn ))]
   [System.Xml.Serialization.XmlInclude( typeof( wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn ))]
   [Serializable]
   public class SdtWWPColumnsSelector : GxUserType
   {
      public SdtWWPColumnsSelector( )
      {
         /* Constructor for serialization */
      }

      public SdtWWPColumnsSelector( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtWWPColumnsSelector deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtWWPColumnsSelector)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtWWPColumnsSelector obj ;
         obj = this;
         obj.gxTpr_Visiblecolumns = deserialized.gxTpr_Visiblecolumns;
         obj.gxTpr_Invisiblecolumns = deserialized.gxTpr_Invisiblecolumns;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "VisibleColumns") )
               {
                  if ( gxTv_SdtWWPColumnsSelector_Visiblecolumns == null )
                  {
                     gxTv_SdtWWPColumnsSelector_Visiblecolumns = new GxObjectCollection( context, "WWPColumnsSelector.VisibleColumn", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPColumnsSelector_VisibleColumn", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtWWPColumnsSelector_Visiblecolumns.readxmlcollection(oReader, "VisibleColumns", "VisibleColumn");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "InvisibleColumns") )
               {
                  if ( gxTv_SdtWWPColumnsSelector_Invisiblecolumns == null )
                  {
                     gxTv_SdtWWPColumnsSelector_Invisiblecolumns = new GxObjectCollection( context, "WWPColumnsSelector.InvisibleColumn", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtWWPColumnsSelector_Invisiblecolumns.readxmlcollection(oReader, "InvisibleColumns", "InvisibleColumn");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPColumnsSelector";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         if ( gxTv_SdtWWPColumnsSelector_Visiblecolumns != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtWWPColumnsSelector_Visiblecolumns.writexmlcollection(oWriter, "VisibleColumns", sNameSpace1, "VisibleColumn", sNameSpace1);
         }
         if ( gxTv_SdtWWPColumnsSelector_Invisiblecolumns != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtWWPColumnsSelector_Invisiblecolumns.writexmlcollection(oWriter, "InvisibleColumns", sNameSpace1, "InvisibleColumn", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         if ( gxTv_SdtWWPColumnsSelector_Visiblecolumns != null )
         {
            AddObjectProperty("VisibleColumns", gxTv_SdtWWPColumnsSelector_Visiblecolumns, false);
         }
         if ( gxTv_SdtWWPColumnsSelector_Invisiblecolumns != null )
         {
            AddObjectProperty("InvisibleColumns", gxTv_SdtWWPColumnsSelector_Invisiblecolumns, false);
         }
         return  ;
      }

      public class gxTv_SdtWWPColumnsSelector_Visiblecolumns_wwpbaseobjects_SdtWWPColumnsSelector_VisibleColumn_80compatibility:wwpbaseobjects.SdtWWPColumnsSelector_VisibleColumn {}
      [  SoapElement( ElementName = "VisibleColumns" )]
      [  XmlArray( ElementName = "VisibleColumns"  )]
      [  XmlArrayItemAttribute( Type= typeof( wwpbaseobjects.SdtWWPColumnsSelector_VisibleColumn ), ElementName= "VisibleColumn"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtWWPColumnsSelector_Visiblecolumns_wwpbaseobjects_SdtWWPColumnsSelector_VisibleColumn_80compatibility ), ElementName= "WWPColumnsSelector.VisibleColumn"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Visiblecolumns_GxObjectCollection
      {
         get {
            if ( gxTv_SdtWWPColumnsSelector_Visiblecolumns == null )
            {
               gxTv_SdtWWPColumnsSelector_Visiblecolumns = new GxObjectCollection( context, "WWPColumnsSelector.VisibleColumn", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPColumnsSelector_VisibleColumn", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtWWPColumnsSelector_Visiblecolumns ;
         }

         set {
            if ( gxTv_SdtWWPColumnsSelector_Visiblecolumns == null )
            {
               gxTv_SdtWWPColumnsSelector_Visiblecolumns = new GxObjectCollection( context, "WWPColumnsSelector.VisibleColumn", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPColumnsSelector_VisibleColumn", "GeneXus.Programs");
            }
            gxTv_SdtWWPColumnsSelector_Visiblecolumns = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Visiblecolumns
      {
         get {
            if ( gxTv_SdtWWPColumnsSelector_Visiblecolumns == null )
            {
               gxTv_SdtWWPColumnsSelector_Visiblecolumns = new GxObjectCollection( context, "WWPColumnsSelector.VisibleColumn", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPColumnsSelector_VisibleColumn", "GeneXus.Programs");
            }
            return gxTv_SdtWWPColumnsSelector_Visiblecolumns ;
         }

         set {
            gxTv_SdtWWPColumnsSelector_Visiblecolumns = value;
         }

      }

      public void gxTv_SdtWWPColumnsSelector_Visiblecolumns_SetNull( )
      {
         gxTv_SdtWWPColumnsSelector_Visiblecolumns = null;
         return  ;
      }

      public bool gxTv_SdtWWPColumnsSelector_Visiblecolumns_IsNull( )
      {
         if ( gxTv_SdtWWPColumnsSelector_Visiblecolumns == null )
         {
            return true ;
         }
         return false ;
      }

      public class gxTv_SdtWWPColumnsSelector_Invisiblecolumns_wwpbaseobjects_SdtWWPColumnsSelector_InvisibleColumn_80compatibility:wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn {}
      [  SoapElement( ElementName = "InvisibleColumns" )]
      [  XmlArray( ElementName = "InvisibleColumns"  )]
      [  XmlArrayItemAttribute( Type= typeof( wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn ), ElementName= "InvisibleColumn"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtWWPColumnsSelector_Invisiblecolumns_wwpbaseobjects_SdtWWPColumnsSelector_InvisibleColumn_80compatibility ), ElementName= "WWPColumnsSelector.InvisibleColumn"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Invisiblecolumns_GxObjectCollection
      {
         get {
            if ( gxTv_SdtWWPColumnsSelector_Invisiblecolumns == null )
            {
               gxTv_SdtWWPColumnsSelector_Invisiblecolumns = new GxObjectCollection( context, "WWPColumnsSelector.InvisibleColumn", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtWWPColumnsSelector_Invisiblecolumns ;
         }

         set {
            if ( gxTv_SdtWWPColumnsSelector_Invisiblecolumns == null )
            {
               gxTv_SdtWWPColumnsSelector_Invisiblecolumns = new GxObjectCollection( context, "WWPColumnsSelector.InvisibleColumn", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn", "GeneXus.Programs");
            }
            gxTv_SdtWWPColumnsSelector_Invisiblecolumns = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Invisiblecolumns
      {
         get {
            if ( gxTv_SdtWWPColumnsSelector_Invisiblecolumns == null )
            {
               gxTv_SdtWWPColumnsSelector_Invisiblecolumns = new GxObjectCollection( context, "WWPColumnsSelector.InvisibleColumn", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn", "GeneXus.Programs");
            }
            return gxTv_SdtWWPColumnsSelector_Invisiblecolumns ;
         }

         set {
            gxTv_SdtWWPColumnsSelector_Invisiblecolumns = value;
         }

      }

      public void gxTv_SdtWWPColumnsSelector_Invisiblecolumns_SetNull( )
      {
         gxTv_SdtWWPColumnsSelector_Invisiblecolumns = null;
         return  ;
      }

      public bool gxTv_SdtWWPColumnsSelector_Invisiblecolumns_IsNull( )
      {
         if ( gxTv_SdtWWPColumnsSelector_Invisiblecolumns == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPColumnsSelector_VisibleColumn ))]
      protected IGxCollection gxTv_SdtWWPColumnsSelector_Visiblecolumns=null ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn ))]
      protected IGxCollection gxTv_SdtWWPColumnsSelector_Invisiblecolumns=null ;
   }

   [DataContract(Name = @"WWPBaseObjects\WWPColumnsSelector", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtWWPColumnsSelector_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtWWPColumnsSelector>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtWWPColumnsSelector_RESTInterface( ) : base()
      {
      }

      public SdtWWPColumnsSelector_RESTInterface( wwpbaseobjects.SdtWWPColumnsSelector psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "VisibleColumns" , Order = 0 )]
      public GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtWWPColumnsSelector_VisibleColumn_RESTInterface> gxTpr_Visiblecolumns
      {
         get {
            return new GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtWWPColumnsSelector_VisibleColumn_RESTInterface>(sdt.gxTpr_Visiblecolumns) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Visiblecolumns);
         }

      }

      [DataMember( Name = "InvisibleColumns" , Order = 1 )]
      public GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn_RESTInterface> gxTpr_Invisiblecolumns
      {
         get {
            return new GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtWWPColumnsSelector_InvisibleColumn_RESTInterface>(sdt.gxTpr_Invisiblecolumns) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Invisiblecolumns);
         }

      }

      public wwpbaseobjects.SdtWWPColumnsSelector sdt
      {
         get {
            return (wwpbaseobjects.SdtWWPColumnsSelector)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtWWPColumnsSelector() ;
         }
      }

   }

}
