/*
               File: GetPromptContratoOcorrenciaFilterData
        Description: Get Prompt Contrato Ocorrencia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 2:22:45.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratoocorrenciafilterdata : GXProcedure
   {
      public getpromptcontratoocorrenciafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratoocorrenciafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratoocorrenciafilterdata objgetpromptcontratoocorrenciafilterdata;
         objgetpromptcontratoocorrenciafilterdata = new getpromptcontratoocorrenciafilterdata();
         objgetpromptcontratoocorrenciafilterdata.AV18DDOName = aP0_DDOName;
         objgetpromptcontratoocorrenciafilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetpromptcontratoocorrenciafilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratoocorrenciafilterdata.AV22OptionsJson = "" ;
         objgetpromptcontratoocorrenciafilterdata.AV25OptionsDescJson = "" ;
         objgetpromptcontratoocorrenciafilterdata.AV27OptionIndexesJson = "" ;
         objgetpromptcontratoocorrenciafilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratoocorrenciafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratoocorrenciafilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratoocorrenciafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOOCORRENCIA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIA_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("PromptContratoOcorrenciaGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoOcorrenciaGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("PromptContratoOcorrenciaGridState"), "");
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_DATA") == 0 )
            {
               AV12TFContratoOcorrencia_Data = context.localUtil.CToD( AV32GridStateFilterValue.gxTpr_Value, 2);
               AV13TFContratoOcorrencia_Data_To = context.localUtil.CToD( AV32GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_DESCRICAO") == 0 )
            {
               AV14TFContratoOcorrencia_Descricao = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_DESCRICAO_SEL") == 0 )
            {
               AV15TFContratoOcorrencia_Descricao_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 )
            {
               AV36ContratoOcorrencia_Data1 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Value, 2);
               AV37ContratoOcorrencia_Data_To1 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV38ContratoOcorrencia_Descricao1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV39DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV40DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 )
               {
                  AV42ContratoOcorrencia_Data2 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Value, 2);
                  AV43ContratoOcorrencia_Data_To2 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 )
               {
                  AV41DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV44ContratoOcorrencia_Descricao2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV16SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV36ContratoOcorrencia_Data1 ,
                                              AV37ContratoOcorrencia_Data_To1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV38ContratoOcorrencia_Descricao1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV42ContratoOcorrencia_Data2 ,
                                              AV43ContratoOcorrencia_Data_To2 ,
                                              AV41DynamicFiltersOperator2 ,
                                              AV44ContratoOcorrencia_Descricao2 ,
                                              AV11TFContrato_Numero_Sel ,
                                              AV10TFContrato_Numero ,
                                              AV12TFContratoOcorrencia_Data ,
                                              AV13TFContratoOcorrencia_Data_To ,
                                              AV15TFContratoOcorrencia_Descricao_Sel ,
                                              AV14TFContratoOcorrencia_Descricao ,
                                              A295ContratoOcorrencia_Data ,
                                              A296ContratoOcorrencia_Descricao ,
                                              A77Contrato_Numero },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV38ContratoOcorrencia_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV38ContratoOcorrencia_Descricao1), "%", "");
         lV38ContratoOcorrencia_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV38ContratoOcorrencia_Descricao1), "%", "");
         lV44ContratoOcorrencia_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV44ContratoOcorrencia_Descricao2), "%", "");
         lV44ContratoOcorrencia_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV44ContratoOcorrencia_Descricao2), "%", "");
         lV10TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFContrato_Numero), 20, "%");
         lV14TFContratoOcorrencia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFContratoOcorrencia_Descricao), "%", "");
         /* Using cursor P00K52 */
         pr_default.execute(0, new Object[] {AV36ContratoOcorrencia_Data1, AV37ContratoOcorrencia_Data_To1, lV38ContratoOcorrencia_Descricao1, lV38ContratoOcorrencia_Descricao1, AV42ContratoOcorrencia_Data2, AV43ContratoOcorrencia_Data_To2, lV44ContratoOcorrencia_Descricao2, lV44ContratoOcorrencia_Descricao2, lV10TFContrato_Numero, AV11TFContrato_Numero_Sel, AV12TFContratoOcorrencia_Data, AV13TFContratoOcorrencia_Data_To, lV14TFContratoOcorrencia_Descricao, AV15TFContratoOcorrencia_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKK52 = false;
            A74Contrato_Codigo = P00K52_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00K52_A77Contrato_Numero[0];
            A296ContratoOcorrencia_Descricao = P00K52_A296ContratoOcorrencia_Descricao[0];
            A295ContratoOcorrencia_Data = P00K52_A295ContratoOcorrencia_Data[0];
            A294ContratoOcorrencia_Codigo = P00K52_A294ContratoOcorrencia_Codigo[0];
            A77Contrato_Numero = P00K52_A77Contrato_Numero[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00K52_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKK52 = false;
               A74Contrato_Codigo = P00K52_A74Contrato_Codigo[0];
               A294ContratoOcorrencia_Codigo = P00K52_A294ContratoOcorrencia_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKK52 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV20Option = A77Contrato_Numero;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK52 )
            {
               BRKK52 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOOCORRENCIA_DESCRICAOOPTIONS' Routine */
         AV14TFContratoOcorrencia_Descricao = AV16SearchTxt;
         AV15TFContratoOcorrencia_Descricao_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV36ContratoOcorrencia_Data1 ,
                                              AV37ContratoOcorrencia_Data_To1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV38ContratoOcorrencia_Descricao1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV42ContratoOcorrencia_Data2 ,
                                              AV43ContratoOcorrencia_Data_To2 ,
                                              AV41DynamicFiltersOperator2 ,
                                              AV44ContratoOcorrencia_Descricao2 ,
                                              AV11TFContrato_Numero_Sel ,
                                              AV10TFContrato_Numero ,
                                              AV12TFContratoOcorrencia_Data ,
                                              AV13TFContratoOcorrencia_Data_To ,
                                              AV15TFContratoOcorrencia_Descricao_Sel ,
                                              AV14TFContratoOcorrencia_Descricao ,
                                              A295ContratoOcorrencia_Data ,
                                              A296ContratoOcorrencia_Descricao ,
                                              A77Contrato_Numero },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV38ContratoOcorrencia_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV38ContratoOcorrencia_Descricao1), "%", "");
         lV38ContratoOcorrencia_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV38ContratoOcorrencia_Descricao1), "%", "");
         lV44ContratoOcorrencia_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV44ContratoOcorrencia_Descricao2), "%", "");
         lV44ContratoOcorrencia_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV44ContratoOcorrencia_Descricao2), "%", "");
         lV10TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFContrato_Numero), 20, "%");
         lV14TFContratoOcorrencia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFContratoOcorrencia_Descricao), "%", "");
         /* Using cursor P00K53 */
         pr_default.execute(1, new Object[] {AV36ContratoOcorrencia_Data1, AV37ContratoOcorrencia_Data_To1, lV38ContratoOcorrencia_Descricao1, lV38ContratoOcorrencia_Descricao1, AV42ContratoOcorrencia_Data2, AV43ContratoOcorrencia_Data_To2, lV44ContratoOcorrencia_Descricao2, lV44ContratoOcorrencia_Descricao2, lV10TFContrato_Numero, AV11TFContrato_Numero_Sel, AV12TFContratoOcorrencia_Data, AV13TFContratoOcorrencia_Data_To, lV14TFContratoOcorrencia_Descricao, AV15TFContratoOcorrencia_Descricao_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKK54 = false;
            A74Contrato_Codigo = P00K53_A74Contrato_Codigo[0];
            A296ContratoOcorrencia_Descricao = P00K53_A296ContratoOcorrencia_Descricao[0];
            A77Contrato_Numero = P00K53_A77Contrato_Numero[0];
            A295ContratoOcorrencia_Data = P00K53_A295ContratoOcorrencia_Data[0];
            A294ContratoOcorrencia_Codigo = P00K53_A294ContratoOcorrencia_Codigo[0];
            A77Contrato_Numero = P00K53_A77Contrato_Numero[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00K53_A296ContratoOcorrencia_Descricao[0], A296ContratoOcorrencia_Descricao) == 0 ) )
            {
               BRKK54 = false;
               A294ContratoOcorrencia_Codigo = P00K53_A294ContratoOcorrencia_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKK54 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A296ContratoOcorrencia_Descricao)) )
            {
               AV20Option = A296ContratoOcorrencia_Descricao;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK54 )
            {
               BRKK54 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContratoOcorrencia_Data = DateTime.MinValue;
         AV13TFContratoOcorrencia_Data_To = DateTime.MinValue;
         AV14TFContratoOcorrencia_Descricao = "";
         AV15TFContratoOcorrencia_Descricao_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV36ContratoOcorrencia_Data1 = DateTime.MinValue;
         AV37ContratoOcorrencia_Data_To1 = DateTime.MinValue;
         AV38ContratoOcorrencia_Descricao1 = "";
         AV40DynamicFiltersSelector2 = "";
         AV42ContratoOcorrencia_Data2 = DateTime.MinValue;
         AV43ContratoOcorrencia_Data_To2 = DateTime.MinValue;
         AV44ContratoOcorrencia_Descricao2 = "";
         scmdbuf = "";
         lV38ContratoOcorrencia_Descricao1 = "";
         lV44ContratoOcorrencia_Descricao2 = "";
         lV10TFContrato_Numero = "";
         lV14TFContratoOcorrencia_Descricao = "";
         A295ContratoOcorrencia_Data = DateTime.MinValue;
         A296ContratoOcorrencia_Descricao = "";
         A77Contrato_Numero = "";
         P00K52_A74Contrato_Codigo = new int[1] ;
         P00K52_A77Contrato_Numero = new String[] {""} ;
         P00K52_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         P00K52_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00K52_A294ContratoOcorrencia_Codigo = new int[1] ;
         AV20Option = "";
         P00K53_A74Contrato_Codigo = new int[1] ;
         P00K53_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         P00K53_A77Contrato_Numero = new String[] {""} ;
         P00K53_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00K53_A294ContratoOcorrencia_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratoocorrenciafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00K52_A74Contrato_Codigo, P00K52_A77Contrato_Numero, P00K52_A296ContratoOcorrencia_Descricao, P00K52_A295ContratoOcorrencia_Data, P00K52_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               P00K53_A74Contrato_Codigo, P00K53_A296ContratoOcorrencia_Descricao, P00K53_A77Contrato_Numero, P00K53_A295ContratoOcorrencia_Data, P00K53_A294ContratoOcorrencia_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV35DynamicFiltersOperator1 ;
      private short AV41DynamicFiltersOperator2 ;
      private int AV47GXV1 ;
      private int A74Contrato_Codigo ;
      private int A294ContratoOcorrencia_Codigo ;
      private long AV28count ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String scmdbuf ;
      private String lV10TFContrato_Numero ;
      private String A77Contrato_Numero ;
      private DateTime AV12TFContratoOcorrencia_Data ;
      private DateTime AV13TFContratoOcorrencia_Data_To ;
      private DateTime AV36ContratoOcorrencia_Data1 ;
      private DateTime AV37ContratoOcorrencia_Data_To1 ;
      private DateTime AV42ContratoOcorrencia_Data2 ;
      private DateTime AV43ContratoOcorrencia_Data_To2 ;
      private DateTime A295ContratoOcorrencia_Data ;
      private bool returnInSub ;
      private bool AV39DynamicFiltersEnabled2 ;
      private bool BRKK52 ;
      private bool BRKK54 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV14TFContratoOcorrencia_Descricao ;
      private String AV15TFContratoOcorrencia_Descricao_Sel ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV38ContratoOcorrencia_Descricao1 ;
      private String AV40DynamicFiltersSelector2 ;
      private String AV44ContratoOcorrencia_Descricao2 ;
      private String lV38ContratoOcorrencia_Descricao1 ;
      private String lV44ContratoOcorrencia_Descricao2 ;
      private String lV14TFContratoOcorrencia_Descricao ;
      private String A296ContratoOcorrencia_Descricao ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00K52_A74Contrato_Codigo ;
      private String[] P00K52_A77Contrato_Numero ;
      private String[] P00K52_A296ContratoOcorrencia_Descricao ;
      private DateTime[] P00K52_A295ContratoOcorrencia_Data ;
      private int[] P00K52_A294ContratoOcorrencia_Codigo ;
      private int[] P00K53_A74Contrato_Codigo ;
      private String[] P00K53_A296ContratoOcorrencia_Descricao ;
      private String[] P00K53_A77Contrato_Numero ;
      private DateTime[] P00K53_A295ContratoOcorrencia_Data ;
      private int[] P00K53_A294ContratoOcorrencia_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getpromptcontratoocorrenciafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00K52( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             DateTime AV36ContratoOcorrencia_Data1 ,
                                             DateTime AV37ContratoOcorrencia_Data_To1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV38ContratoOcorrencia_Descricao1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             DateTime AV42ContratoOcorrencia_Data2 ,
                                             DateTime AV43ContratoOcorrencia_Data_To2 ,
                                             short AV41DynamicFiltersOperator2 ,
                                             String AV44ContratoOcorrencia_Descricao2 ,
                                             String AV11TFContrato_Numero_Sel ,
                                             String AV10TFContrato_Numero ,
                                             DateTime AV12TFContratoOcorrencia_Data ,
                                             DateTime AV13TFContratoOcorrencia_Data_To ,
                                             String AV15TFContratoOcorrencia_Descricao_Sel ,
                                             String AV14TFContratoOcorrencia_Descricao ,
                                             DateTime A295ContratoOcorrencia_Data ,
                                             String A296ContratoOcorrencia_Descricao ,
                                             String A77Contrato_Numero )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [14] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contrato_Numero], T1.[ContratoOcorrencia_Descricao], T1.[ContratoOcorrencia_Data], T1.[ContratoOcorrencia_Codigo] FROM ([ContratoOcorrencia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV36ContratoOcorrencia_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV36ContratoOcorrencia_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV36ContratoOcorrencia_Data1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV37ContratoOcorrencia_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV37ContratoOcorrencia_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV37ContratoOcorrencia_Data_To1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratoOcorrencia_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV38ContratoOcorrencia_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV38ContratoOcorrencia_Descricao1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratoOcorrencia_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV38ContratoOcorrencia_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV38ContratoOcorrencia_Descricao1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV42ContratoOcorrencia_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV42ContratoOcorrencia_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV42ContratoOcorrencia_Data2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV43ContratoOcorrencia_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV43ContratoOcorrencia_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV43ContratoOcorrencia_Data_To2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV41DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratoOcorrencia_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV44ContratoOcorrencia_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV44ContratoOcorrencia_Descricao2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV41DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratoOcorrencia_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV44ContratoOcorrencia_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV44ContratoOcorrencia_Descricao2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFContratoOcorrencia_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV12TFContratoOcorrencia_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV12TFContratoOcorrencia_Data)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFContratoOcorrencia_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV13TFContratoOcorrencia_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV13TFContratoOcorrencia_Data_To)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoOcorrencia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoOcorrencia_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV14TFContratoOcorrencia_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV14TFContratoOcorrencia_Descricao)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoOcorrencia_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] = @AV15TFContratoOcorrencia_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] = @AV15TFContratoOcorrencia_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00K53( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             DateTime AV36ContratoOcorrencia_Data1 ,
                                             DateTime AV37ContratoOcorrencia_Data_To1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV38ContratoOcorrencia_Descricao1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             DateTime AV42ContratoOcorrencia_Data2 ,
                                             DateTime AV43ContratoOcorrencia_Data_To2 ,
                                             short AV41DynamicFiltersOperator2 ,
                                             String AV44ContratoOcorrencia_Descricao2 ,
                                             String AV11TFContrato_Numero_Sel ,
                                             String AV10TFContrato_Numero ,
                                             DateTime AV12TFContratoOcorrencia_Data ,
                                             DateTime AV13TFContratoOcorrencia_Data_To ,
                                             String AV15TFContratoOcorrencia_Descricao_Sel ,
                                             String AV14TFContratoOcorrencia_Descricao ,
                                             DateTime A295ContratoOcorrencia_Data ,
                                             String A296ContratoOcorrencia_Descricao ,
                                             String A77Contrato_Numero )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [14] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[ContratoOcorrencia_Descricao], T2.[Contrato_Numero], T1.[ContratoOcorrencia_Data], T1.[ContratoOcorrencia_Codigo] FROM ([ContratoOcorrencia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV36ContratoOcorrencia_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV36ContratoOcorrencia_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV36ContratoOcorrencia_Data1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV37ContratoOcorrencia_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV37ContratoOcorrencia_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV37ContratoOcorrencia_Data_To1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratoOcorrencia_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV38ContratoOcorrencia_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV38ContratoOcorrencia_Descricao1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratoOcorrencia_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV38ContratoOcorrencia_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV38ContratoOcorrencia_Descricao1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV42ContratoOcorrencia_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV42ContratoOcorrencia_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV42ContratoOcorrencia_Data2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV43ContratoOcorrencia_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV43ContratoOcorrencia_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV43ContratoOcorrencia_Data_To2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV41DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratoOcorrencia_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV44ContratoOcorrencia_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV44ContratoOcorrencia_Descricao2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV41DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratoOcorrencia_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV44ContratoOcorrencia_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV44ContratoOcorrencia_Descricao2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFContratoOcorrencia_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV12TFContratoOcorrencia_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV12TFContratoOcorrencia_Data)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFContratoOcorrencia_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV13TFContratoOcorrencia_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV13TFContratoOcorrencia_Data_To)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoOcorrencia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoOcorrencia_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV14TFContratoOcorrencia_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV14TFContratoOcorrencia_Descricao)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoOcorrencia_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] = @AV15TFContratoOcorrencia_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] = @AV15TFContratoOcorrencia_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoOcorrencia_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00K52(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] );
               case 1 :
                     return conditional_P00K53(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00K52 ;
          prmP00K52 = new Object[] {
          new Object[] {"@AV36ContratoOcorrencia_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV37ContratoOcorrencia_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV38ContratoOcorrencia_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV38ContratoOcorrencia_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV42ContratoOcorrencia_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV43ContratoOcorrencia_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV44ContratoOcorrencia_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV44ContratoOcorrencia_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV10TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV12TFContratoOcorrencia_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV13TFContratoOcorrencia_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV14TFContratoOcorrencia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFContratoOcorrencia_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00K53 ;
          prmP00K53 = new Object[] {
          new Object[] {"@AV36ContratoOcorrencia_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV37ContratoOcorrencia_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV38ContratoOcorrencia_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV38ContratoOcorrencia_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV42ContratoOcorrencia_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV43ContratoOcorrencia_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV44ContratoOcorrencia_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV44ContratoOcorrencia_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV10TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV12TFContratoOcorrencia_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV13TFContratoOcorrencia_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV14TFContratoOcorrencia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFContratoOcorrencia_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00K52", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K52,100,0,true,false )
             ,new CursorDef("P00K53", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K53,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratoocorrenciafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratoocorrenciafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratoocorrenciafilterdata") )
          {
             return  ;
          }
          getpromptcontratoocorrenciafilterdata worker = new getpromptcontratoocorrenciafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
