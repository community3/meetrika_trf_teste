/*
               File: PRC_DLTEvidenciaDoArtefatoDaOS
        Description: Desvincula Evidencia Do Artefato Da OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:54.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_dltevidenciadoartefatodaos : GXProcedure
   {
      public prc_dltevidenciadoartefatodaos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_dltevidenciadoartefatodaos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultadoArtefato_OSCod ,
                           ref int aP1_ContagemResultadoArtefato_ArtefatoCod )
      {
         this.A1772ContagemResultadoArtefato_OSCod = aP0_ContagemResultadoArtefato_OSCod;
         this.A1771ContagemResultadoArtefato_ArtefatoCod = aP1_ContagemResultadoArtefato_ArtefatoCod;
         initialize();
         executePrivate();
         aP0_ContagemResultadoArtefato_OSCod=this.A1772ContagemResultadoArtefato_OSCod;
         aP1_ContagemResultadoArtefato_ArtefatoCod=this.A1771ContagemResultadoArtefato_ArtefatoCod;
      }

      public int executeUdp( ref int aP0_ContagemResultadoArtefato_OSCod )
      {
         this.A1772ContagemResultadoArtefato_OSCod = aP0_ContagemResultadoArtefato_OSCod;
         this.A1771ContagemResultadoArtefato_ArtefatoCod = aP1_ContagemResultadoArtefato_ArtefatoCod;
         initialize();
         executePrivate();
         aP0_ContagemResultadoArtefato_OSCod=this.A1772ContagemResultadoArtefato_OSCod;
         aP1_ContagemResultadoArtefato_ArtefatoCod=this.A1771ContagemResultadoArtefato_ArtefatoCod;
         return A1771ContagemResultadoArtefato_ArtefatoCod ;
      }

      public void executeSubmit( ref int aP0_ContagemResultadoArtefato_OSCod ,
                                 ref int aP1_ContagemResultadoArtefato_ArtefatoCod )
      {
         prc_dltevidenciadoartefatodaos objprc_dltevidenciadoartefatodaos;
         objprc_dltevidenciadoartefatodaos = new prc_dltevidenciadoartefatodaos();
         objprc_dltevidenciadoartefatodaos.A1772ContagemResultadoArtefato_OSCod = aP0_ContagemResultadoArtefato_OSCod;
         objprc_dltevidenciadoartefatodaos.A1771ContagemResultadoArtefato_ArtefatoCod = aP1_ContagemResultadoArtefato_ArtefatoCod;
         objprc_dltevidenciadoartefatodaos.context.SetSubmitInitialConfig(context);
         objprc_dltevidenciadoartefatodaos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_dltevidenciadoartefatodaos);
         aP0_ContagemResultadoArtefato_OSCod=this.A1772ContagemResultadoArtefato_OSCod;
         aP1_ContagemResultadoArtefato_ArtefatoCod=this.A1771ContagemResultadoArtefato_ArtefatoCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_dltevidenciadoartefatodaos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CX2 */
         pr_default.execute(0, new Object[] {A1771ContagemResultadoArtefato_ArtefatoCod, A1772ContagemResultadoArtefato_OSCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1770ContagemResultadoArtefato_EvdCod = P00CX2_A1770ContagemResultadoArtefato_EvdCod[0];
            n1770ContagemResultadoArtefato_EvdCod = P00CX2_n1770ContagemResultadoArtefato_EvdCod[0];
            A1769ContagemResultadoArtefato_Codigo = P00CX2_A1769ContagemResultadoArtefato_Codigo[0];
            A1770ContagemResultadoArtefato_EvdCod = 0;
            n1770ContagemResultadoArtefato_EvdCod = false;
            n1770ContagemResultadoArtefato_EvdCod = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00CX3 */
            pr_default.execute(1, new Object[] {n1770ContagemResultadoArtefato_EvdCod, A1770ContagemResultadoArtefato_EvdCod, A1769ContagemResultadoArtefato_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoArtefato") ;
            if (true) break;
            /* Using cursor P00CX4 */
            pr_default.execute(2, new Object[] {n1770ContagemResultadoArtefato_EvdCod, A1770ContagemResultadoArtefato_EvdCod, A1769ContagemResultadoArtefato_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoArtefato") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00CX2_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         P00CX2_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         P00CX2_A1770ContagemResultadoArtefato_EvdCod = new int[1] ;
         P00CX2_n1770ContagemResultadoArtefato_EvdCod = new bool[] {false} ;
         P00CX2_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_dltevidenciadoartefatodaos__default(),
            new Object[][] {
                new Object[] {
               P00CX2_A1772ContagemResultadoArtefato_OSCod, P00CX2_A1771ContagemResultadoArtefato_ArtefatoCod, P00CX2_A1770ContagemResultadoArtefato_EvdCod, P00CX2_n1770ContagemResultadoArtefato_EvdCod, P00CX2_A1769ContagemResultadoArtefato_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1772ContagemResultadoArtefato_OSCod ;
      private int A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int A1770ContagemResultadoArtefato_EvdCod ;
      private int A1769ContagemResultadoArtefato_Codigo ;
      private String scmdbuf ;
      private bool n1770ContagemResultadoArtefato_EvdCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultadoArtefato_OSCod ;
      private int aP1_ContagemResultadoArtefato_ArtefatoCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00CX2_A1772ContagemResultadoArtefato_OSCod ;
      private int[] P00CX2_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int[] P00CX2_A1770ContagemResultadoArtefato_EvdCod ;
      private bool[] P00CX2_n1770ContagemResultadoArtefato_EvdCod ;
      private int[] P00CX2_A1769ContagemResultadoArtefato_Codigo ;
   }

   public class prc_dltevidenciadoartefatodaos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CX2 ;
          prmP00CX2 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_ArtefatoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00CX3 ;
          prmP00CX3 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_EvdCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00CX4 ;
          prmP00CX4 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_EvdCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CX2", "SELECT TOP 1 [ContagemResultadoArtefato_OSCod], [ContagemResultadoArtefato_ArtefatoCod], [ContagemResultadoArtefato_EvdCod], [ContagemResultadoArtefato_Codigo] FROM [ContagemResultadoArtefato] WITH (UPDLOCK) WHERE ([ContagemResultadoArtefato_ArtefatoCod] = @ContagemResultadoArtefato_ArtefatoCod) AND ([ContagemResultadoArtefato_OSCod] = @ContagemResultadoArtefato_OSCod) ORDER BY [ContagemResultadoArtefato_ArtefatoCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CX2,1,0,true,true )
             ,new CursorDef("P00CX3", "UPDATE [ContagemResultadoArtefato] SET [ContagemResultadoArtefato_EvdCod]=@ContagemResultadoArtefato_EvdCod  WHERE [ContagemResultadoArtefato_Codigo] = @ContagemResultadoArtefato_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00CX3)
             ,new CursorDef("P00CX4", "UPDATE [ContagemResultadoArtefato] SET [ContagemResultadoArtefato_EvdCod]=@ContagemResultadoArtefato_EvdCod  WHERE [ContagemResultadoArtefato_Codigo] = @ContagemResultadoArtefato_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00CX4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
