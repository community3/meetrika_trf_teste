/*
               File: type_SdtInNewWindowTargets_InNewWindowTargetsItem
        Description: InNewWindowTargets
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:57.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "InNewWindowTargets.InNewWindowTargetsItem" )]
   [XmlType(TypeName =  "InNewWindowTargets.InNewWindowTargetsItem" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtInNewWindowTargets_InNewWindowTargetsItem : GxUserType
   {
      public SdtInNewWindowTargets_InNewWindowTargetsItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtInNewWindowTargets_InNewWindowTargetsItem_Target = "";
      }

      public SdtInNewWindowTargets_InNewWindowTargetsItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtInNewWindowTargets_InNewWindowTargetsItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtInNewWindowTargets_InNewWindowTargetsItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtInNewWindowTargets_InNewWindowTargetsItem obj ;
         obj = this;
         obj.gxTpr_Target = deserialized.gxTpr_Target;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Target") )
               {
                  gxTv_SdtInNewWindowTargets_InNewWindowTargetsItem_Target = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "InNewWindowTargets.InNewWindowTargetsItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Target", StringUtil.RTrim( gxTv_SdtInNewWindowTargets_InNewWindowTargetsItem_Target));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Target", gxTv_SdtInNewWindowTargets_InNewWindowTargetsItem_Target, false);
         return  ;
      }

      [  SoapElement( ElementName = "Target" )]
      [  XmlElement( ElementName = "Target"   )]
      public String gxTpr_Target
      {
         get {
            return gxTv_SdtInNewWindowTargets_InNewWindowTargetsItem_Target ;
         }

         set {
            gxTv_SdtInNewWindowTargets_InNewWindowTargetsItem_Target = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtInNewWindowTargets_InNewWindowTargetsItem_Target = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtInNewWindowTargets_InNewWindowTargetsItem_Target ;
      protected String sTagName ;
   }

   [DataContract(Name = @"InNewWindowTargets.InNewWindowTargetsItem", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtInNewWindowTargets_InNewWindowTargetsItem_RESTInterface : GxGenericCollectionItem<SdtInNewWindowTargets_InNewWindowTargetsItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtInNewWindowTargets_InNewWindowTargetsItem_RESTInterface( ) : base()
      {
      }

      public SdtInNewWindowTargets_InNewWindowTargetsItem_RESTInterface( SdtInNewWindowTargets_InNewWindowTargetsItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Target" , Order = 0 )]
      public String gxTpr_Target
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Target) ;
         }

         set {
            sdt.gxTpr_Target = (String)(value);
         }

      }

      public SdtInNewWindowTargets_InNewWindowTargetsItem sdt
      {
         get {
            return (SdtInNewWindowTargets_InNewWindowTargetsItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtInNewWindowTargets_InNewWindowTargetsItem() ;
         }
      }

   }

}
