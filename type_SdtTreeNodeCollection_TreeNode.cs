/*
               File: type_SdtTreeNodeCollection_TreeNode
        Description: TreeNodeCollection
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:8.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "TreeNodeCollection.TreeNode" )]
   [XmlType(TypeName =  "TreeNodeCollection.TreeNode" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtTreeNodeCollection_TreeNode ))]
   [Serializable]
   public class SdtTreeNodeCollection_TreeNode : GxUserType
   {
      public SdtTreeNodeCollection_TreeNode( )
      {
         /* Constructor for serialization */
         gxTv_SdtTreeNodeCollection_TreeNode_Id = "";
         gxTv_SdtTreeNodeCollection_TreeNode_Name = "";
         gxTv_SdtTreeNodeCollection_TreeNode_Link = "";
         gxTv_SdtTreeNodeCollection_TreeNode_Linktarget = "";
         gxTv_SdtTreeNodeCollection_TreeNode_Icon = "";
         gxTv_SdtTreeNodeCollection_TreeNode_Iconwhenselected = "";
      }

      public SdtTreeNodeCollection_TreeNode( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtTreeNodeCollection_TreeNode deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtTreeNodeCollection_TreeNode)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtTreeNodeCollection_TreeNode obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Link = deserialized.gxTpr_Link;
         obj.gxTpr_Linktarget = deserialized.gxTpr_Linktarget;
         obj.gxTpr_Expanded = deserialized.gxTpr_Expanded;
         obj.gxTpr_Dynamicload = deserialized.gxTpr_Dynamicload;
         obj.gxTpr_Icon = deserialized.gxTpr_Icon;
         obj.gxTpr_Iconwhenselected = deserialized.gxTpr_Iconwhenselected;
         obj.gxTpr_Nodes = deserialized.gxTpr_Nodes;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Id") )
               {
                  gxTv_SdtTreeNodeCollection_TreeNode_Id = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Name") )
               {
                  gxTv_SdtTreeNodeCollection_TreeNode_Name = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Link") )
               {
                  gxTv_SdtTreeNodeCollection_TreeNode_Link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LinkTarget") )
               {
                  gxTv_SdtTreeNodeCollection_TreeNode_Linktarget = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Expanded") )
               {
                  gxTv_SdtTreeNodeCollection_TreeNode_Expanded = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DynamicLoad") )
               {
                  gxTv_SdtTreeNodeCollection_TreeNode_Dynamicload = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Icon") )
               {
                  gxTv_SdtTreeNodeCollection_TreeNode_Icon = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "IconWhenSelected") )
               {
                  gxTv_SdtTreeNodeCollection_TreeNode_Iconwhenselected = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Nodes") )
               {
                  if ( gxTv_SdtTreeNodeCollection_TreeNode_Nodes == null )
                  {
                     gxTv_SdtTreeNodeCollection_TreeNode_Nodes = new GxObjectCollection( context, "TreeNodeCollection.TreeNode", "GxEv3Up14_MeetrikaVs3", "SdtTreeNodeCollection_TreeNode", "GeneXus.Programs");
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtTreeNodeCollection_TreeNode_Nodes.readxml(oReader, "Nodes");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "TreeNodeCollection.TreeNode";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Id", StringUtil.RTrim( gxTv_SdtTreeNodeCollection_TreeNode_Id));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Name", StringUtil.RTrim( gxTv_SdtTreeNodeCollection_TreeNode_Name));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Link", StringUtil.RTrim( gxTv_SdtTreeNodeCollection_TreeNode_Link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("LinkTarget", StringUtil.RTrim( gxTv_SdtTreeNodeCollection_TreeNode_Linktarget));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Expanded", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtTreeNodeCollection_TreeNode_Expanded)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("DynamicLoad", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtTreeNodeCollection_TreeNode_Dynamicload)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Icon", StringUtil.RTrim( gxTv_SdtTreeNodeCollection_TreeNode_Icon));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("IconWhenSelected", StringUtil.RTrim( gxTv_SdtTreeNodeCollection_TreeNode_Iconwhenselected));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( gxTv_SdtTreeNodeCollection_TreeNode_Nodes != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtTreeNodeCollection_TreeNode_Nodes.writexml(oWriter, "Nodes", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Id", gxTv_SdtTreeNodeCollection_TreeNode_Id, false);
         AddObjectProperty("Name", gxTv_SdtTreeNodeCollection_TreeNode_Name, false);
         AddObjectProperty("Link", gxTv_SdtTreeNodeCollection_TreeNode_Link, false);
         AddObjectProperty("LinkTarget", gxTv_SdtTreeNodeCollection_TreeNode_Linktarget, false);
         AddObjectProperty("Expanded", gxTv_SdtTreeNodeCollection_TreeNode_Expanded, false);
         AddObjectProperty("DynamicLoad", gxTv_SdtTreeNodeCollection_TreeNode_Dynamicload, false);
         AddObjectProperty("Icon", gxTv_SdtTreeNodeCollection_TreeNode_Icon, false);
         AddObjectProperty("IconWhenSelected", gxTv_SdtTreeNodeCollection_TreeNode_Iconwhenselected, false);
         if ( gxTv_SdtTreeNodeCollection_TreeNode_Nodes != null )
         {
            AddObjectProperty("Nodes", gxTv_SdtTreeNodeCollection_TreeNode_Nodes, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Id" )]
      [  XmlElement( ElementName = "Id"   )]
      public String gxTpr_Id
      {
         get {
            return gxTv_SdtTreeNodeCollection_TreeNode_Id ;
         }

         set {
            gxTv_SdtTreeNodeCollection_TreeNode_Id = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Name" )]
      [  XmlElement( ElementName = "Name"   )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtTreeNodeCollection_TreeNode_Name ;
         }

         set {
            gxTv_SdtTreeNodeCollection_TreeNode_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Link" )]
      [  XmlElement( ElementName = "Link"   )]
      public String gxTpr_Link
      {
         get {
            return gxTv_SdtTreeNodeCollection_TreeNode_Link ;
         }

         set {
            gxTv_SdtTreeNodeCollection_TreeNode_Link = (String)(value);
         }

      }

      [  SoapElement( ElementName = "LinkTarget" )]
      [  XmlElement( ElementName = "LinkTarget"   )]
      public String gxTpr_Linktarget
      {
         get {
            return gxTv_SdtTreeNodeCollection_TreeNode_Linktarget ;
         }

         set {
            gxTv_SdtTreeNodeCollection_TreeNode_Linktarget = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Expanded" )]
      [  XmlElement( ElementName = "Expanded"   )]
      public bool gxTpr_Expanded
      {
         get {
            return gxTv_SdtTreeNodeCollection_TreeNode_Expanded ;
         }

         set {
            gxTv_SdtTreeNodeCollection_TreeNode_Expanded = value;
         }

      }

      [  SoapElement( ElementName = "DynamicLoad" )]
      [  XmlElement( ElementName = "DynamicLoad"   )]
      public bool gxTpr_Dynamicload
      {
         get {
            return gxTv_SdtTreeNodeCollection_TreeNode_Dynamicload ;
         }

         set {
            gxTv_SdtTreeNodeCollection_TreeNode_Dynamicload = value;
         }

      }

      [  SoapElement( ElementName = "Icon" )]
      [  XmlElement( ElementName = "Icon"   )]
      public String gxTpr_Icon
      {
         get {
            return gxTv_SdtTreeNodeCollection_TreeNode_Icon ;
         }

         set {
            gxTv_SdtTreeNodeCollection_TreeNode_Icon = (String)(value);
         }

      }

      [  SoapElement( ElementName = "IconWhenSelected" )]
      [  XmlElement( ElementName = "IconWhenSelected"   )]
      public String gxTpr_Iconwhenselected
      {
         get {
            return gxTv_SdtTreeNodeCollection_TreeNode_Iconwhenselected ;
         }

         set {
            gxTv_SdtTreeNodeCollection_TreeNode_Iconwhenselected = (String)(value);
         }

      }

      public class gxTv_SdtTreeNodeCollection_TreeNode_Nodes_SdtTreeNodeCollection_TreeNode_80compatibility:SdtTreeNodeCollection_TreeNode {}
      [  SoapElement( ElementName = "Nodes" )]
      [  XmlArray( ElementName = "Nodes"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtTreeNodeCollection_TreeNode ), ElementName= "TreeNodeCollection.TreeNode"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Nodes_GxObjectCollection
      {
         get {
            if ( gxTv_SdtTreeNodeCollection_TreeNode_Nodes == null )
            {
               gxTv_SdtTreeNodeCollection_TreeNode_Nodes = new GxObjectCollection( context, "TreeNodeCollection.TreeNode", "GxEv3Up14_MeetrikaVs3", "SdtTreeNodeCollection_TreeNode", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtTreeNodeCollection_TreeNode_Nodes ;
         }

         set {
            if ( gxTv_SdtTreeNodeCollection_TreeNode_Nodes == null )
            {
               gxTv_SdtTreeNodeCollection_TreeNode_Nodes = new GxObjectCollection( context, "TreeNodeCollection.TreeNode", "GxEv3Up14_MeetrikaVs3", "SdtTreeNodeCollection_TreeNode", "GeneXus.Programs");
            }
            gxTv_SdtTreeNodeCollection_TreeNode_Nodes = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Nodes
      {
         get {
            if ( gxTv_SdtTreeNodeCollection_TreeNode_Nodes == null )
            {
               gxTv_SdtTreeNodeCollection_TreeNode_Nodes = new GxObjectCollection( context, "TreeNodeCollection.TreeNode", "GxEv3Up14_MeetrikaVs3", "SdtTreeNodeCollection_TreeNode", "GeneXus.Programs");
            }
            return gxTv_SdtTreeNodeCollection_TreeNode_Nodes ;
         }

         set {
            gxTv_SdtTreeNodeCollection_TreeNode_Nodes = value;
         }

      }

      public void gxTv_SdtTreeNodeCollection_TreeNode_Nodes_SetNull( )
      {
         gxTv_SdtTreeNodeCollection_TreeNode_Nodes = null;
         return  ;
      }

      public bool gxTv_SdtTreeNodeCollection_TreeNode_Nodes_IsNull( )
      {
         if ( gxTv_SdtTreeNodeCollection_TreeNode_Nodes == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtTreeNodeCollection_TreeNode_Id = "";
         gxTv_SdtTreeNodeCollection_TreeNode_Name = "";
         gxTv_SdtTreeNodeCollection_TreeNode_Link = "";
         gxTv_SdtTreeNodeCollection_TreeNode_Linktarget = "";
         gxTv_SdtTreeNodeCollection_TreeNode_Icon = "";
         gxTv_SdtTreeNodeCollection_TreeNode_Iconwhenselected = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtTreeNodeCollection_TreeNode_Id ;
      protected String gxTv_SdtTreeNodeCollection_TreeNode_Name ;
      protected String gxTv_SdtTreeNodeCollection_TreeNode_Link ;
      protected String gxTv_SdtTreeNodeCollection_TreeNode_Linktarget ;
      protected String gxTv_SdtTreeNodeCollection_TreeNode_Icon ;
      protected String gxTv_SdtTreeNodeCollection_TreeNode_Iconwhenselected ;
      protected String sTagName ;
      protected bool gxTv_SdtTreeNodeCollection_TreeNode_Expanded ;
      protected bool gxTv_SdtTreeNodeCollection_TreeNode_Dynamicload ;
      [ObjectCollection(ItemType=typeof( SdtTreeNodeCollection_TreeNode ))]
      protected IGxCollection gxTv_SdtTreeNodeCollection_TreeNode_Nodes=null ;
   }

   [DataContract(Name = @"TreeNodeCollection.TreeNode", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtTreeNodeCollection_TreeNode_RESTInterface : GxGenericCollectionItem<SdtTreeNodeCollection_TreeNode>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtTreeNodeCollection_TreeNode_RESTInterface( ) : base()
      {
      }

      public SdtTreeNodeCollection_TreeNode_RESTInterface( SdtTreeNodeCollection_TreeNode psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Id" , Order = 0 )]
      public String gxTpr_Id
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Id) ;
         }

         set {
            sdt.gxTpr_Id = (String)(value);
         }

      }

      [DataMember( Name = "Name" , Order = 1 )]
      public String gxTpr_Name
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Name) ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "Link" , Order = 2 )]
      public String gxTpr_Link
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Link) ;
         }

         set {
            sdt.gxTpr_Link = (String)(value);
         }

      }

      [DataMember( Name = "LinkTarget" , Order = 3 )]
      public String gxTpr_Linktarget
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Linktarget) ;
         }

         set {
            sdt.gxTpr_Linktarget = (String)(value);
         }

      }

      [DataMember( Name = "Expanded" , Order = 4 )]
      public bool gxTpr_Expanded
      {
         get {
            return sdt.gxTpr_Expanded ;
         }

         set {
            sdt.gxTpr_Expanded = value;
         }

      }

      [DataMember( Name = "DynamicLoad" , Order = 5 )]
      public bool gxTpr_Dynamicload
      {
         get {
            return sdt.gxTpr_Dynamicload ;
         }

         set {
            sdt.gxTpr_Dynamicload = value;
         }

      }

      [DataMember( Name = "Icon" , Order = 6 )]
      public String gxTpr_Icon
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Icon) ;
         }

         set {
            sdt.gxTpr_Icon = (String)(value);
         }

      }

      [DataMember( Name = "IconWhenSelected" , Order = 7 )]
      public String gxTpr_Iconwhenselected
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Iconwhenselected) ;
         }

         set {
            sdt.gxTpr_Iconwhenselected = (String)(value);
         }

      }

      [DataMember( Name = "Nodes" , Order = 8 )]
      public GxGenericCollection<SdtTreeNodeCollection_TreeNode_RESTInterface> gxTpr_Nodes
      {
         get {
            return new GxGenericCollection<SdtTreeNodeCollection_TreeNode_RESTInterface>(sdt.gxTpr_Nodes) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Nodes);
         }

      }

      public SdtTreeNodeCollection_TreeNode sdt
      {
         get {
            return (SdtTreeNodeCollection_TreeNode)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtTreeNodeCollection_TreeNode() ;
         }
      }

   }

}
