/*
               File: PRC_UsaOSistema
        Description: Usa o Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:26:51.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_usaosistema : GXProcedure
   {
      public prc_usaosistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_usaosistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratada_Codigo ,
                           ref int aP1_Contratante_Codigo ,
                           out bool aP2_UsaOSistema )
      {
         this.A39Contratada_Codigo = aP0_Contratada_Codigo;
         this.A29Contratante_Codigo = aP1_Contratante_Codigo;
         this.AV8UsaOSistema = false ;
         initialize();
         executePrivate();
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_Contratante_Codigo=this.A29Contratante_Codigo;
         aP2_UsaOSistema=this.AV8UsaOSistema;
      }

      public bool executeUdp( ref int aP0_Contratada_Codigo ,
                              ref int aP1_Contratante_Codigo )
      {
         this.A39Contratada_Codigo = aP0_Contratada_Codigo;
         this.A29Contratante_Codigo = aP1_Contratante_Codigo;
         this.AV8UsaOSistema = false ;
         initialize();
         executePrivate();
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_Contratante_Codigo=this.A29Contratante_Codigo;
         aP2_UsaOSistema=this.AV8UsaOSistema;
         return AV8UsaOSistema ;
      }

      public void executeSubmit( ref int aP0_Contratada_Codigo ,
                                 ref int aP1_Contratante_Codigo ,
                                 out bool aP2_UsaOSistema )
      {
         prc_usaosistema objprc_usaosistema;
         objprc_usaosistema = new prc_usaosistema();
         objprc_usaosistema.A39Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_usaosistema.A29Contratante_Codigo = aP1_Contratante_Codigo;
         objprc_usaosistema.AV8UsaOSistema = false ;
         objprc_usaosistema.context.SetSubmitInitialConfig(context);
         objprc_usaosistema.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_usaosistema);
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_Contratante_Codigo=this.A29Contratante_Codigo;
         aP2_UsaOSistema=this.AV8UsaOSistema;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_usaosistema)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( A39Contratada_Codigo > 0 )
         {
            /* Using cursor P00AS2 */
            pr_default.execute(0, new Object[] {A39Contratada_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1481Contratada_UsaOSistema = P00AS2_A1481Contratada_UsaOSistema[0];
               n1481Contratada_UsaOSistema = P00AS2_n1481Contratada_UsaOSistema[0];
               AV8UsaOSistema = A1481Contratada_UsaOSistema;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
         }
         else
         {
            /* Using cursor P00AS3 */
            pr_default.execute(1, new Object[] {A29Contratante_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1822Contratante_UsaOSistema = P00AS3_A1822Contratante_UsaOSistema[0];
               n1822Contratante_UsaOSistema = P00AS3_n1822Contratante_UsaOSistema[0];
               AV8UsaOSistema = A1822Contratante_UsaOSistema;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AS2_A39Contratada_Codigo = new int[1] ;
         P00AS2_A1481Contratada_UsaOSistema = new bool[] {false} ;
         P00AS2_n1481Contratada_UsaOSistema = new bool[] {false} ;
         P00AS3_A29Contratante_Codigo = new int[1] ;
         P00AS3_A1822Contratante_UsaOSistema = new bool[] {false} ;
         P00AS3_n1822Contratante_UsaOSistema = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_usaosistema__default(),
            new Object[][] {
                new Object[] {
               P00AS2_A39Contratada_Codigo, P00AS2_A1481Contratada_UsaOSistema, P00AS2_n1481Contratada_UsaOSistema
               }
               , new Object[] {
               P00AS3_A29Contratante_Codigo, P00AS3_A1822Contratante_UsaOSistema, P00AS3_n1822Contratante_UsaOSistema
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A39Contratada_Codigo ;
      private int A29Contratante_Codigo ;
      private String scmdbuf ;
      private bool AV8UsaOSistema ;
      private bool A1481Contratada_UsaOSistema ;
      private bool n1481Contratada_UsaOSistema ;
      private bool A1822Contratante_UsaOSistema ;
      private bool n1822Contratante_UsaOSistema ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratada_Codigo ;
      private int aP1_Contratante_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00AS2_A39Contratada_Codigo ;
      private bool[] P00AS2_A1481Contratada_UsaOSistema ;
      private bool[] P00AS2_n1481Contratada_UsaOSistema ;
      private int[] P00AS3_A29Contratante_Codigo ;
      private bool[] P00AS3_A1822Contratante_UsaOSistema ;
      private bool[] P00AS3_n1822Contratante_UsaOSistema ;
      private bool aP2_UsaOSistema ;
   }

   public class prc_usaosistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AS2 ;
          prmP00AS2 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AS3 ;
          prmP00AS3 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AS2", "SELECT TOP 1 [Contratada_Codigo], [Contratada_UsaOSistema] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AS2,1,0,false,true )
             ,new CursorDef("P00AS3", "SELECT TOP 1 [Contratante_Codigo], [Contratante_UsaOSistema] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AS3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
