/*
               File: FuncaoDadosTabelaConsultaWC
        Description: Tabelas da Fun��o de Dados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:0:54.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaodadostabelaconsultawc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public funcaodadostabelaconsultawc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public funcaodadostabelaconsultawc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoDados_SistemaCod ,
                           int aP1_FuncaoDados_Codigo )
      {
         this.A370FuncaoDados_SistemaCod = aP0_FuncaoDados_SistemaCod;
         this.A368FuncaoDados_Codigo = aP1_FuncaoDados_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavAtributos_tipodados = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A370FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
                  A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A370FuncaoDados_SistemaCod,(int)A368FuncaoDados_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridtabelas") == 0 )
               {
                  nRC_GXsfl_31 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_31_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_31_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGridtabelas_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridtabelas") == 0 )
               {
                  subGridtabelas_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                  A370FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
                  AV10Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Tabela_Codigo), 6, 0)));
                  A172Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A373FuncaoDados_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGridtabelas_refresh( subGridtabelas_Rows, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, AV10Tabela_Codigo, A172Tabela_Codigo, A373FuncaoDados_Tipo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridatributos") == 0 )
               {
                  nRC_GXsfl_42 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_42_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_42_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGridatributos_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridatributos") == 0 )
               {
                  subGridatributos_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A356Atributos_TabelaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0)));
                  AV10Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Tabela_Codigo), 6, 0)));
                  A180Atributos_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A180Atributos_Ativo", A180Atributos_Ativo);
                  A177Atributos_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A177Atributos_Nome", A177Atributos_Nome);
                  A178Atributos_TipoDados = GetNextPar( );
                  n178Atributos_TipoDados = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A178Atributos_TipoDados", A178Atributos_TipoDados);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGridatributos_refresh( subGridatributos_Rows, A356Atributos_TabelaCod, AV10Tabela_Codigo, A180Atributos_Ativo, A177Atributos_Nome, A178Atributos_TipoDados, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAA32( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               /* Using cursor H00A32 */
               pr_default.execute(0, new Object[] {A368FuncaoDados_Codigo});
               A373FuncaoDados_Tipo = H00A32_A373FuncaoDados_Tipo[0];
               pr_default.close(0);
               WSA32( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Tabelas da Fun��o de Dados") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282305424");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaodadostabelaconsultawc.aspx") + "?" + UrlEncode("" +A370FuncaoDados_SistemaCod) + "," + UrlEncode("" +A368FuncaoDados_Codigo)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_31", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_31), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_42", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_42), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA368FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10Tabela_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_TIPO", StringUtil.RTrim( A373FuncaoDados_Tipo));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_TABELACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"ATRIBUTOS_ATIVO", A180Atributos_Ativo);
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_NOME", StringUtil.RTrim( A177Atributos_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_TIPODADOS", StringUtil.RTrim( A178Atributos_TipoDados));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Width", StringUtil.RTrim( Dvpanel__Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Cls", StringUtil.RTrim( Dvpanel__Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Title", StringUtil.RTrim( Dvpanel__Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Collapsible", StringUtil.BoolToStr( Dvpanel__Collapsible));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Collapsed", StringUtil.BoolToStr( Dvpanel__Collapsed));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Autowidth", StringUtil.BoolToStr( Dvpanel__Autowidth));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Autoheight", StringUtil.BoolToStr( Dvpanel__Autoheight));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Showcollapseicon", StringUtil.BoolToStr( Dvpanel__Showcollapseicon));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Iconposition", StringUtil.RTrim( Dvpanel__Iconposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Autoscroll", StringUtil.BoolToStr( Dvpanel__Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormA32( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("funcaodadostabelaconsultawc.js", "?20204282305440");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "FuncaoDadosTabelaConsultaWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tabelas da Fun��o de Dados" ;
      }

      protected void WBA30( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "funcaodadostabelaconsultawc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            }
            wb_table1_2_A32( true) ;
         }
         else
         {
            wb_table1_2_A32( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_A32e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTA32( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Tabelas da Fun��o de Dados", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPA30( ) ;
            }
         }
      }

      protected void WSA32( )
      {
         STARTA32( ) ;
         EVTA32( ) ;
      }

      protected void EVTA32( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'INSERTTABELA'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11A32 */
                                    E11A32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDTABELASPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA30( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDTABELASPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgridtabelas_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgridtabelas_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgridtabelas_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgridtabelas_lastpage( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDATRIBUTOSPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA30( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDATRIBUTOSPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgridatributos_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgridatributos_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgridatributos_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgridatributos_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "GRIDTABELAS.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "'DELETETABELA'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 26), "GRIDTABELAS.ONLINEACTIVATE") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "'DELETETABELA'") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA30( ) ;
                              }
                              nGXsfl_31_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_31_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_31_idx), 4, 0)), 4, "0");
                              SubsflControlProps_312( ) ;
                              AV14ImagemDelete = cgiGet( edtavImagemdelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavImagemdelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV14ImagemDelete)) ? AV18Imagemdelete_GXI : context.convertURL( context.PathToRelativeUrl( AV14ImagemDelete))));
                              A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTabela_Codigo_Internalname), ",", "."));
                              A173Tabela_Nome = StringUtil.Upper( cgiGet( edtTabela_Nome_Internalname));
                              A189Tabela_ModuloDes = StringUtil.Upper( cgiGet( edtTabela_ModuloDes_Internalname));
                              n189Tabela_ModuloDes = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E12A32 */
                                          E12A32 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDTABELAS.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E13A32 */
                                          E13A32 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E14A32 */
                                          E14A32 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DELETETABELA'") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E15A32 */
                                          E15A32 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDTABELAS.ONLINEACTIVATE") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E16A32 */
                                          E16A32 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPA30( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "GRIDATRIBUTOS.LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA30( ) ;
                              }
                              nGXsfl_42_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
                              SubsflControlProps_423( ) ;
                              AV12Atributos_Nome = StringUtil.Upper( cgiGet( edtavAtributos_nome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtributos_nome_Internalname, AV12Atributos_Nome);
                              cmbavAtributos_tipodados.Name = cmbavAtributos_tipodados_Internalname;
                              cmbavAtributos_tipodados.CurrentValue = cgiGet( cmbavAtributos_tipodados_Internalname);
                              AV13Atributos_TipoDados = cgiGet( cmbavAtributos_tipodados_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavAtributos_tipodados_Internalname, AV13Atributos_TipoDados);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "GRIDATRIBUTOS.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E17A33 */
                                          E17A33 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPA30( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEA32( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormA32( ) ;
            }
         }
      }

      protected void PAA32( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "vATRIBUTOS_TIPODADOS_" + sGXsfl_42_idx;
            cmbavAtributos_tipodados.Name = GXCCtl;
            cmbavAtributos_tipodados.WebTags = "";
            cmbavAtributos_tipodados.addItem("", "Desconhecido", 0);
            cmbavAtributos_tipodados.addItem("N", "Numeric", 0);
            cmbavAtributos_tipodados.addItem("C", "Character", 0);
            cmbavAtributos_tipodados.addItem("VC", "Varchar", 0);
            cmbavAtributos_tipodados.addItem("D", "Date", 0);
            cmbavAtributos_tipodados.addItem("DT", "Date Time", 0);
            cmbavAtributos_tipodados.addItem("Bool", "Boolean", 0);
            cmbavAtributos_tipodados.addItem("Blob", "Blob", 0);
            cmbavAtributos_tipodados.addItem("Outr", "Outros", 0);
            if ( cmbavAtributos_tipodados.ItemCount > 0 )
            {
               AV13Atributos_TipoDados = cmbavAtributos_tipodados.getValidValue(AV13Atributos_TipoDados);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavAtributos_tipodados_Internalname, AV13Atributos_TipoDados);
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridtabelas_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_312( ) ;
         while ( nGXsfl_31_idx <= nRC_GXsfl_31 )
         {
            sendrow_312( ) ;
            nGXsfl_31_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_31_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_31_idx+1));
            sGXsfl_31_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_31_idx), 4, 0)), 4, "0");
            SubsflControlProps_312( ) ;
         }
         context.GX_webresponse.AddString(GridtabelasContainer.ToJavascriptSource());
         /* End function gxnrGridtabelas_newrow */
      }

      protected void gxnrGridatributos_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_423( ) ;
         while ( nGXsfl_42_idx <= nRC_GXsfl_42 )
         {
            sendrow_423( ) ;
            nGXsfl_42_idx = (short)(((subGridatributos_Islastpage==1)&&(nGXsfl_42_idx+1>subGridatributos_Recordsperpage( )) ? 1 : nGXsfl_42_idx+1));
            sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
            SubsflControlProps_423( ) ;
         }
         context.GX_webresponse.AddString(GridatributosContainer.ToJavascriptSource());
         /* End function gxnrGridatributos_newrow */
      }

      protected void gxgrGridtabelas_refresh( int subGridtabelas_Rows ,
                                              int A368FuncaoDados_Codigo ,
                                              int A370FuncaoDados_SistemaCod ,
                                              int AV10Tabela_Codigo ,
                                              int A172Tabela_Codigo ,
                                              String A373FuncaoDados_Tipo ,
                                              String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Rows), 6, 0, ".", "")));
         GRIDTABELAS_nCurrentRecord = 0;
         RFA32( ) ;
         /* End function gxgrGridtabelas_refresh */
      }

      protected void gxgrGridatributos_refresh( int subGridatributos_Rows ,
                                                int A356Atributos_TabelaCod ,
                                                int AV10Tabela_Codigo ,
                                                bool A180Atributos_Ativo ,
                                                String A177Atributos_Nome ,
                                                String A178Atributos_TipoDados ,
                                                String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         /* Execute user event: E14A32 */
         E14A32 ();
         GRIDATRIBUTOS_nCurrentRecord = 0;
         RFA33( ) ;
         /* End function gxgrGridatributos_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFA32( ) ;
         RFA33( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFA32( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridtabelasContainer.ClearRows();
         }
         wbStart = 31;
         /* Execute user event: E14A32 */
         E14A32 ();
         nGXsfl_31_idx = 1;
         sGXsfl_31_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_31_idx), 4, 0)), 4, "0");
         SubsflControlProps_312( ) ;
         nGXsfl_31_Refreshing = 1;
         GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
         GridtabelasContainer.AddObjectProperty("CmpContext", sPrefix);
         GridtabelasContainer.AddObjectProperty("InMasterPage", "false");
         GridtabelasContainer.AddObjectProperty("Class", "WorkWith");
         GridtabelasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridtabelasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridtabelasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Backcolorstyle), 1, 0, ".", "")));
         GridtabelasContainer.PageSize = subGridtabelas_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_312( ) ;
            GXPagingFrom2 = (int)(((subGridtabelas_Rows==0) ? 1 : GRIDTABELAS_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGridtabelas_Rows==0) ? 10000 : GRIDTABELAS_nFirstRecordOnPage+subGridtabelas_Recordsperpage( )+1));
            /* Using cursor H00A33 */
            pr_default.execute(1, new Object[] {A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, GXPagingFrom2, GXPagingTo2});
            nGXsfl_31_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGridtabelas_Rows == 0 ) || ( GRIDTABELAS_nCurrentRecord < subGridtabelas_Recordsperpage( ) ) ) ) )
            {
               A188Tabela_ModuloCod = H00A33_A188Tabela_ModuloCod[0];
               n188Tabela_ModuloCod = H00A33_n188Tabela_ModuloCod[0];
               A174Tabela_Ativo = H00A33_A174Tabela_Ativo[0];
               n174Tabela_Ativo = H00A33_n174Tabela_Ativo[0];
               A189Tabela_ModuloDes = H00A33_A189Tabela_ModuloDes[0];
               n189Tabela_ModuloDes = H00A33_n189Tabela_ModuloDes[0];
               A173Tabela_Nome = H00A33_A173Tabela_Nome[0];
               A172Tabela_Codigo = H00A33_A172Tabela_Codigo[0];
               A188Tabela_ModuloCod = H00A33_A188Tabela_ModuloCod[0];
               n188Tabela_ModuloCod = H00A33_n188Tabela_ModuloCod[0];
               A174Tabela_Ativo = H00A33_A174Tabela_Ativo[0];
               n174Tabela_Ativo = H00A33_n174Tabela_Ativo[0];
               A173Tabela_Nome = H00A33_A173Tabela_Nome[0];
               A189Tabela_ModuloDes = H00A33_A189Tabela_ModuloDes[0];
               n189Tabela_ModuloDes = H00A33_n189Tabela_ModuloDes[0];
               /* Execute user event: E13A32 */
               E13A32 ();
               pr_default.readNext(1);
            }
            GRIDTABELAS_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 31;
            WBA30( ) ;
         }
         nGXsfl_31_Refreshing = 0;
      }

      protected void RFA33( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridatributosContainer.ClearRows();
         }
         wbStart = 42;
         nGXsfl_42_idx = 1;
         sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
         SubsflControlProps_423( ) ;
         nGXsfl_42_Refreshing = 1;
         GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
         GridatributosContainer.AddObjectProperty("CmpContext", sPrefix);
         GridatributosContainer.AddObjectProperty("InMasterPage", "false");
         GridatributosContainer.AddObjectProperty("Class", "WorkWith");
         GridatributosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridatributosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridatributosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Backcolorstyle), 1, 0, ".", "")));
         GridatributosContainer.PageSize = subGridatributos_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_423( ) ;
            /* Execute user event: E17A33 */
            E17A33 ();
            wbEnd = 42;
            WBA30( ) ;
         }
         nGXsfl_42_Refreshing = 0;
      }

      protected int subGridtabelas_Pagecount( )
      {
         GRIDTABELAS_nRecordCount = subGridtabelas_Recordcount( );
         if ( ((int)((GRIDTABELAS_nRecordCount) % (subGridtabelas_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDTABELAS_nRecordCount/ (decimal)(subGridtabelas_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDTABELAS_nRecordCount/ (decimal)(subGridtabelas_Recordsperpage( ))))+1) ;
      }

      protected int subGridtabelas_Recordcount( )
      {
         /* Using cursor H00A34 */
         pr_default.execute(2, new Object[] {A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod});
         GRIDTABELAS_nRecordCount = H00A34_AGRIDTABELAS_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRIDTABELAS_nRecordCount) ;
      }

      protected int subGridtabelas_Recordsperpage( )
      {
         if ( subGridtabelas_Rows > 0 )
         {
            return subGridtabelas_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridtabelas_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDTABELAS_nFirstRecordOnPage/ (decimal)(subGridtabelas_Recordsperpage( ))))+1) ;
      }

      protected short subgridtabelas_firstpage( )
      {
         GRIDTABELAS_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, AV10Tabela_Codigo, A172Tabela_Codigo, A373FuncaoDados_Tipo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridtabelas_nextpage( )
      {
         GRIDTABELAS_nRecordCount = subGridtabelas_Recordcount( );
         if ( ( GRIDTABELAS_nRecordCount >= subGridtabelas_Recordsperpage( ) ) && ( GRIDTABELAS_nEOF == 0 ) )
         {
            GRIDTABELAS_nFirstRecordOnPage = (long)(GRIDTABELAS_nFirstRecordOnPage+subGridtabelas_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, AV10Tabela_Codigo, A172Tabela_Codigo, A373FuncaoDados_Tipo, sPrefix) ;
         }
         return (short)(((GRIDTABELAS_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridtabelas_previouspage( )
      {
         if ( GRIDTABELAS_nFirstRecordOnPage >= subGridtabelas_Recordsperpage( ) )
         {
            GRIDTABELAS_nFirstRecordOnPage = (long)(GRIDTABELAS_nFirstRecordOnPage-subGridtabelas_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, AV10Tabela_Codigo, A172Tabela_Codigo, A373FuncaoDados_Tipo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridtabelas_lastpage( )
      {
         GRIDTABELAS_nRecordCount = subGridtabelas_Recordcount( );
         if ( GRIDTABELAS_nRecordCount > subGridtabelas_Recordsperpage( ) )
         {
            if ( ((int)((GRIDTABELAS_nRecordCount) % (subGridtabelas_Recordsperpage( )))) == 0 )
            {
               GRIDTABELAS_nFirstRecordOnPage = (long)(GRIDTABELAS_nRecordCount-subGridtabelas_Recordsperpage( ));
            }
            else
            {
               GRIDTABELAS_nFirstRecordOnPage = (long)(GRIDTABELAS_nRecordCount-((int)((GRIDTABELAS_nRecordCount) % (subGridtabelas_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDTABELAS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, AV10Tabela_Codigo, A172Tabela_Codigo, A373FuncaoDados_Tipo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgridtabelas_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDTABELAS_nFirstRecordOnPage = (long)(subGridtabelas_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDTABELAS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, AV10Tabela_Codigo, A172Tabela_Codigo, A373FuncaoDados_Tipo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected int subGridatributos_Pagecount( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( ((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nRecordCount/ (decimal)(subGridatributos_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nRecordCount/ (decimal)(subGridatributos_Recordsperpage( ))))+1) ;
      }

      protected int subGridatributos_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridatributos_Recordsperpage( )
      {
         if ( subGridatributos_Rows > 0 )
         {
            return subGridatributos_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridatributos_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgridatributos_firstpage( )
      {
         GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, A356Atributos_TabelaCod, AV10Tabela_Codigo, A180Atributos_Ativo, A177Atributos_Nome, A178Atributos_TipoDados, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridatributos_nextpage( )
      {
         if ( GRIDATRIBUTOS_nEOF == 0 )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nFirstRecordOnPage+subGridatributos_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, A356Atributos_TabelaCod, AV10Tabela_Codigo, A180Atributos_Ativo, A177Atributos_Nome, A178Atributos_TipoDados, sPrefix) ;
         }
         return (short)(((GRIDATRIBUTOS_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridatributos_previouspage( )
      {
         if ( GRIDATRIBUTOS_nFirstRecordOnPage >= subGridatributos_Recordsperpage( ) )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nFirstRecordOnPage-subGridatributos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, A356Atributos_TabelaCod, AV10Tabela_Codigo, A180Atributos_Ativo, A177Atributos_Nome, A178Atributos_TipoDados, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridatributos_lastpage( )
      {
         subGridatributos_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, A356Atributos_TabelaCod, AV10Tabela_Codigo, A180Atributos_Ativo, A177Atributos_Nome, A178Atributos_TipoDados, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgridatributos_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(subGridatributos_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, A356Atributos_TabelaCod, AV10Tabela_Codigo, A180Atributos_Ativo, A177Atributos_Nome, A178Atributos_TipoDados, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPA30( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Using cursor H00A35 */
         pr_default.execute(3, new Object[] {A368FuncaoDados_Codigo});
         A373FuncaoDados_Tipo = H00A35_A373FuncaoDados_Tipo[0];
         pr_default.close(3);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12A32 */
         E12A32 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_31 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_31"), ",", "."));
            nRC_GXsfl_42 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_42"), ",", "."));
            wcpOA370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA370FuncaoDados_SistemaCod"), ",", "."));
            wcpOA368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA368FuncaoDados_Codigo"), ",", "."));
            GRIDTABELAS_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDTABELAS_nFirstRecordOnPage"), ",", "."));
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage"), ",", "."));
            GRIDTABELAS_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDTABELAS_nEOF"), ",", "."));
            GRIDATRIBUTOS_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_nEOF"), ",", "."));
            subGridtabelas_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDTABELAS_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Rows), 6, 0, ".", "")));
            subGridatributos_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
            Dvpanel__Width = cgiGet( sPrefix+"DVPANEL__Width");
            Dvpanel__Cls = cgiGet( sPrefix+"DVPANEL__Cls");
            Dvpanel__Title = cgiGet( sPrefix+"DVPANEL__Title");
            Dvpanel__Collapsible = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL__Collapsible"));
            Dvpanel__Collapsed = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL__Collapsed"));
            Dvpanel__Autowidth = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL__Autowidth"));
            Dvpanel__Autoheight = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL__Autoheight"));
            Dvpanel__Showcollapseicon = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL__Showcollapseicon"));
            Dvpanel__Iconposition = cgiGet( sPrefix+"DVPANEL__Iconposition");
            Dvpanel__Autoscroll = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL__Autoscroll"));
            /* Read subfile selected row values. */
            nGXsfl_31_idx = (short)(context.localUtil.CToN( cgiGet( subGridtabelas_Internalname+"_ROW"), ",", "."));
            sGXsfl_31_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_31_idx), 4, 0)), 4, "0");
            SubsflControlProps_312( ) ;
            if ( nGXsfl_31_idx > 0 )
            {
               AV14ImagemDelete = cgiGet( edtavImagemdelete_Internalname);
               A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTabela_Codigo_Internalname), ",", "."));
               A173Tabela_Nome = StringUtil.Upper( cgiGet( edtTabela_Nome_Internalname));
               A189Tabela_ModuloDes = StringUtil.Upper( cgiGet( edtTabela_ModuloDes_Internalname));
               n189Tabela_ModuloDes = false;
            }
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12A32 */
         E12A32 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12A32( )
      {
         /* Start Routine */
         subGridatributos_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         subGridtabelas_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Rows), 6, 0, ".", "")));
         AV9Sistema_Codigo = A370FuncaoDados_SistemaCod;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0)));
         AV15FuncaoDados_Codigo = A368FuncaoDados_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15FuncaoDados_Codigo), 6, 0)));
      }

      private void E13A32( )
      {
         /* Gridtabelas_Load Routine */
         if ( (0==AV10Tabela_Codigo) )
         {
            AV10Tabela_Codigo = A172Tabela_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Tabela_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(A373FuncaoDados_Tipo, "AIE") == 0 )
         {
            edtavImagemdelete_Visible = 0;
         }
         else
         {
            AV14ImagemDelete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavImagemdelete_Internalname, AV14ImagemDelete);
            AV18Imagemdelete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavImagemdelete_Tooltiptext = "Desvincular tabela da Fun��o";
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 31;
         }
         sendrow_312( ) ;
         GRIDTABELAS_nCurrentRecord = (long)(GRIDTABELAS_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_31_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(31, GridtabelasRow);
         }
      }

      protected void E14A32( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
      }

      protected void E16A32( )
      {
         /* Gridtabelas_Onlineactivate Routine */
         AV10Tabela_Codigo = A172Tabela_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Tabela_Codigo), 6, 0)));
         gxgrGridatributos_refresh( subGridatributos_Rows, A356Atributos_TabelaCod, AV10Tabela_Codigo, A180Atributos_Ativo, A177Atributos_Nome, A178Atributos_TipoDados, sPrefix) ;
      }

      protected void E11A32( )
      {
         /* 'InsertTabela' Routine */
         context.PopUp(formatLink("wp_funcaodadostabelains.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode("" +AV15FuncaoDados_Codigo), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E15A32( )
      {
         /* 'DeleteTabela' Routine */
         new prc_funcaodadosdlttabela(context ).execute( ref  AV15FuncaoDados_Codigo, ref  A172Tabela_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15FuncaoDados_Codigo), 6, 0)));
         context.DoAjaxRefreshCmp(sPrefix);
      }

      private void E17A33( )
      {
         /* Gridatributos_Load Routine */
         /* Using cursor H00A36 */
         pr_default.execute(4, new Object[] {AV10Tabela_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A174Tabela_Ativo = H00A36_A174Tabela_Ativo[0];
            n174Tabela_Ativo = H00A36_n174Tabela_Ativo[0];
            A180Atributos_Ativo = H00A36_A180Atributos_Ativo[0];
            A356Atributos_TabelaCod = H00A36_A356Atributos_TabelaCod[0];
            A177Atributos_Nome = H00A36_A177Atributos_Nome[0];
            A178Atributos_TipoDados = H00A36_A178Atributos_TipoDados[0];
            n178Atributos_TipoDados = H00A36_n178Atributos_TipoDados[0];
            A174Tabela_Ativo = H00A36_A174Tabela_Ativo[0];
            n174Tabela_Ativo = H00A36_n174Tabela_Ativo[0];
            AV12Atributos_Nome = A177Atributos_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtributos_nome_Internalname, AV12Atributos_Nome);
            AV13Atributos_TipoDados = A178Atributos_TipoDados;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavAtributos_tipodados_Internalname, AV13Atributos_TipoDados);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 42;
            }
            if ( ( subGridatributos_Islastpage == 1 ) || ( subGridatributos_Rows == 0 ) || ( ( GRIDATRIBUTOS_nCurrentRecord >= GRIDATRIBUTOS_nFirstRecordOnPage ) && ( GRIDATRIBUTOS_nCurrentRecord < GRIDATRIBUTOS_nFirstRecordOnPage + subGridatributos_Recordsperpage( ) ) ) )
            {
               sendrow_423( ) ;
               GRIDATRIBUTOS_nEOF = 1;
               GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ".", "")));
               if ( ( subGridatributos_Islastpage == 1 ) && ( ((int)((GRIDATRIBUTOS_nCurrentRecord) % (subGridatributos_Recordsperpage( )))) == 0 ) )
               {
                  GRIDATRIBUTOS_nFirstRecordOnPage = GRIDATRIBUTOS_nCurrentRecord;
               }
            }
            if ( GRIDATRIBUTOS_nCurrentRecord >= GRIDATRIBUTOS_nFirstRecordOnPage + subGridatributos_Recordsperpage( ) )
            {
               GRIDATRIBUTOS_nEOF = 0;
               GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ".", "")));
            }
            GRIDATRIBUTOS_nCurrentRecord = (long)(GRIDATRIBUTOS_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_42_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(42, GridatributosRow);
            }
            pr_default.readNext(4);
         }
         pr_default.close(4);
         cmbavAtributos_tipodados.CurrentValue = StringUtil.RTrim( AV13Atributos_TipoDados);
      }

      protected void wb_table1_2_A32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "", 0, "center", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table2_8_A32( true) ;
         }
         else
         {
            wb_table2_8_A32( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_A32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table100x100'>") ;
            wb_table3_20_A32( true) ;
         }
         else
         {
            wb_table3_20_A32( false) ;
         }
         return  ;
      }

      protected void wb_table3_20_A32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_A32e( true) ;
         }
         else
         {
            wb_table1_2_A32e( false) ;
         }
      }

      protected void wb_table3_20_A32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVPANEL_Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"DVPANEL_Container"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_25_A32( true) ;
         }
         else
         {
            wb_table4_25_A32( false) ;
         }
         return  ;
      }

      protected void wb_table4_25_A32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_20_A32e( true) ;
         }
         else
         {
            wb_table3_20_A32e( false) ;
         }
      }

      protected void wb_table4_25_A32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            wb_table5_28_A32( true) ;
         }
         else
         {
            wb_table5_28_A32( false) ;
         }
         return  ;
      }

      protected void wb_table5_28_A32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            wb_table6_39_A32( true) ;
         }
         else
         {
            wb_table6_39_A32( false) ;
         }
         return  ;
      }

      protected void wb_table6_39_A32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_25_A32e( true) ;
         }
         else
         {
            wb_table4_25_A32e( false) ;
         }
      }

      protected void wb_table6_39_A32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableContentNoMargin'>") ;
            /*  Grid Control  */
            GridatributosContainer.SetWrapped(nGXWrapped);
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridatributosContainer"+"DivS\" data-gxgridid=\"42\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridatributos_Internalname, subGridatributos_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridatributos_Backcolorstyle == 0 )
               {
                  subGridatributos_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridatributos_Class) > 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Title";
                  }
               }
               else
               {
                  subGridatributos_Titlebackstyle = 1;
                  if ( subGridatributos_Backcolorstyle == 1 )
                  {
                     subGridatributos_Titlebackcolor = subGridatributos_Allbackcolor;
                     if ( StringUtil.Len( subGridatributos_Class) > 0 )
                     {
                        subGridatributos_Linesclass = subGridatributos_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridatributos_Class) > 0 )
                     {
                        subGridatributos_Linesclass = subGridatributos_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Atributo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo de Dados") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
            }
            else
            {
               GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
               GridatributosContainer.AddObjectProperty("Class", "WorkWith");
               GridatributosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Backcolorstyle), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("CmpContext", sPrefix);
               GridatributosContainer.AddObjectProperty("InMasterPage", "false");
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( AV12Atributos_Nome));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( AV13Atributos_TipoDados));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowselection), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Selectioncolor), 9, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowhovering), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Hoveringcolor), 9, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowcollapsing), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 42 )
         {
            wbEnd = 0;
            nRC_GXsfl_42 = (short)(nGXsfl_42_idx-1);
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridatributosContainer.AddObjectProperty("GRIDATRIBUTOS_nEOF", GRIDATRIBUTOS_nEOF);
               GridatributosContainer.AddObjectProperty("GRIDATRIBUTOS_nFirstRecordOnPage", GRIDATRIBUTOS_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridatributosContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Gridatributos", GridatributosContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridatributosContainerData", GridatributosContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridatributosContainerData"+"V", GridatributosContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridatributosContainerData"+"V"+"\" value='"+GridatributosContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_39_A32e( true) ;
         }
         else
         {
            wb_table6_39_A32e( false) ;
         }
      }

      protected void wb_table5_28_A32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableContentNoMargin'>") ;
            /*  Grid Control  */
            GridtabelasContainer.SetWrapped(nGXWrapped);
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridtabelasContainer"+"DivS\" data-gxgridid=\"31\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridtabelas_Internalname, subGridtabelas_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridtabelas_Backcolorstyle == 0 )
               {
                  subGridtabelas_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                  {
                     subGridtabelas_Linesclass = subGridtabelas_Class+"Title";
                  }
               }
               else
               {
                  subGridtabelas_Titlebackstyle = 1;
                  if ( subGridtabelas_Backcolorstyle == 1 )
                  {
                     subGridtabelas_Titlebackcolor = subGridtabelas_Allbackcolor;
                     if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                     {
                        subGridtabelas_Linesclass = subGridtabelas_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                     {
                        subGridtabelas_Linesclass = subGridtabelas_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+((edtavImagemdelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "M�dulo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridtabelasContainer = new GXWebGrid( context);
               }
               else
               {
                  GridtabelasContainer.Clear();
               }
               GridtabelasContainer.SetWrapped(nGXWrapped);
               GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
               GridtabelasContainer.AddObjectProperty("Class", "WorkWith");
               GridtabelasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Backcolorstyle), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("CmpContext", sPrefix);
               GridtabelasContainer.AddObjectProperty("InMasterPage", "false");
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", context.convertURL( AV14ImagemDelete));
               GridtabelasColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavImagemdelete_Tooltiptext));
               GridtabelasColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavImagemdelete_Visible), 5, 0, ".", "")));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ".", "")));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.RTrim( A173Tabela_Nome));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.RTrim( A189Tabela_ModuloDes));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowselection), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Selectioncolor), 9, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowhovering), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Hoveringcolor), 9, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowcollapsing), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 31 )
         {
            wbEnd = 0;
            nRC_GXsfl_31 = (short)(nGXsfl_31_idx-1);
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridtabelasContainer.AddObjectProperty("GRIDTABELAS_nEOF", GRIDTABELAS_nEOF);
               GridtabelasContainer.AddObjectProperty("GRIDTABELAS_nFirstRecordOnPage", GRIDTABELAS_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridtabelasContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Gridtabelas", GridtabelasContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridtabelasContainerData", GridtabelasContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridtabelasContainerData"+"V", GridtabelasContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridtabelasContainerData"+"V"+"\" value='"+GridtabelasContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImage1_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImage1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'INSERTTABELA\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosTabelaConsultaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_28_A32e( true) ;
         }
         else
         {
            wb_table5_28_A32e( false) ;
         }
      }

      protected void wb_table2_8_A32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table7_11_A32( true) ;
         }
         else
         {
            wb_table7_11_A32( false) ;
         }
         return  ;
      }

      protected void wb_table7_11_A32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table8_15_A32( true) ;
         }
         else
         {
            wb_table8_15_A32( false) ;
         }
         return  ;
      }

      protected void wb_table8_15_A32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_A32e( true) ;
         }
         else
         {
            wb_table2_8_A32e( false) ;
         }
      }

      protected void wb_table8_15_A32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_15_A32e( true) ;
         }
         else
         {
            wb_table8_15_A32e( false) ;
         }
      }

      protected void wb_table7_11_A32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_11_A32e( true) ;
         }
         else
         {
            wb_table7_11_A32e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A370FuncaoDados_SistemaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
         A368FuncaoDados_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAA32( ) ;
         WSA32( ) ;
         WEA32( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA370FuncaoDados_SistemaCod = (String)((String)getParm(obj,0));
         sCtrlA368FuncaoDados_Codigo = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAA32( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "funcaodadostabelaconsultawc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAA32( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A370FuncaoDados_SistemaCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
            A368FuncaoDados_Codigo = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         }
         wcpOA370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA370FuncaoDados_SistemaCod"), ",", "."));
         wcpOA368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA368FuncaoDados_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A370FuncaoDados_SistemaCod != wcpOA370FuncaoDados_SistemaCod ) || ( A368FuncaoDados_Codigo != wcpOA368FuncaoDados_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA370FuncaoDados_SistemaCod = A370FuncaoDados_SistemaCod;
         wcpOA368FuncaoDados_Codigo = A368FuncaoDados_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA370FuncaoDados_SistemaCod = cgiGet( sPrefix+"A370FuncaoDados_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlA370FuncaoDados_SistemaCod) > 0 )
         {
            A370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA370FuncaoDados_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
         }
         else
         {
            A370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A370FuncaoDados_SistemaCod_PARM"), ",", "."));
         }
         sCtrlA368FuncaoDados_Codigo = cgiGet( sPrefix+"A368FuncaoDados_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA368FuncaoDados_Codigo) > 0 )
         {
            A368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA368FuncaoDados_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         }
         else
         {
            A368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A368FuncaoDados_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAA32( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSA32( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSA32( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A370FuncaoDados_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA370FuncaoDados_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A370FuncaoDados_SistemaCod_CTRL", StringUtil.RTrim( sCtrlA370FuncaoDados_SistemaCod));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A368FuncaoDados_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA368FuncaoDados_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A368FuncaoDados_Codigo_CTRL", StringUtil.RTrim( sCtrlA368FuncaoDados_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEA32( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282305527");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("funcaodadostabelaconsultawc.js", "?20204282305527");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_312( )
      {
         edtavImagemdelete_Internalname = sPrefix+"vIMAGEMDELETE_"+sGXsfl_31_idx;
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO_"+sGXsfl_31_idx;
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME_"+sGXsfl_31_idx;
         edtTabela_ModuloDes_Internalname = sPrefix+"TABELA_MODULODES_"+sGXsfl_31_idx;
      }

      protected void SubsflControlProps_fel_312( )
      {
         edtavImagemdelete_Internalname = sPrefix+"vIMAGEMDELETE_"+sGXsfl_31_fel_idx;
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO_"+sGXsfl_31_fel_idx;
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME_"+sGXsfl_31_fel_idx;
         edtTabela_ModuloDes_Internalname = sPrefix+"TABELA_MODULODES_"+sGXsfl_31_fel_idx;
      }

      protected void sendrow_312( )
      {
         SubsflControlProps_312( ) ;
         WBA30( ) ;
         if ( ( subGridtabelas_Rows * 1 == 0 ) || ( nGXsfl_31_idx <= subGridtabelas_Recordsperpage( ) * 1 ) )
         {
            GridtabelasRow = GXWebRow.GetNew(context,GridtabelasContainer);
            if ( subGridtabelas_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridtabelas_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
               }
            }
            else if ( subGridtabelas_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridtabelas_Backstyle = 0;
               subGridtabelas_Backcolor = subGridtabelas_Allbackcolor;
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Uniform";
               }
            }
            else if ( subGridtabelas_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridtabelas_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
               }
               subGridtabelas_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridtabelas_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridtabelas_Backstyle = 1;
               if ( ((int)((nGXsfl_31_idx) % (2))) == 0 )
               {
                  subGridtabelas_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
                  {
                     subGridtabelas_Linesclass = subGridtabelas_Class+"Even";
                  }
               }
               else
               {
                  subGridtabelas_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
                  {
                     subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
                  }
               }
            }
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridtabelas_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_31_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavImagemdelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavImagemdelete_Enabled!=0)&&(edtavImagemdelete_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 32,'"+sPrefix+"',false,'',31)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV14ImagemDelete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV14ImagemDelete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV18Imagemdelete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV14ImagemDelete)));
            GridtabelasRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavImagemdelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV14ImagemDelete)) ? AV18Imagemdelete_GXI : context.PathToRelativeUrl( AV14ImagemDelete)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavImagemdelete_Visible,(short)1,(String)"",(String)edtavImagemdelete_Tooltiptext,(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavImagemdelete_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DELETETABELA\\'."+sGXsfl_31_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV14ImagemDelete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)31,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Nome_Internalname,StringUtil.RTrim( A173Tabela_Nome),StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)31,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_ModuloDes_Internalname,StringUtil.RTrim( A189Tabela_ModuloDes),StringUtil.RTrim( context.localUtil.Format( A189Tabela_ModuloDes, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_ModuloDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)31,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GridtabelasContainer.AddRow(GridtabelasRow);
            nGXsfl_31_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_31_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_31_idx+1));
            sGXsfl_31_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_31_idx), 4, 0)), 4, "0");
            SubsflControlProps_312( ) ;
         }
         /* End function sendrow_312 */
      }

      protected void SubsflControlProps_423( )
      {
         edtavAtributos_nome_Internalname = sPrefix+"vATRIBUTOS_NOME_"+sGXsfl_42_idx;
         cmbavAtributos_tipodados_Internalname = sPrefix+"vATRIBUTOS_TIPODADOS_"+sGXsfl_42_idx;
      }

      protected void SubsflControlProps_fel_423( )
      {
         edtavAtributos_nome_Internalname = sPrefix+"vATRIBUTOS_NOME_"+sGXsfl_42_fel_idx;
         cmbavAtributos_tipodados_Internalname = sPrefix+"vATRIBUTOS_TIPODADOS_"+sGXsfl_42_fel_idx;
      }

      protected void sendrow_423( )
      {
         SubsflControlProps_423( ) ;
         WBA30( ) ;
         if ( ( subGridatributos_Rows * 1 == 0 ) || ( nGXsfl_42_idx <= subGridatributos_Recordsperpage( ) * 1 ) )
         {
            GridatributosRow = GXWebRow.GetNew(context,GridatributosContainer);
            if ( subGridatributos_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridatributos_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Odd";
               }
            }
            else if ( subGridatributos_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridatributos_Backstyle = 0;
               subGridatributos_Backcolor = subGridatributos_Allbackcolor;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Uniform";
               }
            }
            else if ( subGridatributos_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridatributos_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Odd";
               }
               subGridatributos_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridatributos_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridatributos_Backstyle = 1;
               if ( ((int)((nGXsfl_42_idx) % (2))) == 0 )
               {
                  subGridatributos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Even";
                  }
               }
               else
               {
                  subGridatributos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Odd";
                  }
               }
            }
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridatributos_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_42_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAtributos_nome_Internalname,StringUtil.RTrim( AV12Atributos_Nome),StringUtil.RTrim( context.localUtil.Format( AV12Atributos_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAtributos_nome_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_42_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vATRIBUTOS_TIPODADOS_" + sGXsfl_42_idx;
               cmbavAtributos_tipodados.Name = GXCCtl;
               cmbavAtributos_tipodados.WebTags = "";
               cmbavAtributos_tipodados.addItem("", "Desconhecido", 0);
               cmbavAtributos_tipodados.addItem("N", "Numeric", 0);
               cmbavAtributos_tipodados.addItem("C", "Character", 0);
               cmbavAtributos_tipodados.addItem("VC", "Varchar", 0);
               cmbavAtributos_tipodados.addItem("D", "Date", 0);
               cmbavAtributos_tipodados.addItem("DT", "Date Time", 0);
               cmbavAtributos_tipodados.addItem("Bool", "Boolean", 0);
               cmbavAtributos_tipodados.addItem("Blob", "Blob", 0);
               cmbavAtributos_tipodados.addItem("Outr", "Outros", 0);
               if ( cmbavAtributos_tipodados.ItemCount > 0 )
               {
                  AV13Atributos_TipoDados = cmbavAtributos_tipodados.getValidValue(AV13Atributos_TipoDados);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavAtributos_tipodados_Internalname, AV13Atributos_TipoDados);
               }
            }
            /* ComboBox */
            GridatributosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavAtributos_tipodados,(String)cmbavAtributos_tipodados_Internalname,StringUtil.RTrim( AV13Atributos_TipoDados),(short)1,(String)cmbavAtributos_tipodados_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)false});
            cmbavAtributos_tipodados.CurrentValue = StringUtil.RTrim( AV13Atributos_TipoDados);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados_Internalname, "Values", (String)(cmbavAtributos_tipodados.ToJavascriptSource()));
            GridatributosContainer.AddRow(GridatributosRow);
            nGXsfl_42_idx = (short)(((subGridatributos_Islastpage==1)&&(nGXsfl_42_idx+1>subGridatributos_Recordsperpage( )) ? 1 : nGXsfl_42_idx+1));
            sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
            SubsflControlProps_423( ) ;
         }
         /* End function sendrow_423 */
      }

      protected void init_default_properties( )
      {
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTableheader_Internalname = sPrefix+"TABLEHEADER";
         edtavImagemdelete_Internalname = sPrefix+"vIMAGEMDELETE";
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO";
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME";
         edtTabela_ModuloDes_Internalname = sPrefix+"TABELA_MODULODES";
         imgImage1_Internalname = sPrefix+"IMAGE1";
         tblTable2_Internalname = sPrefix+"TABLE2";
         edtavAtributos_nome_Internalname = sPrefix+"vATRIBUTOS_NOME";
         cmbavAtributos_tipodados_Internalname = sPrefix+"vATRIBUTOS_TIPODADOS";
         tblTable3_Internalname = sPrefix+"TABLE3";
         tblTable1_Internalname = sPrefix+"TABLE1";
         Dvpanel__Internalname = sPrefix+"DVPANEL_";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         tblTablemain_Internalname = sPrefix+"TABLEMAIN";
         Form.Internalname = sPrefix+"FORM";
         subGridtabelas_Internalname = sPrefix+"GRIDTABELAS";
         subGridatributos_Internalname = sPrefix+"GRIDATRIBUTOS";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbavAtributos_tipodados_Jsonclick = "";
         edtavAtributos_nome_Jsonclick = "";
         edtTabela_ModuloDes_Jsonclick = "";
         edtTabela_Nome_Jsonclick = "";
         edtTabela_Codigo_Jsonclick = "";
         edtavImagemdelete_Jsonclick = "";
         edtavImagemdelete_Enabled = 1;
         subGridtabelas_Allowcollapsing = 0;
         subGridtabelas_Hoveringcolor = (int)(0xEEFAEE);
         subGridtabelas_Allowhovering = -1;
         subGridtabelas_Selectioncolor = (int)(0xC4F0C4);
         subGridtabelas_Allowselection = 1;
         edtavImagemdelete_Tooltiptext = "";
         edtavImagemdelete_Visible = -1;
         subGridtabelas_Class = "WorkWith";
         subGridatributos_Allowcollapsing = 0;
         subGridatributos_Allowselection = 0;
         subGridatributos_Class = "WorkWith";
         subGridatributos_Backcolorstyle = 3;
         subGridtabelas_Backcolorstyle = 3;
         Dvpanel__Autoscroll = Convert.ToBoolean( 0);
         Dvpanel__Iconposition = "left";
         Dvpanel__Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel__Autoheight = Convert.ToBoolean( -1);
         Dvpanel__Autowidth = Convert.ToBoolean( 0);
         Dvpanel__Collapsed = Convert.ToBoolean( 0);
         Dvpanel__Collapsible = Convert.ToBoolean( 0);
         Dvpanel__Title = "Tabelas da Fun��o";
         Dvpanel__Cls = "GXUI-DVelop-Panel";
         subGridatributos_Rows = 0;
         subGridtabelas_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDTABELAS.LOAD","{handler:'E13A32',iparms:[{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''}],oparms:[{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'edtavImagemdelete_Visible',ctrl:'vIMAGEMDELETE',prop:'Visible'},{av:'AV14ImagemDelete',fld:'vIMAGEMDELETE',pic:'',nv:''},{av:'edtavImagemdelete_Tooltiptext',ctrl:'vIMAGEMDELETE',prop:'Tooltiptext'}]}");
         setEventMetadata("GRIDATRIBUTOS.LOAD","{handler:'E17A33',iparms:[{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''}],oparms:[{av:'AV12Atributos_Nome',fld:'vATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV13Atributos_TipoDados',fld:'vATRIBUTOS_TIPODADOS',pic:'',nv:''}]}");
         setEventMetadata("GRIDTABELAS.ONLINEACTIVATE","{handler:'E16A32',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'INSERTTABELA'","{handler:'E11A32',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV9Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DELETETABELA'","{handler:'E15A32',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV15FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDTABELAS_FIRSTPAGE","{handler:'subgridtabelas_firstpage',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDTABELAS_PREVPAGE","{handler:'subgridtabelas_previouspage',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDTABELAS_NEXTPAGE","{handler:'subgridtabelas_nextpage',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDTABELAS_LASTPAGE","{handler:'subgridtabelas_lastpage',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDATRIBUTOS_FIRSTPAGE","{handler:'subgridatributos_firstpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDATRIBUTOS_PREVPAGE","{handler:'subgridatributos_previouspage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDATRIBUTOS_NEXTPAGE","{handler:'subgridatributos_nextpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDATRIBUTOS_LASTPAGE","{handler:'subgridatributos_lastpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV10Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A180Atributos_Ativo',fld:'ATRIBUTOS_ATIVO',pic:'',nv:false},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A178Atributos_TipoDados',fld:'ATRIBUTOS_TIPODADOS',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         A373FuncaoDados_Tipo = "";
         GXKey = "";
         A177Atributos_Nome = "";
         A178Atributos_TipoDados = "";
         scmdbuf = "";
         H00A32_A370FuncaoDados_SistemaCod = new int[1] ;
         H00A32_A373FuncaoDados_Tipo = new String[] {""} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         Dvpanel__Width = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV14ImagemDelete = "";
         AV18Imagemdelete_GXI = "";
         A173Tabela_Nome = "";
         A189Tabela_ModuloDes = "";
         AV12Atributos_Nome = "";
         AV13Atributos_TipoDados = "";
         GXCCtl = "";
         GridtabelasContainer = new GXWebGrid( context);
         GridatributosContainer = new GXWebGrid( context);
         H00A33_A188Tabela_ModuloCod = new int[1] ;
         H00A33_n188Tabela_ModuloCod = new bool[] {false} ;
         H00A33_A368FuncaoDados_Codigo = new int[1] ;
         H00A33_A370FuncaoDados_SistemaCod = new int[1] ;
         H00A33_A174Tabela_Ativo = new bool[] {false} ;
         H00A33_n174Tabela_Ativo = new bool[] {false} ;
         H00A33_A373FuncaoDados_Tipo = new String[] {""} ;
         H00A33_A189Tabela_ModuloDes = new String[] {""} ;
         H00A33_n189Tabela_ModuloDes = new bool[] {false} ;
         H00A33_A173Tabela_Nome = new String[] {""} ;
         H00A33_A172Tabela_Codigo = new int[1] ;
         H00A34_AGRIDTABELAS_nRecordCount = new long[1] ;
         H00A35_A370FuncaoDados_SistemaCod = new int[1] ;
         H00A35_A373FuncaoDados_Tipo = new String[] {""} ;
         GridtabelasRow = new GXWebRow();
         H00A36_A176Atributos_Codigo = new int[1] ;
         H00A36_A174Tabela_Ativo = new bool[] {false} ;
         H00A36_n174Tabela_Ativo = new bool[] {false} ;
         H00A36_A180Atributos_Ativo = new bool[] {false} ;
         H00A36_A356Atributos_TabelaCod = new int[1] ;
         H00A36_A177Atributos_Nome = new String[] {""} ;
         H00A36_A178Atributos_TipoDados = new String[] {""} ;
         H00A36_n178Atributos_TipoDados = new bool[] {false} ;
         GridatributosRow = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGridatributos_Linesclass = "";
         GridatributosColumn = new GXWebColumn();
         subGridtabelas_Linesclass = "";
         GridtabelasColumn = new GXWebColumn();
         TempTags = "";
         imgImage1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA370FuncaoDados_SistemaCod = "";
         sCtrlA368FuncaoDados_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaodadostabelaconsultawc__default(),
            new Object[][] {
                new Object[] {
               H00A32_A370FuncaoDados_SistemaCod, H00A32_A373FuncaoDados_Tipo
               }
               , new Object[] {
               H00A33_A188Tabela_ModuloCod, H00A33_n188Tabela_ModuloCod, H00A33_A368FuncaoDados_Codigo, H00A33_A370FuncaoDados_SistemaCod, H00A33_A174Tabela_Ativo, H00A33_A373FuncaoDados_Tipo, H00A33_A189Tabela_ModuloDes, H00A33_n189Tabela_ModuloDes, H00A33_A173Tabela_Nome, H00A33_A172Tabela_Codigo
               }
               , new Object[] {
               H00A34_AGRIDTABELAS_nRecordCount
               }
               , new Object[] {
               H00A35_A370FuncaoDados_SistemaCod, H00A35_A373FuncaoDados_Tipo
               }
               , new Object[] {
               H00A36_A176Atributos_Codigo, H00A36_A174Tabela_Ativo, H00A36_n174Tabela_Ativo, H00A36_A180Atributos_Ativo, H00A36_A356Atributos_TabelaCod, H00A36_A177Atributos_Nome, H00A36_A178Atributos_TipoDados, H00A36_n178Atributos_TipoDados
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_31 ;
      private short nGXsfl_31_idx=1 ;
      private short nRC_GXsfl_42 ;
      private short nGXsfl_42_idx=1 ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRIDTABELAS_nEOF ;
      private short GRIDATRIBUTOS_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_31_Refreshing=0 ;
      private short subGridtabelas_Backcolorstyle ;
      private short nGXsfl_42_Refreshing=0 ;
      private short subGridatributos_Backcolorstyle ;
      private short subGridatributos_Titlebackstyle ;
      private short subGridatributos_Allowselection ;
      private short subGridatributos_Allowhovering ;
      private short subGridatributos_Allowcollapsing ;
      private short subGridatributos_Collapsed ;
      private short subGridtabelas_Titlebackstyle ;
      private short subGridtabelas_Allowselection ;
      private short subGridtabelas_Allowhovering ;
      private short subGridtabelas_Allowcollapsing ;
      private short subGridtabelas_Collapsed ;
      private short subGridtabelas_Backstyle ;
      private short subGridatributos_Backstyle ;
      private int A370FuncaoDados_SistemaCod ;
      private int A368FuncaoDados_Codigo ;
      private int wcpOA370FuncaoDados_SistemaCod ;
      private int wcpOA368FuncaoDados_Codigo ;
      private int subGridtabelas_Rows ;
      private int AV10Tabela_Codigo ;
      private int A172Tabela_Codigo ;
      private int subGridatributos_Rows ;
      private int A356Atributos_TabelaCod ;
      private int AV9Sistema_Codigo ;
      private int AV15FuncaoDados_Codigo ;
      private int subGridtabelas_Islastpage ;
      private int subGridatributos_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A188Tabela_ModuloCod ;
      private int edtavImagemdelete_Visible ;
      private int subGridatributos_Titlebackcolor ;
      private int subGridatributos_Allbackcolor ;
      private int subGridatributos_Selectioncolor ;
      private int subGridatributos_Hoveringcolor ;
      private int subGridtabelas_Titlebackcolor ;
      private int subGridtabelas_Allbackcolor ;
      private int subGridtabelas_Selectioncolor ;
      private int subGridtabelas_Hoveringcolor ;
      private int idxLst ;
      private int subGridtabelas_Backcolor ;
      private int edtavImagemdelete_Enabled ;
      private int subGridatributos_Backcolor ;
      private long GRIDTABELAS_nFirstRecordOnPage ;
      private long GRIDATRIBUTOS_nFirstRecordOnPage ;
      private long GRIDTABELAS_nCurrentRecord ;
      private long GRIDATRIBUTOS_nCurrentRecord ;
      private long GRIDTABELAS_nRecordCount ;
      private long GRIDATRIBUTOS_nRecordCount ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_31_idx="0001" ;
      private String A373FuncaoDados_Tipo ;
      private String GXKey ;
      private String sGXsfl_42_idx="0001" ;
      private String A177Atributos_Nome ;
      private String A178Atributos_TipoDados ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel__Width ;
      private String Dvpanel__Cls ;
      private String Dvpanel__Title ;
      private String Dvpanel__Iconposition ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavImagemdelete_Internalname ;
      private String edtTabela_Codigo_Internalname ;
      private String A173Tabela_Nome ;
      private String edtTabela_Nome_Internalname ;
      private String A189Tabela_ModuloDes ;
      private String edtTabela_ModuloDes_Internalname ;
      private String AV12Atributos_Nome ;
      private String edtavAtributos_nome_Internalname ;
      private String cmbavAtributos_tipodados_Internalname ;
      private String AV13Atributos_TipoDados ;
      private String GXCCtl ;
      private String subGridtabelas_Internalname ;
      private String edtavImagemdelete_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblTable1_Internalname ;
      private String tblTable3_Internalname ;
      private String subGridatributos_Internalname ;
      private String subGridatributos_Class ;
      private String subGridatributos_Linesclass ;
      private String tblTable2_Internalname ;
      private String subGridtabelas_Class ;
      private String subGridtabelas_Linesclass ;
      private String TempTags ;
      private String imgImage1_Internalname ;
      private String imgImage1_Jsonclick ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String tblTableactions_Internalname ;
      private String sCtrlA370FuncaoDados_SistemaCod ;
      private String sCtrlA368FuncaoDados_Codigo ;
      private String sGXsfl_31_fel_idx="0001" ;
      private String edtavImagemdelete_Jsonclick ;
      private String ROClassString ;
      private String edtTabela_Codigo_Jsonclick ;
      private String edtTabela_Nome_Jsonclick ;
      private String edtTabela_ModuloDes_Jsonclick ;
      private String sGXsfl_42_fel_idx="0001" ;
      private String edtavAtributos_nome_Jsonclick ;
      private String cmbavAtributos_tipodados_Jsonclick ;
      private String Dvpanel__Internalname ;
      private bool entryPointCalled ;
      private bool A180Atributos_Ativo ;
      private bool n178Atributos_TipoDados ;
      private bool toggleJsOutput ;
      private bool Dvpanel__Collapsible ;
      private bool Dvpanel__Collapsed ;
      private bool Dvpanel__Autowidth ;
      private bool Dvpanel__Autoheight ;
      private bool Dvpanel__Showcollapseicon ;
      private bool Dvpanel__Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n189Tabela_ModuloDes ;
      private bool n188Tabela_ModuloCod ;
      private bool A174Tabela_Ativo ;
      private bool n174Tabela_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV14ImagemDelete_IsBlob ;
      private String AV18Imagemdelete_GXI ;
      private String AV14ImagemDelete ;
      private GXWebGrid GridtabelasContainer ;
      private GXWebGrid GridatributosContainer ;
      private GXWebRow GridtabelasRow ;
      private GXWebRow GridatributosRow ;
      private GXWebColumn GridatributosColumn ;
      private GXWebColumn GridtabelasColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavAtributos_tipodados ;
      private IDataStoreProvider pr_default ;
      private int[] H00A32_A370FuncaoDados_SistemaCod ;
      private String[] H00A32_A373FuncaoDados_Tipo ;
      private int[] H00A33_A188Tabela_ModuloCod ;
      private bool[] H00A33_n188Tabela_ModuloCod ;
      private int[] H00A33_A368FuncaoDados_Codigo ;
      private int[] H00A33_A370FuncaoDados_SistemaCod ;
      private bool[] H00A33_A174Tabela_Ativo ;
      private bool[] H00A33_n174Tabela_Ativo ;
      private String[] H00A33_A373FuncaoDados_Tipo ;
      private String[] H00A33_A189Tabela_ModuloDes ;
      private bool[] H00A33_n189Tabela_ModuloDes ;
      private String[] H00A33_A173Tabela_Nome ;
      private int[] H00A33_A172Tabela_Codigo ;
      private long[] H00A34_AGRIDTABELAS_nRecordCount ;
      private int[] H00A35_A370FuncaoDados_SistemaCod ;
      private String[] H00A35_A373FuncaoDados_Tipo ;
      private int[] H00A36_A176Atributos_Codigo ;
      private bool[] H00A36_A174Tabela_Ativo ;
      private bool[] H00A36_n174Tabela_Ativo ;
      private bool[] H00A36_A180Atributos_Ativo ;
      private int[] H00A36_A356Atributos_TabelaCod ;
      private String[] H00A36_A177Atributos_Nome ;
      private String[] H00A36_A178Atributos_TipoDados ;
      private bool[] H00A36_n178Atributos_TipoDados ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class funcaodadostabelaconsultawc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00A32 ;
          prmH00A32 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00A33 ;
          prmH00A33 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00A34 ;
          prmH00A34 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00A35 ;
          prmH00A35 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00A36 ;
          prmH00A36 = new Object[] {
          new Object[] {"@AV10Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00A32", "SELECT [FuncaoDados_SistemaCod], [FuncaoDados_Tipo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A32,1,0,true,false )
             ,new CursorDef("H00A33", "SELECT * FROM (SELECT  T2.[Tabela_ModuloCod] AS Tabela_ModuloCod, T1.[FuncaoDados_Codigo], T4.[FuncaoDados_SistemaCod], T2.[Tabela_Ativo], T4.[FuncaoDados_Tipo], T3.[Modulo_Nome] AS Tabela_ModuloDes, T2.[Tabela_Nome], T1.[Tabela_Codigo], ROW_NUMBER() OVER ( ORDER BY T1.[FuncaoDados_Codigo] ) AS GX_ROW_NUMBER FROM ((([FuncaoDadosTabela] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Tabela_Codigo]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T2.[Tabela_ModuloCod]) INNER JOIN [FuncaoDados] T4 WITH (NOLOCK) ON T4.[FuncaoDados_Codigo] = T1.[FuncaoDados_Codigo]) WHERE (T1.[FuncaoDados_Codigo] = @FuncaoDados_Codigo) AND (T4.[FuncaoDados_SistemaCod] = @FuncaoDados_SistemaCod) AND (T2.[Tabela_Ativo] = 1)) AS GX_CTE WHERE GX_ROW_NUMBER BETWEEN @GXPagingFrom2 AND @GXPagingTo2 OR @GXPagingTo2 < @GXPagingFrom2 AND GX_ROW_NUMBER >= @GXPagingFrom2",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A33,11,0,true,false )
             ,new CursorDef("H00A34", "SELECT COUNT(*) FROM ((([FuncaoDadosTabela] T1 WITH (NOLOCK) INNER JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T1.[Tabela_Codigo]) LEFT JOIN [Modulo] T4 WITH (NOLOCK) ON T4.[Modulo_Codigo] = T3.[Tabela_ModuloCod]) INNER JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoDados_Codigo]) WHERE (T1.[FuncaoDados_Codigo] = @FuncaoDados_Codigo) AND (T2.[FuncaoDados_SistemaCod] = @FuncaoDados_SistemaCod) AND (T3.[Tabela_Ativo] = 1) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A34,1,0,true,false )
             ,new CursorDef("H00A35", "SELECT [FuncaoDados_SistemaCod], [FuncaoDados_Tipo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A35,1,0,true,false )
             ,new CursorDef("H00A36", "SELECT T1.[Atributos_Codigo], T2.[Tabela_Ativo], T1.[Atributos_Ativo], T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, T1.[Atributos_Nome], T1.[Atributos_TipoDados] FROM ([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod]) WHERE (T1.[Atributos_TabelaCod] = @AV10Tabela_Codigo) AND (T2.[Tabela_Ativo] = 1) AND (T1.[Atributos_Ativo] = 1) ORDER BY T1.[Atributos_TabelaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A36,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 3) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
