/*
               File: GetWWContratoOcorrenciaNotificacaoFilterData
        Description: Get WWContrato Ocorrencia Notificacao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:34.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratoocorrencianotificacaofilterdata : GXProcedure
   {
      public getwwcontratoocorrencianotificacaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratoocorrencianotificacaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
         return AV35OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratoocorrencianotificacaofilterdata objgetwwcontratoocorrencianotificacaofilterdata;
         objgetwwcontratoocorrencianotificacaofilterdata = new getwwcontratoocorrencianotificacaofilterdata();
         objgetwwcontratoocorrencianotificacaofilterdata.AV26DDOName = aP0_DDOName;
         objgetwwcontratoocorrencianotificacaofilterdata.AV24SearchTxt = aP1_SearchTxt;
         objgetwwcontratoocorrencianotificacaofilterdata.AV25SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratoocorrencianotificacaofilterdata.AV30OptionsJson = "" ;
         objgetwwcontratoocorrencianotificacaofilterdata.AV33OptionsDescJson = "" ;
         objgetwwcontratoocorrencianotificacaofilterdata.AV35OptionIndexesJson = "" ;
         objgetwwcontratoocorrencianotificacaofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratoocorrencianotificacaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratoocorrencianotificacaofilterdata);
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratoocorrencianotificacaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV29Options = (IGxCollection)(new GxSimpleCollection());
         AV32OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV34OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIANOTIFICACAO_NOMEOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV30OptionsJson = AV29Options.ToJSonString(false);
         AV33OptionsDescJson = AV32OptionsDesc.ToJSonString(false);
         AV35OptionIndexesJson = AV34OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get("WWContratoOcorrenciaNotificacaoGridState"), "") == 0 )
         {
            AV39GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoOcorrenciaNotificacaoGridState"), "");
         }
         else
         {
            AV39GridState.FromXml(AV37Session.Get("WWContratoOcorrenciaNotificacaoGridState"), "");
         }
         AV55GXV1 = 1;
         while ( AV55GXV1 <= AV39GridState.gxTpr_Filtervalues.Count )
         {
            AV40GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV39GridState.gxTpr_Filtervalues.Item(AV55GXV1));
            if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
            {
               AV12TFContratoOcorrenciaNotificacao_Data = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Value, 2);
               AV13TFContratoOcorrenciaNotificacao_Data_To = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO") == 0 )
            {
               AV14TFContratoOcorrenciaNotificacao_Descricao = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL") == 0 )
            {
               AV15TFContratoOcorrenciaNotificacao_Descricao_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 )
            {
               AV16TFContratoOcorrenciaNotificacao_Prazo = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV17TFContratoOcorrenciaNotificacao_Prazo_To = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO") == 0 )
            {
               AV18TFContratoOcorrenciaNotificacao_Cumprido = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Value, 2);
               AV19TFContratoOcorrenciaNotificacao_Cumprido_To = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO") == 0 )
            {
               AV20TFContratoOcorrenciaNotificacao_Protocolo = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL") == 0 )
            {
               AV21TFContratoOcorrenciaNotificacao_Protocolo_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
            {
               AV22TFContratoOcorrenciaNotificacao_Nome = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL") == 0 )
            {
               AV23TFContratoOcorrenciaNotificacao_Nome_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            AV55GXV1 = (int)(AV55GXV1+1);
         }
         if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(1));
            AV42DynamicFiltersSelector1 = AV41GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
            {
               AV44ContratoOcorrenciaNotificacao_Data1 = context.localUtil.CToD( AV41GridStateDynamicFilter.gxTpr_Value, 2);
               AV45ContratoOcorrenciaNotificacao_Data_To1 = context.localUtil.CToD( AV41GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
            {
               AV43DynamicFiltersOperator1 = AV41GridStateDynamicFilter.gxTpr_Operator;
               AV46ContratoOcorrenciaNotificacao_Nome1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV47DynamicFiltersEnabled2 = true;
               AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(2));
               AV48DynamicFiltersSelector2 = AV41GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
               {
                  AV50ContratoOcorrenciaNotificacao_Data2 = context.localUtil.CToD( AV41GridStateDynamicFilter.gxTpr_Value, 2);
                  AV51ContratoOcorrenciaNotificacao_Data_To2 = context.localUtil.CToD( AV41GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
               {
                  AV49DynamicFiltersOperator2 = AV41GridStateDynamicFilter.gxTpr_Operator;
                  AV52ContratoOcorrenciaNotificacao_Nome2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV24SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 = AV44ContratoOcorrenciaNotificacao_Data1;
         AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 = AV45ContratoOcorrenciaNotificacao_Data_To1;
         AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = AV46ContratoOcorrenciaNotificacao_Nome1;
         AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 = AV49DynamicFiltersOperator2;
         AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 = AV50ContratoOcorrenciaNotificacao_Data2;
         AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 = AV51ContratoOcorrenciaNotificacao_Data_To2;
         AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = AV52ContratoOcorrenciaNotificacao_Nome2;
         AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = AV10TFContrato_Numero;
         AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data = AV12TFContratoOcorrenciaNotificacao_Data;
         AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to = AV13TFContratoOcorrenciaNotificacao_Data_To;
         AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = AV14TFContratoOcorrenciaNotificacao_Descricao;
         AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel = AV15TFContratoOcorrenciaNotificacao_Descricao_Sel;
         AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo = AV16TFContratoOcorrenciaNotificacao_Prazo;
         AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to = AV17TFContratoOcorrenciaNotificacao_Prazo_To;
         AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido = AV18TFContratoOcorrenciaNotificacao_Cumprido;
         AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to = AV19TFContratoOcorrenciaNotificacao_Cumprido_To;
         AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = AV20TFContratoOcorrenciaNotificacao_Protocolo;
         AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel = AV21TFContratoOcorrenciaNotificacao_Protocolo_Sel;
         AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = AV22TFContratoOcorrenciaNotificacao_Nome;
         AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel = AV23TFContratoOcorrenciaNotificacao_Nome_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ,
                                              AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ,
                                              AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ,
                                              AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ,
                                              AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ,
                                              AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ,
                                              AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ,
                                              AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ,
                                              AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ,
                                              AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ,
                                              AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ,
                                              AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ,
                                              AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ,
                                              AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ,
                                              AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ,
                                              AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ,
                                              AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ,
                                              AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ,
                                              AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ,
                                              AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ,
                                              AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ,
                                              AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ,
                                              AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ,
                                              AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ,
                                              AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A77Contrato_Numero ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1), 100, "%");
         lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1), 100, "%");
         lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2), 100, "%");
         lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2), 100, "%");
         lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero), 20, "%");
         lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao), "%", "");
         lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = StringUtil.Concat( StringUtil.RTrim( AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo), "%", "");
         lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = StringUtil.PadR( StringUtil.RTrim( AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome), 100, "%");
         /* Using cursor P00K92 */
         pr_default.execute(0, new Object[] {AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1, AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1, lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1, lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1, AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2, AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2, lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2, lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2, lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero, AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel, AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data, AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to, lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao, AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel, AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo, AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to, AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido, AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to, lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo, AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel, lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome, AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKK92 = false;
            A294ContratoOcorrencia_Codigo = P00K92_A294ContratoOcorrencia_Codigo[0];
            A74Contrato_Codigo = P00K92_A74Contrato_Codigo[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = P00K92_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            A77Contrato_Numero = P00K92_A77Contrato_Numero[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = P00K92_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            n302ContratoOcorrenciaNotificacao_Protocolo = P00K92_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A301ContratoOcorrenciaNotificacao_Cumprido = P00K92_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            n301ContratoOcorrenciaNotificacao_Cumprido = P00K92_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A299ContratoOcorrenciaNotificacao_Prazo = P00K92_A299ContratoOcorrenciaNotificacao_Prazo[0];
            A300ContratoOcorrenciaNotificacao_Descricao = P00K92_A300ContratoOcorrenciaNotificacao_Descricao[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K92_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K92_n304ContratoOcorrenciaNotificacao_Nome[0];
            A298ContratoOcorrenciaNotificacao_Data = P00K92_A298ContratoOcorrenciaNotificacao_Data[0];
            A297ContratoOcorrenciaNotificacao_Codigo = P00K92_A297ContratoOcorrenciaNotificacao_Codigo[0];
            A74Contrato_Codigo = P00K92_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00K92_A77Contrato_Numero[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K92_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K92_n304ContratoOcorrenciaNotificacao_Nome[0];
            AV36count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00K92_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKK92 = false;
               A294ContratoOcorrencia_Codigo = P00K92_A294ContratoOcorrencia_Codigo[0];
               A74Contrato_Codigo = P00K92_A74Contrato_Codigo[0];
               A297ContratoOcorrenciaNotificacao_Codigo = P00K92_A297ContratoOcorrenciaNotificacao_Codigo[0];
               A74Contrato_Codigo = P00K92_A74Contrato_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKK92 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV28Option = A77Contrato_Numero;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK92 )
            {
               BRKK92 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOOPTIONS' Routine */
         AV14TFContratoOcorrenciaNotificacao_Descricao = AV24SearchTxt;
         AV15TFContratoOcorrenciaNotificacao_Descricao_Sel = "";
         AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 = AV44ContratoOcorrenciaNotificacao_Data1;
         AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 = AV45ContratoOcorrenciaNotificacao_Data_To1;
         AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = AV46ContratoOcorrenciaNotificacao_Nome1;
         AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 = AV49DynamicFiltersOperator2;
         AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 = AV50ContratoOcorrenciaNotificacao_Data2;
         AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 = AV51ContratoOcorrenciaNotificacao_Data_To2;
         AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = AV52ContratoOcorrenciaNotificacao_Nome2;
         AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = AV10TFContrato_Numero;
         AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data = AV12TFContratoOcorrenciaNotificacao_Data;
         AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to = AV13TFContratoOcorrenciaNotificacao_Data_To;
         AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = AV14TFContratoOcorrenciaNotificacao_Descricao;
         AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel = AV15TFContratoOcorrenciaNotificacao_Descricao_Sel;
         AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo = AV16TFContratoOcorrenciaNotificacao_Prazo;
         AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to = AV17TFContratoOcorrenciaNotificacao_Prazo_To;
         AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido = AV18TFContratoOcorrenciaNotificacao_Cumprido;
         AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to = AV19TFContratoOcorrenciaNotificacao_Cumprido_To;
         AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = AV20TFContratoOcorrenciaNotificacao_Protocolo;
         AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel = AV21TFContratoOcorrenciaNotificacao_Protocolo_Sel;
         AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = AV22TFContratoOcorrenciaNotificacao_Nome;
         AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel = AV23TFContratoOcorrenciaNotificacao_Nome_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ,
                                              AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ,
                                              AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ,
                                              AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ,
                                              AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ,
                                              AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ,
                                              AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ,
                                              AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ,
                                              AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ,
                                              AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ,
                                              AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ,
                                              AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ,
                                              AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ,
                                              AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ,
                                              AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ,
                                              AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ,
                                              AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ,
                                              AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ,
                                              AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ,
                                              AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ,
                                              AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ,
                                              AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ,
                                              AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ,
                                              AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ,
                                              AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A77Contrato_Numero ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1), 100, "%");
         lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1), 100, "%");
         lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2), 100, "%");
         lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2), 100, "%");
         lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero), 20, "%");
         lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao), "%", "");
         lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = StringUtil.Concat( StringUtil.RTrim( AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo), "%", "");
         lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = StringUtil.PadR( StringUtil.RTrim( AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome), 100, "%");
         /* Using cursor P00K93 */
         pr_default.execute(1, new Object[] {AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1, AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1, lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1, lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1, AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2, AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2, lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2, lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2, lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero, AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel, AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data, AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to, lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao, AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel, AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo, AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to, AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido, AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to, lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo, AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel, lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome, AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKK94 = false;
            A294ContratoOcorrencia_Codigo = P00K93_A294ContratoOcorrencia_Codigo[0];
            A74Contrato_Codigo = P00K93_A74Contrato_Codigo[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = P00K93_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            A300ContratoOcorrenciaNotificacao_Descricao = P00K93_A300ContratoOcorrenciaNotificacao_Descricao[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = P00K93_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            n302ContratoOcorrenciaNotificacao_Protocolo = P00K93_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A301ContratoOcorrenciaNotificacao_Cumprido = P00K93_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            n301ContratoOcorrenciaNotificacao_Cumprido = P00K93_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A299ContratoOcorrenciaNotificacao_Prazo = P00K93_A299ContratoOcorrenciaNotificacao_Prazo[0];
            A77Contrato_Numero = P00K93_A77Contrato_Numero[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K93_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K93_n304ContratoOcorrenciaNotificacao_Nome[0];
            A298ContratoOcorrenciaNotificacao_Data = P00K93_A298ContratoOcorrenciaNotificacao_Data[0];
            A297ContratoOcorrenciaNotificacao_Codigo = P00K93_A297ContratoOcorrenciaNotificacao_Codigo[0];
            A74Contrato_Codigo = P00K93_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00K93_A77Contrato_Numero[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K93_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K93_n304ContratoOcorrenciaNotificacao_Nome[0];
            AV36count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00K93_A300ContratoOcorrenciaNotificacao_Descricao[0], A300ContratoOcorrenciaNotificacao_Descricao) == 0 ) )
            {
               BRKK94 = false;
               A297ContratoOcorrenciaNotificacao_Codigo = P00K93_A297ContratoOcorrenciaNotificacao_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKK94 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A300ContratoOcorrenciaNotificacao_Descricao)) )
            {
               AV28Option = A300ContratoOcorrenciaNotificacao_Descricao;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK94 )
            {
               BRKK94 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOOPTIONS' Routine */
         AV20TFContratoOcorrenciaNotificacao_Protocolo = AV24SearchTxt;
         AV21TFContratoOcorrenciaNotificacao_Protocolo_Sel = "";
         AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 = AV44ContratoOcorrenciaNotificacao_Data1;
         AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 = AV45ContratoOcorrenciaNotificacao_Data_To1;
         AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = AV46ContratoOcorrenciaNotificacao_Nome1;
         AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 = AV49DynamicFiltersOperator2;
         AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 = AV50ContratoOcorrenciaNotificacao_Data2;
         AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 = AV51ContratoOcorrenciaNotificacao_Data_To2;
         AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = AV52ContratoOcorrenciaNotificacao_Nome2;
         AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = AV10TFContrato_Numero;
         AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data = AV12TFContratoOcorrenciaNotificacao_Data;
         AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to = AV13TFContratoOcorrenciaNotificacao_Data_To;
         AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = AV14TFContratoOcorrenciaNotificacao_Descricao;
         AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel = AV15TFContratoOcorrenciaNotificacao_Descricao_Sel;
         AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo = AV16TFContratoOcorrenciaNotificacao_Prazo;
         AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to = AV17TFContratoOcorrenciaNotificacao_Prazo_To;
         AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido = AV18TFContratoOcorrenciaNotificacao_Cumprido;
         AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to = AV19TFContratoOcorrenciaNotificacao_Cumprido_To;
         AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = AV20TFContratoOcorrenciaNotificacao_Protocolo;
         AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel = AV21TFContratoOcorrenciaNotificacao_Protocolo_Sel;
         AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = AV22TFContratoOcorrenciaNotificacao_Nome;
         AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel = AV23TFContratoOcorrenciaNotificacao_Nome_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ,
                                              AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ,
                                              AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ,
                                              AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ,
                                              AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ,
                                              AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ,
                                              AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ,
                                              AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ,
                                              AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ,
                                              AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ,
                                              AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ,
                                              AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ,
                                              AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ,
                                              AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ,
                                              AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ,
                                              AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ,
                                              AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ,
                                              AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ,
                                              AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ,
                                              AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ,
                                              AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ,
                                              AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ,
                                              AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ,
                                              AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ,
                                              AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A77Contrato_Numero ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1), 100, "%");
         lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1), 100, "%");
         lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2), 100, "%");
         lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2), 100, "%");
         lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero), 20, "%");
         lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao), "%", "");
         lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = StringUtil.Concat( StringUtil.RTrim( AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo), "%", "");
         lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = StringUtil.PadR( StringUtil.RTrim( AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome), 100, "%");
         /* Using cursor P00K94 */
         pr_default.execute(2, new Object[] {AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1, AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1, lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1, lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1, AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2, AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2, lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2, lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2, lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero, AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel, AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data, AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to, lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao, AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel, AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo, AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to, AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido, AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to, lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo, AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel, lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome, AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKK96 = false;
            A294ContratoOcorrencia_Codigo = P00K94_A294ContratoOcorrencia_Codigo[0];
            A74Contrato_Codigo = P00K94_A74Contrato_Codigo[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = P00K94_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = P00K94_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            n302ContratoOcorrenciaNotificacao_Protocolo = P00K94_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A301ContratoOcorrenciaNotificacao_Cumprido = P00K94_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            n301ContratoOcorrenciaNotificacao_Cumprido = P00K94_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A299ContratoOcorrenciaNotificacao_Prazo = P00K94_A299ContratoOcorrenciaNotificacao_Prazo[0];
            A300ContratoOcorrenciaNotificacao_Descricao = P00K94_A300ContratoOcorrenciaNotificacao_Descricao[0];
            A77Contrato_Numero = P00K94_A77Contrato_Numero[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K94_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K94_n304ContratoOcorrenciaNotificacao_Nome[0];
            A298ContratoOcorrenciaNotificacao_Data = P00K94_A298ContratoOcorrenciaNotificacao_Data[0];
            A297ContratoOcorrenciaNotificacao_Codigo = P00K94_A297ContratoOcorrenciaNotificacao_Codigo[0];
            A74Contrato_Codigo = P00K94_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00K94_A77Contrato_Numero[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K94_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K94_n304ContratoOcorrenciaNotificacao_Nome[0];
            AV36count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00K94_A302ContratoOcorrenciaNotificacao_Protocolo[0], A302ContratoOcorrenciaNotificacao_Protocolo) == 0 ) )
            {
               BRKK96 = false;
               A297ContratoOcorrenciaNotificacao_Codigo = P00K94_A297ContratoOcorrenciaNotificacao_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKK96 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A302ContratoOcorrenciaNotificacao_Protocolo)) )
            {
               AV28Option = A302ContratoOcorrenciaNotificacao_Protocolo;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK96 )
            {
               BRKK96 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATOOCORRENCIANOTIFICACAO_NOMEOPTIONS' Routine */
         AV22TFContratoOcorrenciaNotificacao_Nome = AV24SearchTxt;
         AV23TFContratoOcorrenciaNotificacao_Nome_Sel = "";
         AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 = AV44ContratoOcorrenciaNotificacao_Data1;
         AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 = AV45ContratoOcorrenciaNotificacao_Data_To1;
         AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = AV46ContratoOcorrenciaNotificacao_Nome1;
         AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 = AV49DynamicFiltersOperator2;
         AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 = AV50ContratoOcorrenciaNotificacao_Data2;
         AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 = AV51ContratoOcorrenciaNotificacao_Data_To2;
         AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = AV52ContratoOcorrenciaNotificacao_Nome2;
         AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = AV10TFContrato_Numero;
         AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data = AV12TFContratoOcorrenciaNotificacao_Data;
         AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to = AV13TFContratoOcorrenciaNotificacao_Data_To;
         AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = AV14TFContratoOcorrenciaNotificacao_Descricao;
         AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel = AV15TFContratoOcorrenciaNotificacao_Descricao_Sel;
         AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo = AV16TFContratoOcorrenciaNotificacao_Prazo;
         AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to = AV17TFContratoOcorrenciaNotificacao_Prazo_To;
         AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido = AV18TFContratoOcorrenciaNotificacao_Cumprido;
         AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to = AV19TFContratoOcorrenciaNotificacao_Cumprido_To;
         AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = AV20TFContratoOcorrenciaNotificacao_Protocolo;
         AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel = AV21TFContratoOcorrenciaNotificacao_Protocolo_Sel;
         AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = AV22TFContratoOcorrenciaNotificacao_Nome;
         AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel = AV23TFContratoOcorrenciaNotificacao_Nome_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ,
                                              AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ,
                                              AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ,
                                              AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ,
                                              AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ,
                                              AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ,
                                              AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ,
                                              AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ,
                                              AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ,
                                              AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ,
                                              AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ,
                                              AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ,
                                              AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ,
                                              AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ,
                                              AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ,
                                              AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ,
                                              AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ,
                                              AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ,
                                              AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ,
                                              AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ,
                                              AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ,
                                              AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ,
                                              AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ,
                                              AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ,
                                              AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A77Contrato_Numero ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1), 100, "%");
         lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1), 100, "%");
         lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2), 100, "%");
         lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2), 100, "%");
         lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero), 20, "%");
         lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao), "%", "");
         lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = StringUtil.Concat( StringUtil.RTrim( AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo), "%", "");
         lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = StringUtil.PadR( StringUtil.RTrim( AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome), 100, "%");
         /* Using cursor P00K95 */
         pr_default.execute(3, new Object[] {AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1, AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1, lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1, lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1, AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2, AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2, lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2, lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2, lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero, AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel, AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data, AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to, lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao, AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel, AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo, AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to, AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido, AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to, lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo, AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel, lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome, AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKK98 = false;
            A294ContratoOcorrencia_Codigo = P00K95_A294ContratoOcorrencia_Codigo[0];
            A74Contrato_Codigo = P00K95_A74Contrato_Codigo[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = P00K95_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = P00K95_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            n302ContratoOcorrenciaNotificacao_Protocolo = P00K95_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A301ContratoOcorrenciaNotificacao_Cumprido = P00K95_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            n301ContratoOcorrenciaNotificacao_Cumprido = P00K95_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A299ContratoOcorrenciaNotificacao_Prazo = P00K95_A299ContratoOcorrenciaNotificacao_Prazo[0];
            A300ContratoOcorrenciaNotificacao_Descricao = P00K95_A300ContratoOcorrenciaNotificacao_Descricao[0];
            A77Contrato_Numero = P00K95_A77Contrato_Numero[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K95_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K95_n304ContratoOcorrenciaNotificacao_Nome[0];
            A298ContratoOcorrenciaNotificacao_Data = P00K95_A298ContratoOcorrenciaNotificacao_Data[0];
            A297ContratoOcorrenciaNotificacao_Codigo = P00K95_A297ContratoOcorrenciaNotificacao_Codigo[0];
            A74Contrato_Codigo = P00K95_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00K95_A77Contrato_Numero[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K95_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K95_n304ContratoOcorrenciaNotificacao_Nome[0];
            AV36count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( P00K95_A303ContratoOcorrenciaNotificacao_Responsavel[0] == A303ContratoOcorrenciaNotificacao_Responsavel ) )
            {
               BRKK98 = false;
               A297ContratoOcorrenciaNotificacao_Codigo = P00K95_A297ContratoOcorrenciaNotificacao_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKK98 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A304ContratoOcorrenciaNotificacao_Nome)) )
            {
               AV28Option = A304ContratoOcorrenciaNotificacao_Nome;
               AV27InsertIndex = 1;
               while ( ( AV27InsertIndex <= AV29Options.Count ) && ( StringUtil.StrCmp(((String)AV29Options.Item(AV27InsertIndex)), AV28Option) < 0 ) )
               {
                  AV27InsertIndex = (int)(AV27InsertIndex+1);
               }
               AV29Options.Add(AV28Option, AV27InsertIndex);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), AV27InsertIndex);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK98 )
            {
               BRKK98 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV29Options = new GxSimpleCollection();
         AV32OptionsDesc = new GxSimpleCollection();
         AV34OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37Session = context.GetSession();
         AV39GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV40GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         AV13TFContratoOcorrenciaNotificacao_Data_To = DateTime.MinValue;
         AV14TFContratoOcorrenciaNotificacao_Descricao = "";
         AV15TFContratoOcorrenciaNotificacao_Descricao_Sel = "";
         AV18TFContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         AV19TFContratoOcorrenciaNotificacao_Cumprido_To = DateTime.MinValue;
         AV20TFContratoOcorrenciaNotificacao_Protocolo = "";
         AV21TFContratoOcorrenciaNotificacao_Protocolo_Sel = "";
         AV22TFContratoOcorrenciaNotificacao_Nome = "";
         AV23TFContratoOcorrenciaNotificacao_Nome_Sel = "";
         AV41GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV42DynamicFiltersSelector1 = "";
         AV44ContratoOcorrenciaNotificacao_Data1 = DateTime.MinValue;
         AV45ContratoOcorrenciaNotificacao_Data_To1 = DateTime.MinValue;
         AV46ContratoOcorrenciaNotificacao_Nome1 = "";
         AV48DynamicFiltersSelector2 = "";
         AV50ContratoOcorrenciaNotificacao_Data2 = DateTime.MinValue;
         AV51ContratoOcorrenciaNotificacao_Data_To2 = DateTime.MinValue;
         AV52ContratoOcorrenciaNotificacao_Nome2 = "";
         AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 = "";
         AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 = DateTime.MinValue;
         AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 = DateTime.MinValue;
         AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = "";
         AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 = "";
         AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 = DateTime.MinValue;
         AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 = DateTime.MinValue;
         AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = "";
         AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = "";
         AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel = "";
         AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data = DateTime.MinValue;
         AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to = DateTime.MinValue;
         AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = "";
         AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel = "";
         AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido = DateTime.MinValue;
         AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to = DateTime.MinValue;
         AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = "";
         AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel = "";
         AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = "";
         AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel = "";
         scmdbuf = "";
         lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = "";
         lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = "";
         lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = "";
         lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = "";
         lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = "";
         lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = "";
         A298ContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         A304ContratoOcorrenciaNotificacao_Nome = "";
         A77Contrato_Numero = "";
         A300ContratoOcorrenciaNotificacao_Descricao = "";
         A301ContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         A302ContratoOcorrenciaNotificacao_Protocolo = "";
         P00K92_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00K92_A74Contrato_Codigo = new int[1] ;
         P00K92_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         P00K92_A77Contrato_Numero = new String[] {""} ;
         P00K92_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         P00K92_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         P00K92_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         P00K92_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         P00K92_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         P00K92_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         P00K92_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         P00K92_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         P00K92_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         P00K92_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         AV28Option = "";
         P00K93_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00K93_A74Contrato_Codigo = new int[1] ;
         P00K93_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         P00K93_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         P00K93_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         P00K93_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         P00K93_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         P00K93_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         P00K93_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         P00K93_A77Contrato_Numero = new String[] {""} ;
         P00K93_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         P00K93_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         P00K93_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         P00K93_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         P00K94_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00K94_A74Contrato_Codigo = new int[1] ;
         P00K94_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         P00K94_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         P00K94_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         P00K94_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         P00K94_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         P00K94_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         P00K94_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         P00K94_A77Contrato_Numero = new String[] {""} ;
         P00K94_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         P00K94_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         P00K94_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         P00K94_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         P00K95_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00K95_A74Contrato_Codigo = new int[1] ;
         P00K95_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         P00K95_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         P00K95_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         P00K95_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         P00K95_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         P00K95_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         P00K95_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         P00K95_A77Contrato_Numero = new String[] {""} ;
         P00K95_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         P00K95_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         P00K95_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         P00K95_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratoocorrencianotificacaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00K92_A294ContratoOcorrencia_Codigo, P00K92_A74Contrato_Codigo, P00K92_A303ContratoOcorrenciaNotificacao_Responsavel, P00K92_A77Contrato_Numero, P00K92_A302ContratoOcorrenciaNotificacao_Protocolo, P00K92_n302ContratoOcorrenciaNotificacao_Protocolo, P00K92_A301ContratoOcorrenciaNotificacao_Cumprido, P00K92_n301ContratoOcorrenciaNotificacao_Cumprido, P00K92_A299ContratoOcorrenciaNotificacao_Prazo, P00K92_A300ContratoOcorrenciaNotificacao_Descricao,
               P00K92_A304ContratoOcorrenciaNotificacao_Nome, P00K92_n304ContratoOcorrenciaNotificacao_Nome, P00K92_A298ContratoOcorrenciaNotificacao_Data, P00K92_A297ContratoOcorrenciaNotificacao_Codigo
               }
               , new Object[] {
               P00K93_A294ContratoOcorrencia_Codigo, P00K93_A74Contrato_Codigo, P00K93_A303ContratoOcorrenciaNotificacao_Responsavel, P00K93_A300ContratoOcorrenciaNotificacao_Descricao, P00K93_A302ContratoOcorrenciaNotificacao_Protocolo, P00K93_n302ContratoOcorrenciaNotificacao_Protocolo, P00K93_A301ContratoOcorrenciaNotificacao_Cumprido, P00K93_n301ContratoOcorrenciaNotificacao_Cumprido, P00K93_A299ContratoOcorrenciaNotificacao_Prazo, P00K93_A77Contrato_Numero,
               P00K93_A304ContratoOcorrenciaNotificacao_Nome, P00K93_n304ContratoOcorrenciaNotificacao_Nome, P00K93_A298ContratoOcorrenciaNotificacao_Data, P00K93_A297ContratoOcorrenciaNotificacao_Codigo
               }
               , new Object[] {
               P00K94_A294ContratoOcorrencia_Codigo, P00K94_A74Contrato_Codigo, P00K94_A303ContratoOcorrenciaNotificacao_Responsavel, P00K94_A302ContratoOcorrenciaNotificacao_Protocolo, P00K94_n302ContratoOcorrenciaNotificacao_Protocolo, P00K94_A301ContratoOcorrenciaNotificacao_Cumprido, P00K94_n301ContratoOcorrenciaNotificacao_Cumprido, P00K94_A299ContratoOcorrenciaNotificacao_Prazo, P00K94_A300ContratoOcorrenciaNotificacao_Descricao, P00K94_A77Contrato_Numero,
               P00K94_A304ContratoOcorrenciaNotificacao_Nome, P00K94_n304ContratoOcorrenciaNotificacao_Nome, P00K94_A298ContratoOcorrenciaNotificacao_Data, P00K94_A297ContratoOcorrenciaNotificacao_Codigo
               }
               , new Object[] {
               P00K95_A294ContratoOcorrencia_Codigo, P00K95_A74Contrato_Codigo, P00K95_A303ContratoOcorrenciaNotificacao_Responsavel, P00K95_A302ContratoOcorrenciaNotificacao_Protocolo, P00K95_n302ContratoOcorrenciaNotificacao_Protocolo, P00K95_A301ContratoOcorrenciaNotificacao_Cumprido, P00K95_n301ContratoOcorrenciaNotificacao_Cumprido, P00K95_A299ContratoOcorrenciaNotificacao_Prazo, P00K95_A300ContratoOcorrenciaNotificacao_Descricao, P00K95_A77Contrato_Numero,
               P00K95_A304ContratoOcorrenciaNotificacao_Nome, P00K95_n304ContratoOcorrenciaNotificacao_Nome, P00K95_A298ContratoOcorrenciaNotificacao_Data, P00K95_A297ContratoOcorrenciaNotificacao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TFContratoOcorrenciaNotificacao_Prazo ;
      private short AV17TFContratoOcorrenciaNotificacao_Prazo_To ;
      private short AV43DynamicFiltersOperator1 ;
      private short AV49DynamicFiltersOperator2 ;
      private short AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ;
      private short AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ;
      private short AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ;
      private short AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ;
      private short A299ContratoOcorrenciaNotificacao_Prazo ;
      private int AV55GXV1 ;
      private int A294ContratoOcorrencia_Codigo ;
      private int A74Contrato_Codigo ;
      private int A303ContratoOcorrenciaNotificacao_Responsavel ;
      private int A297ContratoOcorrenciaNotificacao_Codigo ;
      private int AV27InsertIndex ;
      private long AV36count ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String AV22TFContratoOcorrenciaNotificacao_Nome ;
      private String AV23TFContratoOcorrenciaNotificacao_Nome_Sel ;
      private String AV46ContratoOcorrenciaNotificacao_Nome1 ;
      private String AV52ContratoOcorrenciaNotificacao_Nome2 ;
      private String AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ;
      private String AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ;
      private String AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ;
      private String AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ;
      private String AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ;
      private String AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ;
      private String scmdbuf ;
      private String lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ;
      private String lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ;
      private String lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ;
      private String lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ;
      private String A304ContratoOcorrenciaNotificacao_Nome ;
      private String A77Contrato_Numero ;
      private DateTime AV12TFContratoOcorrenciaNotificacao_Data ;
      private DateTime AV13TFContratoOcorrenciaNotificacao_Data_To ;
      private DateTime AV18TFContratoOcorrenciaNotificacao_Cumprido ;
      private DateTime AV19TFContratoOcorrenciaNotificacao_Cumprido_To ;
      private DateTime AV44ContratoOcorrenciaNotificacao_Data1 ;
      private DateTime AV45ContratoOcorrenciaNotificacao_Data_To1 ;
      private DateTime AV50ContratoOcorrenciaNotificacao_Data2 ;
      private DateTime AV51ContratoOcorrenciaNotificacao_Data_To2 ;
      private DateTime AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ;
      private DateTime AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ;
      private DateTime AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ;
      private DateTime AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ;
      private DateTime AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ;
      private DateTime AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ;
      private DateTime AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ;
      private DateTime AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ;
      private DateTime A298ContratoOcorrenciaNotificacao_Data ;
      private DateTime A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool returnInSub ;
      private bool AV47DynamicFiltersEnabled2 ;
      private bool AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ;
      private bool BRKK92 ;
      private bool n302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool n301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool n304ContratoOcorrenciaNotificacao_Nome ;
      private bool BRKK94 ;
      private bool BRKK96 ;
      private bool BRKK98 ;
      private String AV35OptionIndexesJson ;
      private String AV30OptionsJson ;
      private String AV33OptionsDescJson ;
      private String AV26DDOName ;
      private String AV24SearchTxt ;
      private String AV25SearchTxtTo ;
      private String AV14TFContratoOcorrenciaNotificacao_Descricao ;
      private String AV15TFContratoOcorrenciaNotificacao_Descricao_Sel ;
      private String AV20TFContratoOcorrenciaNotificacao_Protocolo ;
      private String AV21TFContratoOcorrenciaNotificacao_Protocolo_Sel ;
      private String AV42DynamicFiltersSelector1 ;
      private String AV48DynamicFiltersSelector2 ;
      private String AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ;
      private String AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ;
      private String AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ;
      private String AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ;
      private String AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ;
      private String AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ;
      private String lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ;
      private String lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ;
      private String A300ContratoOcorrenciaNotificacao_Descricao ;
      private String A302ContratoOcorrenciaNotificacao_Protocolo ;
      private String AV28Option ;
      private IGxSession AV37Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00K92_A294ContratoOcorrencia_Codigo ;
      private int[] P00K92_A74Contrato_Codigo ;
      private int[] P00K92_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] P00K92_A77Contrato_Numero ;
      private String[] P00K92_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] P00K92_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] P00K92_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] P00K92_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private short[] P00K92_A299ContratoOcorrenciaNotificacao_Prazo ;
      private String[] P00K92_A300ContratoOcorrenciaNotificacao_Descricao ;
      private String[] P00K92_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] P00K92_n304ContratoOcorrenciaNotificacao_Nome ;
      private DateTime[] P00K92_A298ContratoOcorrenciaNotificacao_Data ;
      private int[] P00K92_A297ContratoOcorrenciaNotificacao_Codigo ;
      private int[] P00K93_A294ContratoOcorrencia_Codigo ;
      private int[] P00K93_A74Contrato_Codigo ;
      private int[] P00K93_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] P00K93_A300ContratoOcorrenciaNotificacao_Descricao ;
      private String[] P00K93_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] P00K93_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] P00K93_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] P00K93_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private short[] P00K93_A299ContratoOcorrenciaNotificacao_Prazo ;
      private String[] P00K93_A77Contrato_Numero ;
      private String[] P00K93_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] P00K93_n304ContratoOcorrenciaNotificacao_Nome ;
      private DateTime[] P00K93_A298ContratoOcorrenciaNotificacao_Data ;
      private int[] P00K93_A297ContratoOcorrenciaNotificacao_Codigo ;
      private int[] P00K94_A294ContratoOcorrencia_Codigo ;
      private int[] P00K94_A74Contrato_Codigo ;
      private int[] P00K94_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] P00K94_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] P00K94_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] P00K94_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] P00K94_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private short[] P00K94_A299ContratoOcorrenciaNotificacao_Prazo ;
      private String[] P00K94_A300ContratoOcorrenciaNotificacao_Descricao ;
      private String[] P00K94_A77Contrato_Numero ;
      private String[] P00K94_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] P00K94_n304ContratoOcorrenciaNotificacao_Nome ;
      private DateTime[] P00K94_A298ContratoOcorrenciaNotificacao_Data ;
      private int[] P00K94_A297ContratoOcorrenciaNotificacao_Codigo ;
      private int[] P00K95_A294ContratoOcorrencia_Codigo ;
      private int[] P00K95_A74Contrato_Codigo ;
      private int[] P00K95_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] P00K95_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] P00K95_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] P00K95_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] P00K95_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private short[] P00K95_A299ContratoOcorrenciaNotificacao_Prazo ;
      private String[] P00K95_A300ContratoOcorrenciaNotificacao_Descricao ;
      private String[] P00K95_A77Contrato_Numero ;
      private String[] P00K95_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] P00K95_n304ContratoOcorrenciaNotificacao_Nome ;
      private DateTime[] P00K95_A298ContratoOcorrenciaNotificacao_Data ;
      private int[] P00K95_A297ContratoOcorrenciaNotificacao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV39GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV40GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV41GridStateDynamicFilter ;
   }

   public class getwwcontratoocorrencianotificacaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00K92( IGxContext context ,
                                             String AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ,
                                             DateTime AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ,
                                             short AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ,
                                             bool AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ,
                                             String AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ,
                                             DateTime AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ,
                                             short AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ,
                                             String AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ,
                                             String AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ,
                                             String AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ,
                                             DateTime AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ,
                                             DateTime AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ,
                                             String AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ,
                                             String AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ,
                                             short AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ,
                                             short AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ,
                                             DateTime AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ,
                                             DateTime AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ,
                                             String AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ,
                                             String AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ,
                                             String AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ,
                                             String AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             String A77Contrato_Numero ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [22] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoOcorrencia_Codigo], T2.[Contrato_Codigo], T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T3.[Contrato_Numero], T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Prazo], T1.[ContratoOcorrenciaNotificacao_Descricao], T4.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Data], T1.[ContratoOcorrenciaNotificacao_Codigo] FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [ContratoOcorrencia] T2 WITH (NOLOCK) ON T2.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel])";
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] like @lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] = @AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00K93( IGxContext context ,
                                             String AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ,
                                             DateTime AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ,
                                             short AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ,
                                             bool AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ,
                                             String AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ,
                                             DateTime AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ,
                                             short AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ,
                                             String AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ,
                                             String AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ,
                                             String AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ,
                                             DateTime AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ,
                                             DateTime AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ,
                                             String AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ,
                                             String AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ,
                                             short AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ,
                                             short AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ,
                                             DateTime AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ,
                                             DateTime AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ,
                                             String AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ,
                                             String AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ,
                                             String AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ,
                                             String AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             String A77Contrato_Numero ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [22] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoOcorrencia_Codigo], T2.[Contrato_Codigo], T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Descricao], T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Prazo], T3.[Contrato_Numero], T4.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Data], T1.[ContratoOcorrenciaNotificacao_Codigo] FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [ContratoOcorrencia] T2 WITH (NOLOCK) ON T2.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel])";
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] like @lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] = @AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00K94( IGxContext context ,
                                             String AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ,
                                             DateTime AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ,
                                             short AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ,
                                             bool AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ,
                                             String AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ,
                                             DateTime AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ,
                                             short AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ,
                                             String AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ,
                                             String AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ,
                                             String AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ,
                                             DateTime AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ,
                                             DateTime AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ,
                                             String AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ,
                                             String AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ,
                                             short AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ,
                                             short AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ,
                                             DateTime AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ,
                                             DateTime AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ,
                                             String AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ,
                                             String AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ,
                                             String AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ,
                                             String AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             String A77Contrato_Numero ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [22] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoOcorrencia_Codigo], T2.[Contrato_Codigo], T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Prazo], T1.[ContratoOcorrenciaNotificacao_Descricao], T3.[Contrato_Numero], T4.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Data], T1.[ContratoOcorrenciaNotificacao_Codigo] FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [ContratoOcorrencia] T2 WITH (NOLOCK) ON T2.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel])";
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] like @lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] = @AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (0==AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Protocolo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00K95( IGxContext context ,
                                             String AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ,
                                             DateTime AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ,
                                             short AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ,
                                             bool AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ,
                                             String AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ,
                                             DateTime AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ,
                                             short AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ,
                                             String AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ,
                                             String AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ,
                                             String AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ,
                                             DateTime AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ,
                                             DateTime AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ,
                                             String AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ,
                                             String AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ,
                                             short AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ,
                                             short AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ,
                                             DateTime AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ,
                                             DateTime AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ,
                                             String AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ,
                                             String AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ,
                                             String AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ,
                                             String AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             String A77Contrato_Numero ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [22] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoOcorrencia_Codigo], T2.[Contrato_Codigo], T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Prazo], T1.[ContratoOcorrenciaNotificacao_Descricao], T3.[Contrato_Numero], T4.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Data], T1.[ContratoOcorrenciaNotificacao_Codigo] FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [ContratoOcorrencia] T2 WITH (NOLOCK) ON T2.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel])";
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV58WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV62WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV64WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] like @lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] = @AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ! (0==AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( ! (0==AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)";
            }
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)";
            }
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)";
            }
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Responsavel]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00K92(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] );
               case 1 :
                     return conditional_P00K93(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] );
               case 2 :
                     return conditional_P00K94(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] );
               case 3 :
                     return conditional_P00K95(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00K92 ;
          prmP00K92 = new Object[] {
          new Object[] {"@AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel",SqlDbType.Char,100,0}
          } ;
          Object[] prmP00K93 ;
          prmP00K93 = new Object[] {
          new Object[] {"@AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel",SqlDbType.Char,100,0}
          } ;
          Object[] prmP00K94 ;
          prmP00K94 = new Object[] {
          new Object[] {"@AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel",SqlDbType.Char,100,0}
          } ;
          Object[] prmP00K95 ;
          prmP00K95 = new Object[] {
          new Object[] {"@AV59WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV60WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV61WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV65WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV66WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV67WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV69WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV70WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV71WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV72WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV73WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV74WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV75WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV76WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV77WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV78WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV79WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@lV80WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV81WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00K92", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K92,100,0,true,false )
             ,new CursorDef("P00K93", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K93,100,0,true,false )
             ,new CursorDef("P00K94", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K94,100,0,true,false )
             ,new CursorDef("P00K95", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K95,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((short[]) buf[8])[0] = rslt.getShort(7) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[10])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(10) ;
                ((int[]) buf[13])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((short[]) buf[8])[0] = rslt.getShort(7) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
                ((String[]) buf[10])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(10) ;
                ((int[]) buf[13])[0] = rslt.getInt(11) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
                ((String[]) buf[10])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(10) ;
                ((int[]) buf[13])[0] = rslt.getInt(11) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
                ((String[]) buf[10])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(10) ;
                ((int[]) buf[13])[0] = rslt.getInt(11) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratoocorrencianotificacaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratoocorrencianotificacaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratoocorrencianotificacaofilterdata") )
          {
             return  ;
          }
          getwwcontratoocorrencianotificacaofilterdata worker = new getwwcontratoocorrencianotificacaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
