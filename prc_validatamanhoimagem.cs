/*
               File: PRC_ValidaTamanhoImagem
        Description: Verifica o Tamanho e dimens�es da imagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:18.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_validatamanhoimagem : GXProcedure
   {
      public prc_validatamanhoimagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_validatamanhoimagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_Imagem ,
                           out String aP1_IsConfiguracoesValidas )
      {
         this.AV12Imagem = aP0_Imagem;
         this.AV9IsConfiguracoesValidas = "" ;
         initialize();
         executePrivate();
         aP1_IsConfiguracoesValidas=this.AV9IsConfiguracoesValidas;
      }

      public String executeUdp( String aP0_Imagem )
      {
         this.AV12Imagem = aP0_Imagem;
         this.AV9IsConfiguracoesValidas = "" ;
         initialize();
         executePrivate();
         aP1_IsConfiguracoesValidas=this.AV9IsConfiguracoesValidas;
         return AV9IsConfiguracoesValidas ;
      }

      public void executeSubmit( String aP0_Imagem ,
                                 out String aP1_IsConfiguracoesValidas )
      {
         prc_validatamanhoimagem objprc_validatamanhoimagem;
         objprc_validatamanhoimagem = new prc_validatamanhoimagem();
         objprc_validatamanhoimagem.AV12Imagem = aP0_Imagem;
         objprc_validatamanhoimagem.AV9IsConfiguracoesValidas = "" ;
         objprc_validatamanhoimagem.context.SetSubmitInitialConfig(context);
         objprc_validatamanhoimagem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_validatamanhoimagem);
         aP1_IsConfiguracoesValidas=this.AV9IsConfiguracoesValidas;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_validatamanhoimagem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9IsConfiguracoesValidas = "";
         AV10IgImageEditor.open( AV12Imagem);
         if ( AV10IgImageEditor.gxTpr_Isopen )
         {
            AV13GetFormat = AV10IgImageEditor.gxTpr_Getformat;
            if ( ( StringUtil.StrCmp(AV10IgImageEditor.gxTpr_Getformat, "JPG") != 0 ) && ( StringUtil.StrCmp(AV10IgImageEditor.gxTpr_Getformat, "PNG") != 0 ) && ( StringUtil.StrCmp(AV10IgImageEditor.gxTpr_Getformat, "JPEG") != 0 ) )
            {
               AV9IsConfiguracoesValidas = "A Logomarca deve ser do Tipo JPG/PNG.";
            }
            if ( ( AV10IgImageEditor.gxTpr_Getheight > 125 ) || ( AV10IgImageEditor.gxTpr_Getwidth > 125 ) )
            {
               AV9IsConfiguracoesValidas = AV9IsConfiguracoesValidas + StringUtil.NewLine( ) + (String.IsNullOrEmpty(StringUtil.RTrim( AV9IsConfiguracoesValidas)) ? " As dimen��es devem ser no m�ximo 125x125 pixels. Verifique!" : "As dimen��es devem ser no m�ximo 125x125 pixels. Verifique!");
            }
         }
         else
         {
            AV9IsConfiguracoesValidas = "Imagem n�o carregada.";
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV10IgImageEditor = new SdtIgImageEditor(context);
         AV13GetFormat = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV9IsConfiguracoesValidas ;
      private String AV13GetFormat ;
      private String AV12Imagem ;
      private String aP1_IsConfiguracoesValidas ;
      private SdtIgImageEditor AV10IgImageEditor ;
   }

}
