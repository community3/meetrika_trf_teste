/*
               File: type_SdtQueryViewerAxes_Axis_Format_ValueStyle
        Description: QueryViewerAxes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:57.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "QueryViewerAxes.Axis.Format.ValueStyle" )]
   [XmlType(TypeName =  "QueryViewerAxes.Axis.Format.ValueStyle" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtQueryViewerAxes_Axis_Format_ValueStyle : GxUserType
   {
      public SdtQueryViewerAxes_Axis_Format_ValueStyle( )
      {
         /* Constructor for serialization */
         gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Value = "";
         gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Styleorclass = "";
      }

      public SdtQueryViewerAxes_Axis_Format_ValueStyle( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtQueryViewerAxes_Axis_Format_ValueStyle deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtQueryViewerAxes_Axis_Format_ValueStyle)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtQueryViewerAxes_Axis_Format_ValueStyle obj ;
         obj = this;
         obj.gxTpr_Value = deserialized.gxTpr_Value;
         obj.gxTpr_Styleorclass = deserialized.gxTpr_Styleorclass;
         obj.gxTpr_Applytoroworcolumn = deserialized.gxTpr_Applytoroworcolumn;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Value") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Value = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "StyleOrClass") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Styleorclass = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ApplyToRowOrColumn") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Applytoroworcolumn = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "QueryViewerAxes.Axis.Format.ValueStyle";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Value", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Value));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("StyleOrClass", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Styleorclass));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ApplyToRowOrColumn", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Applytoroworcolumn)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Value", gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Value, false);
         AddObjectProperty("StyleOrClass", gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Styleorclass, false);
         AddObjectProperty("ApplyToRowOrColumn", gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Applytoroworcolumn, false);
         return  ;
      }

      [  SoapElement( ElementName = "Value" )]
      [  XmlElement( ElementName = "Value"   )]
      public String gxTpr_Value
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Value ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Value = (String)(value);
         }

      }

      [  SoapElement( ElementName = "StyleOrClass" )]
      [  XmlElement( ElementName = "StyleOrClass"   )]
      public String gxTpr_Styleorclass
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Styleorclass ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Styleorclass = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ApplyToRowOrColumn" )]
      [  XmlElement( ElementName = "ApplyToRowOrColumn"   )]
      public bool gxTpr_Applytoroworcolumn
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Applytoroworcolumn ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Applytoroworcolumn = value;
         }

      }

      public void initialize( )
      {
         gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Value = "";
         gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Styleorclass = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Value ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Styleorclass ;
      protected String sTagName ;
      protected bool gxTv_SdtQueryViewerAxes_Axis_Format_ValueStyle_Applytoroworcolumn ;
   }

   [DataContract(Name = @"QueryViewerAxes.Axis.Format.ValueStyle", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtQueryViewerAxes_Axis_Format_ValueStyle_RESTInterface : GxGenericCollectionItem<SdtQueryViewerAxes_Axis_Format_ValueStyle>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtQueryViewerAxes_Axis_Format_ValueStyle_RESTInterface( ) : base()
      {
      }

      public SdtQueryViewerAxes_Axis_Format_ValueStyle_RESTInterface( SdtQueryViewerAxes_Axis_Format_ValueStyle psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Value" , Order = 0 )]
      public String gxTpr_Value
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Value) ;
         }

         set {
            sdt.gxTpr_Value = (String)(value);
         }

      }

      [DataMember( Name = "StyleOrClass" , Order = 1 )]
      public String gxTpr_Styleorclass
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Styleorclass) ;
         }

         set {
            sdt.gxTpr_Styleorclass = (String)(value);
         }

      }

      [DataMember( Name = "ApplyToRowOrColumn" , Order = 2 )]
      public bool gxTpr_Applytoroworcolumn
      {
         get {
            return sdt.gxTpr_Applytoroworcolumn ;
         }

         set {
            sdt.gxTpr_Applytoroworcolumn = value;
         }

      }

      public SdtQueryViewerAxes_Axis_Format_ValueStyle sdt
      {
         get {
            return (SdtQueryViewerAxes_Axis_Format_ValueStyle)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtQueryViewerAxes_Axis_Format_ValueStyle() ;
         }
      }

   }

}
