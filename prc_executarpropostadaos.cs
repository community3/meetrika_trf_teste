/*
               File: PRC_ExecutarPropostaDaOS
        Description: Stub for PRC_ExecutarPropostaDaOS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:16:54.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_executarpropostadaos : GXProcedure
   {
      public prc_executarpropostadaos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public prc_executarpropostadaos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Proposta_OSCodigo ,
                           int aP1_Contratada_Codigo ,
                           int aP2_Owner ,
                           DateTime aP3_DataInicio ,
                           ref DateTime aP4_DataFim )
      {
         this.AV2Proposta_OSCodigo = aP0_Proposta_OSCodigo;
         this.AV3Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV4Owner = aP2_Owner;
         this.AV5DataInicio = aP3_DataInicio;
         this.AV6DataFim = aP4_DataFim;
         initialize();
         executePrivate();
         aP0_Proposta_OSCodigo=this.AV2Proposta_OSCodigo;
         aP4_DataFim=this.AV6DataFim;
      }

      public DateTime executeUdp( ref int aP0_Proposta_OSCodigo ,
                                  int aP1_Contratada_Codigo ,
                                  int aP2_Owner ,
                                  DateTime aP3_DataInicio )
      {
         this.AV2Proposta_OSCodigo = aP0_Proposta_OSCodigo;
         this.AV3Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV4Owner = aP2_Owner;
         this.AV5DataInicio = aP3_DataInicio;
         this.AV6DataFim = aP4_DataFim;
         initialize();
         executePrivate();
         aP0_Proposta_OSCodigo=this.AV2Proposta_OSCodigo;
         aP4_DataFim=this.AV6DataFim;
         return AV6DataFim ;
      }

      public void executeSubmit( ref int aP0_Proposta_OSCodigo ,
                                 int aP1_Contratada_Codigo ,
                                 int aP2_Owner ,
                                 DateTime aP3_DataInicio ,
                                 ref DateTime aP4_DataFim )
      {
         prc_executarpropostadaos objprc_executarpropostadaos;
         objprc_executarpropostadaos = new prc_executarpropostadaos();
         objprc_executarpropostadaos.AV2Proposta_OSCodigo = aP0_Proposta_OSCodigo;
         objprc_executarpropostadaos.AV3Contratada_Codigo = aP1_Contratada_Codigo;
         objprc_executarpropostadaos.AV4Owner = aP2_Owner;
         objprc_executarpropostadaos.AV5DataInicio = aP3_DataInicio;
         objprc_executarpropostadaos.AV6DataFim = aP4_DataFim;
         objprc_executarpropostadaos.context.SetSubmitInitialConfig(context);
         objprc_executarpropostadaos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_executarpropostadaos);
         aP0_Proposta_OSCodigo=this.AV2Proposta_OSCodigo;
         aP4_DataFim=this.AV6DataFim;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_executarpropostadaos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2Proposta_OSCodigo,(int)AV3Contratada_Codigo,(int)AV4Owner,(DateTime)AV5DataInicio,(DateTime)AV6DataFim} ;
         ClassLoader.Execute("aprc_executarpropostadaos","GeneXus.Programs.aprc_executarpropostadaos", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 5 ) )
         {
            AV2Proposta_OSCodigo = (int)(args[0]) ;
            AV6DataFim = (DateTime)(args[4]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV2Proposta_OSCodigo ;
      private int AV3Contratada_Codigo ;
      private int AV4Owner ;
      private DateTime AV5DataInicio ;
      private DateTime AV6DataFim ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Proposta_OSCodigo ;
      private DateTime aP4_DataFim ;
      private Object[] args ;
   }

}
