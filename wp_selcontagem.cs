/*
               File: WP_SelContagem
        Description: Planilha de Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:28:39.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_selcontagem : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_selcontagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_selcontagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out bool aP0_Confirmado ,
                           String aP1_Demanda )
      {
         this.AV15Confirmado = false ;
         this.AV30Demanda = aP1_Demanda;
         executePrivate();
         aP0_Confirmado=this.AV15Confirmado;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV15Confirmado = (bool)(BooleanUtil.Val(gxfirstwebparm));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Confirmado", AV15Confirmado);
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV30Demanda = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Demanda", AV30Demanda);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV30Demanda, "@!"))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAF32( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTF32( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823283965");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_selcontagem.aspx") + "?" + UrlEncode(StringUtil.BoolToStr(AV15Confirmado)) + "," + UrlEncode(StringUtil.RTrim(AV30Demanda))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vDEMANDA", AV30Demanda);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_PLANILHA", AV10Sdt_Planilha);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_PLANILHA", AV10Sdt_Planilha);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_CONTAGENS", AV11Sdt_Contagens);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_CONTAGENS", AV11Sdt_Contagens);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vCONFIRMADO", AV15Confirmado);
         GxWebStd.gx_hidden_field( context, "gxhash_vDEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV30Demanda, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vDEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV30Demanda, "@!"))));
         GXCCtlgxBlob = "vBLOB" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV5Blob);
         GxWebStd.gx_hidden_field( context, "DVCONFIRMPANEL_Width", StringUtil.RTrim( Dvconfirmpanel_Width));
         GxWebStd.gx_hidden_field( context, "DVCONFIRMPANEL_Height", StringUtil.RTrim( Dvconfirmpanel_Height));
         GxWebStd.gx_hidden_field( context, "DVCONFIRMPANEL_Title", StringUtil.RTrim( Dvconfirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "DVCONFIRMPANEL_Confirmationtext", StringUtil.RTrim( Dvconfirmpanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "DVCONFIRMPANEL_Yesbuttoncaption", StringUtil.RTrim( Dvconfirmpanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "DVCONFIRMPANEL_Nobuttoncaption", StringUtil.RTrim( Dvconfirmpanel_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, "DVCONFIRMPANEL_Result", StringUtil.RTrim( Dvconfirmpanel_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEF32( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTF32( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_selcontagem.aspx") + "?" + UrlEncode(StringUtil.BoolToStr(AV15Confirmado)) + "," + UrlEncode(StringUtil.RTrim(AV30Demanda)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_SelContagem" ;
      }

      public override String GetPgmdesc( )
      {
         return "Planilha de Contagem" ;
      }

      protected void WBF30( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_F32( true) ;
         }
         else
         {
            wb_table1_2_F32( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_F32e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVCONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVCONFIRMPANELContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
         }
         wbLoad = true;
      }

      protected void STARTF32( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Planilha de Contagem", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPF30( ) ;
      }

      protected void WSF32( )
      {
         STARTF32( ) ;
         EVTF32( ) ;
      }

      protected void EVTF32( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DVCONFIRMPANEL.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11F32 */
                              E11F32 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12F32 */
                              E12F32 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ENTER'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13F32 */
                              E13F32 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14F32 */
                              E14F32 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E13F32 */
                                    E13F32 ();
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEF32( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAF32( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavBlob_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFF32( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFF32( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14F32 */
            E14F32 ();
            WBF30( ) ;
         }
      }

      protected void STRUPF30( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12F32 */
         E12F32 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV5Blob = cgiGet( edtavBlob_Internalname);
            AV9FileName = cgiGet( edtavFilename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9FileName", AV9FileName);
            /* Read saved values. */
            Dvconfirmpanel_Width = cgiGet( "DVCONFIRMPANEL_Width");
            Dvconfirmpanel_Height = cgiGet( "DVCONFIRMPANEL_Height");
            Dvconfirmpanel_Title = cgiGet( "DVCONFIRMPANEL_Title");
            Dvconfirmpanel_Confirmationtext = cgiGet( "DVCONFIRMPANEL_Confirmationtext");
            Dvconfirmpanel_Yesbuttoncaption = cgiGet( "DVCONFIRMPANEL_Yesbuttoncaption");
            Dvconfirmpanel_Nobuttoncaption = cgiGet( "DVCONFIRMPANEL_Nobuttoncaption");
            Dvconfirmpanel_Result = cgiGet( "DVCONFIRMPANEL_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) )
            {
               GXCCtlgxBlob = "vBLOB" + "_gxBlob";
               AV5Blob = cgiGet( GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12F32 */
         E12F32 ();
         if (returnInSub) return;
      }

      protected void E12F32( )
      {
         /* Start Routine */
         Form.Jscriptsrc.Add("http://code.jquery.com/jquery-latest.js\">") ;
         Form.Headerrawhtml = "<script type=\"text/javascript\">";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(document).ready(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vBLOB\").blur(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"var nome = $(\"#vBLOB\").val();";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vFILENAME\").val(nome);";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"</script>";
         Form.Caption = "Planilha de Contagem da OS "+StringUtil.Trim( AV30Demanda)+":";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtavBlob_Display = 1;
         AV11Sdt_Contagens.Clear();
      }

      protected void E13F32( )
      {
         /* 'Enter' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV9FileName)) )
         {
            GX_msglist.addItem("Selecione a planilha da contagem!");
         }
         else if ( StringUtil.StringSearch( AV9FileName, StringUtil.Trim( AV30Demanda), 1) == 0 )
         {
            this.executeUsercontrolMethod("", false, "DVCONFIRMPANELContainer", "Confirm", "", new Object[] {});
         }
         else
         {
            /* Execute user subroutine: 'SAVEPLANILHA' */
            S112 ();
            if (returnInSub) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10Sdt_Planilha", AV10Sdt_Planilha);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11Sdt_Contagens", AV11Sdt_Contagens);
      }

      protected void E11F32( )
      {
         /* Dvconfirmpanel_Close Routine */
         if ( StringUtil.StrCmp(Dvconfirmpanel_Result, "Yes") == 0 )
         {
            /* Execute user subroutine: 'SAVEPLANILHA' */
            S112 ();
            if (returnInSub) return;
         }
         else
         {
            AV5Blob = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV5Blob));
            AV9FileName = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9FileName", AV9FileName);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10Sdt_Planilha", AV10Sdt_Planilha);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11Sdt_Contagens", AV11Sdt_Contagens);
      }

      protected void S112( )
      {
         /* 'SAVEPLANILHA' Routine */
         AV9FileName = StringUtil.Substring( AV9FileName, StringUtil.StringSearchRev( AV9FileName, "\\", -1)+1, 255);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9FileName", AV9FileName);
         AV7Ext = StringUtil.Trim( StringUtil.Substring( AV9FileName, StringUtil.StringSearchRev( AV9FileName, ".", -1)+1, 4));
         AV9FileName = StringUtil.Substring( AV9FileName, 1, StringUtil.StringSearchRev( AV9FileName, ".", -1)-1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9FileName", AV9FileName);
         AV10Sdt_Planilha.gxTpr_Contagemresultadoevidencia_arquivo = AV5Blob;
         AV10Sdt_Planilha.gxTpr_Contagemresultadoevidencia_nomearq = AV9FileName;
         AV10Sdt_Planilha.gxTpr_Contagemresultadoevidencia_tipoarq = AV7Ext;
         AV11Sdt_Contagens.Add(AV10Sdt_Planilha, 0);
         AV12Websession.Set("PlanilhaContagem", AV11Sdt_Contagens.ToXml(false, true, "SDT_ContagemResultadoEvidencias", "GxEv3Up14_MeetrikaVs3"));
         AV15Confirmado = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Confirmado", AV15Confirmado);
         context.setWebReturnParms(new Object[] {(bool)AV15Confirmado});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E14F32( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_F32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(300), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable_Internalname, tblTable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:80%")+"\" class='Table'>") ;
            context.WriteHtmlText( "<p>") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 5,'',false,'',0)\"";
            edtavBlob_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) )
            {
               gxblobfileaux.Source = AV5Blob;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavBlob_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavBlob_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV5Blob = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV5Blob));
                  edtavBlob_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV5Blob));
            }
            GxWebStd.gx_blob_field( context, edtavBlob_Internalname, StringUtil.RTrim( AV5Blob), context.PathToRelativeUrl( AV5Blob), (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Filetype)) ? AV5Blob : edtavBlob_Filetype)) : edtavBlob_Contenttype), false, "", edtavBlob_Parameters, edtavBlob_Display, 1, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtavBlob_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,5);\"", "", "", "HLP_WP_SelContagem.htm");
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "<p>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 6,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilename_Internalname, StringUtil.RTrim( AV9FileName), StringUtil.RTrim( context.localUtil.Format( AV9FileName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,6);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilename_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:1.0pt; font-weight:normal; font-style:normal; color:#00FFFFFF; background-color:#00FFFFFF;", "", "", 1, edtavFilename_Enabled, 0, "text", "", 615, "px", 1, "px", 255, 0, 0, 0, 1, 0, -1, true, "", "left", true, "HLP_WP_SelContagem.htm");
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:20%")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttConfirm_Internalname, "", "Confirmar", bttConfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ENTER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SelContagem.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttCancel_Internalname, "", "Fechar", bttCancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SelContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_F32e( true) ;
         }
         else
         {
            wb_table1_2_F32e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV15Confirmado = (bool)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Confirmado", AV15Confirmado);
         AV30Demanda = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Demanda", AV30Demanda);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV30Demanda, "@!"))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAF32( ) ;
         WSF32( ) ;
         WEF32( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823283989");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_selcontagem.js", "?202042823283989");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         edtavBlob_Internalname = "vBLOB";
         edtavFilename_Internalname = "vFILENAME";
         bttConfirm_Internalname = "CONFIRM";
         bttCancel_Internalname = "CANCEL";
         tblTable_Internalname = "TABLE";
         Dvconfirmpanel_Internalname = "DVCONFIRMPANEL";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavFilename_Jsonclick = "";
         edtavFilename_Backstyle = -1;
         edtavFilename_Backcolor = (int)(0x00FFFFFF);
         edtavFilename_Enabled = 1;
         edtavBlob_Jsonclick = "";
         edtavBlob_Parameters = "";
         edtavBlob_Contenttype = "";
         edtavBlob_Filetype = "";
         edtavBlob_Display = 0;
         Dvconfirmpanel_Nobuttoncaption = "N�o";
         Dvconfirmpanel_Yesbuttoncaption = "Sim";
         Dvconfirmpanel_Confirmationtext = "Deseja anexar a planilha selecionada?";
         Dvconfirmpanel_Title = "O nome da planilha n�o cont�m o n�mero da OS!";
         Dvconfirmpanel_Height = "110";
         Dvconfirmpanel_Width = "310";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Planilha de Contagem";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'ENTER'","{handler:'E13F32',iparms:[{av:'AV9FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV30Demanda',fld:'vDEMANDA',pic:'@!',hsh:true,nv:''},{av:'AV5Blob',fld:'vBLOB',pic:'',nv:''},{av:'AV10Sdt_Planilha',fld:'vSDT_PLANILHA',pic:'',nv:null},{av:'AV11Sdt_Contagens',fld:'vSDT_CONTAGENS',pic:'',nv:null}],oparms:[{av:'AV9FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV10Sdt_Planilha',fld:'vSDT_PLANILHA',pic:'',nv:null},{av:'AV11Sdt_Contagens',fld:'vSDT_CONTAGENS',pic:'',nv:null},{av:'AV15Confirmado',fld:'vCONFIRMADO',pic:'',nv:false}]}");
         setEventMetadata("DVCONFIRMPANEL.CLOSE","{handler:'E11F32',iparms:[{av:'Dvconfirmpanel_Result',ctrl:'DVCONFIRMPANEL',prop:'Result'},{av:'AV9FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV5Blob',fld:'vBLOB',pic:'',nv:''},{av:'AV10Sdt_Planilha',fld:'vSDT_PLANILHA',pic:'',nv:null},{av:'AV11Sdt_Contagens',fld:'vSDT_CONTAGENS',pic:'',nv:null}],oparms:[{av:'AV5Blob',fld:'vBLOB',pic:'',nv:''},{av:'AV9FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV10Sdt_Planilha',fld:'vSDT_PLANILHA',pic:'',nv:null},{av:'AV11Sdt_Contagens',fld:'vSDT_CONTAGENS',pic:'',nv:null},{av:'AV15Confirmado',fld:'vCONFIRMADO',pic:'',nv:false}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV30Demanda = "";
         Dvconfirmpanel_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV10Sdt_Planilha = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         AV11Sdt_Contagens = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         GXCCtlgxBlob = "";
         AV5Blob = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9FileName = "";
         AV7Ext = "";
         AV12Websession = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         bttConfirm_Jsonclick = "";
         bttCancel_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short edtavBlob_Display ;
      private short nGXWrapped ;
      private short edtavFilename_Backstyle ;
      private int edtavFilename_Enabled ;
      private int idxLst ;
      private int edtavFilename_Backcolor ;
      private String Dvconfirmpanel_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String Dvconfirmpanel_Width ;
      private String Dvconfirmpanel_Height ;
      private String Dvconfirmpanel_Title ;
      private String Dvconfirmpanel_Confirmationtext ;
      private String Dvconfirmpanel_Yesbuttoncaption ;
      private String Dvconfirmpanel_Nobuttoncaption ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavBlob_Internalname ;
      private String AV9FileName ;
      private String edtavFilename_Internalname ;
      private String AV7Ext ;
      private String sStyleString ;
      private String tblTable_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String edtavBlob_Filetype ;
      private String edtavBlob_Contenttype ;
      private String edtavBlob_Parameters ;
      private String edtavBlob_Jsonclick ;
      private String edtavFilename_Jsonclick ;
      private String bttConfirm_Internalname ;
      private String bttConfirm_Jsonclick ;
      private String bttCancel_Internalname ;
      private String bttCancel_Jsonclick ;
      private String Dvconfirmpanel_Internalname ;
      private bool entryPointCalled ;
      private bool AV15Confirmado ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV30Demanda ;
      private String wcpOAV30Demanda ;
      private String AV5Blob ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private bool aP0_Confirmado ;
      private IGxSession AV12Websession ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV11Sdt_Contagens ;
      private GXWebForm Form ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV10Sdt_Planilha ;
   }

}
