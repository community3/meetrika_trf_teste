/*
               File: PromptContratoServicosTelas
        Description: Selecione Contrato Servicos Telas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:39:4.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratoservicostelas : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratoservicostelas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratoservicostelas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratoServicosTelas_ContratoCod ,
                           ref short aP1_InOutContratoServicosTelas_Sequencial ,
                           ref int aP2_InOutContratoServicosTelas_ServicoCod )
      {
         this.AV7InOutContratoServicosTelas_ContratoCod = aP0_InOutContratoServicosTelas_ContratoCod;
         this.AV31InOutContratoServicosTelas_Sequencial = aP1_InOutContratoServicosTelas_Sequencial;
         this.AV8InOutContratoServicosTelas_ServicoCod = aP2_InOutContratoServicosTelas_ServicoCod;
         executePrivate();
         aP0_InOutContratoServicosTelas_ContratoCod=this.AV7InOutContratoServicosTelas_ContratoCod;
         aP1_InOutContratoServicosTelas_Sequencial=this.AV31InOutContratoServicosTelas_Sequencial;
         aP2_InOutContratoServicosTelas_ServicoCod=this.AV8InOutContratoServicosTelas_ServicoCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         dynavContratoservicostelas_servicocod1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         dynavContratoservicostelas_servicocod2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         dynavContratoservicostelas_servicocod3 = new GXCombobox();
         cmbContratoServicosTelas_Status = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_80 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_80_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_80_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoServicosTelas_ServicoCod1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosTelas_ServicoCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0)));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21ContratoServicosTelas_ServicoCod2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosTelas_ServicoCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0)));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25ContratoServicosTelas_ServicoCod3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosTelas_ServicoCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0)));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV33TFContratada_PessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratada_PessoaNom", AV33TFContratada_PessoaNom);
               AV34TFContratada_PessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratada_PessoaNom_Sel", AV34TFContratada_PessoaNom_Sel);
               AV37TFContratoServicosTelas_ServicoSigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosTelas_ServicoSigla", AV37TFContratoServicosTelas_ServicoSigla);
               AV38TFContratoServicosTelas_ServicoSigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoServicosTelas_ServicoSigla_Sel", AV38TFContratoServicosTelas_ServicoSigla_Sel);
               AV41TFContratoServicosTelas_Tela = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosTelas_Tela", AV41TFContratoServicosTelas_Tela);
               AV42TFContratoServicosTelas_Tela_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoServicosTelas_Tela_Sel", AV42TFContratoServicosTelas_Tela_Sel);
               AV45TFContratoServicosTelas_Link = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosTelas_Link", AV45TFContratoServicosTelas_Link);
               AV46TFContratoServicosTelas_Link_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContratoServicosTelas_Link_Sel", AV46TFContratoServicosTelas_Link_Sel);
               AV49TFContratoServicosTelas_Parms = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicosTelas_Parms", AV49TFContratoServicosTelas_Parms);
               AV50TFContratoServicosTelas_Parms_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoServicosTelas_Parms_Sel", AV50TFContratoServicosTelas_Parms_Sel);
               AV35ddo_Contratada_PessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ddo_Contratada_PessoaNomTitleControlIdToReplace", AV35ddo_Contratada_PessoaNomTitleControlIdToReplace);
               AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace", AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace);
               AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace", AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace);
               AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace", AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace);
               AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace", AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace);
               AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace", AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV54TFContratoServicosTelas_Status_Sels);
               AV63Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosTelas_ServicoCod1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosTelas_ServicoCod2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosTelas_ServicoCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV33TFContratada_PessoaNom, AV34TFContratada_PessoaNom_Sel, AV37TFContratoServicosTelas_ServicoSigla, AV38TFContratoServicosTelas_ServicoSigla_Sel, AV41TFContratoServicosTelas_Tela, AV42TFContratoServicosTelas_Tela_Sel, AV45TFContratoServicosTelas_Link, AV46TFContratoServicosTelas_Link_Sel, AV49TFContratoServicosTelas_Parms, AV50TFContratoServicosTelas_Parms_Sel, AV35ddo_Contratada_PessoaNomTitleControlIdToReplace, AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace, AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV54TFContratoServicosTelas_Status_Sels, AV63Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PromptContratoServicosTelas";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A938ContratoServicosTelas_Sequencial), "ZZ9");
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("promptcontratoservicostelas:[SendSecurityCheck value for]"+"ContratoServicosTelas_Sequencial:"+context.localUtil.Format( (decimal)(A938ContratoServicosTelas_Sequencial), "ZZ9"));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratoServicosTelas_ContratoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoServicosTelas_ContratoCod), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV31InOutContratoServicosTelas_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31InOutContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31InOutContratoServicosTelas_Sequencial), 3, 0)));
                  AV8InOutContratoServicosTelas_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoServicosTelas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContratoServicosTelas_ServicoCod), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAG82( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV63Pgmname = "PromptContratoServicosTelas";
               context.Gx_err = 0;
               WSG82( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEG82( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020529939430");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratoservicostelas.aspx") + "?" + UrlEncode("" +AV7InOutContratoServicosTelas_ContratoCod) + "," + UrlEncode("" +AV31InOutContratoServicosTelas_Sequencial) + "," + UrlEncode("" +AV8InOutContratoServicosTelas_ServicoCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSTELAS_SERVICOCOD1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSTELAS_SERVICOCOD2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSTELAS_SERVICOCOD3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM", StringUtil.RTrim( AV33TFContratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM_SEL", StringUtil.RTrim( AV34TFContratada_PessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA", StringUtil.RTrim( AV37TFContratoServicosTelas_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL", StringUtil.RTrim( AV38TFContratoServicosTelas_ServicoSigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSTELAS_TELA", StringUtil.RTrim( AV41TFContratoServicosTelas_Tela));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSTELAS_TELA_SEL", StringUtil.RTrim( AV42TFContratoServicosTelas_Tela_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSTELAS_LINK", AV45TFContratoServicosTelas_Link);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSTELAS_LINK_SEL", AV46TFContratoServicosTelas_Link_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSTELAS_PARMS", AV49TFContratoServicosTelas_Parms);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSTELAS_PARMS_SEL", AV50TFContratoServicosTelas_Parms_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_80", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_80), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV56DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV56DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV32Contratada_PessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV32Contratada_PessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSTELAS_SERVICOSIGLATITLEFILTERDATA", AV36ContratoServicosTelas_ServicoSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSTELAS_SERVICOSIGLATITLEFILTERDATA", AV36ContratoServicosTelas_ServicoSiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSTELAS_TELATITLEFILTERDATA", AV40ContratoServicosTelas_TelaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSTELAS_TELATITLEFILTERDATA", AV40ContratoServicosTelas_TelaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSTELAS_LINKTITLEFILTERDATA", AV44ContratoServicosTelas_LinkTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSTELAS_LINKTITLEFILTERDATA", AV44ContratoServicosTelas_LinkTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSTELAS_PARMSTITLEFILTERDATA", AV48ContratoServicosTelas_ParmsTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSTELAS_PARMSTITLEFILTERDATA", AV48ContratoServicosTelas_ParmsTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSTELAS_STATUSTITLEFILTERDATA", AV52ContratoServicosTelas_StatusTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSTELAS_STATUSTITLEFILTERDATA", AV52ContratoServicosTelas_StatusTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFCONTRATOSERVICOSTELAS_STATUS_SELS", AV54TFContratoServicosTelas_Status_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFCONTRATOSERVICOSTELAS_STATUS_SELS", AV54TFContratoServicosTelas_Status_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV63Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOSERVICOSTELAS_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratoServicosTelas_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOSERVICOSTELAS_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31InOutContratoServicosTelas_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOSERVICOSTELAS_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8InOutContratoServicosTelas_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Caption", StringUtil.RTrim( Ddo_contratada_pessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cls", StringUtil.RTrim( Ddo_contratada_pessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Caption", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Tooltip", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Cls", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicostelas_servicosigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicostelas_servicosigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicostelas_servicosigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Filtertype", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicostelas_servicosigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicostelas_servicosigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Datalisttype", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Datalistproc", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicostelas_servicosigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Sortasc", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Sortdsc", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Loadingdata", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Caption", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Tooltip", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Cls", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicostelas_tela_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicostelas_tela_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicostelas_tela_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Filtertype", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicostelas_tela_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicostelas_tela_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Datalisttype", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Datalistproc", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicostelas_tela_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Sortasc", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Sortdsc", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Loadingdata", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Caption", StringUtil.RTrim( Ddo_contratoservicostelas_link_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Tooltip", StringUtil.RTrim( Ddo_contratoservicostelas_link_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Cls", StringUtil.RTrim( Ddo_contratoservicostelas_link_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicostelas_link_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicostelas_link_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicostelas_link_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicostelas_link_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicostelas_link_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicostelas_link_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicostelas_link_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicostelas_link_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Filtertype", StringUtil.RTrim( Ddo_contratoservicostelas_link_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicostelas_link_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicostelas_link_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Datalisttype", StringUtil.RTrim( Ddo_contratoservicostelas_link_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Datalistproc", StringUtil.RTrim( Ddo_contratoservicostelas_link_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicostelas_link_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Sortasc", StringUtil.RTrim( Ddo_contratoservicostelas_link_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Sortdsc", StringUtil.RTrim( Ddo_contratoservicostelas_link_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Loadingdata", StringUtil.RTrim( Ddo_contratoservicostelas_link_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicostelas_link_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicostelas_link_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicostelas_link_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Caption", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Tooltip", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Cls", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicostelas_parms_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicostelas_parms_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicostelas_parms_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Filtertype", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicostelas_parms_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicostelas_parms_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Datalisttype", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Datalistproc", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicostelas_parms_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Sortasc", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Sortdsc", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Loadingdata", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Caption", StringUtil.RTrim( Ddo_contratoservicostelas_status_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Tooltip", StringUtil.RTrim( Ddo_contratoservicostelas_status_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Cls", StringUtil.RTrim( Ddo_contratoservicostelas_status_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicostelas_status_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicostelas_status_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicostelas_status_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicostelas_status_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicostelas_status_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicostelas_status_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicostelas_status_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicostelas_status_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Datalisttype", StringUtil.RTrim( Ddo_contratoservicostelas_status_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contratoservicostelas_status_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoservicostelas_status_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Sortasc", StringUtil.RTrim( Ddo_contratoservicostelas_status_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Sortdsc", StringUtil.RTrim( Ddo_contratoservicostelas_status_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicostelas_status_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicostelas_status_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicostelas_servicosigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_TELA_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicostelas_link_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicostelas_link_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_LINK_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicostelas_link_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_PARMS_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicostelas_status_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSTELAS_STATUS_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicostelas_status_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PromptContratoServicosTelas";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A938ContratoServicosTelas_Sequencial), "ZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("promptcontratoservicostelas:[SendSecurityCheck value for]"+"ContratoServicosTelas_Sequencial:"+context.localUtil.Format( (decimal)(A938ContratoServicosTelas_Sequencial), "ZZ9"));
      }

      protected void RenderHtmlCloseFormG82( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratoServicosTelas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contrato Servicos Telas" ;
      }

      protected void WBG80( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_G82( true) ;
         }
         else
         {
            wb_table1_2_G82( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_G82e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosTelas_Sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A938ContratoServicosTelas_Sequencial), "ZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosTelas_Sequencial_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosTelas_Sequencial_Visible, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Sequencial", "right", false, "HLP_PromptContratoServicosTelas.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosTelas_ServicoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A925ContratoServicosTelas_ServicoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A925ContratoServicosTelas_ServicoCod), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosTelas_ServicoCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosTelas_ServicoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_PromptContratoServicosTelas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(95, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(96, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_Internalname, StringUtil.RTrim( AV33TFContratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV33TFContratada_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosTelas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_sel_Internalname, StringUtil.RTrim( AV34TFContratada_PessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV34TFContratada_PessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosTelas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicostelas_servicosigla_Internalname, StringUtil.RTrim( AV37TFContratoServicosTelas_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( AV37TFContratoServicosTelas_ServicoSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicostelas_servicosigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicostelas_servicosigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosTelas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicostelas_servicosigla_sel_Internalname, StringUtil.RTrim( AV38TFContratoServicosTelas_ServicoSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV38TFContratoServicosTelas_ServicoSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicostelas_servicosigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicostelas_servicosigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosTelas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicostelas_tela_Internalname, StringUtil.RTrim( AV41TFContratoServicosTelas_Tela), StringUtil.RTrim( context.localUtil.Format( AV41TFContratoServicosTelas_Tela, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicostelas_tela_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicostelas_tela_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosTelas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicostelas_tela_sel_Internalname, StringUtil.RTrim( AV42TFContratoServicosTelas_Tela_Sel), StringUtil.RTrim( context.localUtil.Format( AV42TFContratoServicosTelas_Tela_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicostelas_tela_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicostelas_tela_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosTelas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicostelas_link_Internalname, AV45TFContratoServicosTelas_Link, StringUtil.RTrim( context.localUtil.Format( AV45TFContratoServicosTelas_Link, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicostelas_link_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicostelas_link_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosTelas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicostelas_link_sel_Internalname, AV46TFContratoServicosTelas_Link_Sel, StringUtil.RTrim( context.localUtil.Format( AV46TFContratoServicosTelas_Link_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicostelas_link_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicostelas_link_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosTelas.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicostelas_parms_Internalname, AV49TFContratoServicosTelas_Parms, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", 0, edtavTfcontratoservicostelas_parms_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContratoServicosTelas.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicostelas_parms_sel_Internalname, AV50TFContratoServicosTelas_Parms_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, edtavTfcontratoservicostelas_parms_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContratoServicosTelas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, AV35ddo_Contratada_PessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosTelas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicostelas_servicosiglatitlecontrolidtoreplace_Internalname, AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavDdo_contratoservicostelas_servicosiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosTelas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSTELAS_TELAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Internalname, AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosTelas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSTELAS_LINKContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Internalname, AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosTelas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSTELAS_PARMSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Internalname, AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", 0, edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosTelas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSTELAS_STATUSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Internalname, AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", 0, edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosTelas.htm");
         }
         wbLoad = true;
      }

      protected void STARTG82( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contrato Servicos Telas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPG80( ) ;
      }

      protected void WSG82( )
      {
         STARTG82( ) ;
         EVTG82( ) ;
      }

      protected void EVTG82( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11G82 */
                           E11G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12G82 */
                           E12G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13G82 */
                           E13G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSTELAS_TELA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14G82 */
                           E14G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSTELAS_LINK.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15G82 */
                           E15G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSTELAS_PARMS.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16G82 */
                           E16G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSTELAS_STATUS.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17G82 */
                           E17G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18G82 */
                           E18G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19G82 */
                           E19G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20G82 */
                           E20G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21G82 */
                           E21G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22G82 */
                           E22G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23G82 */
                           E23G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24G82 */
                           E24G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25G82 */
                           E25G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26G82 */
                           E26G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E27G82 */
                           E27G82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_80_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
                           SubsflControlProps_802( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV62Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A926ContratoServicosTelas_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosTelas_ContratoCod_Internalname), ",", "."));
                           A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
                           n41Contratada_PessoaNom = false;
                           A937ContratoServicosTelas_ServicoSigla = StringUtil.Upper( cgiGet( edtContratoServicosTelas_ServicoSigla_Internalname));
                           n937ContratoServicosTelas_ServicoSigla = false;
                           A931ContratoServicosTelas_Tela = StringUtil.Upper( cgiGet( edtContratoServicosTelas_Tela_Internalname));
                           A928ContratoServicosTelas_Link = cgiGet( edtContratoServicosTelas_Link_Internalname);
                           A929ContratoServicosTelas_Parms = cgiGet( edtContratoServicosTelas_Parms_Internalname);
                           n929ContratoServicosTelas_Parms = false;
                           cmbContratoServicosTelas_Status.Name = cmbContratoServicosTelas_Status_Internalname;
                           cmbContratoServicosTelas_Status.CurrentValue = cgiGet( cmbContratoServicosTelas_Status_Internalname);
                           A932ContratoServicosTelas_Status = cgiGet( cmbContratoServicosTelas_Status_Internalname);
                           n932ContratoServicosTelas_Status = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E28G82 */
                                 E28G82 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E29G82 */
                                 E29G82 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E30G82 */
                                 E30G82 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicostelas_servicocod1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSTELAS_SERVICOCOD1"), ",", ".") != Convert.ToDecimal( AV17ContratoServicosTelas_ServicoCod1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicostelas_servicocod2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSTELAS_SERVICOCOD2"), ",", ".") != Convert.ToDecimal( AV21ContratoServicosTelas_ServicoCod2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicostelas_servicocod3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSTELAS_SERVICOCOD3"), ",", ".") != Convert.ToDecimal( AV25ContratoServicosTelas_ServicoCod3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoanom Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV33TFContratada_PessoaNom) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoanom_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV34TFContratada_PessoaNom_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicostelas_servicosigla Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA"), AV37TFContratoServicosTelas_ServicoSigla) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicostelas_servicosigla_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL"), AV38TFContratoServicosTelas_ServicoSigla_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicostelas_tela Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_TELA"), AV41TFContratoServicosTelas_Tela) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicostelas_tela_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_TELA_SEL"), AV42TFContratoServicosTelas_Tela_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicostelas_link Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_LINK"), AV45TFContratoServicosTelas_Link) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicostelas_link_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_LINK_SEL"), AV46TFContratoServicosTelas_Link_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicostelas_parms Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_PARMS"), AV49TFContratoServicosTelas_Parms) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicostelas_parms_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_PARMS_SEL"), AV50TFContratoServicosTelas_Parms_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E31G82 */
                                       E31G82 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEG82( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormG82( ) ;
            }
         }
      }

      protected void PAG82( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOSERVICOSTELAS_SERVICOCOD", "Servi�o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            dynavContratoservicostelas_servicocod1.Name = "vCONTRATOSERVICOSTELAS_SERVICOCOD1";
            dynavContratoservicostelas_servicocod1.WebTags = "";
            dynavContratoservicostelas_servicocod1.removeAllItems();
            /* Using cursor H00G82 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavContratoservicostelas_servicocod1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00G82_A155Servico_Codigo[0]), 6, 0)), H00G82_A608Servico_Nome[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavContratoservicostelas_servicocod1.ItemCount > 0 )
            {
               AV17ContratoServicosTelas_ServicoCod1 = (int)(NumberUtil.Val( dynavContratoservicostelas_servicocod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosTelas_ServicoCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOSERVICOSTELAS_SERVICOCOD", "Servi�o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            dynavContratoservicostelas_servicocod2.Name = "vCONTRATOSERVICOSTELAS_SERVICOCOD2";
            dynavContratoservicostelas_servicocod2.WebTags = "";
            dynavContratoservicostelas_servicocod2.removeAllItems();
            /* Using cursor H00G83 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               dynavContratoservicostelas_servicocod2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00G83_A155Servico_Codigo[0]), 6, 0)), H00G83_A608Servico_Nome[0], 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            if ( dynavContratoservicostelas_servicocod2.ItemCount > 0 )
            {
               AV21ContratoServicosTelas_ServicoCod2 = (int)(NumberUtil.Val( dynavContratoservicostelas_servicocod2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosTelas_ServicoCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOSERVICOSTELAS_SERVICOCOD", "Servi�o", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            dynavContratoservicostelas_servicocod3.Name = "vCONTRATOSERVICOSTELAS_SERVICOCOD3";
            dynavContratoservicostelas_servicocod3.WebTags = "";
            dynavContratoservicostelas_servicocod3.removeAllItems();
            /* Using cursor H00G84 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               dynavContratoservicostelas_servicocod3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00G84_A155Servico_Codigo[0]), 6, 0)), H00G84_A608Servico_Nome[0], 0);
               pr_default.readNext(2);
            }
            pr_default.close(2);
            if ( dynavContratoservicostelas_servicocod3.ItemCount > 0 )
            {
               AV25ContratoServicosTelas_ServicoCod3 = (int)(NumberUtil.Val( dynavContratoservicostelas_servicocod3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosTelas_ServicoCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0)));
            }
            GXCCtl = "CONTRATOSERVICOSTELAS_STATUS_" + sGXsfl_80_idx;
            cmbContratoServicosTelas_Status.Name = GXCCtl;
            cmbContratoServicosTelas_Status.WebTags = "";
            cmbContratoServicosTelas_Status.addItem("", "Todos", 0);
            cmbContratoServicosTelas_Status.addItem("B", "Stand by", 0);
            cmbContratoServicosTelas_Status.addItem("S", "Solicitada", 0);
            cmbContratoServicosTelas_Status.addItem("E", "Em An�lise", 0);
            cmbContratoServicosTelas_Status.addItem("A", "Em execu��o", 0);
            cmbContratoServicosTelas_Status.addItem("R", "Resolvida", 0);
            cmbContratoServicosTelas_Status.addItem("C", "Conferida", 0);
            cmbContratoServicosTelas_Status.addItem("D", "Retornada", 0);
            cmbContratoServicosTelas_Status.addItem("H", "Homologada", 0);
            cmbContratoServicosTelas_Status.addItem("O", "Aceite", 0);
            cmbContratoServicosTelas_Status.addItem("P", "A Pagar", 0);
            cmbContratoServicosTelas_Status.addItem("L", "Liquidada", 0);
            cmbContratoServicosTelas_Status.addItem("X", "Cancelada", 0);
            cmbContratoServicosTelas_Status.addItem("N", "N�o Faturada", 0);
            cmbContratoServicosTelas_Status.addItem("J", "Planejamento", 0);
            cmbContratoServicosTelas_Status.addItem("I", "An�lise Planejamento", 0);
            cmbContratoServicosTelas_Status.addItem("T", "Validacao T�cnica", 0);
            cmbContratoServicosTelas_Status.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoServicosTelas_Status.addItem("G", "Em Homologa��o", 0);
            cmbContratoServicosTelas_Status.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoServicosTelas_Status.addItem("U", "Rascunho", 0);
            if ( cmbContratoServicosTelas_Status.ItemCount > 0 )
            {
               A932ContratoServicosTelas_Status = cmbContratoServicosTelas_Status.getValidValue(A932ContratoServicosTelas_Status);
               n932ContratoServicosTelas_Status = false;
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATOSERVICOSTELAS_SERVICOCOD3G81( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATOSERVICOSTELAS_SERVICOCOD3_dataG81( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATOSERVICOSTELAS_SERVICOCOD3_htmlG81( )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATOSERVICOSTELAS_SERVICOCOD3_dataG81( ) ;
         gxdynajaxindex = 1;
         dynavContratoservicostelas_servicocod3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratoservicostelas_servicocod3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratoservicostelas_servicocod3.ItemCount > 0 )
         {
            AV25ContratoServicosTelas_ServicoCod3 = (int)(NumberUtil.Val( dynavContratoservicostelas_servicocod3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosTelas_ServicoCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATOSERVICOSTELAS_SERVICOCOD3_dataG81( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00G85 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00G85_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00G85_A608Servico_Nome[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvCONTRATOSERVICOSTELAS_SERVICOCOD2G81( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATOSERVICOSTELAS_SERVICOCOD2_dataG81( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATOSERVICOSTELAS_SERVICOCOD2_htmlG81( )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATOSERVICOSTELAS_SERVICOCOD2_dataG81( ) ;
         gxdynajaxindex = 1;
         dynavContratoservicostelas_servicocod2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratoservicostelas_servicocod2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratoservicostelas_servicocod2.ItemCount > 0 )
         {
            AV21ContratoServicosTelas_ServicoCod2 = (int)(NumberUtil.Val( dynavContratoservicostelas_servicocod2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosTelas_ServicoCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATOSERVICOSTELAS_SERVICOCOD2_dataG81( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00G86 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00G86_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00G86_A608Servico_Nome[0]));
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void GXDLVvCONTRATOSERVICOSTELAS_SERVICOCOD1G81( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATOSERVICOSTELAS_SERVICOCOD1_dataG81( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATOSERVICOSTELAS_SERVICOCOD1_htmlG81( )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATOSERVICOSTELAS_SERVICOCOD1_dataG81( ) ;
         gxdynajaxindex = 1;
         dynavContratoservicostelas_servicocod1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratoservicostelas_servicocod1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratoservicostelas_servicocod1.ItemCount > 0 )
         {
            AV17ContratoServicosTelas_ServicoCod1 = (int)(NumberUtil.Val( dynavContratoservicostelas_servicocod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosTelas_ServicoCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATOSERVICOSTELAS_SERVICOCOD1_dataG81( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00G87 */
         pr_default.execute(5);
         while ( (pr_default.getStatus(5) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00G87_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00G87_A608Servico_Nome[0]));
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_802( ) ;
         while ( nGXsfl_80_idx <= nRC_GXsfl_80 )
         {
            sendrow_802( ) ;
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       int AV17ContratoServicosTelas_ServicoCod1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       int AV21ContratoServicosTelas_ServicoCod2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       int AV25ContratoServicosTelas_ServicoCod3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV33TFContratada_PessoaNom ,
                                       String AV34TFContratada_PessoaNom_Sel ,
                                       String AV37TFContratoServicosTelas_ServicoSigla ,
                                       String AV38TFContratoServicosTelas_ServicoSigla_Sel ,
                                       String AV41TFContratoServicosTelas_Tela ,
                                       String AV42TFContratoServicosTelas_Tela_Sel ,
                                       String AV45TFContratoServicosTelas_Link ,
                                       String AV46TFContratoServicosTelas_Link_Sel ,
                                       String AV49TFContratoServicosTelas_Parms ,
                                       String AV50TFContratoServicosTelas_Parms_Sel ,
                                       String AV35ddo_Contratada_PessoaNomTitleControlIdToReplace ,
                                       String AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace ,
                                       String AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace ,
                                       String AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace ,
                                       String AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace ,
                                       String AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace ,
                                       IGxCollection AV54TFContratoServicosTelas_Status_Sels ,
                                       String AV63Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFG82( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSTELAS_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A926ContratoServicosTelas_ContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSTELAS_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSTELAS_TELA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A931ContratoServicosTelas_Tela, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSTELAS_TELA", StringUtil.RTrim( A931ContratoServicosTelas_Tela));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSTELAS_LINK", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A928ContratoServicosTelas_Link, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSTELAS_LINK", A928ContratoServicosTelas_Link);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSTELAS_PARMS", GetSecureSignedToken( "", A929ContratoServicosTelas_Parms));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSTELAS_PARMS", A929ContratoServicosTelas_Parms);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSTELAS_STATUS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A932ContratoServicosTelas_Status, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSTELAS_STATUS", StringUtil.RTrim( A932ContratoServicosTelas_Status));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( dynavContratoservicostelas_servicocod1.ItemCount > 0 )
         {
            AV17ContratoServicosTelas_ServicoCod1 = (int)(NumberUtil.Val( dynavContratoservicostelas_servicocod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosTelas_ServicoCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( dynavContratoservicostelas_servicocod2.ItemCount > 0 )
         {
            AV21ContratoServicosTelas_ServicoCod2 = (int)(NumberUtil.Val( dynavContratoservicostelas_servicocod2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosTelas_ServicoCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
         if ( dynavContratoservicostelas_servicocod3.ItemCount > 0 )
         {
            AV25ContratoServicosTelas_ServicoCod3 = (int)(NumberUtil.Val( dynavContratoservicostelas_servicocod3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosTelas_ServicoCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFG82( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV63Pgmname = "PromptContratoServicosTelas";
         context.Gx_err = 0;
      }

      protected void RFG82( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 80;
         /* Execute user event: E29G82 */
         E29G82 ();
         nGXsfl_80_idx = 1;
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_802( ) ;
         nGXsfl_80_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_802( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(6, new Object[]{ new Object[]{
                                                 A932ContratoServicosTelas_Status ,
                                                 AV54TFContratoServicosTelas_Status_Sels ,
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17ContratoServicosTelas_ServicoCod1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20DynamicFiltersOperator2 ,
                                                 AV21ContratoServicosTelas_ServicoCod2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24DynamicFiltersOperator3 ,
                                                 AV25ContratoServicosTelas_ServicoCod3 ,
                                                 AV34TFContratada_PessoaNom_Sel ,
                                                 AV33TFContratada_PessoaNom ,
                                                 AV38TFContratoServicosTelas_ServicoSigla_Sel ,
                                                 AV37TFContratoServicosTelas_ServicoSigla ,
                                                 AV42TFContratoServicosTelas_Tela_Sel ,
                                                 AV41TFContratoServicosTelas_Tela ,
                                                 AV46TFContratoServicosTelas_Link_Sel ,
                                                 AV45TFContratoServicosTelas_Link ,
                                                 AV50TFContratoServicosTelas_Parms_Sel ,
                                                 AV49TFContratoServicosTelas_Parms ,
                                                 AV54TFContratoServicosTelas_Status_Sels.Count ,
                                                 A925ContratoServicosTelas_ServicoCod ,
                                                 A41Contratada_PessoaNom ,
                                                 A937ContratoServicosTelas_ServicoSigla ,
                                                 A931ContratoServicosTelas_Tela ,
                                                 A928ContratoServicosTelas_Link ,
                                                 A929ContratoServicosTelas_Parms ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV33TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV33TFContratada_PessoaNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratada_PessoaNom", AV33TFContratada_PessoaNom);
            lV37TFContratoServicosTelas_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV37TFContratoServicosTelas_ServicoSigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosTelas_ServicoSigla", AV37TFContratoServicosTelas_ServicoSigla);
            lV41TFContratoServicosTelas_Tela = StringUtil.PadR( StringUtil.RTrim( AV41TFContratoServicosTelas_Tela), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosTelas_Tela", AV41TFContratoServicosTelas_Tela);
            lV45TFContratoServicosTelas_Link = StringUtil.Concat( StringUtil.RTrim( AV45TFContratoServicosTelas_Link), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosTelas_Link", AV45TFContratoServicosTelas_Link);
            lV49TFContratoServicosTelas_Parms = StringUtil.Concat( StringUtil.RTrim( AV49TFContratoServicosTelas_Parms), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicosTelas_Parms", AV49TFContratoServicosTelas_Parms);
            /* Using cursor H00G88 */
            pr_default.execute(6, new Object[] {AV17ContratoServicosTelas_ServicoCod1, AV17ContratoServicosTelas_ServicoCod1, AV17ContratoServicosTelas_ServicoCod1, AV21ContratoServicosTelas_ServicoCod2, AV21ContratoServicosTelas_ServicoCod2, AV21ContratoServicosTelas_ServicoCod2, AV25ContratoServicosTelas_ServicoCod3, AV25ContratoServicosTelas_ServicoCod3, AV25ContratoServicosTelas_ServicoCod3, lV33TFContratada_PessoaNom, AV34TFContratada_PessoaNom_Sel, lV37TFContratoServicosTelas_ServicoSigla, AV38TFContratoServicosTelas_ServicoSigla_Sel, lV41TFContratoServicosTelas_Tela, AV42TFContratoServicosTelas_Tela_Sel, lV45TFContratoServicosTelas_Link, AV46TFContratoServicosTelas_Link_Sel, lV49TFContratoServicosTelas_Parms, AV50TFContratoServicosTelas_Parms_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_80_idx = 1;
            while ( ( (pr_default.getStatus(6) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A74Contrato_Codigo = H00G88_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00G88_n74Contrato_Codigo[0];
               A39Contratada_Codigo = H00G88_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = H00G88_A40Contratada_PessoaCod[0];
               A938ContratoServicosTelas_Sequencial = H00G88_A938ContratoServicosTelas_Sequencial[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A938ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0)));
               A925ContratoServicosTelas_ServicoCod = H00G88_A925ContratoServicosTelas_ServicoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A925ContratoServicosTelas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A925ContratoServicosTelas_ServicoCod), 6, 0)));
               n925ContratoServicosTelas_ServicoCod = H00G88_n925ContratoServicosTelas_ServicoCod[0];
               A932ContratoServicosTelas_Status = H00G88_A932ContratoServicosTelas_Status[0];
               n932ContratoServicosTelas_Status = H00G88_n932ContratoServicosTelas_Status[0];
               A929ContratoServicosTelas_Parms = H00G88_A929ContratoServicosTelas_Parms[0];
               n929ContratoServicosTelas_Parms = H00G88_n929ContratoServicosTelas_Parms[0];
               A928ContratoServicosTelas_Link = H00G88_A928ContratoServicosTelas_Link[0];
               A931ContratoServicosTelas_Tela = H00G88_A931ContratoServicosTelas_Tela[0];
               A937ContratoServicosTelas_ServicoSigla = H00G88_A937ContratoServicosTelas_ServicoSigla[0];
               n937ContratoServicosTelas_ServicoSigla = H00G88_n937ContratoServicosTelas_ServicoSigla[0];
               A41Contratada_PessoaNom = H00G88_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00G88_n41Contratada_PessoaNom[0];
               A926ContratoServicosTelas_ContratoCod = H00G88_A926ContratoServicosTelas_ContratoCod[0];
               A74Contrato_Codigo = H00G88_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00G88_n74Contrato_Codigo[0];
               A925ContratoServicosTelas_ServicoCod = H00G88_A925ContratoServicosTelas_ServicoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A925ContratoServicosTelas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A925ContratoServicosTelas_ServicoCod), 6, 0)));
               n925ContratoServicosTelas_ServicoCod = H00G88_n925ContratoServicosTelas_ServicoCod[0];
               A39Contratada_Codigo = H00G88_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = H00G88_A40Contratada_PessoaCod[0];
               A41Contratada_PessoaNom = H00G88_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00G88_n41Contratada_PessoaNom[0];
               A937ContratoServicosTelas_ServicoSigla = H00G88_A937ContratoServicosTelas_ServicoSigla[0];
               n937ContratoServicosTelas_ServicoSigla = H00G88_n937ContratoServicosTelas_ServicoSigla[0];
               /* Execute user event: E30G82 */
               E30G82 ();
               pr_default.readNext(6);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(6) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(6);
            wbEnd = 80;
            WBG80( ) ;
         }
         nGXsfl_80_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(7, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV54TFContratoServicosTelas_Status_Sels ,
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17ContratoServicosTelas_ServicoCod1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20DynamicFiltersOperator2 ,
                                              AV21ContratoServicosTelas_ServicoCod2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24DynamicFiltersOperator3 ,
                                              AV25ContratoServicosTelas_ServicoCod3 ,
                                              AV34TFContratada_PessoaNom_Sel ,
                                              AV33TFContratada_PessoaNom ,
                                              AV38TFContratoServicosTelas_ServicoSigla_Sel ,
                                              AV37TFContratoServicosTelas_ServicoSigla ,
                                              AV42TFContratoServicosTelas_Tela_Sel ,
                                              AV41TFContratoServicosTelas_Tela ,
                                              AV46TFContratoServicosTelas_Link_Sel ,
                                              AV45TFContratoServicosTelas_Link ,
                                              AV50TFContratoServicosTelas_Parms_Sel ,
                                              AV49TFContratoServicosTelas_Parms ,
                                              AV54TFContratoServicosTelas_Status_Sels.Count ,
                                              A925ContratoServicosTelas_ServicoCod ,
                                              A41Contratada_PessoaNom ,
                                              A937ContratoServicosTelas_ServicoSigla ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV33TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV33TFContratada_PessoaNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratada_PessoaNom", AV33TFContratada_PessoaNom);
         lV37TFContratoServicosTelas_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV37TFContratoServicosTelas_ServicoSigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosTelas_ServicoSigla", AV37TFContratoServicosTelas_ServicoSigla);
         lV41TFContratoServicosTelas_Tela = StringUtil.PadR( StringUtil.RTrim( AV41TFContratoServicosTelas_Tela), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosTelas_Tela", AV41TFContratoServicosTelas_Tela);
         lV45TFContratoServicosTelas_Link = StringUtil.Concat( StringUtil.RTrim( AV45TFContratoServicosTelas_Link), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosTelas_Link", AV45TFContratoServicosTelas_Link);
         lV49TFContratoServicosTelas_Parms = StringUtil.Concat( StringUtil.RTrim( AV49TFContratoServicosTelas_Parms), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicosTelas_Parms", AV49TFContratoServicosTelas_Parms);
         /* Using cursor H00G89 */
         pr_default.execute(7, new Object[] {AV17ContratoServicosTelas_ServicoCod1, AV17ContratoServicosTelas_ServicoCod1, AV17ContratoServicosTelas_ServicoCod1, AV21ContratoServicosTelas_ServicoCod2, AV21ContratoServicosTelas_ServicoCod2, AV21ContratoServicosTelas_ServicoCod2, AV25ContratoServicosTelas_ServicoCod3, AV25ContratoServicosTelas_ServicoCod3, AV25ContratoServicosTelas_ServicoCod3, lV33TFContratada_PessoaNom, AV34TFContratada_PessoaNom_Sel, lV37TFContratoServicosTelas_ServicoSigla, AV38TFContratoServicosTelas_ServicoSigla_Sel, lV41TFContratoServicosTelas_Tela, AV42TFContratoServicosTelas_Tela_Sel, lV45TFContratoServicosTelas_Link, AV46TFContratoServicosTelas_Link_Sel, lV49TFContratoServicosTelas_Parms, AV50TFContratoServicosTelas_Parms_Sel});
         GRID_nRecordCount = H00G89_AGRID_nRecordCount[0];
         pr_default.close(7);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosTelas_ServicoCod1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosTelas_ServicoCod2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosTelas_ServicoCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV33TFContratada_PessoaNom, AV34TFContratada_PessoaNom_Sel, AV37TFContratoServicosTelas_ServicoSigla, AV38TFContratoServicosTelas_ServicoSigla_Sel, AV41TFContratoServicosTelas_Tela, AV42TFContratoServicosTelas_Tela_Sel, AV45TFContratoServicosTelas_Link, AV46TFContratoServicosTelas_Link_Sel, AV49TFContratoServicosTelas_Parms, AV50TFContratoServicosTelas_Parms_Sel, AV35ddo_Contratada_PessoaNomTitleControlIdToReplace, AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace, AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV54TFContratoServicosTelas_Status_Sels, AV63Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosTelas_ServicoCod1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosTelas_ServicoCod2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosTelas_ServicoCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV33TFContratada_PessoaNom, AV34TFContratada_PessoaNom_Sel, AV37TFContratoServicosTelas_ServicoSigla, AV38TFContratoServicosTelas_ServicoSigla_Sel, AV41TFContratoServicosTelas_Tela, AV42TFContratoServicosTelas_Tela_Sel, AV45TFContratoServicosTelas_Link, AV46TFContratoServicosTelas_Link_Sel, AV49TFContratoServicosTelas_Parms, AV50TFContratoServicosTelas_Parms_Sel, AV35ddo_Contratada_PessoaNomTitleControlIdToReplace, AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace, AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV54TFContratoServicosTelas_Status_Sels, AV63Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosTelas_ServicoCod1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosTelas_ServicoCod2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosTelas_ServicoCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV33TFContratada_PessoaNom, AV34TFContratada_PessoaNom_Sel, AV37TFContratoServicosTelas_ServicoSigla, AV38TFContratoServicosTelas_ServicoSigla_Sel, AV41TFContratoServicosTelas_Tela, AV42TFContratoServicosTelas_Tela_Sel, AV45TFContratoServicosTelas_Link, AV46TFContratoServicosTelas_Link_Sel, AV49TFContratoServicosTelas_Parms, AV50TFContratoServicosTelas_Parms_Sel, AV35ddo_Contratada_PessoaNomTitleControlIdToReplace, AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace, AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV54TFContratoServicosTelas_Status_Sels, AV63Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosTelas_ServicoCod1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosTelas_ServicoCod2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosTelas_ServicoCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV33TFContratada_PessoaNom, AV34TFContratada_PessoaNom_Sel, AV37TFContratoServicosTelas_ServicoSigla, AV38TFContratoServicosTelas_ServicoSigla_Sel, AV41TFContratoServicosTelas_Tela, AV42TFContratoServicosTelas_Tela_Sel, AV45TFContratoServicosTelas_Link, AV46TFContratoServicosTelas_Link_Sel, AV49TFContratoServicosTelas_Parms, AV50TFContratoServicosTelas_Parms_Sel, AV35ddo_Contratada_PessoaNomTitleControlIdToReplace, AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace, AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV54TFContratoServicosTelas_Status_Sels, AV63Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosTelas_ServicoCod1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosTelas_ServicoCod2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosTelas_ServicoCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV33TFContratada_PessoaNom, AV34TFContratada_PessoaNom_Sel, AV37TFContratoServicosTelas_ServicoSigla, AV38TFContratoServicosTelas_ServicoSigla_Sel, AV41TFContratoServicosTelas_Tela, AV42TFContratoServicosTelas_Tela_Sel, AV45TFContratoServicosTelas_Link, AV46TFContratoServicosTelas_Link_Sel, AV49TFContratoServicosTelas_Parms, AV50TFContratoServicosTelas_Parms_Sel, AV35ddo_Contratada_PessoaNomTitleControlIdToReplace, AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace, AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV54TFContratoServicosTelas_Status_Sels, AV63Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPG80( )
      {
         /* Before Start, stand alone formulas. */
         AV63Pgmname = "PromptContratoServicosTelas";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E28G82 */
         E28G82 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV56DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOANOMTITLEFILTERDATA"), AV32Contratada_PessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSTELAS_SERVICOSIGLATITLEFILTERDATA"), AV36ContratoServicosTelas_ServicoSiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSTELAS_TELATITLEFILTERDATA"), AV40ContratoServicosTelas_TelaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSTELAS_LINKTITLEFILTERDATA"), AV44ContratoServicosTelas_LinkTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSTELAS_PARMSTITLEFILTERDATA"), AV48ContratoServicosTelas_ParmsTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSTELAS_STATUSTITLEFILTERDATA"), AV52ContratoServicosTelas_StatusTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            dynavContratoservicostelas_servicocod1.Name = dynavContratoservicostelas_servicocod1_Internalname;
            dynavContratoservicostelas_servicocod1.CurrentValue = cgiGet( dynavContratoservicostelas_servicocod1_Internalname);
            AV17ContratoServicosTelas_ServicoCod1 = (int)(NumberUtil.Val( cgiGet( dynavContratoservicostelas_servicocod1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosTelas_ServicoCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0)));
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            dynavContratoservicostelas_servicocod2.Name = dynavContratoservicostelas_servicocod2_Internalname;
            dynavContratoservicostelas_servicocod2.CurrentValue = cgiGet( dynavContratoservicostelas_servicocod2_Internalname);
            AV21ContratoServicosTelas_ServicoCod2 = (int)(NumberUtil.Val( cgiGet( dynavContratoservicostelas_servicocod2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosTelas_ServicoCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0)));
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            dynavContratoservicostelas_servicocod3.Name = dynavContratoservicostelas_servicocod3_Internalname;
            dynavContratoservicostelas_servicocod3.CurrentValue = cgiGet( dynavContratoservicostelas_servicocod3_Internalname);
            AV25ContratoServicosTelas_ServicoCod3 = (int)(NumberUtil.Val( cgiGet( dynavContratoservicostelas_servicocod3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosTelas_ServicoCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0)));
            A938ContratoServicosTelas_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosTelas_Sequencial_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A938ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0)));
            A925ContratoServicosTelas_ServicoCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosTelas_ServicoCod_Internalname), ",", "."));
            n925ContratoServicosTelas_ServicoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A925ContratoServicosTelas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A925ContratoServicosTelas_ServicoCod), 6, 0)));
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV33TFContratada_PessoaNom = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratada_PessoaNom", AV33TFContratada_PessoaNom);
            AV34TFContratada_PessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratada_PessoaNom_Sel", AV34TFContratada_PessoaNom_Sel);
            AV37TFContratoServicosTelas_ServicoSigla = StringUtil.Upper( cgiGet( edtavTfcontratoservicostelas_servicosigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosTelas_ServicoSigla", AV37TFContratoServicosTelas_ServicoSigla);
            AV38TFContratoServicosTelas_ServicoSigla_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoservicostelas_servicosigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoServicosTelas_ServicoSigla_Sel", AV38TFContratoServicosTelas_ServicoSigla_Sel);
            AV41TFContratoServicosTelas_Tela = StringUtil.Upper( cgiGet( edtavTfcontratoservicostelas_tela_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosTelas_Tela", AV41TFContratoServicosTelas_Tela);
            AV42TFContratoServicosTelas_Tela_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoservicostelas_tela_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoServicosTelas_Tela_Sel", AV42TFContratoServicosTelas_Tela_Sel);
            AV45TFContratoServicosTelas_Link = cgiGet( edtavTfcontratoservicostelas_link_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosTelas_Link", AV45TFContratoServicosTelas_Link);
            AV46TFContratoServicosTelas_Link_Sel = cgiGet( edtavTfcontratoservicostelas_link_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContratoServicosTelas_Link_Sel", AV46TFContratoServicosTelas_Link_Sel);
            AV49TFContratoServicosTelas_Parms = cgiGet( edtavTfcontratoservicostelas_parms_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicosTelas_Parms", AV49TFContratoServicosTelas_Parms);
            AV50TFContratoServicosTelas_Parms_Sel = cgiGet( edtavTfcontratoservicostelas_parms_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoServicosTelas_Parms_Sel", AV50TFContratoServicosTelas_Parms_Sel);
            AV35ddo_Contratada_PessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ddo_Contratada_PessoaNomTitleControlIdToReplace", AV35ddo_Contratada_PessoaNomTitleControlIdToReplace);
            AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicostelas_servicosiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace", AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace);
            AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace", AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace);
            AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace", AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace);
            AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace", AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace);
            AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace", AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_80 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_80"), ",", "."));
            AV58GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV59GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratada_pessoanom_Caption = cgiGet( "DDO_CONTRATADA_PESSOANOM_Caption");
            Ddo_contratada_pessoanom_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOANOM_Tooltip");
            Ddo_contratada_pessoanom_Cls = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cls");
            Ddo_contratada_pessoanom_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_set");
            Ddo_contratada_pessoanom_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set");
            Ddo_contratada_pessoanom_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype");
            Ddo_contratada_pessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace");
            Ddo_contratada_pessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortasc"));
            Ddo_contratada_pessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortdsc"));
            Ddo_contratada_pessoanom_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortedstatus");
            Ddo_contratada_pessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includefilter"));
            Ddo_contratada_pessoanom_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filtertype");
            Ddo_contratada_pessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Filterisrange"));
            Ddo_contratada_pessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includedatalist"));
            Ddo_contratada_pessoanom_Datalisttype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalisttype");
            Ddo_contratada_pessoanom_Datalistproc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistproc");
            Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoanom_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortasc");
            Ddo_contratada_pessoanom_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortdsc");
            Ddo_contratada_pessoanom_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOANOM_Loadingdata");
            Ddo_contratada_pessoanom_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cleanfilter");
            Ddo_contratada_pessoanom_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOANOM_Noresultsfound");
            Ddo_contratada_pessoanom_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOANOM_Searchbuttontext");
            Ddo_contratoservicostelas_servicosigla_Caption = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Caption");
            Ddo_contratoservicostelas_servicosigla_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Tooltip");
            Ddo_contratoservicostelas_servicosigla_Cls = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Cls");
            Ddo_contratoservicostelas_servicosigla_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Filteredtext_set");
            Ddo_contratoservicostelas_servicosigla_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Selectedvalue_set");
            Ddo_contratoservicostelas_servicosigla_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Dropdownoptionstype");
            Ddo_contratoservicostelas_servicosigla_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Titlecontrolidtoreplace");
            Ddo_contratoservicostelas_servicosigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Includesortasc"));
            Ddo_contratoservicostelas_servicosigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Includesortdsc"));
            Ddo_contratoservicostelas_servicosigla_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Sortedstatus");
            Ddo_contratoservicostelas_servicosigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Includefilter"));
            Ddo_contratoservicostelas_servicosigla_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Filtertype");
            Ddo_contratoservicostelas_servicosigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Filterisrange"));
            Ddo_contratoservicostelas_servicosigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Includedatalist"));
            Ddo_contratoservicostelas_servicosigla_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Datalisttype");
            Ddo_contratoservicostelas_servicosigla_Datalistproc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Datalistproc");
            Ddo_contratoservicostelas_servicosigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicostelas_servicosigla_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Sortasc");
            Ddo_contratoservicostelas_servicosigla_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Sortdsc");
            Ddo_contratoservicostelas_servicosigla_Loadingdata = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Loadingdata");
            Ddo_contratoservicostelas_servicosigla_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Cleanfilter");
            Ddo_contratoservicostelas_servicosigla_Noresultsfound = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Noresultsfound");
            Ddo_contratoservicostelas_servicosigla_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Searchbuttontext");
            Ddo_contratoservicostelas_tela_Caption = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Caption");
            Ddo_contratoservicostelas_tela_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Tooltip");
            Ddo_contratoservicostelas_tela_Cls = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Cls");
            Ddo_contratoservicostelas_tela_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Filteredtext_set");
            Ddo_contratoservicostelas_tela_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Selectedvalue_set");
            Ddo_contratoservicostelas_tela_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Dropdownoptionstype");
            Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Titlecontrolidtoreplace");
            Ddo_contratoservicostelas_tela_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Includesortasc"));
            Ddo_contratoservicostelas_tela_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Includesortdsc"));
            Ddo_contratoservicostelas_tela_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Sortedstatus");
            Ddo_contratoservicostelas_tela_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Includefilter"));
            Ddo_contratoservicostelas_tela_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Filtertype");
            Ddo_contratoservicostelas_tela_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Filterisrange"));
            Ddo_contratoservicostelas_tela_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Includedatalist"));
            Ddo_contratoservicostelas_tela_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Datalisttype");
            Ddo_contratoservicostelas_tela_Datalistproc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Datalistproc");
            Ddo_contratoservicostelas_tela_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicostelas_tela_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Sortasc");
            Ddo_contratoservicostelas_tela_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Sortdsc");
            Ddo_contratoservicostelas_tela_Loadingdata = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Loadingdata");
            Ddo_contratoservicostelas_tela_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Cleanfilter");
            Ddo_contratoservicostelas_tela_Noresultsfound = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Noresultsfound");
            Ddo_contratoservicostelas_tela_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Searchbuttontext");
            Ddo_contratoservicostelas_link_Caption = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Caption");
            Ddo_contratoservicostelas_link_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Tooltip");
            Ddo_contratoservicostelas_link_Cls = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Cls");
            Ddo_contratoservicostelas_link_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Filteredtext_set");
            Ddo_contratoservicostelas_link_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Selectedvalue_set");
            Ddo_contratoservicostelas_link_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Dropdownoptionstype");
            Ddo_contratoservicostelas_link_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Titlecontrolidtoreplace");
            Ddo_contratoservicostelas_link_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Includesortasc"));
            Ddo_contratoservicostelas_link_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Includesortdsc"));
            Ddo_contratoservicostelas_link_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Sortedstatus");
            Ddo_contratoservicostelas_link_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Includefilter"));
            Ddo_contratoservicostelas_link_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Filtertype");
            Ddo_contratoservicostelas_link_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Filterisrange"));
            Ddo_contratoservicostelas_link_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Includedatalist"));
            Ddo_contratoservicostelas_link_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Datalisttype");
            Ddo_contratoservicostelas_link_Datalistproc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Datalistproc");
            Ddo_contratoservicostelas_link_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicostelas_link_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Sortasc");
            Ddo_contratoservicostelas_link_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Sortdsc");
            Ddo_contratoservicostelas_link_Loadingdata = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Loadingdata");
            Ddo_contratoservicostelas_link_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Cleanfilter");
            Ddo_contratoservicostelas_link_Noresultsfound = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Noresultsfound");
            Ddo_contratoservicostelas_link_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Searchbuttontext");
            Ddo_contratoservicostelas_parms_Caption = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Caption");
            Ddo_contratoservicostelas_parms_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Tooltip");
            Ddo_contratoservicostelas_parms_Cls = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Cls");
            Ddo_contratoservicostelas_parms_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Filteredtext_set");
            Ddo_contratoservicostelas_parms_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Selectedvalue_set");
            Ddo_contratoservicostelas_parms_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Dropdownoptionstype");
            Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Titlecontrolidtoreplace");
            Ddo_contratoservicostelas_parms_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Includesortasc"));
            Ddo_contratoservicostelas_parms_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Includesortdsc"));
            Ddo_contratoservicostelas_parms_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Sortedstatus");
            Ddo_contratoservicostelas_parms_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Includefilter"));
            Ddo_contratoservicostelas_parms_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Filtertype");
            Ddo_contratoservicostelas_parms_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Filterisrange"));
            Ddo_contratoservicostelas_parms_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Includedatalist"));
            Ddo_contratoservicostelas_parms_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Datalisttype");
            Ddo_contratoservicostelas_parms_Datalistproc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Datalistproc");
            Ddo_contratoservicostelas_parms_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicostelas_parms_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Sortasc");
            Ddo_contratoservicostelas_parms_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Sortdsc");
            Ddo_contratoservicostelas_parms_Loadingdata = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Loadingdata");
            Ddo_contratoservicostelas_parms_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Cleanfilter");
            Ddo_contratoservicostelas_parms_Noresultsfound = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Noresultsfound");
            Ddo_contratoservicostelas_parms_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Searchbuttontext");
            Ddo_contratoservicostelas_status_Caption = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Caption");
            Ddo_contratoservicostelas_status_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Tooltip");
            Ddo_contratoservicostelas_status_Cls = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Cls");
            Ddo_contratoservicostelas_status_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Selectedvalue_set");
            Ddo_contratoservicostelas_status_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Dropdownoptionstype");
            Ddo_contratoservicostelas_status_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Titlecontrolidtoreplace");
            Ddo_contratoservicostelas_status_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Includesortasc"));
            Ddo_contratoservicostelas_status_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Includesortdsc"));
            Ddo_contratoservicostelas_status_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Sortedstatus");
            Ddo_contratoservicostelas_status_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Includefilter"));
            Ddo_contratoservicostelas_status_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Includedatalist"));
            Ddo_contratoservicostelas_status_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Datalisttype");
            Ddo_contratoservicostelas_status_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Allowmultipleselection"));
            Ddo_contratoservicostelas_status_Datalistfixedvalues = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Datalistfixedvalues");
            Ddo_contratoservicostelas_status_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Sortasc");
            Ddo_contratoservicostelas_status_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Sortdsc");
            Ddo_contratoservicostelas_status_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Cleanfilter");
            Ddo_contratoservicostelas_status_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratada_pessoanom_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOANOM_Activeeventkey");
            Ddo_contratada_pessoanom_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_get");
            Ddo_contratada_pessoanom_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get");
            Ddo_contratoservicostelas_servicosigla_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Activeeventkey");
            Ddo_contratoservicostelas_servicosigla_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Filteredtext_get");
            Ddo_contratoservicostelas_servicosigla_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA_Selectedvalue_get");
            Ddo_contratoservicostelas_tela_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Activeeventkey");
            Ddo_contratoservicostelas_tela_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Filteredtext_get");
            Ddo_contratoservicostelas_tela_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSTELAS_TELA_Selectedvalue_get");
            Ddo_contratoservicostelas_link_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Activeeventkey");
            Ddo_contratoservicostelas_link_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Filteredtext_get");
            Ddo_contratoservicostelas_link_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSTELAS_LINK_Selectedvalue_get");
            Ddo_contratoservicostelas_parms_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Activeeventkey");
            Ddo_contratoservicostelas_parms_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Filteredtext_get");
            Ddo_contratoservicostelas_parms_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSTELAS_PARMS_Selectedvalue_get");
            Ddo_contratoservicostelas_status_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Activeeventkey");
            Ddo_contratoservicostelas_status_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSTELAS_STATUS_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "PromptContratoServicosTelas";
            A938ContratoServicosTelas_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosTelas_Sequencial_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A938ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A938ContratoServicosTelas_Sequencial), "ZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("promptcontratoservicostelas:[SecurityCheckFailed value for]"+"ContratoServicosTelas_Sequencial:"+context.localUtil.Format( (decimal)(A938ContratoServicosTelas_Sequencial), "ZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSTELAS_SERVICOCOD1"), ",", ".") != Convert.ToDecimal( AV17ContratoServicosTelas_ServicoCod1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSTELAS_SERVICOCOD2"), ",", ".") != Convert.ToDecimal( AV21ContratoServicosTelas_ServicoCod2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSTELAS_SERVICOCOD3"), ",", ".") != Convert.ToDecimal( AV25ContratoServicosTelas_ServicoCod3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV33TFContratada_PessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV34TFContratada_PessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA"), AV37TFContratoServicosTelas_ServicoSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL"), AV38TFContratoServicosTelas_ServicoSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_TELA"), AV41TFContratoServicosTelas_Tela) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_TELA_SEL"), AV42TFContratoServicosTelas_Tela_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_LINK"), AV45TFContratoServicosTelas_Link) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_LINK_SEL"), AV46TFContratoServicosTelas_Link_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_PARMS"), AV49TFContratoServicosTelas_Parms) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSTELAS_PARMS_SEL"), AV50TFContratoServicosTelas_Parms_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E28G82 */
         E28G82 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28G82( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATOSERVICOSTELAS_SERVICOCOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTRATOSERVICOSTELAS_SERVICOCOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "CONTRATOSERVICOSTELAS_SERVICOCOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontratada_pessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_Visible), 5, 0)));
         edtavTfcontratada_pessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_sel_Visible), 5, 0)));
         edtavTfcontratoservicostelas_servicosigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicostelas_servicosigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_servicosigla_Visible), 5, 0)));
         edtavTfcontratoservicostelas_servicosigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicostelas_servicosigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_servicosigla_sel_Visible), 5, 0)));
         edtavTfcontratoservicostelas_tela_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicostelas_tela_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_tela_Visible), 5, 0)));
         edtavTfcontratoservicostelas_tela_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicostelas_tela_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_tela_sel_Visible), 5, 0)));
         edtavTfcontratoservicostelas_link_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicostelas_link_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_link_Visible), 5, 0)));
         edtavTfcontratoservicostelas_link_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicostelas_link_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_link_sel_Visible), 5, 0)));
         edtavTfcontratoservicostelas_parms_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicostelas_parms_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_parms_Visible), 5, 0)));
         edtavTfcontratoservicostelas_parms_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicostelas_parms_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_parms_sel_Visible), 5, 0)));
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoanom_Titlecontrolidtoreplace);
         AV35ddo_Contratada_PessoaNomTitleControlIdToReplace = Ddo_contratada_pessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ddo_Contratada_PessoaNomTitleControlIdToReplace", AV35ddo_Contratada_PessoaNomTitleControlIdToReplace);
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicostelas_servicosigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosTelas_ServicoSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_servicosigla_Internalname, "TitleControlIdToReplace", Ddo_contratoservicostelas_servicosigla_Titlecontrolidtoreplace);
         AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace = Ddo_contratoservicostelas_servicosigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace", AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace);
         edtavDdo_contratoservicostelas_servicosiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicostelas_servicosiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicostelas_servicosiglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosTelas_Tela";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_tela_Internalname, "TitleControlIdToReplace", Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace);
         AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace = Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace", AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace);
         edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicostelas_link_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosTelas_Link";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_link_Internalname, "TitleControlIdToReplace", Ddo_contratoservicostelas_link_Titlecontrolidtoreplace);
         AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace = Ddo_contratoservicostelas_link_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace", AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace);
         edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosTelas_Parms";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_parms_Internalname, "TitleControlIdToReplace", Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace);
         AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace = Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace", AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace);
         edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicostelas_status_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosTelas_Status";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_status_Internalname, "TitleControlIdToReplace", Ddo_contratoservicostelas_status_Titlecontrolidtoreplace);
         AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace = Ddo_contratoservicostelas_status_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace", AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace);
         edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Contrato Servicos Telas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtContratoServicosTelas_Sequencial_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_Sequencial_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosTelas_Sequencial_Visible), 5, 0)));
         edtContratoServicosTelas_ServicoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_ServicoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosTelas_ServicoCod_Visible), 5, 0)));
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Servi�o", 0);
         cmbavOrderedby.addItem("2", "Contratada", 0);
         cmbavOrderedby.addItem("3", "Servi�o", 0);
         cmbavOrderedby.addItem("4", "Tela", 0);
         cmbavOrderedby.addItem("5", "Link", 0);
         cmbavOrderedby.addItem("6", "Par�metros", 0);
         cmbavOrderedby.addItem("7", "Status", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV56DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV56DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E29G82( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV32Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36ContratoServicosTelas_ServicoSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40ContratoServicosTelas_TelaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44ContratoServicosTelas_LinkTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48ContratoServicosTelas_ParmsTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ContratoServicosTelas_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratada_PessoaNom_Titleformat = 2;
         edtContratada_PessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contratada", AV35ddo_Contratada_PessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Title", edtContratada_PessoaNom_Title);
         edtContratoServicosTelas_ServicoSigla_Titleformat = 2;
         edtContratoServicosTelas_ServicoSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Servi�o", AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_ServicoSigla_Internalname, "Title", edtContratoServicosTelas_ServicoSigla_Title);
         edtContratoServicosTelas_Tela_Titleformat = 2;
         edtContratoServicosTelas_Tela_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tela", AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_Tela_Internalname, "Title", edtContratoServicosTelas_Tela_Title);
         edtContratoServicosTelas_Link_Titleformat = 2;
         edtContratoServicosTelas_Link_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Link", AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_Link_Internalname, "Title", edtContratoServicosTelas_Link_Title);
         edtContratoServicosTelas_Parms_Titleformat = 2;
         edtContratoServicosTelas_Parms_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Par�metros", AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_Parms_Internalname, "Title", edtContratoServicosTelas_Parms_Title);
         cmbContratoServicosTelas_Status_Titleformat = 2;
         cmbContratoServicosTelas_Status.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Status", AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosTelas_Status_Internalname, "Title", cmbContratoServicosTelas_Status.Title.Text);
         AV58GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58GridCurrentPage), 10, 0)));
         AV59GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32Contratada_PessoaNomTitleFilterData", AV32Contratada_PessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36ContratoServicosTelas_ServicoSiglaTitleFilterData", AV36ContratoServicosTelas_ServicoSiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV40ContratoServicosTelas_TelaTitleFilterData", AV40ContratoServicosTelas_TelaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44ContratoServicosTelas_LinkTitleFilterData", AV44ContratoServicosTelas_LinkTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48ContratoServicosTelas_ParmsTitleFilterData", AV48ContratoServicosTelas_ParmsTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52ContratoServicosTelas_StatusTitleFilterData", AV52ContratoServicosTelas_StatusTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11G82( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV57PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV57PageToGo) ;
         }
      }

      protected void E12G82( )
      {
         /* Ddo_contratada_pessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV33TFContratada_PessoaNom = Ddo_contratada_pessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratada_PessoaNom", AV33TFContratada_PessoaNom);
            AV34TFContratada_PessoaNom_Sel = Ddo_contratada_pessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratada_PessoaNom_Sel", AV34TFContratada_PessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13G82( )
      {
         /* Ddo_contratoservicostelas_servicosigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicostelas_servicosigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_servicosigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_servicosigla_Internalname, "SortedStatus", Ddo_contratoservicostelas_servicosigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_servicosigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_servicosigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_servicosigla_Internalname, "SortedStatus", Ddo_contratoservicostelas_servicosigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_servicosigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFContratoServicosTelas_ServicoSigla = Ddo_contratoservicostelas_servicosigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosTelas_ServicoSigla", AV37TFContratoServicosTelas_ServicoSigla);
            AV38TFContratoServicosTelas_ServicoSigla_Sel = Ddo_contratoservicostelas_servicosigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoServicosTelas_ServicoSigla_Sel", AV38TFContratoServicosTelas_ServicoSigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14G82( )
      {
         /* Ddo_contratoservicostelas_tela_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicostelas_tela_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_tela_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_tela_Internalname, "SortedStatus", Ddo_contratoservicostelas_tela_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_tela_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_tela_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_tela_Internalname, "SortedStatus", Ddo_contratoservicostelas_tela_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_tela_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV41TFContratoServicosTelas_Tela = Ddo_contratoservicostelas_tela_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosTelas_Tela", AV41TFContratoServicosTelas_Tela);
            AV42TFContratoServicosTelas_Tela_Sel = Ddo_contratoservicostelas_tela_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoServicosTelas_Tela_Sel", AV42TFContratoServicosTelas_Tela_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15G82( )
      {
         /* Ddo_contratoservicostelas_link_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicostelas_link_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_link_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_link_Internalname, "SortedStatus", Ddo_contratoservicostelas_link_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_link_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_link_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_link_Internalname, "SortedStatus", Ddo_contratoservicostelas_link_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_link_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFContratoServicosTelas_Link = Ddo_contratoservicostelas_link_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosTelas_Link", AV45TFContratoServicosTelas_Link);
            AV46TFContratoServicosTelas_Link_Sel = Ddo_contratoservicostelas_link_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContratoServicosTelas_Link_Sel", AV46TFContratoServicosTelas_Link_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16G82( )
      {
         /* Ddo_contratoservicostelas_parms_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicostelas_parms_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_parms_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_parms_Internalname, "SortedStatus", Ddo_contratoservicostelas_parms_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_parms_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_parms_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_parms_Internalname, "SortedStatus", Ddo_contratoservicostelas_parms_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_parms_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFContratoServicosTelas_Parms = Ddo_contratoservicostelas_parms_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicosTelas_Parms", AV49TFContratoServicosTelas_Parms);
            AV50TFContratoServicosTelas_Parms_Sel = Ddo_contratoservicostelas_parms_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoServicosTelas_Parms_Sel", AV50TFContratoServicosTelas_Parms_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17G82( )
      {
         /* Ddo_contratoservicostelas_status_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicostelas_status_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_status_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_status_Internalname, "SortedStatus", Ddo_contratoservicostelas_status_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_status_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_status_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_status_Internalname, "SortedStatus", Ddo_contratoservicostelas_status_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_status_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV53TFContratoServicosTelas_Status_SelsJson = Ddo_contratoservicostelas_status_Selectedvalue_get;
            AV54TFContratoServicosTelas_Status_Sels.FromJSonString(AV53TFContratoServicosTelas_Status_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54TFContratoServicosTelas_Status_Sels", AV54TFContratoServicosTelas_Status_Sels);
      }

      private void E30G82( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV62Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 80;
         }
         sendrow_802( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_80_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(80, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E31G82 */
         E31G82 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E31G82( )
      {
         /* Enter Routine */
         AV7InOutContratoServicosTelas_ContratoCod = A926ContratoServicosTelas_ContratoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoServicosTelas_ContratoCod), 6, 0)));
         AV31InOutContratoServicosTelas_Sequencial = A938ContratoServicosTelas_Sequencial;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31InOutContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31InOutContratoServicosTelas_Sequencial), 3, 0)));
         AV8InOutContratoServicosTelas_ServicoCod = A925ContratoServicosTelas_ServicoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoServicosTelas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContratoServicosTelas_ServicoCod), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratoServicosTelas_ContratoCod,(short)AV31InOutContratoServicosTelas_Sequencial,(int)AV8InOutContratoServicosTelas_ServicoCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E18G82( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E23G82( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E19G82( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosTelas_ServicoCod1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosTelas_ServicoCod2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosTelas_ServicoCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV33TFContratada_PessoaNom, AV34TFContratada_PessoaNom_Sel, AV37TFContratoServicosTelas_ServicoSigla, AV38TFContratoServicosTelas_ServicoSigla_Sel, AV41TFContratoServicosTelas_Tela, AV42TFContratoServicosTelas_Tela_Sel, AV45TFContratoServicosTelas_Link, AV46TFContratoServicosTelas_Link_Sel, AV49TFContratoServicosTelas_Parms, AV50TFContratoServicosTelas_Parms_Sel, AV35ddo_Contratada_PessoaNomTitleControlIdToReplace, AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace, AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV54TFContratoServicosTelas_Status_Sels, AV63Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         dynavContratoservicostelas_servicocod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod2_Internalname, "Values", dynavContratoservicostelas_servicocod2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         dynavContratoservicostelas_servicocod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod3_Internalname, "Values", dynavContratoservicostelas_servicocod3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContratoservicostelas_servicocod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod1_Internalname, "Values", dynavContratoservicostelas_servicocod1.ToJavascriptSource());
      }

      protected void E24G82( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25G82( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E20G82( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosTelas_ServicoCod1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosTelas_ServicoCod2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosTelas_ServicoCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV33TFContratada_PessoaNom, AV34TFContratada_PessoaNom_Sel, AV37TFContratoServicosTelas_ServicoSigla, AV38TFContratoServicosTelas_ServicoSigla_Sel, AV41TFContratoServicosTelas_Tela, AV42TFContratoServicosTelas_Tela_Sel, AV45TFContratoServicosTelas_Link, AV46TFContratoServicosTelas_Link_Sel, AV49TFContratoServicosTelas_Parms, AV50TFContratoServicosTelas_Parms_Sel, AV35ddo_Contratada_PessoaNomTitleControlIdToReplace, AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace, AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV54TFContratoServicosTelas_Status_Sels, AV63Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         dynavContratoservicostelas_servicocod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod2_Internalname, "Values", dynavContratoservicostelas_servicocod2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         dynavContratoservicostelas_servicocod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod3_Internalname, "Values", dynavContratoservicostelas_servicocod3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContratoservicostelas_servicocod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod1_Internalname, "Values", dynavContratoservicostelas_servicocod1.ToJavascriptSource());
      }

      protected void E26G82( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21G82( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosTelas_ServicoCod1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosTelas_ServicoCod2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosTelas_ServicoCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV33TFContratada_PessoaNom, AV34TFContratada_PessoaNom_Sel, AV37TFContratoServicosTelas_ServicoSigla, AV38TFContratoServicosTelas_ServicoSigla_Sel, AV41TFContratoServicosTelas_Tela, AV42TFContratoServicosTelas_Tela_Sel, AV45TFContratoServicosTelas_Link, AV46TFContratoServicosTelas_Link_Sel, AV49TFContratoServicosTelas_Parms, AV50TFContratoServicosTelas_Parms_Sel, AV35ddo_Contratada_PessoaNomTitleControlIdToReplace, AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace, AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV54TFContratoServicosTelas_Status_Sels, AV63Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         dynavContratoservicostelas_servicocod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod2_Internalname, "Values", dynavContratoservicostelas_servicocod2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         dynavContratoservicostelas_servicocod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod3_Internalname, "Values", dynavContratoservicostelas_servicocod3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContratoservicostelas_servicocod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod1_Internalname, "Values", dynavContratoservicostelas_servicocod1.ToJavascriptSource());
      }

      protected void E27G82( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22G82( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54TFContratoServicosTelas_Status_Sels", AV54TFContratoServicosTelas_Status_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContratoservicostelas_servicocod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod1_Internalname, "Values", dynavContratoservicostelas_servicocod1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         dynavContratoservicostelas_servicocod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod2_Internalname, "Values", dynavContratoservicostelas_servicocod2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         dynavContratoservicostelas_servicocod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod3_Internalname, "Values", dynavContratoservicostelas_servicocod3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratada_pessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         Ddo_contratoservicostelas_servicosigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_servicosigla_Internalname, "SortedStatus", Ddo_contratoservicostelas_servicosigla_Sortedstatus);
         Ddo_contratoservicostelas_tela_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_tela_Internalname, "SortedStatus", Ddo_contratoservicostelas_tela_Sortedstatus);
         Ddo_contratoservicostelas_link_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_link_Internalname, "SortedStatus", Ddo_contratoservicostelas_link_Sortedstatus);
         Ddo_contratoservicostelas_parms_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_parms_Internalname, "SortedStatus", Ddo_contratoservicostelas_parms_Sortedstatus);
         Ddo_contratoservicostelas_status_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_status_Internalname, "SortedStatus", Ddo_contratoservicostelas_status_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contratada_pessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoservicostelas_servicosigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_servicosigla_Internalname, "SortedStatus", Ddo_contratoservicostelas_servicosigla_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratoservicostelas_tela_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_tela_Internalname, "SortedStatus", Ddo_contratoservicostelas_tela_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratoservicostelas_link_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_link_Internalname, "SortedStatus", Ddo_contratoservicostelas_link_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contratoservicostelas_parms_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_parms_Internalname, "SortedStatus", Ddo_contratoservicostelas_parms_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contratoservicostelas_status_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_status_Internalname, "SortedStatus", Ddo_contratoservicostelas_status_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         dynavContratoservicostelas_servicocod1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratoservicostelas_servicocod1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 )
         {
            dynavContratoservicostelas_servicocod1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratoservicostelas_servicocod1.Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         dynavContratoservicostelas_servicocod2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratoservicostelas_servicocod2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 )
         {
            dynavContratoservicostelas_servicocod2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratoservicostelas_servicocod2.Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         dynavContratoservicostelas_servicocod3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratoservicostelas_servicocod3.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 )
         {
            dynavContratoservicostelas_servicocod3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratoservicostelas_servicocod3.Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATOSERVICOSTELAS_SERVICOCOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21ContratoServicosTelas_ServicoCod2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosTelas_ServicoCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CONTRATOSERVICOSTELAS_SERVICOCOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25ContratoServicosTelas_ServicoCod3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosTelas_ServicoCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV33TFContratada_PessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratada_PessoaNom", AV33TFContratada_PessoaNom);
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "FilteredText_set", Ddo_contratada_pessoanom_Filteredtext_set);
         AV34TFContratada_PessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratada_PessoaNom_Sel", AV34TFContratada_PessoaNom_Sel);
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SelectedValue_set", Ddo_contratada_pessoanom_Selectedvalue_set);
         AV37TFContratoServicosTelas_ServicoSigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosTelas_ServicoSigla", AV37TFContratoServicosTelas_ServicoSigla);
         Ddo_contratoservicostelas_servicosigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_servicosigla_Internalname, "FilteredText_set", Ddo_contratoservicostelas_servicosigla_Filteredtext_set);
         AV38TFContratoServicosTelas_ServicoSigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoServicosTelas_ServicoSigla_Sel", AV38TFContratoServicosTelas_ServicoSigla_Sel);
         Ddo_contratoservicostelas_servicosigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_servicosigla_Internalname, "SelectedValue_set", Ddo_contratoservicostelas_servicosigla_Selectedvalue_set);
         AV41TFContratoServicosTelas_Tela = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosTelas_Tela", AV41TFContratoServicosTelas_Tela);
         Ddo_contratoservicostelas_tela_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_tela_Internalname, "FilteredText_set", Ddo_contratoservicostelas_tela_Filteredtext_set);
         AV42TFContratoServicosTelas_Tela_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoServicosTelas_Tela_Sel", AV42TFContratoServicosTelas_Tela_Sel);
         Ddo_contratoservicostelas_tela_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_tela_Internalname, "SelectedValue_set", Ddo_contratoservicostelas_tela_Selectedvalue_set);
         AV45TFContratoServicosTelas_Link = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosTelas_Link", AV45TFContratoServicosTelas_Link);
         Ddo_contratoservicostelas_link_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_link_Internalname, "FilteredText_set", Ddo_contratoservicostelas_link_Filteredtext_set);
         AV46TFContratoServicosTelas_Link_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContratoServicosTelas_Link_Sel", AV46TFContratoServicosTelas_Link_Sel);
         Ddo_contratoservicostelas_link_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_link_Internalname, "SelectedValue_set", Ddo_contratoservicostelas_link_Selectedvalue_set);
         AV49TFContratoServicosTelas_Parms = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicosTelas_Parms", AV49TFContratoServicosTelas_Parms);
         Ddo_contratoservicostelas_parms_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_parms_Internalname, "FilteredText_set", Ddo_contratoservicostelas_parms_Filteredtext_set);
         AV50TFContratoServicosTelas_Parms_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoServicosTelas_Parms_Sel", AV50TFContratoServicosTelas_Parms_Sel);
         Ddo_contratoservicostelas_parms_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_parms_Internalname, "SelectedValue_set", Ddo_contratoservicostelas_parms_Selectedvalue_set);
         AV54TFContratoServicosTelas_Status_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_contratoservicostelas_status_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicostelas_status_Internalname, "SelectedValue_set", Ddo_contratoservicostelas_status_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "CONTRATOSERVICOSTELAS_SERVICOCOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17ContratoServicosTelas_ServicoCod1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosTelas_ServicoCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoServicosTelas_ServicoCod1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosTelas_ServicoCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21ContratoServicosTelas_ServicoCod2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosTelas_ServicoCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25ContratoServicosTelas_ServicoCod3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosTelas_ServicoCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratada_PessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV33TFContratada_PessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratada_PessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV34TFContratada_PessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFContratoServicosTelas_ServicoSigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_SERVICOSIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV37TFContratoServicosTelas_ServicoSigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFContratoServicosTelas_ServicoSigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV38TFContratoServicosTelas_ServicoSigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFContratoServicosTelas_Tela)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_TELA";
            AV11GridStateFilterValue.gxTpr_Value = AV41TFContratoServicosTelas_Tela;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratoServicosTelas_Tela_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_TELA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV42TFContratoServicosTelas_Tela_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContratoServicosTelas_Link)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_LINK";
            AV11GridStateFilterValue.gxTpr_Value = AV45TFContratoServicosTelas_Link;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratoServicosTelas_Link_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_LINK_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV46TFContratoServicosTelas_Link_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContratoServicosTelas_Parms)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_PARMS";
            AV11GridStateFilterValue.gxTpr_Value = AV49TFContratoServicosTelas_Parms;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContratoServicosTelas_Parms_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_PARMS_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV50TFContratoServicosTelas_Parms_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV54TFContratoServicosTelas_Status_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_STATUS_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV54TFContratoServicosTelas_Status_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV63Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ! (0==AV17ContratoServicosTelas_ServicoCod1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ! (0==AV21ContratoServicosTelas_ServicoCod2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ! (0==AV25ContratoServicosTelas_ServicoCod3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_G82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_G82( true) ;
         }
         else
         {
            wb_table2_5_G82( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_G82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_74_G82( true) ;
         }
         else
         {
            wb_table3_74_G82( false) ;
         }
         return  ;
      }

      protected void wb_table3_74_G82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_G82e( true) ;
         }
         else
         {
            wb_table1_2_G82e( false) ;
         }
      }

      protected void wb_table3_74_G82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_77_G82( true) ;
         }
         else
         {
            wb_table4_77_G82( false) ;
         }
         return  ;
      }

      protected void wb_table4_77_G82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_74_G82e( true) ;
         }
         else
         {
            wb_table3_74_G82e( false) ;
         }
      }

      protected void wb_table4_77_G82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"80\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contrato") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosTelas_ServicoSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosTelas_ServicoSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosTelas_ServicoSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosTelas_Tela_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosTelas_Tela_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosTelas_Tela_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosTelas_Link_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosTelas_Link_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosTelas_Link_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosTelas_Parms_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosTelas_Parms_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosTelas_Parms_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoServicosTelas_Status_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoServicosTelas_Status.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoServicosTelas_Status.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A41Contratada_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A937ContratoServicosTelas_ServicoSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosTelas_ServicoSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosTelas_ServicoSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A931ContratoServicosTelas_Tela));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosTelas_Tela_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosTelas_Tela_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A928ContratoServicosTelas_Link);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosTelas_Link_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosTelas_Link_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A929ContratoServicosTelas_Parms);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosTelas_Parms_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosTelas_Parms_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A932ContratoServicosTelas_Status));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoServicosTelas_Status.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoServicosTelas_Status_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 80 )
         {
            wbEnd = 0;
            nRC_GXsfl_80 = (short)(nGXsfl_80_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_77_G82e( true) ;
         }
         else
         {
            wb_table4_77_G82e( false) ;
         }
      }

      protected void wb_table2_5_G82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratoServicosTelas.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_G82( true) ;
         }
         else
         {
            wb_table5_14_G82( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_G82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_G82e( true) ;
         }
         else
         {
            wb_table2_5_G82e( false) ;
         }
      }

      protected void wb_table5_14_G82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_G82( true) ;
         }
         else
         {
            wb_table6_19_G82( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_G82e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_G82e( true) ;
         }
         else
         {
            wb_table5_14_G82e( false) ;
         }
      }

      protected void wb_table6_19_G82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContratoServicosTelas.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_G82( true) ;
         }
         else
         {
            wb_table7_28_G82( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_G82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosTelas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_PromptContratoServicosTelas.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_45_G82( true) ;
         }
         else
         {
            wb_table8_45_G82( false) ;
         }
         return  ;
      }

      protected void wb_table8_45_G82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosTelas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_PromptContratoServicosTelas.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_62_G82( true) ;
         }
         else
         {
            wb_table9_62_G82( false) ;
         }
         return  ;
      }

      protected void wb_table9_62_G82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_G82e( true) ;
         }
         else
         {
            wb_table6_19_G82e( false) ;
         }
      }

      protected void wb_table9_62_G82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_PromptContratoServicosTelas.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratoservicostelas_servicocod3, dynavContratoservicostelas_servicocod3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0)), 1, dynavContratoservicostelas_servicocod3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContratoservicostelas_servicocod3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_PromptContratoServicosTelas.htm");
            dynavContratoservicostelas_servicocod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25ContratoServicosTelas_ServicoCod3), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod3_Internalname, "Values", (String)(dynavContratoservicostelas_servicocod3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_62_G82e( true) ;
         }
         else
         {
            wb_table9_62_G82e( false) ;
         }
      }

      protected void wb_table8_45_G82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptContratoServicosTelas.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratoservicostelas_servicocod2, dynavContratoservicostelas_servicocod2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0)), 1, dynavContratoservicostelas_servicocod2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContratoservicostelas_servicocod2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_PromptContratoServicosTelas.htm");
            dynavContratoservicostelas_servicocod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContratoServicosTelas_ServicoCod2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod2_Internalname, "Values", (String)(dynavContratoservicostelas_servicocod2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_45_G82e( true) ;
         }
         else
         {
            wb_table8_45_G82e( false) ;
         }
      }

      protected void wb_table7_28_G82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContratoServicosTelas.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratoservicostelas_servicocod1, dynavContratoservicostelas_servicocod1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0)), 1, dynavContratoservicostelas_servicocod1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContratoservicostelas_servicocod1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_PromptContratoServicosTelas.htm");
            dynavContratoservicostelas_servicocod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContratoServicosTelas_ServicoCod1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicostelas_servicocod1_Internalname, "Values", (String)(dynavContratoservicostelas_servicocod1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_G82e( true) ;
         }
         else
         {
            wb_table7_28_G82e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratoServicosTelas_ContratoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoServicosTelas_ContratoCod), 6, 0)));
         AV31InOutContratoServicosTelas_Sequencial = Convert.ToInt16(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31InOutContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31InOutContratoServicosTelas_Sequencial), 3, 0)));
         AV8InOutContratoServicosTelas_ServicoCod = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoServicosTelas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContratoServicosTelas_ServicoCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAG82( ) ;
         WSG82( ) ;
         WEG82( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299391219");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratoservicostelas.js", "?20205299391219");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_idx;
         edtContratoServicosTelas_ContratoCod_Internalname = "CONTRATOSERVICOSTELAS_CONTRATOCOD_"+sGXsfl_80_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_80_idx;
         edtContratoServicosTelas_ServicoSigla_Internalname = "CONTRATOSERVICOSTELAS_SERVICOSIGLA_"+sGXsfl_80_idx;
         edtContratoServicosTelas_Tela_Internalname = "CONTRATOSERVICOSTELAS_TELA_"+sGXsfl_80_idx;
         edtContratoServicosTelas_Link_Internalname = "CONTRATOSERVICOSTELAS_LINK_"+sGXsfl_80_idx;
         edtContratoServicosTelas_Parms_Internalname = "CONTRATOSERVICOSTELAS_PARMS_"+sGXsfl_80_idx;
         cmbContratoServicosTelas_Status_Internalname = "CONTRATOSERVICOSTELAS_STATUS_"+sGXsfl_80_idx;
      }

      protected void SubsflControlProps_fel_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_fel_idx;
         edtContratoServicosTelas_ContratoCod_Internalname = "CONTRATOSERVICOSTELAS_CONTRATOCOD_"+sGXsfl_80_fel_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_80_fel_idx;
         edtContratoServicosTelas_ServicoSigla_Internalname = "CONTRATOSERVICOSTELAS_SERVICOSIGLA_"+sGXsfl_80_fel_idx;
         edtContratoServicosTelas_Tela_Internalname = "CONTRATOSERVICOSTELAS_TELA_"+sGXsfl_80_fel_idx;
         edtContratoServicosTelas_Link_Internalname = "CONTRATOSERVICOSTELAS_LINK_"+sGXsfl_80_fel_idx;
         edtContratoServicosTelas_Parms_Internalname = "CONTRATOSERVICOSTELAS_PARMS_"+sGXsfl_80_fel_idx;
         cmbContratoServicosTelas_Status_Internalname = "CONTRATOSERVICOSTELAS_STATUS_"+sGXsfl_80_fel_idx;
      }

      protected void sendrow_802( )
      {
         SubsflControlProps_802( ) ;
         WBG80( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_80_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_80_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_80_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 81,'',false,'',80)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV62Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV62Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_80_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosTelas_ContratoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A926ContratoServicosTelas_ContratoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosTelas_ContratoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaNom_Internalname,StringUtil.RTrim( A41Contratada_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosTelas_ServicoSigla_Internalname,StringUtil.RTrim( A937ContratoServicosTelas_ServicoSigla),StringUtil.RTrim( context.localUtil.Format( A937ContratoServicosTelas_ServicoSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosTelas_ServicoSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosTelas_Tela_Internalname,StringUtil.RTrim( A931ContratoServicosTelas_Tela),StringUtil.RTrim( context.localUtil.Format( A931ContratoServicosTelas_Tela, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosTelas_Tela_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosTelas_Link_Internalname,(String)A928ContratoServicosTelas_Link,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosTelas_Link_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)80,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"LinkMenu",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosTelas_Parms_Internalname,(String)A929ContratoServicosTelas_Parms,(String)A929ContratoServicosTelas_Parms,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosTelas_Parms_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)80,(short)1,(short)0,(short)0,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSERVICOSTELAS_STATUS_" + sGXsfl_80_idx;
               cmbContratoServicosTelas_Status.Name = GXCCtl;
               cmbContratoServicosTelas_Status.WebTags = "";
               cmbContratoServicosTelas_Status.addItem("", "Todos", 0);
               cmbContratoServicosTelas_Status.addItem("B", "Stand by", 0);
               cmbContratoServicosTelas_Status.addItem("S", "Solicitada", 0);
               cmbContratoServicosTelas_Status.addItem("E", "Em An�lise", 0);
               cmbContratoServicosTelas_Status.addItem("A", "Em execu��o", 0);
               cmbContratoServicosTelas_Status.addItem("R", "Resolvida", 0);
               cmbContratoServicosTelas_Status.addItem("C", "Conferida", 0);
               cmbContratoServicosTelas_Status.addItem("D", "Retornada", 0);
               cmbContratoServicosTelas_Status.addItem("H", "Homologada", 0);
               cmbContratoServicosTelas_Status.addItem("O", "Aceite", 0);
               cmbContratoServicosTelas_Status.addItem("P", "A Pagar", 0);
               cmbContratoServicosTelas_Status.addItem("L", "Liquidada", 0);
               cmbContratoServicosTelas_Status.addItem("X", "Cancelada", 0);
               cmbContratoServicosTelas_Status.addItem("N", "N�o Faturada", 0);
               cmbContratoServicosTelas_Status.addItem("J", "Planejamento", 0);
               cmbContratoServicosTelas_Status.addItem("I", "An�lise Planejamento", 0);
               cmbContratoServicosTelas_Status.addItem("T", "Validacao T�cnica", 0);
               cmbContratoServicosTelas_Status.addItem("Q", "Validacao Qualidade", 0);
               cmbContratoServicosTelas_Status.addItem("G", "Em Homologa��o", 0);
               cmbContratoServicosTelas_Status.addItem("M", "Valida��o Mensura��o", 0);
               cmbContratoServicosTelas_Status.addItem("U", "Rascunho", 0);
               if ( cmbContratoServicosTelas_Status.ItemCount > 0 )
               {
                  A932ContratoServicosTelas_Status = cmbContratoServicosTelas_Status.getValidValue(A932ContratoServicosTelas_Status);
                  n932ContratoServicosTelas_Status = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoServicosTelas_Status,(String)cmbContratoServicosTelas_Status_Internalname,StringUtil.RTrim( A932ContratoServicosTelas_Status),(short)1,(String)cmbContratoServicosTelas_Status_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoServicosTelas_Status.CurrentValue = StringUtil.RTrim( A932ContratoServicosTelas_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosTelas_Status_Internalname, "Values", (String)(cmbContratoServicosTelas_Status.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSTELAS_CONTRATOCOD"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A926ContratoServicosTelas_ContratoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSTELAS_TELA"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A931ContratoServicosTelas_Tela, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSTELAS_LINK"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A928ContratoServicosTelas_Link, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSTELAS_PARMS"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, A929ContratoServicosTelas_Parms));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSTELAS_STATUS"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A932ContratoServicosTelas_Status, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         /* End function sendrow_802 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         dynavContratoservicostelas_servicocod1_Internalname = "vCONTRATOSERVICOSTELAS_SERVICOCOD1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         dynavContratoservicostelas_servicocod2_Internalname = "vCONTRATOSERVICOSTELAS_SERVICOCOD2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         dynavContratoservicostelas_servicocod3_Internalname = "vCONTRATOSERVICOSTELAS_SERVICOCOD3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContratoServicosTelas_ContratoCod_Internalname = "CONTRATOSERVICOSTELAS_CONTRATOCOD";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         edtContratoServicosTelas_ServicoSigla_Internalname = "CONTRATOSERVICOSTELAS_SERVICOSIGLA";
         edtContratoServicosTelas_Tela_Internalname = "CONTRATOSERVICOSTELAS_TELA";
         edtContratoServicosTelas_Link_Internalname = "CONTRATOSERVICOSTELAS_LINK";
         edtContratoServicosTelas_Parms_Internalname = "CONTRATOSERVICOSTELAS_PARMS";
         cmbContratoServicosTelas_Status_Internalname = "CONTRATOSERVICOSTELAS_STATUS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratoServicosTelas_Sequencial_Internalname = "CONTRATOSERVICOSTELAS_SEQUENCIAL";
         edtContratoServicosTelas_ServicoCod_Internalname = "CONTRATOSERVICOSTELAS_SERVICOCOD";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontratada_pessoanom_Internalname = "vTFCONTRATADA_PESSOANOM";
         edtavTfcontratada_pessoanom_sel_Internalname = "vTFCONTRATADA_PESSOANOM_SEL";
         edtavTfcontratoservicostelas_servicosigla_Internalname = "vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA";
         edtavTfcontratoservicostelas_servicosigla_sel_Internalname = "vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL";
         edtavTfcontratoservicostelas_tela_Internalname = "vTFCONTRATOSERVICOSTELAS_TELA";
         edtavTfcontratoservicostelas_tela_sel_Internalname = "vTFCONTRATOSERVICOSTELAS_TELA_SEL";
         edtavTfcontratoservicostelas_link_Internalname = "vTFCONTRATOSERVICOSTELAS_LINK";
         edtavTfcontratoservicostelas_link_sel_Internalname = "vTFCONTRATOSERVICOSTELAS_LINK_SEL";
         edtavTfcontratoservicostelas_parms_Internalname = "vTFCONTRATOSERVICOSTELAS_PARMS";
         edtavTfcontratoservicostelas_parms_sel_Internalname = "vTFCONTRATOSERVICOSTELAS_PARMS_SEL";
         Ddo_contratada_pessoanom_Internalname = "DDO_CONTRATADA_PESSOANOM";
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicostelas_servicosigla_Internalname = "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA";
         edtavDdo_contratoservicostelas_servicosiglatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE";
         Ddo_contratoservicostelas_tela_Internalname = "DDO_CONTRATOSERVICOSTELAS_TELA";
         edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE";
         Ddo_contratoservicostelas_link_Internalname = "DDO_CONTRATOSERVICOSTELAS_LINK";
         edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicostelas_parms_Internalname = "DDO_CONTRATOSERVICOSTELAS_PARMS";
         edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicostelas_status_Internalname = "DDO_CONTRATOSERVICOSTELAS_STATUS";
         edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbContratoServicosTelas_Status_Jsonclick = "";
         edtContratoServicosTelas_Parms_Jsonclick = "";
         edtContratoServicosTelas_Link_Jsonclick = "";
         edtContratoServicosTelas_Tela_Jsonclick = "";
         edtContratoServicosTelas_ServicoSigla_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratoServicosTelas_ContratoCod_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         dynavContratoservicostelas_servicocod1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         dynavContratoservicostelas_servicocod2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         dynavContratoservicostelas_servicocod3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         cmbContratoServicosTelas_Status_Titleformat = 0;
         edtContratoServicosTelas_Parms_Titleformat = 0;
         edtContratoServicosTelas_Link_Titleformat = 0;
         edtContratoServicosTelas_Tela_Titleformat = 0;
         edtContratoServicosTelas_ServicoSigla_Titleformat = 0;
         edtContratada_PessoaNom_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         dynavContratoservicostelas_servicocod3.Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         dynavContratoservicostelas_servicocod2.Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         dynavContratoservicostelas_servicocod1.Visible = 1;
         cmbContratoServicosTelas_Status.Title.Text = "Status";
         edtContratoServicosTelas_Parms_Title = "Par�metros";
         edtContratoServicosTelas_Link_Title = "Link";
         edtContratoServicosTelas_Tela_Title = "Tela";
         edtContratoServicosTelas_ServicoSigla_Title = "Servi�o";
         edtContratada_PessoaNom_Title = "Contratada";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicostelas_servicosiglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicostelas_parms_sel_Visible = 1;
         edtavTfcontratoservicostelas_parms_Visible = 1;
         edtavTfcontratoservicostelas_link_sel_Jsonclick = "";
         edtavTfcontratoservicostelas_link_sel_Visible = 1;
         edtavTfcontratoservicostelas_link_Jsonclick = "";
         edtavTfcontratoservicostelas_link_Visible = 1;
         edtavTfcontratoservicostelas_tela_sel_Jsonclick = "";
         edtavTfcontratoservicostelas_tela_sel_Visible = 1;
         edtavTfcontratoservicostelas_tela_Jsonclick = "";
         edtavTfcontratoservicostelas_tela_Visible = 1;
         edtavTfcontratoservicostelas_servicosigla_sel_Jsonclick = "";
         edtavTfcontratoservicostelas_servicosigla_sel_Visible = 1;
         edtavTfcontratoservicostelas_servicosigla_Jsonclick = "";
         edtavTfcontratoservicostelas_servicosigla_Visible = 1;
         edtavTfcontratada_pessoanom_sel_Jsonclick = "";
         edtavTfcontratada_pessoanom_sel_Visible = 1;
         edtavTfcontratada_pessoanom_Jsonclick = "";
         edtavTfcontratada_pessoanom_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtContratoServicosTelas_ServicoCod_Jsonclick = "";
         edtContratoServicosTelas_ServicoCod_Visible = 1;
         edtContratoServicosTelas_Sequencial_Jsonclick = "";
         edtContratoServicosTelas_Sequencial_Visible = 1;
         Ddo_contratoservicostelas_status_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contratoservicostelas_status_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicostelas_status_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicostelas_status_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicostelas_status_Datalistfixedvalues = "B:Stand by,S:Solicitada,E:Em An�lise,A:Em execu��o,R:Resolvida,C:Conferida,D:Retornada,H:Homologada,O:Aceite,P:A Pagar,L:Liquidada,X:Cancelada,N:N�o Faturada,J:Planejamento,I:An�lise Planejamento,T:Validacao T�cnica,Q:Validacao Qualidade,G:Em Homologa��o,M:Valida��o Mensura��o,U:Rascunho";
         Ddo_contratoservicostelas_status_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_status_Datalisttype = "FixedValues";
         Ddo_contratoservicostelas_status_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_status_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratoservicostelas_status_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_status_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_status_Titlecontrolidtoreplace = "";
         Ddo_contratoservicostelas_status_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicostelas_status_Cls = "ColumnSettings";
         Ddo_contratoservicostelas_status_Tooltip = "Op��es";
         Ddo_contratoservicostelas_status_Caption = "";
         Ddo_contratoservicostelas_parms_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicostelas_parms_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicostelas_parms_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicostelas_parms_Loadingdata = "Carregando dados...";
         Ddo_contratoservicostelas_parms_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicostelas_parms_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicostelas_parms_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicostelas_parms_Datalistproc = "GetPromptContratoServicosTelasFilterData";
         Ddo_contratoservicostelas_parms_Datalisttype = "Dynamic";
         Ddo_contratoservicostelas_parms_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_parms_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicostelas_parms_Filtertype = "Character";
         Ddo_contratoservicostelas_parms_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_parms_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_parms_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace = "";
         Ddo_contratoservicostelas_parms_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicostelas_parms_Cls = "ColumnSettings";
         Ddo_contratoservicostelas_parms_Tooltip = "Op��es";
         Ddo_contratoservicostelas_parms_Caption = "";
         Ddo_contratoservicostelas_link_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicostelas_link_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicostelas_link_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicostelas_link_Loadingdata = "Carregando dados...";
         Ddo_contratoservicostelas_link_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicostelas_link_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicostelas_link_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicostelas_link_Datalistproc = "GetPromptContratoServicosTelasFilterData";
         Ddo_contratoservicostelas_link_Datalisttype = "Dynamic";
         Ddo_contratoservicostelas_link_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_link_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicostelas_link_Filtertype = "Character";
         Ddo_contratoservicostelas_link_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_link_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_link_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_link_Titlecontrolidtoreplace = "";
         Ddo_contratoservicostelas_link_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicostelas_link_Cls = "ColumnSettings";
         Ddo_contratoservicostelas_link_Tooltip = "Op��es";
         Ddo_contratoservicostelas_link_Caption = "";
         Ddo_contratoservicostelas_tela_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicostelas_tela_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicostelas_tela_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicostelas_tela_Loadingdata = "Carregando dados...";
         Ddo_contratoservicostelas_tela_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicostelas_tela_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicostelas_tela_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicostelas_tela_Datalistproc = "GetPromptContratoServicosTelasFilterData";
         Ddo_contratoservicostelas_tela_Datalisttype = "Dynamic";
         Ddo_contratoservicostelas_tela_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_tela_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicostelas_tela_Filtertype = "Character";
         Ddo_contratoservicostelas_tela_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_tela_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_tela_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace = "";
         Ddo_contratoservicostelas_tela_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicostelas_tela_Cls = "ColumnSettings";
         Ddo_contratoservicostelas_tela_Tooltip = "Op��es";
         Ddo_contratoservicostelas_tela_Caption = "";
         Ddo_contratoservicostelas_servicosigla_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicostelas_servicosigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicostelas_servicosigla_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicostelas_servicosigla_Loadingdata = "Carregando dados...";
         Ddo_contratoservicostelas_servicosigla_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicostelas_servicosigla_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicostelas_servicosigla_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicostelas_servicosigla_Datalistproc = "GetPromptContratoServicosTelasFilterData";
         Ddo_contratoservicostelas_servicosigla_Datalisttype = "Dynamic";
         Ddo_contratoservicostelas_servicosigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_servicosigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicostelas_servicosigla_Filtertype = "Character";
         Ddo_contratoservicostelas_servicosigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_servicosigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_servicosigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_servicosigla_Titlecontrolidtoreplace = "";
         Ddo_contratoservicostelas_servicosigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicostelas_servicosigla_Cls = "ColumnSettings";
         Ddo_contratoservicostelas_servicosigla_Tooltip = "Op��es";
         Ddo_contratoservicostelas_servicosigla_Caption = "";
         Ddo_contratada_pessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoanom_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoanom_Datalistproc = "GetPromptContratoServicosTelasFilterData";
         Ddo_contratada_pessoanom_Datalisttype = "Dynamic";
         Ddo_contratada_pessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoanom_Filtertype = "Character";
         Ddo_contratada_pessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoanom_Cls = "ColumnSettings";
         Ddo_contratada_pessoanom_Tooltip = "Op��es";
         Ddo_contratada_pessoanom_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contrato Servicos Telas";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'AV32Contratada_PessoaNomTitleFilterData',fld:'vCONTRATADA_PESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV36ContratoServicosTelas_ServicoSiglaTitleFilterData',fld:'vCONTRATOSERVICOSTELAS_SERVICOSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV40ContratoServicosTelas_TelaTitleFilterData',fld:'vCONTRATOSERVICOSTELAS_TELATITLEFILTERDATA',pic:'',nv:null},{av:'AV44ContratoServicosTelas_LinkTitleFilterData',fld:'vCONTRATOSERVICOSTELAS_LINKTITLEFILTERDATA',pic:'',nv:null},{av:'AV48ContratoServicosTelas_ParmsTitleFilterData',fld:'vCONTRATOSERVICOSTELAS_PARMSTITLEFILTERDATA',pic:'',nv:null},{av:'AV52ContratoServicosTelas_StatusTitleFilterData',fld:'vCONTRATOSERVICOSTELAS_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratada_PessoaNom_Titleformat',ctrl:'CONTRATADA_PESSOANOM',prop:'Titleformat'},{av:'edtContratada_PessoaNom_Title',ctrl:'CONTRATADA_PESSOANOM',prop:'Title'},{av:'edtContratoServicosTelas_ServicoSigla_Titleformat',ctrl:'CONTRATOSERVICOSTELAS_SERVICOSIGLA',prop:'Titleformat'},{av:'edtContratoServicosTelas_ServicoSigla_Title',ctrl:'CONTRATOSERVICOSTELAS_SERVICOSIGLA',prop:'Title'},{av:'edtContratoServicosTelas_Tela_Titleformat',ctrl:'CONTRATOSERVICOSTELAS_TELA',prop:'Titleformat'},{av:'edtContratoServicosTelas_Tela_Title',ctrl:'CONTRATOSERVICOSTELAS_TELA',prop:'Title'},{av:'edtContratoServicosTelas_Link_Titleformat',ctrl:'CONTRATOSERVICOSTELAS_LINK',prop:'Titleformat'},{av:'edtContratoServicosTelas_Link_Title',ctrl:'CONTRATOSERVICOSTELAS_LINK',prop:'Title'},{av:'edtContratoServicosTelas_Parms_Titleformat',ctrl:'CONTRATOSERVICOSTELAS_PARMS',prop:'Titleformat'},{av:'edtContratoServicosTelas_Parms_Title',ctrl:'CONTRATOSERVICOSTELAS_PARMS',prop:'Title'},{av:'cmbContratoServicosTelas_Status'},{av:'AV58GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV59GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11G82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV35ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED","{handler:'E12G82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV35ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratada_pessoanom_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoanom_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoanom_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratoservicostelas_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_tela_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_link_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_parms_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_status_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA.ONOPTIONCLICKED","{handler:'E13G82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV35ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicostelas_servicosigla_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA',prop:'ActiveEventKey'},{av:'Ddo_contratoservicostelas_servicosigla_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA',prop:'FilteredText_get'},{av:'Ddo_contratoservicostelas_servicosigla_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicostelas_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA',prop:'SortedStatus'},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_tela_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_link_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_parms_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_status_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSTELAS_TELA.ONOPTIONCLICKED","{handler:'E14G82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV35ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicostelas_tela_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'ActiveEventKey'},{av:'Ddo_contratoservicostelas_tela_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'FilteredText_get'},{av:'Ddo_contratoservicostelas_tela_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicostelas_tela_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SortedStatus'},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_link_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_parms_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_status_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSTELAS_LINK.ONOPTIONCLICKED","{handler:'E15G82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV35ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicostelas_link_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'ActiveEventKey'},{av:'Ddo_contratoservicostelas_link_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'FilteredText_get'},{av:'Ddo_contratoservicostelas_link_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicostelas_link_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SortedStatus'},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_tela_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_parms_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_status_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSTELAS_PARMS.ONOPTIONCLICKED","{handler:'E16G82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV35ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicostelas_parms_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'ActiveEventKey'},{av:'Ddo_contratoservicostelas_parms_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'FilteredText_get'},{av:'Ddo_contratoservicostelas_parms_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicostelas_parms_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SortedStatus'},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_tela_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_link_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_status_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSTELAS_STATUS.ONOPTIONCLICKED","{handler:'E17G82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV35ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicostelas_status_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'ActiveEventKey'},{av:'Ddo_contratoservicostelas_status_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicostelas_status_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SortedStatus'},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_tela_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_link_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_parms_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E30G82',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E31G82',iparms:[{av:'A926ContratoServicosTelas_ContratoCod',fld:'CONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'A925ContratoServicosTelas_ServicoCod',fld:'CONTRATOSERVICOSTELAS_SERVICOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7InOutContratoServicosTelas_ContratoCod',fld:'vINOUTCONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV31InOutContratoServicosTelas_Sequencial',fld:'vINOUTCONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'AV8InOutContratoServicosTelas_ServicoCod',fld:'vINOUTCONTRATOSERVICOSTELAS_SERVICOCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E18G82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV35ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E23G82',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E19G82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV35ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'dynavContratoservicostelas_servicocod2'},{av:'cmbavDynamicfiltersoperator2'},{av:'dynavContratoservicostelas_servicocod3'},{av:'cmbavDynamicfiltersoperator3'},{av:'dynavContratoservicostelas_servicocod1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E24G82',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'dynavContratoservicostelas_servicocod1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E25G82',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E20G82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV35ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'dynavContratoservicostelas_servicocod2'},{av:'cmbavDynamicfiltersoperator2'},{av:'dynavContratoservicostelas_servicocod3'},{av:'cmbavDynamicfiltersoperator3'},{av:'dynavContratoservicostelas_servicocod1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E26G82',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'dynavContratoservicostelas_servicocod2'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E21G82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV35ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'dynavContratoservicostelas_servicocod2'},{av:'cmbavDynamicfiltersoperator2'},{av:'dynavContratoservicostelas_servicocod3'},{av:'cmbavDynamicfiltersoperator3'},{av:'dynavContratoservicostelas_servicocod1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E27G82',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'dynavContratoservicostelas_servicocod3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E22G82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV35ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV33TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_set'},{av:'AV34TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Selectedvalue_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_set'},{av:'AV37TFContratoServicosTelas_ServicoSigla',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA',pic:'@!',nv:''},{av:'Ddo_contratoservicostelas_servicosigla_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA',prop:'FilteredText_set'},{av:'AV38TFContratoServicosTelas_ServicoSigla_Sel',fld:'vTFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratoservicostelas_servicosigla_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA',prop:'SelectedValue_set'},{av:'AV41TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'Ddo_contratoservicostelas_tela_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'FilteredText_set'},{av:'AV42TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'Ddo_contratoservicostelas_tela_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SelectedValue_set'},{av:'AV45TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'Ddo_contratoservicostelas_link_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'FilteredText_set'},{av:'AV46TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'Ddo_contratoservicostelas_link_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SelectedValue_set'},{av:'AV49TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'Ddo_contratoservicostelas_parms_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'FilteredText_set'},{av:'AV50TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'Ddo_contratoservicostelas_parms_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SelectedValue_set'},{av:'AV54TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'Ddo_contratoservicostelas_status_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosTelas_ServicoCod1',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'dynavContratoservicostelas_servicocod1'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosTelas_ServicoCod2',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosTelas_ServicoCod3',fld:'vCONTRATOSERVICOSTELAS_SERVICOCOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'dynavContratoservicostelas_servicocod2'},{av:'cmbavDynamicfiltersoperator2'},{av:'dynavContratoservicostelas_servicocod3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratada_pessoanom_Activeeventkey = "";
         Ddo_contratada_pessoanom_Filteredtext_get = "";
         Ddo_contratada_pessoanom_Selectedvalue_get = "";
         Ddo_contratoservicostelas_servicosigla_Activeeventkey = "";
         Ddo_contratoservicostelas_servicosigla_Filteredtext_get = "";
         Ddo_contratoservicostelas_servicosigla_Selectedvalue_get = "";
         Ddo_contratoservicostelas_tela_Activeeventkey = "";
         Ddo_contratoservicostelas_tela_Filteredtext_get = "";
         Ddo_contratoservicostelas_tela_Selectedvalue_get = "";
         Ddo_contratoservicostelas_link_Activeeventkey = "";
         Ddo_contratoservicostelas_link_Filteredtext_get = "";
         Ddo_contratoservicostelas_link_Selectedvalue_get = "";
         Ddo_contratoservicostelas_parms_Activeeventkey = "";
         Ddo_contratoservicostelas_parms_Filteredtext_get = "";
         Ddo_contratoservicostelas_parms_Selectedvalue_get = "";
         Ddo_contratoservicostelas_status_Activeeventkey = "";
         Ddo_contratoservicostelas_status_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV33TFContratada_PessoaNom = "";
         AV34TFContratada_PessoaNom_Sel = "";
         AV37TFContratoServicosTelas_ServicoSigla = "";
         AV38TFContratoServicosTelas_ServicoSigla_Sel = "";
         AV41TFContratoServicosTelas_Tela = "";
         AV42TFContratoServicosTelas_Tela_Sel = "";
         AV45TFContratoServicosTelas_Link = "";
         AV46TFContratoServicosTelas_Link_Sel = "";
         AV49TFContratoServicosTelas_Parms = "";
         AV50TFContratoServicosTelas_Parms_Sel = "";
         AV35ddo_Contratada_PessoaNomTitleControlIdToReplace = "";
         AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace = "";
         AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace = "";
         AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace = "";
         AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace = "";
         AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace = "";
         AV54TFContratoServicosTelas_Status_Sels = new GxSimpleCollection();
         AV63Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV56DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV32Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36ContratoServicosTelas_ServicoSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40ContratoServicosTelas_TelaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44ContratoServicosTelas_LinkTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48ContratoServicosTelas_ParmsTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ContratoServicosTelas_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         Ddo_contratada_pessoanom_Sortedstatus = "";
         Ddo_contratoservicostelas_servicosigla_Filteredtext_set = "";
         Ddo_contratoservicostelas_servicosigla_Selectedvalue_set = "";
         Ddo_contratoservicostelas_servicosigla_Sortedstatus = "";
         Ddo_contratoservicostelas_tela_Filteredtext_set = "";
         Ddo_contratoservicostelas_tela_Selectedvalue_set = "";
         Ddo_contratoservicostelas_tela_Sortedstatus = "";
         Ddo_contratoservicostelas_link_Filteredtext_set = "";
         Ddo_contratoservicostelas_link_Selectedvalue_set = "";
         Ddo_contratoservicostelas_link_Sortedstatus = "";
         Ddo_contratoservicostelas_parms_Filteredtext_set = "";
         Ddo_contratoservicostelas_parms_Selectedvalue_set = "";
         Ddo_contratoservicostelas_parms_Sortedstatus = "";
         Ddo_contratoservicostelas_status_Selectedvalue_set = "";
         Ddo_contratoservicostelas_status_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV62Select_GXI = "";
         A41Contratada_PessoaNom = "";
         A937ContratoServicosTelas_ServicoSigla = "";
         A931ContratoServicosTelas_Tela = "";
         A928ContratoServicosTelas_Link = "";
         A929ContratoServicosTelas_Parms = "";
         A932ContratoServicosTelas_Status = "";
         scmdbuf = "";
         H00G82_A155Servico_Codigo = new int[1] ;
         H00G82_A608Servico_Nome = new String[] {""} ;
         H00G83_A155Servico_Codigo = new int[1] ;
         H00G83_A608Servico_Nome = new String[] {""} ;
         H00G84_A155Servico_Codigo = new int[1] ;
         H00G84_A608Servico_Nome = new String[] {""} ;
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00G85_A155Servico_Codigo = new int[1] ;
         H00G85_A608Servico_Nome = new String[] {""} ;
         H00G86_A155Servico_Codigo = new int[1] ;
         H00G86_A608Servico_Nome = new String[] {""} ;
         H00G87_A155Servico_Codigo = new int[1] ;
         H00G87_A608Servico_Nome = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         lV33TFContratada_PessoaNom = "";
         lV37TFContratoServicosTelas_ServicoSigla = "";
         lV41TFContratoServicosTelas_Tela = "";
         lV45TFContratoServicosTelas_Link = "";
         lV49TFContratoServicosTelas_Parms = "";
         H00G88_A74Contrato_Codigo = new int[1] ;
         H00G88_n74Contrato_Codigo = new bool[] {false} ;
         H00G88_A39Contratada_Codigo = new int[1] ;
         H00G88_A40Contratada_PessoaCod = new int[1] ;
         H00G88_A938ContratoServicosTelas_Sequencial = new short[1] ;
         H00G88_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         H00G88_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         H00G88_A932ContratoServicosTelas_Status = new String[] {""} ;
         H00G88_n932ContratoServicosTelas_Status = new bool[] {false} ;
         H00G88_A929ContratoServicosTelas_Parms = new String[] {""} ;
         H00G88_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         H00G88_A928ContratoServicosTelas_Link = new String[] {""} ;
         H00G88_A931ContratoServicosTelas_Tela = new String[] {""} ;
         H00G88_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         H00G88_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         H00G88_A41Contratada_PessoaNom = new String[] {""} ;
         H00G88_n41Contratada_PessoaNom = new bool[] {false} ;
         H00G88_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         H00G89_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV53TFContratoServicosTelas_Status_SelsJson = "";
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratoservicostelas__default(),
            new Object[][] {
                new Object[] {
               H00G82_A155Servico_Codigo, H00G82_A608Servico_Nome
               }
               , new Object[] {
               H00G83_A155Servico_Codigo, H00G83_A608Servico_Nome
               }
               , new Object[] {
               H00G84_A155Servico_Codigo, H00G84_A608Servico_Nome
               }
               , new Object[] {
               H00G85_A155Servico_Codigo, H00G85_A608Servico_Nome
               }
               , new Object[] {
               H00G86_A155Servico_Codigo, H00G86_A608Servico_Nome
               }
               , new Object[] {
               H00G87_A155Servico_Codigo, H00G87_A608Servico_Nome
               }
               , new Object[] {
               H00G88_A74Contrato_Codigo, H00G88_n74Contrato_Codigo, H00G88_A39Contratada_Codigo, H00G88_A40Contratada_PessoaCod, H00G88_A938ContratoServicosTelas_Sequencial, H00G88_A925ContratoServicosTelas_ServicoCod, H00G88_n925ContratoServicosTelas_ServicoCod, H00G88_A932ContratoServicosTelas_Status, H00G88_n932ContratoServicosTelas_Status, H00G88_A929ContratoServicosTelas_Parms,
               H00G88_n929ContratoServicosTelas_Parms, H00G88_A928ContratoServicosTelas_Link, H00G88_A931ContratoServicosTelas_Tela, H00G88_A937ContratoServicosTelas_ServicoSigla, H00G88_n937ContratoServicosTelas_ServicoSigla, H00G88_A41Contratada_PessoaNom, H00G88_n41Contratada_PessoaNom, H00G88_A926ContratoServicosTelas_ContratoCod
               }
               , new Object[] {
               H00G89_AGRID_nRecordCount
               }
            }
         );
         AV63Pgmname = "PromptContratoServicosTelas";
         /* GeneXus formulas. */
         AV63Pgmname = "PromptContratoServicosTelas";
         context.Gx_err = 0;
      }

      private short AV31InOutContratoServicosTelas_Sequencial ;
      private short wcpOAV31InOutContratoServicosTelas_Sequencial ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_80 ;
      private short nGXsfl_80_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short A938ContratoServicosTelas_Sequencial ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_80_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratada_PessoaNom_Titleformat ;
      private short edtContratoServicosTelas_ServicoSigla_Titleformat ;
      private short edtContratoServicosTelas_Tela_Titleformat ;
      private short edtContratoServicosTelas_Link_Titleformat ;
      private short edtContratoServicosTelas_Parms_Titleformat ;
      private short cmbContratoServicosTelas_Status_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratoServicosTelas_ContratoCod ;
      private int AV8InOutContratoServicosTelas_ServicoCod ;
      private int wcpOAV7InOutContratoServicosTelas_ContratoCod ;
      private int wcpOAV8InOutContratoServicosTelas_ServicoCod ;
      private int subGrid_Rows ;
      private int AV17ContratoServicosTelas_ServicoCod1 ;
      private int AV21ContratoServicosTelas_ServicoCod2 ;
      private int AV25ContratoServicosTelas_ServicoCod3 ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratada_pessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicostelas_servicosigla_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicostelas_tela_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicostelas_link_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicostelas_parms_Datalistupdateminimumcharacters ;
      private int edtContratoServicosTelas_Sequencial_Visible ;
      private int A925ContratoServicosTelas_ServicoCod ;
      private int edtContratoServicosTelas_ServicoCod_Visible ;
      private int edtavTfcontratada_pessoanom_Visible ;
      private int edtavTfcontratada_pessoanom_sel_Visible ;
      private int edtavTfcontratoservicostelas_servicosigla_Visible ;
      private int edtavTfcontratoservicostelas_servicosigla_sel_Visible ;
      private int edtavTfcontratoservicostelas_tela_Visible ;
      private int edtavTfcontratoservicostelas_tela_sel_Visible ;
      private int edtavTfcontratoservicostelas_link_Visible ;
      private int edtavTfcontratoservicostelas_link_sel_Visible ;
      private int edtavTfcontratoservicostelas_parms_Visible ;
      private int edtavTfcontratoservicostelas_parms_sel_Visible ;
      private int edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicostelas_servicosiglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Visible ;
      private int A926ContratoServicosTelas_ContratoCod ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV54TFContratoServicosTelas_Status_Sels_Count ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV57PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV58GridCurrentPage ;
      private long AV59GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratada_pessoanom_Activeeventkey ;
      private String Ddo_contratada_pessoanom_Filteredtext_get ;
      private String Ddo_contratada_pessoanom_Selectedvalue_get ;
      private String Ddo_contratoservicostelas_servicosigla_Activeeventkey ;
      private String Ddo_contratoservicostelas_servicosigla_Filteredtext_get ;
      private String Ddo_contratoservicostelas_servicosigla_Selectedvalue_get ;
      private String Ddo_contratoservicostelas_tela_Activeeventkey ;
      private String Ddo_contratoservicostelas_tela_Filteredtext_get ;
      private String Ddo_contratoservicostelas_tela_Selectedvalue_get ;
      private String Ddo_contratoservicostelas_link_Activeeventkey ;
      private String Ddo_contratoservicostelas_link_Filteredtext_get ;
      private String Ddo_contratoservicostelas_link_Selectedvalue_get ;
      private String Ddo_contratoservicostelas_parms_Activeeventkey ;
      private String Ddo_contratoservicostelas_parms_Filteredtext_get ;
      private String Ddo_contratoservicostelas_parms_Selectedvalue_get ;
      private String Ddo_contratoservicostelas_status_Activeeventkey ;
      private String Ddo_contratoservicostelas_status_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_80_idx="0001" ;
      private String AV33TFContratada_PessoaNom ;
      private String AV34TFContratada_PessoaNom_Sel ;
      private String AV37TFContratoServicosTelas_ServicoSigla ;
      private String AV38TFContratoServicosTelas_ServicoSigla_Sel ;
      private String AV41TFContratoServicosTelas_Tela ;
      private String AV42TFContratoServicosTelas_Tela_Sel ;
      private String AV63Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratada_pessoanom_Caption ;
      private String Ddo_contratada_pessoanom_Tooltip ;
      private String Ddo_contratada_pessoanom_Cls ;
      private String Ddo_contratada_pessoanom_Filteredtext_set ;
      private String Ddo_contratada_pessoanom_Selectedvalue_set ;
      private String Ddo_contratada_pessoanom_Dropdownoptionstype ;
      private String Ddo_contratada_pessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoanom_Sortedstatus ;
      private String Ddo_contratada_pessoanom_Filtertype ;
      private String Ddo_contratada_pessoanom_Datalisttype ;
      private String Ddo_contratada_pessoanom_Datalistproc ;
      private String Ddo_contratada_pessoanom_Sortasc ;
      private String Ddo_contratada_pessoanom_Sortdsc ;
      private String Ddo_contratada_pessoanom_Loadingdata ;
      private String Ddo_contratada_pessoanom_Cleanfilter ;
      private String Ddo_contratada_pessoanom_Noresultsfound ;
      private String Ddo_contratada_pessoanom_Searchbuttontext ;
      private String Ddo_contratoservicostelas_servicosigla_Caption ;
      private String Ddo_contratoservicostelas_servicosigla_Tooltip ;
      private String Ddo_contratoservicostelas_servicosigla_Cls ;
      private String Ddo_contratoservicostelas_servicosigla_Filteredtext_set ;
      private String Ddo_contratoservicostelas_servicosigla_Selectedvalue_set ;
      private String Ddo_contratoservicostelas_servicosigla_Dropdownoptionstype ;
      private String Ddo_contratoservicostelas_servicosigla_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicostelas_servicosigla_Sortedstatus ;
      private String Ddo_contratoservicostelas_servicosigla_Filtertype ;
      private String Ddo_contratoservicostelas_servicosigla_Datalisttype ;
      private String Ddo_contratoservicostelas_servicosigla_Datalistproc ;
      private String Ddo_contratoservicostelas_servicosigla_Sortasc ;
      private String Ddo_contratoservicostelas_servicosigla_Sortdsc ;
      private String Ddo_contratoservicostelas_servicosigla_Loadingdata ;
      private String Ddo_contratoservicostelas_servicosigla_Cleanfilter ;
      private String Ddo_contratoservicostelas_servicosigla_Noresultsfound ;
      private String Ddo_contratoservicostelas_servicosigla_Searchbuttontext ;
      private String Ddo_contratoservicostelas_tela_Caption ;
      private String Ddo_contratoservicostelas_tela_Tooltip ;
      private String Ddo_contratoservicostelas_tela_Cls ;
      private String Ddo_contratoservicostelas_tela_Filteredtext_set ;
      private String Ddo_contratoservicostelas_tela_Selectedvalue_set ;
      private String Ddo_contratoservicostelas_tela_Dropdownoptionstype ;
      private String Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicostelas_tela_Sortedstatus ;
      private String Ddo_contratoservicostelas_tela_Filtertype ;
      private String Ddo_contratoservicostelas_tela_Datalisttype ;
      private String Ddo_contratoservicostelas_tela_Datalistproc ;
      private String Ddo_contratoservicostelas_tela_Sortasc ;
      private String Ddo_contratoservicostelas_tela_Sortdsc ;
      private String Ddo_contratoservicostelas_tela_Loadingdata ;
      private String Ddo_contratoservicostelas_tela_Cleanfilter ;
      private String Ddo_contratoservicostelas_tela_Noresultsfound ;
      private String Ddo_contratoservicostelas_tela_Searchbuttontext ;
      private String Ddo_contratoservicostelas_link_Caption ;
      private String Ddo_contratoservicostelas_link_Tooltip ;
      private String Ddo_contratoservicostelas_link_Cls ;
      private String Ddo_contratoservicostelas_link_Filteredtext_set ;
      private String Ddo_contratoservicostelas_link_Selectedvalue_set ;
      private String Ddo_contratoservicostelas_link_Dropdownoptionstype ;
      private String Ddo_contratoservicostelas_link_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicostelas_link_Sortedstatus ;
      private String Ddo_contratoservicostelas_link_Filtertype ;
      private String Ddo_contratoservicostelas_link_Datalisttype ;
      private String Ddo_contratoservicostelas_link_Datalistproc ;
      private String Ddo_contratoservicostelas_link_Sortasc ;
      private String Ddo_contratoservicostelas_link_Sortdsc ;
      private String Ddo_contratoservicostelas_link_Loadingdata ;
      private String Ddo_contratoservicostelas_link_Cleanfilter ;
      private String Ddo_contratoservicostelas_link_Noresultsfound ;
      private String Ddo_contratoservicostelas_link_Searchbuttontext ;
      private String Ddo_contratoservicostelas_parms_Caption ;
      private String Ddo_contratoservicostelas_parms_Tooltip ;
      private String Ddo_contratoservicostelas_parms_Cls ;
      private String Ddo_contratoservicostelas_parms_Filteredtext_set ;
      private String Ddo_contratoservicostelas_parms_Selectedvalue_set ;
      private String Ddo_contratoservicostelas_parms_Dropdownoptionstype ;
      private String Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicostelas_parms_Sortedstatus ;
      private String Ddo_contratoservicostelas_parms_Filtertype ;
      private String Ddo_contratoservicostelas_parms_Datalisttype ;
      private String Ddo_contratoservicostelas_parms_Datalistproc ;
      private String Ddo_contratoservicostelas_parms_Sortasc ;
      private String Ddo_contratoservicostelas_parms_Sortdsc ;
      private String Ddo_contratoservicostelas_parms_Loadingdata ;
      private String Ddo_contratoservicostelas_parms_Cleanfilter ;
      private String Ddo_contratoservicostelas_parms_Noresultsfound ;
      private String Ddo_contratoservicostelas_parms_Searchbuttontext ;
      private String Ddo_contratoservicostelas_status_Caption ;
      private String Ddo_contratoservicostelas_status_Tooltip ;
      private String Ddo_contratoservicostelas_status_Cls ;
      private String Ddo_contratoservicostelas_status_Selectedvalue_set ;
      private String Ddo_contratoservicostelas_status_Dropdownoptionstype ;
      private String Ddo_contratoservicostelas_status_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicostelas_status_Sortedstatus ;
      private String Ddo_contratoservicostelas_status_Datalisttype ;
      private String Ddo_contratoservicostelas_status_Datalistfixedvalues ;
      private String Ddo_contratoservicostelas_status_Sortasc ;
      private String Ddo_contratoservicostelas_status_Sortdsc ;
      private String Ddo_contratoservicostelas_status_Cleanfilter ;
      private String Ddo_contratoservicostelas_status_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String edtContratoServicosTelas_Sequencial_Internalname ;
      private String edtContratoServicosTelas_Sequencial_Jsonclick ;
      private String edtContratoServicosTelas_ServicoCod_Internalname ;
      private String edtContratoServicosTelas_ServicoCod_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontratada_pessoanom_Internalname ;
      private String edtavTfcontratada_pessoanom_Jsonclick ;
      private String edtavTfcontratada_pessoanom_sel_Internalname ;
      private String edtavTfcontratada_pessoanom_sel_Jsonclick ;
      private String edtavTfcontratoservicostelas_servicosigla_Internalname ;
      private String edtavTfcontratoservicostelas_servicosigla_Jsonclick ;
      private String edtavTfcontratoservicostelas_servicosigla_sel_Internalname ;
      private String edtavTfcontratoservicostelas_servicosigla_sel_Jsonclick ;
      private String edtavTfcontratoservicostelas_tela_Internalname ;
      private String edtavTfcontratoservicostelas_tela_Jsonclick ;
      private String edtavTfcontratoservicostelas_tela_sel_Internalname ;
      private String edtavTfcontratoservicostelas_tela_sel_Jsonclick ;
      private String edtavTfcontratoservicostelas_link_Internalname ;
      private String edtavTfcontratoservicostelas_link_Jsonclick ;
      private String edtavTfcontratoservicostelas_link_sel_Internalname ;
      private String edtavTfcontratoservicostelas_link_sel_Jsonclick ;
      private String edtavTfcontratoservicostelas_parms_Internalname ;
      private String edtavTfcontratoservicostelas_parms_sel_Internalname ;
      private String edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicostelas_servicosiglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContratoServicosTelas_ContratoCod_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Internalname ;
      private String A937ContratoServicosTelas_ServicoSigla ;
      private String edtContratoServicosTelas_ServicoSigla_Internalname ;
      private String A931ContratoServicosTelas_Tela ;
      private String edtContratoServicosTelas_Tela_Internalname ;
      private String edtContratoServicosTelas_Link_Internalname ;
      private String edtContratoServicosTelas_Parms_Internalname ;
      private String cmbContratoServicosTelas_Status_Internalname ;
      private String A932ContratoServicosTelas_Status ;
      private String scmdbuf ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String lV33TFContratada_PessoaNom ;
      private String lV37TFContratoServicosTelas_ServicoSigla ;
      private String lV41TFContratoServicosTelas_Tela ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String dynavContratoservicostelas_servicocod1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String dynavContratoservicostelas_servicocod2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String dynavContratoservicostelas_servicocod3_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratada_pessoanom_Internalname ;
      private String Ddo_contratoservicostelas_servicosigla_Internalname ;
      private String Ddo_contratoservicostelas_tela_Internalname ;
      private String Ddo_contratoservicostelas_link_Internalname ;
      private String Ddo_contratoservicostelas_parms_Internalname ;
      private String Ddo_contratoservicostelas_status_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratada_PessoaNom_Title ;
      private String edtContratoServicosTelas_ServicoSigla_Title ;
      private String edtContratoServicosTelas_Tela_Title ;
      private String edtContratoServicosTelas_Link_Title ;
      private String edtContratoServicosTelas_Parms_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String dynavContratoservicostelas_servicocod3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String dynavContratoservicostelas_servicocod2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String dynavContratoservicostelas_servicocod1_Jsonclick ;
      private String sGXsfl_80_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContratoServicosTelas_ContratoCod_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String edtContratoServicosTelas_ServicoSigla_Jsonclick ;
      private String edtContratoServicosTelas_Tela_Jsonclick ;
      private String edtContratoServicosTelas_Link_Jsonclick ;
      private String edtContratoServicosTelas_Parms_Jsonclick ;
      private String cmbContratoServicosTelas_Status_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratada_pessoanom_Includesortasc ;
      private bool Ddo_contratada_pessoanom_Includesortdsc ;
      private bool Ddo_contratada_pessoanom_Includefilter ;
      private bool Ddo_contratada_pessoanom_Filterisrange ;
      private bool Ddo_contratada_pessoanom_Includedatalist ;
      private bool Ddo_contratoservicostelas_servicosigla_Includesortasc ;
      private bool Ddo_contratoservicostelas_servicosigla_Includesortdsc ;
      private bool Ddo_contratoservicostelas_servicosigla_Includefilter ;
      private bool Ddo_contratoservicostelas_servicosigla_Filterisrange ;
      private bool Ddo_contratoservicostelas_servicosigla_Includedatalist ;
      private bool Ddo_contratoservicostelas_tela_Includesortasc ;
      private bool Ddo_contratoservicostelas_tela_Includesortdsc ;
      private bool Ddo_contratoservicostelas_tela_Includefilter ;
      private bool Ddo_contratoservicostelas_tela_Filterisrange ;
      private bool Ddo_contratoservicostelas_tela_Includedatalist ;
      private bool Ddo_contratoservicostelas_link_Includesortasc ;
      private bool Ddo_contratoservicostelas_link_Includesortdsc ;
      private bool Ddo_contratoservicostelas_link_Includefilter ;
      private bool Ddo_contratoservicostelas_link_Filterisrange ;
      private bool Ddo_contratoservicostelas_link_Includedatalist ;
      private bool Ddo_contratoservicostelas_parms_Includesortasc ;
      private bool Ddo_contratoservicostelas_parms_Includesortdsc ;
      private bool Ddo_contratoservicostelas_parms_Includefilter ;
      private bool Ddo_contratoservicostelas_parms_Filterisrange ;
      private bool Ddo_contratoservicostelas_parms_Includedatalist ;
      private bool Ddo_contratoservicostelas_status_Includesortasc ;
      private bool Ddo_contratoservicostelas_status_Includesortdsc ;
      private bool Ddo_contratoservicostelas_status_Includefilter ;
      private bool Ddo_contratoservicostelas_status_Includedatalist ;
      private bool Ddo_contratoservicostelas_status_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n41Contratada_PessoaNom ;
      private bool n937ContratoServicosTelas_ServicoSigla ;
      private bool n929ContratoServicosTelas_Parms ;
      private bool n932ContratoServicosTelas_Status ;
      private bool n74Contrato_Codigo ;
      private bool n925ContratoServicosTelas_ServicoCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String A929ContratoServicosTelas_Parms ;
      private String AV53TFContratoServicosTelas_Status_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV45TFContratoServicosTelas_Link ;
      private String AV46TFContratoServicosTelas_Link_Sel ;
      private String AV49TFContratoServicosTelas_Parms ;
      private String AV50TFContratoServicosTelas_Parms_Sel ;
      private String AV35ddo_Contratada_PessoaNomTitleControlIdToReplace ;
      private String AV39ddo_ContratoServicosTelas_ServicoSiglaTitleControlIdToReplace ;
      private String AV43ddo_ContratoServicosTelas_TelaTitleControlIdToReplace ;
      private String AV47ddo_ContratoServicosTelas_LinkTitleControlIdToReplace ;
      private String AV51ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace ;
      private String AV55ddo_ContratoServicosTelas_StatusTitleControlIdToReplace ;
      private String AV62Select_GXI ;
      private String A928ContratoServicosTelas_Link ;
      private String lV45TFContratoServicosTelas_Link ;
      private String lV49TFContratoServicosTelas_Parms ;
      private String AV28Select ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratoServicosTelas_ContratoCod ;
      private short aP1_InOutContratoServicosTelas_Sequencial ;
      private int aP2_InOutContratoServicosTelas_ServicoCod ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox dynavContratoservicostelas_servicocod1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox dynavContratoservicostelas_servicocod2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox dynavContratoservicostelas_servicocod3 ;
      private GXCombobox cmbContratoServicosTelas_Status ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00G82_A155Servico_Codigo ;
      private String[] H00G82_A608Servico_Nome ;
      private int[] H00G83_A155Servico_Codigo ;
      private String[] H00G83_A608Servico_Nome ;
      private int[] H00G84_A155Servico_Codigo ;
      private String[] H00G84_A608Servico_Nome ;
      private int[] H00G85_A155Servico_Codigo ;
      private String[] H00G85_A608Servico_Nome ;
      private int[] H00G86_A155Servico_Codigo ;
      private String[] H00G86_A608Servico_Nome ;
      private int[] H00G87_A155Servico_Codigo ;
      private String[] H00G87_A608Servico_Nome ;
      private int[] H00G88_A74Contrato_Codigo ;
      private bool[] H00G88_n74Contrato_Codigo ;
      private int[] H00G88_A39Contratada_Codigo ;
      private int[] H00G88_A40Contratada_PessoaCod ;
      private short[] H00G88_A938ContratoServicosTelas_Sequencial ;
      private int[] H00G88_A925ContratoServicosTelas_ServicoCod ;
      private bool[] H00G88_n925ContratoServicosTelas_ServicoCod ;
      private String[] H00G88_A932ContratoServicosTelas_Status ;
      private bool[] H00G88_n932ContratoServicosTelas_Status ;
      private String[] H00G88_A929ContratoServicosTelas_Parms ;
      private bool[] H00G88_n929ContratoServicosTelas_Parms ;
      private String[] H00G88_A928ContratoServicosTelas_Link ;
      private String[] H00G88_A931ContratoServicosTelas_Tela ;
      private String[] H00G88_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] H00G88_n937ContratoServicosTelas_ServicoSigla ;
      private String[] H00G88_A41Contratada_PessoaNom ;
      private bool[] H00G88_n41Contratada_PessoaNom ;
      private int[] H00G88_A926ContratoServicosTelas_ContratoCod ;
      private long[] H00G89_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV54TFContratoServicosTelas_Status_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV32Contratada_PessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36ContratoServicosTelas_ServicoSiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV40ContratoServicosTelas_TelaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44ContratoServicosTelas_LinkTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48ContratoServicosTelas_ParmsTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV52ContratoServicosTelas_StatusTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV56DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratoservicostelas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00G88( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV54TFContratoServicosTelas_Status_Sels ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             int AV17ContratoServicosTelas_ServicoCod1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             int AV21ContratoServicosTelas_ServicoCod2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             int AV25ContratoServicosTelas_ServicoCod3 ,
                                             String AV34TFContratada_PessoaNom_Sel ,
                                             String AV33TFContratada_PessoaNom ,
                                             String AV38TFContratoServicosTelas_ServicoSigla_Sel ,
                                             String AV37TFContratoServicosTelas_ServicoSigla ,
                                             String AV42TFContratoServicosTelas_Tela_Sel ,
                                             String AV41TFContratoServicosTelas_Tela ,
                                             String AV46TFContratoServicosTelas_Link_Sel ,
                                             String AV45TFContratoServicosTelas_Link ,
                                             String AV50TFContratoServicosTelas_Parms_Sel ,
                                             String AV49TFContratoServicosTelas_Parms ,
                                             int AV54TFContratoServicosTelas_Status_Sels_Count ,
                                             int A925ContratoServicosTelas_ServicoCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A937ContratoServicosTelas_ServicoSigla ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [24] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicosTelas_Sequencial], T2.[Servico_Codigo] AS ContratoServicosTelas_ServicoCod, T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Tela], T6.[Servico_Sigla] AS ContratoServicosTelas_ServicoSigla, T5.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod";
         sFromString = " FROM ((((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T2.[Servico_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (0==AV17ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV17ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV17ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! (0==AV17ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV17ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV17ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV16DynamicFiltersOperator1 == 2 ) && ( ! (0==AV17ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV17ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV17ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! (0==AV21ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV21ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV21ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! (0==AV21ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV21ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV21ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV20DynamicFiltersOperator2 == 2 ) && ( ! (0==AV21ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV21ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV21ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! (0==AV25ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV25ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV25ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! (0==AV25ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV25ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV25ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV24DynamicFiltersOperator3 == 2 ) && ( ! (0==AV25ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV25ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV25ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV33TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV33TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV34TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV34TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV38TFContratoServicosTelas_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFContratoServicosTelas_ServicoSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV37TFContratoServicosTelas_ServicoSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] like @lV37TFContratoServicosTelas_ServicoSigla)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFContratoServicosTelas_ServicoSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV38TFContratoServicosTelas_ServicoSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] = @AV38TFContratoServicosTelas_ServicoSigla_Sel)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratoServicosTelas_Tela_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFContratoServicosTelas_Tela)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV41TFContratoServicosTelas_Tela)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] like @lV41TFContratoServicosTelas_Tela)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratoServicosTelas_Tela_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV42TFContratoServicosTelas_Tela_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] = @AV42TFContratoServicosTelas_Tela_Sel)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratoServicosTelas_Link_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContratoServicosTelas_Link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV45TFContratoServicosTelas_Link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] like @lV45TFContratoServicosTelas_Link)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratoServicosTelas_Link_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV46TFContratoServicosTelas_Link_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] = @AV46TFContratoServicosTelas_Link_Sel)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContratoServicosTelas_Parms_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContratoServicosTelas_Parms)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV49TFContratoServicosTelas_Parms)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] like @lV49TFContratoServicosTelas_Parms)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContratoServicosTelas_Parms_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV50TFContratoServicosTelas_Parms_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] = @AV50TFContratoServicosTelas_Parms_Sel)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( AV54TFContratoServicosTelas_Status_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV54TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV54TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( AV13OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Servico_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T6.[Servico_Sigla]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T6.[Servico_Sigla] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_Tela]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_Tela] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_Link]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_Link] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_Parms]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_Parms] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_Status]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_Status] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_ContratoCod], T1.[ContratoServicosTelas_Sequencial]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00G89( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV54TFContratoServicosTelas_Status_Sels ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             int AV17ContratoServicosTelas_ServicoCod1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             int AV21ContratoServicosTelas_ServicoCod2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             int AV25ContratoServicosTelas_ServicoCod3 ,
                                             String AV34TFContratada_PessoaNom_Sel ,
                                             String AV33TFContratada_PessoaNom ,
                                             String AV38TFContratoServicosTelas_ServicoSigla_Sel ,
                                             String AV37TFContratoServicosTelas_ServicoSigla ,
                                             String AV42TFContratoServicosTelas_Tela_Sel ,
                                             String AV41TFContratoServicosTelas_Tela ,
                                             String AV46TFContratoServicosTelas_Link_Sel ,
                                             String AV45TFContratoServicosTelas_Link ,
                                             String AV50TFContratoServicosTelas_Parms_Sel ,
                                             String AV49TFContratoServicosTelas_Parms ,
                                             int AV54TFContratoServicosTelas_Status_Sels_Count ,
                                             int A925ContratoServicosTelas_ServicoCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A937ContratoServicosTelas_ServicoSigla ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [19] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T4.[Contratada_Codigo]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Contratada_PessoaCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (0==AV17ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV17ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV17ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! (0==AV17ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV17ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV17ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV16DynamicFiltersOperator1 == 2 ) && ( ! (0==AV17ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV17ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV17ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! (0==AV21ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV21ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV21ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! (0==AV21ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV21ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV21ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV20DynamicFiltersOperator2 == 2 ) && ( ! (0==AV21ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV21ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV21ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! (0==AV25ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV25ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV25ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! (0==AV25ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV25ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV25ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV24DynamicFiltersOperator3 == 2 ) && ( ! (0==AV25ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV25ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV25ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV33TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Pessoa_Nome] like @lV33TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Pessoa_Nome] = @AV34TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Pessoa_Nome] = @AV34TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV38TFContratoServicosTelas_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFContratoServicosTelas_ServicoSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV37TFContratoServicosTelas_ServicoSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV37TFContratoServicosTelas_ServicoSigla)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFContratoServicosTelas_ServicoSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV38TFContratoServicosTelas_ServicoSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] = @AV38TFContratoServicosTelas_ServicoSigla_Sel)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratoServicosTelas_Tela_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFContratoServicosTelas_Tela)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV41TFContratoServicosTelas_Tela)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] like @lV41TFContratoServicosTelas_Tela)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratoServicosTelas_Tela_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV42TFContratoServicosTelas_Tela_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] = @AV42TFContratoServicosTelas_Tela_Sel)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratoServicosTelas_Link_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContratoServicosTelas_Link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV45TFContratoServicosTelas_Link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] like @lV45TFContratoServicosTelas_Link)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratoServicosTelas_Link_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV46TFContratoServicosTelas_Link_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] = @AV46TFContratoServicosTelas_Link_Sel)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContratoServicosTelas_Parms_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContratoServicosTelas_Parms)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV49TFContratoServicosTelas_Parms)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] like @lV49TFContratoServicosTelas_Parms)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContratoServicosTelas_Parms_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV50TFContratoServicosTelas_Parms_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] = @AV50TFContratoServicosTelas_Parms_Sel)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV54TFContratoServicosTelas_Status_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV54TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV54TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 6 :
                     return conditional_H00G88(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (bool)dynConstraints[31] );
               case 7 :
                     return conditional_H00G89(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (bool)dynConstraints[31] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00G82 ;
          prmH00G82 = new Object[] {
          } ;
          Object[] prmH00G83 ;
          prmH00G83 = new Object[] {
          } ;
          Object[] prmH00G84 ;
          prmH00G84 = new Object[] {
          } ;
          Object[] prmH00G85 ;
          prmH00G85 = new Object[] {
          } ;
          Object[] prmH00G86 ;
          prmH00G86 = new Object[] {
          } ;
          Object[] prmH00G87 ;
          prmH00G87 = new Object[] {
          } ;
          Object[] prmH00G88 ;
          prmH00G88 = new Object[] {
          new Object[] {"@AV17ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV33TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV34TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV37TFContratoServicosTelas_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV38TFContratoServicosTelas_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV41TFContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV42TFContratoServicosTelas_Tela_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV45TFContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV46TFContratoServicosTelas_Link_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV49TFContratoServicosTelas_Parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV50TFContratoServicosTelas_Parms_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00G89 ;
          prmH00G89 = new Object[] {
          new Object[] {"@AV17ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV33TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV34TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV37TFContratoServicosTelas_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV38TFContratoServicosTelas_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV41TFContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV42TFContratoServicosTelas_Tela_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV45TFContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV46TFContratoServicosTelas_Link_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV49TFContratoServicosTelas_Parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV50TFContratoServicosTelas_Parms_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00G82", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G82,0,0,true,false )
             ,new CursorDef("H00G83", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G83,0,0,true,false )
             ,new CursorDef("H00G84", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G84,0,0,true,false )
             ,new CursorDef("H00G85", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G85,0,0,true,false )
             ,new CursorDef("H00G86", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G86,0,0,true,false )
             ,new CursorDef("H00G87", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G87,0,0,true,false )
             ,new CursorDef("H00G88", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G88,11,0,true,false )
             ,new CursorDef("H00G89", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G89,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((int[]) buf[17])[0] = rslt.getInt(12) ;
                return;
             case 7 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                return;
             case 7 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                return;
       }
    }

 }

}
