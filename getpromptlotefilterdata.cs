/*
               File: GetPromptLoteFilterData
        Description: Get Prompt Lote Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:43.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptlotefilterdata : GXProcedure
   {
      public getpromptlotefilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptlotefilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV34DDOName = aP0_DDOName;
         this.AV32SearchTxt = aP1_SearchTxt;
         this.AV33SearchTxtTo = aP2_SearchTxtTo;
         this.AV38OptionsJson = "" ;
         this.AV41OptionsDescJson = "" ;
         this.AV43OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV38OptionsJson;
         aP4_OptionsDescJson=this.AV41OptionsDescJson;
         aP5_OptionIndexesJson=this.AV43OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV34DDOName = aP0_DDOName;
         this.AV32SearchTxt = aP1_SearchTxt;
         this.AV33SearchTxtTo = aP2_SearchTxtTo;
         this.AV38OptionsJson = "" ;
         this.AV41OptionsDescJson = "" ;
         this.AV43OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV38OptionsJson;
         aP4_OptionsDescJson=this.AV41OptionsDescJson;
         aP5_OptionIndexesJson=this.AV43OptionIndexesJson;
         return AV43OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptlotefilterdata objgetpromptlotefilterdata;
         objgetpromptlotefilterdata = new getpromptlotefilterdata();
         objgetpromptlotefilterdata.AV34DDOName = aP0_DDOName;
         objgetpromptlotefilterdata.AV32SearchTxt = aP1_SearchTxt;
         objgetpromptlotefilterdata.AV33SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptlotefilterdata.AV38OptionsJson = "" ;
         objgetpromptlotefilterdata.AV41OptionsDescJson = "" ;
         objgetpromptlotefilterdata.AV43OptionIndexesJson = "" ;
         objgetpromptlotefilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptlotefilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptlotefilterdata);
         aP3_OptionsJson=this.AV38OptionsJson;
         aP4_OptionsDescJson=this.AV41OptionsDescJson;
         aP5_OptionIndexesJson=this.AV43OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptlotefilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV37Options = (IGxCollection)(new GxSimpleCollection());
         AV40OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV42OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV34DDOName), "DDO_LOTE_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADLOTE_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV34DDOName), "DDO_LOTE_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADLOTE_NOMEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV34DDOName), "DDO_LOTE_USERNOM") == 0 )
         {
            /* Execute user subroutine: 'LOADLOTE_USERNOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV38OptionsJson = AV37Options.ToJSonString(false);
         AV41OptionsDescJson = AV40OptionsDesc.ToJSonString(false);
         AV43OptionIndexesJson = AV42OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV45Session.Get("PromptLoteGridState"), "") == 0 )
         {
            AV47GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptLoteGridState"), "");
         }
         else
         {
            AV47GridState.FromXml(AV45Session.Get("PromptLoteGridState"), "");
         }
         AV68GXV1 = 1;
         while ( AV68GXV1 <= AV47GridState.gxTpr_Filtervalues.Count )
         {
            AV48GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV47GridState.gxTpr_Filtervalues.Item(AV68GXV1));
            if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "LOTE_AREATRABALHOCOD") == 0 )
            {
               AV50Lote_AreaTrabalhoCod = (int)(NumberUtil.Val( AV48GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "LOTE_CONTRATADACOD") == 0 )
            {
               AV51Lote_ContratadaCod = (int)(NumberUtil.Val( AV48GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_NUMERO") == 0 )
            {
               AV10TFLote_Numero = AV48GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_NUMERO_SEL") == 0 )
            {
               AV11TFLote_Numero_Sel = AV48GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_NOME") == 0 )
            {
               AV12TFLote_Nome = AV48GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_NOME_SEL") == 0 )
            {
               AV13TFLote_Nome_Sel = AV48GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_DATA") == 0 )
            {
               AV14TFLote_Data = context.localUtil.CToT( AV48GridStateFilterValue.gxTpr_Value, 2);
               AV15TFLote_Data_To = context.localUtil.CToT( AV48GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_USERCOD") == 0 )
            {
               AV16TFLote_UserCod = (int)(NumberUtil.Val( AV48GridStateFilterValue.gxTpr_Value, "."));
               AV17TFLote_UserCod_To = (int)(NumberUtil.Val( AV48GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_PESSOACOD") == 0 )
            {
               AV18TFLote_PessoaCod = (int)(NumberUtil.Val( AV48GridStateFilterValue.gxTpr_Value, "."));
               AV19TFLote_PessoaCod_To = (int)(NumberUtil.Val( AV48GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_USERNOM") == 0 )
            {
               AV20TFLote_UserNom = AV48GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_USERNOM_SEL") == 0 )
            {
               AV21TFLote_UserNom_Sel = AV48GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_VALORPF") == 0 )
            {
               AV22TFLote_ValorPF = NumberUtil.Val( AV48GridStateFilterValue.gxTpr_Value, ".");
               AV23TFLote_ValorPF_To = NumberUtil.Val( AV48GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_DATAINI") == 0 )
            {
               AV24TFLote_DataIni = context.localUtil.CToD( AV48GridStateFilterValue.gxTpr_Value, 2);
               AV25TFLote_DataIni_To = context.localUtil.CToD( AV48GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_DATAFIM") == 0 )
            {
               AV26TFLote_DataFim = context.localUtil.CToD( AV48GridStateFilterValue.gxTpr_Value, 2);
               AV27TFLote_DataFim_To = context.localUtil.CToD( AV48GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_QTDEDMN") == 0 )
            {
               AV28TFLote_QtdeDmn = (short)(NumberUtil.Val( AV48GridStateFilterValue.gxTpr_Value, "."));
               AV29TFLote_QtdeDmn_To = (short)(NumberUtil.Val( AV48GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV48GridStateFilterValue.gxTpr_Name, "TFLOTE_VALOR") == 0 )
            {
               AV30TFLote_Valor = NumberUtil.Val( AV48GridStateFilterValue.gxTpr_Value, ".");
               AV31TFLote_Valor_To = NumberUtil.Val( AV48GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV68GXV1 = (int)(AV68GXV1+1);
         }
         if ( AV47GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV49GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV47GridState.gxTpr_Dynamicfilters.Item(1));
            AV52DynamicFiltersSelector1 = AV49GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_NOME") == 0 )
            {
               AV53DynamicFiltersOperator1 = AV49GridStateDynamicFilter.gxTpr_Operator;
               AV54Lote_Nome1 = AV49GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_USERNOM") == 0 )
            {
               AV53DynamicFiltersOperator1 = AV49GridStateDynamicFilter.gxTpr_Operator;
               AV55Lote_UserNom1 = AV49GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV47GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV56DynamicFiltersEnabled2 = true;
               AV49GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV47GridState.gxTpr_Dynamicfilters.Item(2));
               AV57DynamicFiltersSelector2 = AV49GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_NOME") == 0 )
               {
                  AV58DynamicFiltersOperator2 = AV49GridStateDynamicFilter.gxTpr_Operator;
                  AV59Lote_Nome2 = AV49GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_USERNOM") == 0 )
               {
                  AV58DynamicFiltersOperator2 = AV49GridStateDynamicFilter.gxTpr_Operator;
                  AV60Lote_UserNom2 = AV49GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV47GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV61DynamicFiltersEnabled3 = true;
                  AV49GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV47GridState.gxTpr_Dynamicfilters.Item(3));
                  AV62DynamicFiltersSelector3 = AV49GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_NOME") == 0 )
                  {
                     AV63DynamicFiltersOperator3 = AV49GridStateDynamicFilter.gxTpr_Operator;
                     AV64Lote_Nome3 = AV49GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_USERNOM") == 0 )
                  {
                     AV63DynamicFiltersOperator3 = AV49GridStateDynamicFilter.gxTpr_Operator;
                     AV65Lote_UserNom3 = AV49GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADLOTE_NUMEROOPTIONS' Routine */
         AV10TFLote_Numero = AV32SearchTxt;
         AV11TFLote_Numero_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV52DynamicFiltersSelector1 ,
                                              AV53DynamicFiltersOperator1 ,
                                              AV54Lote_Nome1 ,
                                              AV55Lote_UserNom1 ,
                                              AV56DynamicFiltersEnabled2 ,
                                              AV57DynamicFiltersSelector2 ,
                                              AV58DynamicFiltersOperator2 ,
                                              AV59Lote_Nome2 ,
                                              AV60Lote_UserNom2 ,
                                              AV61DynamicFiltersEnabled3 ,
                                              AV62DynamicFiltersSelector3 ,
                                              AV63DynamicFiltersOperator3 ,
                                              AV64Lote_Nome3 ,
                                              AV65Lote_UserNom3 ,
                                              AV11TFLote_Numero_Sel ,
                                              AV10TFLote_Numero ,
                                              AV13TFLote_Nome_Sel ,
                                              AV12TFLote_Nome ,
                                              AV14TFLote_Data ,
                                              AV15TFLote_Data_To ,
                                              AV16TFLote_UserCod ,
                                              AV17TFLote_UserCod_To ,
                                              AV18TFLote_PessoaCod ,
                                              AV19TFLote_PessoaCod_To ,
                                              AV21TFLote_UserNom_Sel ,
                                              AV20TFLote_UserNom ,
                                              AV22TFLote_ValorPF ,
                                              AV23TFLote_ValorPF_To ,
                                              AV28TFLote_QtdeDmn ,
                                              AV29TFLote_QtdeDmn_To ,
                                              A563Lote_Nome ,
                                              A561Lote_UserNom ,
                                              A562Lote_Numero ,
                                              A564Lote_Data ,
                                              A559Lote_UserCod ,
                                              A560Lote_PessoaCod ,
                                              A565Lote_ValorPF ,
                                              A569Lote_QtdeDmn ,
                                              AV9WWPContext.gxTpr_Userehcontratante ,
                                              A1231Lote_ContratadaCod ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV24TFLote_DataIni ,
                                              A567Lote_DataIni ,
                                              AV25TFLote_DataIni_To ,
                                              AV26TFLote_DataFim ,
                                              A568Lote_DataFim ,
                                              AV27TFLote_DataFim_To ,
                                              AV30TFLote_Valor ,
                                              A572Lote_Valor ,
                                              AV31TFLote_Valor_To ,
                                              A595Lote_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV54Lote_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV54Lote_Nome1), 50, "%");
         lV54Lote_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV54Lote_Nome1), 50, "%");
         lV55Lote_UserNom1 = StringUtil.PadR( StringUtil.RTrim( AV55Lote_UserNom1), 100, "%");
         lV55Lote_UserNom1 = StringUtil.PadR( StringUtil.RTrim( AV55Lote_UserNom1), 100, "%");
         lV59Lote_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV59Lote_Nome2), 50, "%");
         lV59Lote_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV59Lote_Nome2), 50, "%");
         lV60Lote_UserNom2 = StringUtil.PadR( StringUtil.RTrim( AV60Lote_UserNom2), 100, "%");
         lV60Lote_UserNom2 = StringUtil.PadR( StringUtil.RTrim( AV60Lote_UserNom2), 100, "%");
         lV64Lote_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV64Lote_Nome3), 50, "%");
         lV64Lote_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV64Lote_Nome3), 50, "%");
         lV65Lote_UserNom3 = StringUtil.PadR( StringUtil.RTrim( AV65Lote_UserNom3), 100, "%");
         lV65Lote_UserNom3 = StringUtil.PadR( StringUtil.RTrim( AV65Lote_UserNom3), 100, "%");
         lV10TFLote_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFLote_Numero), 10, "%");
         lV12TFLote_Nome = StringUtil.PadR( StringUtil.RTrim( AV12TFLote_Nome), 50, "%");
         lV20TFLote_UserNom = StringUtil.PadR( StringUtil.RTrim( AV20TFLote_UserNom), 100, "%");
         /* Using cursor P00MR8 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Userehcontratante, AV9WWPContext.gxTpr_Contratada_codigo, AV24TFLote_DataIni, AV24TFLote_DataIni, AV25TFLote_DataIni_To, AV25TFLote_DataIni_To, AV26TFLote_DataFim, AV26TFLote_DataFim, AV27TFLote_DataFim_To, AV27TFLote_DataFim_To, AV9WWPContext.gxTpr_Areatrabalho_codigo, lV54Lote_Nome1, lV54Lote_Nome1, lV55Lote_UserNom1, lV55Lote_UserNom1, lV59Lote_Nome2, lV59Lote_Nome2, lV60Lote_UserNom2, lV60Lote_UserNom2, lV64Lote_Nome3, lV64Lote_Nome3, lV65Lote_UserNom3, lV65Lote_UserNom3, lV10TFLote_Numero, AV11TFLote_Numero_Sel, lV12TFLote_Nome, AV13TFLote_Nome_Sel, AV14TFLote_Data, AV15TFLote_Data_To, AV16TFLote_UserCod, AV17TFLote_UserCod_To, AV18TFLote_PessoaCod, AV19TFLote_PessoaCod_To, lV20TFLote_UserNom, AV21TFLote_UserNom_Sel, AV22TFLote_ValorPF, AV23TFLote_ValorPF_To, AV28TFLote_QtdeDmn, AV29TFLote_QtdeDmn_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKMR2 = false;
            A562Lote_Numero = P00MR8_A562Lote_Numero[0];
            A565Lote_ValorPF = P00MR8_A565Lote_ValorPF[0];
            A560Lote_PessoaCod = P00MR8_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00MR8_n560Lote_PessoaCod[0];
            A559Lote_UserCod = P00MR8_A559Lote_UserCod[0];
            A564Lote_Data = P00MR8_A564Lote_Data[0];
            A561Lote_UserNom = P00MR8_A561Lote_UserNom[0];
            n561Lote_UserNom = P00MR8_n561Lote_UserNom[0];
            A563Lote_Nome = P00MR8_A563Lote_Nome[0];
            A595Lote_AreaTrabalhoCod = P00MR8_A595Lote_AreaTrabalhoCod[0];
            A596Lote_Codigo = P00MR8_A596Lote_Codigo[0];
            A569Lote_QtdeDmn = P00MR8_A569Lote_QtdeDmn[0];
            A568Lote_DataFim = P00MR8_A568Lote_DataFim[0];
            A567Lote_DataIni = P00MR8_A567Lote_DataIni[0];
            A1231Lote_ContratadaCod = P00MR8_A1231Lote_ContratadaCod[0];
            A1057Lote_ValorGlosas = P00MR8_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00MR8_n1057Lote_ValorGlosas[0];
            A560Lote_PessoaCod = P00MR8_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00MR8_n560Lote_PessoaCod[0];
            A561Lote_UserNom = P00MR8_A561Lote_UserNom[0];
            n561Lote_UserNom = P00MR8_n561Lote_UserNom[0];
            A1057Lote_ValorGlosas = P00MR8_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00MR8_n1057Lote_ValorGlosas[0];
            A569Lote_QtdeDmn = P00MR8_A569Lote_QtdeDmn[0];
            A1231Lote_ContratadaCod = P00MR8_A1231Lote_ContratadaCod[0];
            A568Lote_DataFim = P00MR8_A568Lote_DataFim[0];
            A567Lote_DataIni = P00MR8_A567Lote_DataIni[0];
            GetLote_ValorOSs( A596Lote_Codigo) ;
            A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
            if ( (Convert.ToDecimal(0)==AV30TFLote_Valor) || ( ( A572Lote_Valor >= AV30TFLote_Valor ) ) )
            {
               if ( (Convert.ToDecimal(0)==AV31TFLote_Valor_To) || ( ( A572Lote_Valor <= AV31TFLote_Valor_To ) ) )
               {
                  AV44count = 0;
                  while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00MR8_A562Lote_Numero[0], A562Lote_Numero) == 0 ) )
                  {
                     BRKMR2 = false;
                     A596Lote_Codigo = P00MR8_A596Lote_Codigo[0];
                     AV44count = (long)(AV44count+1);
                     BRKMR2 = true;
                     pr_default.readNext(0);
                  }
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A562Lote_Numero)) )
                  {
                     AV36Option = A562Lote_Numero;
                     AV37Options.Add(AV36Option, 0);
                     AV42OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV44count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                  }
                  if ( AV37Options.Count == 50 )
                  {
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
            }
            if ( ! BRKMR2 )
            {
               BRKMR2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADLOTE_NOMEOPTIONS' Routine */
         AV12TFLote_Nome = AV32SearchTxt;
         AV13TFLote_Nome_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV52DynamicFiltersSelector1 ,
                                              AV53DynamicFiltersOperator1 ,
                                              AV54Lote_Nome1 ,
                                              AV55Lote_UserNom1 ,
                                              AV56DynamicFiltersEnabled2 ,
                                              AV57DynamicFiltersSelector2 ,
                                              AV58DynamicFiltersOperator2 ,
                                              AV59Lote_Nome2 ,
                                              AV60Lote_UserNom2 ,
                                              AV61DynamicFiltersEnabled3 ,
                                              AV62DynamicFiltersSelector3 ,
                                              AV63DynamicFiltersOperator3 ,
                                              AV64Lote_Nome3 ,
                                              AV65Lote_UserNom3 ,
                                              AV11TFLote_Numero_Sel ,
                                              AV10TFLote_Numero ,
                                              AV13TFLote_Nome_Sel ,
                                              AV12TFLote_Nome ,
                                              AV14TFLote_Data ,
                                              AV15TFLote_Data_To ,
                                              AV16TFLote_UserCod ,
                                              AV17TFLote_UserCod_To ,
                                              AV18TFLote_PessoaCod ,
                                              AV19TFLote_PessoaCod_To ,
                                              AV21TFLote_UserNom_Sel ,
                                              AV20TFLote_UserNom ,
                                              AV22TFLote_ValorPF ,
                                              AV23TFLote_ValorPF_To ,
                                              AV28TFLote_QtdeDmn ,
                                              AV29TFLote_QtdeDmn_To ,
                                              A563Lote_Nome ,
                                              A561Lote_UserNom ,
                                              A562Lote_Numero ,
                                              A564Lote_Data ,
                                              A559Lote_UserCod ,
                                              A560Lote_PessoaCod ,
                                              A565Lote_ValorPF ,
                                              A569Lote_QtdeDmn ,
                                              AV9WWPContext.gxTpr_Userehcontratante ,
                                              A1231Lote_ContratadaCod ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV24TFLote_DataIni ,
                                              A567Lote_DataIni ,
                                              AV25TFLote_DataIni_To ,
                                              AV26TFLote_DataFim ,
                                              A568Lote_DataFim ,
                                              AV27TFLote_DataFim_To ,
                                              AV30TFLote_Valor ,
                                              A572Lote_Valor ,
                                              AV31TFLote_Valor_To ,
                                              A595Lote_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV54Lote_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV54Lote_Nome1), 50, "%");
         lV54Lote_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV54Lote_Nome1), 50, "%");
         lV55Lote_UserNom1 = StringUtil.PadR( StringUtil.RTrim( AV55Lote_UserNom1), 100, "%");
         lV55Lote_UserNom1 = StringUtil.PadR( StringUtil.RTrim( AV55Lote_UserNom1), 100, "%");
         lV59Lote_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV59Lote_Nome2), 50, "%");
         lV59Lote_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV59Lote_Nome2), 50, "%");
         lV60Lote_UserNom2 = StringUtil.PadR( StringUtil.RTrim( AV60Lote_UserNom2), 100, "%");
         lV60Lote_UserNom2 = StringUtil.PadR( StringUtil.RTrim( AV60Lote_UserNom2), 100, "%");
         lV64Lote_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV64Lote_Nome3), 50, "%");
         lV64Lote_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV64Lote_Nome3), 50, "%");
         lV65Lote_UserNom3 = StringUtil.PadR( StringUtil.RTrim( AV65Lote_UserNom3), 100, "%");
         lV65Lote_UserNom3 = StringUtil.PadR( StringUtil.RTrim( AV65Lote_UserNom3), 100, "%");
         lV10TFLote_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFLote_Numero), 10, "%");
         lV12TFLote_Nome = StringUtil.PadR( StringUtil.RTrim( AV12TFLote_Nome), 50, "%");
         lV20TFLote_UserNom = StringUtil.PadR( StringUtil.RTrim( AV20TFLote_UserNom), 100, "%");
         /* Using cursor P00MR15 */
         pr_default.execute(1, new Object[] {AV9WWPContext.gxTpr_Userehcontratante, AV9WWPContext.gxTpr_Contratada_codigo, AV24TFLote_DataIni, AV24TFLote_DataIni, AV25TFLote_DataIni_To, AV25TFLote_DataIni_To, AV26TFLote_DataFim, AV26TFLote_DataFim, AV27TFLote_DataFim_To, AV27TFLote_DataFim_To, AV9WWPContext.gxTpr_Areatrabalho_codigo, lV54Lote_Nome1, lV54Lote_Nome1, lV55Lote_UserNom1, lV55Lote_UserNom1, lV59Lote_Nome2, lV59Lote_Nome2, lV60Lote_UserNom2, lV60Lote_UserNom2, lV64Lote_Nome3, lV64Lote_Nome3, lV65Lote_UserNom3, lV65Lote_UserNom3, lV10TFLote_Numero, AV11TFLote_Numero_Sel, lV12TFLote_Nome, AV13TFLote_Nome_Sel, AV14TFLote_Data, AV15TFLote_Data_To, AV16TFLote_UserCod, AV17TFLote_UserCod_To, AV18TFLote_PessoaCod, AV19TFLote_PessoaCod_To, lV20TFLote_UserNom, AV21TFLote_UserNom_Sel, AV22TFLote_ValorPF, AV23TFLote_ValorPF_To, AV28TFLote_QtdeDmn, AV29TFLote_QtdeDmn_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKMR4 = false;
            A595Lote_AreaTrabalhoCod = P00MR15_A595Lote_AreaTrabalhoCod[0];
            A563Lote_Nome = P00MR15_A563Lote_Nome[0];
            A565Lote_ValorPF = P00MR15_A565Lote_ValorPF[0];
            A560Lote_PessoaCod = P00MR15_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00MR15_n560Lote_PessoaCod[0];
            A559Lote_UserCod = P00MR15_A559Lote_UserCod[0];
            A564Lote_Data = P00MR15_A564Lote_Data[0];
            A562Lote_Numero = P00MR15_A562Lote_Numero[0];
            A561Lote_UserNom = P00MR15_A561Lote_UserNom[0];
            n561Lote_UserNom = P00MR15_n561Lote_UserNom[0];
            A596Lote_Codigo = P00MR15_A596Lote_Codigo[0];
            A569Lote_QtdeDmn = P00MR15_A569Lote_QtdeDmn[0];
            A568Lote_DataFim = P00MR15_A568Lote_DataFim[0];
            A567Lote_DataIni = P00MR15_A567Lote_DataIni[0];
            A1231Lote_ContratadaCod = P00MR15_A1231Lote_ContratadaCod[0];
            A1057Lote_ValorGlosas = P00MR15_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00MR15_n1057Lote_ValorGlosas[0];
            A560Lote_PessoaCod = P00MR15_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00MR15_n560Lote_PessoaCod[0];
            A561Lote_UserNom = P00MR15_A561Lote_UserNom[0];
            n561Lote_UserNom = P00MR15_n561Lote_UserNom[0];
            A1057Lote_ValorGlosas = P00MR15_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00MR15_n1057Lote_ValorGlosas[0];
            A569Lote_QtdeDmn = P00MR15_A569Lote_QtdeDmn[0];
            A1231Lote_ContratadaCod = P00MR15_A1231Lote_ContratadaCod[0];
            A568Lote_DataFim = P00MR15_A568Lote_DataFim[0];
            A567Lote_DataIni = P00MR15_A567Lote_DataIni[0];
            GetLote_ValorOSs( A596Lote_Codigo) ;
            A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
            if ( (Convert.ToDecimal(0)==AV30TFLote_Valor) || ( ( A572Lote_Valor >= AV30TFLote_Valor ) ) )
            {
               if ( (Convert.ToDecimal(0)==AV31TFLote_Valor_To) || ( ( A572Lote_Valor <= AV31TFLote_Valor_To ) ) )
               {
                  AV44count = 0;
                  while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00MR15_A563Lote_Nome[0], A563Lote_Nome) == 0 ) )
                  {
                     BRKMR4 = false;
                     A596Lote_Codigo = P00MR15_A596Lote_Codigo[0];
                     AV44count = (long)(AV44count+1);
                     BRKMR4 = true;
                     pr_default.readNext(1);
                  }
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A563Lote_Nome)) )
                  {
                     AV36Option = A563Lote_Nome;
                     AV37Options.Add(AV36Option, 0);
                     AV42OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV44count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                  }
                  if ( AV37Options.Count == 50 )
                  {
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
            }
            if ( ! BRKMR4 )
            {
               BRKMR4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADLOTE_USERNOMOPTIONS' Routine */
         AV20TFLote_UserNom = AV32SearchTxt;
         AV21TFLote_UserNom_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV52DynamicFiltersSelector1 ,
                                              AV53DynamicFiltersOperator1 ,
                                              AV54Lote_Nome1 ,
                                              AV55Lote_UserNom1 ,
                                              AV56DynamicFiltersEnabled2 ,
                                              AV57DynamicFiltersSelector2 ,
                                              AV58DynamicFiltersOperator2 ,
                                              AV59Lote_Nome2 ,
                                              AV60Lote_UserNom2 ,
                                              AV61DynamicFiltersEnabled3 ,
                                              AV62DynamicFiltersSelector3 ,
                                              AV63DynamicFiltersOperator3 ,
                                              AV64Lote_Nome3 ,
                                              AV65Lote_UserNom3 ,
                                              AV11TFLote_Numero_Sel ,
                                              AV10TFLote_Numero ,
                                              AV13TFLote_Nome_Sel ,
                                              AV12TFLote_Nome ,
                                              AV14TFLote_Data ,
                                              AV15TFLote_Data_To ,
                                              AV16TFLote_UserCod ,
                                              AV17TFLote_UserCod_To ,
                                              AV18TFLote_PessoaCod ,
                                              AV19TFLote_PessoaCod_To ,
                                              AV21TFLote_UserNom_Sel ,
                                              AV20TFLote_UserNom ,
                                              AV22TFLote_ValorPF ,
                                              AV23TFLote_ValorPF_To ,
                                              AV28TFLote_QtdeDmn ,
                                              AV29TFLote_QtdeDmn_To ,
                                              A563Lote_Nome ,
                                              A561Lote_UserNom ,
                                              A562Lote_Numero ,
                                              A564Lote_Data ,
                                              A559Lote_UserCod ,
                                              A560Lote_PessoaCod ,
                                              A565Lote_ValorPF ,
                                              A569Lote_QtdeDmn ,
                                              AV9WWPContext.gxTpr_Userehcontratante ,
                                              A1231Lote_ContratadaCod ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV24TFLote_DataIni ,
                                              A567Lote_DataIni ,
                                              AV25TFLote_DataIni_To ,
                                              AV26TFLote_DataFim ,
                                              A568Lote_DataFim ,
                                              AV27TFLote_DataFim_To ,
                                              AV30TFLote_Valor ,
                                              A572Lote_Valor ,
                                              AV31TFLote_Valor_To ,
                                              A595Lote_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV54Lote_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV54Lote_Nome1), 50, "%");
         lV54Lote_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV54Lote_Nome1), 50, "%");
         lV55Lote_UserNom1 = StringUtil.PadR( StringUtil.RTrim( AV55Lote_UserNom1), 100, "%");
         lV55Lote_UserNom1 = StringUtil.PadR( StringUtil.RTrim( AV55Lote_UserNom1), 100, "%");
         lV59Lote_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV59Lote_Nome2), 50, "%");
         lV59Lote_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV59Lote_Nome2), 50, "%");
         lV60Lote_UserNom2 = StringUtil.PadR( StringUtil.RTrim( AV60Lote_UserNom2), 100, "%");
         lV60Lote_UserNom2 = StringUtil.PadR( StringUtil.RTrim( AV60Lote_UserNom2), 100, "%");
         lV64Lote_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV64Lote_Nome3), 50, "%");
         lV64Lote_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV64Lote_Nome3), 50, "%");
         lV65Lote_UserNom3 = StringUtil.PadR( StringUtil.RTrim( AV65Lote_UserNom3), 100, "%");
         lV65Lote_UserNom3 = StringUtil.PadR( StringUtil.RTrim( AV65Lote_UserNom3), 100, "%");
         lV10TFLote_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFLote_Numero), 10, "%");
         lV12TFLote_Nome = StringUtil.PadR( StringUtil.RTrim( AV12TFLote_Nome), 50, "%");
         lV20TFLote_UserNom = StringUtil.PadR( StringUtil.RTrim( AV20TFLote_UserNom), 100, "%");
         /* Using cursor P00MR22 */
         pr_default.execute(2, new Object[] {AV9WWPContext.gxTpr_Userehcontratante, AV9WWPContext.gxTpr_Contratada_codigo, AV24TFLote_DataIni, AV24TFLote_DataIni, AV25TFLote_DataIni_To, AV25TFLote_DataIni_To, AV26TFLote_DataFim, AV26TFLote_DataFim, AV27TFLote_DataFim_To, AV27TFLote_DataFim_To, AV9WWPContext.gxTpr_Areatrabalho_codigo, lV54Lote_Nome1, lV54Lote_Nome1, lV55Lote_UserNom1, lV55Lote_UserNom1, lV59Lote_Nome2, lV59Lote_Nome2, lV60Lote_UserNom2, lV60Lote_UserNom2, lV64Lote_Nome3, lV64Lote_Nome3, lV65Lote_UserNom3, lV65Lote_UserNom3, lV10TFLote_Numero, AV11TFLote_Numero_Sel, lV12TFLote_Nome, AV13TFLote_Nome_Sel, AV14TFLote_Data, AV15TFLote_Data_To, AV16TFLote_UserCod, AV17TFLote_UserCod_To, AV18TFLote_PessoaCod, AV19TFLote_PessoaCod_To, lV20TFLote_UserNom, AV21TFLote_UserNom_Sel, AV22TFLote_ValorPF, AV23TFLote_ValorPF_To, AV28TFLote_QtdeDmn, AV29TFLote_QtdeDmn_To});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKMR6 = false;
            A595Lote_AreaTrabalhoCod = P00MR22_A595Lote_AreaTrabalhoCod[0];
            A561Lote_UserNom = P00MR22_A561Lote_UserNom[0];
            n561Lote_UserNom = P00MR22_n561Lote_UserNom[0];
            A565Lote_ValorPF = P00MR22_A565Lote_ValorPF[0];
            A560Lote_PessoaCod = P00MR22_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00MR22_n560Lote_PessoaCod[0];
            A559Lote_UserCod = P00MR22_A559Lote_UserCod[0];
            A564Lote_Data = P00MR22_A564Lote_Data[0];
            A562Lote_Numero = P00MR22_A562Lote_Numero[0];
            A563Lote_Nome = P00MR22_A563Lote_Nome[0];
            A596Lote_Codigo = P00MR22_A596Lote_Codigo[0];
            A569Lote_QtdeDmn = P00MR22_A569Lote_QtdeDmn[0];
            A568Lote_DataFim = P00MR22_A568Lote_DataFim[0];
            A567Lote_DataIni = P00MR22_A567Lote_DataIni[0];
            A1231Lote_ContratadaCod = P00MR22_A1231Lote_ContratadaCod[0];
            A1057Lote_ValorGlosas = P00MR22_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00MR22_n1057Lote_ValorGlosas[0];
            A560Lote_PessoaCod = P00MR22_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00MR22_n560Lote_PessoaCod[0];
            A561Lote_UserNom = P00MR22_A561Lote_UserNom[0];
            n561Lote_UserNom = P00MR22_n561Lote_UserNom[0];
            A1057Lote_ValorGlosas = P00MR22_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00MR22_n1057Lote_ValorGlosas[0];
            A569Lote_QtdeDmn = P00MR22_A569Lote_QtdeDmn[0];
            A1231Lote_ContratadaCod = P00MR22_A1231Lote_ContratadaCod[0];
            A568Lote_DataFim = P00MR22_A568Lote_DataFim[0];
            A567Lote_DataIni = P00MR22_A567Lote_DataIni[0];
            GetLote_ValorOSs( A596Lote_Codigo) ;
            A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
            if ( (Convert.ToDecimal(0)==AV30TFLote_Valor) || ( ( A572Lote_Valor >= AV30TFLote_Valor ) ) )
            {
               if ( (Convert.ToDecimal(0)==AV31TFLote_Valor_To) || ( ( A572Lote_Valor <= AV31TFLote_Valor_To ) ) )
               {
                  AV44count = 0;
                  while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00MR22_A561Lote_UserNom[0], A561Lote_UserNom) == 0 ) )
                  {
                     BRKMR6 = false;
                     A560Lote_PessoaCod = P00MR22_A560Lote_PessoaCod[0];
                     n560Lote_PessoaCod = P00MR22_n560Lote_PessoaCod[0];
                     A559Lote_UserCod = P00MR22_A559Lote_UserCod[0];
                     A596Lote_Codigo = P00MR22_A596Lote_Codigo[0];
                     A560Lote_PessoaCod = P00MR22_A560Lote_PessoaCod[0];
                     n560Lote_PessoaCod = P00MR22_n560Lote_PessoaCod[0];
                     AV44count = (long)(AV44count+1);
                     BRKMR6 = true;
                     pr_default.readNext(2);
                  }
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A561Lote_UserNom)) )
                  {
                     AV36Option = A561Lote_UserNom;
                     AV37Options.Add(AV36Option, 0);
                     AV42OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV44count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                  }
                  if ( AV37Options.Count == 50 )
                  {
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
            }
            if ( ! BRKMR6 )
            {
               BRKMR6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void GetLote_ValorOSs( int A596Lote_Codigo )
      {
         /* Create private dbobjects */
         /* Navigation */
         A1058Lote_ValorOSs = 0;
         /* Using cursor P00MR23 */
         pr_default.execute(3, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(3) != 101) && ( P00MR23_A597ContagemResultado_LoteAceiteCod[0] == A596Lote_Codigo ) )
         {
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  P00MR23_A456ContagemResultado_Codigo[0], out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*P00MR23_A512ContagemResultado_ValorPF[0]);
            A1058Lote_ValorOSs = (decimal)(A1058Lote_ValorOSs+A606ContagemResultado_ValorFinal);
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV37Options = new GxSimpleCollection();
         AV40OptionsDesc = new GxSimpleCollection();
         AV42OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV45Session = context.GetSession();
         AV47GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV48GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFLote_Numero = "";
         AV11TFLote_Numero_Sel = "";
         AV12TFLote_Nome = "";
         AV13TFLote_Nome_Sel = "";
         AV14TFLote_Data = (DateTime)(DateTime.MinValue);
         AV15TFLote_Data_To = (DateTime)(DateTime.MinValue);
         AV20TFLote_UserNom = "";
         AV21TFLote_UserNom_Sel = "";
         AV24TFLote_DataIni = DateTime.MinValue;
         AV25TFLote_DataIni_To = DateTime.MinValue;
         AV26TFLote_DataFim = DateTime.MinValue;
         AV27TFLote_DataFim_To = DateTime.MinValue;
         AV49GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV52DynamicFiltersSelector1 = "";
         AV54Lote_Nome1 = "";
         AV55Lote_UserNom1 = "";
         AV57DynamicFiltersSelector2 = "";
         AV59Lote_Nome2 = "";
         AV60Lote_UserNom2 = "";
         AV62DynamicFiltersSelector3 = "";
         AV64Lote_Nome3 = "";
         AV65Lote_UserNom3 = "";
         scmdbuf = "";
         lV54Lote_Nome1 = "";
         lV55Lote_UserNom1 = "";
         lV59Lote_Nome2 = "";
         lV60Lote_UserNom2 = "";
         lV64Lote_Nome3 = "";
         lV65Lote_UserNom3 = "";
         lV10TFLote_Numero = "";
         lV12TFLote_Nome = "";
         lV20TFLote_UserNom = "";
         A563Lote_Nome = "";
         A561Lote_UserNom = "";
         A562Lote_Numero = "";
         A564Lote_Data = (DateTime)(DateTime.MinValue);
         A567Lote_DataIni = DateTime.MinValue;
         A568Lote_DataFim = DateTime.MinValue;
         P00MR8_A562Lote_Numero = new String[] {""} ;
         P00MR8_A565Lote_ValorPF = new decimal[1] ;
         P00MR8_A560Lote_PessoaCod = new int[1] ;
         P00MR8_n560Lote_PessoaCod = new bool[] {false} ;
         P00MR8_A559Lote_UserCod = new int[1] ;
         P00MR8_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         P00MR8_A561Lote_UserNom = new String[] {""} ;
         P00MR8_n561Lote_UserNom = new bool[] {false} ;
         P00MR8_A563Lote_Nome = new String[] {""} ;
         P00MR8_A595Lote_AreaTrabalhoCod = new int[1] ;
         P00MR8_A596Lote_Codigo = new int[1] ;
         P00MR8_A569Lote_QtdeDmn = new short[1] ;
         P00MR8_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         P00MR8_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         P00MR8_A1231Lote_ContratadaCod = new int[1] ;
         P00MR8_A1057Lote_ValorGlosas = new decimal[1] ;
         P00MR8_n1057Lote_ValorGlosas = new bool[] {false} ;
         AV36Option = "";
         P00MR15_A595Lote_AreaTrabalhoCod = new int[1] ;
         P00MR15_A563Lote_Nome = new String[] {""} ;
         P00MR15_A565Lote_ValorPF = new decimal[1] ;
         P00MR15_A560Lote_PessoaCod = new int[1] ;
         P00MR15_n560Lote_PessoaCod = new bool[] {false} ;
         P00MR15_A559Lote_UserCod = new int[1] ;
         P00MR15_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         P00MR15_A562Lote_Numero = new String[] {""} ;
         P00MR15_A561Lote_UserNom = new String[] {""} ;
         P00MR15_n561Lote_UserNom = new bool[] {false} ;
         P00MR15_A596Lote_Codigo = new int[1] ;
         P00MR15_A569Lote_QtdeDmn = new short[1] ;
         P00MR15_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         P00MR15_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         P00MR15_A1231Lote_ContratadaCod = new int[1] ;
         P00MR15_A1057Lote_ValorGlosas = new decimal[1] ;
         P00MR15_n1057Lote_ValorGlosas = new bool[] {false} ;
         P00MR22_A595Lote_AreaTrabalhoCod = new int[1] ;
         P00MR22_A561Lote_UserNom = new String[] {""} ;
         P00MR22_n561Lote_UserNom = new bool[] {false} ;
         P00MR22_A565Lote_ValorPF = new decimal[1] ;
         P00MR22_A560Lote_PessoaCod = new int[1] ;
         P00MR22_n560Lote_PessoaCod = new bool[] {false} ;
         P00MR22_A559Lote_UserCod = new int[1] ;
         P00MR22_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         P00MR22_A562Lote_Numero = new String[] {""} ;
         P00MR22_A563Lote_Nome = new String[] {""} ;
         P00MR22_A596Lote_Codigo = new int[1] ;
         P00MR22_A569Lote_QtdeDmn = new short[1] ;
         P00MR22_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         P00MR22_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         P00MR22_A1231Lote_ContratadaCod = new int[1] ;
         P00MR22_A1057Lote_ValorGlosas = new decimal[1] ;
         P00MR22_n1057Lote_ValorGlosas = new bool[] {false} ;
         P00MR23_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00MR23_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00MR23_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00MR23_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00MR23_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptlotefilterdata__default(),
            new Object[][] {
                new Object[] {
               P00MR8_A562Lote_Numero, P00MR8_A565Lote_ValorPF, P00MR8_A560Lote_PessoaCod, P00MR8_n560Lote_PessoaCod, P00MR8_A559Lote_UserCod, P00MR8_A564Lote_Data, P00MR8_A561Lote_UserNom, P00MR8_n561Lote_UserNom, P00MR8_A563Lote_Nome, P00MR8_A595Lote_AreaTrabalhoCod,
               P00MR8_A596Lote_Codigo, P00MR8_A569Lote_QtdeDmn, P00MR8_A568Lote_DataFim, P00MR8_A567Lote_DataIni, P00MR8_A1231Lote_ContratadaCod, P00MR8_A1057Lote_ValorGlosas, P00MR8_n1057Lote_ValorGlosas
               }
               , new Object[] {
               P00MR15_A595Lote_AreaTrabalhoCod, P00MR15_A563Lote_Nome, P00MR15_A565Lote_ValorPF, P00MR15_A560Lote_PessoaCod, P00MR15_n560Lote_PessoaCod, P00MR15_A559Lote_UserCod, P00MR15_A564Lote_Data, P00MR15_A562Lote_Numero, P00MR15_A561Lote_UserNom, P00MR15_n561Lote_UserNom,
               P00MR15_A596Lote_Codigo, P00MR15_A569Lote_QtdeDmn, P00MR15_A568Lote_DataFim, P00MR15_A567Lote_DataIni, P00MR15_A1231Lote_ContratadaCod, P00MR15_A1057Lote_ValorGlosas, P00MR15_n1057Lote_ValorGlosas
               }
               , new Object[] {
               P00MR22_A595Lote_AreaTrabalhoCod, P00MR22_A561Lote_UserNom, P00MR22_n561Lote_UserNom, P00MR22_A565Lote_ValorPF, P00MR22_A560Lote_PessoaCod, P00MR22_n560Lote_PessoaCod, P00MR22_A559Lote_UserCod, P00MR22_A564Lote_Data, P00MR22_A562Lote_Numero, P00MR22_A563Lote_Nome,
               P00MR22_A596Lote_Codigo, P00MR22_A569Lote_QtdeDmn, P00MR22_A568Lote_DataFim, P00MR22_A567Lote_DataIni, P00MR22_A1231Lote_ContratadaCod, P00MR22_A1057Lote_ValorGlosas, P00MR22_n1057Lote_ValorGlosas
               }
               , new Object[] {
               P00MR23_A597ContagemResultado_LoteAceiteCod, P00MR23_n597ContagemResultado_LoteAceiteCod, P00MR23_A512ContagemResultado_ValorPF, P00MR23_n512ContagemResultado_ValorPF, P00MR23_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV28TFLote_QtdeDmn ;
      private short AV29TFLote_QtdeDmn_To ;
      private short AV53DynamicFiltersOperator1 ;
      private short AV58DynamicFiltersOperator2 ;
      private short AV63DynamicFiltersOperator3 ;
      private short A569Lote_QtdeDmn ;
      private int AV68GXV1 ;
      private int AV50Lote_AreaTrabalhoCod ;
      private int AV51Lote_ContratadaCod ;
      private int AV16TFLote_UserCod ;
      private int AV17TFLote_UserCod_To ;
      private int AV18TFLote_PessoaCod ;
      private int AV19TFLote_PessoaCod_To ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A559Lote_UserCod ;
      private int A560Lote_PessoaCod ;
      private int A1231Lote_ContratadaCod ;
      private int A595Lote_AreaTrabalhoCod ;
      private int A596Lote_Codigo ;
      private long AV44count ;
      private decimal AV22TFLote_ValorPF ;
      private decimal AV23TFLote_ValorPF_To ;
      private decimal AV30TFLote_Valor ;
      private decimal AV31TFLote_Valor_To ;
      private decimal A565Lote_ValorPF ;
      private decimal A572Lote_Valor ;
      private decimal A1057Lote_ValorGlosas ;
      private decimal A1058Lote_ValorOSs ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private decimal A606ContagemResultado_ValorFinal ;
      private String AV10TFLote_Numero ;
      private String AV11TFLote_Numero_Sel ;
      private String AV12TFLote_Nome ;
      private String AV13TFLote_Nome_Sel ;
      private String AV20TFLote_UserNom ;
      private String AV21TFLote_UserNom_Sel ;
      private String AV54Lote_Nome1 ;
      private String AV55Lote_UserNom1 ;
      private String AV59Lote_Nome2 ;
      private String AV60Lote_UserNom2 ;
      private String AV64Lote_Nome3 ;
      private String AV65Lote_UserNom3 ;
      private String scmdbuf ;
      private String lV54Lote_Nome1 ;
      private String lV55Lote_UserNom1 ;
      private String lV59Lote_Nome2 ;
      private String lV60Lote_UserNom2 ;
      private String lV64Lote_Nome3 ;
      private String lV65Lote_UserNom3 ;
      private String lV10TFLote_Numero ;
      private String lV12TFLote_Nome ;
      private String lV20TFLote_UserNom ;
      private String A563Lote_Nome ;
      private String A561Lote_UserNom ;
      private String A562Lote_Numero ;
      private DateTime AV14TFLote_Data ;
      private DateTime AV15TFLote_Data_To ;
      private DateTime A564Lote_Data ;
      private DateTime AV24TFLote_DataIni ;
      private DateTime AV25TFLote_DataIni_To ;
      private DateTime AV26TFLote_DataFim ;
      private DateTime AV27TFLote_DataFim_To ;
      private DateTime A567Lote_DataIni ;
      private DateTime A568Lote_DataFim ;
      private bool returnInSub ;
      private bool AV56DynamicFiltersEnabled2 ;
      private bool AV61DynamicFiltersEnabled3 ;
      private bool AV9WWPContext_gxTpr_Userehcontratante ;
      private bool BRKMR2 ;
      private bool n560Lote_PessoaCod ;
      private bool n561Lote_UserNom ;
      private bool n1057Lote_ValorGlosas ;
      private bool BRKMR4 ;
      private bool BRKMR6 ;
      private String AV43OptionIndexesJson ;
      private String AV38OptionsJson ;
      private String AV41OptionsDescJson ;
      private String AV34DDOName ;
      private String AV32SearchTxt ;
      private String AV33SearchTxtTo ;
      private String AV52DynamicFiltersSelector1 ;
      private String AV57DynamicFiltersSelector2 ;
      private String AV62DynamicFiltersSelector3 ;
      private String AV36Option ;
      private IGxSession AV45Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00MR8_A562Lote_Numero ;
      private decimal[] P00MR8_A565Lote_ValorPF ;
      private int[] P00MR8_A560Lote_PessoaCod ;
      private bool[] P00MR8_n560Lote_PessoaCod ;
      private int[] P00MR8_A559Lote_UserCod ;
      private DateTime[] P00MR8_A564Lote_Data ;
      private String[] P00MR8_A561Lote_UserNom ;
      private bool[] P00MR8_n561Lote_UserNom ;
      private String[] P00MR8_A563Lote_Nome ;
      private int[] P00MR8_A595Lote_AreaTrabalhoCod ;
      private int[] P00MR8_A596Lote_Codigo ;
      private short[] P00MR8_A569Lote_QtdeDmn ;
      private DateTime[] P00MR8_A568Lote_DataFim ;
      private DateTime[] P00MR8_A567Lote_DataIni ;
      private int[] P00MR8_A1231Lote_ContratadaCod ;
      private decimal[] P00MR8_A1057Lote_ValorGlosas ;
      private bool[] P00MR8_n1057Lote_ValorGlosas ;
      private int[] P00MR15_A595Lote_AreaTrabalhoCod ;
      private String[] P00MR15_A563Lote_Nome ;
      private decimal[] P00MR15_A565Lote_ValorPF ;
      private int[] P00MR15_A560Lote_PessoaCod ;
      private bool[] P00MR15_n560Lote_PessoaCod ;
      private int[] P00MR15_A559Lote_UserCod ;
      private DateTime[] P00MR15_A564Lote_Data ;
      private String[] P00MR15_A562Lote_Numero ;
      private String[] P00MR15_A561Lote_UserNom ;
      private bool[] P00MR15_n561Lote_UserNom ;
      private int[] P00MR15_A596Lote_Codigo ;
      private short[] P00MR15_A569Lote_QtdeDmn ;
      private DateTime[] P00MR15_A568Lote_DataFim ;
      private DateTime[] P00MR15_A567Lote_DataIni ;
      private int[] P00MR15_A1231Lote_ContratadaCod ;
      private decimal[] P00MR15_A1057Lote_ValorGlosas ;
      private bool[] P00MR15_n1057Lote_ValorGlosas ;
      private int[] P00MR22_A595Lote_AreaTrabalhoCod ;
      private String[] P00MR22_A561Lote_UserNom ;
      private bool[] P00MR22_n561Lote_UserNom ;
      private decimal[] P00MR22_A565Lote_ValorPF ;
      private int[] P00MR22_A560Lote_PessoaCod ;
      private bool[] P00MR22_n560Lote_PessoaCod ;
      private int[] P00MR22_A559Lote_UserCod ;
      private DateTime[] P00MR22_A564Lote_Data ;
      private String[] P00MR22_A562Lote_Numero ;
      private String[] P00MR22_A563Lote_Nome ;
      private int[] P00MR22_A596Lote_Codigo ;
      private short[] P00MR22_A569Lote_QtdeDmn ;
      private DateTime[] P00MR22_A568Lote_DataFim ;
      private DateTime[] P00MR22_A567Lote_DataIni ;
      private int[] P00MR22_A1231Lote_ContratadaCod ;
      private decimal[] P00MR22_A1057Lote_ValorGlosas ;
      private bool[] P00MR22_n1057Lote_ValorGlosas ;
      private int[] P00MR23_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00MR23_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] P00MR23_A512ContagemResultado_ValorPF ;
      private bool[] P00MR23_n512ContagemResultado_ValorPF ;
      private int[] P00MR23_A456ContagemResultado_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV37Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV40OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV42OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV47GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV48GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV49GridStateDynamicFilter ;
   }

   public class getpromptlotefilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00MR8( IGxContext context ,
                                             String AV52DynamicFiltersSelector1 ,
                                             short AV53DynamicFiltersOperator1 ,
                                             String AV54Lote_Nome1 ,
                                             String AV55Lote_UserNom1 ,
                                             bool AV56DynamicFiltersEnabled2 ,
                                             String AV57DynamicFiltersSelector2 ,
                                             short AV58DynamicFiltersOperator2 ,
                                             String AV59Lote_Nome2 ,
                                             String AV60Lote_UserNom2 ,
                                             bool AV61DynamicFiltersEnabled3 ,
                                             String AV62DynamicFiltersSelector3 ,
                                             short AV63DynamicFiltersOperator3 ,
                                             String AV64Lote_Nome3 ,
                                             String AV65Lote_UserNom3 ,
                                             String AV11TFLote_Numero_Sel ,
                                             String AV10TFLote_Numero ,
                                             String AV13TFLote_Nome_Sel ,
                                             String AV12TFLote_Nome ,
                                             DateTime AV14TFLote_Data ,
                                             DateTime AV15TFLote_Data_To ,
                                             int AV16TFLote_UserCod ,
                                             int AV17TFLote_UserCod_To ,
                                             int AV18TFLote_PessoaCod ,
                                             int AV19TFLote_PessoaCod_To ,
                                             String AV21TFLote_UserNom_Sel ,
                                             String AV20TFLote_UserNom ,
                                             decimal AV22TFLote_ValorPF ,
                                             decimal AV23TFLote_ValorPF_To ,
                                             short AV28TFLote_QtdeDmn ,
                                             short AV29TFLote_QtdeDmn_To ,
                                             String A563Lote_Nome ,
                                             String A561Lote_UserNom ,
                                             String A562Lote_Numero ,
                                             DateTime A564Lote_Data ,
                                             int A559Lote_UserCod ,
                                             int A560Lote_PessoaCod ,
                                             decimal A565Lote_ValorPF ,
                                             short A569Lote_QtdeDmn ,
                                             bool AV9WWPContext_gxTpr_Userehcontratante ,
                                             int A1231Lote_ContratadaCod ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             DateTime AV24TFLote_DataIni ,
                                             DateTime A567Lote_DataIni ,
                                             DateTime AV25TFLote_DataIni_To ,
                                             DateTime AV26TFLote_DataFim ,
                                             DateTime A568Lote_DataFim ,
                                             DateTime AV27TFLote_DataFim_To ,
                                             decimal AV30TFLote_Valor ,
                                             decimal A572Lote_Valor ,
                                             decimal AV31TFLote_Valor_To ,
                                             int A595Lote_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [39] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[Lote_Numero], T1.[Lote_ValorPF], T2.[Usuario_PessoaCod] AS Lote_PessoaCod, T1.[Lote_UserCod] AS Lote_UserCod, T1.[Lote_Data], T3.[Pessoa_Nome] AS Lote_UserNom, T1.[Lote_Nome], T1.[Lote_AreaTrabalhoCod], T1.[Lote_Codigo], COALESCE( T5.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn, COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim, COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T5.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod, COALESCE( T4.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas FROM ((((([Lote] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Lote_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN (SELECT COALESCE( T9.[GXC2], 0) + COALESCE( T8.[GXC3], 0) AS Lote_ValorGlosas, T7.[Lote_Codigo] FROM (([Lote] T7 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T10.[ContagemResultadoIndicadores_Valor]) AS GXC3, T11.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T10 WITH (NOLOCK),  [Lote] T11 WITH (NOLOCK) WHERE T10.[ContagemResultadoIndicadores_LoteCod] = T11.[Lote_Codigo] GROUP BY T11.[Lote_Codigo] ) T8 ON T8.[Lote_Codigo] = T7.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC2, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T9 ON T9.[ContagemResultado_LoteAceiteCod] = T7.[Lote_Codigo]) ) T4 ON T4.[Lote_Codigo] = T1.[Lote_Codigo]) LEFT JOIN (SELECT COUNT(*) AS Lote_QtdeDmn, [ContagemResultado_LoteAceiteCod], MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T5 ON T5.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) LEFT JOIN";
         scmdbuf = scmdbuf + " (SELECT MAX(COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim, T7.[ContagemResultado_LoteAceiteCod], MIN(COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni FROM ([ContagemResultado] T7 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T7.[ContagemResultado_Codigo]) GROUP BY T7.[ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo])";
         scmdbuf = scmdbuf + " WHERE (@AV9WWPCo_2Userehcontratante = 1 or ( COALESCE( T5.[Lote_ContratadaCod], 0) = @AV9WWPCo_1Contratada_codigo))";
         scmdbuf = scmdbuf + " and ((@AV24TFLote_DataIni = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) >= @AV24TFLote_DataIni))";
         scmdbuf = scmdbuf + " and ((@AV25TFLote_DataIni_To = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) <= @AV25TFLote_DataIni_To))";
         scmdbuf = scmdbuf + " and ((@AV26TFLote_DataFim = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV26TFLote_DataFim))";
         scmdbuf = scmdbuf + " and ((@AV27TFLote_DataFim_To = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) <= @AV27TFLote_DataFim_To))";
         scmdbuf = scmdbuf + " and (T1.[Lote_AreaTrabalhoCod] = @AV9WWPCo_3Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Lote_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV54Lote_Nome1)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Lote_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV54Lote_Nome1)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_USERNOM") == 0 ) && ( AV53DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Lote_UserNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV55Lote_UserNom1)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_USERNOM") == 0 ) && ( AV53DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Lote_UserNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV55Lote_UserNom1)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV56DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_NOME") == 0 ) && ( AV58DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Lote_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV59Lote_Nome2)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV56DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_NOME") == 0 ) && ( AV58DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Lote_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV59Lote_Nome2)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV56DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_USERNOM") == 0 ) && ( AV58DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Lote_UserNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV60Lote_UserNom2)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( AV56DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_USERNOM") == 0 ) && ( AV58DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Lote_UserNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV60Lote_UserNom2)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_NOME") == 0 ) && ( AV63DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64Lote_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV64Lote_Nome3)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_NOME") == 0 ) && ( AV63DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64Lote_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV64Lote_Nome3)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_USERNOM") == 0 ) && ( AV63DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65Lote_UserNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV65Lote_UserNom3)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_USERNOM") == 0 ) && ( AV63DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65Lote_UserNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV65Lote_UserNom3)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFLote_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFLote_Numero)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like @lV10TFLote_Numero)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFLote_Numero_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] = @AV11TFLote_Numero_Sel)";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFLote_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFLote_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV12TFLote_Nome)";
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFLote_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] = @AV13TFLote_Nome_Sel)";
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFLote_Data) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] >= @AV14TFLote_Data)";
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFLote_Data_To) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] <= @AV15TFLote_Data_To)";
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( ! (0==AV16TFLote_UserCod) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_UserCod] >= @AV16TFLote_UserCod)";
         }
         else
         {
            GXv_int2[29] = 1;
         }
         if ( ! (0==AV17TFLote_UserCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_UserCod] <= @AV17TFLote_UserCod_To)";
         }
         else
         {
            GXv_int2[30] = 1;
         }
         if ( ! (0==AV18TFLote_PessoaCod) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV18TFLote_PessoaCod)";
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( ! (0==AV19TFLote_PessoaCod_To) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV19TFLote_PessoaCod_To)";
         }
         else
         {
            GXv_int2[32] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFLote_UserNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFLote_UserNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV20TFLote_UserNom)";
         }
         else
         {
            GXv_int2[33] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFLote_UserNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV21TFLote_UserNom_Sel)";
         }
         else
         {
            GXv_int2[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFLote_ValorPF) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] >= @AV22TFLote_ValorPF)";
         }
         else
         {
            GXv_int2[35] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFLote_ValorPF_To) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] <= @AV23TFLote_ValorPF_To)";
         }
         else
         {
            GXv_int2[36] = 1;
         }
         if ( ! (0==AV28TFLote_QtdeDmn) )
         {
            sWhereString = sWhereString + " and (COALESCE( T5.[Lote_QtdeDmn], 0) >= @AV28TFLote_QtdeDmn)";
         }
         else
         {
            GXv_int2[37] = 1;
         }
         if ( ! (0==AV29TFLote_QtdeDmn_To) )
         {
            sWhereString = sWhereString + " and (COALESCE( T5.[Lote_QtdeDmn], 0) <= @AV29TFLote_QtdeDmn_To)";
         }
         else
         {
            GXv_int2[38] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Numero]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P00MR15( IGxContext context ,
                                              String AV52DynamicFiltersSelector1 ,
                                              short AV53DynamicFiltersOperator1 ,
                                              String AV54Lote_Nome1 ,
                                              String AV55Lote_UserNom1 ,
                                              bool AV56DynamicFiltersEnabled2 ,
                                              String AV57DynamicFiltersSelector2 ,
                                              short AV58DynamicFiltersOperator2 ,
                                              String AV59Lote_Nome2 ,
                                              String AV60Lote_UserNom2 ,
                                              bool AV61DynamicFiltersEnabled3 ,
                                              String AV62DynamicFiltersSelector3 ,
                                              short AV63DynamicFiltersOperator3 ,
                                              String AV64Lote_Nome3 ,
                                              String AV65Lote_UserNom3 ,
                                              String AV11TFLote_Numero_Sel ,
                                              String AV10TFLote_Numero ,
                                              String AV13TFLote_Nome_Sel ,
                                              String AV12TFLote_Nome ,
                                              DateTime AV14TFLote_Data ,
                                              DateTime AV15TFLote_Data_To ,
                                              int AV16TFLote_UserCod ,
                                              int AV17TFLote_UserCod_To ,
                                              int AV18TFLote_PessoaCod ,
                                              int AV19TFLote_PessoaCod_To ,
                                              String AV21TFLote_UserNom_Sel ,
                                              String AV20TFLote_UserNom ,
                                              decimal AV22TFLote_ValorPF ,
                                              decimal AV23TFLote_ValorPF_To ,
                                              short AV28TFLote_QtdeDmn ,
                                              short AV29TFLote_QtdeDmn_To ,
                                              String A563Lote_Nome ,
                                              String A561Lote_UserNom ,
                                              String A562Lote_Numero ,
                                              DateTime A564Lote_Data ,
                                              int A559Lote_UserCod ,
                                              int A560Lote_PessoaCod ,
                                              decimal A565Lote_ValorPF ,
                                              short A569Lote_QtdeDmn ,
                                              bool AV9WWPContext_gxTpr_Userehcontratante ,
                                              int A1231Lote_ContratadaCod ,
                                              int AV9WWPContext_gxTpr_Contratada_codigo ,
                                              DateTime AV24TFLote_DataIni ,
                                              DateTime A567Lote_DataIni ,
                                              DateTime AV25TFLote_DataIni_To ,
                                              DateTime AV26TFLote_DataFim ,
                                              DateTime A568Lote_DataFim ,
                                              DateTime AV27TFLote_DataFim_To ,
                                              decimal AV30TFLote_Valor ,
                                              decimal A572Lote_Valor ,
                                              decimal AV31TFLote_Valor_To ,
                                              int A595Lote_AreaTrabalhoCod ,
                                              int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [39] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[Lote_AreaTrabalhoCod], T1.[Lote_Nome], T1.[Lote_ValorPF], T2.[Usuario_PessoaCod] AS Lote_PessoaCod, T1.[Lote_UserCod] AS Lote_UserCod, T1.[Lote_Data], T1.[Lote_Numero], T3.[Pessoa_Nome] AS Lote_UserNom, T1.[Lote_Codigo], COALESCE( T5.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn, COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim, COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T5.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod, COALESCE( T4.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas FROM ((((([Lote] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Lote_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN (SELECT COALESCE( T9.[GXC2], 0) + COALESCE( T8.[GXC3], 0) AS Lote_ValorGlosas, T7.[Lote_Codigo] FROM (([Lote] T7 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T10.[ContagemResultadoIndicadores_Valor]) AS GXC3, T11.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T10 WITH (NOLOCK),  [Lote] T11 WITH (NOLOCK) WHERE T10.[ContagemResultadoIndicadores_LoteCod] = T11.[Lote_Codigo] GROUP BY T11.[Lote_Codigo] ) T8 ON T8.[Lote_Codigo] = T7.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC2, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T9 ON T9.[ContagemResultado_LoteAceiteCod] = T7.[Lote_Codigo]) ) T4 ON T4.[Lote_Codigo] = T1.[Lote_Codigo]) LEFT JOIN (SELECT COUNT(*) AS Lote_QtdeDmn, [ContagemResultado_LoteAceiteCod], MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T5 ON T5.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) LEFT JOIN";
         scmdbuf = scmdbuf + " (SELECT MAX(COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim, T7.[ContagemResultado_LoteAceiteCod], MIN(COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni FROM ([ContagemResultado] T7 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T7.[ContagemResultado_Codigo]) GROUP BY T7.[ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo])";
         scmdbuf = scmdbuf + " WHERE (@AV9WWPCo_2Userehcontratante = 1 or ( COALESCE( T5.[Lote_ContratadaCod], 0) = @AV9WWPCo_1Contratada_codigo))";
         scmdbuf = scmdbuf + " and ((@AV24TFLote_DataIni = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) >= @AV24TFLote_DataIni))";
         scmdbuf = scmdbuf + " and ((@AV25TFLote_DataIni_To = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) <= @AV25TFLote_DataIni_To))";
         scmdbuf = scmdbuf + " and ((@AV26TFLote_DataFim = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV26TFLote_DataFim))";
         scmdbuf = scmdbuf + " and ((@AV27TFLote_DataFim_To = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) <= @AV27TFLote_DataFim_To))";
         scmdbuf = scmdbuf + " and (T1.[Lote_AreaTrabalhoCod] = @AV9WWPCo_3Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Lote_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV54Lote_Nome1)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Lote_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV54Lote_Nome1)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_USERNOM") == 0 ) && ( AV53DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Lote_UserNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV55Lote_UserNom1)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_USERNOM") == 0 ) && ( AV53DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Lote_UserNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV55Lote_UserNom1)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV56DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_NOME") == 0 ) && ( AV58DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Lote_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV59Lote_Nome2)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV56DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_NOME") == 0 ) && ( AV58DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Lote_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV59Lote_Nome2)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV56DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_USERNOM") == 0 ) && ( AV58DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Lote_UserNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV60Lote_UserNom2)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( AV56DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_USERNOM") == 0 ) && ( AV58DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Lote_UserNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV60Lote_UserNom2)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_NOME") == 0 ) && ( AV63DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64Lote_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV64Lote_Nome3)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_NOME") == 0 ) && ( AV63DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64Lote_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV64Lote_Nome3)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_USERNOM") == 0 ) && ( AV63DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65Lote_UserNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV65Lote_UserNom3)";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_USERNOM") == 0 ) && ( AV63DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65Lote_UserNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV65Lote_UserNom3)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFLote_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFLote_Numero)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like @lV10TFLote_Numero)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFLote_Numero_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] = @AV11TFLote_Numero_Sel)";
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFLote_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFLote_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV12TFLote_Nome)";
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFLote_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] = @AV13TFLote_Nome_Sel)";
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFLote_Data) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] >= @AV14TFLote_Data)";
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFLote_Data_To) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] <= @AV15TFLote_Data_To)";
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( ! (0==AV16TFLote_UserCod) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_UserCod] >= @AV16TFLote_UserCod)";
         }
         else
         {
            GXv_int4[29] = 1;
         }
         if ( ! (0==AV17TFLote_UserCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_UserCod] <= @AV17TFLote_UserCod_To)";
         }
         else
         {
            GXv_int4[30] = 1;
         }
         if ( ! (0==AV18TFLote_PessoaCod) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV18TFLote_PessoaCod)";
         }
         else
         {
            GXv_int4[31] = 1;
         }
         if ( ! (0==AV19TFLote_PessoaCod_To) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV19TFLote_PessoaCod_To)";
         }
         else
         {
            GXv_int4[32] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFLote_UserNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFLote_UserNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV20TFLote_UserNom)";
         }
         else
         {
            GXv_int4[33] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFLote_UserNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV21TFLote_UserNom_Sel)";
         }
         else
         {
            GXv_int4[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFLote_ValorPF) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] >= @AV22TFLote_ValorPF)";
         }
         else
         {
            GXv_int4[35] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFLote_ValorPF_To) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] <= @AV23TFLote_ValorPF_To)";
         }
         else
         {
            GXv_int4[36] = 1;
         }
         if ( ! (0==AV28TFLote_QtdeDmn) )
         {
            sWhereString = sWhereString + " and (COALESCE( T5.[Lote_QtdeDmn], 0) >= @AV28TFLote_QtdeDmn)";
         }
         else
         {
            GXv_int4[37] = 1;
         }
         if ( ! (0==AV29TFLote_QtdeDmn_To) )
         {
            sWhereString = sWhereString + " and (COALESCE( T5.[Lote_QtdeDmn], 0) <= @AV29TFLote_QtdeDmn_To)";
         }
         else
         {
            GXv_int4[38] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Nome]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_P00MR22( IGxContext context ,
                                              String AV52DynamicFiltersSelector1 ,
                                              short AV53DynamicFiltersOperator1 ,
                                              String AV54Lote_Nome1 ,
                                              String AV55Lote_UserNom1 ,
                                              bool AV56DynamicFiltersEnabled2 ,
                                              String AV57DynamicFiltersSelector2 ,
                                              short AV58DynamicFiltersOperator2 ,
                                              String AV59Lote_Nome2 ,
                                              String AV60Lote_UserNom2 ,
                                              bool AV61DynamicFiltersEnabled3 ,
                                              String AV62DynamicFiltersSelector3 ,
                                              short AV63DynamicFiltersOperator3 ,
                                              String AV64Lote_Nome3 ,
                                              String AV65Lote_UserNom3 ,
                                              String AV11TFLote_Numero_Sel ,
                                              String AV10TFLote_Numero ,
                                              String AV13TFLote_Nome_Sel ,
                                              String AV12TFLote_Nome ,
                                              DateTime AV14TFLote_Data ,
                                              DateTime AV15TFLote_Data_To ,
                                              int AV16TFLote_UserCod ,
                                              int AV17TFLote_UserCod_To ,
                                              int AV18TFLote_PessoaCod ,
                                              int AV19TFLote_PessoaCod_To ,
                                              String AV21TFLote_UserNom_Sel ,
                                              String AV20TFLote_UserNom ,
                                              decimal AV22TFLote_ValorPF ,
                                              decimal AV23TFLote_ValorPF_To ,
                                              short AV28TFLote_QtdeDmn ,
                                              short AV29TFLote_QtdeDmn_To ,
                                              String A563Lote_Nome ,
                                              String A561Lote_UserNom ,
                                              String A562Lote_Numero ,
                                              DateTime A564Lote_Data ,
                                              int A559Lote_UserCod ,
                                              int A560Lote_PessoaCod ,
                                              decimal A565Lote_ValorPF ,
                                              short A569Lote_QtdeDmn ,
                                              bool AV9WWPContext_gxTpr_Userehcontratante ,
                                              int A1231Lote_ContratadaCod ,
                                              int AV9WWPContext_gxTpr_Contratada_codigo ,
                                              DateTime AV24TFLote_DataIni ,
                                              DateTime A567Lote_DataIni ,
                                              DateTime AV25TFLote_DataIni_To ,
                                              DateTime AV26TFLote_DataFim ,
                                              DateTime A568Lote_DataFim ,
                                              DateTime AV27TFLote_DataFim_To ,
                                              decimal AV30TFLote_Valor ,
                                              decimal A572Lote_Valor ,
                                              decimal AV31TFLote_Valor_To ,
                                              int A595Lote_AreaTrabalhoCod ,
                                              int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [39] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[Lote_AreaTrabalhoCod], T3.[Pessoa_Nome] AS Lote_UserNom, T1.[Lote_ValorPF], T2.[Usuario_PessoaCod] AS Lote_PessoaCod, T1.[Lote_UserCod] AS Lote_UserCod, T1.[Lote_Data], T1.[Lote_Numero], T1.[Lote_Nome], T1.[Lote_Codigo], COALESCE( T5.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn, COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim, COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T5.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod, COALESCE( T4.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas FROM ((((([Lote] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Lote_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN (SELECT COALESCE( T9.[GXC2], 0) + COALESCE( T8.[GXC3], 0) AS Lote_ValorGlosas, T7.[Lote_Codigo] FROM (([Lote] T7 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T10.[ContagemResultadoIndicadores_Valor]) AS GXC3, T11.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T10 WITH (NOLOCK),  [Lote] T11 WITH (NOLOCK) WHERE T10.[ContagemResultadoIndicadores_LoteCod] = T11.[Lote_Codigo] GROUP BY T11.[Lote_Codigo] ) T8 ON T8.[Lote_Codigo] = T7.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC2, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T9 ON T9.[ContagemResultado_LoteAceiteCod] = T7.[Lote_Codigo]) ) T4 ON T4.[Lote_Codigo] = T1.[Lote_Codigo]) LEFT JOIN (SELECT COUNT(*) AS Lote_QtdeDmn, [ContagemResultado_LoteAceiteCod], MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T5 ON T5.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) LEFT JOIN";
         scmdbuf = scmdbuf + " (SELECT MAX(COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim, T7.[ContagemResultado_LoteAceiteCod], MIN(COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni FROM ([ContagemResultado] T7 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T7.[ContagemResultado_Codigo]) GROUP BY T7.[ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo])";
         scmdbuf = scmdbuf + " WHERE (@AV9WWPCo_2Userehcontratante = 1 or ( COALESCE( T5.[Lote_ContratadaCod], 0) = @AV9WWPCo_1Contratada_codigo))";
         scmdbuf = scmdbuf + " and ((@AV24TFLote_DataIni = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) >= @AV24TFLote_DataIni))";
         scmdbuf = scmdbuf + " and ((@AV25TFLote_DataIni_To = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) <= @AV25TFLote_DataIni_To))";
         scmdbuf = scmdbuf + " and ((@AV26TFLote_DataFim = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV26TFLote_DataFim))";
         scmdbuf = scmdbuf + " and ((@AV27TFLote_DataFim_To = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) <= @AV27TFLote_DataFim_To))";
         scmdbuf = scmdbuf + " and (T1.[Lote_AreaTrabalhoCod] = @AV9WWPCo_3Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Lote_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV54Lote_Nome1)";
         }
         else
         {
            GXv_int6[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Lote_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV54Lote_Nome1)";
         }
         else
         {
            GXv_int6[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_USERNOM") == 0 ) && ( AV53DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Lote_UserNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV55Lote_UserNom1)";
         }
         else
         {
            GXv_int6[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "LOTE_USERNOM") == 0 ) && ( AV53DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Lote_UserNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV55Lote_UserNom1)";
         }
         else
         {
            GXv_int6[14] = 1;
         }
         if ( AV56DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_NOME") == 0 ) && ( AV58DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Lote_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV59Lote_Nome2)";
         }
         else
         {
            GXv_int6[15] = 1;
         }
         if ( AV56DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_NOME") == 0 ) && ( AV58DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Lote_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV59Lote_Nome2)";
         }
         else
         {
            GXv_int6[16] = 1;
         }
         if ( AV56DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_USERNOM") == 0 ) && ( AV58DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Lote_UserNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV60Lote_UserNom2)";
         }
         else
         {
            GXv_int6[17] = 1;
         }
         if ( AV56DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector2, "LOTE_USERNOM") == 0 ) && ( AV58DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Lote_UserNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV60Lote_UserNom2)";
         }
         else
         {
            GXv_int6[18] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_NOME") == 0 ) && ( AV63DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64Lote_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV64Lote_Nome3)";
         }
         else
         {
            GXv_int6[19] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_NOME") == 0 ) && ( AV63DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64Lote_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV64Lote_Nome3)";
         }
         else
         {
            GXv_int6[20] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_USERNOM") == 0 ) && ( AV63DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65Lote_UserNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV65Lote_UserNom3)";
         }
         else
         {
            GXv_int6[21] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "LOTE_USERNOM") == 0 ) && ( AV63DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65Lote_UserNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV65Lote_UserNom3)";
         }
         else
         {
            GXv_int6[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFLote_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFLote_Numero)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like @lV10TFLote_Numero)";
         }
         else
         {
            GXv_int6[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFLote_Numero_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] = @AV11TFLote_Numero_Sel)";
         }
         else
         {
            GXv_int6[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFLote_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFLote_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV12TFLote_Nome)";
         }
         else
         {
            GXv_int6[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFLote_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] = @AV13TFLote_Nome_Sel)";
         }
         else
         {
            GXv_int6[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFLote_Data) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] >= @AV14TFLote_Data)";
         }
         else
         {
            GXv_int6[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFLote_Data_To) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] <= @AV15TFLote_Data_To)";
         }
         else
         {
            GXv_int6[28] = 1;
         }
         if ( ! (0==AV16TFLote_UserCod) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_UserCod] >= @AV16TFLote_UserCod)";
         }
         else
         {
            GXv_int6[29] = 1;
         }
         if ( ! (0==AV17TFLote_UserCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_UserCod] <= @AV17TFLote_UserCod_To)";
         }
         else
         {
            GXv_int6[30] = 1;
         }
         if ( ! (0==AV18TFLote_PessoaCod) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV18TFLote_PessoaCod)";
         }
         else
         {
            GXv_int6[31] = 1;
         }
         if ( ! (0==AV19TFLote_PessoaCod_To) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV19TFLote_PessoaCod_To)";
         }
         else
         {
            GXv_int6[32] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFLote_UserNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFLote_UserNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV20TFLote_UserNom)";
         }
         else
         {
            GXv_int6[33] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFLote_UserNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV21TFLote_UserNom_Sel)";
         }
         else
         {
            GXv_int6[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFLote_ValorPF) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] >= @AV22TFLote_ValorPF)";
         }
         else
         {
            GXv_int6[35] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFLote_ValorPF_To) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] <= @AV23TFLote_ValorPF_To)";
         }
         else
         {
            GXv_int6[36] = 1;
         }
         if ( ! (0==AV28TFLote_QtdeDmn) )
         {
            sWhereString = sWhereString + " and (COALESCE( T5.[Lote_QtdeDmn], 0) >= @AV28TFLote_QtdeDmn)";
         }
         else
         {
            GXv_int6[37] = 1;
         }
         if ( ! (0==AV29TFLote_QtdeDmn_To) )
         {
            sWhereString = sWhereString + " and (COALESCE( T5.[Lote_QtdeDmn], 0) <= @AV29TFLote_QtdeDmn_To)";
         }
         else
         {
            GXv_int6[38] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00MR8(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (DateTime)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (decimal)dynConstraints[36] , (short)dynConstraints[37] , (bool)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (DateTime)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] );
               case 1 :
                     return conditional_P00MR15(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (DateTime)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (decimal)dynConstraints[36] , (short)dynConstraints[37] , (bool)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (DateTime)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] );
               case 2 :
                     return conditional_P00MR22(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (DateTime)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (decimal)dynConstraints[36] , (short)dynConstraints[37] , (bool)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (DateTime)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00MR23 ;
          prmP00MR23 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00MR8 ;
          prmP00MR8 = new Object[] {
          new Object[] {"@AV9WWPCo_2Userehcontratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV9WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24TFLote_DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24TFLote_DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFLote_DataIni_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFLote_DataIni_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV26TFLote_DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV26TFLote_DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV27TFLote_DataFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV27TFLote_DataFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV9WWPCo_3Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV54Lote_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54Lote_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55Lote_UserNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV55Lote_UserNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV59Lote_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59Lote_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60Lote_UserNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60Lote_UserNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV64Lote_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64Lote_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65Lote_UserNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV65Lote_UserNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV10TFLote_Numero",SqlDbType.Char,10,0} ,
          new Object[] {"@AV11TFLote_Numero_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV12TFLote_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFLote_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV14TFLote_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV15TFLote_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV16TFLote_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFLote_UserCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFLote_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFLote_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFLote_UserNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFLote_UserNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV22TFLote_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV23TFLote_ValorPF_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV28TFLote_QtdeDmn",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV29TFLote_QtdeDmn_To",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00MR15 ;
          prmP00MR15 = new Object[] {
          new Object[] {"@AV9WWPCo_2Userehcontratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV9WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24TFLote_DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24TFLote_DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFLote_DataIni_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFLote_DataIni_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV26TFLote_DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV26TFLote_DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV27TFLote_DataFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV27TFLote_DataFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV9WWPCo_3Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV54Lote_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54Lote_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55Lote_UserNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV55Lote_UserNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV59Lote_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59Lote_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60Lote_UserNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60Lote_UserNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV64Lote_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64Lote_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65Lote_UserNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV65Lote_UserNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV10TFLote_Numero",SqlDbType.Char,10,0} ,
          new Object[] {"@AV11TFLote_Numero_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV12TFLote_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFLote_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV14TFLote_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV15TFLote_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV16TFLote_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFLote_UserCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFLote_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFLote_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFLote_UserNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFLote_UserNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV22TFLote_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV23TFLote_ValorPF_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV28TFLote_QtdeDmn",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV29TFLote_QtdeDmn_To",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00MR22 ;
          prmP00MR22 = new Object[] {
          new Object[] {"@AV9WWPCo_2Userehcontratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV9WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24TFLote_DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24TFLote_DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFLote_DataIni_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFLote_DataIni_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV26TFLote_DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV26TFLote_DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV27TFLote_DataFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV27TFLote_DataFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV9WWPCo_3Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV54Lote_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54Lote_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55Lote_UserNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV55Lote_UserNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV59Lote_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59Lote_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60Lote_UserNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60Lote_UserNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV64Lote_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64Lote_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65Lote_UserNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV65Lote_UserNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV10TFLote_Numero",SqlDbType.Char,10,0} ,
          new Object[] {"@AV11TFLote_Numero_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV12TFLote_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFLote_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV14TFLote_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV15TFLote_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV16TFLote_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFLote_UserCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFLote_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFLote_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFLote_UserNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFLote_UserNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV22TFLote_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV23TFLote_ValorPF_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV28TFLote_QtdeDmn",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV29TFLote_QtdeDmn_To",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00MR8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00MR8,100,0,true,false )
             ,new CursorDef("P00MR15", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00MR15,100,0,true,false )
             ,new CursorDef("P00MR22", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00MR22,100,0,true,false )
             ,new CursorDef("P00MR23", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00MR23,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((short[]) buf[11])[0] = rslt.getShort(10) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(11) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(12) ;
                ((int[]) buf[14])[0] = rslt.getInt(13) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(14);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 10) ;
                ((String[]) buf[8])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((short[]) buf[11])[0] = rslt.getShort(10) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(11) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(12) ;
                ((int[]) buf[14])[0] = rslt.getInt(13) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(14);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(6) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 50) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((short[]) buf[11])[0] = rslt.getShort(10) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(11) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(12) ;
                ((int[]) buf[14])[0] = rslt.getInt(13) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(14);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[39]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[68]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[69]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[74]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[75]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[76]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[77]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[39]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[68]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[69]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[74]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[75]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[76]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[77]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[39]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[68]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[69]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[74]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[75]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[76]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[77]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptlotefilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptlotefilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptlotefilterdata") )
          {
             return  ;
          }
          getpromptlotefilterdata worker = new getpromptlotefilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
