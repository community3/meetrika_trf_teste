/*
               File: REL_TAV1
        Description: Termo de Aceite V1
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:14:38.35
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_tav1 : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               A596Lote_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_tav1( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_tav1( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Lote_Codigo )
      {
         this.A596Lote_Codigo = aP0_Lote_Codigo;
         initialize();
         executePrivate();
         aP0_Lote_Codigo=this.A596Lote_Codigo;
      }

      public int executeUdp( )
      {
         this.A596Lote_Codigo = aP0_Lote_Codigo;
         initialize();
         executePrivate();
         aP0_Lote_Codigo=this.A596Lote_Codigo;
         return A596Lote_Codigo ;
      }

      public void executeSubmit( ref int aP0_Lote_Codigo )
      {
         arel_tav1 objarel_tav1;
         objarel_tav1 = new arel_tav1();
         objarel_tav1.A596Lote_Codigo = aP0_Lote_Codigo;
         objarel_tav1.context.SetSubmitInitialConfig(context);
         objarel_tav1.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_tav1);
         aP0_Lote_Codigo=this.A596Lote_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_tav1)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            /* Using cursor P00XN7 */
            pr_default.execute(0, new Object[] {A596Lote_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A595Lote_AreaTrabalhoCod = P00XN7_A595Lote_AreaTrabalhoCod[0];
               A559Lote_UserCod = P00XN7_A559Lote_UserCod[0];
               A560Lote_PessoaCod = P00XN7_A560Lote_PessoaCod[0];
               n560Lote_PessoaCod = P00XN7_n560Lote_PessoaCod[0];
               A29Contratante_Codigo = P00XN7_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00XN7_n29Contratante_Codigo[0];
               A1125Contratante_LogoNomeArq = P00XN7_A1125Contratante_LogoNomeArq[0];
               n1125Contratante_LogoNomeArq = P00XN7_n1125Contratante_LogoNomeArq[0];
               A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
               A1126Contratante_LogoTipoArq = P00XN7_A1126Contratante_LogoTipoArq[0];
               n1126Contratante_LogoTipoArq = P00XN7_n1126Contratante_LogoTipoArq[0];
               A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
               A562Lote_Numero = P00XN7_A562Lote_Numero[0];
               A563Lote_Nome = P00XN7_A563Lote_Nome[0];
               A564Lote_Data = P00XN7_A564Lote_Data[0];
               A2088Lote_ParecerFinal = P00XN7_A2088Lote_ParecerFinal[0];
               n2088Lote_ParecerFinal = P00XN7_n2088Lote_ParecerFinal[0];
               A2087Lote_Comentarios = P00XN7_A2087Lote_Comentarios[0];
               n2087Lote_Comentarios = P00XN7_n2087Lote_Comentarios[0];
               A561Lote_UserNom = P00XN7_A561Lote_UserNom[0];
               n561Lote_UserNom = P00XN7_n561Lote_UserNom[0];
               A568Lote_DataFim = P00XN7_A568Lote_DataFim[0];
               A567Lote_DataIni = P00XN7_A567Lote_DataIni[0];
               A1057Lote_ValorGlosas = P00XN7_A1057Lote_ValorGlosas[0];
               n1057Lote_ValorGlosas = P00XN7_n1057Lote_ValorGlosas[0];
               A1124Contratante_LogoArquivo = P00XN7_A1124Contratante_LogoArquivo[0];
               n1124Contratante_LogoArquivo = P00XN7_n1124Contratante_LogoArquivo[0];
               A29Contratante_Codigo = P00XN7_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00XN7_n29Contratante_Codigo[0];
               A1125Contratante_LogoNomeArq = P00XN7_A1125Contratante_LogoNomeArq[0];
               n1125Contratante_LogoNomeArq = P00XN7_n1125Contratante_LogoNomeArq[0];
               A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
               A1126Contratante_LogoTipoArq = P00XN7_A1126Contratante_LogoTipoArq[0];
               n1126Contratante_LogoTipoArq = P00XN7_n1126Contratante_LogoTipoArq[0];
               A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
               A1124Contratante_LogoArquivo = P00XN7_A1124Contratante_LogoArquivo[0];
               n1124Contratante_LogoArquivo = P00XN7_n1124Contratante_LogoArquivo[0];
               A560Lote_PessoaCod = P00XN7_A560Lote_PessoaCod[0];
               n560Lote_PessoaCod = P00XN7_n560Lote_PessoaCod[0];
               A561Lote_UserNom = P00XN7_A561Lote_UserNom[0];
               n561Lote_UserNom = P00XN7_n561Lote_UserNom[0];
               A568Lote_DataFim = P00XN7_A568Lote_DataFim[0];
               A567Lote_DataIni = P00XN7_A567Lote_DataIni[0];
               A1057Lote_ValorGlosas = P00XN7_A1057Lote_ValorGlosas[0];
               n1057Lote_ValorGlosas = P00XN7_n1057Lote_ValorGlosas[0];
               GetLote_ValorOSs( A596Lote_Codigo) ;
               A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
               AV15Lote = A562Lote_Numero;
               AV16Lote_Nome = A563Lote_Nome;
               AV9Contratante_Logo = A1124Contratante_LogoArquivo;
               A40000Contratante_Logo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( A1124Contratante_LogoArquivo);
               AV17MesAno = StringUtil.Trim( DateTimeUtil.CMonth( A564Lote_Data, "por")) + "/" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( A564Lote_Data)), 10, 0));
               AV11Date = DateTimeUtil.ResetTime(A564Lote_Data);
               AV18Parecer = A2088Lote_ParecerFinal;
               AV8Comentarios = A2087Lote_Comentarios;
               AV22Responsavel = A561Lote_UserNom;
               AV19Periodo = context.localUtil.DToC( A567Lote_DataIni, 2, "/") + " - " + context.localUtil.DToC( A568Lote_DataFim, 2, "/");
               AV26Valor = A572Lote_Valor;
               /* Using cursor P00XN8 */
               pr_default.execute(1, new Object[] {A596Lote_Codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A490ContagemResultado_ContratadaCod = P00XN8_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00XN8_n490ContagemResultado_ContratadaCod[0];
                  A499ContagemResultado_ContratadaPessoaCod = P00XN8_A499ContagemResultado_ContratadaPessoaCod[0];
                  n499ContagemResultado_ContratadaPessoaCod = P00XN8_n499ContagemResultado_ContratadaPessoaCod[0];
                  A1553ContagemResultado_CntSrvCod = P00XN8_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00XN8_n1553ContagemResultado_CntSrvCod[0];
                  A1603ContagemResultado_CntCod = P00XN8_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P00XN8_n1603ContagemResultado_CntCod[0];
                  A1604ContagemResultado_CntPrpCod = P00XN8_A1604ContagemResultado_CntPrpCod[0];
                  n1604ContagemResultado_CntPrpCod = P00XN8_n1604ContagemResultado_CntPrpCod[0];
                  A1605ContagemResultado_CntPrpPesCod = P00XN8_A1605ContagemResultado_CntPrpPesCod[0];
                  n1605ContagemResultado_CntPrpPesCod = P00XN8_n1605ContagemResultado_CntPrpPesCod[0];
                  A597ContagemResultado_LoteAceiteCod = P00XN8_A597ContagemResultado_LoteAceiteCod[0];
                  n597ContagemResultado_LoteAceiteCod = P00XN8_n597ContagemResultado_LoteAceiteCod[0];
                  A500ContagemResultado_ContratadaPessoaNom = P00XN8_A500ContagemResultado_ContratadaPessoaNom[0];
                  n500ContagemResultado_ContratadaPessoaNom = P00XN8_n500ContagemResultado_ContratadaPessoaNom[0];
                  A1606ContagemResultado_CntPrpPesNom = P00XN8_A1606ContagemResultado_CntPrpPesNom[0];
                  n1606ContagemResultado_CntPrpPesNom = P00XN8_n1606ContagemResultado_CntPrpPesNom[0];
                  A1612ContagemResultado_CntNum = P00XN8_A1612ContagemResultado_CntNum[0];
                  n1612ContagemResultado_CntNum = P00XN8_n1612ContagemResultado_CntNum[0];
                  A78Contrato_NumeroAta = P00XN8_A78Contrato_NumeroAta[0];
                  n78Contrato_NumeroAta = P00XN8_n78Contrato_NumeroAta[0];
                  A456ContagemResultado_Codigo = P00XN8_A456ContagemResultado_Codigo[0];
                  A499ContagemResultado_ContratadaPessoaCod = P00XN8_A499ContagemResultado_ContratadaPessoaCod[0];
                  n499ContagemResultado_ContratadaPessoaCod = P00XN8_n499ContagemResultado_ContratadaPessoaCod[0];
                  A500ContagemResultado_ContratadaPessoaNom = P00XN8_A500ContagemResultado_ContratadaPessoaNom[0];
                  n500ContagemResultado_ContratadaPessoaNom = P00XN8_n500ContagemResultado_ContratadaPessoaNom[0];
                  A1603ContagemResultado_CntCod = P00XN8_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P00XN8_n1603ContagemResultado_CntCod[0];
                  A1604ContagemResultado_CntPrpCod = P00XN8_A1604ContagemResultado_CntPrpCod[0];
                  n1604ContagemResultado_CntPrpCod = P00XN8_n1604ContagemResultado_CntPrpCod[0];
                  A1612ContagemResultado_CntNum = P00XN8_A1612ContagemResultado_CntNum[0];
                  n1612ContagemResultado_CntNum = P00XN8_n1612ContagemResultado_CntNum[0];
                  A78Contrato_NumeroAta = P00XN8_A78Contrato_NumeroAta[0];
                  n78Contrato_NumeroAta = P00XN8_n78Contrato_NumeroAta[0];
                  A1605ContagemResultado_CntPrpPesCod = P00XN8_A1605ContagemResultado_CntPrpPesCod[0];
                  n1605ContagemResultado_CntPrpPesCod = P00XN8_n1605ContagemResultado_CntPrpPesCod[0];
                  A1606ContagemResultado_CntPrpPesNom = P00XN8_A1606ContagemResultado_CntPrpPesNom[0];
                  n1606ContagemResultado_CntPrpPesNom = P00XN8_n1606ContagemResultado_CntPrpPesNom[0];
                  AV21RazaoSocial = A500ContagemResultado_ContratadaPessoaNom;
                  AV13Gerente = A1606ContagemResultado_CntPrpPesNom;
                  AV10Contrato = A1612ContagemResultado_CntNum;
                  AV20Processo = A78Contrato_NumeroAta;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            HXN0( false, 169) ;
            getPrinter().GxAttris("Arial", 12, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Vers�o 1.0", 375, Gx_line+117, 458, Gx_line+137, 1+256, 0, 0, 0) ;
            getPrinter().GxAttris("Arial", 14, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Termo de Aceite", 333, Gx_line+83, 489, Gx_line+106, 1+256, 0, 0, 0) ;
            getPrinter().GxAttris("Arial", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("QUALIDADE DE SOFTWARE", 267, Gx_line+50, 563, Gx_line+77, 1+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+169);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            HXN0( false, 67) ;
            getPrinter().GxDrawLine(100, Gx_line+33, 100, Gx_line+66, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(583, Gx_line+33, 583, Gx_line+66, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(158, Gx_line+33, 158, Gx_line+66, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(17, Gx_line+33, 784, Gx_line+66, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Data", 42, Gx_line+42, 73, Gx_line+59, 1+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Vers�o", 108, Gx_line+42, 154, Gx_line+59, 1+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Descri��o", 333, Gx_line+42, 399, Gx_line+59, 1+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Autor", 658, Gx_line+42, 694, Gx_line+59, 1+256, 0, 0, 0) ;
            getPrinter().GxAttris("Arial", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Hist�rico da Revis�o", 300, Gx_line+0, 518, Gx_line+27, 1+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+67);
            HXN0( false, 17) ;
            getPrinter().GxDrawRect(17, Gx_line+0, 784, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(158, Gx_line+0, 158, Gx_line+17, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(583, Gx_line+0, 583, Gx_line+17, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(100, Gx_line+0, 100, Gx_line+17, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV11Date, "99/99/99"), 33, Gx_line+0, 82, Gx_line+15, 1+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV16Lote_Nome, "@!")), 167, Gx_line+0, 428, Gx_line+15, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV22Responsavel, "@!")), 592, Gx_line+0, 784, Gx_line+15, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+17);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            HXN0( false, 916) ;
            getPrinter().GxAttris("Arial", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("SUM�RIO", 367, Gx_line+17, 468, Gx_line+44, 1+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("1. MANUAL DE INSTRU��ES DE EENCHIMENTO ______________________________ 4", 108, Gx_line+67, 708, Gx_line+100, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("1.1. IDENTIFICA��O ____________________________________________________ 4", 125, Gx_line+100, 708, Gx_line+133, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("1.2. TERMO DE ENTREGA � LISTA DE PRODUTOS ENTREGUES _______________ 4", 125, Gx_line+133, 708, Gx_line+166, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("4. TERMO DE HOMOLOGA��O � SISTEMAS INSTITUCIONAIS ___________________ 9", 108, Gx_line+350, 708, Gx_line+383, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("4.1. LISTA DE PRODUTOS EM HOMOLOGA��O _____________________________ 9", 125, Gx_line+383, 708, Gx_line+416, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("3.1. LISTA DE PRODUTOS ENTREGUES ____________________________________ 7", 125, Gx_line+300, 708, Gx_line+333, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("2. IDENTIFICA��O ________________________________________________________ 6", 108, Gx_line+217, 708, Gx_line+250, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("3. TERMO DE ENTREGA ___________________________________________________ 7", 108, Gx_line+267, 708, Gx_line+300, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("1.3. MACRO FLUXO DO PROCESSO _______________________________________ 5", 125, Gx_line+167, 708, Gx_line+200, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("5. TERMO DE HOMOLOGA��O � SISTEMAS TERCEIRIZADOS ___________________10", 108, Gx_line+433, 708, Gx_line+466, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("5.1. LISTA DE PRODUTOS EM HOMOLOGA��O _____________________________10", 125, Gx_line+467, 708, Gx_line+500, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("6. TERMO DE HOMOLOGA��O � PROJETO ___________________________________12", 108, Gx_line+517, 708, Gx_line+550, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("6.1. LISTA DE PRODUTOS EM HOMOLOGA��O _____________________________ 12", 125, Gx_line+550, 708, Gx_line+583, 0+16, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+916);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            HXN0( false, 1169) ;
            getPrinter().GxAttris("Arial", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("1.1. Identifica��o", 75, Gx_line+67, 733, Gx_line+105, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Dados do Projeto e Dados da Empresa. Essa parte dever� se preenchida pela empresa, informando:", 75, Gx_line+105, 733, Gx_line+143, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("   * Data da Emiss�o;", 75, Gx_line+143, 733, Gx_line+181, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("   * O nome do Gerente Respons�vel;", 75, Gx_line+181, 733, Gx_line+219, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("   * O n�mero do Termo de homologa��o que dever� ter uma sequencia l�gica e ser� sempre relativo ao per�odo;", 75, Gx_line+219, 733, Gx_line+257, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("   * Per�odo: dever� constar o per�odo de refer�ncia do contrato. Por exemplo: de 15/05/2013 a 14/06/2013;", 75, Gx_line+257, 733, Gx_line+295, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("   * Informar o valor a ser faturado;", 75, Gx_line+295, 733, Gx_line+333, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("   * Informar o CNPJ da empresa;", 75, Gx_line+333, 733, Gx_line+371, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("   * O n�mero do termo de homologa��o tem o formato de xxx/aa onde xxx � a ordem", 75, Gx_line+371, 733, Gx_line+409, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("num�rica de emiss�o e aa � o ano de emiss�o. Por exemplo, 001/13 � Primeiro termo", 75, Gx_line+409, 733, Gx_line+447, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("emitido em 2013;", 75, Gx_line+447, 733, Gx_line+485, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("1.2. Termo de Entrega � Lista de Produtos Entregues.", 75, Gx_line+485, 733, Gx_line+523, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Descrever na tabela quais foram os artefatos entregues no per�odo apontando a que item se refere no contrato (item e n�mero de p�gina).", 75, Gx_line+523, 733, Gx_line+561, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("A forma em que o produto foi entregue:", 75, Gx_line+561, 733, Gx_line+599, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("   * Papel;", 75, Gx_line+599, 733, Gx_line+637, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("   * Eletronicamente.", 75, Gx_line+637, 733, Gx_line+675, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Preenchido esses campos, o documento dever� ser encaminhado para solicitante ", 75, Gx_line+675, 733, Gx_line+713, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("juntamente. Ap�s a �rea solicitante analisar os termos contratuais juntamente com esse ", 75, Gx_line+713, 733, Gx_line+751, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("documento, o mesmo ser� encaminhado para �rea de contratos juntamente com o ", 75, Gx_line+751, 733, Gx_line+789, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("documento de ateste. A �rea t�cnica (solicitante) ser� respons�vel pela an�lise dos ", 75, Gx_line+789, 733, Gx_line+827, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("artefatos entregues e preencher� as tabelas referentes � Lista de Produtos entregues. ", 75, Gx_line+827, 733, Gx_line+865, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Se estiver de acordo os documentos ser�o devolvidos para �rea de contratos que ", 75, Gx_line+865, 733, Gx_line+903, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("encaminhar� para Secretaria de Or�amentos e Finan�as para libera��o do pagamento. ", 75, Gx_line+903, 733, Gx_line+941, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Caso n�o seja cumprido os termos contratuais e/ou algum artefato n�o estiver de ", 75, Gx_line+941, 733, Gx_line+979, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("acordo, a empresa ser� comunicada para proceder � regulariza��o.", 75, Gx_line+979, 733, Gx_line+1017, 0+16, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 12, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("1. MANUAL DE INSTRU��ES DE PREENCHIMENTO", 75, Gx_line+33, 524, Gx_line+54, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+1169);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            HXN0( false, 686) ;
            getPrinter().GxDrawBitMap(context.GetImagePath( "1200046a-09bd-4b18-aef3-8d117553c84f", "", context.GetTheme( )), 75, Gx_line+83, 733, Gx_line+440) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("1.3. Macro Fluxo do Processo", 75, Gx_line+33, 293, Gx_line+54, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+686);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            HXN0( false, 539) ;
            getPrinter().GxDrawRect(67, Gx_line+50, 767, Gx_line+83, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(67, Gx_line+83, 767, Gx_line+116, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(67, Gx_line+117, 767, Gx_line+150, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(608, Gx_line+83, 608, Gx_line+116, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(67, Gx_line+250, 767, Gx_line+283, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(67, Gx_line+217, 767, Gx_line+250, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(67, Gx_line+183, 767, Gx_line+216, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(67, Gx_line+450, 767, Gx_line+483, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(67, Gx_line+283, 767, Gx_line+316, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(67, Gx_line+317, 767, Gx_line+350, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(67, Gx_line+350, 767, Gx_line+383, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(67, Gx_line+417, 767, Gx_line+450, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Dados do Projeto", 75, Gx_line+190, 203, Gx_line+208, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Data de Emiss�o", 625, Gx_line+57, 750, Gx_line+75, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Dados da Empresa", 75, Gx_line+190, 216, Gx_line+208, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor Mensal Correspondente aos Servi�os Executados", 75, Gx_line+424, 478, Gx_line+442, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("F�BRICA DE M�TRICAS", 75, Gx_line+89, 269, Gx_line+110, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("TERMO DE ACEITE", 75, Gx_line+123, 228, Gx_line+144, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV15Lote, "")), 242, Gx_line+123, 316, Gx_line+145, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV11Date, "99/99/99"), 654, Gx_line+89, 720, Gx_line+111, 1+256, 0, 0, 1) ;
            getPrinter().GxDrawText("Raz�o Social:", 67, Gx_line+223, 170, Gx_line+244, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Gerente Respons�vel:", 75, Gx_line+256, 242, Gx_line+277, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Contrato:", 75, Gx_line+289, 145, Gx_line+310, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Per�odo:", 75, Gx_line+323, 136, Gx_line+344, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Processo:", 75, Gx_line+356, 149, Gx_line+377, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor R$", 75, Gx_line+456, 141, Gx_line+477, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV21RazaoSocial, "@!")), 183, Gx_line+223, 549, Gx_line+245, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV13Gerente, "@!")), 258, Gx_line+256, 624, Gx_line+278, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV10Contrato, "")), 158, Gx_line+289, 305, Gx_line+311, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV19Periodo, "")), 150, Gx_line+323, 297, Gx_line+345, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV20Processo, "")), 167, Gx_line+356, 241, Gx_line+378, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV26Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")), 158, Gx_line+456, 328, Gx_line+478, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 12, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("2. IDENTIFICA��O", 75, Gx_line+17, 240, Gx_line+38, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+539);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            HXN0( false, 122) ;
            getPrinter().GxDrawRect(67, Gx_line+83, 767, Gx_line+122, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Descri��o", 118, Gx_line+93, 242, Gx_line+111, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Item", 75, Gx_line+93, 103, Gx_line+111, 0+256+16, 0, 0, 0) ;
            getPrinter().GxDrawText("OS", 271, Gx_line+93, 309, Gx_line+111, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Data da solicita��o", 312, Gx_line+85, 379, Gx_line+120, 1+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Data de envio", 382, Gx_line+85, 440, Gx_line+120, 1+16, 0, 0, 0) ;
            getPrinter().GxDrawText("PF Total", 465, Gx_line+83, 510, Gx_line+118, 1+256+16, 0, 0, 0) ;
            getPrinter().GxDrawText("PF Encaminhados", 517, Gx_line+85, 609, Gx_line+120, 1+16, 0, 0, 0) ;
            getPrinter().GxDrawText("PF Validado", 608, Gx_line+83, 670, Gx_line+118, 1+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Tipo de Contagem", 683, Gx_line+83, 758, Gx_line+118, 1+16, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("3.1. Lista de Produtos Entregues", 75, Gx_line+50, 308, Gx_line+68, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 12, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("3. TERMO DE ENTREGA", 75, Gx_line+17, 286, Gx_line+38, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+122);
            AV14Item = 1;
            /* Using cursor P00XN10 */
            pr_default.execute(2, new Object[] {A596Lote_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P00XN10_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00XN10_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = P00XN10_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00XN10_n601ContagemResultado_Servico[0];
               A597ContagemResultado_LoteAceiteCod = P00XN10_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P00XN10_n597ContagemResultado_LoteAceiteCod[0];
               A1349ContagemResultado_DataExecucao = P00XN10_A1349ContagemResultado_DataExecucao[0];
               n1349ContagemResultado_DataExecucao = P00XN10_n1349ContagemResultado_DataExecucao[0];
               A493ContagemResultado_DemandaFM = P00XN10_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P00XN10_n493ContagemResultado_DemandaFM[0];
               A494ContagemResultado_Descricao = P00XN10_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P00XN10_n494ContagemResultado_Descricao[0];
               A801ContagemResultado_ServicoSigla = P00XN10_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P00XN10_n801ContagemResultado_ServicoSigla[0];
               A471ContagemResultado_DataDmn = P00XN10_A471ContagemResultado_DataDmn[0];
               A682ContagemResultado_PFBFMUltima = P00XN10_A682ContagemResultado_PFBFMUltima[0];
               A684ContagemResultado_PFBFSUltima = P00XN10_A684ContagemResultado_PFBFSUltima[0];
               A456ContagemResultado_Codigo = P00XN10_A456ContagemResultado_Codigo[0];
               A601ContagemResultado_Servico = P00XN10_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00XN10_n601ContagemResultado_Servico[0];
               A801ContagemResultado_ServicoSigla = P00XN10_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P00XN10_n801ContagemResultado_ServicoSigla[0];
               A682ContagemResultado_PFBFMUltima = P00XN10_A682ContagemResultado_PFBFMUltima[0];
               A684ContagemResultado_PFBFSUltima = P00XN10_A684ContagemResultado_PFBFSUltima[0];
               GXt_decimal1 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
               A574ContagemResultado_PFFinal = GXt_decimal1;
               AV12Envio = DateTimeUtil.ResetTime(A1349ContagemResultado_DataExecucao);
               HXN0( false, 33) ;
               getPrinter().GxDrawRect(67, Gx_line+0, 767, Gx_line+33, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(108, Gx_line+0, 108, Gx_line+33, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(250, Gx_line+0, 250, Gx_line+33, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(317, Gx_line+0, 317, Gx_line+33, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(383, Gx_line+0, 383, Gx_line+33, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(450, Gx_line+0, 450, Gx_line+33, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(525, Gx_line+0, 525, Gx_line+33, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(600, Gx_line+0, 600, Gx_line+33, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(675, Gx_line+0, 675, Gx_line+33, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14Item), "ZZZ9")), 67, Gx_line+9, 93, Gx_line+24, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), 326, Gx_line+9, 375, Gx_line+24, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A574ContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")), 454, Gx_line+9, 521, Gx_line+24, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A684ContagemResultado_PFBFSUltima, "ZZ,ZZZ,ZZ9.999")), 529, Gx_line+9, 596, Gx_line+24, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A682ContagemResultado_PFBFMUltima, "ZZ,ZZZ,ZZ9.999")), 604, Gx_line+9, 671, Gx_line+24, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")), 679, Gx_line+0, 763, Gx_line+33, 0+16, 0, 0, 1) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, "")), 110, Gx_line+0, 250, Gx_line+33, 0+16, 0, 0, 1) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, "")), 253, Gx_line+0, 315, Gx_line+33, 0+16, 0, 0, 1) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV12Envio, "99/99/99"), 393, Gx_line+9, 442, Gx_line+24, 1+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+33);
               AV23TotalPF = (decimal)(AV23TotalPF+A574ContagemResultado_PFFinal);
               AV25TotalPFFS = (decimal)(AV25TotalPFFS+A684ContagemResultado_PFBFSUltima);
               AV24TotalPFFM = (decimal)(AV24TotalPFFM+A682ContagemResultado_PFBFMUltima);
               AV14Item = (short)(AV14Item+1);
               pr_default.readNext(2);
            }
            pr_default.close(2);
            HXN0( false, 33) ;
            getPrinter().GxDrawLine(675, Gx_line+0, 675, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(600, Gx_line+0, 600, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(525, Gx_line+0, 525, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(450, Gx_line+0, 450, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(67, Gx_line+0, 767, Gx_line+33, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("N�MERO TOTAL EM PONTOS DE FUN��O", 74, Gx_line+8, 424, Gx_line+25, 0+16, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV23TotalPF, "ZZ,ZZZ,ZZ9.999")), 454, Gx_line+8, 521, Gx_line+23, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV24TotalPFFM, "ZZ,ZZZ,ZZ9.999")), 604, Gx_line+8, 671, Gx_line+23, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV25TotalPFFS, "ZZ,ZZZ,ZZ9.999")), 529, Gx_line+8, 596, Gx_line+23, 2, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+33);
            HXN0( false, 389) ;
            getPrinter().GxDrawRect(67, Gx_line+100, 767, Gx_line+333, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(67, Gx_line+83, 767, Gx_line+104, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(AV8Comentarios, 67, Gx_line+117, 750, Gx_line+317, 3+16, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Tipo de Relat�rio", 75, Gx_line+50, 203, Gx_line+68, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 12, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("RELAT�RIOS GERENCIAIS ENTREGUE", 75, Gx_line+17, 423, Gx_line+38, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+389);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            HXN0( false, 550) ;
            getPrinter().GxDrawRect(58, Gx_line+433, 758, Gx_line+475, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(58, Gx_line+17, 758, Gx_line+38, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(58, Gx_line+38, 758, Gx_line+400, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(58, Gx_line+474, 758, Gx_line+533, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(283, Gx_line+475, 283, Gx_line+534, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(283, Gx_line+475, 283, Gx_line+535, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(450, Gx_line+475, 450, Gx_line+535, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(450, Gx_line+475, 450, Gx_line+535, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(567, Gx_line+475, 567, Gx_line+535, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(567, Gx_line+475, 567, Gx_line+535, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV22Responsavel, "@!")), 67, Gx_line+479, 284, Gx_line+528, 0, 0, 0, 1) ;
            getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Supervisor de Servi�os", 292, Gx_line+478, 438, Gx_line+528, 0+16, 0, 0, 1) ;
            getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PARECER FINAL - HOMOLOGA��O E ACEITE", 275, Gx_line+17, 550, Gx_line+36, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Respons�vel pela Homologa��o", 67, Gx_line+433, 271, Gx_line+471, 0+256+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Papel", 292, Gx_line+433, 329, Gx_line+452, 0+256+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Data do Recebimento", 467, Gx_line+433, 559, Gx_line+466, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Assinatura", 625, Gx_line+433, 717, Gx_line+452, 0+16, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(AV18Parecer, 67, Gx_line+50, 750, Gx_line+383, 3+16, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+550);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HXN0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void HXN0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Arial", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Termo de Aceite - F�brica de M�tricas - v1.0", 300, Gx_line+0, 523, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 758, Gx_line+0, 797, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV17MesAno, "")), 25, Gx_line+0, 130, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+32);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Calibri", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("PODER JUDICIARIO", 367, Gx_line+17, 470, Gx_line+32, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText("TRIBUUNAL DE JUSTI�A DE S�O PAULO", 308, Gx_line+33, 515, Gx_line+48, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawBitMap(AV9Contratante_Logo, 25, Gx_line+0, 195, Gx_line+85) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+86);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
         add_metrics3( ) ;
         add_metrics4( ) ;
         add_metrics5( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Arial", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Arial", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics3( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics4( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics5( )
      {
         getPrinter().setMetrics("Calibri", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      protected void GetLote_ValorOSs( int A596Lote_Codigo )
      {
         /* Create private dbobjects */
         /* Navigation */
         A1058Lote_ValorOSs = 0;
         /* Using cursor P00XN11 */
         pr_default.execute(3, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(3) != 101) && ( P00XN11_A597ContagemResultado_LoteAceiteCod[0] == A596Lote_Codigo ) )
         {
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  P00XN11_A456ContagemResultado_Codigo[0], out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*P00XN11_A512ContagemResultado_ValorPF[0]);
            A1058Lote_ValorOSs = (decimal)(A1058Lote_ValorOSs+A606ContagemResultado_ValorFinal);
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         scmdbuf = "";
         P00XN7_A595Lote_AreaTrabalhoCod = new int[1] ;
         P00XN7_A559Lote_UserCod = new int[1] ;
         P00XN7_A560Lote_PessoaCod = new int[1] ;
         P00XN7_n560Lote_PessoaCod = new bool[] {false} ;
         P00XN7_A29Contratante_Codigo = new int[1] ;
         P00XN7_n29Contratante_Codigo = new bool[] {false} ;
         P00XN7_A1125Contratante_LogoNomeArq = new String[] {""} ;
         P00XN7_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         P00XN7_A1126Contratante_LogoTipoArq = new String[] {""} ;
         P00XN7_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         P00XN7_A562Lote_Numero = new String[] {""} ;
         P00XN7_A563Lote_Nome = new String[] {""} ;
         P00XN7_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         P00XN7_A2088Lote_ParecerFinal = new String[] {""} ;
         P00XN7_n2088Lote_ParecerFinal = new bool[] {false} ;
         P00XN7_A2087Lote_Comentarios = new String[] {""} ;
         P00XN7_n2087Lote_Comentarios = new bool[] {false} ;
         P00XN7_A561Lote_UserNom = new String[] {""} ;
         P00XN7_n561Lote_UserNom = new bool[] {false} ;
         P00XN7_A596Lote_Codigo = new int[1] ;
         P00XN7_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         P00XN7_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         P00XN7_A1057Lote_ValorGlosas = new decimal[1] ;
         P00XN7_n1057Lote_ValorGlosas = new bool[] {false} ;
         P00XN7_A1124Contratante_LogoArquivo = new String[] {""} ;
         P00XN7_n1124Contratante_LogoArquivo = new bool[] {false} ;
         A1125Contratante_LogoNomeArq = "";
         A1124Contratante_LogoArquivo_Filename = "";
         A1126Contratante_LogoTipoArq = "";
         A1124Contratante_LogoArquivo_Filetype = "";
         A562Lote_Numero = "";
         A563Lote_Nome = "";
         A564Lote_Data = (DateTime)(DateTime.MinValue);
         A2088Lote_ParecerFinal = "";
         A2087Lote_Comentarios = "";
         A561Lote_UserNom = "";
         A568Lote_DataFim = DateTime.MinValue;
         A567Lote_DataIni = DateTime.MinValue;
         A1124Contratante_LogoArquivo = "";
         AV15Lote = "";
         AV16Lote_Nome = "";
         AV9Contratante_Logo = "";
         A40000Contratante_Logo_GXI = "";
         AV17MesAno = "";
         AV11Date = DateTime.MinValue;
         AV18Parecer = "";
         AV8Comentarios = "";
         AV22Responsavel = "";
         AV19Periodo = "";
         P00XN8_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00XN8_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00XN8_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         P00XN8_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         P00XN8_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00XN8_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00XN8_A1603ContagemResultado_CntCod = new int[1] ;
         P00XN8_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00XN8_A1604ContagemResultado_CntPrpCod = new int[1] ;
         P00XN8_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         P00XN8_A1605ContagemResultado_CntPrpPesCod = new int[1] ;
         P00XN8_n1605ContagemResultado_CntPrpPesCod = new bool[] {false} ;
         P00XN8_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00XN8_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00XN8_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         P00XN8_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         P00XN8_A1606ContagemResultado_CntPrpPesNom = new String[] {""} ;
         P00XN8_n1606ContagemResultado_CntPrpPesNom = new bool[] {false} ;
         P00XN8_A1612ContagemResultado_CntNum = new String[] {""} ;
         P00XN8_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P00XN8_A78Contrato_NumeroAta = new String[] {""} ;
         P00XN8_n78Contrato_NumeroAta = new bool[] {false} ;
         P00XN8_A456ContagemResultado_Codigo = new int[1] ;
         A500ContagemResultado_ContratadaPessoaNom = "";
         A1606ContagemResultado_CntPrpPesNom = "";
         A1612ContagemResultado_CntNum = "";
         A78Contrato_NumeroAta = "";
         AV21RazaoSocial = "";
         AV13Gerente = "";
         AV10Contrato = "";
         AV20Processo = "";
         P00XN10_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00XN10_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00XN10_A601ContagemResultado_Servico = new int[1] ;
         P00XN10_n601ContagemResultado_Servico = new bool[] {false} ;
         P00XN10_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00XN10_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00XN10_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P00XN10_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P00XN10_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00XN10_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00XN10_A494ContagemResultado_Descricao = new String[] {""} ;
         P00XN10_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00XN10_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00XN10_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00XN10_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00XN10_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00XN10_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00XN10_A456ContagemResultado_Codigo = new int[1] ;
         A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
         A493ContagemResultado_DemandaFM = "";
         A494ContagemResultado_Descricao = "";
         A801ContagemResultado_ServicoSigla = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         AV12Envio = DateTime.MinValue;
         AV9Contratante_Logo = "";
         P00XN11_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00XN11_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00XN11_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00XN11_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00XN11_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_tav1__default(),
            new Object[][] {
                new Object[] {
               P00XN7_A595Lote_AreaTrabalhoCod, P00XN7_A559Lote_UserCod, P00XN7_A560Lote_PessoaCod, P00XN7_n560Lote_PessoaCod, P00XN7_A29Contratante_Codigo, P00XN7_n29Contratante_Codigo, P00XN7_A1125Contratante_LogoNomeArq, P00XN7_n1125Contratante_LogoNomeArq, P00XN7_A1126Contratante_LogoTipoArq, P00XN7_n1126Contratante_LogoTipoArq,
               P00XN7_A562Lote_Numero, P00XN7_A563Lote_Nome, P00XN7_A564Lote_Data, P00XN7_A2088Lote_ParecerFinal, P00XN7_n2088Lote_ParecerFinal, P00XN7_A2087Lote_Comentarios, P00XN7_n2087Lote_Comentarios, P00XN7_A561Lote_UserNom, P00XN7_n561Lote_UserNom, P00XN7_A596Lote_Codigo,
               P00XN7_A568Lote_DataFim, P00XN7_A567Lote_DataIni, P00XN7_A1057Lote_ValorGlosas, P00XN7_n1057Lote_ValorGlosas, P00XN7_A1124Contratante_LogoArquivo, P00XN7_n1124Contratante_LogoArquivo
               }
               , new Object[] {
               P00XN8_A490ContagemResultado_ContratadaCod, P00XN8_n490ContagemResultado_ContratadaCod, P00XN8_A499ContagemResultado_ContratadaPessoaCod, P00XN8_n499ContagemResultado_ContratadaPessoaCod, P00XN8_A1553ContagemResultado_CntSrvCod, P00XN8_n1553ContagemResultado_CntSrvCod, P00XN8_A1603ContagemResultado_CntCod, P00XN8_n1603ContagemResultado_CntCod, P00XN8_A1604ContagemResultado_CntPrpCod, P00XN8_n1604ContagemResultado_CntPrpCod,
               P00XN8_A1605ContagemResultado_CntPrpPesCod, P00XN8_n1605ContagemResultado_CntPrpPesCod, P00XN8_A597ContagemResultado_LoteAceiteCod, P00XN8_n597ContagemResultado_LoteAceiteCod, P00XN8_A500ContagemResultado_ContratadaPessoaNom, P00XN8_n500ContagemResultado_ContratadaPessoaNom, P00XN8_A1606ContagemResultado_CntPrpPesNom, P00XN8_n1606ContagemResultado_CntPrpPesNom, P00XN8_A1612ContagemResultado_CntNum, P00XN8_n1612ContagemResultado_CntNum,
               P00XN8_A78Contrato_NumeroAta, P00XN8_n78Contrato_NumeroAta, P00XN8_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00XN10_A1553ContagemResultado_CntSrvCod, P00XN10_n1553ContagemResultado_CntSrvCod, P00XN10_A601ContagemResultado_Servico, P00XN10_n601ContagemResultado_Servico, P00XN10_A597ContagemResultado_LoteAceiteCod, P00XN10_n597ContagemResultado_LoteAceiteCod, P00XN10_A1349ContagemResultado_DataExecucao, P00XN10_n1349ContagemResultado_DataExecucao, P00XN10_A493ContagemResultado_DemandaFM, P00XN10_n493ContagemResultado_DemandaFM,
               P00XN10_A494ContagemResultado_Descricao, P00XN10_n494ContagemResultado_Descricao, P00XN10_A801ContagemResultado_ServicoSigla, P00XN10_n801ContagemResultado_ServicoSigla, P00XN10_A471ContagemResultado_DataDmn, P00XN10_A682ContagemResultado_PFBFMUltima, P00XN10_A684ContagemResultado_PFBFSUltima, P00XN10_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00XN11_A597ContagemResultado_LoteAceiteCod, P00XN11_n597ContagemResultado_LoteAceiteCod, P00XN11_A512ContagemResultado_ValorPF, P00XN11_n512ContagemResultado_ValorPF, P00XN11_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV14Item ;
      private int A596Lote_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A595Lote_AreaTrabalhoCod ;
      private int A559Lote_UserCod ;
      private int A560Lote_PessoaCod ;
      private int A29Contratante_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int A1605ContagemResultado_CntPrpPesCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A456ContagemResultado_Codigo ;
      private int Gx_OldLine ;
      private int A601ContagemResultado_Servico ;
      private decimal A1057Lote_ValorGlosas ;
      private decimal A572Lote_Valor ;
      private decimal A1058Lote_ValorOSs ;
      private decimal AV26Valor ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal AV23TotalPF ;
      private decimal AV25TotalPFFS ;
      private decimal AV24TotalPFFM ;
      private decimal GXt_decimal1 ;
      private decimal A606ContagemResultado_ValorFinal ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String scmdbuf ;
      private String A1125Contratante_LogoNomeArq ;
      private String A1124Contratante_LogoArquivo_Filename ;
      private String A1126Contratante_LogoTipoArq ;
      private String A1124Contratante_LogoArquivo_Filetype ;
      private String A562Lote_Numero ;
      private String A563Lote_Nome ;
      private String A561Lote_UserNom ;
      private String AV15Lote ;
      private String AV16Lote_Nome ;
      private String AV17MesAno ;
      private String AV22Responsavel ;
      private String AV19Periodo ;
      private String A500ContagemResultado_ContratadaPessoaNom ;
      private String A1606ContagemResultado_CntPrpPesNom ;
      private String A1612ContagemResultado_CntNum ;
      private String A78Contrato_NumeroAta ;
      private String AV21RazaoSocial ;
      private String AV13Gerente ;
      private String AV10Contrato ;
      private String AV20Processo ;
      private String A801ContagemResultado_ServicoSigla ;
      private DateTime A564Lote_Data ;
      private DateTime A1349ContagemResultado_DataExecucao ;
      private DateTime A568Lote_DataFim ;
      private DateTime A567Lote_DataIni ;
      private DateTime AV11Date ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime AV12Envio ;
      private bool entryPointCalled ;
      private bool n560Lote_PessoaCod ;
      private bool n29Contratante_Codigo ;
      private bool n1125Contratante_LogoNomeArq ;
      private bool n1126Contratante_LogoTipoArq ;
      private bool n2088Lote_ParecerFinal ;
      private bool n2087Lote_Comentarios ;
      private bool n561Lote_UserNom ;
      private bool n1057Lote_ValorGlosas ;
      private bool n1124Contratante_LogoArquivo ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private bool n1605ContagemResultado_CntPrpPesCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n500ContagemResultado_ContratadaPessoaNom ;
      private bool n1606ContagemResultado_CntPrpPesNom ;
      private bool n1612ContagemResultado_CntNum ;
      private bool n78Contrato_NumeroAta ;
      private bool n601ContagemResultado_Servico ;
      private bool n1349ContagemResultado_DataExecucao ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n494ContagemResultado_Descricao ;
      private bool n801ContagemResultado_ServicoSigla ;
      private String A2088Lote_ParecerFinal ;
      private String A2087Lote_Comentarios ;
      private String AV18Parecer ;
      private String AV8Comentarios ;
      private String A40000Contratante_Logo_GXI ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String AV9Contratante_Logo ;
      private String Contratante_logo ;
      private String A1124Contratante_LogoArquivo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Lote_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00XN7_A595Lote_AreaTrabalhoCod ;
      private int[] P00XN7_A559Lote_UserCod ;
      private int[] P00XN7_A560Lote_PessoaCod ;
      private bool[] P00XN7_n560Lote_PessoaCod ;
      private int[] P00XN7_A29Contratante_Codigo ;
      private bool[] P00XN7_n29Contratante_Codigo ;
      private String[] P00XN7_A1125Contratante_LogoNomeArq ;
      private bool[] P00XN7_n1125Contratante_LogoNomeArq ;
      private String[] P00XN7_A1126Contratante_LogoTipoArq ;
      private bool[] P00XN7_n1126Contratante_LogoTipoArq ;
      private String[] P00XN7_A562Lote_Numero ;
      private String[] P00XN7_A563Lote_Nome ;
      private DateTime[] P00XN7_A564Lote_Data ;
      private String[] P00XN7_A2088Lote_ParecerFinal ;
      private bool[] P00XN7_n2088Lote_ParecerFinal ;
      private String[] P00XN7_A2087Lote_Comentarios ;
      private bool[] P00XN7_n2087Lote_Comentarios ;
      private String[] P00XN7_A561Lote_UserNom ;
      private bool[] P00XN7_n561Lote_UserNom ;
      private int[] P00XN7_A596Lote_Codigo ;
      private DateTime[] P00XN7_A568Lote_DataFim ;
      private DateTime[] P00XN7_A567Lote_DataIni ;
      private decimal[] P00XN7_A1057Lote_ValorGlosas ;
      private bool[] P00XN7_n1057Lote_ValorGlosas ;
      private String[] P00XN7_A1124Contratante_LogoArquivo ;
      private bool[] P00XN7_n1124Contratante_LogoArquivo ;
      private int[] P00XN8_A490ContagemResultado_ContratadaCod ;
      private bool[] P00XN8_n490ContagemResultado_ContratadaCod ;
      private int[] P00XN8_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] P00XN8_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] P00XN8_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00XN8_n1553ContagemResultado_CntSrvCod ;
      private int[] P00XN8_A1603ContagemResultado_CntCod ;
      private bool[] P00XN8_n1603ContagemResultado_CntCod ;
      private int[] P00XN8_A1604ContagemResultado_CntPrpCod ;
      private bool[] P00XN8_n1604ContagemResultado_CntPrpCod ;
      private int[] P00XN8_A1605ContagemResultado_CntPrpPesCod ;
      private bool[] P00XN8_n1605ContagemResultado_CntPrpPesCod ;
      private int[] P00XN8_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00XN8_n597ContagemResultado_LoteAceiteCod ;
      private String[] P00XN8_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] P00XN8_n500ContagemResultado_ContratadaPessoaNom ;
      private String[] P00XN8_A1606ContagemResultado_CntPrpPesNom ;
      private bool[] P00XN8_n1606ContagemResultado_CntPrpPesNom ;
      private String[] P00XN8_A1612ContagemResultado_CntNum ;
      private bool[] P00XN8_n1612ContagemResultado_CntNum ;
      private String[] P00XN8_A78Contrato_NumeroAta ;
      private bool[] P00XN8_n78Contrato_NumeroAta ;
      private int[] P00XN8_A456ContagemResultado_Codigo ;
      private int[] P00XN10_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00XN10_n1553ContagemResultado_CntSrvCod ;
      private int[] P00XN10_A601ContagemResultado_Servico ;
      private bool[] P00XN10_n601ContagemResultado_Servico ;
      private int[] P00XN10_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00XN10_n597ContagemResultado_LoteAceiteCod ;
      private DateTime[] P00XN10_A1349ContagemResultado_DataExecucao ;
      private bool[] P00XN10_n1349ContagemResultado_DataExecucao ;
      private String[] P00XN10_A493ContagemResultado_DemandaFM ;
      private bool[] P00XN10_n493ContagemResultado_DemandaFM ;
      private String[] P00XN10_A494ContagemResultado_Descricao ;
      private bool[] P00XN10_n494ContagemResultado_Descricao ;
      private String[] P00XN10_A801ContagemResultado_ServicoSigla ;
      private bool[] P00XN10_n801ContagemResultado_ServicoSigla ;
      private DateTime[] P00XN10_A471ContagemResultado_DataDmn ;
      private decimal[] P00XN10_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00XN10_A684ContagemResultado_PFBFSUltima ;
      private int[] P00XN10_A456ContagemResultado_Codigo ;
      private int[] P00XN11_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00XN11_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] P00XN11_A512ContagemResultado_ValorPF ;
      private bool[] P00XN11_n512ContagemResultado_ValorPF ;
      private int[] P00XN11_A456ContagemResultado_Codigo ;
   }

   public class arel_tav1__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XN7 ;
          prmP00XN7 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00XN7 ;
          cmdBufferP00XN7=" SELECT TOP 1 T1.[Lote_AreaTrabalhoCod] AS Lote_AreaTrabalhoCod, T1.[Lote_UserCod] AS Lote_UserCod, T4.[Usuario_PessoaCod] AS Lote_PessoaCod, T2.[Contratante_Codigo], T3.[Contratante_LogoNomeArq], T3.[Contratante_LogoTipoArq], T1.[Lote_Numero], T1.[Lote_Nome], T1.[Lote_Data], T1.[Lote_ParecerFinal], T1.[Lote_Comentarios], T5.[Pessoa_Nome] AS Lote_UserNom, T1.[Lote_Codigo], COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim, COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T7.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas, T3.[Contratante_LogoArquivo] FROM (((((([Lote] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Lote_AreaTrabalhoCod]) LEFT JOIN [Contratante] T3 WITH (NOLOCK) ON T3.[Contratante_Codigo] = T2.[Contratante_Codigo]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[Lote_UserCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) LEFT JOIN (SELECT MAX(COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim, T8.[ContagemResultado_LoteAceiteCod], MIN(COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni FROM ([ContagemResultado] T8 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T9 ON T9.[ContagemResultado_Codigo] = T8.[ContagemResultado_Codigo]) GROUP BY T8.[ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) INNER JOIN (SELECT COALESCE( T10.[GXC5], 0) + COALESCE( "
          + " T9.[GXC6], 0) AS Lote_ValorGlosas, T8.[Lote_Codigo] FROM (([Lote] T8 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T11.[ContagemResultadoIndicadores_Valor]) AS GXC6, T12.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T11 WITH (NOLOCK),  [Lote] T12 WITH (NOLOCK) WHERE T11.[ContagemResultadoIndicadores_LoteCod] = T12.[Lote_Codigo] GROUP BY T12.[Lote_Codigo] ) T9 ON T9.[Lote_Codigo] = T8.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC5, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T10 ON T10.[ContagemResultado_LoteAceiteCod] = T8.[Lote_Codigo]) ) T7 ON T7.[Lote_Codigo] = T1.[Lote_Codigo]) WHERE T1.[Lote_Codigo] = @Lote_Codigo ORDER BY T1.[Lote_Codigo]" ;
          Object[] prmP00XN8 ;
          prmP00XN8 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XN10 ;
          prmP00XN10 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XN11 ;
          prmP00XN11 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XN7", cmdBufferP00XN7,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XN7,1,0,true,true )
             ,new CursorDef("P00XN8", "SELECT TOP 1 T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Contrato_Codigo] AS ContagemResultado_CntCod, T5.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod, T6.[Usuario_PessoaCod] AS ContagemResultado_CntPrpPesCod, T1.[ContagemResultado_LoteAceiteCod], T3.[Pessoa_Nome] AS ContagemResultado_ContratadaPessoaNom, T7.[Pessoa_Nome] AS ContagemResultado_CntPrpPesNom, T5.[Contrato_Numero] AS ContagemResultado_CntNum, T5.[Contrato_NumeroAta], T1.[ContagemResultado_Codigo] FROM (((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) LEFT JOIN [Usuario] T6 WITH (NOLOCK) ON T6.[Usuario_Codigo] = T5.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Usuario_PessoaCod]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY T1.[ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XN8,1,0,false,true )
             ,new CursorDef("P00XN10", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_DataExecucao], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Descricao], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DataDmn], COALESCE( T4.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T4.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, T1.[ContagemResultado_Codigo] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY T1.[ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XN10,100,0,true,false )
             ,new CursorDef("P00XN11", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XN11,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 10) ;
                ((String[]) buf[11])[0] = rslt.getString(8, 50) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDateTime(9) ;
                ((String[]) buf[13])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getLongVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 100) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((int[]) buf[19])[0] = rslt.getInt(13) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(14) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(15) ;
                ((decimal[]) buf[22])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(16);
                ((String[]) buf[24])[0] = rslt.getBLOBFile(17, rslt.getString(6, 10), rslt.getString(5, 50)) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(17);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getString(10, 20) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((String[]) buf[20])[0] = rslt.getString(11, 10) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(8) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(9) ;
                ((decimal[]) buf[16])[0] = rslt.getDecimal(10) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
