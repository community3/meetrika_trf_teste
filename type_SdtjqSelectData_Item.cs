/*
               File: type_SdtjqSelectData_Item
        Description: jqSelectData
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:57.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "jqSelectData.Item" )]
   [XmlType(TypeName =  "jqSelectData.Item" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtjqSelectData_Item : GxUserType
   {
      public SdtjqSelectData_Item( )
      {
         /* Constructor for serialization */
         gxTv_SdtjqSelectData_Item_Id = "";
         gxTv_SdtjqSelectData_Item_Descr = "";
      }

      public SdtjqSelectData_Item( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtjqSelectData_Item deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtjqSelectData_Item)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtjqSelectData_Item obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Descr = deserialized.gxTpr_Descr;
         obj.gxTpr_Selected = deserialized.gxTpr_Selected;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Id") )
               {
                  gxTv_SdtjqSelectData_Item_Id = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Descr") )
               {
                  gxTv_SdtjqSelectData_Item_Descr = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Selected") )
               {
                  gxTv_SdtjqSelectData_Item_Selected = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "jqSelectData.Item";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Id", StringUtil.RTrim( gxTv_SdtjqSelectData_Item_Id));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Descr", StringUtil.RTrim( gxTv_SdtjqSelectData_Item_Descr));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Selected", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtjqSelectData_Item_Selected)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Id", gxTv_SdtjqSelectData_Item_Id, false);
         AddObjectProperty("Descr", gxTv_SdtjqSelectData_Item_Descr, false);
         AddObjectProperty("Selected", gxTv_SdtjqSelectData_Item_Selected, false);
         return  ;
      }

      [  SoapElement( ElementName = "Id" )]
      [  XmlElement( ElementName = "Id"   )]
      public String gxTpr_Id
      {
         get {
            return gxTv_SdtjqSelectData_Item_Id ;
         }

         set {
            gxTv_SdtjqSelectData_Item_Id = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Descr" )]
      [  XmlElement( ElementName = "Descr"   )]
      public String gxTpr_Descr
      {
         get {
            return gxTv_SdtjqSelectData_Item_Descr ;
         }

         set {
            gxTv_SdtjqSelectData_Item_Descr = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Selected" )]
      [  XmlElement( ElementName = "Selected"   )]
      public bool gxTpr_Selected
      {
         get {
            return gxTv_SdtjqSelectData_Item_Selected ;
         }

         set {
            gxTv_SdtjqSelectData_Item_Selected = value;
         }

      }

      public void initialize( )
      {
         gxTv_SdtjqSelectData_Item_Id = "";
         gxTv_SdtjqSelectData_Item_Descr = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected bool gxTv_SdtjqSelectData_Item_Selected ;
      protected String gxTv_SdtjqSelectData_Item_Id ;
      protected String gxTv_SdtjqSelectData_Item_Descr ;
   }

   [DataContract(Name = @"jqSelectData.Item", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtjqSelectData_Item_RESTInterface : GxGenericCollectionItem<SdtjqSelectData_Item>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtjqSelectData_Item_RESTInterface( ) : base()
      {
      }

      public SdtjqSelectData_Item_RESTInterface( SdtjqSelectData_Item psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Id" , Order = 0 )]
      public String gxTpr_Id
      {
         get {
            return sdt.gxTpr_Id ;
         }

         set {
            sdt.gxTpr_Id = (String)(value);
         }

      }

      [DataMember( Name = "Descr" , Order = 1 )]
      public String gxTpr_Descr
      {
         get {
            return sdt.gxTpr_Descr ;
         }

         set {
            sdt.gxTpr_Descr = (String)(value);
         }

      }

      [DataMember( Name = "Selected" , Order = 2 )]
      public bool gxTpr_Selected
      {
         get {
            return sdt.gxTpr_Selected ;
         }

         set {
            sdt.gxTpr_Selected = value;
         }

      }

      public SdtjqSelectData_Item sdt
      {
         get {
            return (SdtjqSelectData_Item)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtjqSelectData_Item() ;
         }
      }

   }

}
