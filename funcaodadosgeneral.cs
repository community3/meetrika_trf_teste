/*
               File: FuncaoDadosGeneral
        Description: Funcao Dados General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:34.69
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaodadosgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public funcaodadosgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public funcaodadosgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoDados_Codigo ,
                           int aP1_FuncaoDados_SistemaCod )
      {
         this.A368FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.A370FuncaoDados_SistemaCod = aP1_FuncaoDados_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbFuncaoDados_Tipo = new GXCombobox();
         cmbFuncaoDados_Ativo = new GXCombobox();
         cmbFuncaoDados_UpdAoImpBsln = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                  A370FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A368FuncaoDados_Codigo,(int)A370FuncaoDados_SistemaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA9E2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV17Pgmname = "FuncaoDadosGeneral";
               context.Gx_err = 0;
               WS9E2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Funcao Dados General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822503474");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaodadosgeneral.aspx") + "?" + UrlEncode("" +A368FuncaoDados_Codigo) + "," + UrlEncode("" +A370FuncaoDados_SistemaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_OBSERVACAO", A1245FuncaoDados_Observacao);
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA368FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A369FuncaoDados_Nome, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_DESCRICAO", GetSecureSignedToken( sPrefix, A397FuncaoDados_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_ATIVO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A394FuncaoDados_Ativo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_UPDAOIMPBSLN", GetSecureSignedToken( sPrefix, A1267FuncaoDados_UpdAoImpBsln));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_OBSERVACAO_Enabled", StringUtil.BoolToStr( Funcaodados_observacao_Enabled));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm9E2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("funcaodadosgeneral.js", "?202042822503477");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            if ( ! ( WebComp_Tabelas == null ) )
            {
               WebComp_Tabelas.componentjscripts();
            }
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "FuncaoDadosGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Funcao Dados General" ;
      }

      protected void WB9E0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "funcaodadosgeneral.aspx");
               context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
               context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
            }
            wb_table1_2_9E2( true) ;
         }
         else
         {
            wb_table1_2_9E2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9E2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoDados_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncaoDadosGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_SistemaCod_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoDados_SistemaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncaoDadosGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START9E2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Funcao Dados General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP9E0( ) ;
            }
         }
      }

      protected void WS9E2( )
      {
         START9E2( ) ;
         EVT9E2( ) ;
      }

      protected void EVT9E2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E119E2 */
                                    E119E2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E129E2 */
                                    E129E2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E139E2 */
                                    E139E2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOATUALIZAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E149E2 */
                                    E149E2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 54 )
                        {
                           OldTabelas = cgiGet( sPrefix+"W0054");
                           if ( ( StringUtil.Len( OldTabelas) == 0 ) || ( StringUtil.StrCmp(OldTabelas, WebComp_Tabelas_Component) != 0 ) )
                           {
                              WebComp_Tabelas = getWebComponent(GetType(), "GeneXus.Programs", OldTabelas, new Object[] {context} );
                              WebComp_Tabelas.ComponentInit();
                              WebComp_Tabelas.Name = "OldTabelas";
                              WebComp_Tabelas_Component = OldTabelas;
                           }
                           if ( StringUtil.Len( WebComp_Tabelas_Component) != 0 )
                           {
                              WebComp_Tabelas.componentprocess(sPrefix+"W0054", "", sEvt);
                           }
                           WebComp_Tabelas_Component = OldTabelas;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE9E2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm9E2( ) ;
            }
         }
      }

      protected void PA9E2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbFuncaoDados_Tipo.Name = "FUNCAODADOS_TIPO";
            cmbFuncaoDados_Tipo.WebTags = "";
            cmbFuncaoDados_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoDados_Tipo.addItem("ALI", "ALI", 0);
            cmbFuncaoDados_Tipo.addItem("AIE", "AIE", 0);
            cmbFuncaoDados_Tipo.addItem("DC", "DC", 0);
            if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
            {
               A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
            }
            cmbFuncaoDados_Ativo.Name = "FUNCAODADOS_ATIVO";
            cmbFuncaoDados_Ativo.WebTags = "";
            cmbFuncaoDados_Ativo.addItem("A", "Ativa", 0);
            cmbFuncaoDados_Ativo.addItem("E", "Exclu�da", 0);
            cmbFuncaoDados_Ativo.addItem("R", "Rejeitada", 0);
            if ( cmbFuncaoDados_Ativo.ItemCount > 0 )
            {
               A394FuncaoDados_Ativo = cmbFuncaoDados_Ativo.getValidValue(A394FuncaoDados_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_ATIVO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A394FuncaoDados_Ativo, ""))));
            }
            cmbFuncaoDados_UpdAoImpBsln.Name = "FUNCAODADOS_UPDAOIMPBSLN";
            cmbFuncaoDados_UpdAoImpBsln.WebTags = "";
            cmbFuncaoDados_UpdAoImpBsln.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbFuncaoDados_UpdAoImpBsln.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbFuncaoDados_UpdAoImpBsln.ItemCount > 0 )
            {
               A1267FuncaoDados_UpdAoImpBsln = StringUtil.StrToBool( cmbFuncaoDados_UpdAoImpBsln.getValidValue(StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln)));
               n1267FuncaoDados_UpdAoImpBsln = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_UPDAOIMPBSLN", GetSecureSignedToken( sPrefix, A1267FuncaoDados_UpdAoImpBsln));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
         {
            A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
         }
         if ( cmbFuncaoDados_Ativo.ItemCount > 0 )
         {
            A394FuncaoDados_Ativo = cmbFuncaoDados_Ativo.getValidValue(A394FuncaoDados_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_ATIVO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A394FuncaoDados_Ativo, ""))));
         }
         if ( cmbFuncaoDados_UpdAoImpBsln.ItemCount > 0 )
         {
            A1267FuncaoDados_UpdAoImpBsln = StringUtil.StrToBool( cmbFuncaoDados_UpdAoImpBsln.getValidValue(StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln)));
            n1267FuncaoDados_UpdAoImpBsln = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_UPDAOIMPBSLN", GetSecureSignedToken( sPrefix, A1267FuncaoDados_UpdAoImpBsln));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9E2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV17Pgmname = "FuncaoDadosGeneral";
         context.Gx_err = 0;
      }

      protected void RF9E2( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Tabelas_Component) != 0 )
               {
                  WebComp_Tabelas.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H009E2 */
            pr_default.execute(0, new Object[] {A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1267FuncaoDados_UpdAoImpBsln = H009E2_A1267FuncaoDados_UpdAoImpBsln[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_UPDAOIMPBSLN", GetSecureSignedToken( sPrefix, A1267FuncaoDados_UpdAoImpBsln));
               n1267FuncaoDados_UpdAoImpBsln = H009E2_n1267FuncaoDados_UpdAoImpBsln[0];
               A394FuncaoDados_Ativo = H009E2_A394FuncaoDados_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_ATIVO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A394FuncaoDados_Ativo, ""))));
               A1245FuncaoDados_Observacao = H009E2_A1245FuncaoDados_Observacao[0];
               n1245FuncaoDados_Observacao = H009E2_n1245FuncaoDados_Observacao[0];
               A397FuncaoDados_Descricao = H009E2_A397FuncaoDados_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_DESCRICAO", GetSecureSignedToken( sPrefix, A397FuncaoDados_Descricao));
               n397FuncaoDados_Descricao = H009E2_n397FuncaoDados_Descricao[0];
               A373FuncaoDados_Tipo = H009E2_A373FuncaoDados_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
               A369FuncaoDados_Nome = H009E2_A369FuncaoDados_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A369FuncaoDados_Nome, ""))));
               /* Execute user event: E129E2 */
               E129E2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB9E0( ) ;
         }
      }

      protected void STRUP9E0( )
      {
         /* Before Start, stand alone formulas. */
         AV17Pgmname = "FuncaoDadosGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E119E2 */
         E119E2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A369FuncaoDados_Nome = cgiGet( edtFuncaoDados_Nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A369FuncaoDados_Nome, ""))));
            cmbFuncaoDados_Tipo.CurrentValue = cgiGet( cmbFuncaoDados_Tipo_Internalname);
            A373FuncaoDados_Tipo = cgiGet( cmbFuncaoDados_Tipo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
            A397FuncaoDados_Descricao = cgiGet( edtFuncaoDados_Descricao_Internalname);
            n397FuncaoDados_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_DESCRICAO", GetSecureSignedToken( sPrefix, A397FuncaoDados_Descricao));
            cmbFuncaoDados_Ativo.CurrentValue = cgiGet( cmbFuncaoDados_Ativo_Internalname);
            A394FuncaoDados_Ativo = cgiGet( cmbFuncaoDados_Ativo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_ATIVO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A394FuncaoDados_Ativo, ""))));
            cmbFuncaoDados_UpdAoImpBsln.CurrentValue = cgiGet( cmbFuncaoDados_UpdAoImpBsln_Internalname);
            A1267FuncaoDados_UpdAoImpBsln = StringUtil.StrToBool( cgiGet( cmbFuncaoDados_UpdAoImpBsln_Internalname));
            n1267FuncaoDados_UpdAoImpBsln = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAODADOS_UPDAOIMPBSLN", GetSecureSignedToken( sPrefix, A1267FuncaoDados_UpdAoImpBsln));
            /* Read saved values. */
            A1245FuncaoDados_Observacao = cgiGet( sPrefix+"FUNCAODADOS_OBSERVACAO");
            wcpOA368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA368FuncaoDados_Codigo"), ",", "."));
            wcpOA370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA370FuncaoDados_SistemaCod"), ",", "."));
            Funcaodados_observacao_Enabled = StringUtil.StrToBool( cgiGet( sPrefix+"FUNCAODADOS_OBSERVACAO_Enabled"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E119E2 */
         E119E2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E119E2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Tabelas_Component), StringUtil.Lower( "WC_FuncaoDadosTabelas")) != 0 )
         {
            WebComp_Tabelas = getWebComponent(GetType(), "GeneXus.Programs", "wc_funcaodadostabelas", new Object[] {context} );
            WebComp_Tabelas.ComponentInit();
            WebComp_Tabelas.Name = "WC_FuncaoDadosTabelas";
            WebComp_Tabelas_Component = "WC_FuncaoDadosTabelas";
         }
         if ( StringUtil.Len( WebComp_Tabelas_Component) != 0 )
         {
            WebComp_Tabelas.setjustcreated();
            WebComp_Tabelas.componentprepare(new Object[] {(String)sPrefix+"W0054",(String)"",(int)A370FuncaoDados_SistemaCod,(int)A368FuncaoDados_Codigo});
            WebComp_Tabelas.componentbind(new Object[] {(String)"",(String)""});
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E129E2( )
      {
         /* Load Routine */
         edtFuncaoDados_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_Codigo_Visible), 5, 0)));
         edtFuncaoDados_SistemaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_SistemaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_SistemaCod_Visible), 5, 0)));
      }

      protected void E139E2( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +A370FuncaoDados_SistemaCod) + "," + UrlEncode(StringUtil.RTrim("FuncaoDados"));
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV17Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "FuncaoDados";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "FuncaoDados_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7FuncaoDados_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "FuncaoDados_SistemaCod";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV12FuncaoDados_SistemaCod), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E149E2( )
      {
         /* 'DoAtualizar' Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void wb_table1_2_9E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_9E2( true) ;
         }
         else
         {
            wb_table2_8_9E2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_9E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9E2e( true) ;
         }
         else
         {
            wb_table1_2_9E2e( false) ;
         }
      }

      protected void wb_table2_8_9E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_11_9E2( true) ;
         }
         else
         {
            wb_table3_11_9E2( false) ;
         }
         return  ;
      }

      protected void wb_table3_11_9E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table4_51_9E2( true) ;
         }
         else
         {
            wb_table4_51_9E2( false) ;
         }
         return  ;
      }

      protected void wb_table4_51_9E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_9E2e( true) ;
         }
         else
         {
            wb_table2_8_9E2e( false) ;
         }
      }

      protected void wb_table4_51_9E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, sPrefix+"W0054"+"", StringUtil.RTrim( WebComp_Tabelas_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+sPrefix+"gxHTMLWrpW0054"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Tabelas_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabelas), StringUtil.Lower( WebComp_Tabelas_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp(sPrefix+"gxHTMLWrpW0054"+"");
                  }
                  WebComp_Tabelas.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabelas), StringUtil.Lower( WebComp_Tabelas_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoDadosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_51_9E2e( true) ;
         }
         else
         {
            wb_table4_51_9E2e( false) ;
         }
      }

      protected void wb_table3_11_9E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td rowspan=\"2\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_nome_Internalname, "Nome", "", "", lblTextblockfuncaodados_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDadosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"2\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoDados_Nome_Internalname, A369FuncaoDados_Nome, "", "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_FuncaoDadosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo_Internalname, "Tipo", "", "", lblTextblockfuncaodados_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDadosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDados_Tipo, cmbFuncaoDados_Tipo_Internalname, StringUtil.RTrim( A373FuncaoDados_Tipo), 1, cmbFuncaoDados_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_FuncaoDadosGeneral.htm");
            cmbFuncaoDados_Tipo.CurrentValue = StringUtil.RTrim( A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoDados_Tipo_Internalname, "Values", (String)(cmbFuncaoDados_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_descricao_Internalname, "Descri��o", "", "", lblTextblockfuncaodados_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDadosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoDados_Descricao_Internalname, A397FuncaoDados_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_FuncaoDadosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_observacao_Internalname, "Observa��o", "", "", lblTextblockfuncaodados_observacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDadosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"FUNCAODADOS_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_ativo_Internalname, "Status", "", "", lblTextblockfuncaodados_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDadosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDados_Ativo, cmbFuncaoDados_Ativo_Internalname, StringUtil.RTrim( A394FuncaoDados_Ativo), 1, cmbFuncaoDados_Ativo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_FuncaoDadosGeneral.htm");
            cmbFuncaoDados_Ativo.CurrentValue = StringUtil.RTrim( A394FuncaoDados_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoDados_Ativo_Internalname, "Values", (String)(cmbFuncaoDados_Ativo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_updaoimpbsln_Internalname, "Atualizar ao importar Baseline", "", "", lblTextblockfuncaodados_updaoimpbsln_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoDadosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDados_UpdAoImpBsln, cmbFuncaoDados_UpdAoImpBsln_Internalname, StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln), 1, cmbFuncaoDados_UpdAoImpBsln_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_FuncaoDadosGeneral.htm");
            cmbFuncaoDados_UpdAoImpBsln.CurrentValue = StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoDados_UpdAoImpBsln_Internalname, "Values", (String)(cmbFuncaoDados_UpdAoImpBsln.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_11_9E2e( true) ;
         }
         else
         {
            wb_table3_11_9E2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A368FuncaoDados_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A370FuncaoDados_SistemaCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9E2( ) ;
         WS9E2( ) ;
         WE9E2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA368FuncaoDados_Codigo = (String)((String)getParm(obj,0));
         sCtrlA370FuncaoDados_SistemaCod = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA9E2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "funcaodadosgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA9E2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A368FuncaoDados_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A370FuncaoDados_SistemaCod = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
         }
         wcpOA368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA368FuncaoDados_Codigo"), ",", "."));
         wcpOA370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA370FuncaoDados_SistemaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A368FuncaoDados_Codigo != wcpOA368FuncaoDados_Codigo ) || ( A370FuncaoDados_SistemaCod != wcpOA370FuncaoDados_SistemaCod ) ) )
         {
            setjustcreated();
         }
         wcpOA368FuncaoDados_Codigo = A368FuncaoDados_Codigo;
         wcpOA370FuncaoDados_SistemaCod = A370FuncaoDados_SistemaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA368FuncaoDados_Codigo = cgiGet( sPrefix+"A368FuncaoDados_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA368FuncaoDados_Codigo) > 0 )
         {
            A368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA368FuncaoDados_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         }
         else
         {
            A368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A368FuncaoDados_Codigo_PARM"), ",", "."));
         }
         sCtrlA370FuncaoDados_SistemaCod = cgiGet( sPrefix+"A370FuncaoDados_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlA370FuncaoDados_SistemaCod) > 0 )
         {
            A370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA370FuncaoDados_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
         }
         else
         {
            A370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A370FuncaoDados_SistemaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA9E2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS9E2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS9E2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A368FuncaoDados_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA368FuncaoDados_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A368FuncaoDados_Codigo_CTRL", StringUtil.RTrim( sCtrlA368FuncaoDados_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A370FuncaoDados_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA370FuncaoDados_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A370FuncaoDados_SistemaCod_CTRL", StringUtil.RTrim( sCtrlA370FuncaoDados_SistemaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE9E2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
         if ( ! ( WebComp_Tabelas == null ) )
         {
            WebComp_Tabelas.componentjscripts();
         }
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         if ( ! ( WebComp_Tabelas == null ) )
         {
            if ( StringUtil.Len( WebComp_Tabelas_Component) != 0 )
            {
               WebComp_Tabelas.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822503525");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("funcaodadosgeneral.js", "?202042822503525");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockfuncaodados_nome_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_NOME";
         edtFuncaoDados_Nome_Internalname = sPrefix+"FUNCAODADOS_NOME";
         lblTextblockfuncaodados_tipo_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_TIPO";
         cmbFuncaoDados_Tipo_Internalname = sPrefix+"FUNCAODADOS_TIPO";
         lblTextblockfuncaodados_descricao_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_DESCRICAO";
         edtFuncaoDados_Descricao_Internalname = sPrefix+"FUNCAODADOS_DESCRICAO";
         lblTextblockfuncaodados_observacao_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_OBSERVACAO";
         Funcaodados_observacao_Internalname = sPrefix+"FUNCAODADOS_OBSERVACAO";
         lblTextblockfuncaodados_ativo_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_ATIVO";
         cmbFuncaoDados_Ativo_Internalname = sPrefix+"FUNCAODADOS_ATIVO";
         lblTextblockfuncaodados_updaoimpbsln_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_UPDAOIMPBSLN";
         cmbFuncaoDados_UpdAoImpBsln_Internalname = sPrefix+"FUNCAODADOS_UPDAOIMPBSLN";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtFuncaoDados_Codigo_Internalname = sPrefix+"FUNCAODADOS_CODIGO";
         edtFuncaoDados_SistemaCod_Internalname = sPrefix+"FUNCAODADOS_SISTEMACOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbFuncaoDados_UpdAoImpBsln_Jsonclick = "";
         cmbFuncaoDados_Ativo_Jsonclick = "";
         Funcaodados_observacao_Enabled = Convert.ToBoolean( 0);
         cmbFuncaoDados_Tipo_Jsonclick = "";
         edtFuncaoDados_SistemaCod_Jsonclick = "";
         edtFuncaoDados_SistemaCod_Visible = 1;
         edtFuncaoDados_Codigo_Jsonclick = "";
         edtFuncaoDados_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E139E2',iparms:[{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOATUALIZAR'","{handler:'E149E2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV17Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1245FuncaoDados_Observacao = "";
         A369FuncaoDados_Nome = "";
         A373FuncaoDados_Tipo = "";
         A397FuncaoDados_Descricao = "";
         A394FuncaoDados_Ativo = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldTabelas = "";
         WebComp_Tabelas_Component = "";
         scmdbuf = "";
         H009E2_A368FuncaoDados_Codigo = new int[1] ;
         H009E2_A370FuncaoDados_SistemaCod = new int[1] ;
         H009E2_A1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         H009E2_n1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         H009E2_A394FuncaoDados_Ativo = new String[] {""} ;
         H009E2_A1245FuncaoDados_Observacao = new String[] {""} ;
         H009E2_n1245FuncaoDados_Observacao = new bool[] {false} ;
         H009E2_A397FuncaoDados_Descricao = new String[] {""} ;
         H009E2_n397FuncaoDados_Descricao = new bool[] {false} ;
         H009E2_A373FuncaoDados_Tipo = new String[] {""} ;
         H009E2_A369FuncaoDados_Nome = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockfuncaodados_nome_Jsonclick = "";
         lblTextblockfuncaodados_tipo_Jsonclick = "";
         lblTextblockfuncaodados_descricao_Jsonclick = "";
         lblTextblockfuncaodados_observacao_Jsonclick = "";
         lblTextblockfuncaodados_ativo_Jsonclick = "";
         lblTextblockfuncaodados_updaoimpbsln_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA368FuncaoDados_Codigo = "";
         sCtrlA370FuncaoDados_SistemaCod = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaodadosgeneral__default(),
            new Object[][] {
                new Object[] {
               H009E2_A368FuncaoDados_Codigo, H009E2_A370FuncaoDados_SistemaCod, H009E2_A1267FuncaoDados_UpdAoImpBsln, H009E2_n1267FuncaoDados_UpdAoImpBsln, H009E2_A394FuncaoDados_Ativo, H009E2_A1245FuncaoDados_Observacao, H009E2_n1245FuncaoDados_Observacao, H009E2_A397FuncaoDados_Descricao, H009E2_n397FuncaoDados_Descricao, H009E2_A373FuncaoDados_Tipo,
               H009E2_A369FuncaoDados_Nome
               }
            }
         );
         WebComp_Tabelas = new GeneXus.Http.GXNullWebComponent();
         AV17Pgmname = "FuncaoDadosGeneral";
         /* GeneXus formulas. */
         AV17Pgmname = "FuncaoDadosGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A368FuncaoDados_Codigo ;
      private int A370FuncaoDados_SistemaCod ;
      private int wcpOA368FuncaoDados_Codigo ;
      private int wcpOA370FuncaoDados_SistemaCod ;
      private int edtFuncaoDados_Codigo_Visible ;
      private int edtFuncaoDados_SistemaCod_Visible ;
      private int AV7FuncaoDados_Codigo ;
      private int AV12FuncaoDados_SistemaCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV17Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A373FuncaoDados_Tipo ;
      private String A394FuncaoDados_Ativo ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtFuncaoDados_Codigo_Internalname ;
      private String edtFuncaoDados_Codigo_Jsonclick ;
      private String edtFuncaoDados_SistemaCod_Internalname ;
      private String edtFuncaoDados_SistemaCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldTabelas ;
      private String WebComp_Tabelas_Component ;
      private String scmdbuf ;
      private String edtFuncaoDados_Nome_Internalname ;
      private String cmbFuncaoDados_Tipo_Internalname ;
      private String edtFuncaoDados_Descricao_Internalname ;
      private String cmbFuncaoDados_Ativo_Internalname ;
      private String cmbFuncaoDados_UpdAoImpBsln_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableattributes_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String TempTags ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockfuncaodados_nome_Internalname ;
      private String lblTextblockfuncaodados_nome_Jsonclick ;
      private String lblTextblockfuncaodados_tipo_Internalname ;
      private String lblTextblockfuncaodados_tipo_Jsonclick ;
      private String cmbFuncaoDados_Tipo_Jsonclick ;
      private String lblTextblockfuncaodados_descricao_Internalname ;
      private String lblTextblockfuncaodados_descricao_Jsonclick ;
      private String lblTextblockfuncaodados_observacao_Internalname ;
      private String lblTextblockfuncaodados_observacao_Jsonclick ;
      private String lblTextblockfuncaodados_ativo_Internalname ;
      private String lblTextblockfuncaodados_ativo_Jsonclick ;
      private String cmbFuncaoDados_Ativo_Jsonclick ;
      private String lblTextblockfuncaodados_updaoimpbsln_Internalname ;
      private String lblTextblockfuncaodados_updaoimpbsln_Jsonclick ;
      private String cmbFuncaoDados_UpdAoImpBsln_Jsonclick ;
      private String sCtrlA368FuncaoDados_Codigo ;
      private String sCtrlA370FuncaoDados_SistemaCod ;
      private String Funcaodados_observacao_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1267FuncaoDados_UpdAoImpBsln ;
      private bool Funcaodados_observacao_Enabled ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1267FuncaoDados_UpdAoImpBsln ;
      private bool n1245FuncaoDados_Observacao ;
      private bool n397FuncaoDados_Descricao ;
      private bool returnInSub ;
      private String A1245FuncaoDados_Observacao ;
      private String A397FuncaoDados_Descricao ;
      private String A369FuncaoDados_Nome ;
      private GXWebComponent WebComp_Tabelas ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbFuncaoDados_Tipo ;
      private GXCombobox cmbFuncaoDados_Ativo ;
      private GXCombobox cmbFuncaoDados_UpdAoImpBsln ;
      private IDataStoreProvider pr_default ;
      private int[] H009E2_A368FuncaoDados_Codigo ;
      private int[] H009E2_A370FuncaoDados_SistemaCod ;
      private bool[] H009E2_A1267FuncaoDados_UpdAoImpBsln ;
      private bool[] H009E2_n1267FuncaoDados_UpdAoImpBsln ;
      private String[] H009E2_A394FuncaoDados_Ativo ;
      private String[] H009E2_A1245FuncaoDados_Observacao ;
      private bool[] H009E2_n1245FuncaoDados_Observacao ;
      private String[] H009E2_A397FuncaoDados_Descricao ;
      private bool[] H009E2_n397FuncaoDados_Descricao ;
      private String[] H009E2_A373FuncaoDados_Tipo ;
      private String[] H009E2_A369FuncaoDados_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class funcaodadosgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009E2 ;
          prmH009E2 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009E2", "SELECT [FuncaoDados_Codigo], [FuncaoDados_SistemaCod], [FuncaoDados_UpdAoImpBsln], [FuncaoDados_Ativo], [FuncaoDados_Observacao], [FuncaoDados_Descricao], [FuncaoDados_Tipo], [FuncaoDados_Nome] FROM [FuncaoDados] WITH (NOLOCK) WHERE ([FuncaoDados_Codigo] = @FuncaoDados_Codigo) AND ([FuncaoDados_SistemaCod] = @FuncaoDados_SistemaCod) ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009E2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 3) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
