/*
               File: FuncaoAPFEvidencia
        Description: Evidencia da Funcao de Transa��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/3/2020 22:40:17.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaoapfevidencia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_19") == 0 )
         {
            A749FuncaoAPFEvidencia_MelhoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n749FuncaoAPFEvidencia_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A749FuncaoAPFEvidencia_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A749FuncaoAPFEvidencia_MelhoraCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_19( A749FuncaoAPFEvidencia_MelhoraCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7FuncaoAPFEvidencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPFEvidencia_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAOAPFEVIDENCIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoAPFEvidencia_Codigo), "ZZZZZ9")));
               A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Evidencia da Funcao de Transa��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtFuncaoAPFEvidencia_Descricao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public funcaoapfevidencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public funcaoapfevidencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_FuncaoAPFEvidencia_Codigo ,
                           ref int aP2_FuncaoAPF_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7FuncaoAPFEvidencia_Codigo = aP1_FuncaoAPFEvidencia_Codigo;
         this.A165FuncaoAPF_Codigo = aP2_FuncaoAPF_Codigo;
         executePrivate();
         aP2_FuncaoAPF_Codigo=this.A165FuncaoAPF_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1O63( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1O63e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFEvidencia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0, ",", "")), ((edtFuncaoAPFEvidencia_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A406FuncaoAPFEvidencia_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A406FuncaoAPFEvidencia_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFEvidencia_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoAPFEvidencia_Codigo_Visible, edtFuncaoAPFEvidencia_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncaoAPFEvidencia.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1O63( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1O63( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1O63e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_48_1O63( true) ;
         }
         return  ;
      }

      protected void wb_table3_48_1O63e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1O63e( true) ;
         }
         else
         {
            wb_table1_2_1O63e( false) ;
         }
      }

      protected void wb_table3_48_1O63( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_48_1O63e( true) ;
         }
         else
         {
            wb_table3_48_1O63e( false) ;
         }
      }

      protected void wb_table2_5_1O63( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1O63( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1O63e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1O63e( true) ;
         }
         else
         {
            wb_table2_5_1O63e( false) ;
         }
      }

      protected void wb_table4_13_1O63( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td rowspan=\"4\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfevidencia_descricao_Internalname, "Descri��o", "", "", lblTextblockfuncaoapfevidencia_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" rowspan=\"4\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPFEvidencia_Descricao_Internalname, A407FuncaoAPFEvidencia_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", 0, 1, edtFuncaoAPFEvidencia_Descricao_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfevidencia_arquivo_Internalname, "Arquivo", "", "", lblTextblockfuncaoapfevidencia_arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataContentCell'>") ;
            wb_table5_26_1O63( true) ;
         }
         return  ;
      }

      protected void wb_table5_26_1O63e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfevidencia_nomearq_Internalname, "Nome", "", "", lblTextblockfuncaoapfevidencia_nomearq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFEvidencia_NomeArq_Internalname, StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq), StringUtil.RTrim( context.localUtil.Format( A409FuncaoAPFEvidencia_NomeArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFEvidencia_NomeArq_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncaoAPFEvidencia_NomeArq_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "NomeArq", "left", true, "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfevidencia_tipoarq_Internalname, "Tipo", "", "", lblTextblockfuncaoapfevidencia_tipoarq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFEvidencia_TipoArq_Internalname, StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq), StringUtil.RTrim( context.localUtil.Format( A410FuncaoAPFEvidencia_TipoArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFEvidencia_TipoArq_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncaoAPFEvidencia_TipoArq_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "TipoArq", "left", true, "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfevidencia_data_Internalname, "Data/Hora", "", "", lblTextblockfuncaoapfevidencia_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtFuncaoAPFEvidencia_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFEvidencia_Data_Internalname, context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A411FuncaoAPFEvidencia_Data, "99/99/99 99:99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFEvidencia_Data_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtFuncaoAPFEvidencia_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_FuncaoAPFEvidencia.htm");
            GxWebStd.gx_bitmap( context, edtFuncaoAPFEvidencia_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtFuncaoAPFEvidencia_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1O63e( true) ;
         }
         else
         {
            wb_table4_13_1O63e( false) ;
         }
      }

      protected void wb_table5_26_1O63( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedfuncaoapfevidencia_arquivo_Internalname, tblTablemergedfuncaoapfevidencia_arquivo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "BootstrapAttribute";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            edtFuncaoAPFEvidencia_Arquivo_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "Filetype", edtFuncaoAPFEvidencia_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo)) )
            {
               gxblobfileaux.Source = A408FuncaoAPFEvidencia_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtFuncaoAPFEvidencia_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtFuncaoAPFEvidencia_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A408FuncaoAPFEvidencia_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n408FuncaoAPFEvidencia_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A408FuncaoAPFEvidencia_Arquivo", A408FuncaoAPFEvidencia_Arquivo);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A408FuncaoAPFEvidencia_Arquivo));
                  edtFuncaoAPFEvidencia_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "Filetype", edtFuncaoAPFEvidencia_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A408FuncaoAPFEvidencia_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtFuncaoAPFEvidencia_Arquivo_Internalname, StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo), context.PathToRelativeUrl( A408FuncaoAPFEvidencia_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtFuncaoAPFEvidencia_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtFuncaoAPFEvidencia_Arquivo_Filetype)) ? A408FuncaoAPFEvidencia_Arquivo : edtFuncaoAPFEvidencia_Arquivo_Filetype)) : edtFuncaoAPFEvidencia_Arquivo_Contenttype), false, "", edtFuncaoAPFEvidencia_Arquivo_Parameters, 0, edtFuncaoAPFEvidencia_Arquivo_Enabled, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtFuncaoAPFEvidencia_Arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", "", "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleararquivoanexo_Internalname, context.GetImagePath( "b2e8640a-9c28-412e-aa86-7f21d878f895", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgCleararquivoanexo_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleararquivoanexo_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEARARQUIVOANEXO\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_26_1O63e( true) ;
         }
         else
         {
            wb_table5_26_1O63e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111O2 */
         E111O2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A407FuncaoAPFEvidencia_Descricao = cgiGet( edtFuncaoAPFEvidencia_Descricao_Internalname);
               n407FuncaoAPFEvidencia_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A407FuncaoAPFEvidencia_Descricao", A407FuncaoAPFEvidencia_Descricao);
               n407FuncaoAPFEvidencia_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A407FuncaoAPFEvidencia_Descricao)) ? true : false);
               A408FuncaoAPFEvidencia_Arquivo = cgiGet( edtFuncaoAPFEvidencia_Arquivo_Internalname);
               n408FuncaoAPFEvidencia_Arquivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A408FuncaoAPFEvidencia_Arquivo", A408FuncaoAPFEvidencia_Arquivo);
               n408FuncaoAPFEvidencia_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo)) ? true : false);
               A409FuncaoAPFEvidencia_NomeArq = cgiGet( edtFuncaoAPFEvidencia_NomeArq_Internalname);
               n409FuncaoAPFEvidencia_NomeArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A409FuncaoAPFEvidencia_NomeArq", A409FuncaoAPFEvidencia_NomeArq);
               n409FuncaoAPFEvidencia_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq)) ? true : false);
               A410FuncaoAPFEvidencia_TipoArq = cgiGet( edtFuncaoAPFEvidencia_TipoArq_Internalname);
               n410FuncaoAPFEvidencia_TipoArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A410FuncaoAPFEvidencia_TipoArq", A410FuncaoAPFEvidencia_TipoArq);
               n410FuncaoAPFEvidencia_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq)) ? true : false);
               A411FuncaoAPFEvidencia_Data = context.localUtil.CToT( cgiGet( edtFuncaoAPFEvidencia_Data_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A411FuncaoAPFEvidencia_Data", context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
               A406FuncaoAPFEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFEvidencia_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
               /* Read saved values. */
               Z406FuncaoAPFEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z406FuncaoAPFEvidencia_Codigo"), ",", "."));
               Z411FuncaoAPFEvidencia_Data = context.localUtil.CToT( cgiGet( "Z411FuncaoAPFEvidencia_Data"), 0);
               Z409FuncaoAPFEvidencia_NomeArq = cgiGet( "Z409FuncaoAPFEvidencia_NomeArq");
               n409FuncaoAPFEvidencia_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq)) ? true : false);
               Z410FuncaoAPFEvidencia_TipoArq = cgiGet( "Z410FuncaoAPFEvidencia_TipoArq");
               n410FuncaoAPFEvidencia_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq)) ? true : false);
               Z750FuncaoAPFEvidencia_Ativo = StringUtil.StrToBool( cgiGet( "Z750FuncaoAPFEvidencia_Ativo"));
               n750FuncaoAPFEvidencia_Ativo = ((false==A750FuncaoAPFEvidencia_Ativo) ? true : false);
               Z749FuncaoAPFEvidencia_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "Z749FuncaoAPFEvidencia_MelhoraCod"), ",", "."));
               n749FuncaoAPFEvidencia_MelhoraCod = ((0==A749FuncaoAPFEvidencia_MelhoraCod) ? true : false);
               A750FuncaoAPFEvidencia_Ativo = StringUtil.StrToBool( cgiGet( "Z750FuncaoAPFEvidencia_Ativo"));
               n750FuncaoAPFEvidencia_Ativo = false;
               n750FuncaoAPFEvidencia_Ativo = ((false==A750FuncaoAPFEvidencia_Ativo) ? true : false);
               A749FuncaoAPFEvidencia_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "Z749FuncaoAPFEvidencia_MelhoraCod"), ",", "."));
               n749FuncaoAPFEvidencia_MelhoraCod = false;
               n749FuncaoAPFEvidencia_MelhoraCod = ((0==A749FuncaoAPFEvidencia_MelhoraCod) ? true : false);
               O410FuncaoAPFEvidencia_TipoArq = cgiGet( "O410FuncaoAPFEvidencia_TipoArq");
               n410FuncaoAPFEvidencia_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq)) ? true : false);
               O409FuncaoAPFEvidencia_NomeArq = cgiGet( "O409FuncaoAPFEvidencia_NomeArq");
               n409FuncaoAPFEvidencia_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq)) ? true : false);
               O408FuncaoAPFEvidencia_Arquivo = cgiGet( "O408FuncaoAPFEvidencia_Arquivo");
               n408FuncaoAPFEvidencia_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo)) ? true : false);
               O407FuncaoAPFEvidencia_Descricao = cgiGet( "O407FuncaoAPFEvidencia_Descricao");
               n407FuncaoAPFEvidencia_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A407FuncaoAPFEvidencia_Descricao)) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( "N165FuncaoAPF_Codigo"), ",", "."));
               N749FuncaoAPFEvidencia_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "N749FuncaoAPFEvidencia_MelhoraCod"), ",", "."));
               n749FuncaoAPFEvidencia_MelhoraCod = ((0==A749FuncaoAPFEvidencia_MelhoraCod) ? true : false);
               AV7FuncaoAPFEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( "vFUNCAOAPFEVIDENCIA_CODIGO"), ",", "."));
               AV11Insert_FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_FUNCAOAPF_CODIGO"), ",", "."));
               A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( "FUNCAOAPF_CODIGO"), ",", "."));
               AV13Insert_FuncaoAPFEvidencia_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_FUNCAOAPFEVIDENCIA_MELHORACOD"), ",", "."));
               A749FuncaoAPFEvidencia_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "FUNCAOAPFEVIDENCIA_MELHORACOD"), ",", "."));
               n749FuncaoAPFEvidencia_MelhoraCod = ((0==A749FuncaoAPFEvidencia_MelhoraCod) ? true : false);
               A166FuncaoAPF_Nome = cgiGet( "FUNCAOAPF_NOME");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A750FuncaoAPFEvidencia_Ativo = StringUtil.StrToBool( cgiGet( "FUNCAOAPFEVIDENCIA_ATIVO"));
               n750FuncaoAPFEvidencia_Ativo = ((false==A750FuncaoAPFEvidencia_Ativo) ? true : false);
               A360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "FUNCAOAPF_SISTEMACOD"), ",", "."));
               n360FuncaoAPF_SistemaCod = false;
               AV15Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               edtFuncaoAPFEvidencia_Arquivo_Filename = cgiGet( "FUNCAOAPFEVIDENCIA_ARQUIVO_Filename");
               edtFuncaoAPFEvidencia_Arquivo_Filetype = cgiGet( "FUNCAOAPFEVIDENCIA_ARQUIVO_Filetype");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo)) )
               {
                  edtFuncaoAPFEvidencia_Arquivo_Filename = (String)(CGIGetFileName(edtFuncaoAPFEvidencia_Arquivo_Internalname));
                  edtFuncaoAPFEvidencia_Arquivo_Filetype = (String)(CGIGetFileType(edtFuncaoAPFEvidencia_Arquivo_Internalname));
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo)) )
               {
                  GXCCtlgxBlob = "FUNCAOAPFEVIDENCIA_ARQUIVO" + "_gxBlob";
                  A408FuncaoAPFEvidencia_Arquivo = cgiGet( GXCCtlgxBlob);
                  n408FuncaoAPFEvidencia_Arquivo = false;
                  n408FuncaoAPFEvidencia_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo)) ? true : false);
               }
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "FuncaoAPFEvidencia";
               A406FuncaoAPFEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFEvidencia_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A406FuncaoAPFEvidencia_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A749FuncaoAPFEvidencia_MelhoraCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A750FuncaoAPFEvidencia_Ativo);
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A406FuncaoAPFEvidencia_Codigo != Z406FuncaoAPFEvidencia_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("funcaoapfevidencia:[SecurityCheckFailed value for]"+"FuncaoAPFEvidencia_Codigo:"+context.localUtil.Format( (decimal)(A406FuncaoAPFEvidencia_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("funcaoapfevidencia:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("funcaoapfevidencia:[SecurityCheckFailed value for]"+"FuncaoAPFEvidencia_MelhoraCod:"+context.localUtil.Format( (decimal)(A749FuncaoAPFEvidencia_MelhoraCod), "ZZZZZ9"));
                  GXUtil.WriteLog("funcaoapfevidencia:[SecurityCheckFailed value for]"+"FuncaoAPFEvidencia_Ativo:"+StringUtil.BoolToStr( A750FuncaoAPFEvidencia_Ativo));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A406FuncaoAPFEvidencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode63 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode63;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound63 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1O0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "FUNCAOAPFEVIDENCIA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtFuncaoAPFEvidencia_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111O2 */
                           E111O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121O2 */
                           E121O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEARARQUIVOANEXO'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E131O2 */
                           E131O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121O2 */
            E121O2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1O63( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1O63( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1O0( )
      {
         BeforeValidate1O63( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1O63( ) ;
            }
            else
            {
               CheckExtendedTable1O63( ) ;
               CloseExtendedTableCursors1O63( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1O0( )
      {
      }

      protected void E111O2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV15Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV16GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            while ( AV16GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV16GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "FuncaoAPF_Codigo") == 0 )
               {
                  AV11Insert_FuncaoAPF_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_FuncaoAPF_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "FuncaoAPFEvidencia_MelhoraCod") == 0 )
               {
                  AV13Insert_FuncaoAPFEvidencia_MelhoraCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_FuncaoAPFEvidencia_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_FuncaoAPFEvidencia_MelhoraCod), 6, 0)));
               }
               AV16GXV1 = (int)(AV16GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            }
         }
         edtFuncaoAPFEvidencia_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFEvidencia_Codigo_Visible), 5, 0)));
      }

      protected void E121O2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwfuncaoapfevidencia.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A165FuncaoAPF_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E131O2( )
      {
         /* 'DoClearArquivoAnexo' Routine */
         new prc_cleararquivoanexo(context ).execute(  A406FuncaoAPFEvidencia_Codigo,  "Evidencia") ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)A165FuncaoAPF_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1O63( short GX_JID )
      {
         if ( ( GX_JID == 17 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z411FuncaoAPFEvidencia_Data = T001O3_A411FuncaoAPFEvidencia_Data[0];
               Z409FuncaoAPFEvidencia_NomeArq = T001O3_A409FuncaoAPFEvidencia_NomeArq[0];
               Z410FuncaoAPFEvidencia_TipoArq = T001O3_A410FuncaoAPFEvidencia_TipoArq[0];
               Z750FuncaoAPFEvidencia_Ativo = T001O3_A750FuncaoAPFEvidencia_Ativo[0];
               Z749FuncaoAPFEvidencia_MelhoraCod = T001O3_A749FuncaoAPFEvidencia_MelhoraCod[0];
            }
            else
            {
               Z411FuncaoAPFEvidencia_Data = A411FuncaoAPFEvidencia_Data;
               Z409FuncaoAPFEvidencia_NomeArq = A409FuncaoAPFEvidencia_NomeArq;
               Z410FuncaoAPFEvidencia_TipoArq = A410FuncaoAPFEvidencia_TipoArq;
               Z750FuncaoAPFEvidencia_Ativo = A750FuncaoAPFEvidencia_Ativo;
               Z749FuncaoAPFEvidencia_MelhoraCod = A749FuncaoAPFEvidencia_MelhoraCod;
            }
         }
         if ( GX_JID == -17 )
         {
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
            Z406FuncaoAPFEvidencia_Codigo = A406FuncaoAPFEvidencia_Codigo;
            Z411FuncaoAPFEvidencia_Data = A411FuncaoAPFEvidencia_Data;
            Z409FuncaoAPFEvidencia_NomeArq = A409FuncaoAPFEvidencia_NomeArq;
            Z410FuncaoAPFEvidencia_TipoArq = A410FuncaoAPFEvidencia_TipoArq;
            Z407FuncaoAPFEvidencia_Descricao = A407FuncaoAPFEvidencia_Descricao;
            Z408FuncaoAPFEvidencia_Arquivo = A408FuncaoAPFEvidencia_Arquivo;
            Z750FuncaoAPFEvidencia_Ativo = A750FuncaoAPFEvidencia_Ativo;
            Z749FuncaoAPFEvidencia_MelhoraCod = A749FuncaoAPFEvidencia_MelhoraCod;
            Z166FuncaoAPF_Nome = A166FuncaoAPF_Nome;
            Z360FuncaoAPF_SistemaCod = A360FuncaoAPF_SistemaCod;
         }
      }

      protected void standaloneNotModal( )
      {
         edtFuncaoAPFEvidencia_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFEvidencia_Data_Enabled), 5, 0)));
         edtFuncaoAPFEvidencia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFEvidencia_Codigo_Enabled), 5, 0)));
         AV15Pgmname = "FuncaoAPFEvidencia";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Pgmname", AV15Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtFuncaoAPFEvidencia_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFEvidencia_Data_Enabled), 5, 0)));
         edtFuncaoAPFEvidencia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFEvidencia_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7FuncaoAPFEvidencia_Codigo) )
         {
            A406FuncaoAPFEvidencia_Codigo = AV7FuncaoAPFEvidencia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
         }
         /* Using cursor T001O4 */
         pr_default.execute(2, new Object[] {A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Fun��es APF - An�lise de Ponto de Fun��o'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A166FuncaoAPF_Nome = T001O4_A166FuncaoAPF_Nome[0];
         A360FuncaoAPF_SistemaCod = T001O4_A360FuncaoAPF_SistemaCod[0];
         n360FuncaoAPF_SistemaCod = T001O4_n360FuncaoAPF_SistemaCod[0];
         pr_default.close(2);
         Dvpanel_tableattributes_Title = "Evid�ncia de \" "+StringUtil.Substring( A166FuncaoAPF_Nome, 1, 130)+" \"";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_FuncaoAPFEvidencia_MelhoraCod) )
         {
            A749FuncaoAPFEvidencia_MelhoraCod = AV13Insert_FuncaoAPFEvidencia_MelhoraCod;
            n749FuncaoAPFEvidencia_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A749FuncaoAPFEvidencia_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A749FuncaoAPFEvidencia_MelhoraCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A411FuncaoAPFEvidencia_Data) && ( Gx_BScreen == 0 ) )
         {
            A411FuncaoAPFEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A411FuncaoAPFEvidencia_Data", context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A750FuncaoAPFEvidencia_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A750FuncaoAPFEvidencia_Ativo = true;
            n750FuncaoAPFEvidencia_Ativo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A750FuncaoAPFEvidencia_Ativo", A750FuncaoAPFEvidencia_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_FuncaoAPF_Codigo) )
            {
               A165FuncaoAPF_Codigo = AV11Insert_FuncaoAPF_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            }
         }
      }

      protected void Load1O63( )
      {
         /* Using cursor T001O6 */
         pr_default.execute(4, new Object[] {A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound63 = 1;
            A411FuncaoAPFEvidencia_Data = T001O6_A411FuncaoAPFEvidencia_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A411FuncaoAPFEvidencia_Data", context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
            A409FuncaoAPFEvidencia_NomeArq = T001O6_A409FuncaoAPFEvidencia_NomeArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A409FuncaoAPFEvidencia_NomeArq", A409FuncaoAPFEvidencia_NomeArq);
            n409FuncaoAPFEvidencia_NomeArq = T001O6_n409FuncaoAPFEvidencia_NomeArq[0];
            A410FuncaoAPFEvidencia_TipoArq = T001O6_A410FuncaoAPFEvidencia_TipoArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A410FuncaoAPFEvidencia_TipoArq", A410FuncaoAPFEvidencia_TipoArq);
            n410FuncaoAPFEvidencia_TipoArq = T001O6_n410FuncaoAPFEvidencia_TipoArq[0];
            A166FuncaoAPF_Nome = T001O6_A166FuncaoAPF_Nome[0];
            A407FuncaoAPFEvidencia_Descricao = T001O6_A407FuncaoAPFEvidencia_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A407FuncaoAPFEvidencia_Descricao", A407FuncaoAPFEvidencia_Descricao);
            n407FuncaoAPFEvidencia_Descricao = T001O6_n407FuncaoAPFEvidencia_Descricao[0];
            A750FuncaoAPFEvidencia_Ativo = T001O6_A750FuncaoAPFEvidencia_Ativo[0];
            n750FuncaoAPFEvidencia_Ativo = T001O6_n750FuncaoAPFEvidencia_Ativo[0];
            A749FuncaoAPFEvidencia_MelhoraCod = T001O6_A749FuncaoAPFEvidencia_MelhoraCod[0];
            n749FuncaoAPFEvidencia_MelhoraCod = T001O6_n749FuncaoAPFEvidencia_MelhoraCod[0];
            A360FuncaoAPF_SistemaCod = T001O6_A360FuncaoAPF_SistemaCod[0];
            n360FuncaoAPF_SistemaCod = T001O6_n360FuncaoAPF_SistemaCod[0];
            A408FuncaoAPFEvidencia_Arquivo = T001O6_A408FuncaoAPFEvidencia_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A408FuncaoAPFEvidencia_Arquivo", A408FuncaoAPFEvidencia_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A408FuncaoAPFEvidencia_Arquivo));
            n408FuncaoAPFEvidencia_Arquivo = T001O6_n408FuncaoAPFEvidencia_Arquivo[0];
            ZM1O63( -17) ;
         }
         pr_default.close(4);
         OnLoadActions1O63( ) ;
      }

      protected void OnLoadActions1O63( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_FuncaoAPF_Codigo) )
         {
            A165FuncaoAPF_Codigo = AV11Insert_FuncaoAPF_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         }
         imgCleararquivoanexo_Visible = (!String.IsNullOrEmpty(StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleararquivoanexo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgCleararquivoanexo_Visible), 5, 0)));
      }

      protected void CheckExtendedTable1O63( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_FuncaoAPF_Codigo) )
         {
            A165FuncaoAPF_Codigo = AV11Insert_FuncaoAPF_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         }
         imgCleararquivoanexo_Visible = (!String.IsNullOrEmpty(StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleararquivoanexo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgCleararquivoanexo_Visible), 5, 0)));
         /* Using cursor T001O5 */
         pr_default.execute(3, new Object[] {n749FuncaoAPFEvidencia_MelhoraCod, A749FuncaoAPFEvidencia_MelhoraCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A749FuncaoAPFEvidencia_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APFEvidencia_Funcao APFEvoidencia Melhora'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1O63( )
      {
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_19( int A749FuncaoAPFEvidencia_MelhoraCod )
      {
         /* Using cursor T001O7 */
         pr_default.execute(5, new Object[] {n749FuncaoAPFEvidencia_MelhoraCod, A749FuncaoAPFEvidencia_MelhoraCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A749FuncaoAPFEvidencia_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APFEvidencia_Funcao APFEvoidencia Melhora'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void GetKey1O63( )
      {
         /* Using cursor T001O8 */
         pr_default.execute(6, new Object[] {A406FuncaoAPFEvidencia_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound63 = 1;
         }
         else
         {
            RcdFound63 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001O3 */
         pr_default.execute(1, new Object[] {A406FuncaoAPFEvidencia_Codigo});
         if ( (pr_default.getStatus(1) != 101) && ( T001O3_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) )
         {
            ZM1O63( 17) ;
            RcdFound63 = 1;
            A406FuncaoAPFEvidencia_Codigo = T001O3_A406FuncaoAPFEvidencia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
            A411FuncaoAPFEvidencia_Data = T001O3_A411FuncaoAPFEvidencia_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A411FuncaoAPFEvidencia_Data", context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
            A409FuncaoAPFEvidencia_NomeArq = T001O3_A409FuncaoAPFEvidencia_NomeArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A409FuncaoAPFEvidencia_NomeArq", A409FuncaoAPFEvidencia_NomeArq);
            n409FuncaoAPFEvidencia_NomeArq = T001O3_n409FuncaoAPFEvidencia_NomeArq[0];
            A410FuncaoAPFEvidencia_TipoArq = T001O3_A410FuncaoAPFEvidencia_TipoArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A410FuncaoAPFEvidencia_TipoArq", A410FuncaoAPFEvidencia_TipoArq);
            n410FuncaoAPFEvidencia_TipoArq = T001O3_n410FuncaoAPFEvidencia_TipoArq[0];
            A407FuncaoAPFEvidencia_Descricao = T001O3_A407FuncaoAPFEvidencia_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A407FuncaoAPFEvidencia_Descricao", A407FuncaoAPFEvidencia_Descricao);
            n407FuncaoAPFEvidencia_Descricao = T001O3_n407FuncaoAPFEvidencia_Descricao[0];
            A750FuncaoAPFEvidencia_Ativo = T001O3_A750FuncaoAPFEvidencia_Ativo[0];
            n750FuncaoAPFEvidencia_Ativo = T001O3_n750FuncaoAPFEvidencia_Ativo[0];
            A749FuncaoAPFEvidencia_MelhoraCod = T001O3_A749FuncaoAPFEvidencia_MelhoraCod[0];
            n749FuncaoAPFEvidencia_MelhoraCod = T001O3_n749FuncaoAPFEvidencia_MelhoraCod[0];
            A408FuncaoAPFEvidencia_Arquivo = T001O3_A408FuncaoAPFEvidencia_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A408FuncaoAPFEvidencia_Arquivo", A408FuncaoAPFEvidencia_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A408FuncaoAPFEvidencia_Arquivo));
            n408FuncaoAPFEvidencia_Arquivo = T001O3_n408FuncaoAPFEvidencia_Arquivo[0];
            O410FuncaoAPFEvidencia_TipoArq = A410FuncaoAPFEvidencia_TipoArq;
            n410FuncaoAPFEvidencia_TipoArq = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A410FuncaoAPFEvidencia_TipoArq", A410FuncaoAPFEvidencia_TipoArq);
            O409FuncaoAPFEvidencia_NomeArq = A409FuncaoAPFEvidencia_NomeArq;
            n409FuncaoAPFEvidencia_NomeArq = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A409FuncaoAPFEvidencia_NomeArq", A409FuncaoAPFEvidencia_NomeArq);
            O408FuncaoAPFEvidencia_Arquivo = A408FuncaoAPFEvidencia_Arquivo;
            n408FuncaoAPFEvidencia_Arquivo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A408FuncaoAPFEvidencia_Arquivo", A408FuncaoAPFEvidencia_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A408FuncaoAPFEvidencia_Arquivo));
            O407FuncaoAPFEvidencia_Descricao = A407FuncaoAPFEvidencia_Descricao;
            n407FuncaoAPFEvidencia_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A407FuncaoAPFEvidencia_Descricao", A407FuncaoAPFEvidencia_Descricao);
            Z406FuncaoAPFEvidencia_Codigo = A406FuncaoAPFEvidencia_Codigo;
            sMode63 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1O63( ) ;
            if ( AnyError == 1 )
            {
               RcdFound63 = 0;
               InitializeNonKey1O63( ) ;
            }
            Gx_mode = sMode63;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound63 = 0;
            InitializeNonKey1O63( ) ;
            sMode63 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode63;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1O63( ) ;
         if ( RcdFound63 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound63 = 0;
         /* Using cursor T001O9 */
         pr_default.execute(7, new Object[] {A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T001O9_A406FuncaoAPFEvidencia_Codigo[0] < A406FuncaoAPFEvidencia_Codigo ) ) && ( T001O9_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T001O9_A406FuncaoAPFEvidencia_Codigo[0] > A406FuncaoAPFEvidencia_Codigo ) ) && ( T001O9_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) )
            {
               A406FuncaoAPFEvidencia_Codigo = T001O9_A406FuncaoAPFEvidencia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
               RcdFound63 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void move_previous( )
      {
         RcdFound63 = 0;
         /* Using cursor T001O10 */
         pr_default.execute(8, new Object[] {A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T001O10_A406FuncaoAPFEvidencia_Codigo[0] > A406FuncaoAPFEvidencia_Codigo ) ) && ( T001O10_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T001O10_A406FuncaoAPFEvidencia_Codigo[0] < A406FuncaoAPFEvidencia_Codigo ) ) && ( T001O10_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) )
            {
               A406FuncaoAPFEvidencia_Codigo = T001O10_A406FuncaoAPFEvidencia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
               RcdFound63 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1O63( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtFuncaoAPFEvidencia_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1O63( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound63 == 1 )
            {
               if ( A406FuncaoAPFEvidencia_Codigo != Z406FuncaoAPFEvidencia_Codigo )
               {
                  A406FuncaoAPFEvidencia_Codigo = Z406FuncaoAPFEvidencia_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "FUNCAOAPFEVIDENCIA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoAPFEvidencia_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtFuncaoAPFEvidencia_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1O63( ) ;
                  GX_FocusControl = edtFuncaoAPFEvidencia_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A406FuncaoAPFEvidencia_Codigo != Z406FuncaoAPFEvidencia_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtFuncaoAPFEvidencia_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1O63( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "FUNCAOAPFEVIDENCIA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtFuncaoAPFEvidencia_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtFuncaoAPFEvidencia_Descricao_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1O63( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A406FuncaoAPFEvidencia_Codigo != Z406FuncaoAPFEvidencia_Codigo )
         {
            A406FuncaoAPFEvidencia_Codigo = Z406FuncaoAPFEvidencia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "FUNCAOAPFEVIDENCIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPFEvidencia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtFuncaoAPFEvidencia_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1O63( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001O2 */
            pr_default.execute(0, new Object[] {A406FuncaoAPFEvidencia_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncaoAPFEvidencia"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z411FuncaoAPFEvidencia_Data != T001O2_A411FuncaoAPFEvidencia_Data[0] ) || ( StringUtil.StrCmp(Z409FuncaoAPFEvidencia_NomeArq, T001O2_A409FuncaoAPFEvidencia_NomeArq[0]) != 0 ) || ( StringUtil.StrCmp(Z410FuncaoAPFEvidencia_TipoArq, T001O2_A410FuncaoAPFEvidencia_TipoArq[0]) != 0 ) || ( Z750FuncaoAPFEvidencia_Ativo != T001O2_A750FuncaoAPFEvidencia_Ativo[0] ) || ( Z749FuncaoAPFEvidencia_MelhoraCod != T001O2_A749FuncaoAPFEvidencia_MelhoraCod[0] ) )
            {
               if ( Z411FuncaoAPFEvidencia_Data != T001O2_A411FuncaoAPFEvidencia_Data[0] )
               {
                  GXUtil.WriteLog("funcaoapfevidencia:[seudo value changed for attri]"+"FuncaoAPFEvidencia_Data");
                  GXUtil.WriteLogRaw("Old: ",Z411FuncaoAPFEvidencia_Data);
                  GXUtil.WriteLogRaw("Current: ",T001O2_A411FuncaoAPFEvidencia_Data[0]);
               }
               if ( StringUtil.StrCmp(Z409FuncaoAPFEvidencia_NomeArq, T001O2_A409FuncaoAPFEvidencia_NomeArq[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaoapfevidencia:[seudo value changed for attri]"+"FuncaoAPFEvidencia_NomeArq");
                  GXUtil.WriteLogRaw("Old: ",Z409FuncaoAPFEvidencia_NomeArq);
                  GXUtil.WriteLogRaw("Current: ",T001O2_A409FuncaoAPFEvidencia_NomeArq[0]);
               }
               if ( StringUtil.StrCmp(Z410FuncaoAPFEvidencia_TipoArq, T001O2_A410FuncaoAPFEvidencia_TipoArq[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaoapfevidencia:[seudo value changed for attri]"+"FuncaoAPFEvidencia_TipoArq");
                  GXUtil.WriteLogRaw("Old: ",Z410FuncaoAPFEvidencia_TipoArq);
                  GXUtil.WriteLogRaw("Current: ",T001O2_A410FuncaoAPFEvidencia_TipoArq[0]);
               }
               if ( Z750FuncaoAPFEvidencia_Ativo != T001O2_A750FuncaoAPFEvidencia_Ativo[0] )
               {
                  GXUtil.WriteLog("funcaoapfevidencia:[seudo value changed for attri]"+"FuncaoAPFEvidencia_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z750FuncaoAPFEvidencia_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T001O2_A750FuncaoAPFEvidencia_Ativo[0]);
               }
               if ( Z749FuncaoAPFEvidencia_MelhoraCod != T001O2_A749FuncaoAPFEvidencia_MelhoraCod[0] )
               {
                  GXUtil.WriteLog("funcaoapfevidencia:[seudo value changed for attri]"+"FuncaoAPFEvidencia_MelhoraCod");
                  GXUtil.WriteLogRaw("Old: ",Z749FuncaoAPFEvidencia_MelhoraCod);
                  GXUtil.WriteLogRaw("Current: ",T001O2_A749FuncaoAPFEvidencia_MelhoraCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"FuncaoAPFEvidencia"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1O63( )
      {
         BeforeValidate1O63( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1O63( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1O63( 0) ;
            CheckOptimisticConcurrency1O63( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1O63( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1O63( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001O11 */
                     pr_default.execute(9, new Object[] {A165FuncaoAPF_Codigo, A411FuncaoAPFEvidencia_Data, n409FuncaoAPFEvidencia_NomeArq, A409FuncaoAPFEvidencia_NomeArq, n410FuncaoAPFEvidencia_TipoArq, A410FuncaoAPFEvidencia_TipoArq, n407FuncaoAPFEvidencia_Descricao, A407FuncaoAPFEvidencia_Descricao, n408FuncaoAPFEvidencia_Arquivo, A408FuncaoAPFEvidencia_Arquivo, n750FuncaoAPFEvidencia_Ativo, A750FuncaoAPFEvidencia_Ativo, n749FuncaoAPFEvidencia_MelhoraCod, A749FuncaoAPFEvidencia_MelhoraCod});
                     A406FuncaoAPFEvidencia_Codigo = T001O11_A406FuncaoAPFEvidencia_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFEvidencia") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1O0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1O63( ) ;
            }
            EndLevel1O63( ) ;
         }
         CloseExtendedTableCursors1O63( ) ;
      }

      protected void Update1O63( )
      {
         BeforeValidate1O63( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1O63( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1O63( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1O63( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1O63( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001O12 */
                     pr_default.execute(10, new Object[] {A165FuncaoAPF_Codigo, A411FuncaoAPFEvidencia_Data, n409FuncaoAPFEvidencia_NomeArq, A409FuncaoAPFEvidencia_NomeArq, n410FuncaoAPFEvidencia_TipoArq, A410FuncaoAPFEvidencia_TipoArq, n407FuncaoAPFEvidencia_Descricao, A407FuncaoAPFEvidencia_Descricao, n750FuncaoAPFEvidencia_Ativo, A750FuncaoAPFEvidencia_Ativo, n749FuncaoAPFEvidencia_MelhoraCod, A749FuncaoAPFEvidencia_MelhoraCod, A406FuncaoAPFEvidencia_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFEvidencia") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncaoAPFEvidencia"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1O63( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1O63( ) ;
         }
         CloseExtendedTableCursors1O63( ) ;
      }

      protected void DeferredUpdate1O63( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T001O13 */
            pr_default.execute(11, new Object[] {n408FuncaoAPFEvidencia_Arquivo, A408FuncaoAPFEvidencia_Arquivo, A406FuncaoAPFEvidencia_Codigo});
            pr_default.close(11);
            dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFEvidencia") ;
         }
      }

      protected void delete( )
      {
         BeforeValidate1O63( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1O63( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1O63( ) ;
            AfterConfirm1O63( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1O63( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001O14 */
                  pr_default.execute(12, new Object[] {A406FuncaoAPFEvidencia_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFEvidencia") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode63 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1O63( ) ;
         Gx_mode = sMode63;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1O63( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            imgCleararquivoanexo_Visible = (!String.IsNullOrEmpty(StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleararquivoanexo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgCleararquivoanexo_Visible), 5, 0)));
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T001O15 */
            pr_default.execute(13, new Object[] {A406FuncaoAPFEvidencia_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Evidencia da Funcao de Transa��o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel1O63( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1O63( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "FuncaoAPFEvidencia");
            if ( AnyError == 0 )
            {
               ConfirmValues1O0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "FuncaoAPFEvidencia");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1O63( )
      {
         /* Scan By routine */
         /* Using cursor T001O16 */
         pr_default.execute(14, new Object[] {A165FuncaoAPF_Codigo});
         RcdFound63 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound63 = 1;
            A406FuncaoAPFEvidencia_Codigo = T001O16_A406FuncaoAPFEvidencia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1O63( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound63 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound63 = 1;
            A406FuncaoAPFEvidencia_Codigo = T001O16_A406FuncaoAPFEvidencia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1O63( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm1O63( )
      {
         /* After Confirm Rules */
         if ( ( StringUtil.StrCmp(A407FuncaoAPFEvidencia_Descricao, O407FuncaoAPFEvidencia_Descricao) != 0 ) || ( StringUtil.StrCmp(A408FuncaoAPFEvidencia_Arquivo, O408FuncaoAPFEvidencia_Arquivo) != 0 ) )
         {
            A411FuncaoAPFEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A411FuncaoAPFEvidencia_Data", context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( ( StringUtil.StrCmp(A410FuncaoAPFEvidencia_TipoArq, O410FuncaoAPFEvidencia_TipoArq) == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo)) )
         {
            A410FuncaoAPFEvidencia_TipoArq = edtFuncaoAPFEvidencia_Arquivo_Filetype;
            n410FuncaoAPFEvidencia_TipoArq = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A410FuncaoAPFEvidencia_TipoArq", A410FuncaoAPFEvidencia_TipoArq);
         }
         if ( ( StringUtil.StrCmp(A409FuncaoAPFEvidencia_NomeArq, O409FuncaoAPFEvidencia_NomeArq) == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo)) )
         {
            A409FuncaoAPFEvidencia_NomeArq = edtFuncaoAPFEvidencia_Arquivo_Filename;
            n409FuncaoAPFEvidencia_NomeArq = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A409FuncaoAPFEvidencia_NomeArq", A409FuncaoAPFEvidencia_NomeArq);
         }
      }

      protected void BeforeInsert1O63( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1O63( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1O63( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1O63( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1O63( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1O63( )
      {
         edtFuncaoAPFEvidencia_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFEvidencia_Descricao_Enabled), 5, 0)));
         edtFuncaoAPFEvidencia_Arquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFEvidencia_Arquivo_Enabled), 5, 0)));
         edtFuncaoAPFEvidencia_NomeArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_NomeArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFEvidencia_NomeArq_Enabled), 5, 0)));
         edtFuncaoAPFEvidencia_TipoArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_TipoArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFEvidencia_TipoArq_Enabled), 5, 0)));
         edtFuncaoAPFEvidencia_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFEvidencia_Data_Enabled), 5, 0)));
         edtFuncaoAPFEvidencia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFEvidencia_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1O0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205322401924");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaoapfevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7FuncaoAPFEvidencia_Codigo) + "," + UrlEncode("" +A165FuncaoAPF_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z406FuncaoAPFEvidencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z411FuncaoAPFEvidencia_Data", context.localUtil.TToC( Z411FuncaoAPFEvidencia_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z409FuncaoAPFEvidencia_NomeArq", StringUtil.RTrim( Z409FuncaoAPFEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, "Z410FuncaoAPFEvidencia_TipoArq", StringUtil.RTrim( Z410FuncaoAPFEvidencia_TipoArq));
         GxWebStd.gx_boolean_hidden_field( context, "Z750FuncaoAPFEvidencia_Ativo", Z750FuncaoAPFEvidencia_Ativo);
         GxWebStd.gx_hidden_field( context, "Z749FuncaoAPFEvidencia_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z749FuncaoAPFEvidencia_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O410FuncaoAPFEvidencia_TipoArq", StringUtil.RTrim( O410FuncaoAPFEvidencia_TipoArq));
         GxWebStd.gx_hidden_field( context, "O409FuncaoAPFEvidencia_NomeArq", StringUtil.RTrim( O409FuncaoAPFEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, "O408FuncaoAPFEvidencia_Arquivo", O408FuncaoAPFEvidencia_Arquivo);
         GxWebStd.gx_hidden_field( context, "O407FuncaoAPFEvidencia_Descricao", O407FuncaoAPFEvidencia_Descricao);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N749FuncaoAPFEvidencia_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A749FuncaoAPFEvidencia_MelhoraCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vFUNCAOAPFEVIDENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoAPFEvidencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_FUNCAOAPFEVIDENCIA_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_FuncaoAPFEvidencia_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFEVIDENCIA_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A749FuncaoAPFEvidencia_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_NOME", A166FuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "FUNCAOAPFEVIDENCIA_ATIVO", A750FuncaoAPFEvidencia_Ativo);
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV15Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vFUNCAOAPFEVIDENCIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoAPFEvidencia_Codigo), "ZZZZZ9")));
         GXCCtlgxBlob = "FUNCAOAPFEVIDENCIA_ARQUIVO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A408FuncaoAPFEvidencia_Arquivo);
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFEVIDENCIA_ARQUIVO_Filename", StringUtil.RTrim( edtFuncaoAPFEvidencia_Arquivo_Filename));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFEVIDENCIA_ARQUIVO_Filetype", StringUtil.RTrim( edtFuncaoAPFEvidencia_Arquivo_Filetype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "FuncaoAPFEvidencia";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A406FuncaoAPFEvidencia_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A749FuncaoAPFEvidencia_MelhoraCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A750FuncaoAPFEvidencia_Ativo);
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("funcaoapfevidencia:[SendSecurityCheck value for]"+"FuncaoAPFEvidencia_Codigo:"+context.localUtil.Format( (decimal)(A406FuncaoAPFEvidencia_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("funcaoapfevidencia:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("funcaoapfevidencia:[SendSecurityCheck value for]"+"FuncaoAPFEvidencia_MelhoraCod:"+context.localUtil.Format( (decimal)(A749FuncaoAPFEvidencia_MelhoraCod), "ZZZZZ9"));
         GXUtil.WriteLog("funcaoapfevidencia:[SendSecurityCheck value for]"+"FuncaoAPFEvidencia_Ativo:"+StringUtil.BoolToStr( A750FuncaoAPFEvidencia_Ativo));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("funcaoapfevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7FuncaoAPFEvidencia_Codigo) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "FuncaoAPFEvidencia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Evidencia da Funcao de Transa��o" ;
      }

      protected void InitializeNonKey1O63( )
      {
         A749FuncaoAPFEvidencia_MelhoraCod = 0;
         n749FuncaoAPFEvidencia_MelhoraCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A749FuncaoAPFEvidencia_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A749FuncaoAPFEvidencia_MelhoraCod), 6, 0)));
         A409FuncaoAPFEvidencia_NomeArq = "";
         n409FuncaoAPFEvidencia_NomeArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A409FuncaoAPFEvidencia_NomeArq", A409FuncaoAPFEvidencia_NomeArq);
         n409FuncaoAPFEvidencia_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq)) ? true : false);
         A410FuncaoAPFEvidencia_TipoArq = "";
         n410FuncaoAPFEvidencia_TipoArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A410FuncaoAPFEvidencia_TipoArq", A410FuncaoAPFEvidencia_TipoArq);
         n410FuncaoAPFEvidencia_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq)) ? true : false);
         A407FuncaoAPFEvidencia_Descricao = "";
         n407FuncaoAPFEvidencia_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A407FuncaoAPFEvidencia_Descricao", A407FuncaoAPFEvidencia_Descricao);
         n407FuncaoAPFEvidencia_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A407FuncaoAPFEvidencia_Descricao)) ? true : false);
         A408FuncaoAPFEvidencia_Arquivo = "";
         n408FuncaoAPFEvidencia_Arquivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A408FuncaoAPFEvidencia_Arquivo", A408FuncaoAPFEvidencia_Arquivo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A408FuncaoAPFEvidencia_Arquivo));
         n408FuncaoAPFEvidencia_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo)) ? true : false);
         A411FuncaoAPFEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A411FuncaoAPFEvidencia_Data", context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
         A750FuncaoAPFEvidencia_Ativo = true;
         n750FuncaoAPFEvidencia_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A750FuncaoAPFEvidencia_Ativo", A750FuncaoAPFEvidencia_Ativo);
         O410FuncaoAPFEvidencia_TipoArq = A410FuncaoAPFEvidencia_TipoArq;
         n410FuncaoAPFEvidencia_TipoArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A410FuncaoAPFEvidencia_TipoArq", A410FuncaoAPFEvidencia_TipoArq);
         O409FuncaoAPFEvidencia_NomeArq = A409FuncaoAPFEvidencia_NomeArq;
         n409FuncaoAPFEvidencia_NomeArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A409FuncaoAPFEvidencia_NomeArq", A409FuncaoAPFEvidencia_NomeArq);
         O408FuncaoAPFEvidencia_Arquivo = A408FuncaoAPFEvidencia_Arquivo;
         n408FuncaoAPFEvidencia_Arquivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A408FuncaoAPFEvidencia_Arquivo", A408FuncaoAPFEvidencia_Arquivo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A408FuncaoAPFEvidencia_Arquivo));
         O407FuncaoAPFEvidencia_Descricao = A407FuncaoAPFEvidencia_Descricao;
         n407FuncaoAPFEvidencia_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A407FuncaoAPFEvidencia_Descricao", A407FuncaoAPFEvidencia_Descricao);
         Z411FuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
         Z409FuncaoAPFEvidencia_NomeArq = "";
         Z410FuncaoAPFEvidencia_TipoArq = "";
         Z750FuncaoAPFEvidencia_Ativo = false;
         Z749FuncaoAPFEvidencia_MelhoraCod = 0;
      }

      protected void InitAll1O63( )
      {
         A406FuncaoAPFEvidencia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
         InitializeNonKey1O63( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A411FuncaoAPFEvidencia_Data = i411FuncaoAPFEvidencia_Data;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A411FuncaoAPFEvidencia_Data", context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
         A750FuncaoAPFEvidencia_Ativo = i750FuncaoAPFEvidencia_Ativo;
         n750FuncaoAPFEvidencia_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A750FuncaoAPFEvidencia_Ativo", A750FuncaoAPFEvidencia_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205322401949");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("funcaoapfevidencia.js", "?20205322401950");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockfuncaoapfevidencia_descricao_Internalname = "TEXTBLOCKFUNCAOAPFEVIDENCIA_DESCRICAO";
         edtFuncaoAPFEvidencia_Descricao_Internalname = "FUNCAOAPFEVIDENCIA_DESCRICAO";
         lblTextblockfuncaoapfevidencia_arquivo_Internalname = "TEXTBLOCKFUNCAOAPFEVIDENCIA_ARQUIVO";
         edtFuncaoAPFEvidencia_Arquivo_Internalname = "FUNCAOAPFEVIDENCIA_ARQUIVO";
         imgCleararquivoanexo_Internalname = "CLEARARQUIVOANEXO";
         tblTablemergedfuncaoapfevidencia_arquivo_Internalname = "TABLEMERGEDFUNCAOAPFEVIDENCIA_ARQUIVO";
         lblTextblockfuncaoapfevidencia_nomearq_Internalname = "TEXTBLOCKFUNCAOAPFEVIDENCIA_NOMEARQ";
         edtFuncaoAPFEvidencia_NomeArq_Internalname = "FUNCAOAPFEVIDENCIA_NOMEARQ";
         lblTextblockfuncaoapfevidencia_tipoarq_Internalname = "TEXTBLOCKFUNCAOAPFEVIDENCIA_TIPOARQ";
         edtFuncaoAPFEvidencia_TipoArq_Internalname = "FUNCAOAPFEVIDENCIA_TIPOARQ";
         lblTextblockfuncaoapfevidencia_data_Internalname = "TEXTBLOCKFUNCAOAPFEVIDENCIA_DATA";
         edtFuncaoAPFEvidencia_Data_Internalname = "FUNCAOAPFEVIDENCIA_DATA";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtFuncaoAPFEvidencia_Codigo_Internalname = "FUNCAOAPFEVIDENCIA_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Evidencia da Funcao de Transa��o";
         edtFuncaoAPFEvidencia_Arquivo_Filename = "";
         Dvpanel_tableattributes_Title = "Evid�ncia";
         imgCleararquivoanexo_Visible = 1;
         edtFuncaoAPFEvidencia_Arquivo_Jsonclick = "";
         edtFuncaoAPFEvidencia_Arquivo_Parameters = "";
         edtFuncaoAPFEvidencia_Arquivo_Contenttype = "";
         edtFuncaoAPFEvidencia_Arquivo_Filetype = "";
         edtFuncaoAPFEvidencia_Arquivo_Enabled = 1;
         edtFuncaoAPFEvidencia_Data_Jsonclick = "";
         edtFuncaoAPFEvidencia_Data_Enabled = 0;
         edtFuncaoAPFEvidencia_TipoArq_Jsonclick = "";
         edtFuncaoAPFEvidencia_TipoArq_Enabled = 1;
         edtFuncaoAPFEvidencia_NomeArq_Jsonclick = "";
         edtFuncaoAPFEvidencia_NomeArq_Enabled = 1;
         edtFuncaoAPFEvidencia_Descricao_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtFuncaoAPFEvidencia_Codigo_Jsonclick = "";
         edtFuncaoAPFEvidencia_Codigo_Enabled = 0;
         edtFuncaoAPFEvidencia_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7FuncaoAPFEvidencia_Codigo',fld:'vFUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121O2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOCLEARARQUIVOANEXO'","{handler:'E131O2',iparms:[{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z411FuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
         Z409FuncaoAPFEvidencia_NomeArq = "";
         Z410FuncaoAPFEvidencia_TipoArq = "";
         O410FuncaoAPFEvidencia_TipoArq = "";
         O409FuncaoAPFEvidencia_NomeArq = "";
         O408FuncaoAPFEvidencia_Arquivo = "";
         O407FuncaoAPFEvidencia_Descricao = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockfuncaoapfevidencia_descricao_Jsonclick = "";
         A407FuncaoAPFEvidencia_Descricao = "";
         lblTextblockfuncaoapfevidencia_arquivo_Jsonclick = "";
         lblTextblockfuncaoapfevidencia_nomearq_Jsonclick = "";
         A409FuncaoAPFEvidencia_NomeArq = "";
         lblTextblockfuncaoapfevidencia_tipoarq_Jsonclick = "";
         A410FuncaoAPFEvidencia_TipoArq = "";
         lblTextblockfuncaoapfevidencia_data_Jsonclick = "";
         A411FuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         A408FuncaoAPFEvidencia_Arquivo = "";
         imgCleararquivoanexo_Jsonclick = "";
         A166FuncaoAPF_Nome = "";
         AV15Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         GXCCtlgxBlob = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode63 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z407FuncaoAPFEvidencia_Descricao = "";
         Z408FuncaoAPFEvidencia_Arquivo = "";
         Z166FuncaoAPF_Nome = "";
         T001O4_A166FuncaoAPF_Nome = new String[] {""} ;
         T001O4_A360FuncaoAPF_SistemaCod = new int[1] ;
         T001O4_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         T001O6_A165FuncaoAPF_Codigo = new int[1] ;
         T001O6_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         T001O6_A411FuncaoAPFEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         T001O6_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         T001O6_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         T001O6_A410FuncaoAPFEvidencia_TipoArq = new String[] {""} ;
         T001O6_n410FuncaoAPFEvidencia_TipoArq = new bool[] {false} ;
         T001O6_A166FuncaoAPF_Nome = new String[] {""} ;
         T001O6_A407FuncaoAPFEvidencia_Descricao = new String[] {""} ;
         T001O6_n407FuncaoAPFEvidencia_Descricao = new bool[] {false} ;
         T001O6_A750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         T001O6_n750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         T001O6_A749FuncaoAPFEvidencia_MelhoraCod = new int[1] ;
         T001O6_n749FuncaoAPFEvidencia_MelhoraCod = new bool[] {false} ;
         T001O6_A360FuncaoAPF_SistemaCod = new int[1] ;
         T001O6_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         T001O6_A408FuncaoAPFEvidencia_Arquivo = new String[] {""} ;
         T001O6_n408FuncaoAPFEvidencia_Arquivo = new bool[] {false} ;
         T001O5_A749FuncaoAPFEvidencia_MelhoraCod = new int[1] ;
         T001O5_n749FuncaoAPFEvidencia_MelhoraCod = new bool[] {false} ;
         T001O7_A749FuncaoAPFEvidencia_MelhoraCod = new int[1] ;
         T001O7_n749FuncaoAPFEvidencia_MelhoraCod = new bool[] {false} ;
         T001O8_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         T001O3_A165FuncaoAPF_Codigo = new int[1] ;
         T001O3_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         T001O3_A411FuncaoAPFEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         T001O3_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         T001O3_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         T001O3_A410FuncaoAPFEvidencia_TipoArq = new String[] {""} ;
         T001O3_n410FuncaoAPFEvidencia_TipoArq = new bool[] {false} ;
         T001O3_A407FuncaoAPFEvidencia_Descricao = new String[] {""} ;
         T001O3_n407FuncaoAPFEvidencia_Descricao = new bool[] {false} ;
         T001O3_A750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         T001O3_n750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         T001O3_A749FuncaoAPFEvidencia_MelhoraCod = new int[1] ;
         T001O3_n749FuncaoAPFEvidencia_MelhoraCod = new bool[] {false} ;
         T001O3_A408FuncaoAPFEvidencia_Arquivo = new String[] {""} ;
         T001O3_n408FuncaoAPFEvidencia_Arquivo = new bool[] {false} ;
         T001O9_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         T001O9_A165FuncaoAPF_Codigo = new int[1] ;
         T001O10_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         T001O10_A165FuncaoAPF_Codigo = new int[1] ;
         T001O2_A165FuncaoAPF_Codigo = new int[1] ;
         T001O2_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         T001O2_A411FuncaoAPFEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         T001O2_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         T001O2_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         T001O2_A410FuncaoAPFEvidencia_TipoArq = new String[] {""} ;
         T001O2_n410FuncaoAPFEvidencia_TipoArq = new bool[] {false} ;
         T001O2_A407FuncaoAPFEvidencia_Descricao = new String[] {""} ;
         T001O2_n407FuncaoAPFEvidencia_Descricao = new bool[] {false} ;
         T001O2_A750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         T001O2_n750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         T001O2_A749FuncaoAPFEvidencia_MelhoraCod = new int[1] ;
         T001O2_n749FuncaoAPFEvidencia_MelhoraCod = new bool[] {false} ;
         T001O2_A408FuncaoAPFEvidencia_Arquivo = new String[] {""} ;
         T001O2_n408FuncaoAPFEvidencia_Arquivo = new bool[] {false} ;
         T001O11_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         T001O15_A749FuncaoAPFEvidencia_MelhoraCod = new int[1] ;
         T001O15_n749FuncaoAPFEvidencia_MelhoraCod = new bool[] {false} ;
         T001O16_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i411FuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaoapfevidencia__default(),
            new Object[][] {
                new Object[] {
               T001O2_A165FuncaoAPF_Codigo, T001O2_A406FuncaoAPFEvidencia_Codigo, T001O2_A411FuncaoAPFEvidencia_Data, T001O2_A409FuncaoAPFEvidencia_NomeArq, T001O2_n409FuncaoAPFEvidencia_NomeArq, T001O2_A410FuncaoAPFEvidencia_TipoArq, T001O2_n410FuncaoAPFEvidencia_TipoArq, T001O2_A407FuncaoAPFEvidencia_Descricao, T001O2_n407FuncaoAPFEvidencia_Descricao, T001O2_A750FuncaoAPFEvidencia_Ativo,
               T001O2_n750FuncaoAPFEvidencia_Ativo, T001O2_A749FuncaoAPFEvidencia_MelhoraCod, T001O2_n749FuncaoAPFEvidencia_MelhoraCod, T001O2_A408FuncaoAPFEvidencia_Arquivo, T001O2_n408FuncaoAPFEvidencia_Arquivo
               }
               , new Object[] {
               T001O3_A165FuncaoAPF_Codigo, T001O3_A406FuncaoAPFEvidencia_Codigo, T001O3_A411FuncaoAPFEvidencia_Data, T001O3_A409FuncaoAPFEvidencia_NomeArq, T001O3_n409FuncaoAPFEvidencia_NomeArq, T001O3_A410FuncaoAPFEvidencia_TipoArq, T001O3_n410FuncaoAPFEvidencia_TipoArq, T001O3_A407FuncaoAPFEvidencia_Descricao, T001O3_n407FuncaoAPFEvidencia_Descricao, T001O3_A750FuncaoAPFEvidencia_Ativo,
               T001O3_n750FuncaoAPFEvidencia_Ativo, T001O3_A749FuncaoAPFEvidencia_MelhoraCod, T001O3_n749FuncaoAPFEvidencia_MelhoraCod, T001O3_A408FuncaoAPFEvidencia_Arquivo, T001O3_n408FuncaoAPFEvidencia_Arquivo
               }
               , new Object[] {
               T001O4_A166FuncaoAPF_Nome, T001O4_A360FuncaoAPF_SistemaCod, T001O4_n360FuncaoAPF_SistemaCod
               }
               , new Object[] {
               T001O5_A749FuncaoAPFEvidencia_MelhoraCod
               }
               , new Object[] {
               T001O6_A165FuncaoAPF_Codigo, T001O6_A406FuncaoAPFEvidencia_Codigo, T001O6_A411FuncaoAPFEvidencia_Data, T001O6_A409FuncaoAPFEvidencia_NomeArq, T001O6_n409FuncaoAPFEvidencia_NomeArq, T001O6_A410FuncaoAPFEvidencia_TipoArq, T001O6_n410FuncaoAPFEvidencia_TipoArq, T001O6_A166FuncaoAPF_Nome, T001O6_A407FuncaoAPFEvidencia_Descricao, T001O6_n407FuncaoAPFEvidencia_Descricao,
               T001O6_A750FuncaoAPFEvidencia_Ativo, T001O6_n750FuncaoAPFEvidencia_Ativo, T001O6_A749FuncaoAPFEvidencia_MelhoraCod, T001O6_n749FuncaoAPFEvidencia_MelhoraCod, T001O6_A360FuncaoAPF_SistemaCod, T001O6_n360FuncaoAPF_SistemaCod, T001O6_A408FuncaoAPFEvidencia_Arquivo, T001O6_n408FuncaoAPFEvidencia_Arquivo
               }
               , new Object[] {
               T001O7_A749FuncaoAPFEvidencia_MelhoraCod
               }
               , new Object[] {
               T001O8_A406FuncaoAPFEvidencia_Codigo
               }
               , new Object[] {
               T001O9_A406FuncaoAPFEvidencia_Codigo, T001O9_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               T001O10_A406FuncaoAPFEvidencia_Codigo, T001O10_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               T001O11_A406FuncaoAPFEvidencia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001O15_A749FuncaoAPFEvidencia_MelhoraCod
               }
               , new Object[] {
               T001O16_A406FuncaoAPFEvidencia_Codigo
               }
            }
         );
         N165FuncaoAPF_Codigo = 0;
         Z165FuncaoAPF_Codigo = 0;
         A165FuncaoAPF_Codigo = 0;
         Z750FuncaoAPFEvidencia_Ativo = true;
         n750FuncaoAPFEvidencia_Ativo = false;
         A750FuncaoAPFEvidencia_Ativo = true;
         n750FuncaoAPFEvidencia_Ativo = false;
         i750FuncaoAPFEvidencia_Ativo = true;
         n750FuncaoAPFEvidencia_Ativo = false;
         AV15Pgmname = "FuncaoAPFEvidencia";
         Z411FuncaoAPFEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         A411FuncaoAPFEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         i411FuncaoAPFEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound63 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV7FuncaoAPFEvidencia_Codigo ;
      private int wcpOA165FuncaoAPF_Codigo ;
      private int Z406FuncaoAPFEvidencia_Codigo ;
      private int Z749FuncaoAPFEvidencia_MelhoraCod ;
      private int N165FuncaoAPF_Codigo ;
      private int N749FuncaoAPFEvidencia_MelhoraCod ;
      private int A749FuncaoAPFEvidencia_MelhoraCod ;
      private int AV7FuncaoAPFEvidencia_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int trnEnded ;
      private int A406FuncaoAPFEvidencia_Codigo ;
      private int edtFuncaoAPFEvidencia_Codigo_Enabled ;
      private int edtFuncaoAPFEvidencia_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtFuncaoAPFEvidencia_Descricao_Enabled ;
      private int edtFuncaoAPFEvidencia_NomeArq_Enabled ;
      private int edtFuncaoAPFEvidencia_TipoArq_Enabled ;
      private int edtFuncaoAPFEvidencia_Data_Enabled ;
      private int edtFuncaoAPFEvidencia_Arquivo_Enabled ;
      private int imgCleararquivoanexo_Visible ;
      private int AV11Insert_FuncaoAPF_Codigo ;
      private int AV13Insert_FuncaoAPFEvidencia_MelhoraCod ;
      private int A360FuncaoAPF_SistemaCod ;
      private int AV16GXV1 ;
      private int Z165FuncaoAPF_Codigo ;
      private int Z360FuncaoAPF_SistemaCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z409FuncaoAPFEvidencia_NomeArq ;
      private String Z410FuncaoAPFEvidencia_TipoArq ;
      private String O410FuncaoAPFEvidencia_TipoArq ;
      private String O409FuncaoAPFEvidencia_NomeArq ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtFuncaoAPFEvidencia_Descricao_Internalname ;
      private String edtFuncaoAPFEvidencia_Codigo_Internalname ;
      private String edtFuncaoAPFEvidencia_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockfuncaoapfevidencia_descricao_Internalname ;
      private String lblTextblockfuncaoapfevidencia_descricao_Jsonclick ;
      private String lblTextblockfuncaoapfevidencia_arquivo_Internalname ;
      private String lblTextblockfuncaoapfevidencia_arquivo_Jsonclick ;
      private String lblTextblockfuncaoapfevidencia_nomearq_Internalname ;
      private String lblTextblockfuncaoapfevidencia_nomearq_Jsonclick ;
      private String edtFuncaoAPFEvidencia_NomeArq_Internalname ;
      private String A409FuncaoAPFEvidencia_NomeArq ;
      private String edtFuncaoAPFEvidencia_NomeArq_Jsonclick ;
      private String lblTextblockfuncaoapfevidencia_tipoarq_Internalname ;
      private String lblTextblockfuncaoapfevidencia_tipoarq_Jsonclick ;
      private String edtFuncaoAPFEvidencia_TipoArq_Internalname ;
      private String A410FuncaoAPFEvidencia_TipoArq ;
      private String edtFuncaoAPFEvidencia_TipoArq_Jsonclick ;
      private String lblTextblockfuncaoapfevidencia_data_Internalname ;
      private String lblTextblockfuncaoapfevidencia_data_Jsonclick ;
      private String edtFuncaoAPFEvidencia_Data_Internalname ;
      private String edtFuncaoAPFEvidencia_Data_Jsonclick ;
      private String tblTablemergedfuncaoapfevidencia_arquivo_Internalname ;
      private String edtFuncaoAPFEvidencia_Arquivo_Filetype ;
      private String edtFuncaoAPFEvidencia_Arquivo_Internalname ;
      private String edtFuncaoAPFEvidencia_Arquivo_Contenttype ;
      private String edtFuncaoAPFEvidencia_Arquivo_Parameters ;
      private String edtFuncaoAPFEvidencia_Arquivo_Jsonclick ;
      private String imgCleararquivoanexo_Internalname ;
      private String imgCleararquivoanexo_Jsonclick ;
      private String AV15Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String edtFuncaoAPFEvidencia_Arquivo_Filename ;
      private String GXCCtlgxBlob ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode63 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Dvpanel_tableattributes_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z411FuncaoAPFEvidencia_Data ;
      private DateTime A411FuncaoAPFEvidencia_Data ;
      private DateTime i411FuncaoAPFEvidencia_Data ;
      private bool Z750FuncaoAPFEvidencia_Ativo ;
      private bool entryPointCalled ;
      private bool n749FuncaoAPFEvidencia_MelhoraCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n408FuncaoAPFEvidencia_Arquivo ;
      private bool n407FuncaoAPFEvidencia_Descricao ;
      private bool n409FuncaoAPFEvidencia_NomeArq ;
      private bool n410FuncaoAPFEvidencia_TipoArq ;
      private bool n750FuncaoAPFEvidencia_Ativo ;
      private bool A750FuncaoAPFEvidencia_Ativo ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool i750FuncaoAPFEvidencia_Ativo ;
      private String O407FuncaoAPFEvidencia_Descricao ;
      private String A407FuncaoAPFEvidencia_Descricao ;
      private String Z407FuncaoAPFEvidencia_Descricao ;
      private String A166FuncaoAPF_Nome ;
      private String Z166FuncaoAPF_Nome ;
      private String O408FuncaoAPFEvidencia_Arquivo ;
      private String A408FuncaoAPFEvidencia_Arquivo ;
      private String Z408FuncaoAPFEvidencia_Arquivo ;
      private IGxSession AV10WebSession ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_FuncaoAPF_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] T001O4_A166FuncaoAPF_Nome ;
      private int[] T001O4_A360FuncaoAPF_SistemaCod ;
      private bool[] T001O4_n360FuncaoAPF_SistemaCod ;
      private int[] T001O6_A165FuncaoAPF_Codigo ;
      private int[] T001O6_A406FuncaoAPFEvidencia_Codigo ;
      private DateTime[] T001O6_A411FuncaoAPFEvidencia_Data ;
      private String[] T001O6_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] T001O6_n409FuncaoAPFEvidencia_NomeArq ;
      private String[] T001O6_A410FuncaoAPFEvidencia_TipoArq ;
      private bool[] T001O6_n410FuncaoAPFEvidencia_TipoArq ;
      private String[] T001O6_A166FuncaoAPF_Nome ;
      private String[] T001O6_A407FuncaoAPFEvidencia_Descricao ;
      private bool[] T001O6_n407FuncaoAPFEvidencia_Descricao ;
      private bool[] T001O6_A750FuncaoAPFEvidencia_Ativo ;
      private bool[] T001O6_n750FuncaoAPFEvidencia_Ativo ;
      private int[] T001O6_A749FuncaoAPFEvidencia_MelhoraCod ;
      private bool[] T001O6_n749FuncaoAPFEvidencia_MelhoraCod ;
      private int[] T001O6_A360FuncaoAPF_SistemaCod ;
      private bool[] T001O6_n360FuncaoAPF_SistemaCod ;
      private String[] T001O6_A408FuncaoAPFEvidencia_Arquivo ;
      private bool[] T001O6_n408FuncaoAPFEvidencia_Arquivo ;
      private int[] T001O5_A749FuncaoAPFEvidencia_MelhoraCod ;
      private bool[] T001O5_n749FuncaoAPFEvidencia_MelhoraCod ;
      private int[] T001O7_A749FuncaoAPFEvidencia_MelhoraCod ;
      private bool[] T001O7_n749FuncaoAPFEvidencia_MelhoraCod ;
      private int[] T001O8_A406FuncaoAPFEvidencia_Codigo ;
      private int[] T001O3_A165FuncaoAPF_Codigo ;
      private int[] T001O3_A406FuncaoAPFEvidencia_Codigo ;
      private DateTime[] T001O3_A411FuncaoAPFEvidencia_Data ;
      private String[] T001O3_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] T001O3_n409FuncaoAPFEvidencia_NomeArq ;
      private String[] T001O3_A410FuncaoAPFEvidencia_TipoArq ;
      private bool[] T001O3_n410FuncaoAPFEvidencia_TipoArq ;
      private String[] T001O3_A407FuncaoAPFEvidencia_Descricao ;
      private bool[] T001O3_n407FuncaoAPFEvidencia_Descricao ;
      private bool[] T001O3_A750FuncaoAPFEvidencia_Ativo ;
      private bool[] T001O3_n750FuncaoAPFEvidencia_Ativo ;
      private int[] T001O3_A749FuncaoAPFEvidencia_MelhoraCod ;
      private bool[] T001O3_n749FuncaoAPFEvidencia_MelhoraCod ;
      private String[] T001O3_A408FuncaoAPFEvidencia_Arquivo ;
      private bool[] T001O3_n408FuncaoAPFEvidencia_Arquivo ;
      private int[] T001O9_A406FuncaoAPFEvidencia_Codigo ;
      private int[] T001O9_A165FuncaoAPF_Codigo ;
      private int[] T001O10_A406FuncaoAPFEvidencia_Codigo ;
      private int[] T001O10_A165FuncaoAPF_Codigo ;
      private int[] T001O2_A165FuncaoAPF_Codigo ;
      private int[] T001O2_A406FuncaoAPFEvidencia_Codigo ;
      private DateTime[] T001O2_A411FuncaoAPFEvidencia_Data ;
      private String[] T001O2_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] T001O2_n409FuncaoAPFEvidencia_NomeArq ;
      private String[] T001O2_A410FuncaoAPFEvidencia_TipoArq ;
      private bool[] T001O2_n410FuncaoAPFEvidencia_TipoArq ;
      private String[] T001O2_A407FuncaoAPFEvidencia_Descricao ;
      private bool[] T001O2_n407FuncaoAPFEvidencia_Descricao ;
      private bool[] T001O2_A750FuncaoAPFEvidencia_Ativo ;
      private bool[] T001O2_n750FuncaoAPFEvidencia_Ativo ;
      private int[] T001O2_A749FuncaoAPFEvidencia_MelhoraCod ;
      private bool[] T001O2_n749FuncaoAPFEvidencia_MelhoraCod ;
      private String[] T001O2_A408FuncaoAPFEvidencia_Arquivo ;
      private bool[] T001O2_n408FuncaoAPFEvidencia_Arquivo ;
      private int[] T001O11_A406FuncaoAPFEvidencia_Codigo ;
      private int[] T001O15_A749FuncaoAPFEvidencia_MelhoraCod ;
      private bool[] T001O15_n749FuncaoAPFEvidencia_MelhoraCod ;
      private int[] T001O16_A406FuncaoAPFEvidencia_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class funcaoapfevidencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001O4 ;
          prmT001O4 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O6 ;
          prmT001O6 = new Object[] {
          new Object[] {"@FuncaoAPFEvidencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O5 ;
          prmT001O5 = new Object[] {
          new Object[] {"@FuncaoAPFEvidencia_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O7 ;
          prmT001O7 = new Object[] {
          new Object[] {"@FuncaoAPFEvidencia_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O8 ;
          prmT001O8 = new Object[] {
          new Object[] {"@FuncaoAPFEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O3 ;
          prmT001O3 = new Object[] {
          new Object[] {"@FuncaoAPFEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O9 ;
          prmT001O9 = new Object[] {
          new Object[] {"@FuncaoAPFEvidencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O10 ;
          prmT001O10 = new Object[] {
          new Object[] {"@FuncaoAPFEvidencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O2 ;
          prmT001O2 = new Object[] {
          new Object[] {"@FuncaoAPFEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O11 ;
          prmT001O11 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFEvidencia_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@FuncaoAPFEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@FuncaoAPFEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@FuncaoAPFEvidencia_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@FuncaoAPFEvidencia_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@FuncaoAPFEvidencia_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPFEvidencia_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O12 ;
          prmT001O12 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFEvidencia_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@FuncaoAPFEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@FuncaoAPFEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@FuncaoAPFEvidencia_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@FuncaoAPFEvidencia_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPFEvidencia_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O13 ;
          prmT001O13 = new Object[] {
          new Object[] {"@FuncaoAPFEvidencia_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@FuncaoAPFEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O14 ;
          prmT001O14 = new Object[] {
          new Object[] {"@FuncaoAPFEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O15 ;
          prmT001O15 = new Object[] {
          new Object[] {"@FuncaoAPFEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001O16 ;
          prmT001O16 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001O2", "SELECT [FuncaoAPF_Codigo], [FuncaoAPFEvidencia_Codigo], [FuncaoAPFEvidencia_Data], [FuncaoAPFEvidencia_NomeArq], [FuncaoAPFEvidencia_TipoArq], [FuncaoAPFEvidencia_Descricao], [FuncaoAPFEvidencia_Ativo], [FuncaoAPFEvidencia_MelhoraCod] AS FuncaoAPFEvidencia_MelhoraCod, [FuncaoAPFEvidencia_Arquivo] FROM [FuncaoAPFEvidencia] WITH (UPDLOCK) WHERE [FuncaoAPFEvidencia_Codigo] = @FuncaoAPFEvidencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001O2,1,0,true,false )
             ,new CursorDef("T001O3", "SELECT [FuncaoAPF_Codigo], [FuncaoAPFEvidencia_Codigo], [FuncaoAPFEvidencia_Data], [FuncaoAPFEvidencia_NomeArq], [FuncaoAPFEvidencia_TipoArq], [FuncaoAPFEvidencia_Descricao], [FuncaoAPFEvidencia_Ativo], [FuncaoAPFEvidencia_MelhoraCod] AS FuncaoAPFEvidencia_MelhoraCod, [FuncaoAPFEvidencia_Arquivo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK) WHERE [FuncaoAPFEvidencia_Codigo] = @FuncaoAPFEvidencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001O3,1,0,true,false )
             ,new CursorDef("T001O4", "SELECT [FuncaoAPF_Nome], [FuncaoAPF_SistemaCod] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001O4,1,0,true,false )
             ,new CursorDef("T001O5", "SELECT [FuncaoAPFEvidencia_Codigo] AS FuncaoAPFEvidencia_MelhoraCod FROM [FuncaoAPFEvidencia] WITH (NOLOCK) WHERE [FuncaoAPFEvidencia_Codigo] = @FuncaoAPFEvidencia_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001O5,1,0,true,false )
             ,new CursorDef("T001O6", "SELECT TM1.[FuncaoAPF_Codigo], TM1.[FuncaoAPFEvidencia_Codigo], TM1.[FuncaoAPFEvidencia_Data], TM1.[FuncaoAPFEvidencia_NomeArq], TM1.[FuncaoAPFEvidencia_TipoArq], T2.[FuncaoAPF_Nome], TM1.[FuncaoAPFEvidencia_Descricao], TM1.[FuncaoAPFEvidencia_Ativo], TM1.[FuncaoAPFEvidencia_MelhoraCod] AS FuncaoAPFEvidencia_MelhoraCod, T2.[FuncaoAPF_SistemaCod], TM1.[FuncaoAPFEvidencia_Arquivo] FROM ([FuncaoAPFEvidencia] TM1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = TM1.[FuncaoAPF_Codigo]) WHERE TM1.[FuncaoAPFEvidencia_Codigo] = @FuncaoAPFEvidencia_Codigo and TM1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY TM1.[FuncaoAPFEvidencia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001O6,100,0,true,false )
             ,new CursorDef("T001O7", "SELECT [FuncaoAPFEvidencia_Codigo] AS FuncaoAPFEvidencia_MelhoraCod FROM [FuncaoAPFEvidencia] WITH (NOLOCK) WHERE [FuncaoAPFEvidencia_Codigo] = @FuncaoAPFEvidencia_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001O7,1,0,true,false )
             ,new CursorDef("T001O8", "SELECT [FuncaoAPFEvidencia_Codigo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK) WHERE [FuncaoAPFEvidencia_Codigo] = @FuncaoAPFEvidencia_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001O8,1,0,true,false )
             ,new CursorDef("T001O9", "SELECT TOP 1 [FuncaoAPFEvidencia_Codigo], [FuncaoAPF_Codigo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK) WHERE ( [FuncaoAPFEvidencia_Codigo] > @FuncaoAPFEvidencia_Codigo) and [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY [FuncaoAPFEvidencia_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001O9,1,0,true,true )
             ,new CursorDef("T001O10", "SELECT TOP 1 [FuncaoAPFEvidencia_Codigo], [FuncaoAPF_Codigo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK) WHERE ( [FuncaoAPFEvidencia_Codigo] < @FuncaoAPFEvidencia_Codigo) and [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY [FuncaoAPFEvidencia_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001O10,1,0,true,true )
             ,new CursorDef("T001O11", "INSERT INTO [FuncaoAPFEvidencia]([FuncaoAPF_Codigo], [FuncaoAPFEvidencia_Data], [FuncaoAPFEvidencia_NomeArq], [FuncaoAPFEvidencia_TipoArq], [FuncaoAPFEvidencia_Descricao], [FuncaoAPFEvidencia_Arquivo], [FuncaoAPFEvidencia_Ativo], [FuncaoAPFEvidencia_MelhoraCod]) VALUES(@FuncaoAPF_Codigo, @FuncaoAPFEvidencia_Data, @FuncaoAPFEvidencia_NomeArq, @FuncaoAPFEvidencia_TipoArq, @FuncaoAPFEvidencia_Descricao, @FuncaoAPFEvidencia_Arquivo, @FuncaoAPFEvidencia_Ativo, @FuncaoAPFEvidencia_MelhoraCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001O11)
             ,new CursorDef("T001O12", "UPDATE [FuncaoAPFEvidencia] SET [FuncaoAPF_Codigo]=@FuncaoAPF_Codigo, [FuncaoAPFEvidencia_Data]=@FuncaoAPFEvidencia_Data, [FuncaoAPFEvidencia_NomeArq]=@FuncaoAPFEvidencia_NomeArq, [FuncaoAPFEvidencia_TipoArq]=@FuncaoAPFEvidencia_TipoArq, [FuncaoAPFEvidencia_Descricao]=@FuncaoAPFEvidencia_Descricao, [FuncaoAPFEvidencia_Ativo]=@FuncaoAPFEvidencia_Ativo, [FuncaoAPFEvidencia_MelhoraCod]=@FuncaoAPFEvidencia_MelhoraCod  WHERE [FuncaoAPFEvidencia_Codigo] = @FuncaoAPFEvidencia_Codigo", GxErrorMask.GX_NOMASK,prmT001O12)
             ,new CursorDef("T001O13", "UPDATE [FuncaoAPFEvidencia] SET [FuncaoAPFEvidencia_Arquivo]=@FuncaoAPFEvidencia_Arquivo  WHERE [FuncaoAPFEvidencia_Codigo] = @FuncaoAPFEvidencia_Codigo", GxErrorMask.GX_NOMASK,prmT001O13)
             ,new CursorDef("T001O14", "DELETE FROM [FuncaoAPFEvidencia]  WHERE [FuncaoAPFEvidencia_Codigo] = @FuncaoAPFEvidencia_Codigo", GxErrorMask.GX_NOMASK,prmT001O14)
             ,new CursorDef("T001O15", "SELECT TOP 1 [FuncaoAPFEvidencia_Codigo] AS FuncaoAPFEvidencia_MelhoraCod FROM [FuncaoAPFEvidencia] WITH (NOLOCK) WHERE [FuncaoAPFEvidencia_MelhoraCod] = @FuncaoAPFEvidencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001O15,1,0,true,true )
             ,new CursorDef("T001O16", "SELECT [FuncaoAPFEvidencia_Codigo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY [FuncaoAPFEvidencia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001O16,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, "tmp", "") ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, "tmp", "") ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[8])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getBLOBFile(11, "tmp", "") ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(7, (bool)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[13]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[11]);
                }
                stmt.SetParameter(8, (int)parms[12]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
