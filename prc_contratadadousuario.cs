/*
               File: PRC_ContratadaDoUsuario
        Description: Contratada Do Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:45.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contratadadousuario : GXProcedure
   {
      public prc_contratadadousuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contratadadousuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratadaUsuario_UsuarioCod ,
                           ref int aP1_ContratadaUsuario_AreaTrabalhoCod ,
                           out int aP2_ContratadaCodigo )
      {
         this.A69ContratadaUsuario_UsuarioCod = aP0_ContratadaUsuario_UsuarioCod;
         this.A1228ContratadaUsuario_AreaTrabalhoCod = aP1_ContratadaUsuario_AreaTrabalhoCod;
         this.AV8ContratadaCodigo = 0 ;
         initialize();
         executePrivate();
         aP0_ContratadaUsuario_UsuarioCod=this.A69ContratadaUsuario_UsuarioCod;
         aP1_ContratadaUsuario_AreaTrabalhoCod=this.A1228ContratadaUsuario_AreaTrabalhoCod;
         aP2_ContratadaCodigo=this.AV8ContratadaCodigo;
      }

      public int executeUdp( ref int aP0_ContratadaUsuario_UsuarioCod ,
                             ref int aP1_ContratadaUsuario_AreaTrabalhoCod )
      {
         this.A69ContratadaUsuario_UsuarioCod = aP0_ContratadaUsuario_UsuarioCod;
         this.A1228ContratadaUsuario_AreaTrabalhoCod = aP1_ContratadaUsuario_AreaTrabalhoCod;
         this.AV8ContratadaCodigo = 0 ;
         initialize();
         executePrivate();
         aP0_ContratadaUsuario_UsuarioCod=this.A69ContratadaUsuario_UsuarioCod;
         aP1_ContratadaUsuario_AreaTrabalhoCod=this.A1228ContratadaUsuario_AreaTrabalhoCod;
         aP2_ContratadaCodigo=this.AV8ContratadaCodigo;
         return AV8ContratadaCodigo ;
      }

      public void executeSubmit( ref int aP0_ContratadaUsuario_UsuarioCod ,
                                 ref int aP1_ContratadaUsuario_AreaTrabalhoCod ,
                                 out int aP2_ContratadaCodigo )
      {
         prc_contratadadousuario objprc_contratadadousuario;
         objprc_contratadadousuario = new prc_contratadadousuario();
         objprc_contratadadousuario.A69ContratadaUsuario_UsuarioCod = aP0_ContratadaUsuario_UsuarioCod;
         objprc_contratadadousuario.A1228ContratadaUsuario_AreaTrabalhoCod = aP1_ContratadaUsuario_AreaTrabalhoCod;
         objprc_contratadadousuario.AV8ContratadaCodigo = 0 ;
         objprc_contratadadousuario.context.SetSubmitInitialConfig(context);
         objprc_contratadadousuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contratadadousuario);
         aP0_ContratadaUsuario_UsuarioCod=this.A69ContratadaUsuario_UsuarioCod;
         aP1_ContratadaUsuario_AreaTrabalhoCod=this.A1228ContratadaUsuario_AreaTrabalhoCod;
         aP2_ContratadaCodigo=this.AV8ContratadaCodigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contratadadousuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00812 */
         pr_default.execute(0, new Object[] {A69ContratadaUsuario_UsuarioCod, n1228ContratadaUsuario_AreaTrabalhoCod, A1228ContratadaUsuario_AreaTrabalhoCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A66ContratadaUsuario_ContratadaCod = P00812_A66ContratadaUsuario_ContratadaCod[0];
            OV8ContratadaCodigo = AV8ContratadaCodigo;
            AV8ContratadaCodigo = A66ContratadaUsuario_ContratadaCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00812_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00812_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         P00812_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P00812_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contratadadousuario__default(),
            new Object[][] {
                new Object[] {
               P00812_A69ContratadaUsuario_UsuarioCod, P00812_A1228ContratadaUsuario_AreaTrabalhoCod, P00812_n1228ContratadaUsuario_AreaTrabalhoCod, P00812_A66ContratadaUsuario_ContratadaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int AV8ContratadaCodigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int OV8ContratadaCodigo ;
      private String scmdbuf ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratadaUsuario_UsuarioCod ;
      private int aP1_ContratadaUsuario_AreaTrabalhoCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00812_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00812_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] P00812_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] P00812_A66ContratadaUsuario_ContratadaCod ;
      private int aP2_ContratadaCodigo ;
   }

   public class prc_contratadadousuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00812 ;
          prmP00812 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00812", "SELECT TOP 1 T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod) AND (T2.[Contratada_AreaTrabalhoCod] = @ContratadaUsuario_AreaTrabalhoCod) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00812,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                return;
       }
    }

 }

}
