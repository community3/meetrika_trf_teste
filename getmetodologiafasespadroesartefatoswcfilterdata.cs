/*
               File: GetMetodologiaFasesPadroesArtefatosWCFilterData
        Description: Get Metodologia Fases Padroes Artefatos WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:43.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getmetodologiafasespadroesartefatoswcfilterdata : GXProcedure
   {
      public getmetodologiafasespadroesartefatoswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getmetodologiafasespadroesartefatoswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getmetodologiafasespadroesartefatoswcfilterdata objgetmetodologiafasespadroesartefatoswcfilterdata;
         objgetmetodologiafasespadroesartefatoswcfilterdata = new getmetodologiafasespadroesartefatoswcfilterdata();
         objgetmetodologiafasespadroesartefatoswcfilterdata.AV14DDOName = aP0_DDOName;
         objgetmetodologiafasespadroesartefatoswcfilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetmetodologiafasespadroesartefatoswcfilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetmetodologiafasespadroesartefatoswcfilterdata.AV18OptionsJson = "" ;
         objgetmetodologiafasespadroesartefatoswcfilterdata.AV21OptionsDescJson = "" ;
         objgetmetodologiafasespadroesartefatoswcfilterdata.AV23OptionIndexesJson = "" ;
         objgetmetodologiafasespadroesartefatoswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetmetodologiafasespadroesartefatoswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetmetodologiafasespadroesartefatoswcfilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getmetodologiafasespadroesartefatoswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_PADROESARTEFATOS_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADPADROESARTEFATOS_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("MetodologiaFasesPadroesArtefatosWCGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "MetodologiaFasesPadroesArtefatosWCGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("MetodologiaFasesPadroesArtefatosWCGridState"), "");
         }
         AV40GXV1 = 1;
         while ( AV40GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV40GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFPADROESARTEFATOS_DESCRICAO") == 0 )
            {
               AV10TFPadroesArtefatos_Descricao = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFPADROESARTEFATOS_DESCRICAO_SEL") == 0 )
            {
               AV11TFPadroesArtefatos_Descricao_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "PARM_&METODOLOGIAFASES_CODIGO") == 0 )
            {
               AV37MetodologiaFases_Codigo = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            AV40GXV1 = (int)(AV40GXV1+1);
         }
         if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(1));
            AV30DynamicFiltersSelector1 = AV29GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "PADROESARTEFATOS_DESCRICAO") == 0 )
            {
               AV31DynamicFiltersOperator1 = AV29GridStateDynamicFilter.gxTpr_Operator;
               AV32PadroesArtefatos_Descricao1 = AV29GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV33DynamicFiltersEnabled2 = true;
               AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(2));
               AV34DynamicFiltersSelector2 = AV29GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "PADROESARTEFATOS_DESCRICAO") == 0 )
               {
                  AV35DynamicFiltersOperator2 = AV29GridStateDynamicFilter.gxTpr_Operator;
                  AV36PadroesArtefatos_Descricao2 = AV29GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADPADROESARTEFATOS_DESCRICAOOPTIONS' Routine */
         AV10TFPadroesArtefatos_Descricao = AV12SearchTxt;
         AV11TFPadroesArtefatos_Descricao_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV30DynamicFiltersSelector1 ,
                                              AV31DynamicFiltersOperator1 ,
                                              AV32PadroesArtefatos_Descricao1 ,
                                              AV33DynamicFiltersEnabled2 ,
                                              AV34DynamicFiltersSelector2 ,
                                              AV35DynamicFiltersOperator2 ,
                                              AV36PadroesArtefatos_Descricao2 ,
                                              AV11TFPadroesArtefatos_Descricao_Sel ,
                                              AV10TFPadroesArtefatos_Descricao ,
                                              A151PadroesArtefatos_Descricao ,
                                              AV37MetodologiaFases_Codigo ,
                                              A147MetodologiaFases_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV32PadroesArtefatos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV32PadroesArtefatos_Descricao1), "%", "");
         lV32PadroesArtefatos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV32PadroesArtefatos_Descricao1), "%", "");
         lV36PadroesArtefatos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV36PadroesArtefatos_Descricao2), "%", "");
         lV36PadroesArtefatos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV36PadroesArtefatos_Descricao2), "%", "");
         lV10TFPadroesArtefatos_Descricao = StringUtil.Concat( StringUtil.RTrim( AV10TFPadroesArtefatos_Descricao), "%", "");
         /* Using cursor P00KX2 */
         pr_default.execute(0, new Object[] {AV37MetodologiaFases_Codigo, lV32PadroesArtefatos_Descricao1, lV32PadroesArtefatos_Descricao1, lV36PadroesArtefatos_Descricao2, lV36PadroesArtefatos_Descricao2, lV10TFPadroesArtefatos_Descricao, AV11TFPadroesArtefatos_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKKX2 = false;
            A147MetodologiaFases_Codigo = P00KX2_A147MetodologiaFases_Codigo[0];
            A151PadroesArtefatos_Descricao = P00KX2_A151PadroesArtefatos_Descricao[0];
            A150PadroesArtefatos_Codigo = P00KX2_A150PadroesArtefatos_Codigo[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00KX2_A147MetodologiaFases_Codigo[0] == A147MetodologiaFases_Codigo ) && ( StringUtil.StrCmp(P00KX2_A151PadroesArtefatos_Descricao[0], A151PadroesArtefatos_Descricao) == 0 ) )
            {
               BRKKX2 = false;
               A150PadroesArtefatos_Codigo = P00KX2_A150PadroesArtefatos_Codigo[0];
               AV24count = (long)(AV24count+1);
               BRKKX2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A151PadroesArtefatos_Descricao)) )
            {
               AV16Option = A151PadroesArtefatos_Descricao;
               AV17Options.Add(AV16Option, 0);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKX2 )
            {
               BRKKX2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFPadroesArtefatos_Descricao = "";
         AV11TFPadroesArtefatos_Descricao_Sel = "";
         AV29GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV30DynamicFiltersSelector1 = "";
         AV32PadroesArtefatos_Descricao1 = "";
         AV34DynamicFiltersSelector2 = "";
         AV36PadroesArtefatos_Descricao2 = "";
         scmdbuf = "";
         lV32PadroesArtefatos_Descricao1 = "";
         lV36PadroesArtefatos_Descricao2 = "";
         lV10TFPadroesArtefatos_Descricao = "";
         A151PadroesArtefatos_Descricao = "";
         P00KX2_A147MetodologiaFases_Codigo = new int[1] ;
         P00KX2_A151PadroesArtefatos_Descricao = new String[] {""} ;
         P00KX2_A150PadroesArtefatos_Codigo = new int[1] ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getmetodologiafasespadroesartefatoswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00KX2_A147MetodologiaFases_Codigo, P00KX2_A151PadroesArtefatos_Descricao, P00KX2_A150PadroesArtefatos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV31DynamicFiltersOperator1 ;
      private short AV35DynamicFiltersOperator2 ;
      private int AV40GXV1 ;
      private int AV37MetodologiaFases_Codigo ;
      private int A147MetodologiaFases_Codigo ;
      private int A150PadroesArtefatos_Codigo ;
      private long AV24count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV33DynamicFiltersEnabled2 ;
      private bool BRKKX2 ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV10TFPadroesArtefatos_Descricao ;
      private String AV11TFPadroesArtefatos_Descricao_Sel ;
      private String AV30DynamicFiltersSelector1 ;
      private String AV32PadroesArtefatos_Descricao1 ;
      private String AV34DynamicFiltersSelector2 ;
      private String AV36PadroesArtefatos_Descricao2 ;
      private String lV32PadroesArtefatos_Descricao1 ;
      private String lV36PadroesArtefatos_Descricao2 ;
      private String lV10TFPadroesArtefatos_Descricao ;
      private String A151PadroesArtefatos_Descricao ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00KX2_A147MetodologiaFases_Codigo ;
      private String[] P00KX2_A151PadroesArtefatos_Descricao ;
      private int[] P00KX2_A150PadroesArtefatos_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV29GridStateDynamicFilter ;
   }

   public class getmetodologiafasespadroesartefatoswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00KX2( IGxContext context ,
                                             String AV30DynamicFiltersSelector1 ,
                                             short AV31DynamicFiltersOperator1 ,
                                             String AV32PadroesArtefatos_Descricao1 ,
                                             bool AV33DynamicFiltersEnabled2 ,
                                             String AV34DynamicFiltersSelector2 ,
                                             short AV35DynamicFiltersOperator2 ,
                                             String AV36PadroesArtefatos_Descricao2 ,
                                             String AV11TFPadroesArtefatos_Descricao_Sel ,
                                             String AV10TFPadroesArtefatos_Descricao ,
                                             String A151PadroesArtefatos_Descricao ,
                                             int AV37MetodologiaFases_Codigo ,
                                             int A147MetodologiaFases_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [MetodologiaFases_Codigo], [PadroesArtefatos_Descricao], [PadroesArtefatos_Codigo] FROM [PadroesArtefatos] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([MetodologiaFases_Codigo] = @AV37MetodologiaFases_Codigo)";
         if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV31DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32PadroesArtefatos_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([PadroesArtefatos_Descricao] like @lV32PadroesArtefatos_Descricao1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV31DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32PadroesArtefatos_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([PadroesArtefatos_Descricao] like '%' + @lV32PadroesArtefatos_Descricao1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV33DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV35DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36PadroesArtefatos_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([PadroesArtefatos_Descricao] like @lV36PadroesArtefatos_Descricao2)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV33DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV35DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36PadroesArtefatos_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([PadroesArtefatos_Descricao] like '%' + @lV36PadroesArtefatos_Descricao2)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFPadroesArtefatos_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFPadroesArtefatos_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([PadroesArtefatos_Descricao] like @lV10TFPadroesArtefatos_Descricao)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFPadroesArtefatos_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([PadroesArtefatos_Descricao] = @AV11TFPadroesArtefatos_Descricao_Sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [MetodologiaFases_Codigo], [PadroesArtefatos_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00KX2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00KX2 ;
          prmP00KX2 = new Object[] {
          new Object[] {"@AV37MetodologiaFases_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV32PadroesArtefatos_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV32PadroesArtefatos_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV36PadroesArtefatos_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV36PadroesArtefatos_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV10TFPadroesArtefatos_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV11TFPadroesArtefatos_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00KX2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KX2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getmetodologiafasespadroesartefatoswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getmetodologiafasespadroesartefatoswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getmetodologiafasespadroesartefatoswcfilterdata") )
          {
             return  ;
          }
          getmetodologiafasespadroesartefatoswcfilterdata worker = new getmetodologiafasespadroesartefatoswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
