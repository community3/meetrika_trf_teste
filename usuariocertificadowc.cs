/*
               File: UsuarioCertificadoWC
        Description: Usuario Certificado WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:56:18.70
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuariocertificadowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public usuariocertificadowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public usuariocertificadowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo )
      {
         this.AV7Usuario_Codigo = aP0_Usuario_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Usuario_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_5 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_5_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_5_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV36Pessoa_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Pessoa_Codigo), 6, 0)));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV17TFPessoaCertificado_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFPessoaCertificado_Nome", AV17TFPessoaCertificado_Nome);
                  AV18TFPessoaCertificado_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFPessoaCertificado_Nome_Sel", AV18TFPessoaCertificado_Nome_Sel);
                  AV21TFPessoaCertificado_Emissao = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFPessoaCertificado_Emissao", context.localUtil.Format(AV21TFPessoaCertificado_Emissao, "99/99/99"));
                  AV22TFPessoaCertificado_Emissao_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFPessoaCertificado_Emissao_To", context.localUtil.Format(AV22TFPessoaCertificado_Emissao_To, "99/99/99"));
                  AV27TFPessoaCertificado_Validade = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFPessoaCertificado_Validade", context.localUtil.Format(AV27TFPessoaCertificado_Validade, "99/99/99"));
                  AV28TFPessoaCertificado_Validade_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFPessoaCertificado_Validade_To", context.localUtil.Format(AV28TFPessoaCertificado_Validade_To, "99/99/99"));
                  AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace", AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace);
                  AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace", AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace);
                  AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace", AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace);
                  AV40Pgmname = GetNextPar( );
                  AV7Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV36Pessoa_Codigo, AV13OrderedBy, AV14OrderedDsc, AV17TFPessoaCertificado_Nome, AV18TFPessoaCertificado_Nome_Sel, AV21TFPessoaCertificado_Emissao, AV22TFPessoaCertificado_Emissao_To, AV27TFPessoaCertificado_Validade, AV28TFPessoaCertificado_Validade_To, AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV40Pgmname, AV7Usuario_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAR82( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV40Pgmname = "UsuarioCertificadoWC";
               context.Gx_err = 0;
               WSR82( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Usuario Certificado WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812561917");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("usuariocertificadowc.aspx") + "?" + UrlEncode("" +AV7Usuario_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vPESSOA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36Pessoa_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFPESSOACERTIFICADO_NOME", StringUtil.RTrim( AV17TFPessoaCertificado_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFPESSOACERTIFICADO_NOME_SEL", StringUtil.RTrim( AV18TFPessoaCertificado_Nome_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFPESSOACERTIFICADO_EMISSAO", context.localUtil.Format(AV21TFPessoaCertificado_Emissao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFPESSOACERTIFICADO_EMISSAO_TO", context.localUtil.Format(AV22TFPessoaCertificado_Emissao_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFPESSOACERTIFICADO_VALIDADE", context.localUtil.Format(AV27TFPessoaCertificado_Validade, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFPESSOACERTIFICADO_VALIDADE_TO", context.localUtil.Format(AV28TFPessoaCertificado_Validade_To, "99/99/99"));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_5", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_5), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV32DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV32DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vPESSOACERTIFICADO_NOMETITLEFILTERDATA", AV16PessoaCertificado_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vPESSOACERTIFICADO_NOMETITLEFILTERDATA", AV16PessoaCertificado_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vPESSOACERTIFICADO_EMISSAOTITLEFILTERDATA", AV20PessoaCertificado_EmissaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vPESSOACERTIFICADO_EMISSAOTITLEFILTERDATA", AV20PessoaCertificado_EmissaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vPESSOACERTIFICADO_VALIDADETITLEFILTERDATA", AV26PessoaCertificado_ValidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vPESSOACERTIFICADO_VALIDADETITLEFILTERDATA", AV26PessoaCertificado_ValidadeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Usuario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV40Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"vUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Caption", StringUtil.RTrim( Ddo_pessoacertificado_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Tooltip", StringUtil.RTrim( Ddo_pessoacertificado_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Cls", StringUtil.RTrim( Ddo_pessoacertificado_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_pessoacertificado_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoacertificado_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoacertificado_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoacertificado_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_pessoacertificado_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoacertificado_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_pessoacertificado_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_pessoacertificado_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Filtertype", StringUtil.RTrim( Ddo_pessoacertificado_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_pessoacertificado_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_pessoacertificado_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Datalisttype", StringUtil.RTrim( Ddo_pessoacertificado_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Datalistproc", StringUtil.RTrim( Ddo_pessoacertificado_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pessoacertificado_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Sortasc", StringUtil.RTrim( Ddo_pessoacertificado_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Sortdsc", StringUtil.RTrim( Ddo_pessoacertificado_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Loadingdata", StringUtil.RTrim( Ddo_pessoacertificado_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_pessoacertificado_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_pessoacertificado_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_pessoacertificado_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Caption", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Tooltip", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Cls", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Filteredtext_set", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Filteredtextto_set", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Includesortasc", StringUtil.BoolToStr( Ddo_pessoacertificado_emissao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoacertificado_emissao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Sortedstatus", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Includefilter", StringUtil.BoolToStr( Ddo_pessoacertificado_emissao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Filtertype", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Filterisrange", StringUtil.BoolToStr( Ddo_pessoacertificado_emissao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Includedatalist", StringUtil.BoolToStr( Ddo_pessoacertificado_emissao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Sortasc", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Sortdsc", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Cleanfilter", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Rangefilterfrom", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Rangefilterto", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Searchbuttontext", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Caption", StringUtil.RTrim( Ddo_pessoacertificado_validade_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Tooltip", StringUtil.RTrim( Ddo_pessoacertificado_validade_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Cls", StringUtil.RTrim( Ddo_pessoacertificado_validade_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Filteredtext_set", StringUtil.RTrim( Ddo_pessoacertificado_validade_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Filteredtextto_set", StringUtil.RTrim( Ddo_pessoacertificado_validade_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoacertificado_validade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoacertificado_validade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_pessoacertificado_validade_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoacertificado_validade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Sortedstatus", StringUtil.RTrim( Ddo_pessoacertificado_validade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Includefilter", StringUtil.BoolToStr( Ddo_pessoacertificado_validade_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Filtertype", StringUtil.RTrim( Ddo_pessoacertificado_validade_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_pessoacertificado_validade_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_pessoacertificado_validade_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Sortasc", StringUtil.RTrim( Ddo_pessoacertificado_validade_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Sortdsc", StringUtil.RTrim( Ddo_pessoacertificado_validade_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Cleanfilter", StringUtil.RTrim( Ddo_pessoacertificado_validade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Rangefilterfrom", StringUtil.RTrim( Ddo_pessoacertificado_validade_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Rangefilterto", StringUtil.RTrim( Ddo_pessoacertificado_validade_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_pessoacertificado_validade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_pessoacertificado_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_pessoacertificado_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoacertificado_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Activeeventkey", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Filteredtext_get", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Filteredtextto_get", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Activeeventkey", StringUtil.RTrim( Ddo_pessoacertificado_validade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Filteredtext_get", StringUtil.RTrim( Ddo_pessoacertificado_validade_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Filteredtextto_get", StringUtil.RTrim( Ddo_pessoacertificado_validade_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormR82( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("usuariocertificadowc.js", "?202051812562062");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "UsuarioCertificadoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Usuario Certificado WC" ;
      }

      protected void WBR80( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "usuariocertificadowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_R82( true) ;
         }
         else
         {
            wb_table1_2_R82( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_R82e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36Pessoa_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36Pessoa_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,12);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavPessoa_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioCertificadoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,13);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioCertificadoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,14);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_UsuarioCertificadoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoacertificado_nome_Internalname, StringUtil.RTrim( AV17TFPessoaCertificado_Nome), StringUtil.RTrim( context.localUtil.Format( AV17TFPessoaCertificado_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,15);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoacertificado_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoacertificado_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_UsuarioCertificadoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoacertificado_nome_sel_Internalname, StringUtil.RTrim( AV18TFPessoaCertificado_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV18TFPessoaCertificado_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,16);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoacertificado_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoacertificado_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_UsuarioCertificadoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfpessoacertificado_emissao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfpessoacertificado_emissao_Internalname, context.localUtil.Format(AV21TFPessoaCertificado_Emissao, "99/99/99"), context.localUtil.Format( AV21TFPessoaCertificado_Emissao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,17);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoacertificado_emissao_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoacertificado_emissao_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioCertificadoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfpessoacertificado_emissao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfpessoacertificado_emissao_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_UsuarioCertificadoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfpessoacertificado_emissao_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfpessoacertificado_emissao_to_Internalname, context.localUtil.Format(AV22TFPessoaCertificado_Emissao_To, "99/99/99"), context.localUtil.Format( AV22TFPessoaCertificado_Emissao_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,18);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoacertificado_emissao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoacertificado_emissao_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioCertificadoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfpessoacertificado_emissao_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfpessoacertificado_emissao_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_UsuarioCertificadoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_pessoacertificado_emissaoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_pessoacertificado_emissaoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_pessoacertificado_emissaoauxdate_Internalname, context.localUtil.Format(AV23DDO_PessoaCertificado_EmissaoAuxDate, "99/99/99"), context.localUtil.Format( AV23DDO_PessoaCertificado_EmissaoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,20);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_pessoacertificado_emissaoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioCertificadoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_pessoacertificado_emissaoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_UsuarioCertificadoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_pessoacertificado_emissaoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_pessoacertificado_emissaoauxdateto_Internalname, context.localUtil.Format(AV24DDO_PessoaCertificado_EmissaoAuxDateTo, "99/99/99"), context.localUtil.Format( AV24DDO_PessoaCertificado_EmissaoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,21);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_pessoacertificado_emissaoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioCertificadoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_pessoacertificado_emissaoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_UsuarioCertificadoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfpessoacertificado_validade_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfpessoacertificado_validade_Internalname, context.localUtil.Format(AV27TFPessoaCertificado_Validade, "99/99/99"), context.localUtil.Format( AV27TFPessoaCertificado_Validade, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,22);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoacertificado_validade_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoacertificado_validade_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioCertificadoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfpessoacertificado_validade_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfpessoacertificado_validade_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_UsuarioCertificadoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfpessoacertificado_validade_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfpessoacertificado_validade_to_Internalname, context.localUtil.Format(AV28TFPessoaCertificado_Validade_To, "99/99/99"), context.localUtil.Format( AV28TFPessoaCertificado_Validade_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,23);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoacertificado_validade_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoacertificado_validade_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioCertificadoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfpessoacertificado_validade_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfpessoacertificado_validade_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_UsuarioCertificadoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_pessoacertificado_validadeauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_pessoacertificado_validadeauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_pessoacertificado_validadeauxdate_Internalname, context.localUtil.Format(AV29DDO_PessoaCertificado_ValidadeAuxDate, "99/99/99"), context.localUtil.Format( AV29DDO_PessoaCertificado_ValidadeAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,25);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_pessoacertificado_validadeauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioCertificadoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_pessoacertificado_validadeauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_UsuarioCertificadoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_pessoacertificado_validadeauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_pessoacertificado_validadeauxdateto_Internalname, context.localUtil.Format(AV30DDO_PessoaCertificado_ValidadeAuxDateTo, "99/99/99"), context.localUtil.Format( AV30DDO_PessoaCertificado_ValidadeAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,26);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_pessoacertificado_validadeauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioCertificadoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_pessoacertificado_validadeauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_UsuarioCertificadoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_PESSOACERTIFICADO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Internalname, AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", 0, edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_UsuarioCertificadoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_PESSOACERTIFICADO_EMISSAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Internalname, AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", 0, edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_UsuarioCertificadoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_PESSOACERTIFICADO_VALIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Internalname, AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", 0, edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_UsuarioCertificadoWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTR82( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Usuario Certificado WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPR80( ) ;
            }
         }
      }

      protected void WSR82( )
      {
         STARTR82( ) ;
         EVTR82( ) ;
      }

      protected void EVTR82( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPR80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOACERTIFICADO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPR80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11R82 */
                                    E11R82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOACERTIFICADO_EMISSAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPR80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12R82 */
                                    E12R82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOACERTIFICADO_VALIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPR80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13R82 */
                                    E13R82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPR80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14R82 */
                                    E14R82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPR80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavPessoa_codigo_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPR80( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPR80( ) ;
                              }
                              nGXsfl_5_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
                              SubsflControlProps_52( ) ;
                              A2013PessoaCertificado_Nome = StringUtil.Upper( cgiGet( edtPessoaCertificado_Nome_Internalname));
                              A2012PessoaCertificado_Emissao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtPessoaCertificado_Emissao_Internalname), 0));
                              A2014PessoaCertificado_Validade = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtPessoaCertificado_Validade_Internalname), 0));
                              n2014PessoaCertificado_Validade = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPessoa_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15R82 */
                                          E15R82 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPessoa_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16R82 */
                                          E16R82 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPessoa_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17R82 */
                                          E17R82 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Pessoa_codigo Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vPESSOA_CODIGO"), ",", ".") != Convert.ToDecimal( AV36Pessoa_Codigo )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfpessoacertificado_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFPESSOACERTIFICADO_NOME"), AV17TFPessoaCertificado_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfpessoacertificado_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFPESSOACERTIFICADO_NOME_SEL"), AV18TFPessoaCertificado_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfpessoacertificado_emissao Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFPESSOACERTIFICADO_EMISSAO"), 0) != AV21TFPessoaCertificado_Emissao )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfpessoacertificado_emissao_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFPESSOACERTIFICADO_EMISSAO_TO"), 0) != AV22TFPessoaCertificado_Emissao_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfpessoacertificado_validade Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFPESSOACERTIFICADO_VALIDADE"), 0) != AV27TFPessoaCertificado_Validade )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfpessoacertificado_validade_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFPESSOACERTIFICADO_VALIDADE_TO"), 0) != AV28TFPessoaCertificado_Validade_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPessoa_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPR80( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPessoa_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WER82( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormR82( ) ;
            }
         }
      }

      protected void PAR82( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavPessoa_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_52( ) ;
         while ( nGXsfl_5_idx <= nRC_GXsfl_5 )
         {
            sendrow_52( ) ;
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int AV36Pessoa_Codigo ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV17TFPessoaCertificado_Nome ,
                                       String AV18TFPessoaCertificado_Nome_Sel ,
                                       DateTime AV21TFPessoaCertificado_Emissao ,
                                       DateTime AV22TFPessoaCertificado_Emissao_To ,
                                       DateTime AV27TFPessoaCertificado_Validade ,
                                       DateTime AV28TFPessoaCertificado_Validade_To ,
                                       String AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace ,
                                       String AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace ,
                                       String AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace ,
                                       String AV40Pgmname ,
                                       int AV7Usuario_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFR82( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOACERTIFICADO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2013PessoaCertificado_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"PESSOACERTIFICADO_NOME", StringUtil.RTrim( A2013PessoaCertificado_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOACERTIFICADO_EMISSAO", GetSecureSignedToken( sPrefix, A2012PessoaCertificado_Emissao));
         GxWebStd.gx_hidden_field( context, sPrefix+"PESSOACERTIFICADO_EMISSAO", context.localUtil.Format(A2012PessoaCertificado_Emissao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOACERTIFICADO_VALIDADE", GetSecureSignedToken( sPrefix, A2014PessoaCertificado_Validade));
         GxWebStd.gx_hidden_field( context, sPrefix+"PESSOACERTIFICADO_VALIDADE", context.localUtil.Format(A2014PessoaCertificado_Validade, "99/99/99"));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFR82( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV40Pgmname = "UsuarioCertificadoWC";
         context.Gx_err = 0;
      }

      protected void RFR82( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 5;
         /* Execute user event: E16R82 */
         E16R82 ();
         nGXsfl_5_idx = 1;
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         nGXsfl_5_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_52( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV18TFPessoaCertificado_Nome_Sel ,
                                                 AV17TFPessoaCertificado_Nome ,
                                                 AV21TFPessoaCertificado_Emissao ,
                                                 AV22TFPessoaCertificado_Emissao_To ,
                                                 AV27TFPessoaCertificado_Validade ,
                                                 AV28TFPessoaCertificado_Validade_To ,
                                                 A2013PessoaCertificado_Nome ,
                                                 A2012PessoaCertificado_Emissao ,
                                                 A2014PessoaCertificado_Validade ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A2011PessoaCertificado_PesCod ,
                                                 AV36Pessoa_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV17TFPessoaCertificado_Nome = StringUtil.PadR( StringUtil.RTrim( AV17TFPessoaCertificado_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFPessoaCertificado_Nome", AV17TFPessoaCertificado_Nome);
            /* Using cursor H00R82 */
            pr_default.execute(0, new Object[] {AV36Pessoa_Codigo, lV17TFPessoaCertificado_Nome, AV18TFPessoaCertificado_Nome_Sel, AV21TFPessoaCertificado_Emissao, AV22TFPessoaCertificado_Emissao_To, AV27TFPessoaCertificado_Validade, AV28TFPessoaCertificado_Validade_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_5_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2011PessoaCertificado_PesCod = H00R82_A2011PessoaCertificado_PesCod[0];
               A2014PessoaCertificado_Validade = H00R82_A2014PessoaCertificado_Validade[0];
               n2014PessoaCertificado_Validade = H00R82_n2014PessoaCertificado_Validade[0];
               A2012PessoaCertificado_Emissao = H00R82_A2012PessoaCertificado_Emissao[0];
               A2013PessoaCertificado_Nome = H00R82_A2013PessoaCertificado_Nome[0];
               /* Execute user event: E17R82 */
               E17R82 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 5;
            WBR80( ) ;
         }
         nGXsfl_5_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV18TFPessoaCertificado_Nome_Sel ,
                                              AV17TFPessoaCertificado_Nome ,
                                              AV21TFPessoaCertificado_Emissao ,
                                              AV22TFPessoaCertificado_Emissao_To ,
                                              AV27TFPessoaCertificado_Validade ,
                                              AV28TFPessoaCertificado_Validade_To ,
                                              A2013PessoaCertificado_Nome ,
                                              A2012PessoaCertificado_Emissao ,
                                              A2014PessoaCertificado_Validade ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A2011PessoaCertificado_PesCod ,
                                              AV36Pessoa_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV17TFPessoaCertificado_Nome = StringUtil.PadR( StringUtil.RTrim( AV17TFPessoaCertificado_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFPessoaCertificado_Nome", AV17TFPessoaCertificado_Nome);
         /* Using cursor H00R83 */
         pr_default.execute(1, new Object[] {AV36Pessoa_Codigo, lV17TFPessoaCertificado_Nome, AV18TFPessoaCertificado_Nome_Sel, AV21TFPessoaCertificado_Emissao, AV22TFPessoaCertificado_Emissao_To, AV27TFPessoaCertificado_Validade, AV28TFPessoaCertificado_Validade_To});
         GRID_nRecordCount = H00R83_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV36Pessoa_Codigo, AV13OrderedBy, AV14OrderedDsc, AV17TFPessoaCertificado_Nome, AV18TFPessoaCertificado_Nome_Sel, AV21TFPessoaCertificado_Emissao, AV22TFPessoaCertificado_Emissao_To, AV27TFPessoaCertificado_Validade, AV28TFPessoaCertificado_Validade_To, AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV40Pgmname, AV7Usuario_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV36Pessoa_Codigo, AV13OrderedBy, AV14OrderedDsc, AV17TFPessoaCertificado_Nome, AV18TFPessoaCertificado_Nome_Sel, AV21TFPessoaCertificado_Emissao, AV22TFPessoaCertificado_Emissao_To, AV27TFPessoaCertificado_Validade, AV28TFPessoaCertificado_Validade_To, AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV40Pgmname, AV7Usuario_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV36Pessoa_Codigo, AV13OrderedBy, AV14OrderedDsc, AV17TFPessoaCertificado_Nome, AV18TFPessoaCertificado_Nome_Sel, AV21TFPessoaCertificado_Emissao, AV22TFPessoaCertificado_Emissao_To, AV27TFPessoaCertificado_Validade, AV28TFPessoaCertificado_Validade_To, AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV40Pgmname, AV7Usuario_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV36Pessoa_Codigo, AV13OrderedBy, AV14OrderedDsc, AV17TFPessoaCertificado_Nome, AV18TFPessoaCertificado_Nome_Sel, AV21TFPessoaCertificado_Emissao, AV22TFPessoaCertificado_Emissao_To, AV27TFPessoaCertificado_Validade, AV28TFPessoaCertificado_Validade_To, AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV40Pgmname, AV7Usuario_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV36Pessoa_Codigo, AV13OrderedBy, AV14OrderedDsc, AV17TFPessoaCertificado_Nome, AV18TFPessoaCertificado_Nome_Sel, AV21TFPessoaCertificado_Emissao, AV22TFPessoaCertificado_Emissao_To, AV27TFPessoaCertificado_Validade, AV28TFPessoaCertificado_Validade_To, AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV40Pgmname, AV7Usuario_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPR80( )
      {
         /* Before Start, stand alone formulas. */
         AV40Pgmname = "UsuarioCertificadoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E15R82 */
         E15R82 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV32DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vPESSOACERTIFICADO_NOMETITLEFILTERDATA"), AV16PessoaCertificado_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vPESSOACERTIFICADO_EMISSAOTITLEFILTERDATA"), AV20PessoaCertificado_EmissaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vPESSOACERTIFICADO_VALIDADETITLEFILTERDATA"), AV26PessoaCertificado_ValidadeTitleFilterData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPessoa_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPessoa_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPESSOA_CODIGO");
               GX_FocusControl = edtavPessoa_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36Pessoa_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Pessoa_Codigo), 6, 0)));
            }
            else
            {
               AV36Pessoa_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavPessoa_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Pessoa_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            AV17TFPessoaCertificado_Nome = StringUtil.Upper( cgiGet( edtavTfpessoacertificado_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFPessoaCertificado_Nome", AV17TFPessoaCertificado_Nome);
            AV18TFPessoaCertificado_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfpessoacertificado_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFPessoaCertificado_Nome_Sel", AV18TFPessoaCertificado_Nome_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfpessoacertificado_emissao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFPessoa Certificado_Emissao"}), 1, "vTFPESSOACERTIFICADO_EMISSAO");
               GX_FocusControl = edtavTfpessoacertificado_emissao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21TFPessoaCertificado_Emissao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFPessoaCertificado_Emissao", context.localUtil.Format(AV21TFPessoaCertificado_Emissao, "99/99/99"));
            }
            else
            {
               AV21TFPessoaCertificado_Emissao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfpessoacertificado_emissao_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFPessoaCertificado_Emissao", context.localUtil.Format(AV21TFPessoaCertificado_Emissao, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfpessoacertificado_emissao_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFPessoa Certificado_Emissao_To"}), 1, "vTFPESSOACERTIFICADO_EMISSAO_TO");
               GX_FocusControl = edtavTfpessoacertificado_emissao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22TFPessoaCertificado_Emissao_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFPessoaCertificado_Emissao_To", context.localUtil.Format(AV22TFPessoaCertificado_Emissao_To, "99/99/99"));
            }
            else
            {
               AV22TFPessoaCertificado_Emissao_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfpessoacertificado_emissao_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFPessoaCertificado_Emissao_To", context.localUtil.Format(AV22TFPessoaCertificado_Emissao_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_pessoacertificado_emissaoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Pessoa Certificado_Emissao Aux Date"}), 1, "vDDO_PESSOACERTIFICADO_EMISSAOAUXDATE");
               GX_FocusControl = edtavDdo_pessoacertificado_emissaoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23DDO_PessoaCertificado_EmissaoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DDO_PessoaCertificado_EmissaoAuxDate", context.localUtil.Format(AV23DDO_PessoaCertificado_EmissaoAuxDate, "99/99/99"));
            }
            else
            {
               AV23DDO_PessoaCertificado_EmissaoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_pessoacertificado_emissaoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DDO_PessoaCertificado_EmissaoAuxDate", context.localUtil.Format(AV23DDO_PessoaCertificado_EmissaoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_pessoacertificado_emissaoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Pessoa Certificado_Emissao Aux Date To"}), 1, "vDDO_PESSOACERTIFICADO_EMISSAOAUXDATETO");
               GX_FocusControl = edtavDdo_pessoacertificado_emissaoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24DDO_PessoaCertificado_EmissaoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DDO_PessoaCertificado_EmissaoAuxDateTo", context.localUtil.Format(AV24DDO_PessoaCertificado_EmissaoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV24DDO_PessoaCertificado_EmissaoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_pessoacertificado_emissaoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DDO_PessoaCertificado_EmissaoAuxDateTo", context.localUtil.Format(AV24DDO_PessoaCertificado_EmissaoAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfpessoacertificado_validade_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFPessoa Certificado_Validade"}), 1, "vTFPESSOACERTIFICADO_VALIDADE");
               GX_FocusControl = edtavTfpessoacertificado_validade_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV27TFPessoaCertificado_Validade = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFPessoaCertificado_Validade", context.localUtil.Format(AV27TFPessoaCertificado_Validade, "99/99/99"));
            }
            else
            {
               AV27TFPessoaCertificado_Validade = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfpessoacertificado_validade_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFPessoaCertificado_Validade", context.localUtil.Format(AV27TFPessoaCertificado_Validade, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfpessoacertificado_validade_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFPessoa Certificado_Validade_To"}), 1, "vTFPESSOACERTIFICADO_VALIDADE_TO");
               GX_FocusControl = edtavTfpessoacertificado_validade_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28TFPessoaCertificado_Validade_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFPessoaCertificado_Validade_To", context.localUtil.Format(AV28TFPessoaCertificado_Validade_To, "99/99/99"));
            }
            else
            {
               AV28TFPessoaCertificado_Validade_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfpessoacertificado_validade_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFPessoaCertificado_Validade_To", context.localUtil.Format(AV28TFPessoaCertificado_Validade_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_pessoacertificado_validadeauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Pessoa Certificado_Validade Aux Date"}), 1, "vDDO_PESSOACERTIFICADO_VALIDADEAUXDATE");
               GX_FocusControl = edtavDdo_pessoacertificado_validadeauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29DDO_PessoaCertificado_ValidadeAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DDO_PessoaCertificado_ValidadeAuxDate", context.localUtil.Format(AV29DDO_PessoaCertificado_ValidadeAuxDate, "99/99/99"));
            }
            else
            {
               AV29DDO_PessoaCertificado_ValidadeAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_pessoacertificado_validadeauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DDO_PessoaCertificado_ValidadeAuxDate", context.localUtil.Format(AV29DDO_PessoaCertificado_ValidadeAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_pessoacertificado_validadeauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Pessoa Certificado_Validade Aux Date To"}), 1, "vDDO_PESSOACERTIFICADO_VALIDADEAUXDATETO");
               GX_FocusControl = edtavDdo_pessoacertificado_validadeauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30DDO_PessoaCertificado_ValidadeAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DDO_PessoaCertificado_ValidadeAuxDateTo", context.localUtil.Format(AV30DDO_PessoaCertificado_ValidadeAuxDateTo, "99/99/99"));
            }
            else
            {
               AV30DDO_PessoaCertificado_ValidadeAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_pessoacertificado_validadeauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DDO_PessoaCertificado_ValidadeAuxDateTo", context.localUtil.Format(AV30DDO_PessoaCertificado_ValidadeAuxDateTo, "99/99/99"));
            }
            AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace = cgiGet( edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace", AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace);
            AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace = cgiGet( edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace", AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace);
            AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace = cgiGet( edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace", AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_5 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_5"), ",", "."));
            wcpOAV7Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Usuario_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_pessoacertificado_nome_Caption = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Caption");
            Ddo_pessoacertificado_nome_Tooltip = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Tooltip");
            Ddo_pessoacertificado_nome_Cls = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Cls");
            Ddo_pessoacertificado_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Filteredtext_set");
            Ddo_pessoacertificado_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Selectedvalue_set");
            Ddo_pessoacertificado_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Dropdownoptionstype");
            Ddo_pessoacertificado_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Titlecontrolidtoreplace");
            Ddo_pessoacertificado_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Includesortasc"));
            Ddo_pessoacertificado_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Includesortdsc"));
            Ddo_pessoacertificado_nome_Sortedstatus = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Sortedstatus");
            Ddo_pessoacertificado_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Includefilter"));
            Ddo_pessoacertificado_nome_Filtertype = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Filtertype");
            Ddo_pessoacertificado_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Filterisrange"));
            Ddo_pessoacertificado_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Includedatalist"));
            Ddo_pessoacertificado_nome_Datalisttype = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Datalisttype");
            Ddo_pessoacertificado_nome_Datalistproc = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Datalistproc");
            Ddo_pessoacertificado_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pessoacertificado_nome_Sortasc = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Sortasc");
            Ddo_pessoacertificado_nome_Sortdsc = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Sortdsc");
            Ddo_pessoacertificado_nome_Loadingdata = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Loadingdata");
            Ddo_pessoacertificado_nome_Cleanfilter = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Cleanfilter");
            Ddo_pessoacertificado_nome_Noresultsfound = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Noresultsfound");
            Ddo_pessoacertificado_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Searchbuttontext");
            Ddo_pessoacertificado_emissao_Caption = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Caption");
            Ddo_pessoacertificado_emissao_Tooltip = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Tooltip");
            Ddo_pessoacertificado_emissao_Cls = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Cls");
            Ddo_pessoacertificado_emissao_Filteredtext_set = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Filteredtext_set");
            Ddo_pessoacertificado_emissao_Filteredtextto_set = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Filteredtextto_set");
            Ddo_pessoacertificado_emissao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Dropdownoptionstype");
            Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Titlecontrolidtoreplace");
            Ddo_pessoacertificado_emissao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Includesortasc"));
            Ddo_pessoacertificado_emissao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Includesortdsc"));
            Ddo_pessoacertificado_emissao_Sortedstatus = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Sortedstatus");
            Ddo_pessoacertificado_emissao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Includefilter"));
            Ddo_pessoacertificado_emissao_Filtertype = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Filtertype");
            Ddo_pessoacertificado_emissao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Filterisrange"));
            Ddo_pessoacertificado_emissao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Includedatalist"));
            Ddo_pessoacertificado_emissao_Sortasc = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Sortasc");
            Ddo_pessoacertificado_emissao_Sortdsc = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Sortdsc");
            Ddo_pessoacertificado_emissao_Cleanfilter = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Cleanfilter");
            Ddo_pessoacertificado_emissao_Rangefilterfrom = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Rangefilterfrom");
            Ddo_pessoacertificado_emissao_Rangefilterto = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Rangefilterto");
            Ddo_pessoacertificado_emissao_Searchbuttontext = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Searchbuttontext");
            Ddo_pessoacertificado_validade_Caption = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Caption");
            Ddo_pessoacertificado_validade_Tooltip = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Tooltip");
            Ddo_pessoacertificado_validade_Cls = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Cls");
            Ddo_pessoacertificado_validade_Filteredtext_set = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Filteredtext_set");
            Ddo_pessoacertificado_validade_Filteredtextto_set = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Filteredtextto_set");
            Ddo_pessoacertificado_validade_Dropdownoptionstype = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Dropdownoptionstype");
            Ddo_pessoacertificado_validade_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Titlecontrolidtoreplace");
            Ddo_pessoacertificado_validade_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Includesortasc"));
            Ddo_pessoacertificado_validade_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Includesortdsc"));
            Ddo_pessoacertificado_validade_Sortedstatus = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Sortedstatus");
            Ddo_pessoacertificado_validade_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Includefilter"));
            Ddo_pessoacertificado_validade_Filtertype = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Filtertype");
            Ddo_pessoacertificado_validade_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Filterisrange"));
            Ddo_pessoacertificado_validade_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Includedatalist"));
            Ddo_pessoacertificado_validade_Sortasc = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Sortasc");
            Ddo_pessoacertificado_validade_Sortdsc = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Sortdsc");
            Ddo_pessoacertificado_validade_Cleanfilter = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Cleanfilter");
            Ddo_pessoacertificado_validade_Rangefilterfrom = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Rangefilterfrom");
            Ddo_pessoacertificado_validade_Rangefilterto = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Rangefilterto");
            Ddo_pessoacertificado_validade_Searchbuttontext = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Searchbuttontext");
            Ddo_pessoacertificado_nome_Activeeventkey = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Activeeventkey");
            Ddo_pessoacertificado_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Filteredtext_get");
            Ddo_pessoacertificado_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_NOME_Selectedvalue_get");
            Ddo_pessoacertificado_emissao_Activeeventkey = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Activeeventkey");
            Ddo_pessoacertificado_emissao_Filteredtext_get = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Filteredtext_get");
            Ddo_pessoacertificado_emissao_Filteredtextto_get = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO_Filteredtextto_get");
            Ddo_pessoacertificado_validade_Activeeventkey = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Activeeventkey");
            Ddo_pessoacertificado_validade_Filteredtext_get = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Filteredtext_get");
            Ddo_pessoacertificado_validade_Filteredtextto_get = cgiGet( sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vPESSOA_CODIGO"), ",", ".") != Convert.ToDecimal( AV36Pessoa_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFPESSOACERTIFICADO_NOME"), AV17TFPessoaCertificado_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFPESSOACERTIFICADO_NOME_SEL"), AV18TFPessoaCertificado_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFPESSOACERTIFICADO_EMISSAO"), 0) != AV21TFPessoaCertificado_Emissao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFPESSOACERTIFICADO_EMISSAO_TO"), 0) != AV22TFPessoaCertificado_Emissao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFPESSOACERTIFICADO_VALIDADE"), 0) != AV27TFPessoaCertificado_Validade )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFPESSOACERTIFICADO_VALIDADE_TO"), 0) != AV28TFPessoaCertificado_Validade_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E15R82 */
         E15R82 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15R82( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfpessoacertificado_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfpessoacertificado_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoacertificado_nome_Visible), 5, 0)));
         edtavTfpessoacertificado_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfpessoacertificado_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoacertificado_nome_sel_Visible), 5, 0)));
         edtavTfpessoacertificado_emissao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfpessoacertificado_emissao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoacertificado_emissao_Visible), 5, 0)));
         edtavTfpessoacertificado_emissao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfpessoacertificado_emissao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoacertificado_emissao_to_Visible), 5, 0)));
         edtavTfpessoacertificado_validade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfpessoacertificado_validade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoacertificado_validade_Visible), 5, 0)));
         edtavTfpessoacertificado_validade_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfpessoacertificado_validade_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoacertificado_validade_to_Visible), 5, 0)));
         Ddo_pessoacertificado_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_PessoaCertificado_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_nome_Internalname, "TitleControlIdToReplace", Ddo_pessoacertificado_nome_Titlecontrolidtoreplace);
         AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace = Ddo_pessoacertificado_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace", AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace);
         edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace = subGrid_Internalname+"_PessoaCertificado_Emissao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_emissao_Internalname, "TitleControlIdToReplace", Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace);
         AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace = Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace", AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace);
         edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoacertificado_validade_Titlecontrolidtoreplace = subGrid_Internalname+"_PessoaCertificado_Validade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_validade_Internalname, "TitleControlIdToReplace", Ddo_pessoacertificado_validade_Titlecontrolidtoreplace);
         AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace = Ddo_pessoacertificado_validade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace", AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace);
         edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV32DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV32DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         edtavPessoa_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPessoa_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_codigo_Visible), 5, 0)));
         /* Using cursor H00R84 */
         pr_default.execute(2, new Object[] {AV7Usuario_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1Usuario_Codigo = H00R84_A1Usuario_Codigo[0];
            A57Usuario_PessoaCod = H00R84_A57Usuario_PessoaCod[0];
            AV36Pessoa_Codigo = A57Usuario_PessoaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Pessoa_Codigo), 6, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
      }

      protected void E16R82( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV16PessoaCertificado_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV20PessoaCertificado_EmissaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV26PessoaCertificado_ValidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPessoaCertificado_Nome_Titleformat = 2;
         edtPessoaCertificado_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPessoaCertificado_Nome_Internalname, "Title", edtPessoaCertificado_Nome_Title);
         edtPessoaCertificado_Emissao_Titleformat = 2;
         edtPessoaCertificado_Emissao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Emiss�o", AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPessoaCertificado_Emissao_Internalname, "Title", edtPessoaCertificado_Emissao_Title);
         edtPessoaCertificado_Validade_Titleformat = 2;
         edtPessoaCertificado_Validade_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Validade", AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPessoaCertificado_Validade_Internalname, "Title", edtPessoaCertificado_Validade_Title);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV16PessoaCertificado_NomeTitleFilterData", AV16PessoaCertificado_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV20PessoaCertificado_EmissaoTitleFilterData", AV20PessoaCertificado_EmissaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV26PessoaCertificado_ValidadeTitleFilterData", AV26PessoaCertificado_ValidadeTitleFilterData);
      }

      protected void E11R82( )
      {
         /* Ddo_pessoacertificado_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoacertificado_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoacertificado_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_nome_Internalname, "SortedStatus", Ddo_pessoacertificado_nome_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_pessoacertificado_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoacertificado_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_nome_Internalname, "SortedStatus", Ddo_pessoacertificado_nome_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_pessoacertificado_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV17TFPessoaCertificado_Nome = Ddo_pessoacertificado_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFPessoaCertificado_Nome", AV17TFPessoaCertificado_Nome);
            AV18TFPessoaCertificado_Nome_Sel = Ddo_pessoacertificado_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFPessoaCertificado_Nome_Sel", AV18TFPessoaCertificado_Nome_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E12R82( )
      {
         /* Ddo_pessoacertificado_emissao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoacertificado_emissao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoacertificado_emissao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_emissao_Internalname, "SortedStatus", Ddo_pessoacertificado_emissao_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_pessoacertificado_emissao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoacertificado_emissao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_emissao_Internalname, "SortedStatus", Ddo_pessoacertificado_emissao_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_pessoacertificado_emissao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV21TFPessoaCertificado_Emissao = context.localUtil.CToD( Ddo_pessoacertificado_emissao_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFPessoaCertificado_Emissao", context.localUtil.Format(AV21TFPessoaCertificado_Emissao, "99/99/99"));
            AV22TFPessoaCertificado_Emissao_To = context.localUtil.CToD( Ddo_pessoacertificado_emissao_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFPessoaCertificado_Emissao_To", context.localUtil.Format(AV22TFPessoaCertificado_Emissao_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
      }

      protected void E13R82( )
      {
         /* Ddo_pessoacertificado_validade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoacertificado_validade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoacertificado_validade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_validade_Internalname, "SortedStatus", Ddo_pessoacertificado_validade_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_pessoacertificado_validade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoacertificado_validade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_validade_Internalname, "SortedStatus", Ddo_pessoacertificado_validade_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_pessoacertificado_validade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV27TFPessoaCertificado_Validade = context.localUtil.CToD( Ddo_pessoacertificado_validade_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFPessoaCertificado_Validade", context.localUtil.Format(AV27TFPessoaCertificado_Validade, "99/99/99"));
            AV28TFPessoaCertificado_Validade_To = context.localUtil.CToD( Ddo_pessoacertificado_validade_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFPessoaCertificado_Validade_To", context.localUtil.Format(AV28TFPessoaCertificado_Validade_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
      }

      private void E17R82( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 5;
         }
         sendrow_52( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
      }

      protected void E14R82( )
      {
         /* 'DoInsert' Routine */
         context.PopUp(formatLink("pessoacertificado.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV36Pessoa_Codigo), new Object[] {"AV36Pessoa_Codigo"});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_pessoacertificado_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_nome_Internalname, "SortedStatus", Ddo_pessoacertificado_nome_Sortedstatus);
         Ddo_pessoacertificado_emissao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_emissao_Internalname, "SortedStatus", Ddo_pessoacertificado_emissao_Sortedstatus);
         Ddo_pessoacertificado_validade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_validade_Internalname, "SortedStatus", Ddo_pessoacertificado_validade_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_pessoacertificado_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_nome_Internalname, "SortedStatus", Ddo_pessoacertificado_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_pessoacertificado_emissao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_emissao_Internalname, "SortedStatus", Ddo_pessoacertificado_emissao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_pessoacertificado_validade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_validade_Internalname, "SortedStatus", Ddo_pessoacertificado_validade_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV40Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV40Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV40Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV41GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFPESSOACERTIFICADO_NOME") == 0 )
            {
               AV17TFPessoaCertificado_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFPessoaCertificado_Nome", AV17TFPessoaCertificado_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFPessoaCertificado_Nome)) )
               {
                  Ddo_pessoacertificado_nome_Filteredtext_set = AV17TFPessoaCertificado_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_nome_Internalname, "FilteredText_set", Ddo_pessoacertificado_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFPESSOACERTIFICADO_NOME_SEL") == 0 )
            {
               AV18TFPessoaCertificado_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFPessoaCertificado_Nome_Sel", AV18TFPessoaCertificado_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFPessoaCertificado_Nome_Sel)) )
               {
                  Ddo_pessoacertificado_nome_Selectedvalue_set = AV18TFPessoaCertificado_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_nome_Internalname, "SelectedValue_set", Ddo_pessoacertificado_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFPESSOACERTIFICADO_EMISSAO") == 0 )
            {
               AV21TFPessoaCertificado_Emissao = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFPessoaCertificado_Emissao", context.localUtil.Format(AV21TFPessoaCertificado_Emissao, "99/99/99"));
               AV22TFPessoaCertificado_Emissao_To = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFPessoaCertificado_Emissao_To", context.localUtil.Format(AV22TFPessoaCertificado_Emissao_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV21TFPessoaCertificado_Emissao) )
               {
                  Ddo_pessoacertificado_emissao_Filteredtext_set = context.localUtil.DToC( AV21TFPessoaCertificado_Emissao, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_emissao_Internalname, "FilteredText_set", Ddo_pessoacertificado_emissao_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV22TFPessoaCertificado_Emissao_To) )
               {
                  Ddo_pessoacertificado_emissao_Filteredtextto_set = context.localUtil.DToC( AV22TFPessoaCertificado_Emissao_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_emissao_Internalname, "FilteredTextTo_set", Ddo_pessoacertificado_emissao_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFPESSOACERTIFICADO_VALIDADE") == 0 )
            {
               AV27TFPessoaCertificado_Validade = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFPessoaCertificado_Validade", context.localUtil.Format(AV27TFPessoaCertificado_Validade, "99/99/99"));
               AV28TFPessoaCertificado_Validade_To = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFPessoaCertificado_Validade_To", context.localUtil.Format(AV28TFPessoaCertificado_Validade_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV27TFPessoaCertificado_Validade) )
               {
                  Ddo_pessoacertificado_validade_Filteredtext_set = context.localUtil.DToC( AV27TFPessoaCertificado_Validade, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_validade_Internalname, "FilteredText_set", Ddo_pessoacertificado_validade_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV28TFPessoaCertificado_Validade_To) )
               {
                  Ddo_pessoacertificado_validade_Filteredtextto_set = context.localUtil.DToC( AV28TFPessoaCertificado_Validade_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_pessoacertificado_validade_Internalname, "FilteredTextTo_set", Ddo_pessoacertificado_validade_Filteredtextto_set);
               }
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV40Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFPessoaCertificado_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFPESSOACERTIFICADO_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV17TFPessoaCertificado_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFPessoaCertificado_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFPESSOACERTIFICADO_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV18TFPessoaCertificado_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV21TFPessoaCertificado_Emissao) && (DateTime.MinValue==AV22TFPessoaCertificado_Emissao_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFPESSOACERTIFICADO_EMISSAO";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV21TFPessoaCertificado_Emissao, 2, "/");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV22TFPessoaCertificado_Emissao_To, 2, "/");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV27TFPessoaCertificado_Validade) && (DateTime.MinValue==AV28TFPessoaCertificado_Validade_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFPESSOACERTIFICADO_VALIDADE";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV27TFPessoaCertificado_Validade, 2, "/");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV28TFPessoaCertificado_Validade_To, 2, "/");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7Usuario_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&USUARIO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV40Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV40Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "PessoaCertificado";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "PessoaCertificado_PesCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV36Pessoa_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_R82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 5, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"5\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoaCertificado_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoaCertificado_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoaCertificado_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoaCertificado_Emissao_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoaCertificado_Emissao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoaCertificado_Emissao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoaCertificado_Validade_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoaCertificado_Validade_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoaCertificado_Validade_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2013PessoaCertificado_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoaCertificado_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoaCertificado_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A2012PessoaCertificado_Emissao, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoaCertificado_Emissao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoaCertificado_Emissao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A2014PessoaCertificado_Validade, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoaCertificado_Validade_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoaCertificado_Validade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 5 )
         {
            wbEnd = 0;
            nRC_GXsfl_5 = (short)(nGXsfl_5_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir um novo certificado", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioCertificadoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_R82e( true) ;
         }
         else
         {
            wb_table1_2_R82e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Usuario_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAR82( ) ;
         WSR82( ) ;
         WER82( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Usuario_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAR82( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "usuariocertificadowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAR82( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Usuario_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
         }
         wcpOAV7Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Usuario_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Usuario_Codigo != wcpOAV7Usuario_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Usuario_Codigo = AV7Usuario_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Usuario_Codigo = cgiGet( sPrefix+"AV7Usuario_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Usuario_Codigo) > 0 )
         {
            AV7Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Usuario_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
         }
         else
         {
            AV7Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Usuario_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAR82( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSR82( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSR82( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Usuario_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Usuario_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Usuario_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Usuario_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Usuario_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WER82( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812562594");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("usuariocertificadowc.js", "?202051812562594");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_52( )
      {
         edtPessoaCertificado_Nome_Internalname = sPrefix+"PESSOACERTIFICADO_NOME_"+sGXsfl_5_idx;
         edtPessoaCertificado_Emissao_Internalname = sPrefix+"PESSOACERTIFICADO_EMISSAO_"+sGXsfl_5_idx;
         edtPessoaCertificado_Validade_Internalname = sPrefix+"PESSOACERTIFICADO_VALIDADE_"+sGXsfl_5_idx;
      }

      protected void SubsflControlProps_fel_52( )
      {
         edtPessoaCertificado_Nome_Internalname = sPrefix+"PESSOACERTIFICADO_NOME_"+sGXsfl_5_fel_idx;
         edtPessoaCertificado_Emissao_Internalname = sPrefix+"PESSOACERTIFICADO_EMISSAO_"+sGXsfl_5_fel_idx;
         edtPessoaCertificado_Validade_Internalname = sPrefix+"PESSOACERTIFICADO_VALIDADE_"+sGXsfl_5_fel_idx;
      }

      protected void sendrow_52( )
      {
         SubsflControlProps_52( ) ;
         WBR80( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_5_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_5_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_5_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoaCertificado_Nome_Internalname,StringUtil.RTrim( A2013PessoaCertificado_Nome),StringUtil.RTrim( context.localUtil.Format( A2013PessoaCertificado_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoaCertificado_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoaCertificado_Emissao_Internalname,context.localUtil.Format(A2012PessoaCertificado_Emissao, "99/99/99"),context.localUtil.Format( A2012PessoaCertificado_Emissao, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoaCertificado_Emissao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoaCertificado_Validade_Internalname,context.localUtil.Format(A2014PessoaCertificado_Validade, "99/99/99"),context.localUtil.Format( A2014PessoaCertificado_Validade, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoaCertificado_Validade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOACERTIFICADO_NOME"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( A2013PessoaCertificado_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOACERTIFICADO_EMISSAO"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, A2012PessoaCertificado_Emissao));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOACERTIFICADO_VALIDADE"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, A2014PessoaCertificado_Validade));
            GridContainer.AddRow(GridRow);
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         /* End function sendrow_52 */
      }

      protected void init_default_properties( )
      {
         edtPessoaCertificado_Nome_Internalname = sPrefix+"PESSOACERTIFICADO_NOME";
         edtPessoaCertificado_Emissao_Internalname = sPrefix+"PESSOACERTIFICADO_EMISSAO";
         edtPessoaCertificado_Validade_Internalname = sPrefix+"PESSOACERTIFICADO_VALIDADE";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavPessoa_codigo_Internalname = sPrefix+"vPESSOA_CODIGO";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfpessoacertificado_nome_Internalname = sPrefix+"vTFPESSOACERTIFICADO_NOME";
         edtavTfpessoacertificado_nome_sel_Internalname = sPrefix+"vTFPESSOACERTIFICADO_NOME_SEL";
         edtavTfpessoacertificado_emissao_Internalname = sPrefix+"vTFPESSOACERTIFICADO_EMISSAO";
         edtavTfpessoacertificado_emissao_to_Internalname = sPrefix+"vTFPESSOACERTIFICADO_EMISSAO_TO";
         edtavDdo_pessoacertificado_emissaoauxdate_Internalname = sPrefix+"vDDO_PESSOACERTIFICADO_EMISSAOAUXDATE";
         edtavDdo_pessoacertificado_emissaoauxdateto_Internalname = sPrefix+"vDDO_PESSOACERTIFICADO_EMISSAOAUXDATETO";
         divDdo_pessoacertificado_emissaoauxdates_Internalname = sPrefix+"DDO_PESSOACERTIFICADO_EMISSAOAUXDATES";
         edtavTfpessoacertificado_validade_Internalname = sPrefix+"vTFPESSOACERTIFICADO_VALIDADE";
         edtavTfpessoacertificado_validade_to_Internalname = sPrefix+"vTFPESSOACERTIFICADO_VALIDADE_TO";
         edtavDdo_pessoacertificado_validadeauxdate_Internalname = sPrefix+"vDDO_PESSOACERTIFICADO_VALIDADEAUXDATE";
         edtavDdo_pessoacertificado_validadeauxdateto_Internalname = sPrefix+"vDDO_PESSOACERTIFICADO_VALIDADEAUXDATETO";
         divDdo_pessoacertificado_validadeauxdates_Internalname = sPrefix+"DDO_PESSOACERTIFICADO_VALIDADEAUXDATES";
         Ddo_pessoacertificado_nome_Internalname = sPrefix+"DDO_PESSOACERTIFICADO_NOME";
         edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_pessoacertificado_emissao_Internalname = sPrefix+"DDO_PESSOACERTIFICADO_EMISSAO";
         edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE";
         Ddo_pessoacertificado_validade_Internalname = sPrefix+"DDO_PESSOACERTIFICADO_VALIDADE";
         edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtPessoaCertificado_Validade_Jsonclick = "";
         edtPessoaCertificado_Emissao_Jsonclick = "";
         edtPessoaCertificado_Nome_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtPessoaCertificado_Validade_Titleformat = 0;
         edtPessoaCertificado_Emissao_Titleformat = 0;
         edtPessoaCertificado_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtPessoaCertificado_Validade_Title = "Validade";
         edtPessoaCertificado_Emissao_Title = "Emiss�o";
         edtPessoaCertificado_Nome_Title = "Nome";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoacertificado_validadeauxdateto_Jsonclick = "";
         edtavDdo_pessoacertificado_validadeauxdate_Jsonclick = "";
         edtavTfpessoacertificado_validade_to_Jsonclick = "";
         edtavTfpessoacertificado_validade_to_Visible = 1;
         edtavTfpessoacertificado_validade_Jsonclick = "";
         edtavTfpessoacertificado_validade_Visible = 1;
         edtavDdo_pessoacertificado_emissaoauxdateto_Jsonclick = "";
         edtavDdo_pessoacertificado_emissaoauxdate_Jsonclick = "";
         edtavTfpessoacertificado_emissao_to_Jsonclick = "";
         edtavTfpessoacertificado_emissao_to_Visible = 1;
         edtavTfpessoacertificado_emissao_Jsonclick = "";
         edtavTfpessoacertificado_emissao_Visible = 1;
         edtavTfpessoacertificado_nome_sel_Jsonclick = "";
         edtavTfpessoacertificado_nome_sel_Visible = 1;
         edtavTfpessoacertificado_nome_Jsonclick = "";
         edtavTfpessoacertificado_nome_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtavPessoa_codigo_Jsonclick = "";
         edtavPessoa_codigo_Visible = 1;
         Ddo_pessoacertificado_validade_Searchbuttontext = "Pesquisar";
         Ddo_pessoacertificado_validade_Rangefilterto = "At�";
         Ddo_pessoacertificado_validade_Rangefilterfrom = "Desde";
         Ddo_pessoacertificado_validade_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoacertificado_validade_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoacertificado_validade_Sortasc = "Ordenar de A � Z";
         Ddo_pessoacertificado_validade_Includedatalist = Convert.ToBoolean( 0);
         Ddo_pessoacertificado_validade_Filterisrange = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_validade_Filtertype = "Date";
         Ddo_pessoacertificado_validade_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_validade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_validade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_validade_Titlecontrolidtoreplace = "";
         Ddo_pessoacertificado_validade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoacertificado_validade_Cls = "ColumnSettings";
         Ddo_pessoacertificado_validade_Tooltip = "Op��es";
         Ddo_pessoacertificado_validade_Caption = "";
         Ddo_pessoacertificado_emissao_Searchbuttontext = "Pesquisar";
         Ddo_pessoacertificado_emissao_Rangefilterto = "At�";
         Ddo_pessoacertificado_emissao_Rangefilterfrom = "Desde";
         Ddo_pessoacertificado_emissao_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoacertificado_emissao_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoacertificado_emissao_Sortasc = "Ordenar de A � Z";
         Ddo_pessoacertificado_emissao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_pessoacertificado_emissao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_emissao_Filtertype = "Date";
         Ddo_pessoacertificado_emissao_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_emissao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_emissao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace = "";
         Ddo_pessoacertificado_emissao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoacertificado_emissao_Cls = "ColumnSettings";
         Ddo_pessoacertificado_emissao_Tooltip = "Op��es";
         Ddo_pessoacertificado_emissao_Caption = "";
         Ddo_pessoacertificado_nome_Searchbuttontext = "Pesquisar";
         Ddo_pessoacertificado_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pessoacertificado_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoacertificado_nome_Loadingdata = "Carregando dados...";
         Ddo_pessoacertificado_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoacertificado_nome_Sortasc = "Ordenar de A � Z";
         Ddo_pessoacertificado_nome_Datalistupdateminimumcharacters = 0;
         Ddo_pessoacertificado_nome_Datalistproc = "GetUsuarioCertificadoWCFilterData";
         Ddo_pessoacertificado_nome_Datalisttype = "Dynamic";
         Ddo_pessoacertificado_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pessoacertificado_nome_Filtertype = "Character";
         Ddo_pessoacertificado_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_nome_Titlecontrolidtoreplace = "";
         Ddo_pessoacertificado_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoacertificado_nome_Cls = "ColumnSettings";
         Ddo_pessoacertificado_nome_Tooltip = "Op��es";
         Ddo_pessoacertificado_nome_Caption = "";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV36Pessoa_Codigo',fld:'vPESSOA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV18TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV21TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV22TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV27TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV28TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV16PessoaCertificado_NomeTitleFilterData',fld:'vPESSOACERTIFICADO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV20PessoaCertificado_EmissaoTitleFilterData',fld:'vPESSOACERTIFICADO_EMISSAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV26PessoaCertificado_ValidadeTitleFilterData',fld:'vPESSOACERTIFICADO_VALIDADETITLEFILTERDATA',pic:'',nv:null},{av:'edtPessoaCertificado_Nome_Titleformat',ctrl:'PESSOACERTIFICADO_NOME',prop:'Titleformat'},{av:'edtPessoaCertificado_Nome_Title',ctrl:'PESSOACERTIFICADO_NOME',prop:'Title'},{av:'edtPessoaCertificado_Emissao_Titleformat',ctrl:'PESSOACERTIFICADO_EMISSAO',prop:'Titleformat'},{av:'edtPessoaCertificado_Emissao_Title',ctrl:'PESSOACERTIFICADO_EMISSAO',prop:'Title'},{av:'edtPessoaCertificado_Validade_Titleformat',ctrl:'PESSOACERTIFICADO_VALIDADE',prop:'Titleformat'},{av:'edtPessoaCertificado_Validade_Title',ctrl:'PESSOACERTIFICADO_VALIDADE',prop:'Title'}]}");
         setEventMetadata("DDO_PESSOACERTIFICADO_NOME.ONOPTIONCLICKED","{handler:'E11R82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV36Pessoa_Codigo',fld:'vPESSOA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV18TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV21TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV22TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV27TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV28TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_pessoacertificado_nome_Activeeventkey',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'ActiveEventKey'},{av:'Ddo_pessoacertificado_nome_Filteredtext_get',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'FilteredText_get'},{av:'Ddo_pessoacertificado_nome_Selectedvalue_get',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoacertificado_nome_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'SortedStatus'},{av:'AV17TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV18TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_pessoacertificado_emissao_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'SortedStatus'},{av:'Ddo_pessoacertificado_validade_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOACERTIFICADO_EMISSAO.ONOPTIONCLICKED","{handler:'E12R82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV36Pessoa_Codigo',fld:'vPESSOA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV18TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV21TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV22TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV27TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV28TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_pessoacertificado_emissao_Activeeventkey',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'ActiveEventKey'},{av:'Ddo_pessoacertificado_emissao_Filteredtext_get',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'FilteredText_get'},{av:'Ddo_pessoacertificado_emissao_Filteredtextto_get',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoacertificado_emissao_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'SortedStatus'},{av:'AV21TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV22TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'Ddo_pessoacertificado_nome_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'SortedStatus'},{av:'Ddo_pessoacertificado_validade_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOACERTIFICADO_VALIDADE.ONOPTIONCLICKED","{handler:'E13R82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV36Pessoa_Codigo',fld:'vPESSOA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV18TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV21TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV22TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV27TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV28TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_pessoacertificado_validade_Activeeventkey',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'ActiveEventKey'},{av:'Ddo_pessoacertificado_validade_Filteredtext_get',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'FilteredText_get'},{av:'Ddo_pessoacertificado_validade_Filteredtextto_get',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoacertificado_validade_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'SortedStatus'},{av:'AV27TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV28TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'Ddo_pessoacertificado_nome_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'SortedStatus'},{av:'Ddo_pessoacertificado_emissao_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E17R82',iparms:[],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E14R82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV36Pessoa_Codigo',fld:'vPESSOA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV18TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV21TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV22TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV27TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV28TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV36Pessoa_Codigo',fld:'vPESSOA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV36Pessoa_Codigo',fld:'vPESSOA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV18TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV21TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV22TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV27TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV28TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV16PessoaCertificado_NomeTitleFilterData',fld:'vPESSOACERTIFICADO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV20PessoaCertificado_EmissaoTitleFilterData',fld:'vPESSOACERTIFICADO_EMISSAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV26PessoaCertificado_ValidadeTitleFilterData',fld:'vPESSOACERTIFICADO_VALIDADETITLEFILTERDATA',pic:'',nv:null},{av:'edtPessoaCertificado_Nome_Titleformat',ctrl:'PESSOACERTIFICADO_NOME',prop:'Titleformat'},{av:'edtPessoaCertificado_Nome_Title',ctrl:'PESSOACERTIFICADO_NOME',prop:'Title'},{av:'edtPessoaCertificado_Emissao_Titleformat',ctrl:'PESSOACERTIFICADO_EMISSAO',prop:'Titleformat'},{av:'edtPessoaCertificado_Emissao_Title',ctrl:'PESSOACERTIFICADO_EMISSAO',prop:'Title'},{av:'edtPessoaCertificado_Validade_Titleformat',ctrl:'PESSOACERTIFICADO_VALIDADE',prop:'Titleformat'},{av:'edtPessoaCertificado_Validade_Title',ctrl:'PESSOACERTIFICADO_VALIDADE',prop:'Title'}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV36Pessoa_Codigo',fld:'vPESSOA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV18TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV21TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV22TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV27TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV28TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV16PessoaCertificado_NomeTitleFilterData',fld:'vPESSOACERTIFICADO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV20PessoaCertificado_EmissaoTitleFilterData',fld:'vPESSOACERTIFICADO_EMISSAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV26PessoaCertificado_ValidadeTitleFilterData',fld:'vPESSOACERTIFICADO_VALIDADETITLEFILTERDATA',pic:'',nv:null},{av:'edtPessoaCertificado_Nome_Titleformat',ctrl:'PESSOACERTIFICADO_NOME',prop:'Titleformat'},{av:'edtPessoaCertificado_Nome_Title',ctrl:'PESSOACERTIFICADO_NOME',prop:'Title'},{av:'edtPessoaCertificado_Emissao_Titleformat',ctrl:'PESSOACERTIFICADO_EMISSAO',prop:'Titleformat'},{av:'edtPessoaCertificado_Emissao_Title',ctrl:'PESSOACERTIFICADO_EMISSAO',prop:'Title'},{av:'edtPessoaCertificado_Validade_Titleformat',ctrl:'PESSOACERTIFICADO_VALIDADE',prop:'Titleformat'},{av:'edtPessoaCertificado_Validade_Title',ctrl:'PESSOACERTIFICADO_VALIDADE',prop:'Title'}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV36Pessoa_Codigo',fld:'vPESSOA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV18TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV21TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV22TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV27TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV28TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV16PessoaCertificado_NomeTitleFilterData',fld:'vPESSOACERTIFICADO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV20PessoaCertificado_EmissaoTitleFilterData',fld:'vPESSOACERTIFICADO_EMISSAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV26PessoaCertificado_ValidadeTitleFilterData',fld:'vPESSOACERTIFICADO_VALIDADETITLEFILTERDATA',pic:'',nv:null},{av:'edtPessoaCertificado_Nome_Titleformat',ctrl:'PESSOACERTIFICADO_NOME',prop:'Titleformat'},{av:'edtPessoaCertificado_Nome_Title',ctrl:'PESSOACERTIFICADO_NOME',prop:'Title'},{av:'edtPessoaCertificado_Emissao_Titleformat',ctrl:'PESSOACERTIFICADO_EMISSAO',prop:'Titleformat'},{av:'edtPessoaCertificado_Emissao_Title',ctrl:'PESSOACERTIFICADO_EMISSAO',prop:'Title'},{av:'edtPessoaCertificado_Validade_Titleformat',ctrl:'PESSOACERTIFICADO_VALIDADE',prop:'Titleformat'},{av:'edtPessoaCertificado_Validade_Title',ctrl:'PESSOACERTIFICADO_VALIDADE',prop:'Title'}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV36Pessoa_Codigo',fld:'vPESSOA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV18TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV21TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV22TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV27TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV28TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV16PessoaCertificado_NomeTitleFilterData',fld:'vPESSOACERTIFICADO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV20PessoaCertificado_EmissaoTitleFilterData',fld:'vPESSOACERTIFICADO_EMISSAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV26PessoaCertificado_ValidadeTitleFilterData',fld:'vPESSOACERTIFICADO_VALIDADETITLEFILTERDATA',pic:'',nv:null},{av:'edtPessoaCertificado_Nome_Titleformat',ctrl:'PESSOACERTIFICADO_NOME',prop:'Titleformat'},{av:'edtPessoaCertificado_Nome_Title',ctrl:'PESSOACERTIFICADO_NOME',prop:'Title'},{av:'edtPessoaCertificado_Emissao_Titleformat',ctrl:'PESSOACERTIFICADO_EMISSAO',prop:'Titleformat'},{av:'edtPessoaCertificado_Emissao_Title',ctrl:'PESSOACERTIFICADO_EMISSAO',prop:'Title'},{av:'edtPessoaCertificado_Validade_Titleformat',ctrl:'PESSOACERTIFICADO_VALIDADE',prop:'Titleformat'},{av:'edtPessoaCertificado_Validade_Title',ctrl:'PESSOACERTIFICADO_VALIDADE',prop:'Title'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Ddo_pessoacertificado_nome_Activeeventkey = "";
         Ddo_pessoacertificado_nome_Filteredtext_get = "";
         Ddo_pessoacertificado_nome_Selectedvalue_get = "";
         Ddo_pessoacertificado_emissao_Activeeventkey = "";
         Ddo_pessoacertificado_emissao_Filteredtext_get = "";
         Ddo_pessoacertificado_emissao_Filteredtextto_get = "";
         Ddo_pessoacertificado_validade_Activeeventkey = "";
         Ddo_pessoacertificado_validade_Filteredtext_get = "";
         Ddo_pessoacertificado_validade_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV17TFPessoaCertificado_Nome = "";
         AV18TFPessoaCertificado_Nome_Sel = "";
         AV21TFPessoaCertificado_Emissao = DateTime.MinValue;
         AV22TFPessoaCertificado_Emissao_To = DateTime.MinValue;
         AV27TFPessoaCertificado_Validade = DateTime.MinValue;
         AV28TFPessoaCertificado_Validade_To = DateTime.MinValue;
         AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace = "";
         AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace = "";
         AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace = "";
         AV40Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV32DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV16PessoaCertificado_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV20PessoaCertificado_EmissaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV26PessoaCertificado_ValidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_pessoacertificado_nome_Filteredtext_set = "";
         Ddo_pessoacertificado_nome_Selectedvalue_set = "";
         Ddo_pessoacertificado_nome_Sortedstatus = "";
         Ddo_pessoacertificado_emissao_Filteredtext_set = "";
         Ddo_pessoacertificado_emissao_Filteredtextto_set = "";
         Ddo_pessoacertificado_emissao_Sortedstatus = "";
         Ddo_pessoacertificado_validade_Filteredtext_set = "";
         Ddo_pessoacertificado_validade_Filteredtextto_set = "";
         Ddo_pessoacertificado_validade_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         AV23DDO_PessoaCertificado_EmissaoAuxDate = DateTime.MinValue;
         AV24DDO_PessoaCertificado_EmissaoAuxDateTo = DateTime.MinValue;
         AV29DDO_PessoaCertificado_ValidadeAuxDate = DateTime.MinValue;
         AV30DDO_PessoaCertificado_ValidadeAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A2013PessoaCertificado_Nome = "";
         A2012PessoaCertificado_Emissao = DateTime.MinValue;
         A2014PessoaCertificado_Validade = DateTime.MinValue;
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17TFPessoaCertificado_Nome = "";
         H00R82_A2010PessoaCertificado_Codigo = new int[1] ;
         H00R82_A2011PessoaCertificado_PesCod = new int[1] ;
         H00R82_A2014PessoaCertificado_Validade = new DateTime[] {DateTime.MinValue} ;
         H00R82_n2014PessoaCertificado_Validade = new bool[] {false} ;
         H00R82_A2012PessoaCertificado_Emissao = new DateTime[] {DateTime.MinValue} ;
         H00R82_A2013PessoaCertificado_Nome = new String[] {""} ;
         H00R83_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         H00R84_A1Usuario_Codigo = new int[1] ;
         H00R84_A57Usuario_PessoaCod = new int[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Usuario_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuariocertificadowc__default(),
            new Object[][] {
                new Object[] {
               H00R82_A2010PessoaCertificado_Codigo, H00R82_A2011PessoaCertificado_PesCod, H00R82_A2014PessoaCertificado_Validade, H00R82_n2014PessoaCertificado_Validade, H00R82_A2012PessoaCertificado_Emissao, H00R82_A2013PessoaCertificado_Nome
               }
               , new Object[] {
               H00R83_AGRID_nRecordCount
               }
               , new Object[] {
               H00R84_A1Usuario_Codigo, H00R84_A57Usuario_PessoaCod
               }
            }
         );
         AV40Pgmname = "UsuarioCertificadoWC";
         /* GeneXus formulas. */
         AV40Pgmname = "UsuarioCertificadoWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_5 ;
      private short nGXsfl_5_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_5_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtPessoaCertificado_Nome_Titleformat ;
      private short edtPessoaCertificado_Emissao_Titleformat ;
      private short edtPessoaCertificado_Validade_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Usuario_Codigo ;
      private int wcpOAV7Usuario_Codigo ;
      private int subGrid_Rows ;
      private int AV36Pessoa_Codigo ;
      private int Ddo_pessoacertificado_nome_Datalistupdateminimumcharacters ;
      private int edtavPessoa_codigo_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfpessoacertificado_nome_Visible ;
      private int edtavTfpessoacertificado_nome_sel_Visible ;
      private int edtavTfpessoacertificado_emissao_Visible ;
      private int edtavTfpessoacertificado_emissao_to_Visible ;
      private int edtavTfpessoacertificado_validade_Visible ;
      private int edtavTfpessoacertificado_validade_to_Visible ;
      private int edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A2011PessoaCertificado_PesCod ;
      private int A1Usuario_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int AV41GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Ddo_pessoacertificado_nome_Activeeventkey ;
      private String Ddo_pessoacertificado_nome_Filteredtext_get ;
      private String Ddo_pessoacertificado_nome_Selectedvalue_get ;
      private String Ddo_pessoacertificado_emissao_Activeeventkey ;
      private String Ddo_pessoacertificado_emissao_Filteredtext_get ;
      private String Ddo_pessoacertificado_emissao_Filteredtextto_get ;
      private String Ddo_pessoacertificado_validade_Activeeventkey ;
      private String Ddo_pessoacertificado_validade_Filteredtext_get ;
      private String Ddo_pessoacertificado_validade_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_5_idx="0001" ;
      private String AV17TFPessoaCertificado_Nome ;
      private String AV18TFPessoaCertificado_Nome_Sel ;
      private String AV40Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_pessoacertificado_nome_Caption ;
      private String Ddo_pessoacertificado_nome_Tooltip ;
      private String Ddo_pessoacertificado_nome_Cls ;
      private String Ddo_pessoacertificado_nome_Filteredtext_set ;
      private String Ddo_pessoacertificado_nome_Selectedvalue_set ;
      private String Ddo_pessoacertificado_nome_Dropdownoptionstype ;
      private String Ddo_pessoacertificado_nome_Titlecontrolidtoreplace ;
      private String Ddo_pessoacertificado_nome_Sortedstatus ;
      private String Ddo_pessoacertificado_nome_Filtertype ;
      private String Ddo_pessoacertificado_nome_Datalisttype ;
      private String Ddo_pessoacertificado_nome_Datalistproc ;
      private String Ddo_pessoacertificado_nome_Sortasc ;
      private String Ddo_pessoacertificado_nome_Sortdsc ;
      private String Ddo_pessoacertificado_nome_Loadingdata ;
      private String Ddo_pessoacertificado_nome_Cleanfilter ;
      private String Ddo_pessoacertificado_nome_Noresultsfound ;
      private String Ddo_pessoacertificado_nome_Searchbuttontext ;
      private String Ddo_pessoacertificado_emissao_Caption ;
      private String Ddo_pessoacertificado_emissao_Tooltip ;
      private String Ddo_pessoacertificado_emissao_Cls ;
      private String Ddo_pessoacertificado_emissao_Filteredtext_set ;
      private String Ddo_pessoacertificado_emissao_Filteredtextto_set ;
      private String Ddo_pessoacertificado_emissao_Dropdownoptionstype ;
      private String Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace ;
      private String Ddo_pessoacertificado_emissao_Sortedstatus ;
      private String Ddo_pessoacertificado_emissao_Filtertype ;
      private String Ddo_pessoacertificado_emissao_Sortasc ;
      private String Ddo_pessoacertificado_emissao_Sortdsc ;
      private String Ddo_pessoacertificado_emissao_Cleanfilter ;
      private String Ddo_pessoacertificado_emissao_Rangefilterfrom ;
      private String Ddo_pessoacertificado_emissao_Rangefilterto ;
      private String Ddo_pessoacertificado_emissao_Searchbuttontext ;
      private String Ddo_pessoacertificado_validade_Caption ;
      private String Ddo_pessoacertificado_validade_Tooltip ;
      private String Ddo_pessoacertificado_validade_Cls ;
      private String Ddo_pessoacertificado_validade_Filteredtext_set ;
      private String Ddo_pessoacertificado_validade_Filteredtextto_set ;
      private String Ddo_pessoacertificado_validade_Dropdownoptionstype ;
      private String Ddo_pessoacertificado_validade_Titlecontrolidtoreplace ;
      private String Ddo_pessoacertificado_validade_Sortedstatus ;
      private String Ddo_pessoacertificado_validade_Filtertype ;
      private String Ddo_pessoacertificado_validade_Sortasc ;
      private String Ddo_pessoacertificado_validade_Sortdsc ;
      private String Ddo_pessoacertificado_validade_Cleanfilter ;
      private String Ddo_pessoacertificado_validade_Rangefilterfrom ;
      private String Ddo_pessoacertificado_validade_Rangefilterto ;
      private String Ddo_pessoacertificado_validade_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavPessoa_codigo_Internalname ;
      private String edtavPessoa_codigo_Jsonclick ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfpessoacertificado_nome_Internalname ;
      private String edtavTfpessoacertificado_nome_Jsonclick ;
      private String edtavTfpessoacertificado_nome_sel_Internalname ;
      private String edtavTfpessoacertificado_nome_sel_Jsonclick ;
      private String edtavTfpessoacertificado_emissao_Internalname ;
      private String edtavTfpessoacertificado_emissao_Jsonclick ;
      private String edtavTfpessoacertificado_emissao_to_Internalname ;
      private String edtavTfpessoacertificado_emissao_to_Jsonclick ;
      private String divDdo_pessoacertificado_emissaoauxdates_Internalname ;
      private String edtavDdo_pessoacertificado_emissaoauxdate_Internalname ;
      private String edtavDdo_pessoacertificado_emissaoauxdate_Jsonclick ;
      private String edtavDdo_pessoacertificado_emissaoauxdateto_Internalname ;
      private String edtavDdo_pessoacertificado_emissaoauxdateto_Jsonclick ;
      private String edtavTfpessoacertificado_validade_Internalname ;
      private String edtavTfpessoacertificado_validade_Jsonclick ;
      private String edtavTfpessoacertificado_validade_to_Internalname ;
      private String edtavTfpessoacertificado_validade_to_Jsonclick ;
      private String divDdo_pessoacertificado_validadeauxdates_Internalname ;
      private String edtavDdo_pessoacertificado_validadeauxdate_Internalname ;
      private String edtavDdo_pessoacertificado_validadeauxdate_Jsonclick ;
      private String edtavDdo_pessoacertificado_validadeauxdateto_Internalname ;
      private String edtavDdo_pessoacertificado_validadeauxdateto_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String A2013PessoaCertificado_Nome ;
      private String edtPessoaCertificado_Nome_Internalname ;
      private String edtPessoaCertificado_Emissao_Internalname ;
      private String edtPessoaCertificado_Validade_Internalname ;
      private String scmdbuf ;
      private String lV17TFPessoaCertificado_Nome ;
      private String subGrid_Internalname ;
      private String Ddo_pessoacertificado_nome_Internalname ;
      private String Ddo_pessoacertificado_emissao_Internalname ;
      private String Ddo_pessoacertificado_validade_Internalname ;
      private String edtPessoaCertificado_Nome_Title ;
      private String edtPessoaCertificado_Emissao_Title ;
      private String edtPessoaCertificado_Validade_Title ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7Usuario_Codigo ;
      private String sGXsfl_5_fel_idx="0001" ;
      private String ROClassString ;
      private String edtPessoaCertificado_Nome_Jsonclick ;
      private String edtPessoaCertificado_Emissao_Jsonclick ;
      private String edtPessoaCertificado_Validade_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV21TFPessoaCertificado_Emissao ;
      private DateTime AV22TFPessoaCertificado_Emissao_To ;
      private DateTime AV27TFPessoaCertificado_Validade ;
      private DateTime AV28TFPessoaCertificado_Validade_To ;
      private DateTime AV23DDO_PessoaCertificado_EmissaoAuxDate ;
      private DateTime AV24DDO_PessoaCertificado_EmissaoAuxDateTo ;
      private DateTime AV29DDO_PessoaCertificado_ValidadeAuxDate ;
      private DateTime AV30DDO_PessoaCertificado_ValidadeAuxDateTo ;
      private DateTime A2012PessoaCertificado_Emissao ;
      private DateTime A2014PessoaCertificado_Validade ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Ddo_pessoacertificado_nome_Includesortasc ;
      private bool Ddo_pessoacertificado_nome_Includesortdsc ;
      private bool Ddo_pessoacertificado_nome_Includefilter ;
      private bool Ddo_pessoacertificado_nome_Filterisrange ;
      private bool Ddo_pessoacertificado_nome_Includedatalist ;
      private bool Ddo_pessoacertificado_emissao_Includesortasc ;
      private bool Ddo_pessoacertificado_emissao_Includesortdsc ;
      private bool Ddo_pessoacertificado_emissao_Includefilter ;
      private bool Ddo_pessoacertificado_emissao_Filterisrange ;
      private bool Ddo_pessoacertificado_emissao_Includedatalist ;
      private bool Ddo_pessoacertificado_validade_Includesortasc ;
      private bool Ddo_pessoacertificado_validade_Includesortdsc ;
      private bool Ddo_pessoacertificado_validade_Includefilter ;
      private bool Ddo_pessoacertificado_validade_Filterisrange ;
      private bool Ddo_pessoacertificado_validade_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2014PessoaCertificado_Validade ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV19ddo_PessoaCertificado_NomeTitleControlIdToReplace ;
      private String AV25ddo_PessoaCertificado_EmissaoTitleControlIdToReplace ;
      private String AV31ddo_PessoaCertificado_ValidadeTitleControlIdToReplace ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00R82_A2010PessoaCertificado_Codigo ;
      private int[] H00R82_A2011PessoaCertificado_PesCod ;
      private DateTime[] H00R82_A2014PessoaCertificado_Validade ;
      private bool[] H00R82_n2014PessoaCertificado_Validade ;
      private DateTime[] H00R82_A2012PessoaCertificado_Emissao ;
      private String[] H00R82_A2013PessoaCertificado_Nome ;
      private long[] H00R83_AGRID_nRecordCount ;
      private int[] H00R84_A1Usuario_Codigo ;
      private int[] H00R84_A57Usuario_PessoaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV16PessoaCertificado_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV20PessoaCertificado_EmissaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV26PessoaCertificado_ValidadeTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV32DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class usuariocertificadowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00R82( IGxContext context ,
                                             String AV18TFPessoaCertificado_Nome_Sel ,
                                             String AV17TFPessoaCertificado_Nome ,
                                             DateTime AV21TFPessoaCertificado_Emissao ,
                                             DateTime AV22TFPessoaCertificado_Emissao_To ,
                                             DateTime AV27TFPessoaCertificado_Validade ,
                                             DateTime AV28TFPessoaCertificado_Validade_To ,
                                             String A2013PessoaCertificado_Nome ,
                                             DateTime A2012PessoaCertificado_Emissao ,
                                             DateTime A2014PessoaCertificado_Validade ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A2011PessoaCertificado_PesCod ,
                                             int AV36Pessoa_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [PessoaCertificado_Codigo], [PessoaCertificado_PesCod], [PessoaCertificado_Validade], [PessoaCertificado_Emissao], [PessoaCertificado_Nome]";
         sFromString = " FROM [PessoaCertificado] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([PessoaCertificado_PesCod] = @AV36Pessoa_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV18TFPessoaCertificado_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFPessoaCertificado_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([PessoaCertificado_Nome] like @lV17TFPessoaCertificado_Nome)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFPessoaCertificado_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([PessoaCertificado_Nome] = @AV18TFPessoaCertificado_Nome_Sel)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV21TFPessoaCertificado_Emissao) )
         {
            sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV21TFPessoaCertificado_Emissao)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV22TFPessoaCertificado_Emissao_To) )
         {
            sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV22TFPessoaCertificado_Emissao_To)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV27TFPessoaCertificado_Validade) )
         {
            sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV27TFPessoaCertificado_Validade)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV28TFPessoaCertificado_Validade_To) )
         {
            sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV28TFPessoaCertificado_Validade_To)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Emissao]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Emissao] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Validade]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Validade] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00R83( IGxContext context ,
                                             String AV18TFPessoaCertificado_Nome_Sel ,
                                             String AV17TFPessoaCertificado_Nome ,
                                             DateTime AV21TFPessoaCertificado_Emissao ,
                                             DateTime AV22TFPessoaCertificado_Emissao_To ,
                                             DateTime AV27TFPessoaCertificado_Validade ,
                                             DateTime AV28TFPessoaCertificado_Validade_To ,
                                             String A2013PessoaCertificado_Nome ,
                                             DateTime A2012PessoaCertificado_Emissao ,
                                             DateTime A2014PessoaCertificado_Validade ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A2011PessoaCertificado_PesCod ,
                                             int AV36Pessoa_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [PessoaCertificado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([PessoaCertificado_PesCod] = @AV36Pessoa_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV18TFPessoaCertificado_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFPessoaCertificado_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([PessoaCertificado_Nome] like @lV17TFPessoaCertificado_Nome)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFPessoaCertificado_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([PessoaCertificado_Nome] = @AV18TFPessoaCertificado_Nome_Sel)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV21TFPessoaCertificado_Emissao) )
         {
            sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV21TFPessoaCertificado_Emissao)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV22TFPessoaCertificado_Emissao_To) )
         {
            sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV22TFPessoaCertificado_Emissao_To)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV27TFPessoaCertificado_Validade) )
         {
            sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV27TFPessoaCertificado_Validade)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV28TFPessoaCertificado_Validade_To) )
         {
            sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV28TFPessoaCertificado_Validade_To)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00R82(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
               case 1 :
                     return conditional_H00R83(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00R84 ;
          prmH00R84 = new Object[] {
          new Object[] {"@AV7Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R82 ;
          prmH00R82 = new Object[] {
          new Object[] {"@AV36Pessoa_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17TFPessoaCertificado_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV18TFPessoaCertificado_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV21TFPessoaCertificado_Emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV22TFPessoaCertificado_Emissao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV27TFPessoaCertificado_Validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV28TFPessoaCertificado_Validade_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00R83 ;
          prmH00R83 = new Object[] {
          new Object[] {"@AV36Pessoa_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17TFPessoaCertificado_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV18TFPessoaCertificado_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV21TFPessoaCertificado_Emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV22TFPessoaCertificado_Emissao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV27TFPessoaCertificado_Validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV28TFPessoaCertificado_Validade_To",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00R82", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R82,11,0,true,false )
             ,new CursorDef("H00R83", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R83,1,0,true,false )
             ,new CursorDef("H00R84", "SELECT TOP 1 [Usuario_Codigo], [Usuario_PessoaCod] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV7Usuario_Codigo ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R84,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[13]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
