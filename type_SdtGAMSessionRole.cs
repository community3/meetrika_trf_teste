/*
               File: type_SdtGAMSessionRole
        Description: GAMSessionRole
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 1:32:17.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMSessionRole : GxUserType, IGxExternalObject
   {
      public SdtGAMSessionRole( )
      {
         initialize();
      }

      public SdtGAMSessionRole( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMSessionRole_externalReference == null )
         {
            GAMSessionRole_externalReference = new Artech.Security.GAMSessionRole(context);
         }
         returntostring = "";
         returntostring = (String)(GAMSessionRole_externalReference.ToString());
         return returntostring ;
      }

      public long gxTpr_Id
      {
         get {
            if ( GAMSessionRole_externalReference == null )
            {
               GAMSessionRole_externalReference = new Artech.Security.GAMSessionRole(context);
            }
            return GAMSessionRole_externalReference.Id ;
         }

         set {
            if ( GAMSessionRole_externalReference == null )
            {
               GAMSessionRole_externalReference = new Artech.Security.GAMSessionRole(context);
            }
            GAMSessionRole_externalReference.Id = value;
         }

      }

      public String gxTpr_Externalid
      {
         get {
            if ( GAMSessionRole_externalReference == null )
            {
               GAMSessionRole_externalReference = new Artech.Security.GAMSessionRole(context);
            }
            return GAMSessionRole_externalReference.ExternalId ;
         }

         set {
            if ( GAMSessionRole_externalReference == null )
            {
               GAMSessionRole_externalReference = new Artech.Security.GAMSessionRole(context);
            }
            GAMSessionRole_externalReference.ExternalId = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMSessionRole_externalReference == null )
            {
               GAMSessionRole_externalReference = new Artech.Security.GAMSessionRole(context);
            }
            return GAMSessionRole_externalReference ;
         }

         set {
            GAMSessionRole_externalReference = (Artech.Security.GAMSessionRole)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMSessionRole GAMSessionRole_externalReference=null ;
   }

}
