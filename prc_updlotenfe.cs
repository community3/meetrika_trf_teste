/*
               File: PRC_UpdLoteNFe
        Description: Upd Lote NFe
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:29.3
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updlotenfe : GXProcedure
   {
      public prc_updlotenfe( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updlotenfe( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Lote_Codigo ,
                           int aP1_Lote_NFe ,
                           DateTime aP2_Lote_DataNFe ,
                           String aP3_Lote_NFeArq ,
                           String aP4_Lote_NFeNomeArq ,
                           String aP5_Lote_NFeTipoArq ,
                           DateTime aP6_Lote_DataPagamento ,
                           DateTime aP7_Lote_NFeDataProtocolo )
      {
         this.A596Lote_Codigo = aP0_Lote_Codigo;
         this.AV9Lote_NFe = aP1_Lote_NFe;
         this.AV8Lote_DataNFe = aP2_Lote_DataNFe;
         this.AV10Lote_NFeArq = aP3_Lote_NFeArq;
         this.AV14Lote_NFeNomeArq = aP4_Lote_NFeNomeArq;
         this.AV15Lote_NFeTipoArq = aP5_Lote_NFeTipoArq;
         this.AV16Lote_DataPagamento = aP6_Lote_DataPagamento;
         this.AV17Lote_NFeDataProtocolo = aP7_Lote_NFeDataProtocolo;
         initialize();
         executePrivate();
         aP0_Lote_Codigo=this.A596Lote_Codigo;
      }

      public void executeSubmit( ref int aP0_Lote_Codigo ,
                                 int aP1_Lote_NFe ,
                                 DateTime aP2_Lote_DataNFe ,
                                 String aP3_Lote_NFeArq ,
                                 String aP4_Lote_NFeNomeArq ,
                                 String aP5_Lote_NFeTipoArq ,
                                 DateTime aP6_Lote_DataPagamento ,
                                 DateTime aP7_Lote_NFeDataProtocolo )
      {
         prc_updlotenfe objprc_updlotenfe;
         objprc_updlotenfe = new prc_updlotenfe();
         objprc_updlotenfe.A596Lote_Codigo = aP0_Lote_Codigo;
         objprc_updlotenfe.AV9Lote_NFe = aP1_Lote_NFe;
         objprc_updlotenfe.AV8Lote_DataNFe = aP2_Lote_DataNFe;
         objprc_updlotenfe.AV10Lote_NFeArq = aP3_Lote_NFeArq;
         objprc_updlotenfe.AV14Lote_NFeNomeArq = aP4_Lote_NFeNomeArq;
         objprc_updlotenfe.AV15Lote_NFeTipoArq = aP5_Lote_NFeTipoArq;
         objprc_updlotenfe.AV16Lote_DataPagamento = aP6_Lote_DataPagamento;
         objprc_updlotenfe.AV17Lote_NFeDataProtocolo = aP7_Lote_NFeDataProtocolo;
         objprc_updlotenfe.context.SetSubmitInitialConfig(context);
         objprc_updlotenfe.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updlotenfe);
         aP0_Lote_Codigo=this.A596Lote_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updlotenfe)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P004Z2 */
         pr_default.execute(0, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A679Lote_NFeNomeArq = P004Z2_A679Lote_NFeNomeArq[0];
            n679Lote_NFeNomeArq = P004Z2_n679Lote_NFeNomeArq[0];
            A680Lote_NFeTipoArq = P004Z2_A680Lote_NFeTipoArq[0];
            n680Lote_NFeTipoArq = P004Z2_n680Lote_NFeTipoArq[0];
            A673Lote_NFe = P004Z2_A673Lote_NFe[0];
            n673Lote_NFe = P004Z2_n673Lote_NFe[0];
            A674Lote_DataNfe = P004Z2_A674Lote_DataNfe[0];
            n674Lote_DataNfe = P004Z2_n674Lote_DataNfe[0];
            A857Lote_PrevPagamento = P004Z2_A857Lote_PrevPagamento[0];
            n857Lote_PrevPagamento = P004Z2_n857Lote_PrevPagamento[0];
            A1001Lote_NFeDataProtocolo = P004Z2_A1001Lote_NFeDataProtocolo[0];
            n1001Lote_NFeDataProtocolo = P004Z2_n1001Lote_NFeDataProtocolo[0];
            A678Lote_NFeArq = P004Z2_A678Lote_NFeArq[0];
            n678Lote_NFeArq = P004Z2_n678Lote_NFeArq[0];
            A673Lote_NFe = AV9Lote_NFe;
            n673Lote_NFe = false;
            A674Lote_DataNfe = AV8Lote_DataNFe;
            n674Lote_DataNfe = false;
            A678Lote_NFeArq = AV10Lote_NFeArq;
            n678Lote_NFeArq = false;
            A679Lote_NFeNomeArq = AV14Lote_NFeNomeArq;
            n679Lote_NFeNomeArq = false;
            A680Lote_NFeTipoArq = AV15Lote_NFeTipoArq;
            n680Lote_NFeTipoArq = false;
            A857Lote_PrevPagamento = AV16Lote_DataPagamento;
            n857Lote_PrevPagamento = false;
            A1001Lote_NFeDataProtocolo = AV17Lote_NFeDataProtocolo;
            n1001Lote_NFeDataProtocolo = false;
            /* Using cursor P004Z3 */
            A680Lote_NFeTipoArq = FileUtil.GetFileType( A678Lote_NFeArq);
            n680Lote_NFeTipoArq = false;
            A679Lote_NFeNomeArq = FileUtil.GetFileName( A678Lote_NFeArq);
            n679Lote_NFeNomeArq = false;
            pr_default.execute(1, new Object[] {n673Lote_NFe, A673Lote_NFe, n674Lote_DataNfe, A674Lote_DataNfe, n678Lote_NFeArq, A678Lote_NFeArq, n857Lote_PrevPagamento, A857Lote_PrevPagamento, n1001Lote_NFeDataProtocolo, A1001Lote_NFeDataProtocolo, n680Lote_NFeTipoArq, A680Lote_NFeTipoArq, n679Lote_NFeNomeArq, A679Lote_NFeNomeArq, A596Lote_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("Lote") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P004Z2_A596Lote_Codigo = new int[1] ;
         P004Z2_A679Lote_NFeNomeArq = new String[] {""} ;
         P004Z2_n679Lote_NFeNomeArq = new bool[] {false} ;
         P004Z2_A680Lote_NFeTipoArq = new String[] {""} ;
         P004Z2_n680Lote_NFeTipoArq = new bool[] {false} ;
         P004Z2_A673Lote_NFe = new int[1] ;
         P004Z2_n673Lote_NFe = new bool[] {false} ;
         P004Z2_A674Lote_DataNfe = new DateTime[] {DateTime.MinValue} ;
         P004Z2_n674Lote_DataNfe = new bool[] {false} ;
         P004Z2_A857Lote_PrevPagamento = new DateTime[] {DateTime.MinValue} ;
         P004Z2_n857Lote_PrevPagamento = new bool[] {false} ;
         P004Z2_A1001Lote_NFeDataProtocolo = new DateTime[] {DateTime.MinValue} ;
         P004Z2_n1001Lote_NFeDataProtocolo = new bool[] {false} ;
         P004Z2_A678Lote_NFeArq = new String[] {""} ;
         P004Z2_n678Lote_NFeArq = new bool[] {false} ;
         A679Lote_NFeNomeArq = "";
         A680Lote_NFeTipoArq = "";
         A674Lote_DataNfe = DateTime.MinValue;
         A857Lote_PrevPagamento = DateTime.MinValue;
         A1001Lote_NFeDataProtocolo = DateTime.MinValue;
         A678Lote_NFeArq = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updlotenfe__default(),
            new Object[][] {
                new Object[] {
               P004Z2_A596Lote_Codigo, P004Z2_A679Lote_NFeNomeArq, P004Z2_n679Lote_NFeNomeArq, P004Z2_A680Lote_NFeTipoArq, P004Z2_n680Lote_NFeTipoArq, P004Z2_A673Lote_NFe, P004Z2_n673Lote_NFe, P004Z2_A674Lote_DataNfe, P004Z2_n674Lote_DataNfe, P004Z2_A857Lote_PrevPagamento,
               P004Z2_n857Lote_PrevPagamento, P004Z2_A1001Lote_NFeDataProtocolo, P004Z2_n1001Lote_NFeDataProtocolo, P004Z2_A678Lote_NFeArq, P004Z2_n678Lote_NFeArq
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A596Lote_Codigo ;
      private int AV9Lote_NFe ;
      private int A673Lote_NFe ;
      private String AV10Lote_NFeArq ;
      private String AV14Lote_NFeNomeArq ;
      private String AV15Lote_NFeTipoArq ;
      private String scmdbuf ;
      private String A679Lote_NFeNomeArq ;
      private String A680Lote_NFeTipoArq ;
      private DateTime AV8Lote_DataNFe ;
      private DateTime AV16Lote_DataPagamento ;
      private DateTime AV17Lote_NFeDataProtocolo ;
      private DateTime A674Lote_DataNfe ;
      private DateTime A857Lote_PrevPagamento ;
      private DateTime A1001Lote_NFeDataProtocolo ;
      private bool n679Lote_NFeNomeArq ;
      private bool n680Lote_NFeTipoArq ;
      private bool n673Lote_NFe ;
      private bool n674Lote_DataNfe ;
      private bool n857Lote_PrevPagamento ;
      private bool n1001Lote_NFeDataProtocolo ;
      private bool n678Lote_NFeArq ;
      private String A678Lote_NFeArq ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Lote_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P004Z2_A596Lote_Codigo ;
      private String[] P004Z2_A679Lote_NFeNomeArq ;
      private bool[] P004Z2_n679Lote_NFeNomeArq ;
      private String[] P004Z2_A680Lote_NFeTipoArq ;
      private bool[] P004Z2_n680Lote_NFeTipoArq ;
      private int[] P004Z2_A673Lote_NFe ;
      private bool[] P004Z2_n673Lote_NFe ;
      private DateTime[] P004Z2_A674Lote_DataNfe ;
      private bool[] P004Z2_n674Lote_DataNfe ;
      private DateTime[] P004Z2_A857Lote_PrevPagamento ;
      private bool[] P004Z2_n857Lote_PrevPagamento ;
      private DateTime[] P004Z2_A1001Lote_NFeDataProtocolo ;
      private bool[] P004Z2_n1001Lote_NFeDataProtocolo ;
      private String[] P004Z2_A678Lote_NFeArq ;
      private bool[] P004Z2_n678Lote_NFeArq ;
   }

   public class prc_updlotenfe__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004Z2 ;
          prmP004Z2 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004Z3 ;
          prmP004Z3 = new Object[] {
          new Object[] {"@Lote_NFe",SqlDbType.Int,6,0} ,
          new Object[] {"@Lote_DataNfe",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_NFeArq",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Lote_PrevPagamento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_NFeDataProtocolo",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_NFeTipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Lote_NFeNomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004Z2", "SELECT [Lote_Codigo], [Lote_NFeNomeArq], [Lote_NFeTipoArq], [Lote_NFe], [Lote_DataNfe], [Lote_PrevPagamento], [Lote_NFeDataProtocolo], [Lote_NFeArq] FROM [Lote] WITH (UPDLOCK) WHERE [Lote_Codigo] = @Lote_Codigo ORDER BY [Lote_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004Z2,1,0,true,true )
             ,new CursorDef("P004Z3", "UPDATE [Lote] SET [Lote_NFe]=@Lote_NFe, [Lote_DataNfe]=@Lote_DataNfe, [Lote_NFeArq]=@Lote_NFeArq, [Lote_PrevPagamento]=@Lote_PrevPagamento, [Lote_NFeDataProtocolo]=@Lote_NFeDataProtocolo, [Lote_NFeTipoArq]=@Lote_NFeTipoArq, [Lote_NFeNomeArq]=@Lote_NFeNomeArq  WHERE [Lote_Codigo] = @Lote_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004Z3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getBLOBFile(8, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[13]);
                }
                stmt.SetParameter(8, (int)parms[14]);
                return;
       }
    }

 }

}
