/*
               File: type_SdtSDT_WS_Demandas_Demanda
        Description: SDT_WS_Demandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:8.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_WS_Demandas.Demanda" )]
   [XmlType(TypeName =  "SDT_WS_Demandas.Demanda" , Namespace = "Demanda" )]
   [Serializable]
   public class SdtSDT_WS_Demandas_Demanda : GxUserType
   {
      public SdtSDT_WS_Demandas_Demanda( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn = DateTime.MinValue;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega = DateTime.MinValue;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador_N = 1;
      }

      public SdtSDT_WS_Demandas_Demanda( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
            mapper[ "OSReferencia" ] =  "Contagemresultado_osreferencia" ;
            mapper[ "Servico" ] =  "Contagemresultado_servicosigla" ;
            mapper[ "StatusDemanda" ] =  "Contagemresultado_statusdmndescricao" ;
            mapper[ "Titulo" ] =  "Contagemresultado_descricao" ;
            mapper[ "DataDemanda" ] =  "Contagemresultado_datadmn" ;
            mapper[ "DataEntrega" ] =  "Contagemresultado_dataentrega" ;
            mapper[ "Sistema" ] =  "Contagemrresultado_sistemasigla" ;
            mapper[ "Solicitante" ] =  "Contagemresultado_owner_identificao" ;
            mapper[ "Agrupador" ] =  "Contagemresultado_agrupador" ;
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_WS_Demandas_Demanda deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "Demanda" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_WS_Demandas_Demanda)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_WS_Demandas_Demanda obj ;
         obj = this;
         obj.gxTpr_Contagemresultado_osreferencia = deserialized.gxTpr_Contagemresultado_osreferencia;
         obj.gxTpr_Contagemresultado_servicosigla = deserialized.gxTpr_Contagemresultado_servicosigla;
         obj.gxTpr_Contagemresultado_statusdmndescricao = deserialized.gxTpr_Contagemresultado_statusdmndescricao;
         obj.gxTpr_Contagemresultado_descricao = deserialized.gxTpr_Contagemresultado_descricao;
         obj.gxTpr_Contagemresultado_datadmn = deserialized.gxTpr_Contagemresultado_datadmn;
         obj.gxTpr_Contagemresultado_dataprevista = deserialized.gxTpr_Contagemresultado_dataprevista;
         obj.gxTpr_Contagemresultado_dataentrega = deserialized.gxTpr_Contagemresultado_dataentrega;
         obj.gxTpr_Contagemrresultado_sistemasigla = deserialized.gxTpr_Contagemrresultado_sistemasigla;
         obj.gxTpr_Contagemresultado_owner_identificao = deserialized.gxTpr_Contagemresultado_owner_identificao;
         obj.gxTpr_Contagemresultado_agrupador = deserialized.gxTpr_Contagemresultado_agrupador;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "OSReferencia") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "OSReferencia") == 0 ) )
               {
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia = oReader.Value;
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia_N = 0;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "Servico") == 0 ) )
               {
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla = oReader.Value;
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla_N = 0;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "StatusDemanda") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "StatusDemanda") == 0 ) )
               {
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao = oReader.Value;
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao_N = 0;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Titulo") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "Titulo") == 0 ) )
               {
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao = oReader.Value;
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao_N = 0;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DataDemanda") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "DataDemanda") == 0 ) )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn_N = 0;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DataPrevistaEntrega") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "DataPrevistaEntrega") == 0 ) )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista_N = 0;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DataEntrega") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "DataEntrega") == 0 ) )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega_N = 0;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "Sistema") == 0 ) )
               {
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla = oReader.Value;
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla_N = 0;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitante") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "Solicitante") == 0 ) )
               {
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao = oReader.Value;
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao_N = 0;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Agrupador") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "Agrupador") == 0 ) )
               {
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador = oReader.Value;
                  gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador_N = 0;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_WS_Demandas.Demanda";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia)) || ( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia_N != 1 ) )
         {
            oWriter.WriteElement("OSReferencia", StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia));
            if ( StringUtil.StrCmp(sNameSpace, "OSReferencia") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "OSReferencia");
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla)) || ( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla_N != 1 ) )
         {
            oWriter.WriteElement("Servico", StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla));
            if ( StringUtil.StrCmp(sNameSpace, "Servico") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "Servico");
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao)) || ( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao_N != 1 ) )
         {
            oWriter.WriteElement("StatusDemanda", StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao));
            if ( StringUtil.StrCmp(sNameSpace, "StatusDemanda") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "StatusDemanda");
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao)) || ( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao_N != 1 ) )
         {
            oWriter.WriteElement("Titulo", StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao));
            if ( StringUtil.StrCmp(sNameSpace, "Titulo") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "Titulo");
            }
         }
         if ( ! (DateTime.MinValue==gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn) || ( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn_N != 1 ) )
         {
            if ( (DateTime.MinValue==gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn) )
            {
               oWriter.WriteStartElement("DataDemanda");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("DataDemanda", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "DataDemanda") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "DataDemanda");
               }
            }
         }
         if ( ! (DateTime.MinValue==gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista) || ( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista_N != 1 ) )
         {
            if ( (DateTime.MinValue==gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista) )
            {
               oWriter.WriteStartElement("DataPrevistaEntrega");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("DataPrevistaEntrega", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "DataPrevistaEntrega") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "DataPrevistaEntrega");
               }
            }
         }
         if ( ! (DateTime.MinValue==gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega) || ( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega_N != 1 ) )
         {
            if ( (DateTime.MinValue==gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega) )
            {
               oWriter.WriteStartElement("DataEntrega");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("DataEntrega", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "DataEntrega") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "DataEntrega");
               }
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla)) || ( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla_N != 1 ) )
         {
            oWriter.WriteElement("Sistema", StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla));
            if ( StringUtil.StrCmp(sNameSpace, "Sistema") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "Sistema");
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao)) || ( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao_N != 1 ) )
         {
            oWriter.WriteElement("Solicitante", StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao));
            if ( StringUtil.StrCmp(sNameSpace, "Solicitante") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "Solicitante");
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador)) || ( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador_N != 1 ) )
         {
            oWriter.WriteElement("Agrupador", StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador));
            if ( StringUtil.StrCmp(sNameSpace, "Agrupador") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "Agrupador");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("OSReferencia", gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia, false);
         AddObjectProperty("Servico", gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla, false);
         AddObjectProperty("StatusDemanda", gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao, false);
         AddObjectProperty("Titulo", gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("DataDemanda", sDateCnv, false);
         datetime_STZ = gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataPrevista", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("DataEntrega", sDateCnv, false);
         AddObjectProperty("Sistema", gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla, false);
         AddObjectProperty("Solicitante", gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao, false);
         AddObjectProperty("Agrupador", gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador, false);
         return  ;
      }

      [  SoapElement( ElementName = "OSReferencia" )]
      [  XmlElement( ElementName = "OSReferencia" , Namespace = "OSReferencia"  )]
      public String gxTpr_Contagemresultado_osreferencia
      {
         get {
            return gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia_N = 0;
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia = (String)(value);
         }

      }

      public bool ShouldSerializegxTpr_Contagemresultado_osreferencia( )
      {
         return (bool)(gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia_N == 0) ;
      }

      [  SoapElement( ElementName = "Servico" )]
      [  XmlElement( ElementName = "Servico" , Namespace = "Servico"  )]
      public String gxTpr_Contagemresultado_servicosigla
      {
         get {
            return gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla_N = 0;
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla = (String)(value);
         }

      }

      public bool ShouldSerializegxTpr_Contagemresultado_servicosigla( )
      {
         return (bool)(gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla_N == 0) ;
      }

      [  SoapElement( ElementName = "StatusDemanda" )]
      [  XmlElement( ElementName = "StatusDemanda" , Namespace = "StatusDemanda"  )]
      public String gxTpr_Contagemresultado_statusdmndescricao
      {
         get {
            return gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao_N = 0;
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao = (String)(value);
         }

      }

      public bool ShouldSerializegxTpr_Contagemresultado_statusdmndescricao( )
      {
         return (bool)(gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao_N == 0) ;
      }

      [  SoapElement( ElementName = "Titulo" )]
      [  XmlElement( ElementName = "Titulo" , Namespace = "Titulo"  )]
      public String gxTpr_Contagemresultado_descricao
      {
         get {
            return gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao_N = 0;
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao = (String)(value);
         }

      }

      public bool ShouldSerializegxTpr_Contagemresultado_descricao( )
      {
         return (bool)(gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao_N == 0) ;
      }

      [  SoapElement( ElementName = "DataDemanda" )]
      [  XmlElement( ElementName = "DataDemanda" , Namespace = "DataDemanda" , IsNullable=true )]
      public string gxTpr_Contagemresultado_datadmn_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn).value ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn_N = 0;
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn = DateTime.MinValue;
            else
               gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datadmn
      {
         get {
            return gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn_N = 0;
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn = (DateTime)(value);
         }

      }

      public bool ShouldSerializegxTpr_Contagemresultado_datadmn_Nullable( )
      {
         return (bool)(gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn != DateTime.MinValue ) ;
      }

      [  SoapElement( ElementName = "DataPrevistaEntrega" )]
      [  XmlElement( ElementName = "DataPrevistaEntrega" , Namespace = "DataPrevistaEntrega" , IsNullable=true )]
      public string gxTpr_Contagemresultado_dataprevista_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista).value ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista = DateTime.MinValue;
            else
               gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_dataprevista
      {
         get {
            return gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista_N = 0;
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista = (DateTime)(value);
         }

      }

      public bool ShouldSerializegxTpr_Contagemresultado_dataprevista_Nullable( )
      {
         return (bool)(gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista != DateTime.MinValue ) ;
      }

      [  SoapElement( ElementName = "DataEntrega" )]
      [  XmlElement( ElementName = "DataEntrega" , Namespace = "DataEntrega" , IsNullable=true )]
      public string gxTpr_Contagemresultado_dataentrega_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega).value ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega_N = 0;
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega = DateTime.MinValue;
            else
               gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_dataentrega
      {
         get {
            return gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega_N = 0;
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega = (DateTime)(value);
         }

      }

      public bool ShouldSerializegxTpr_Contagemresultado_dataentrega_Nullable( )
      {
         return (bool)(gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega != DateTime.MinValue ) ;
      }

      [  SoapElement( ElementName = "Sistema" )]
      [  XmlElement( ElementName = "Sistema" , Namespace = "Sistema"  )]
      public String gxTpr_Contagemrresultado_sistemasigla
      {
         get {
            return gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla_N = 0;
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla = (String)(value);
         }

      }

      public bool ShouldSerializegxTpr_Contagemrresultado_sistemasigla( )
      {
         return (bool)(gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla_N == 0) ;
      }

      [  SoapElement( ElementName = "Solicitante" )]
      [  XmlElement( ElementName = "Solicitante" , Namespace = "Solicitante"  )]
      public String gxTpr_Contagemresultado_owner_identificao
      {
         get {
            return gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao_N = 0;
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao = (String)(value);
         }

      }

      public bool ShouldSerializegxTpr_Contagemresultado_owner_identificao( )
      {
         return (bool)(gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao_N == 0) ;
      }

      [  SoapElement( ElementName = "Agrupador" )]
      [  XmlElement( ElementName = "Agrupador" , Namespace = "Agrupador"  )]
      public String gxTpr_Contagemresultado_agrupador
      {
         get {
            return gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador_N = 0;
            gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador = (String)(value);
         }

      }

      public bool ShouldSerializegxTpr_Contagemresultado_agrupador( )
      {
         return (bool)(gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador_N == 0) ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn = DateTime.MinValue;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega = DateTime.MinValue;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador = "";
         gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador_N = 1;
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      protected short gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia_N ;
      protected short gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla_N ;
      protected short gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao_N ;
      protected short gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao_N ;
      protected short gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn_N ;
      protected short gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista_N ;
      protected short gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega_N ;
      protected short gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla_N ;
      protected short gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao_N ;
      protected short gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador_N ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_servicosigla ;
      protected String gxTv_SdtSDT_WS_Demandas_Demanda_Contagemrresultado_sistemasigla ;
      protected String gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_agrupador ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataprevista ;
      protected DateTime datetime_STZ ;
      protected DateTime gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_datadmn ;
      protected DateTime gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_dataentrega ;
      protected String gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_osreferencia ;
      protected String gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_statusdmndescricao ;
      protected String gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_descricao ;
      protected String gxTv_SdtSDT_WS_Demandas_Demanda_Contagemresultado_owner_identificao ;
   }

   [DataContract(Name = @"SDT_WS_Demandas.Demanda", Namespace = "Demanda")]
   public class SdtSDT_WS_Demandas_Demanda_RESTInterface : GxGenericCollectionItem<SdtSDT_WS_Demandas_Demanda>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_WS_Demandas_Demanda_RESTInterface( ) : base()
      {
      }

      public SdtSDT_WS_Demandas_Demanda_RESTInterface( SdtSDT_WS_Demandas_Demanda psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultado_OsReferencia" , Order = 0 )]
      public String gxTpr_Contagemresultado_osreferencia
      {
         get {
            return sdt.gxTpr_Contagemresultado_osreferencia ;
         }

         set {
            sdt.gxTpr_Contagemresultado_osreferencia = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ServicoSigla" , Order = 1 )]
      public String gxTpr_Contagemresultado_servicosigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_servicosigla) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servicosigla = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_StatusDmnDescricao" , Order = 2 )]
      public String gxTpr_Contagemresultado_statusdmndescricao
      {
         get {
            return sdt.gxTpr_Contagemresultado_statusdmndescricao ;
         }

         set {
            sdt.gxTpr_Contagemresultado_statusdmndescricao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_Descricao" , Order = 3 )]
      public String gxTpr_Contagemresultado_descricao
      {
         get {
            return sdt.gxTpr_Contagemresultado_descricao ;
         }

         set {
            sdt.gxTpr_Contagemresultado_descricao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_DataDmn" , Order = 4 )]
      public String gxTpr_Contagemresultado_datadmn
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_datadmn) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_datadmn = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_DataPrevista" , Order = 5 )]
      public String gxTpr_Contagemresultado_dataprevista
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_dataprevista) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dataprevista = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_DataEntrega" , Order = 6 )]
      public String gxTpr_Contagemresultado_dataentrega
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_dataentrega) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dataentrega = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemrResultado_SistemaSigla" , Order = 7 )]
      public String gxTpr_Contagemrresultado_sistemasigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemrresultado_sistemasigla) ;
         }

         set {
            sdt.gxTpr_Contagemrresultado_sistemasigla = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_Owner_Identificao" , Order = 8 )]
      public String gxTpr_Contagemresultado_owner_identificao
      {
         get {
            return sdt.gxTpr_Contagemresultado_owner_identificao ;
         }

         set {
            sdt.gxTpr_Contagemresultado_owner_identificao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_Agrupador" , Order = 9 )]
      public String gxTpr_Contagemresultado_agrupador
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_agrupador) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_agrupador = (String)(value);
         }

      }

      public SdtSDT_WS_Demandas_Demanda sdt
      {
         get {
            return (SdtSDT_WS_Demandas_Demanda)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_WS_Demandas_Demanda() ;
         }
      }

   }

}
