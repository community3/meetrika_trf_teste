/*
               File: GravaLogBC
        Description: Grava Log BC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:8:52.69
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gravalogbc : GXProcedure
   {
      public gravalogbc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public gravalogbc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_LogErroBCModulo ,
                           String aP1_LogErroBCNomeBC ,
                           String aP2_LogErroBCJsonBC ,
                           String aP3_LogErroBCJasonMessage )
      {
         this.AV10LogErroBCModulo = aP0_LogErroBCModulo;
         this.AV11LogErroBCNomeBC = aP1_LogErroBCNomeBC;
         this.AV9LogErroBCJsonBC = aP2_LogErroBCJsonBC;
         this.AV8LogErroBCJasonMessage = aP3_LogErroBCJasonMessage;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_LogErroBCModulo ,
                                 String aP1_LogErroBCNomeBC ,
                                 String aP2_LogErroBCJsonBC ,
                                 String aP3_LogErroBCJasonMessage )
      {
         gravalogbc objgravalogbc;
         objgravalogbc = new gravalogbc();
         objgravalogbc.AV10LogErroBCModulo = aP0_LogErroBCModulo;
         objgravalogbc.AV11LogErroBCNomeBC = aP1_LogErroBCNomeBC;
         objgravalogbc.AV9LogErroBCJsonBC = aP2_LogErroBCJsonBC;
         objgravalogbc.AV8LogErroBCJasonMessage = aP3_LogErroBCJasonMessage;
         objgravalogbc.context.SetSubmitInitialConfig(context);
         objgravalogbc.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgravalogbc);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((gravalogbc)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12LogErroBC.Load(0);
         AV12LogErroBC.gxTpr_Logerrobcdata = Gx_date;
         AV12LogErroBC.gxTpr_Logerrobcmodulo = AV10LogErroBCModulo;
         AV12LogErroBC.gxTpr_Logerrobcnomebc = AV11LogErroBCNomeBC;
         AV12LogErroBC.gxTpr_Logerrobcjsonbc = AV9LogErroBCJsonBC;
         AV12LogErroBC.gxTpr_Logerrobcjasonmessage = AV8LogErroBCJasonMessage;
         AV12LogErroBC.Save();
         if ( AV12LogErroBC.Success() )
         {
            context.CommitDataStores( "GravaLogBC");
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV12LogErroBC = new SdtLogErroBC(context);
         Gx_date = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gravalogbc__default(),
            new Object[][] {
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private DateTime Gx_date ;
      private String AV9LogErroBCJsonBC ;
      private String AV8LogErroBCJasonMessage ;
      private String AV10LogErroBCModulo ;
      private String AV11LogErroBCNomeBC ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private SdtLogErroBC AV12LogErroBC ;
   }

   public class gravalogbc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
