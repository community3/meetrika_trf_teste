/*
               File: AreaTrabalhoGeneral
        Description: Area Trabalho General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:26:47.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class areatrabalhogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public areatrabalhogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public areatrabalhogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbAreaTrabalho_CalculoPFinal = new GXCombobox();
         cmbAreaTrabalho_ValidaOSFM = new GXCombobox();
         cmbAreaTrabalho_VerTA = new GXCombobox();
         chkAreaTrabalho_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A5AreaTrabalho_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA0V2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "AreaTrabalhoGeneral";
               context.Gx_err = 0;
               WS0V2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Area Trabalho General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205302126488");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("areatrabalhogeneral.aspx") + "?" + UrlEncode("" +A5AreaTrabalho_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AREATRABALHO_TIPOPLANILHA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1154AreaTrabalho_TipoPlanilha), "Z9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AREATRABALHO_SS_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1588AreaTrabalho_SS_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AREATRABALHO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AREATRABALHO_CALCULOPFINAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A642AreaTrabalho_CalculoPFinal, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AREATRABALHO_SERVICOPADRAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A830AreaTrabalho_ServicoPadrao), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AREATRABALHO_VALIDAOSFM", GetSecureSignedToken( sPrefix, A834AreaTrabalho_ValidaOSFM));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AREATRABALHO_DIASPARAPAGAR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A855AreaTrabalho_DiasParaPagar), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AREATRABALHO_CONTRATADAUPDBSLCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AREATRABALHO_VERTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2081AreaTrabalho_VerTA), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AREATRABALHO_ATIVO", GetSecureSignedToken( sPrefix, A72AreaTrabalho_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AREATRABALHO_ORGANIZACAOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_CONTRATANTE_Width", StringUtil.RTrim( Dvpanel_contratante_Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_CONTRATANTE_Cls", StringUtil.RTrim( Dvpanel_contratante_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_CONTRATANTE_Title", StringUtil.RTrim( Dvpanel_contratante_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_CONTRATANTE_Collapsible", StringUtil.BoolToStr( Dvpanel_contratante_Collapsible));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_CONTRATANTE_Collapsed", StringUtil.BoolToStr( Dvpanel_contratante_Collapsed));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_CONTRATANTE_Autowidth", StringUtil.BoolToStr( Dvpanel_contratante_Autowidth));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_CONTRATANTE_Autoheight", StringUtil.BoolToStr( Dvpanel_contratante_Autoheight));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_CONTRATANTE_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_contratante_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_CONTRATANTE_Iconposition", StringUtil.RTrim( Dvpanel_contratante_Iconposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_CONTRATANTE_Autoscroll", StringUtil.BoolToStr( Dvpanel_contratante_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "AreaTrabalhoGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("areatrabalhogeneral:[SendSecurityCheck value for]"+"Contratante_Codigo:"+context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm0V2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("areatrabalhogeneral.js", "?202053021264829");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "AreaTrabalhoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Area Trabalho General" ;
      }

      protected void WB0V0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "areatrabalhogeneral.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            }
            wb_table1_2_0V2( true) ;
         }
         else
         {
            wb_table1_2_0V2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_0V2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_OrganizacaoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_OrganizacaoCod_Jsonclick, 0, "Attribute", "", "", "", edtAreaTrabalho_OrganizacaoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AreaTrabalhoGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratante_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AreaTrabalhoGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_PessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A335Contratante_PessoaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_PessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContratante_PessoaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AreaTrabalhoGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtEstado_UF_Internalname, StringUtil.RTrim( A23Estado_UF), StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEstado_UF_Jsonclick, 0, "Attribute", "", "", "", edtEstado_UF_Visible, 0, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "UF", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMunicipio_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMunicipio_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtMunicipio_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AreaTrabalhoGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtAreaTrabalho_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AreaTrabalhoGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START0V2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Area Trabalho General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP0V0( ) ;
            }
         }
      }

      protected void WS0V2( )
      {
         START0V2( ) ;
         EVT0V2( ) ;
      }

      protected void EVT0V2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0V0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0V0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E110V2 */
                                    E110V2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0V0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E120V2 */
                                    E120V2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0V0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E130V2 */
                                    E130V2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0V0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E140V2 */
                                    E140V2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0V0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0V0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE0V2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm0V2( ) ;
            }
         }
      }

      protected void PA0V2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbAreaTrabalho_CalculoPFinal.Name = "AREATRABALHO_CALCULOPFINAL";
            cmbAreaTrabalho_CalculoPFinal.WebTags = "";
            cmbAreaTrabalho_CalculoPFinal.addItem("MB", "Menor PFB", 0);
            cmbAreaTrabalho_CalculoPFinal.addItem("BM", "PFB FM", 0);
            if ( cmbAreaTrabalho_CalculoPFinal.ItemCount > 0 )
            {
               A642AreaTrabalho_CalculoPFinal = cmbAreaTrabalho_CalculoPFinal.getValidValue(A642AreaTrabalho_CalculoPFinal);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A642AreaTrabalho_CalculoPFinal", A642AreaTrabalho_CalculoPFinal);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_CALCULOPFINAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A642AreaTrabalho_CalculoPFinal, ""))));
            }
            cmbAreaTrabalho_ValidaOSFM.Name = "AREATRABALHO_VALIDAOSFM";
            cmbAreaTrabalho_ValidaOSFM.WebTags = "";
            cmbAreaTrabalho_ValidaOSFM.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbAreaTrabalho_ValidaOSFM.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbAreaTrabalho_ValidaOSFM.ItemCount > 0 )
            {
               A834AreaTrabalho_ValidaOSFM = StringUtil.StrToBool( cmbAreaTrabalho_ValidaOSFM.getValidValue(StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM)));
               n834AreaTrabalho_ValidaOSFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A834AreaTrabalho_ValidaOSFM", A834AreaTrabalho_ValidaOSFM);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_VALIDAOSFM", GetSecureSignedToken( sPrefix, A834AreaTrabalho_ValidaOSFM));
            }
            cmbAreaTrabalho_VerTA.Name = "AREATRABALHO_VERTA";
            cmbAreaTrabalho_VerTA.WebTags = "";
            cmbAreaTrabalho_VerTA.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Padr�o", 0);
            cmbAreaTrabalho_VerTA.addItem("1", "Vers�o 1", 0);
            if ( cmbAreaTrabalho_VerTA.ItemCount > 0 )
            {
               A2081AreaTrabalho_VerTA = (short)(NumberUtil.Val( cmbAreaTrabalho_VerTA.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0))), "."));
               n2081AreaTrabalho_VerTA = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2081AreaTrabalho_VerTA", StringUtil.LTrim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_VERTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2081AreaTrabalho_VerTA), "ZZZ9")));
            }
            chkAreaTrabalho_Ativo.Name = "AREATRABALHO_ATIVO";
            chkAreaTrabalho_Ativo.WebTags = "";
            chkAreaTrabalho_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAreaTrabalho_Ativo_Internalname, "TitleCaption", chkAreaTrabalho_Ativo.Caption);
            chkAreaTrabalho_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbAreaTrabalho_CalculoPFinal.ItemCount > 0 )
         {
            A642AreaTrabalho_CalculoPFinal = cmbAreaTrabalho_CalculoPFinal.getValidValue(A642AreaTrabalho_CalculoPFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A642AreaTrabalho_CalculoPFinal", A642AreaTrabalho_CalculoPFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_CALCULOPFINAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A642AreaTrabalho_CalculoPFinal, ""))));
         }
         if ( cmbAreaTrabalho_ValidaOSFM.ItemCount > 0 )
         {
            A834AreaTrabalho_ValidaOSFM = StringUtil.StrToBool( cmbAreaTrabalho_ValidaOSFM.getValidValue(StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM)));
            n834AreaTrabalho_ValidaOSFM = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A834AreaTrabalho_ValidaOSFM", A834AreaTrabalho_ValidaOSFM);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_VALIDAOSFM", GetSecureSignedToken( sPrefix, A834AreaTrabalho_ValidaOSFM));
         }
         if ( cmbAreaTrabalho_VerTA.ItemCount > 0 )
         {
            A2081AreaTrabalho_VerTA = (short)(NumberUtil.Val( cmbAreaTrabalho_VerTA.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0))), "."));
            n2081AreaTrabalho_VerTA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2081AreaTrabalho_VerTA", StringUtil.LTrim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_VERTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2081AreaTrabalho_VerTA), "ZZZ9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0V2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "AreaTrabalhoGeneral";
         context.Gx_err = 0;
      }

      protected void RF0V2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H000V2 */
            pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A25Municipio_Codigo = H000V2_A25Municipio_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
               n25Municipio_Codigo = H000V2_n25Municipio_Codigo[0];
               A23Estado_UF = H000V2_A23Estado_UF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A23Estado_UF", A23Estado_UF);
               A335Contratante_PessoaCod = H000V2_A335Contratante_PessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A335Contratante_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0)));
               A29Contratante_Codigo = H000V2_A29Contratante_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")));
               n29Contratante_Codigo = H000V2_n29Contratante_Codigo[0];
               A1216AreaTrabalho_OrganizacaoCod = H000V2_A1216AreaTrabalho_OrganizacaoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1216AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_ORGANIZACAOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9")));
               n1216AreaTrabalho_OrganizacaoCod = H000V2_n1216AreaTrabalho_OrganizacaoCod[0];
               A72AreaTrabalho_Ativo = H000V2_A72AreaTrabalho_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A72AreaTrabalho_Ativo", A72AreaTrabalho_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_ATIVO", GetSecureSignedToken( sPrefix, A72AreaTrabalho_Ativo));
               A26Municipio_Nome = H000V2_A26Municipio_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A26Municipio_Nome", A26Municipio_Nome);
               A24Estado_Nome = H000V2_A24Estado_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A24Estado_Nome", A24Estado_Nome);
               A33Contratante_Fax = H000V2_A33Contratante_Fax[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A33Contratante_Fax", A33Contratante_Fax);
               n33Contratante_Fax = H000V2_n33Contratante_Fax[0];
               A32Contratante_Ramal = H000V2_A32Contratante_Ramal[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A32Contratante_Ramal", A32Contratante_Ramal);
               n32Contratante_Ramal = H000V2_n32Contratante_Ramal[0];
               A31Contratante_Telefone = H000V2_A31Contratante_Telefone[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A31Contratante_Telefone", A31Contratante_Telefone);
               A14Contratante_Email = H000V2_A14Contratante_Email[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A14Contratante_Email", A14Contratante_Email);
               n14Contratante_Email = H000V2_n14Contratante_Email[0];
               A13Contratante_WebSite = H000V2_A13Contratante_WebSite[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A13Contratante_WebSite", A13Contratante_WebSite);
               n13Contratante_WebSite = H000V2_n13Contratante_WebSite[0];
               A11Contratante_IE = H000V2_A11Contratante_IE[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A11Contratante_IE", A11Contratante_IE);
               A10Contratante_NomeFantasia = H000V2_A10Contratante_NomeFantasia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A10Contratante_NomeFantasia", A10Contratante_NomeFantasia);
               A9Contratante_RazaoSocial = H000V2_A9Contratante_RazaoSocial[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
               n9Contratante_RazaoSocial = H000V2_n9Contratante_RazaoSocial[0];
               A12Contratante_CNPJ = H000V2_A12Contratante_CNPJ[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
               n12Contratante_CNPJ = H000V2_n12Contratante_CNPJ[0];
               A2081AreaTrabalho_VerTA = H000V2_A2081AreaTrabalho_VerTA[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2081AreaTrabalho_VerTA", StringUtil.LTrim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_VERTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2081AreaTrabalho_VerTA), "ZZZ9")));
               n2081AreaTrabalho_VerTA = H000V2_n2081AreaTrabalho_VerTA[0];
               A987AreaTrabalho_ContratadaUpdBslCod = H000V2_A987AreaTrabalho_ContratadaUpdBslCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A987AreaTrabalho_ContratadaUpdBslCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_CONTRATADAUPDBSLCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), "ZZZZZ9")));
               n987AreaTrabalho_ContratadaUpdBslCod = H000V2_n987AreaTrabalho_ContratadaUpdBslCod[0];
               A855AreaTrabalho_DiasParaPagar = H000V2_A855AreaTrabalho_DiasParaPagar[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A855AreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_DIASPARAPAGAR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A855AreaTrabalho_DiasParaPagar), "ZZZ9")));
               n855AreaTrabalho_DiasParaPagar = H000V2_n855AreaTrabalho_DiasParaPagar[0];
               A834AreaTrabalho_ValidaOSFM = H000V2_A834AreaTrabalho_ValidaOSFM[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A834AreaTrabalho_ValidaOSFM", A834AreaTrabalho_ValidaOSFM);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_VALIDAOSFM", GetSecureSignedToken( sPrefix, A834AreaTrabalho_ValidaOSFM));
               n834AreaTrabalho_ValidaOSFM = H000V2_n834AreaTrabalho_ValidaOSFM[0];
               A830AreaTrabalho_ServicoPadrao = H000V2_A830AreaTrabalho_ServicoPadrao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A830AreaTrabalho_ServicoPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_SERVICOPADRAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A830AreaTrabalho_ServicoPadrao), "ZZZZZ9")));
               n830AreaTrabalho_ServicoPadrao = H000V2_n830AreaTrabalho_ServicoPadrao[0];
               A642AreaTrabalho_CalculoPFinal = H000V2_A642AreaTrabalho_CalculoPFinal[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A642AreaTrabalho_CalculoPFinal", A642AreaTrabalho_CalculoPFinal);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_CALCULOPFINAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A642AreaTrabalho_CalculoPFinal, ""))));
               A6AreaTrabalho_Descricao = H000V2_A6AreaTrabalho_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!"))));
               A1588AreaTrabalho_SS_Codigo = H000V2_A1588AreaTrabalho_SS_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1588AreaTrabalho_SS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1588AreaTrabalho_SS_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_SS_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1588AreaTrabalho_SS_Codigo), "ZZZZZ9")));
               n1588AreaTrabalho_SS_Codigo = H000V2_n1588AreaTrabalho_SS_Codigo[0];
               A1154AreaTrabalho_TipoPlanilha = H000V2_A1154AreaTrabalho_TipoPlanilha[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1154AreaTrabalho_TipoPlanilha", StringUtil.LTrim( StringUtil.Str( (decimal)(A1154AreaTrabalho_TipoPlanilha), 2, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_TIPOPLANILHA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1154AreaTrabalho_TipoPlanilha), "Z9")));
               n1154AreaTrabalho_TipoPlanilha = H000V2_n1154AreaTrabalho_TipoPlanilha[0];
               A1214Organizacao_Nome = H000V2_A1214Organizacao_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
               n1214Organizacao_Nome = H000V2_n1214Organizacao_Nome[0];
               A25Municipio_Codigo = H000V2_A25Municipio_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
               n25Municipio_Codigo = H000V2_n25Municipio_Codigo[0];
               A335Contratante_PessoaCod = H000V2_A335Contratante_PessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A335Contratante_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0)));
               A33Contratante_Fax = H000V2_A33Contratante_Fax[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A33Contratante_Fax", A33Contratante_Fax);
               n33Contratante_Fax = H000V2_n33Contratante_Fax[0];
               A32Contratante_Ramal = H000V2_A32Contratante_Ramal[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A32Contratante_Ramal", A32Contratante_Ramal);
               n32Contratante_Ramal = H000V2_n32Contratante_Ramal[0];
               A31Contratante_Telefone = H000V2_A31Contratante_Telefone[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A31Contratante_Telefone", A31Contratante_Telefone);
               A14Contratante_Email = H000V2_A14Contratante_Email[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A14Contratante_Email", A14Contratante_Email);
               n14Contratante_Email = H000V2_n14Contratante_Email[0];
               A13Contratante_WebSite = H000V2_A13Contratante_WebSite[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A13Contratante_WebSite", A13Contratante_WebSite);
               n13Contratante_WebSite = H000V2_n13Contratante_WebSite[0];
               A11Contratante_IE = H000V2_A11Contratante_IE[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A11Contratante_IE", A11Contratante_IE);
               A10Contratante_NomeFantasia = H000V2_A10Contratante_NomeFantasia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A10Contratante_NomeFantasia", A10Contratante_NomeFantasia);
               A23Estado_UF = H000V2_A23Estado_UF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A23Estado_UF", A23Estado_UF);
               A26Municipio_Nome = H000V2_A26Municipio_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A26Municipio_Nome", A26Municipio_Nome);
               A24Estado_Nome = H000V2_A24Estado_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A24Estado_Nome", A24Estado_Nome);
               A9Contratante_RazaoSocial = H000V2_A9Contratante_RazaoSocial[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
               n9Contratante_RazaoSocial = H000V2_n9Contratante_RazaoSocial[0];
               A12Contratante_CNPJ = H000V2_A12Contratante_CNPJ[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
               n12Contratante_CNPJ = H000V2_n12Contratante_CNPJ[0];
               A1214Organizacao_Nome = H000V2_A1214Organizacao_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
               n1214Organizacao_Nome = H000V2_n1214Organizacao_Nome[0];
               /* Execute user event: E120V2 */
               E120V2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB0V0( ) ;
         }
      }

      protected void STRUP0V0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "AreaTrabalhoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110V2 */
         E110V2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1214Organizacao_Nome = StringUtil.Upper( cgiGet( edtOrganizacao_Nome_Internalname));
            n1214Organizacao_Nome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
            A1154AreaTrabalho_TipoPlanilha = (short)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_TipoPlanilha_Internalname), ",", "."));
            n1154AreaTrabalho_TipoPlanilha = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1154AreaTrabalho_TipoPlanilha", StringUtil.LTrim( StringUtil.Str( (decimal)(A1154AreaTrabalho_TipoPlanilha), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_TIPOPLANILHA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1154AreaTrabalho_TipoPlanilha), "Z9")));
            A1588AreaTrabalho_SS_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_SS_Codigo_Internalname), ",", "."));
            n1588AreaTrabalho_SS_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1588AreaTrabalho_SS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1588AreaTrabalho_SS_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_SS_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1588AreaTrabalho_SS_Codigo), "ZZZZZ9")));
            A6AreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtAreaTrabalho_Descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!"))));
            cmbAreaTrabalho_CalculoPFinal.CurrentValue = cgiGet( cmbAreaTrabalho_CalculoPFinal_Internalname);
            A642AreaTrabalho_CalculoPFinal = cgiGet( cmbAreaTrabalho_CalculoPFinal_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A642AreaTrabalho_CalculoPFinal", A642AreaTrabalho_CalculoPFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_CALCULOPFINAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A642AreaTrabalho_CalculoPFinal, ""))));
            A830AreaTrabalho_ServicoPadrao = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_ServicoPadrao_Internalname), ",", "."));
            n830AreaTrabalho_ServicoPadrao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A830AreaTrabalho_ServicoPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_SERVICOPADRAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A830AreaTrabalho_ServicoPadrao), "ZZZZZ9")));
            cmbAreaTrabalho_ValidaOSFM.CurrentValue = cgiGet( cmbAreaTrabalho_ValidaOSFM_Internalname);
            A834AreaTrabalho_ValidaOSFM = StringUtil.StrToBool( cgiGet( cmbAreaTrabalho_ValidaOSFM_Internalname));
            n834AreaTrabalho_ValidaOSFM = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A834AreaTrabalho_ValidaOSFM", A834AreaTrabalho_ValidaOSFM);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_VALIDAOSFM", GetSecureSignedToken( sPrefix, A834AreaTrabalho_ValidaOSFM));
            A855AreaTrabalho_DiasParaPagar = (short)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_DiasParaPagar_Internalname), ",", "."));
            n855AreaTrabalho_DiasParaPagar = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A855AreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_DIASPARAPAGAR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A855AreaTrabalho_DiasParaPagar), "ZZZ9")));
            A987AreaTrabalho_ContratadaUpdBslCod = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_ContratadaUpdBslCod_Internalname), ",", "."));
            n987AreaTrabalho_ContratadaUpdBslCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A987AreaTrabalho_ContratadaUpdBslCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_CONTRATADAUPDBSLCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), "ZZZZZ9")));
            cmbAreaTrabalho_VerTA.CurrentValue = cgiGet( cmbAreaTrabalho_VerTA_Internalname);
            A2081AreaTrabalho_VerTA = (short)(NumberUtil.Val( cgiGet( cmbAreaTrabalho_VerTA_Internalname), "."));
            n2081AreaTrabalho_VerTA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2081AreaTrabalho_VerTA", StringUtil.LTrim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_VERTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2081AreaTrabalho_VerTA), "ZZZ9")));
            A12Contratante_CNPJ = cgiGet( edtContratante_CNPJ_Internalname);
            n12Contratante_CNPJ = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
            A9Contratante_RazaoSocial = StringUtil.Upper( cgiGet( edtContratante_RazaoSocial_Internalname));
            n9Contratante_RazaoSocial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
            A10Contratante_NomeFantasia = StringUtil.Upper( cgiGet( edtContratante_NomeFantasia_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A10Contratante_NomeFantasia", A10Contratante_NomeFantasia);
            A11Contratante_IE = cgiGet( edtContratante_IE_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A11Contratante_IE", A11Contratante_IE);
            A13Contratante_WebSite = cgiGet( edtContratante_WebSite_Internalname);
            n13Contratante_WebSite = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A13Contratante_WebSite", A13Contratante_WebSite);
            A14Contratante_Email = cgiGet( edtContratante_Email_Internalname);
            n14Contratante_Email = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A14Contratante_Email", A14Contratante_Email);
            A31Contratante_Telefone = cgiGet( edtContratante_Telefone_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A31Contratante_Telefone", A31Contratante_Telefone);
            A32Contratante_Ramal = cgiGet( edtContratante_Ramal_Internalname);
            n32Contratante_Ramal = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A32Contratante_Ramal", A32Contratante_Ramal);
            A33Contratante_Fax = cgiGet( edtContratante_Fax_Internalname);
            n33Contratante_Fax = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A33Contratante_Fax", A33Contratante_Fax);
            A24Estado_Nome = StringUtil.Upper( cgiGet( edtEstado_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A24Estado_Nome", A24Estado_Nome);
            A26Municipio_Nome = StringUtil.Upper( cgiGet( edtMunicipio_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A26Municipio_Nome", A26Municipio_Nome);
            A72AreaTrabalho_Ativo = StringUtil.StrToBool( cgiGet( chkAreaTrabalho_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A72AreaTrabalho_Ativo", A72AreaTrabalho_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_ATIVO", GetSecureSignedToken( sPrefix, A72AreaTrabalho_Ativo));
            A1216AreaTrabalho_OrganizacaoCod = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_OrganizacaoCod_Internalname), ",", "."));
            n1216AreaTrabalho_OrganizacaoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1216AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AREATRABALHO_ORGANIZACAOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9")));
            A29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratante_Codigo_Internalname), ",", "."));
            n29Contratante_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")));
            A335Contratante_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratante_PessoaCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A335Contratante_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0)));
            A23Estado_UF = StringUtil.Upper( cgiGet( edtEstado_UF_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A23Estado_UF", A23Estado_UF);
            A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", "."));
            n25Municipio_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            /* Read saved values. */
            wcpOA5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA5AreaTrabalho_Codigo"), ",", "."));
            Dvpanel_contratante_Width = cgiGet( sPrefix+"DVPANEL_CONTRATANTE_Width");
            Dvpanel_contratante_Cls = cgiGet( sPrefix+"DVPANEL_CONTRATANTE_Cls");
            Dvpanel_contratante_Title = cgiGet( sPrefix+"DVPANEL_CONTRATANTE_Title");
            Dvpanel_contratante_Collapsible = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_CONTRATANTE_Collapsible"));
            Dvpanel_contratante_Collapsed = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_CONTRATANTE_Collapsed"));
            Dvpanel_contratante_Autowidth = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_CONTRATANTE_Autowidth"));
            Dvpanel_contratante_Autoheight = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_CONTRATANTE_Autoheight"));
            Dvpanel_contratante_Showcollapseicon = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_CONTRATANTE_Showcollapseicon"));
            Dvpanel_contratante_Iconposition = cgiGet( sPrefix+"DVPANEL_CONTRATANTE_Iconposition");
            Dvpanel_contratante_Autoscroll = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_CONTRATANTE_Autoscroll"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "AreaTrabalhoGeneral";
            A29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratante_Codigo_Internalname), ",", "."));
            n29Contratante_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("areatrabalhogeneral:[SecurityCheckFailed value for]"+"Contratante_Codigo:"+context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E110V2 */
         E110V2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E110V2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update||AV6WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete||AV6WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E120V2( )
      {
         /* Load Routine */
         edtContratante_CNPJ_Link = formatLink("viewcontratante.aspx") + "?" + UrlEncode("" +A29Contratante_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_CNPJ_Internalname, "Link", edtContratante_CNPJ_Link);
         edtContratante_Email_Link = "mailto:"+A14Contratante_Email;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_Email_Internalname, "Link", edtContratante_Email_Link);
         edtEstado_Nome_Link = formatLink("viewestado.aspx") + "?" + UrlEncode(StringUtil.RTrim(A23Estado_UF)) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtEstado_Nome_Internalname, "Link", edtEstado_Nome_Link);
         edtMunicipio_Nome_Link = formatLink("viewmunicipio.aspx") + "?" + UrlEncode("" +A25Municipio_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtMunicipio_Nome_Internalname, "Link", edtMunicipio_Nome_Link);
         edtAreaTrabalho_OrganizacaoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAreaTrabalho_OrganizacaoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_OrganizacaoCod_Visible), 5, 0)));
         edtContratante_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Codigo_Visible), 5, 0)));
         edtContratante_PessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_PessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_PessoaCod_Visible), 5, 0)));
         edtEstado_UF_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtEstado_UF_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEstado_UF_Visible), 5, 0)));
         edtMunicipio_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtMunicipio_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMunicipio_Codigo_Visible), 5, 0)));
         edtAreaTrabalho_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAreaTrabalho_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Codigo_Visible), 5, 0)));
      }

      protected void E130V2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("areatrabalho.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A5AreaTrabalho_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E140V2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("areatrabalho.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A5AreaTrabalho_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "AreaTrabalho";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "AreaTrabalho_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_0V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_0V2( true) ;
         }
         else
         {
            wb_table2_8_0V2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_0V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_117_0V2( true) ;
         }
         else
         {
            wb_table3_117_0V2( false) ;
         }
         return  ;
      }

      protected void wb_table3_117_0V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_125_0V2( true) ;
         }
         else
         {
            wb_table4_125_0V2( false) ;
         }
         return  ;
      }

      protected void wb_table4_125_0V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0V2e( true) ;
         }
         else
         {
            wb_table1_2_0V2e( false) ;
         }
      }

      protected void wb_table4_125_0V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_125_0V2e( true) ;
         }
         else
         {
            wb_table4_125_0V2e( false) ;
         }
      }

      protected void wb_table3_117_0V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_ativo_Internalname, "Ativa", "", "", lblTextblockareatrabalho_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkAreaTrabalho_Ativo_Internalname, StringUtil.BoolToStr( A72AreaTrabalho_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_117_0V2e( true) ;
         }
         else
         {
            wb_table3_117_0V2e( false) ;
         }
      }

      protected void wb_table2_8_0V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockorganizacao_nome_Internalname, "Nome", "", "", lblTextblockorganizacao_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtOrganizacao_Nome_Internalname, StringUtil.RTrim( A1214Organizacao_Nome), StringUtil.RTrim( context.localUtil.Format( A1214Organizacao_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtOrganizacao_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_tipoplanilha_Internalname, "de Planilha", "", "", lblTextblockareatrabalho_tipoplanilha_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_TipoPlanilha_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1154AreaTrabalho_TipoPlanilha), 2, 0, ",", "")), context.localUtil.Format( (decimal)(A1154AreaTrabalho_TipoPlanilha), "Z9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_TipoPlanilha_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_ss_codigo_Internalname, "Trabalho_SS_Codigo", "", "", lblTextblockareatrabalho_ss_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_SS_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1588AreaTrabalho_SS_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1588AreaTrabalho_SS_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_SS_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            wb_table5_24_0V2( true) ;
         }
         else
         {
            wb_table5_24_0V2( false) ;
         }
         return  ;
      }

      protected void wb_table5_24_0V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVPANEL_CONTRATANTEContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"DVPANEL_CONTRATANTEContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table6_64_0V2( true) ;
         }
         else
         {
            wb_table6_64_0V2( false) ;
         }
         return  ;
      }

      protected void wb_table6_64_0V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_0V2e( true) ;
         }
         else
         {
            wb_table2_8_0V2e( false) ;
         }
      }

      protected void wb_table6_64_0V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblContratante_Internalname, tblContratante_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_cnpj_Internalname, "CNPJ", "", "", lblTextblockcontratante_cnpj_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_CNPJ_Internalname, A12Contratante_CNPJ, StringUtil.RTrim( context.localUtil.Format( A12Contratante_CNPJ, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratante_CNPJ_Link, "", "", "", edtContratante_CNPJ_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_razaosocial_Internalname, "Raz�o Social", "", "", lblTextblockcontratante_razaosocial_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_RazaoSocial_Internalname, StringUtil.RTrim( A9Contratante_RazaoSocial), StringUtil.RTrim( context.localUtil.Format( A9Contratante_RazaoSocial, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_RazaoSocial_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_nomefantasia_Internalname, "Nome Fantasia", "", "", lblTextblockcontratante_nomefantasia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_NomeFantasia_Internalname, StringUtil.RTrim( A10Contratante_NomeFantasia), StringUtil.RTrim( context.localUtil.Format( A10Contratante_NomeFantasia, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_NomeFantasia_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ie_Internalname, "Insc. Estadual", "", "", lblTextblockcontratante_ie_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_IE_Internalname, StringUtil.RTrim( A11Contratante_IE), StringUtil.RTrim( context.localUtil.Format( A11Contratante_IE, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_IE_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "IE", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_website_Internalname, "Site", "", "", lblTextblockcontratante_website_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_WebSite_Internalname, A13Contratante_WebSite, StringUtil.RTrim( context.localUtil.Format( A13Contratante_WebSite, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_WebSite_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_email_Internalname, "Email", "", "", lblTextblockcontratante_email_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_Email_Internalname, A14Contratante_Email, StringUtil.RTrim( context.localUtil.Format( A14Contratante_Email, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratante_Email_Link, "", "", "", edtContratante_Email_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "email", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "Email", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_telefone_Internalname, "Telefone", "", "", lblTextblockcontratante_telefone_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            if ( context.isSmartDevice( ) )
            {
               gxphoneLink = "tel:" + StringUtil.RTrim( A31Contratante_Telefone);
            }
            GxWebStd.gx_single_line_edit( context, edtContratante_Telefone_Internalname, StringUtil.RTrim( A31Contratante_Telefone), StringUtil.RTrim( context.localUtil.Format( A31Contratante_Telefone, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", gxphoneLink, "", "", "", edtContratante_Telefone_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "tel", "", 100, "px", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "Phone", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ramal_Internalname, "Ramal", "", "", lblTextblockcontratante_ramal_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_Ramal_Internalname, StringUtil.RTrim( A32Contratante_Ramal), StringUtil.RTrim( context.localUtil.Format( A32Contratante_Ramal, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_Ramal_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 100, "px", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "Ramal", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_fax_Internalname, "Fax", "", "", lblTextblockcontratante_fax_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            if ( context.isSmartDevice( ) )
            {
               gxphoneLink = "tel:" + StringUtil.RTrim( A33Contratante_Fax);
            }
            GxWebStd.gx_single_line_edit( context, edtContratante_Fax_Internalname, StringUtil.RTrim( A33Contratante_Fax), StringUtil.RTrim( context.localUtil.Format( A33Contratante_Fax, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", gxphoneLink, "", "", "", edtContratante_Fax_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "tel", "", 100, "px", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "Phone", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockestado_nome_Internalname, "Estado", "", "", lblTextblockestado_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtEstado_Nome_Internalname, StringUtil.RTrim( A24Estado_Nome), StringUtil.RTrim( context.localUtil.Format( A24Estado_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtEstado_Nome_Link, "", "", "", edtEstado_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmunicipio_nome_Internalname, "Municipio", "", "", lblTextblockmunicipio_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMunicipio_Nome_Internalname, StringUtil.RTrim( A26Municipio_Nome), StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtMunicipio_Nome_Link, "", "", "", edtMunicipio_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_64_0V2e( true) ;
         }
         else
         {
            wb_table6_64_0V2e( false) ;
         }
      }

      protected void wb_table5_24_0V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_descricao_Internalname, "Nome", "", "", lblTextblockareatrabalho_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_Descricao_Internalname, A6AreaTrabalho_Descricao, StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_Descricao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_calculopfinal_Internalname, "Calculo P. Final", "", "", lblTextblockareatrabalho_calculopfinal_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbAreaTrabalho_CalculoPFinal, cmbAreaTrabalho_CalculoPFinal_Internalname, StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal), 1, cmbAreaTrabalho_CalculoPFinal_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_AreaTrabalhoGeneral.htm");
            cmbAreaTrabalho_CalculoPFinal.CurrentValue = StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbAreaTrabalho_CalculoPFinal_Internalname, "Values", (String)(cmbAreaTrabalho_CalculoPFinal.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_servicopadrao_Internalname, "Servi�o padr�o", "", "", lblTextblockareatrabalho_servicopadrao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_ServicoPadrao_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A830AreaTrabalho_ServicoPadrao), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_ServicoPadrao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_validaosfm_Internalname, "Valida OS FM", "", "", lblTextblockareatrabalho_validaosfm_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbAreaTrabalho_ValidaOSFM, cmbAreaTrabalho_ValidaOSFM_Internalname, StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM), 1, cmbAreaTrabalho_ValidaOSFM_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_AreaTrabalhoGeneral.htm");
            cmbAreaTrabalho_ValidaOSFM.CurrentValue = StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbAreaTrabalho_ValidaOSFM_Internalname, "Values", (String)(cmbAreaTrabalho_ValidaOSFM.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_diasparapagar_Internalname, "Dias para pagar", "", "", lblTextblockareatrabalho_diasparapagar_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_DiasParaPagar_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A855AreaTrabalho_DiasParaPagar), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_DiasParaPagar_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_contratadaupdbslcod_Internalname, "Quem atualiza o Baseline", "", "", lblTextblockareatrabalho_contratadaupdbslcod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_ContratadaUpdBslCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_ContratadaUpdBslCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_verta_Internalname, "Vers�o do TA", "", "", lblTextblockareatrabalho_verta_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbAreaTrabalho_VerTA, cmbAreaTrabalho_VerTA_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0)), 1, cmbAreaTrabalho_VerTA_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_AreaTrabalhoGeneral.htm");
            cmbAreaTrabalho_VerTA.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbAreaTrabalho_VerTA_Internalname, "Values", (String)(cmbAreaTrabalho_VerTA.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_24_0V2e( true) ;
         }
         else
         {
            wb_table5_24_0V2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A5AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0V2( ) ;
         WS0V2( ) ;
         WE0V2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA5AreaTrabalho_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA0V2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "areatrabalhogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA0V2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A5AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         }
         wcpOA5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA5AreaTrabalho_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A5AreaTrabalho_Codigo != wcpOA5AreaTrabalho_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA5AreaTrabalho_Codigo = cgiGet( sPrefix+"A5AreaTrabalho_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA5AreaTrabalho_Codigo) > 0 )
         {
            A5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA5AreaTrabalho_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         }
         else
         {
            A5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A5AreaTrabalho_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA0V2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS0V2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS0V2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A5AreaTrabalho_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA5AreaTrabalho_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A5AreaTrabalho_Codigo_CTRL", StringUtil.RTrim( sCtrlA5AreaTrabalho_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE0V2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202053021265017");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("areatrabalhogeneral.js", "?202053021265017");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockorganizacao_nome_Internalname = sPrefix+"TEXTBLOCKORGANIZACAO_NOME";
         edtOrganizacao_Nome_Internalname = sPrefix+"ORGANIZACAO_NOME";
         lblTextblockareatrabalho_tipoplanilha_Internalname = sPrefix+"TEXTBLOCKAREATRABALHO_TIPOPLANILHA";
         edtAreaTrabalho_TipoPlanilha_Internalname = sPrefix+"AREATRABALHO_TIPOPLANILHA";
         lblTextblockareatrabalho_ss_codigo_Internalname = sPrefix+"TEXTBLOCKAREATRABALHO_SS_CODIGO";
         edtAreaTrabalho_SS_Codigo_Internalname = sPrefix+"AREATRABALHO_SS_CODIGO";
         lblTextblockareatrabalho_descricao_Internalname = sPrefix+"TEXTBLOCKAREATRABALHO_DESCRICAO";
         edtAreaTrabalho_Descricao_Internalname = sPrefix+"AREATRABALHO_DESCRICAO";
         lblTextblockareatrabalho_calculopfinal_Internalname = sPrefix+"TEXTBLOCKAREATRABALHO_CALCULOPFINAL";
         cmbAreaTrabalho_CalculoPFinal_Internalname = sPrefix+"AREATRABALHO_CALCULOPFINAL";
         lblTextblockareatrabalho_servicopadrao_Internalname = sPrefix+"TEXTBLOCKAREATRABALHO_SERVICOPADRAO";
         edtAreaTrabalho_ServicoPadrao_Internalname = sPrefix+"AREATRABALHO_SERVICOPADRAO";
         lblTextblockareatrabalho_validaosfm_Internalname = sPrefix+"TEXTBLOCKAREATRABALHO_VALIDAOSFM";
         cmbAreaTrabalho_ValidaOSFM_Internalname = sPrefix+"AREATRABALHO_VALIDAOSFM";
         lblTextblockareatrabalho_diasparapagar_Internalname = sPrefix+"TEXTBLOCKAREATRABALHO_DIASPARAPAGAR";
         edtAreaTrabalho_DiasParaPagar_Internalname = sPrefix+"AREATRABALHO_DIASPARAPAGAR";
         lblTextblockareatrabalho_contratadaupdbslcod_Internalname = sPrefix+"TEXTBLOCKAREATRABALHO_CONTRATADAUPDBSLCOD";
         edtAreaTrabalho_ContratadaUpdBslCod_Internalname = sPrefix+"AREATRABALHO_CONTRATADAUPDBSLCOD";
         lblTextblockareatrabalho_verta_Internalname = sPrefix+"TEXTBLOCKAREATRABALHO_VERTA";
         cmbAreaTrabalho_VerTA_Internalname = sPrefix+"AREATRABALHO_VERTA";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         lblTextblockcontratante_cnpj_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_CNPJ";
         edtContratante_CNPJ_Internalname = sPrefix+"CONTRATANTE_CNPJ";
         lblTextblockcontratante_razaosocial_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_RAZAOSOCIAL";
         edtContratante_RazaoSocial_Internalname = sPrefix+"CONTRATANTE_RAZAOSOCIAL";
         lblTextblockcontratante_nomefantasia_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_NOMEFANTASIA";
         edtContratante_NomeFantasia_Internalname = sPrefix+"CONTRATANTE_NOMEFANTASIA";
         lblTextblockcontratante_ie_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_IE";
         edtContratante_IE_Internalname = sPrefix+"CONTRATANTE_IE";
         lblTextblockcontratante_website_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_WEBSITE";
         edtContratante_WebSite_Internalname = sPrefix+"CONTRATANTE_WEBSITE";
         lblTextblockcontratante_email_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_EMAIL";
         edtContratante_Email_Internalname = sPrefix+"CONTRATANTE_EMAIL";
         lblTextblockcontratante_telefone_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_TELEFONE";
         edtContratante_Telefone_Internalname = sPrefix+"CONTRATANTE_TELEFONE";
         lblTextblockcontratante_ramal_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_RAMAL";
         edtContratante_Ramal_Internalname = sPrefix+"CONTRATANTE_RAMAL";
         lblTextblockcontratante_fax_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_FAX";
         edtContratante_Fax_Internalname = sPrefix+"CONTRATANTE_FAX";
         lblTextblockestado_nome_Internalname = sPrefix+"TEXTBLOCKESTADO_NOME";
         edtEstado_Nome_Internalname = sPrefix+"ESTADO_NOME";
         lblTextblockmunicipio_nome_Internalname = sPrefix+"TEXTBLOCKMUNICIPIO_NOME";
         edtMunicipio_Nome_Internalname = sPrefix+"MUNICIPIO_NOME";
         tblContratante_Internalname = sPrefix+"CONTRATANTE";
         Dvpanel_contratante_Internalname = sPrefix+"DVPANEL_CONTRATANTE";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         lblTextblockareatrabalho_ativo_Internalname = sPrefix+"TEXTBLOCKAREATRABALHO_ATIVO";
         chkAreaTrabalho_Ativo_Internalname = sPrefix+"AREATRABALHO_ATIVO";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtAreaTrabalho_OrganizacaoCod_Internalname = sPrefix+"AREATRABALHO_ORGANIZACAOCOD";
         edtContratante_Codigo_Internalname = sPrefix+"CONTRATANTE_CODIGO";
         edtContratante_PessoaCod_Internalname = sPrefix+"CONTRATANTE_PESSOACOD";
         edtEstado_UF_Internalname = sPrefix+"ESTADO_UF";
         edtMunicipio_Codigo_Internalname = sPrefix+"MUNICIPIO_CODIGO";
         edtAreaTrabalho_Codigo_Internalname = sPrefix+"AREATRABALHO_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbAreaTrabalho_VerTA_Jsonclick = "";
         edtAreaTrabalho_ContratadaUpdBslCod_Jsonclick = "";
         edtAreaTrabalho_DiasParaPagar_Jsonclick = "";
         cmbAreaTrabalho_ValidaOSFM_Jsonclick = "";
         edtAreaTrabalho_ServicoPadrao_Jsonclick = "";
         cmbAreaTrabalho_CalculoPFinal_Jsonclick = "";
         edtAreaTrabalho_Descricao_Jsonclick = "";
         edtMunicipio_Nome_Jsonclick = "";
         edtEstado_Nome_Jsonclick = "";
         edtContratante_Fax_Jsonclick = "";
         edtContratante_Ramal_Jsonclick = "";
         edtContratante_Telefone_Jsonclick = "";
         edtContratante_Email_Jsonclick = "";
         edtContratante_WebSite_Jsonclick = "";
         edtContratante_IE_Jsonclick = "";
         edtContratante_NomeFantasia_Jsonclick = "";
         edtContratante_RazaoSocial_Jsonclick = "";
         edtContratante_CNPJ_Jsonclick = "";
         edtAreaTrabalho_SS_Codigo_Jsonclick = "";
         edtAreaTrabalho_TipoPlanilha_Jsonclick = "";
         edtOrganizacao_Nome_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtMunicipio_Nome_Link = "";
         edtEstado_Nome_Link = "";
         edtContratante_Email_Link = "";
         edtContratante_CNPJ_Link = "";
         chkAreaTrabalho_Ativo.Caption = "";
         edtAreaTrabalho_Codigo_Jsonclick = "";
         edtAreaTrabalho_Codigo_Visible = 1;
         edtMunicipio_Codigo_Jsonclick = "";
         edtMunicipio_Codigo_Visible = 1;
         edtEstado_UF_Jsonclick = "";
         edtEstado_UF_Visible = 1;
         edtContratante_PessoaCod_Jsonclick = "";
         edtContratante_PessoaCod_Visible = 1;
         edtContratante_Codigo_Jsonclick = "";
         edtContratante_Codigo_Visible = 1;
         edtAreaTrabalho_OrganizacaoCod_Jsonclick = "";
         edtAreaTrabalho_OrganizacaoCod_Visible = 1;
         Dvpanel_contratante_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_contratante_Iconposition = "left";
         Dvpanel_contratante_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_contratante_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_contratante_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_contratante_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_contratante_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_contratante_Title = "Contratante";
         Dvpanel_contratante_Cls = "GXUI-DVelop-Panel";
         Dvpanel_contratante_Width = "100%";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E130V2',iparms:[{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E140V2',iparms:[{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A6AreaTrabalho_Descricao = "";
         A642AreaTrabalho_CalculoPFinal = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         A23Estado_UF = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H000V2_A5AreaTrabalho_Codigo = new int[1] ;
         H000V2_A25Municipio_Codigo = new int[1] ;
         H000V2_n25Municipio_Codigo = new bool[] {false} ;
         H000V2_A23Estado_UF = new String[] {""} ;
         H000V2_A335Contratante_PessoaCod = new int[1] ;
         H000V2_A29Contratante_Codigo = new int[1] ;
         H000V2_n29Contratante_Codigo = new bool[] {false} ;
         H000V2_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         H000V2_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         H000V2_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H000V2_A26Municipio_Nome = new String[] {""} ;
         H000V2_A24Estado_Nome = new String[] {""} ;
         H000V2_A33Contratante_Fax = new String[] {""} ;
         H000V2_n33Contratante_Fax = new bool[] {false} ;
         H000V2_A32Contratante_Ramal = new String[] {""} ;
         H000V2_n32Contratante_Ramal = new bool[] {false} ;
         H000V2_A31Contratante_Telefone = new String[] {""} ;
         H000V2_A14Contratante_Email = new String[] {""} ;
         H000V2_n14Contratante_Email = new bool[] {false} ;
         H000V2_A13Contratante_WebSite = new String[] {""} ;
         H000V2_n13Contratante_WebSite = new bool[] {false} ;
         H000V2_A11Contratante_IE = new String[] {""} ;
         H000V2_A10Contratante_NomeFantasia = new String[] {""} ;
         H000V2_A9Contratante_RazaoSocial = new String[] {""} ;
         H000V2_n9Contratante_RazaoSocial = new bool[] {false} ;
         H000V2_A12Contratante_CNPJ = new String[] {""} ;
         H000V2_n12Contratante_CNPJ = new bool[] {false} ;
         H000V2_A2081AreaTrabalho_VerTA = new short[1] ;
         H000V2_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         H000V2_A987AreaTrabalho_ContratadaUpdBslCod = new int[1] ;
         H000V2_n987AreaTrabalho_ContratadaUpdBslCod = new bool[] {false} ;
         H000V2_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         H000V2_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         H000V2_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         H000V2_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         H000V2_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         H000V2_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         H000V2_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         H000V2_A6AreaTrabalho_Descricao = new String[] {""} ;
         H000V2_A1588AreaTrabalho_SS_Codigo = new int[1] ;
         H000V2_n1588AreaTrabalho_SS_Codigo = new bool[] {false} ;
         H000V2_A1154AreaTrabalho_TipoPlanilha = new short[1] ;
         H000V2_n1154AreaTrabalho_TipoPlanilha = new bool[] {false} ;
         H000V2_A1214Organizacao_Nome = new String[] {""} ;
         H000V2_n1214Organizacao_Nome = new bool[] {false} ;
         A26Municipio_Nome = "";
         A24Estado_Nome = "";
         A33Contratante_Fax = "";
         A32Contratante_Ramal = "";
         A31Contratante_Telefone = "";
         A14Contratante_Email = "";
         A13Contratante_WebSite = "";
         A11Contratante_IE = "";
         A10Contratante_NomeFantasia = "";
         A9Contratante_RazaoSocial = "";
         A12Contratante_CNPJ = "";
         A1214Organizacao_Nome = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockareatrabalho_ativo_Jsonclick = "";
         lblTextblockorganizacao_nome_Jsonclick = "";
         lblTextblockareatrabalho_tipoplanilha_Jsonclick = "";
         lblTextblockareatrabalho_ss_codigo_Jsonclick = "";
         lblTextblockcontratante_cnpj_Jsonclick = "";
         lblTextblockcontratante_razaosocial_Jsonclick = "";
         lblTextblockcontratante_nomefantasia_Jsonclick = "";
         lblTextblockcontratante_ie_Jsonclick = "";
         lblTextblockcontratante_website_Jsonclick = "";
         lblTextblockcontratante_email_Jsonclick = "";
         lblTextblockcontratante_telefone_Jsonclick = "";
         gxphoneLink = "";
         lblTextblockcontratante_ramal_Jsonclick = "";
         lblTextblockcontratante_fax_Jsonclick = "";
         lblTextblockestado_nome_Jsonclick = "";
         lblTextblockmunicipio_nome_Jsonclick = "";
         lblTextblockareatrabalho_descricao_Jsonclick = "";
         lblTextblockareatrabalho_calculopfinal_Jsonclick = "";
         lblTextblockareatrabalho_servicopadrao_Jsonclick = "";
         lblTextblockareatrabalho_validaosfm_Jsonclick = "";
         lblTextblockareatrabalho_diasparapagar_Jsonclick = "";
         lblTextblockareatrabalho_contratadaupdbslcod_Jsonclick = "";
         lblTextblockareatrabalho_verta_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA5AreaTrabalho_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.areatrabalhogeneral__default(),
            new Object[][] {
                new Object[] {
               H000V2_A5AreaTrabalho_Codigo, H000V2_A25Municipio_Codigo, H000V2_n25Municipio_Codigo, H000V2_A23Estado_UF, H000V2_A335Contratante_PessoaCod, H000V2_A29Contratante_Codigo, H000V2_n29Contratante_Codigo, H000V2_A1216AreaTrabalho_OrganizacaoCod, H000V2_n1216AreaTrabalho_OrganizacaoCod, H000V2_A72AreaTrabalho_Ativo,
               H000V2_A26Municipio_Nome, H000V2_A24Estado_Nome, H000V2_A33Contratante_Fax, H000V2_n33Contratante_Fax, H000V2_A32Contratante_Ramal, H000V2_n32Contratante_Ramal, H000V2_A31Contratante_Telefone, H000V2_A14Contratante_Email, H000V2_n14Contratante_Email, H000V2_A13Contratante_WebSite,
               H000V2_n13Contratante_WebSite, H000V2_A11Contratante_IE, H000V2_A10Contratante_NomeFantasia, H000V2_A9Contratante_RazaoSocial, H000V2_n9Contratante_RazaoSocial, H000V2_A12Contratante_CNPJ, H000V2_n12Contratante_CNPJ, H000V2_A2081AreaTrabalho_VerTA, H000V2_n2081AreaTrabalho_VerTA, H000V2_A987AreaTrabalho_ContratadaUpdBslCod,
               H000V2_n987AreaTrabalho_ContratadaUpdBslCod, H000V2_A855AreaTrabalho_DiasParaPagar, H000V2_n855AreaTrabalho_DiasParaPagar, H000V2_A834AreaTrabalho_ValidaOSFM, H000V2_n834AreaTrabalho_ValidaOSFM, H000V2_A830AreaTrabalho_ServicoPadrao, H000V2_n830AreaTrabalho_ServicoPadrao, H000V2_A642AreaTrabalho_CalculoPFinal, H000V2_A6AreaTrabalho_Descricao, H000V2_A1588AreaTrabalho_SS_Codigo,
               H000V2_n1588AreaTrabalho_SS_Codigo, H000V2_A1154AreaTrabalho_TipoPlanilha, H000V2_n1154AreaTrabalho_TipoPlanilha, H000V2_A1214Organizacao_Nome, H000V2_n1214Organizacao_Nome
               }
            }
         );
         AV14Pgmname = "AreaTrabalhoGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "AreaTrabalhoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A1154AreaTrabalho_TipoPlanilha ;
      private short A855AreaTrabalho_DiasParaPagar ;
      private short A2081AreaTrabalho_VerTA ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A5AreaTrabalho_Codigo ;
      private int wcpOA5AreaTrabalho_Codigo ;
      private int A1588AreaTrabalho_SS_Codigo ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int A987AreaTrabalho_ContratadaUpdBslCod ;
      private int A1216AreaTrabalho_OrganizacaoCod ;
      private int A29Contratante_Codigo ;
      private int edtAreaTrabalho_OrganizacaoCod_Visible ;
      private int edtContratante_Codigo_Visible ;
      private int A335Contratante_PessoaCod ;
      private int edtContratante_PessoaCod_Visible ;
      private int edtEstado_UF_Visible ;
      private int A25Municipio_Codigo ;
      private int edtMunicipio_Codigo_Visible ;
      private int edtAreaTrabalho_Codigo_Visible ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7AreaTrabalho_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A642AreaTrabalho_CalculoPFinal ;
      private String Dvpanel_contratante_Width ;
      private String Dvpanel_contratante_Cls ;
      private String Dvpanel_contratante_Title ;
      private String Dvpanel_contratante_Iconposition ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtAreaTrabalho_OrganizacaoCod_Internalname ;
      private String edtAreaTrabalho_OrganizacaoCod_Jsonclick ;
      private String edtContratante_Codigo_Internalname ;
      private String edtContratante_Codigo_Jsonclick ;
      private String edtContratante_PessoaCod_Internalname ;
      private String edtContratante_PessoaCod_Jsonclick ;
      private String edtEstado_UF_Internalname ;
      private String A23Estado_UF ;
      private String edtEstado_UF_Jsonclick ;
      private String edtMunicipio_Codigo_Internalname ;
      private String edtMunicipio_Codigo_Jsonclick ;
      private String edtAreaTrabalho_Codigo_Internalname ;
      private String edtAreaTrabalho_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkAreaTrabalho_Ativo_Internalname ;
      private String scmdbuf ;
      private String A26Municipio_Nome ;
      private String A24Estado_Nome ;
      private String A33Contratante_Fax ;
      private String A32Contratante_Ramal ;
      private String A31Contratante_Telefone ;
      private String A11Contratante_IE ;
      private String A10Contratante_NomeFantasia ;
      private String A9Contratante_RazaoSocial ;
      private String A1214Organizacao_Nome ;
      private String edtOrganizacao_Nome_Internalname ;
      private String edtAreaTrabalho_TipoPlanilha_Internalname ;
      private String edtAreaTrabalho_SS_Codigo_Internalname ;
      private String edtAreaTrabalho_Descricao_Internalname ;
      private String cmbAreaTrabalho_CalculoPFinal_Internalname ;
      private String edtAreaTrabalho_ServicoPadrao_Internalname ;
      private String cmbAreaTrabalho_ValidaOSFM_Internalname ;
      private String edtAreaTrabalho_DiasParaPagar_Internalname ;
      private String edtAreaTrabalho_ContratadaUpdBslCod_Internalname ;
      private String cmbAreaTrabalho_VerTA_Internalname ;
      private String edtContratante_CNPJ_Internalname ;
      private String edtContratante_RazaoSocial_Internalname ;
      private String edtContratante_NomeFantasia_Internalname ;
      private String edtContratante_IE_Internalname ;
      private String edtContratante_WebSite_Internalname ;
      private String edtContratante_Email_Internalname ;
      private String edtContratante_Telefone_Internalname ;
      private String edtContratante_Ramal_Internalname ;
      private String edtContratante_Fax_Internalname ;
      private String edtEstado_Nome_Internalname ;
      private String edtMunicipio_Nome_Internalname ;
      private String hsh ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtContratante_CNPJ_Link ;
      private String edtContratante_Email_Link ;
      private String edtEstado_Nome_Link ;
      private String edtMunicipio_Nome_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockareatrabalho_ativo_Internalname ;
      private String lblTextblockareatrabalho_ativo_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockorganizacao_nome_Internalname ;
      private String lblTextblockorganizacao_nome_Jsonclick ;
      private String edtOrganizacao_Nome_Jsonclick ;
      private String lblTextblockareatrabalho_tipoplanilha_Internalname ;
      private String lblTextblockareatrabalho_tipoplanilha_Jsonclick ;
      private String edtAreaTrabalho_TipoPlanilha_Jsonclick ;
      private String lblTextblockareatrabalho_ss_codigo_Internalname ;
      private String lblTextblockareatrabalho_ss_codigo_Jsonclick ;
      private String edtAreaTrabalho_SS_Codigo_Jsonclick ;
      private String tblContratante_Internalname ;
      private String lblTextblockcontratante_cnpj_Internalname ;
      private String lblTextblockcontratante_cnpj_Jsonclick ;
      private String edtContratante_CNPJ_Jsonclick ;
      private String lblTextblockcontratante_razaosocial_Internalname ;
      private String lblTextblockcontratante_razaosocial_Jsonclick ;
      private String edtContratante_RazaoSocial_Jsonclick ;
      private String lblTextblockcontratante_nomefantasia_Internalname ;
      private String lblTextblockcontratante_nomefantasia_Jsonclick ;
      private String edtContratante_NomeFantasia_Jsonclick ;
      private String lblTextblockcontratante_ie_Internalname ;
      private String lblTextblockcontratante_ie_Jsonclick ;
      private String edtContratante_IE_Jsonclick ;
      private String lblTextblockcontratante_website_Internalname ;
      private String lblTextblockcontratante_website_Jsonclick ;
      private String edtContratante_WebSite_Jsonclick ;
      private String lblTextblockcontratante_email_Internalname ;
      private String lblTextblockcontratante_email_Jsonclick ;
      private String edtContratante_Email_Jsonclick ;
      private String lblTextblockcontratante_telefone_Internalname ;
      private String lblTextblockcontratante_telefone_Jsonclick ;
      private String gxphoneLink ;
      private String edtContratante_Telefone_Jsonclick ;
      private String lblTextblockcontratante_ramal_Internalname ;
      private String lblTextblockcontratante_ramal_Jsonclick ;
      private String edtContratante_Ramal_Jsonclick ;
      private String lblTextblockcontratante_fax_Internalname ;
      private String lblTextblockcontratante_fax_Jsonclick ;
      private String edtContratante_Fax_Jsonclick ;
      private String lblTextblockestado_nome_Internalname ;
      private String lblTextblockestado_nome_Jsonclick ;
      private String edtEstado_Nome_Jsonclick ;
      private String lblTextblockmunicipio_nome_Internalname ;
      private String lblTextblockmunicipio_nome_Jsonclick ;
      private String edtMunicipio_Nome_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTextblockareatrabalho_descricao_Internalname ;
      private String lblTextblockareatrabalho_descricao_Jsonclick ;
      private String edtAreaTrabalho_Descricao_Jsonclick ;
      private String lblTextblockareatrabalho_calculopfinal_Internalname ;
      private String lblTextblockareatrabalho_calculopfinal_Jsonclick ;
      private String cmbAreaTrabalho_CalculoPFinal_Jsonclick ;
      private String lblTextblockareatrabalho_servicopadrao_Internalname ;
      private String lblTextblockareatrabalho_servicopadrao_Jsonclick ;
      private String edtAreaTrabalho_ServicoPadrao_Jsonclick ;
      private String lblTextblockareatrabalho_validaosfm_Internalname ;
      private String lblTextblockareatrabalho_validaosfm_Jsonclick ;
      private String cmbAreaTrabalho_ValidaOSFM_Jsonclick ;
      private String lblTextblockareatrabalho_diasparapagar_Internalname ;
      private String lblTextblockareatrabalho_diasparapagar_Jsonclick ;
      private String edtAreaTrabalho_DiasParaPagar_Jsonclick ;
      private String lblTextblockareatrabalho_contratadaupdbslcod_Internalname ;
      private String lblTextblockareatrabalho_contratadaupdbslcod_Jsonclick ;
      private String edtAreaTrabalho_ContratadaUpdBslCod_Jsonclick ;
      private String lblTextblockareatrabalho_verta_Internalname ;
      private String lblTextblockareatrabalho_verta_Jsonclick ;
      private String cmbAreaTrabalho_VerTA_Jsonclick ;
      private String sCtrlA5AreaTrabalho_Codigo ;
      private String Dvpanel_contratante_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A834AreaTrabalho_ValidaOSFM ;
      private bool A72AreaTrabalho_Ativo ;
      private bool Dvpanel_contratante_Collapsible ;
      private bool Dvpanel_contratante_Collapsed ;
      private bool Dvpanel_contratante_Autowidth ;
      private bool Dvpanel_contratante_Autoheight ;
      private bool Dvpanel_contratante_Showcollapseicon ;
      private bool Dvpanel_contratante_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n834AreaTrabalho_ValidaOSFM ;
      private bool n2081AreaTrabalho_VerTA ;
      private bool n25Municipio_Codigo ;
      private bool n29Contratante_Codigo ;
      private bool n1216AreaTrabalho_OrganizacaoCod ;
      private bool n33Contratante_Fax ;
      private bool n32Contratante_Ramal ;
      private bool n14Contratante_Email ;
      private bool n13Contratante_WebSite ;
      private bool n9Contratante_RazaoSocial ;
      private bool n12Contratante_CNPJ ;
      private bool n987AreaTrabalho_ContratadaUpdBslCod ;
      private bool n855AreaTrabalho_DiasParaPagar ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool n1588AreaTrabalho_SS_Codigo ;
      private bool n1154AreaTrabalho_TipoPlanilha ;
      private bool n1214Organizacao_Nome ;
      private bool returnInSub ;
      private String A6AreaTrabalho_Descricao ;
      private String A14Contratante_Email ;
      private String A13Contratante_WebSite ;
      private String A12Contratante_CNPJ ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbAreaTrabalho_CalculoPFinal ;
      private GXCombobox cmbAreaTrabalho_ValidaOSFM ;
      private GXCombobox cmbAreaTrabalho_VerTA ;
      private GXCheckbox chkAreaTrabalho_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H000V2_A5AreaTrabalho_Codigo ;
      private int[] H000V2_A25Municipio_Codigo ;
      private bool[] H000V2_n25Municipio_Codigo ;
      private String[] H000V2_A23Estado_UF ;
      private int[] H000V2_A335Contratante_PessoaCod ;
      private int[] H000V2_A29Contratante_Codigo ;
      private bool[] H000V2_n29Contratante_Codigo ;
      private int[] H000V2_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] H000V2_n1216AreaTrabalho_OrganizacaoCod ;
      private bool[] H000V2_A72AreaTrabalho_Ativo ;
      private String[] H000V2_A26Municipio_Nome ;
      private String[] H000V2_A24Estado_Nome ;
      private String[] H000V2_A33Contratante_Fax ;
      private bool[] H000V2_n33Contratante_Fax ;
      private String[] H000V2_A32Contratante_Ramal ;
      private bool[] H000V2_n32Contratante_Ramal ;
      private String[] H000V2_A31Contratante_Telefone ;
      private String[] H000V2_A14Contratante_Email ;
      private bool[] H000V2_n14Contratante_Email ;
      private String[] H000V2_A13Contratante_WebSite ;
      private bool[] H000V2_n13Contratante_WebSite ;
      private String[] H000V2_A11Contratante_IE ;
      private String[] H000V2_A10Contratante_NomeFantasia ;
      private String[] H000V2_A9Contratante_RazaoSocial ;
      private bool[] H000V2_n9Contratante_RazaoSocial ;
      private String[] H000V2_A12Contratante_CNPJ ;
      private bool[] H000V2_n12Contratante_CNPJ ;
      private short[] H000V2_A2081AreaTrabalho_VerTA ;
      private bool[] H000V2_n2081AreaTrabalho_VerTA ;
      private int[] H000V2_A987AreaTrabalho_ContratadaUpdBslCod ;
      private bool[] H000V2_n987AreaTrabalho_ContratadaUpdBslCod ;
      private short[] H000V2_A855AreaTrabalho_DiasParaPagar ;
      private bool[] H000V2_n855AreaTrabalho_DiasParaPagar ;
      private bool[] H000V2_A834AreaTrabalho_ValidaOSFM ;
      private bool[] H000V2_n834AreaTrabalho_ValidaOSFM ;
      private int[] H000V2_A830AreaTrabalho_ServicoPadrao ;
      private bool[] H000V2_n830AreaTrabalho_ServicoPadrao ;
      private String[] H000V2_A642AreaTrabalho_CalculoPFinal ;
      private String[] H000V2_A6AreaTrabalho_Descricao ;
      private int[] H000V2_A1588AreaTrabalho_SS_Codigo ;
      private bool[] H000V2_n1588AreaTrabalho_SS_Codigo ;
      private short[] H000V2_A1154AreaTrabalho_TipoPlanilha ;
      private bool[] H000V2_n1154AreaTrabalho_TipoPlanilha ;
      private String[] H000V2_A1214Organizacao_Nome ;
      private bool[] H000V2_n1214Organizacao_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class areatrabalhogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000V2 ;
          prmH000V2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H000V2", "SELECT T1.[AreaTrabalho_Codigo], T2.[Municipio_Codigo], T3.[Estado_UF], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[Contratante_Codigo], T1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T1.[AreaTrabalho_Ativo], T3.[Municipio_Nome], T4.[Estado_Nome], T2.[Contratante_Fax], T2.[Contratante_Ramal], T2.[Contratante_Telefone], T2.[Contratante_Email], T2.[Contratante_WebSite], T2.[Contratante_IE], T2.[Contratante_NomeFantasia], T5.[Pessoa_Nome] AS Contratante_RazaoSocial, T5.[Pessoa_Docto] AS Contratante_CNPJ, T1.[AreaTrabalho_VerTA], T1.[AreaTrabalho_ContratadaUpdBslCod], T1.[AreaTrabalho_DiasParaPagar], T1.[AreaTrabalho_ValidaOSFM], T1.[AreaTrabalho_ServicoPadrao], T1.[AreaTrabalho_CalculoPFinal], T1.[AreaTrabalho_Descricao], T1.[AreaTrabalho_SS_Codigo], T1.[AreaTrabalho_TipoPlanilha], T6.[Organizacao_Nome] FROM ((((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Estado] T4 WITH (NOLOCK) ON T4.[Estado_UF] = T3.[Estado_UF]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) LEFT JOIN [Organizacao] T6 WITH (NOLOCK) ON T6.[Organizacao_Codigo] = T1.[AreaTrabalho_OrganizacaoCod]) WHERE T1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000V2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[12])[0] = rslt.getString(10, 20) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((String[]) buf[16])[0] = rslt.getString(12, 20) ;
                ((String[]) buf[17])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((String[]) buf[19])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((String[]) buf[21])[0] = rslt.getString(15, 15) ;
                ((String[]) buf[22])[0] = rslt.getString(16, 100) ;
                ((String[]) buf[23])[0] = rslt.getString(17, 100) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(17);
                ((String[]) buf[25])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(18);
                ((short[]) buf[27])[0] = rslt.getShort(19) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(19);
                ((int[]) buf[29])[0] = rslt.getInt(20) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(20);
                ((short[]) buf[31])[0] = rslt.getShort(21) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(21);
                ((bool[]) buf[33])[0] = rslt.getBool(22) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((String[]) buf[37])[0] = rslt.getString(24, 2) ;
                ((String[]) buf[38])[0] = rslt.getVarchar(25) ;
                ((int[]) buf[39])[0] = rslt.getInt(26) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(26);
                ((short[]) buf[41])[0] = rslt.getShort(27) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(27);
                ((String[]) buf[43])[0] = rslt.getString(28, 50) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(28);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
