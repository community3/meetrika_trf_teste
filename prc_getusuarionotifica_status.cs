/*
               File: PRC_GetUsuarioNotifica_Status
        Description: Get Usuario Notifica_Status
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:15.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getusuarionotifica_status : GXProcedure
   {
      public prc_getusuarionotifica_status( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getusuarionotifica_status( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_UsuarioNotifica_Codigo ,
                           out String aP1_UsuarioNotifica_NoStatus )
      {
         this.A2077UsuarioNotifica_Codigo = aP0_UsuarioNotifica_Codigo;
         this.AV8UsuarioNotifica_NoStatus = "" ;
         initialize();
         executePrivate();
         aP0_UsuarioNotifica_Codigo=this.A2077UsuarioNotifica_Codigo;
         aP1_UsuarioNotifica_NoStatus=this.AV8UsuarioNotifica_NoStatus;
      }

      public String executeUdp( ref int aP0_UsuarioNotifica_Codigo )
      {
         this.A2077UsuarioNotifica_Codigo = aP0_UsuarioNotifica_Codigo;
         this.AV8UsuarioNotifica_NoStatus = "" ;
         initialize();
         executePrivate();
         aP0_UsuarioNotifica_Codigo=this.A2077UsuarioNotifica_Codigo;
         aP1_UsuarioNotifica_NoStatus=this.AV8UsuarioNotifica_NoStatus;
         return AV8UsuarioNotifica_NoStatus ;
      }

      public void executeSubmit( ref int aP0_UsuarioNotifica_Codigo ,
                                 out String aP1_UsuarioNotifica_NoStatus )
      {
         prc_getusuarionotifica_status objprc_getusuarionotifica_status;
         objprc_getusuarionotifica_status = new prc_getusuarionotifica_status();
         objprc_getusuarionotifica_status.A2077UsuarioNotifica_Codigo = aP0_UsuarioNotifica_Codigo;
         objprc_getusuarionotifica_status.AV8UsuarioNotifica_NoStatus = "" ;
         objprc_getusuarionotifica_status.context.SetSubmitInitialConfig(context);
         objprc_getusuarionotifica_status.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getusuarionotifica_status);
         aP0_UsuarioNotifica_Codigo=this.A2077UsuarioNotifica_Codigo;
         aP1_UsuarioNotifica_NoStatus=this.AV8UsuarioNotifica_NoStatus;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getusuarionotifica_status)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00X92 */
         pr_default.execute(0, new Object[] {A2077UsuarioNotifica_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2079UsuarioNotifica_NoStatus = P00X92_A2079UsuarioNotifica_NoStatus[0];
            AV8UsuarioNotifica_NoStatus = A2079UsuarioNotifica_NoStatus;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00X92_A2077UsuarioNotifica_Codigo = new int[1] ;
         P00X92_A2079UsuarioNotifica_NoStatus = new String[] {""} ;
         A2079UsuarioNotifica_NoStatus = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getusuarionotifica_status__default(),
            new Object[][] {
                new Object[] {
               P00X92_A2077UsuarioNotifica_Codigo, P00X92_A2079UsuarioNotifica_NoStatus
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A2077UsuarioNotifica_Codigo ;
      private String scmdbuf ;
      private String AV8UsuarioNotifica_NoStatus ;
      private String A2079UsuarioNotifica_NoStatus ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_UsuarioNotifica_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00X92_A2077UsuarioNotifica_Codigo ;
      private String[] P00X92_A2079UsuarioNotifica_NoStatus ;
      private String aP1_UsuarioNotifica_NoStatus ;
   }

   public class prc_getusuarionotifica_status__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00X92 ;
          prmP00X92 = new Object[] {
          new Object[] {"@UsuarioNotifica_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00X92", "SELECT TOP 1 [UsuarioNotifica_Codigo], [UsuarioNotifica_NoStatus] FROM [UsuarioNotifica] WITH (NOLOCK) WHERE [UsuarioNotifica_Codigo] = @UsuarioNotifica_Codigo ORDER BY [UsuarioNotifica_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00X92,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
