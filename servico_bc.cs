/*
               File: Servico_BC
        Description: Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:15:32.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servico_bc : GXHttpHandler, IGxSilentTrn
   {
      public servico_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public servico_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow0V130( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey0V130( ) ;
         standaloneModal( ) ;
         AddRow0V130( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E110V2 */
            E110V2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z155Servico_Codigo = A155Servico_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_0V0( )
      {
         BeforeValidate0V130( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0V130( ) ;
            }
            else
            {
               CheckExtendedTable0V130( ) ;
               if ( AnyError == 0 )
               {
                  ZM0V130( 22) ;
                  ZM0V130( 23) ;
                  ZM0V130( 24) ;
                  ZM0V130( 25) ;
               }
               CloseExtendedTableCursors0V130( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E120V2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV25Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV26GXV1 = 1;
            while ( AV26GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV26GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ServicoGrupo_Codigo") == 0 )
               {
                  AV11Insert_ServicoGrupo_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Servico_UO") == 0 )
               {
                  AV15Insert_Servico_UO = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Servico_Vinculado") == 0 )
               {
                  AV14Insert_Servico_Vinculado = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Servico_LinNegCod") == 0 )
               {
                  AV24Insert_Servico_LinNegCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV26GXV1 = (int)(AV26GXV1+1);
            }
         }
      }

      protected void E110V2( )
      {
         /* After Trn Routine */
         new wwpbaseobjects.audittransaction(context ).execute(  AV19AuditingObject,  AV25Pgmname) ;
      }

      protected void E130V2( )
      {
         /* 'DoSelectUO' Routine */
         context.PopUp(formatLink("wp_selecionauo.aspx") + "?" + UrlEncode("" +AV17Servico_UO), new Object[] {"AV17Servico_UO"});
      }

      protected void S112( )
      {
         /* 'ATTRIBUTESSECURITYCODE' Routine */
      }

      protected void E140V2( )
      {
         /* Servico_IsPublico_Click Routine */
         if ( A1635Servico_IsPublico )
         {
            GXt_int1 = AV22Responsavel_Codigo;
            new prc_getservico_responsavel(context ).execute( ref  AV7Servico_Codigo, out  GXt_int1) ;
            AV22Responsavel_Codigo = GXt_int1;
         }
         else
         {
            AV22Responsavel_Codigo = 0;
         }
      }

      protected void ZM0V130( short GX_JID )
      {
         if ( ( GX_JID == 21 ) || ( GX_JID == 0 ) )
         {
            Z608Servico_Nome = A608Servico_Nome;
            Z605Servico_Sigla = A605Servico_Sigla;
            Z1077Servico_UORespExclusiva = A1077Servico_UORespExclusiva;
            Z889Servico_Terceriza = A889Servico_Terceriza;
            Z1061Servico_Tela = A1061Servico_Tela;
            Z1072Servico_Atende = A1072Servico_Atende;
            Z1429Servico_ObrigaValores = A1429Servico_ObrigaValores;
            Z1436Servico_ObjetoControle = A1436Servico_ObjetoControle;
            Z1530Servico_TipoHierarquia = A1530Servico_TipoHierarquia;
            Z1534Servico_PercTmp = A1534Servico_PercTmp;
            Z1535Servico_PercPgm = A1535Servico_PercPgm;
            Z1536Servico_PercCnc = A1536Servico_PercCnc;
            Z1545Servico_Anterior = A1545Servico_Anterior;
            Z1546Servico_Posterior = A1546Servico_Posterior;
            Z632Servico_Ativo = A632Servico_Ativo;
            Z1635Servico_IsPublico = A1635Servico_IsPublico;
            Z2092Servico_IsOrigemReferencia = A2092Servico_IsOrigemReferencia;
            Z2131Servico_PausaSLA = A2131Servico_PausaSLA;
            Z157ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
            Z633Servico_UO = A633Servico_UO;
            Z2047Servico_LinNegCod = A2047Servico_LinNegCod;
            Z631Servico_Vinculado = A631Servico_Vinculado;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
         }
         if ( ( GX_JID == 22 ) || ( GX_JID == 0 ) )
         {
            Z158ServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
         }
         if ( ( GX_JID == 23 ) || ( GX_JID == 0 ) )
         {
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
         }
         if ( ( GX_JID == 24 ) || ( GX_JID == 0 ) )
         {
            Z2048Servico_LinNegDsc = A2048Servico_LinNegDsc;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
         }
         if ( ( GX_JID == 25 ) || ( GX_JID == 0 ) )
         {
            Z1557Servico_VincSigla = A1557Servico_VincSigla;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
         }
         if ( GX_JID == -21 )
         {
            Z155Servico_Codigo = A155Servico_Codigo;
            Z608Servico_Nome = A608Servico_Nome;
            Z156Servico_Descricao = A156Servico_Descricao;
            Z605Servico_Sigla = A605Servico_Sigla;
            Z1077Servico_UORespExclusiva = A1077Servico_UORespExclusiva;
            Z889Servico_Terceriza = A889Servico_Terceriza;
            Z1061Servico_Tela = A1061Servico_Tela;
            Z1072Servico_Atende = A1072Servico_Atende;
            Z1429Servico_ObrigaValores = A1429Servico_ObrigaValores;
            Z1436Servico_ObjetoControle = A1436Servico_ObjetoControle;
            Z1530Servico_TipoHierarquia = A1530Servico_TipoHierarquia;
            Z1534Servico_PercTmp = A1534Servico_PercTmp;
            Z1535Servico_PercPgm = A1535Servico_PercPgm;
            Z1536Servico_PercCnc = A1536Servico_PercCnc;
            Z1545Servico_Anterior = A1545Servico_Anterior;
            Z1546Servico_Posterior = A1546Servico_Posterior;
            Z632Servico_Ativo = A632Servico_Ativo;
            Z1635Servico_IsPublico = A1635Servico_IsPublico;
            Z2092Servico_IsOrigemReferencia = A2092Servico_IsOrigemReferencia;
            Z2131Servico_PausaSLA = A2131Servico_PausaSLA;
            Z157ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
            Z633Servico_UO = A633Servico_UO;
            Z2047Servico_LinNegCod = A2047Servico_LinNegCod;
            Z631Servico_Vinculado = A631Servico_Vinculado;
            Z158ServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
            Z641Servico_VincDesc = A641Servico_VincDesc;
            Z1557Servico_VincSigla = A1557Servico_VincSigla;
            Z2048Servico_LinNegDsc = A2048Servico_LinNegDsc;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV25Pgmname = "Servico_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A2131Servico_PausaSLA) && ( Gx_BScreen == 0 ) )
         {
            A2131Servico_PausaSLA = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1635Servico_IsPublico) && ( Gx_BScreen == 0 ) )
         {
            A1635Servico_IsPublico = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A632Servico_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A632Servico_Ativo = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1530Servico_TipoHierarquia) && ( Gx_BScreen == 0 ) )
         {
            A1530Servico_TipoHierarquia = 1;
            n1530Servico_TipoHierarquia = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load0V130( )
      {
         /* Using cursor BC000V8 */
         pr_default.execute(6, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound130 = 1;
            A608Servico_Nome = BC000V8_A608Servico_Nome[0];
            A156Servico_Descricao = BC000V8_A156Servico_Descricao[0];
            n156Servico_Descricao = BC000V8_n156Servico_Descricao[0];
            A605Servico_Sigla = BC000V8_A605Servico_Sigla[0];
            A158ServicoGrupo_Descricao = BC000V8_A158ServicoGrupo_Descricao[0];
            A1077Servico_UORespExclusiva = BC000V8_A1077Servico_UORespExclusiva[0];
            n1077Servico_UORespExclusiva = BC000V8_n1077Servico_UORespExclusiva[0];
            A641Servico_VincDesc = BC000V8_A641Servico_VincDesc[0];
            n641Servico_VincDesc = BC000V8_n641Servico_VincDesc[0];
            A1557Servico_VincSigla = BC000V8_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = BC000V8_n1557Servico_VincSigla[0];
            A889Servico_Terceriza = BC000V8_A889Servico_Terceriza[0];
            n889Servico_Terceriza = BC000V8_n889Servico_Terceriza[0];
            A1061Servico_Tela = BC000V8_A1061Servico_Tela[0];
            n1061Servico_Tela = BC000V8_n1061Servico_Tela[0];
            A1072Servico_Atende = BC000V8_A1072Servico_Atende[0];
            n1072Servico_Atende = BC000V8_n1072Servico_Atende[0];
            A1429Servico_ObrigaValores = BC000V8_A1429Servico_ObrigaValores[0];
            n1429Servico_ObrigaValores = BC000V8_n1429Servico_ObrigaValores[0];
            A1436Servico_ObjetoControle = BC000V8_A1436Servico_ObjetoControle[0];
            A1530Servico_TipoHierarquia = BC000V8_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = BC000V8_n1530Servico_TipoHierarquia[0];
            A1534Servico_PercTmp = BC000V8_A1534Servico_PercTmp[0];
            n1534Servico_PercTmp = BC000V8_n1534Servico_PercTmp[0];
            A1535Servico_PercPgm = BC000V8_A1535Servico_PercPgm[0];
            n1535Servico_PercPgm = BC000V8_n1535Servico_PercPgm[0];
            A1536Servico_PercCnc = BC000V8_A1536Servico_PercCnc[0];
            n1536Servico_PercCnc = BC000V8_n1536Servico_PercCnc[0];
            A1545Servico_Anterior = BC000V8_A1545Servico_Anterior[0];
            n1545Servico_Anterior = BC000V8_n1545Servico_Anterior[0];
            A1546Servico_Posterior = BC000V8_A1546Servico_Posterior[0];
            n1546Servico_Posterior = BC000V8_n1546Servico_Posterior[0];
            A632Servico_Ativo = BC000V8_A632Servico_Ativo[0];
            A1635Servico_IsPublico = BC000V8_A1635Servico_IsPublico[0];
            A2048Servico_LinNegDsc = BC000V8_A2048Servico_LinNegDsc[0];
            n2048Servico_LinNegDsc = BC000V8_n2048Servico_LinNegDsc[0];
            A2092Servico_IsOrigemReferencia = BC000V8_A2092Servico_IsOrigemReferencia[0];
            A2131Servico_PausaSLA = BC000V8_A2131Servico_PausaSLA[0];
            A157ServicoGrupo_Codigo = BC000V8_A157ServicoGrupo_Codigo[0];
            A633Servico_UO = BC000V8_A633Servico_UO[0];
            n633Servico_UO = BC000V8_n633Servico_UO[0];
            A2047Servico_LinNegCod = BC000V8_A2047Servico_LinNegCod[0];
            n2047Servico_LinNegCod = BC000V8_n2047Servico_LinNegCod[0];
            A631Servico_Vinculado = BC000V8_A631Servico_Vinculado[0];
            n631Servico_Vinculado = BC000V8_n631Servico_Vinculado[0];
            ZM0V130( -21) ;
         }
         pr_default.close(6);
         OnLoadActions0V130( ) ;
      }

      protected void OnLoadActions0V130( )
      {
         GXt_int1 = A1551Servico_Responsavel;
         new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
         A1551Servico_Responsavel = GXt_int1;
         GXt_int2 = A640Servico_Vinculados;
         new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
         A640Servico_Vinculados = GXt_int2;
         A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
      }

      protected void CheckExtendedTable0V130( )
      {
         standaloneModal( ) ;
         GXt_int1 = A1551Servico_Responsavel;
         new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
         A1551Servico_Responsavel = GXt_int1;
         GXt_int2 = A640Servico_Vinculados;
         new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
         A640Servico_Vinculados = GXt_int2;
         A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A608Servico_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A605Servico_Sigla)) )
         {
            GX_msglist.addItem("Sigla � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC000V4 */
         pr_default.execute(2, new Object[] {A157ServicoGrupo_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico Grupo'.", "ForeignKeyNotFound", 1, "SERVICOGRUPO_CODIGO");
            AnyError = 1;
         }
         A158ServicoGrupo_Descricao = BC000V4_A158ServicoGrupo_Descricao[0];
         pr_default.close(2);
         /* Using cursor BC000V5 */
         pr_default.execute(3, new Object[] {n633Servico_UO, A633Servico_UO});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A633Servico_UO) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico_UnidadeOrganizacional'.", "ForeignKeyNotFound", 1, "SERVICO_UO");
               AnyError = 1;
            }
         }
         pr_default.close(3);
         /* Using cursor BC000V7 */
         pr_default.execute(5, new Object[] {n631Servico_Vinculado, A631Servico_Vinculado});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A631Servico_Vinculado) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico_Servico Vinculado'.", "ForeignKeyNotFound", 1, "SERVICO_VINCULADO");
               AnyError = 1;
            }
         }
         A641Servico_VincDesc = BC000V7_A641Servico_VincDesc[0];
         n641Servico_VincDesc = BC000V7_n641Servico_VincDesc[0];
         A1557Servico_VincSigla = BC000V7_A1557Servico_VincSigla[0];
         n1557Servico_VincSigla = BC000V7_n1557Servico_VincSigla[0];
         pr_default.close(5);
         if ( ! ( ( StringUtil.StrCmp(A1061Servico_Tela, "CNT") == 0 ) || ( StringUtil.StrCmp(A1061Servico_Tela, "CHK") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1061Servico_Tela)) ) )
         {
            GX_msglist.addItem("Campo Tela fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( StringUtil.StrCmp(A1072Servico_Atende, "S") == 0 ) || ( StringUtil.StrCmp(A1072Servico_Atende, "H") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1072Servico_Atende)) ) )
         {
            GX_msglist.addItem("Campo Atende fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( StringUtil.StrCmp(A1429Servico_ObrigaValores, "") == 0 ) || ( StringUtil.StrCmp(A1429Servico_ObrigaValores, "L") == 0 ) || ( StringUtil.StrCmp(A1429Servico_ObrigaValores, "B") == 0 ) || ( StringUtil.StrCmp(A1429Servico_ObrigaValores, "A") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1429Servico_ObrigaValores)) ) )
         {
            GX_msglist.addItem("Campo Obriga Valores fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( StringUtil.StrCmp(A1436Servico_ObjetoControle, "SIS") == 0 ) || ( StringUtil.StrCmp(A1436Servico_ObjetoControle, "PRC") == 0 ) || ( StringUtil.StrCmp(A1436Servico_ObjetoControle, "PRD") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Servico_Objeto Controle fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A1530Servico_TipoHierarquia == 1 ) || ( A1530Servico_TipoHierarquia == 2 ) || (0==A1530Servico_TipoHierarquia) ) )
         {
            GX_msglist.addItem("Campo Tipo de Hierarquia fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC000V6 */
         pr_default.execute(4, new Object[] {n2047Servico_LinNegCod, A2047Servico_LinNegCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A2047Servico_LinNegCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico_Linha Negocio'.", "ForeignKeyNotFound", 1, "SERVICO_LINNEGCOD");
               AnyError = 1;
            }
         }
         A2048Servico_LinNegDsc = BC000V6_A2048Servico_LinNegDsc[0];
         n2048Servico_LinNegDsc = BC000V6_n2048Servico_LinNegDsc[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors0V130( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(5);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0V130( )
      {
         /* Using cursor BC000V9 */
         pr_default.execute(7, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound130 = 1;
         }
         else
         {
            RcdFound130 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC000V3 */
         pr_default.execute(1, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0V130( 21) ;
            RcdFound130 = 1;
            A155Servico_Codigo = BC000V3_A155Servico_Codigo[0];
            A608Servico_Nome = BC000V3_A608Servico_Nome[0];
            A156Servico_Descricao = BC000V3_A156Servico_Descricao[0];
            n156Servico_Descricao = BC000V3_n156Servico_Descricao[0];
            A605Servico_Sigla = BC000V3_A605Servico_Sigla[0];
            A1077Servico_UORespExclusiva = BC000V3_A1077Servico_UORespExclusiva[0];
            n1077Servico_UORespExclusiva = BC000V3_n1077Servico_UORespExclusiva[0];
            A889Servico_Terceriza = BC000V3_A889Servico_Terceriza[0];
            n889Servico_Terceriza = BC000V3_n889Servico_Terceriza[0];
            A1061Servico_Tela = BC000V3_A1061Servico_Tela[0];
            n1061Servico_Tela = BC000V3_n1061Servico_Tela[0];
            A1072Servico_Atende = BC000V3_A1072Servico_Atende[0];
            n1072Servico_Atende = BC000V3_n1072Servico_Atende[0];
            A1429Servico_ObrigaValores = BC000V3_A1429Servico_ObrigaValores[0];
            n1429Servico_ObrigaValores = BC000V3_n1429Servico_ObrigaValores[0];
            A1436Servico_ObjetoControle = BC000V3_A1436Servico_ObjetoControle[0];
            A1530Servico_TipoHierarquia = BC000V3_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = BC000V3_n1530Servico_TipoHierarquia[0];
            A1534Servico_PercTmp = BC000V3_A1534Servico_PercTmp[0];
            n1534Servico_PercTmp = BC000V3_n1534Servico_PercTmp[0];
            A1535Servico_PercPgm = BC000V3_A1535Servico_PercPgm[0];
            n1535Servico_PercPgm = BC000V3_n1535Servico_PercPgm[0];
            A1536Servico_PercCnc = BC000V3_A1536Servico_PercCnc[0];
            n1536Servico_PercCnc = BC000V3_n1536Servico_PercCnc[0];
            A1545Servico_Anterior = BC000V3_A1545Servico_Anterior[0];
            n1545Servico_Anterior = BC000V3_n1545Servico_Anterior[0];
            A1546Servico_Posterior = BC000V3_A1546Servico_Posterior[0];
            n1546Servico_Posterior = BC000V3_n1546Servico_Posterior[0];
            A632Servico_Ativo = BC000V3_A632Servico_Ativo[0];
            A1635Servico_IsPublico = BC000V3_A1635Servico_IsPublico[0];
            A2092Servico_IsOrigemReferencia = BC000V3_A2092Servico_IsOrigemReferencia[0];
            A2131Servico_PausaSLA = BC000V3_A2131Servico_PausaSLA[0];
            A157ServicoGrupo_Codigo = BC000V3_A157ServicoGrupo_Codigo[0];
            A633Servico_UO = BC000V3_A633Servico_UO[0];
            n633Servico_UO = BC000V3_n633Servico_UO[0];
            A2047Servico_LinNegCod = BC000V3_A2047Servico_LinNegCod[0];
            n2047Servico_LinNegCod = BC000V3_n2047Servico_LinNegCod[0];
            A631Servico_Vinculado = BC000V3_A631Servico_Vinculado[0];
            n631Servico_Vinculado = BC000V3_n631Servico_Vinculado[0];
            Z155Servico_Codigo = A155Servico_Codigo;
            sMode130 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load0V130( ) ;
            if ( AnyError == 1 )
            {
               RcdFound130 = 0;
               InitializeNonKey0V130( ) ;
            }
            Gx_mode = sMode130;
         }
         else
         {
            RcdFound130 = 0;
            InitializeNonKey0V130( ) ;
            sMode130 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode130;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0V130( ) ;
         if ( RcdFound130 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_0V0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency0V130( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000V2 */
            pr_default.execute(0, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Servico"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z608Servico_Nome, BC000V2_A608Servico_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z605Servico_Sigla, BC000V2_A605Servico_Sigla[0]) != 0 ) || ( Z1077Servico_UORespExclusiva != BC000V2_A1077Servico_UORespExclusiva[0] ) || ( Z889Servico_Terceriza != BC000V2_A889Servico_Terceriza[0] ) || ( StringUtil.StrCmp(Z1061Servico_Tela, BC000V2_A1061Servico_Tela[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1072Servico_Atende, BC000V2_A1072Servico_Atende[0]) != 0 ) || ( StringUtil.StrCmp(Z1429Servico_ObrigaValores, BC000V2_A1429Servico_ObrigaValores[0]) != 0 ) || ( StringUtil.StrCmp(Z1436Servico_ObjetoControle, BC000V2_A1436Servico_ObjetoControle[0]) != 0 ) || ( Z1530Servico_TipoHierarquia != BC000V2_A1530Servico_TipoHierarquia[0] ) || ( Z1534Servico_PercTmp != BC000V2_A1534Servico_PercTmp[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1535Servico_PercPgm != BC000V2_A1535Servico_PercPgm[0] ) || ( Z1536Servico_PercCnc != BC000V2_A1536Servico_PercCnc[0] ) || ( Z1545Servico_Anterior != BC000V2_A1545Servico_Anterior[0] ) || ( Z1546Servico_Posterior != BC000V2_A1546Servico_Posterior[0] ) || ( Z632Servico_Ativo != BC000V2_A632Servico_Ativo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1635Servico_IsPublico != BC000V2_A1635Servico_IsPublico[0] ) || ( Z2092Servico_IsOrigemReferencia != BC000V2_A2092Servico_IsOrigemReferencia[0] ) || ( Z2131Servico_PausaSLA != BC000V2_A2131Servico_PausaSLA[0] ) || ( Z157ServicoGrupo_Codigo != BC000V2_A157ServicoGrupo_Codigo[0] ) || ( Z633Servico_UO != BC000V2_A633Servico_UO[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2047Servico_LinNegCod != BC000V2_A2047Servico_LinNegCod[0] ) || ( Z631Servico_Vinculado != BC000V2_A631Servico_Vinculado[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Servico"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0V130( )
      {
         BeforeValidate0V130( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0V130( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0V130( 0) ;
            CheckOptimisticConcurrency0V130( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0V130( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0V130( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000V10 */
                     pr_default.execute(8, new Object[] {A608Servico_Nome, n156Servico_Descricao, A156Servico_Descricao, A605Servico_Sigla, n1077Servico_UORespExclusiva, A1077Servico_UORespExclusiva, n889Servico_Terceriza, A889Servico_Terceriza, n1061Servico_Tela, A1061Servico_Tela, n1072Servico_Atende, A1072Servico_Atende, n1429Servico_ObrigaValores, A1429Servico_ObrigaValores, A1436Servico_ObjetoControle, n1530Servico_TipoHierarquia, A1530Servico_TipoHierarquia, n1534Servico_PercTmp, A1534Servico_PercTmp, n1535Servico_PercPgm, A1535Servico_PercPgm, n1536Servico_PercCnc, A1536Servico_PercCnc, n1545Servico_Anterior, A1545Servico_Anterior, n1546Servico_Posterior, A1546Servico_Posterior, A632Servico_Ativo, A1635Servico_IsPublico, A2092Servico_IsOrigemReferencia, A2131Servico_PausaSLA, A157ServicoGrupo_Codigo, n633Servico_UO, A633Servico_UO, n2047Servico_LinNegCod, A2047Servico_LinNegCod, n631Servico_Vinculado, A631Servico_Vinculado});
                     A155Servico_Codigo = BC000V10_A155Servico_Codigo[0];
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Servico") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0V130( ) ;
            }
            EndLevel0V130( ) ;
         }
         CloseExtendedTableCursors0V130( ) ;
      }

      protected void Update0V130( )
      {
         BeforeValidate0V130( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0V130( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0V130( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0V130( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0V130( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000V11 */
                     pr_default.execute(9, new Object[] {A608Servico_Nome, n156Servico_Descricao, A156Servico_Descricao, A605Servico_Sigla, n1077Servico_UORespExclusiva, A1077Servico_UORespExclusiva, n889Servico_Terceriza, A889Servico_Terceriza, n1061Servico_Tela, A1061Servico_Tela, n1072Servico_Atende, A1072Servico_Atende, n1429Servico_ObrigaValores, A1429Servico_ObrigaValores, A1436Servico_ObjetoControle, n1530Servico_TipoHierarquia, A1530Servico_TipoHierarquia, n1534Servico_PercTmp, A1534Servico_PercTmp, n1535Servico_PercPgm, A1535Servico_PercPgm, n1536Servico_PercCnc, A1536Servico_PercCnc, n1545Servico_Anterior, A1545Servico_Anterior, n1546Servico_Posterior, A1546Servico_Posterior, A632Servico_Ativo, A1635Servico_IsPublico, A2092Servico_IsOrigemReferencia, A2131Servico_PausaSLA, A157ServicoGrupo_Codigo, n633Servico_UO, A633Servico_UO, n2047Servico_LinNegCod, A2047Servico_LinNegCod, n631Servico_Vinculado, A631Servico_Vinculado, A155Servico_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Servico") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Servico"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0V130( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0V130( ) ;
         }
         CloseExtendedTableCursors0V130( ) ;
      }

      protected void DeferredUpdate0V130( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate0V130( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0V130( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0V130( ) ;
            AfterConfirm0V130( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0V130( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000V12 */
                  pr_default.execute(10, new Object[] {A155Servico_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("Servico") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode130 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0V130( ) ;
         Gx_mode = sMode130;
      }

      protected void OnDeleteControls0V130( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_int1 = A1551Servico_Responsavel;
            new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
            A1551Servico_Responsavel = GXt_int1;
            GXt_int2 = A640Servico_Vinculados;
            new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
            A640Servico_Vinculados = GXt_int2;
            A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
            /* Using cursor BC000V13 */
            pr_default.execute(11, new Object[] {A157ServicoGrupo_Codigo});
            A158ServicoGrupo_Descricao = BC000V13_A158ServicoGrupo_Descricao[0];
            pr_default.close(11);
            /* Using cursor BC000V14 */
            pr_default.execute(12, new Object[] {n631Servico_Vinculado, A631Servico_Vinculado});
            A641Servico_VincDesc = BC000V14_A641Servico_VincDesc[0];
            n641Servico_VincDesc = BC000V14_n641Servico_VincDesc[0];
            A1557Servico_VincSigla = BC000V14_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = BC000V14_n1557Servico_VincSigla[0];
            pr_default.close(12);
            /* Using cursor BC000V15 */
            pr_default.execute(13, new Object[] {n2047Servico_LinNegCod, A2047Servico_LinNegCod});
            A2048Servico_LinNegDsc = BC000V15_A2048Servico_LinNegDsc[0];
            n2048Servico_LinNegDsc = BC000V15_n2048Servico_LinNegDsc[0];
            pr_default.close(13);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000V16 */
            pr_default.execute(14, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
            /* Using cursor BC000V17 */
            pr_default.execute(15, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico Fluxo"+" ("+"Servico Fluxo_Servico Posterior"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
            /* Using cursor BC000V18 */
            pr_default.execute(16, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico Fluxo"+" ("+"Servico_Servico Fluxo"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
            /* Using cursor BC000V19 */
            pr_default.execute(17, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Prioridades padr�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
            /* Using cursor BC000V20 */
            pr_default.execute(18, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas do Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
            /* Using cursor BC000V21 */
            pr_default.execute(19, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Area de Trabalho"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(19);
            /* Using cursor BC000V22 */
            pr_default.execute(20, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario Servicos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
            /* Using cursor BC000V23 */
            pr_default.execute(21, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor BC000V24 */
            pr_default.execute(22, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
            /* Using cursor BC000V25 */
            pr_default.execute(23, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico Ans"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor BC000V26 */
            pr_default.execute(24, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Servicos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor BC000V27 */
            pr_default.execute(25, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Check Lists"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
            /* Using cursor BC000V28 */
            pr_default.execute(26, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Artefatos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
         }
      }

      protected void EndLevel0V130( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0V130( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0V130( )
      {
         /* Scan By routine */
         /* Using cursor BC000V29 */
         pr_default.execute(27, new Object[] {A155Servico_Codigo});
         RcdFound130 = 0;
         if ( (pr_default.getStatus(27) != 101) )
         {
            RcdFound130 = 1;
            A155Servico_Codigo = BC000V29_A155Servico_Codigo[0];
            A608Servico_Nome = BC000V29_A608Servico_Nome[0];
            A156Servico_Descricao = BC000V29_A156Servico_Descricao[0];
            n156Servico_Descricao = BC000V29_n156Servico_Descricao[0];
            A605Servico_Sigla = BC000V29_A605Servico_Sigla[0];
            A158ServicoGrupo_Descricao = BC000V29_A158ServicoGrupo_Descricao[0];
            A1077Servico_UORespExclusiva = BC000V29_A1077Servico_UORespExclusiva[0];
            n1077Servico_UORespExclusiva = BC000V29_n1077Servico_UORespExclusiva[0];
            A641Servico_VincDesc = BC000V29_A641Servico_VincDesc[0];
            n641Servico_VincDesc = BC000V29_n641Servico_VincDesc[0];
            A1557Servico_VincSigla = BC000V29_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = BC000V29_n1557Servico_VincSigla[0];
            A889Servico_Terceriza = BC000V29_A889Servico_Terceriza[0];
            n889Servico_Terceriza = BC000V29_n889Servico_Terceriza[0];
            A1061Servico_Tela = BC000V29_A1061Servico_Tela[0];
            n1061Servico_Tela = BC000V29_n1061Servico_Tela[0];
            A1072Servico_Atende = BC000V29_A1072Servico_Atende[0];
            n1072Servico_Atende = BC000V29_n1072Servico_Atende[0];
            A1429Servico_ObrigaValores = BC000V29_A1429Servico_ObrigaValores[0];
            n1429Servico_ObrigaValores = BC000V29_n1429Servico_ObrigaValores[0];
            A1436Servico_ObjetoControle = BC000V29_A1436Servico_ObjetoControle[0];
            A1530Servico_TipoHierarquia = BC000V29_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = BC000V29_n1530Servico_TipoHierarquia[0];
            A1534Servico_PercTmp = BC000V29_A1534Servico_PercTmp[0];
            n1534Servico_PercTmp = BC000V29_n1534Servico_PercTmp[0];
            A1535Servico_PercPgm = BC000V29_A1535Servico_PercPgm[0];
            n1535Servico_PercPgm = BC000V29_n1535Servico_PercPgm[0];
            A1536Servico_PercCnc = BC000V29_A1536Servico_PercCnc[0];
            n1536Servico_PercCnc = BC000V29_n1536Servico_PercCnc[0];
            A1545Servico_Anterior = BC000V29_A1545Servico_Anterior[0];
            n1545Servico_Anterior = BC000V29_n1545Servico_Anterior[0];
            A1546Servico_Posterior = BC000V29_A1546Servico_Posterior[0];
            n1546Servico_Posterior = BC000V29_n1546Servico_Posterior[0];
            A632Servico_Ativo = BC000V29_A632Servico_Ativo[0];
            A1635Servico_IsPublico = BC000V29_A1635Servico_IsPublico[0];
            A2048Servico_LinNegDsc = BC000V29_A2048Servico_LinNegDsc[0];
            n2048Servico_LinNegDsc = BC000V29_n2048Servico_LinNegDsc[0];
            A2092Servico_IsOrigemReferencia = BC000V29_A2092Servico_IsOrigemReferencia[0];
            A2131Servico_PausaSLA = BC000V29_A2131Servico_PausaSLA[0];
            A157ServicoGrupo_Codigo = BC000V29_A157ServicoGrupo_Codigo[0];
            A633Servico_UO = BC000V29_A633Servico_UO[0];
            n633Servico_UO = BC000V29_n633Servico_UO[0];
            A2047Servico_LinNegCod = BC000V29_A2047Servico_LinNegCod[0];
            n2047Servico_LinNegCod = BC000V29_n2047Servico_LinNegCod[0];
            A631Servico_Vinculado = BC000V29_A631Servico_Vinculado[0];
            n631Servico_Vinculado = BC000V29_n631Servico_Vinculado[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0V130( )
      {
         /* Scan next routine */
         pr_default.readNext(27);
         RcdFound130 = 0;
         ScanKeyLoad0V130( ) ;
      }

      protected void ScanKeyLoad0V130( )
      {
         sMode130 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(27) != 101) )
         {
            RcdFound130 = 1;
            A155Servico_Codigo = BC000V29_A155Servico_Codigo[0];
            A608Servico_Nome = BC000V29_A608Servico_Nome[0];
            A156Servico_Descricao = BC000V29_A156Servico_Descricao[0];
            n156Servico_Descricao = BC000V29_n156Servico_Descricao[0];
            A605Servico_Sigla = BC000V29_A605Servico_Sigla[0];
            A158ServicoGrupo_Descricao = BC000V29_A158ServicoGrupo_Descricao[0];
            A1077Servico_UORespExclusiva = BC000V29_A1077Servico_UORespExclusiva[0];
            n1077Servico_UORespExclusiva = BC000V29_n1077Servico_UORespExclusiva[0];
            A641Servico_VincDesc = BC000V29_A641Servico_VincDesc[0];
            n641Servico_VincDesc = BC000V29_n641Servico_VincDesc[0];
            A1557Servico_VincSigla = BC000V29_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = BC000V29_n1557Servico_VincSigla[0];
            A889Servico_Terceriza = BC000V29_A889Servico_Terceriza[0];
            n889Servico_Terceriza = BC000V29_n889Servico_Terceriza[0];
            A1061Servico_Tela = BC000V29_A1061Servico_Tela[0];
            n1061Servico_Tela = BC000V29_n1061Servico_Tela[0];
            A1072Servico_Atende = BC000V29_A1072Servico_Atende[0];
            n1072Servico_Atende = BC000V29_n1072Servico_Atende[0];
            A1429Servico_ObrigaValores = BC000V29_A1429Servico_ObrigaValores[0];
            n1429Servico_ObrigaValores = BC000V29_n1429Servico_ObrigaValores[0];
            A1436Servico_ObjetoControle = BC000V29_A1436Servico_ObjetoControle[0];
            A1530Servico_TipoHierarquia = BC000V29_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = BC000V29_n1530Servico_TipoHierarquia[0];
            A1534Servico_PercTmp = BC000V29_A1534Servico_PercTmp[0];
            n1534Servico_PercTmp = BC000V29_n1534Servico_PercTmp[0];
            A1535Servico_PercPgm = BC000V29_A1535Servico_PercPgm[0];
            n1535Servico_PercPgm = BC000V29_n1535Servico_PercPgm[0];
            A1536Servico_PercCnc = BC000V29_A1536Servico_PercCnc[0];
            n1536Servico_PercCnc = BC000V29_n1536Servico_PercCnc[0];
            A1545Servico_Anterior = BC000V29_A1545Servico_Anterior[0];
            n1545Servico_Anterior = BC000V29_n1545Servico_Anterior[0];
            A1546Servico_Posterior = BC000V29_A1546Servico_Posterior[0];
            n1546Servico_Posterior = BC000V29_n1546Servico_Posterior[0];
            A632Servico_Ativo = BC000V29_A632Servico_Ativo[0];
            A1635Servico_IsPublico = BC000V29_A1635Servico_IsPublico[0];
            A2048Servico_LinNegDsc = BC000V29_A2048Servico_LinNegDsc[0];
            n2048Servico_LinNegDsc = BC000V29_n2048Servico_LinNegDsc[0];
            A2092Servico_IsOrigemReferencia = BC000V29_A2092Servico_IsOrigemReferencia[0];
            A2131Servico_PausaSLA = BC000V29_A2131Servico_PausaSLA[0];
            A157ServicoGrupo_Codigo = BC000V29_A157ServicoGrupo_Codigo[0];
            A633Servico_UO = BC000V29_A633Servico_UO[0];
            n633Servico_UO = BC000V29_n633Servico_UO[0];
            A2047Servico_LinNegCod = BC000V29_A2047Servico_LinNegCod[0];
            n2047Servico_LinNegCod = BC000V29_n2047Servico_LinNegCod[0];
            A631Servico_Vinculado = BC000V29_A631Servico_Vinculado[0];
            n631Servico_Vinculado = BC000V29_n631Servico_Vinculado[0];
         }
         Gx_mode = sMode130;
      }

      protected void ScanKeyEnd0V130( )
      {
         pr_default.close(27);
      }

      protected void AfterConfirm0V130( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0V130( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0V130( )
      {
         /* Before Update Rules */
         new loadauditservico(context ).execute(  "Y", ref  AV19AuditingObject,  A155Servico_Codigo,  Gx_mode) ;
      }

      protected void BeforeDelete0V130( )
      {
         /* Before Delete Rules */
         new loadauditservico(context ).execute(  "Y", ref  AV19AuditingObject,  A155Servico_Codigo,  Gx_mode) ;
      }

      protected void BeforeComplete0V130( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditservico(context ).execute(  "N", ref  AV19AuditingObject,  A155Servico_Codigo,  Gx_mode) ;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditservico(context ).execute(  "N", ref  AV19AuditingObject,  A155Servico_Codigo,  Gx_mode) ;
         }
      }

      protected void BeforeValidate0V130( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0V130( )
      {
      }

      protected void AddRow0V130( )
      {
         VarsToRow130( bcServico) ;
      }

      protected void ReadRow0V130( )
      {
         RowToVars130( bcServico, 1) ;
      }

      protected void InitializeNonKey0V130( )
      {
         AV19AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A2107Servico_Identificacao = "";
         A640Servico_Vinculados = 0;
         A1551Servico_Responsavel = 0;
         A608Servico_Nome = "";
         A156Servico_Descricao = "";
         n156Servico_Descricao = false;
         A605Servico_Sigla = "";
         A157ServicoGrupo_Codigo = 0;
         A158ServicoGrupo_Descricao = "";
         A633Servico_UO = 0;
         n633Servico_UO = false;
         A1077Servico_UORespExclusiva = false;
         n1077Servico_UORespExclusiva = false;
         A631Servico_Vinculado = 0;
         n631Servico_Vinculado = false;
         A641Servico_VincDesc = "";
         n641Servico_VincDesc = false;
         A1557Servico_VincSigla = "";
         n1557Servico_VincSigla = false;
         A889Servico_Terceriza = false;
         n889Servico_Terceriza = false;
         A1061Servico_Tela = "";
         n1061Servico_Tela = false;
         A1072Servico_Atende = "";
         n1072Servico_Atende = false;
         A1429Servico_ObrigaValores = "";
         n1429Servico_ObrigaValores = false;
         A1436Servico_ObjetoControle = "";
         A1534Servico_PercTmp = 0;
         n1534Servico_PercTmp = false;
         A1535Servico_PercPgm = 0;
         n1535Servico_PercPgm = false;
         A1536Servico_PercCnc = 0;
         n1536Servico_PercCnc = false;
         A1545Servico_Anterior = 0;
         n1545Servico_Anterior = false;
         A1546Servico_Posterior = 0;
         n1546Servico_Posterior = false;
         A2047Servico_LinNegCod = 0;
         n2047Servico_LinNegCod = false;
         A2048Servico_LinNegDsc = "";
         n2048Servico_LinNegDsc = false;
         A2092Servico_IsOrigemReferencia = false;
         A1530Servico_TipoHierarquia = 1;
         n1530Servico_TipoHierarquia = false;
         A632Servico_Ativo = true;
         A1635Servico_IsPublico = false;
         A2131Servico_PausaSLA = false;
         Z608Servico_Nome = "";
         Z605Servico_Sigla = "";
         Z1077Servico_UORespExclusiva = false;
         Z889Servico_Terceriza = false;
         Z1061Servico_Tela = "";
         Z1072Servico_Atende = "";
         Z1429Servico_ObrigaValores = "";
         Z1436Servico_ObjetoControle = "";
         Z1530Servico_TipoHierarquia = 0;
         Z1534Servico_PercTmp = 0;
         Z1535Servico_PercPgm = 0;
         Z1536Servico_PercCnc = 0;
         Z1545Servico_Anterior = 0;
         Z1546Servico_Posterior = 0;
         Z632Servico_Ativo = false;
         Z1635Servico_IsPublico = false;
         Z2092Servico_IsOrigemReferencia = false;
         Z2131Servico_PausaSLA = false;
         Z157ServicoGrupo_Codigo = 0;
         Z633Servico_UO = 0;
         Z2047Servico_LinNegCod = 0;
         Z631Servico_Vinculado = 0;
      }

      protected void InitAll0V130( )
      {
         A155Servico_Codigo = 0;
         InitializeNonKey0V130( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2131Servico_PausaSLA = i2131Servico_PausaSLA;
         A1635Servico_IsPublico = i1635Servico_IsPublico;
         A632Servico_Ativo = i632Servico_Ativo;
         A1530Servico_TipoHierarquia = i1530Servico_TipoHierarquia;
         n1530Servico_TipoHierarquia = false;
      }

      public void VarsToRow130( SdtServico obj130 )
      {
         obj130.gxTpr_Mode = Gx_mode;
         obj130.gxTpr_Servico_identificacao = A2107Servico_Identificacao;
         obj130.gxTpr_Servico_vinculados = A640Servico_Vinculados;
         obj130.gxTpr_Servico_responsavel = A1551Servico_Responsavel;
         obj130.gxTpr_Servico_nome = A608Servico_Nome;
         obj130.gxTpr_Servico_descricao = A156Servico_Descricao;
         obj130.gxTpr_Servico_sigla = A605Servico_Sigla;
         obj130.gxTpr_Servicogrupo_codigo = A157ServicoGrupo_Codigo;
         obj130.gxTpr_Servicogrupo_descricao = A158ServicoGrupo_Descricao;
         obj130.gxTpr_Servico_uo = A633Servico_UO;
         obj130.gxTpr_Servico_uorespexclusiva = A1077Servico_UORespExclusiva;
         obj130.gxTpr_Servico_vinculado = A631Servico_Vinculado;
         obj130.gxTpr_Servico_vincdesc = A641Servico_VincDesc;
         obj130.gxTpr_Servico_vincsigla = A1557Servico_VincSigla;
         obj130.gxTpr_Servico_terceriza = A889Servico_Terceriza;
         obj130.gxTpr_Servico_tela = A1061Servico_Tela;
         obj130.gxTpr_Servico_atende = A1072Servico_Atende;
         obj130.gxTpr_Servico_obrigavalores = A1429Servico_ObrigaValores;
         obj130.gxTpr_Servico_objetocontrole = A1436Servico_ObjetoControle;
         obj130.gxTpr_Servico_perctmp = A1534Servico_PercTmp;
         obj130.gxTpr_Servico_percpgm = A1535Servico_PercPgm;
         obj130.gxTpr_Servico_perccnc = A1536Servico_PercCnc;
         obj130.gxTpr_Servico_anterior = A1545Servico_Anterior;
         obj130.gxTpr_Servico_posterior = A1546Servico_Posterior;
         obj130.gxTpr_Servico_linnegcod = A2047Servico_LinNegCod;
         obj130.gxTpr_Servico_linnegdsc = A2048Servico_LinNegDsc;
         obj130.gxTpr_Servico_isorigemreferencia = A2092Servico_IsOrigemReferencia;
         obj130.gxTpr_Servico_tipohierarquia = A1530Servico_TipoHierarquia;
         obj130.gxTpr_Servico_ativo = A632Servico_Ativo;
         obj130.gxTpr_Servico_ispublico = A1635Servico_IsPublico;
         obj130.gxTpr_Servico_pausasla = A2131Servico_PausaSLA;
         obj130.gxTpr_Servico_codigo = A155Servico_Codigo;
         obj130.gxTpr_Servico_codigo_Z = Z155Servico_Codigo;
         obj130.gxTpr_Servico_nome_Z = Z608Servico_Nome;
         obj130.gxTpr_Servico_sigla_Z = Z605Servico_Sigla;
         obj130.gxTpr_Servicogrupo_codigo_Z = Z157ServicoGrupo_Codigo;
         obj130.gxTpr_Servicogrupo_descricao_Z = Z158ServicoGrupo_Descricao;
         obj130.gxTpr_Servico_uo_Z = Z633Servico_UO;
         obj130.gxTpr_Servico_uorespexclusiva_Z = Z1077Servico_UORespExclusiva;
         obj130.gxTpr_Servico_vinculado_Z = Z631Servico_Vinculado;
         obj130.gxTpr_Servico_vinculados_Z = Z640Servico_Vinculados;
         obj130.gxTpr_Servico_vincsigla_Z = Z1557Servico_VincSigla;
         obj130.gxTpr_Servico_terceriza_Z = Z889Servico_Terceriza;
         obj130.gxTpr_Servico_tela_Z = Z1061Servico_Tela;
         obj130.gxTpr_Servico_atende_Z = Z1072Servico_Atende;
         obj130.gxTpr_Servico_obrigavalores_Z = Z1429Servico_ObrigaValores;
         obj130.gxTpr_Servico_objetocontrole_Z = Z1436Servico_ObjetoControle;
         obj130.gxTpr_Servico_tipohierarquia_Z = Z1530Servico_TipoHierarquia;
         obj130.gxTpr_Servico_perctmp_Z = Z1534Servico_PercTmp;
         obj130.gxTpr_Servico_percpgm_Z = Z1535Servico_PercPgm;
         obj130.gxTpr_Servico_perccnc_Z = Z1536Servico_PercCnc;
         obj130.gxTpr_Servico_anterior_Z = Z1545Servico_Anterior;
         obj130.gxTpr_Servico_posterior_Z = Z1546Servico_Posterior;
         obj130.gxTpr_Servico_responsavel_Z = Z1551Servico_Responsavel;
         obj130.gxTpr_Servico_ativo_Z = Z632Servico_Ativo;
         obj130.gxTpr_Servico_ispublico_Z = Z1635Servico_IsPublico;
         obj130.gxTpr_Servico_linnegcod_Z = Z2047Servico_LinNegCod;
         obj130.gxTpr_Servico_linnegdsc_Z = Z2048Servico_LinNegDsc;
         obj130.gxTpr_Servico_isorigemreferencia_Z = Z2092Servico_IsOrigemReferencia;
         obj130.gxTpr_Servico_identificacao_Z = Z2107Servico_Identificacao;
         obj130.gxTpr_Servico_pausasla_Z = Z2131Servico_PausaSLA;
         obj130.gxTpr_Servico_descricao_N = (short)(Convert.ToInt16(n156Servico_Descricao));
         obj130.gxTpr_Servico_uo_N = (short)(Convert.ToInt16(n633Servico_UO));
         obj130.gxTpr_Servico_uorespexclusiva_N = (short)(Convert.ToInt16(n1077Servico_UORespExclusiva));
         obj130.gxTpr_Servico_vinculado_N = (short)(Convert.ToInt16(n631Servico_Vinculado));
         obj130.gxTpr_Servico_vincdesc_N = (short)(Convert.ToInt16(n641Servico_VincDesc));
         obj130.gxTpr_Servico_vincsigla_N = (short)(Convert.ToInt16(n1557Servico_VincSigla));
         obj130.gxTpr_Servico_terceriza_N = (short)(Convert.ToInt16(n889Servico_Terceriza));
         obj130.gxTpr_Servico_tela_N = (short)(Convert.ToInt16(n1061Servico_Tela));
         obj130.gxTpr_Servico_atende_N = (short)(Convert.ToInt16(n1072Servico_Atende));
         obj130.gxTpr_Servico_obrigavalores_N = (short)(Convert.ToInt16(n1429Servico_ObrigaValores));
         obj130.gxTpr_Servico_tipohierarquia_N = (short)(Convert.ToInt16(n1530Servico_TipoHierarquia));
         obj130.gxTpr_Servico_perctmp_N = (short)(Convert.ToInt16(n1534Servico_PercTmp));
         obj130.gxTpr_Servico_percpgm_N = (short)(Convert.ToInt16(n1535Servico_PercPgm));
         obj130.gxTpr_Servico_perccnc_N = (short)(Convert.ToInt16(n1536Servico_PercCnc));
         obj130.gxTpr_Servico_anterior_N = (short)(Convert.ToInt16(n1545Servico_Anterior));
         obj130.gxTpr_Servico_posterior_N = (short)(Convert.ToInt16(n1546Servico_Posterior));
         obj130.gxTpr_Servico_linnegcod_N = (short)(Convert.ToInt16(n2047Servico_LinNegCod));
         obj130.gxTpr_Servico_linnegdsc_N = (short)(Convert.ToInt16(n2048Servico_LinNegDsc));
         obj130.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow130( SdtServico obj130 )
      {
         obj130.gxTpr_Servico_codigo = A155Servico_Codigo;
         return  ;
      }

      public void RowToVars130( SdtServico obj130 ,
                                int forceLoad )
      {
         Gx_mode = obj130.gxTpr_Mode;
         A2107Servico_Identificacao = obj130.gxTpr_Servico_identificacao;
         A640Servico_Vinculados = obj130.gxTpr_Servico_vinculados;
         A1551Servico_Responsavel = obj130.gxTpr_Servico_responsavel;
         A608Servico_Nome = obj130.gxTpr_Servico_nome;
         A156Servico_Descricao = obj130.gxTpr_Servico_descricao;
         n156Servico_Descricao = false;
         A605Servico_Sigla = obj130.gxTpr_Servico_sigla;
         A157ServicoGrupo_Codigo = obj130.gxTpr_Servicogrupo_codigo;
         A158ServicoGrupo_Descricao = obj130.gxTpr_Servicogrupo_descricao;
         A633Servico_UO = obj130.gxTpr_Servico_uo;
         n633Servico_UO = false;
         A1077Servico_UORespExclusiva = obj130.gxTpr_Servico_uorespexclusiva;
         n1077Servico_UORespExclusiva = false;
         A631Servico_Vinculado = obj130.gxTpr_Servico_vinculado;
         n631Servico_Vinculado = false;
         A641Servico_VincDesc = obj130.gxTpr_Servico_vincdesc;
         n641Servico_VincDesc = false;
         A1557Servico_VincSigla = obj130.gxTpr_Servico_vincsigla;
         n1557Servico_VincSigla = false;
         A889Servico_Terceriza = obj130.gxTpr_Servico_terceriza;
         n889Servico_Terceriza = false;
         A1061Servico_Tela = obj130.gxTpr_Servico_tela;
         n1061Servico_Tela = false;
         A1072Servico_Atende = obj130.gxTpr_Servico_atende;
         n1072Servico_Atende = false;
         A1429Servico_ObrigaValores = obj130.gxTpr_Servico_obrigavalores;
         n1429Servico_ObrigaValores = false;
         A1436Servico_ObjetoControle = obj130.gxTpr_Servico_objetocontrole;
         A1534Servico_PercTmp = obj130.gxTpr_Servico_perctmp;
         n1534Servico_PercTmp = false;
         A1535Servico_PercPgm = obj130.gxTpr_Servico_percpgm;
         n1535Servico_PercPgm = false;
         A1536Servico_PercCnc = obj130.gxTpr_Servico_perccnc;
         n1536Servico_PercCnc = false;
         A1545Servico_Anterior = obj130.gxTpr_Servico_anterior;
         n1545Servico_Anterior = false;
         A1546Servico_Posterior = obj130.gxTpr_Servico_posterior;
         n1546Servico_Posterior = false;
         A2047Servico_LinNegCod = obj130.gxTpr_Servico_linnegcod;
         n2047Servico_LinNegCod = false;
         A2048Servico_LinNegDsc = obj130.gxTpr_Servico_linnegdsc;
         n2048Servico_LinNegDsc = false;
         A2092Servico_IsOrigemReferencia = obj130.gxTpr_Servico_isorigemreferencia;
         A1530Servico_TipoHierarquia = obj130.gxTpr_Servico_tipohierarquia;
         n1530Servico_TipoHierarquia = false;
         A632Servico_Ativo = obj130.gxTpr_Servico_ativo;
         A1635Servico_IsPublico = obj130.gxTpr_Servico_ispublico;
         A2131Servico_PausaSLA = obj130.gxTpr_Servico_pausasla;
         A155Servico_Codigo = obj130.gxTpr_Servico_codigo;
         Z155Servico_Codigo = obj130.gxTpr_Servico_codigo_Z;
         Z608Servico_Nome = obj130.gxTpr_Servico_nome_Z;
         Z605Servico_Sigla = obj130.gxTpr_Servico_sigla_Z;
         Z157ServicoGrupo_Codigo = obj130.gxTpr_Servicogrupo_codigo_Z;
         Z158ServicoGrupo_Descricao = obj130.gxTpr_Servicogrupo_descricao_Z;
         Z633Servico_UO = obj130.gxTpr_Servico_uo_Z;
         Z1077Servico_UORespExclusiva = obj130.gxTpr_Servico_uorespexclusiva_Z;
         Z631Servico_Vinculado = obj130.gxTpr_Servico_vinculado_Z;
         Z640Servico_Vinculados = obj130.gxTpr_Servico_vinculados_Z;
         Z1557Servico_VincSigla = obj130.gxTpr_Servico_vincsigla_Z;
         Z889Servico_Terceriza = obj130.gxTpr_Servico_terceriza_Z;
         Z1061Servico_Tela = obj130.gxTpr_Servico_tela_Z;
         Z1072Servico_Atende = obj130.gxTpr_Servico_atende_Z;
         Z1429Servico_ObrigaValores = obj130.gxTpr_Servico_obrigavalores_Z;
         Z1436Servico_ObjetoControle = obj130.gxTpr_Servico_objetocontrole_Z;
         Z1530Servico_TipoHierarquia = obj130.gxTpr_Servico_tipohierarquia_Z;
         Z1534Servico_PercTmp = obj130.gxTpr_Servico_perctmp_Z;
         Z1535Servico_PercPgm = obj130.gxTpr_Servico_percpgm_Z;
         Z1536Servico_PercCnc = obj130.gxTpr_Servico_perccnc_Z;
         Z1545Servico_Anterior = obj130.gxTpr_Servico_anterior_Z;
         Z1546Servico_Posterior = obj130.gxTpr_Servico_posterior_Z;
         Z1551Servico_Responsavel = obj130.gxTpr_Servico_responsavel_Z;
         Z632Servico_Ativo = obj130.gxTpr_Servico_ativo_Z;
         Z1635Servico_IsPublico = obj130.gxTpr_Servico_ispublico_Z;
         Z2047Servico_LinNegCod = obj130.gxTpr_Servico_linnegcod_Z;
         Z2048Servico_LinNegDsc = obj130.gxTpr_Servico_linnegdsc_Z;
         Z2092Servico_IsOrigemReferencia = obj130.gxTpr_Servico_isorigemreferencia_Z;
         Z2107Servico_Identificacao = obj130.gxTpr_Servico_identificacao_Z;
         Z2131Servico_PausaSLA = obj130.gxTpr_Servico_pausasla_Z;
         n156Servico_Descricao = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_descricao_N));
         n633Servico_UO = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_uo_N));
         n1077Servico_UORespExclusiva = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_uorespexclusiva_N));
         n631Servico_Vinculado = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_vinculado_N));
         n641Servico_VincDesc = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_vincdesc_N));
         n1557Servico_VincSigla = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_vincsigla_N));
         n889Servico_Terceriza = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_terceriza_N));
         n1061Servico_Tela = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_tela_N));
         n1072Servico_Atende = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_atende_N));
         n1429Servico_ObrigaValores = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_obrigavalores_N));
         n1530Servico_TipoHierarquia = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_tipohierarquia_N));
         n1534Servico_PercTmp = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_perctmp_N));
         n1535Servico_PercPgm = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_percpgm_N));
         n1536Servico_PercCnc = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_perccnc_N));
         n1545Servico_Anterior = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_anterior_N));
         n1546Servico_Posterior = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_posterior_N));
         n2047Servico_LinNegCod = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_linnegcod_N));
         n2048Servico_LinNegDsc = (bool)(Convert.ToBoolean(obj130.gxTpr_Servico_linnegdsc_N));
         Gx_mode = obj130.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A155Servico_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey0V130( ) ;
         ScanKeyStart0V130( ) ;
         if ( RcdFound130 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z155Servico_Codigo = A155Servico_Codigo;
         }
         ZM0V130( -21) ;
         OnLoadActions0V130( ) ;
         AddRow0V130( ) ;
         ScanKeyEnd0V130( ) ;
         if ( RcdFound130 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars130( bcServico, 0) ;
         ScanKeyStart0V130( ) ;
         if ( RcdFound130 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z155Servico_Codigo = A155Servico_Codigo;
         }
         ZM0V130( -21) ;
         OnLoadActions0V130( ) ;
         AddRow0V130( ) ;
         ScanKeyEnd0V130( ) ;
         if ( RcdFound130 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars130( bcServico, 0) ;
         nKeyPressed = 1;
         GetKey0V130( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert0V130( ) ;
         }
         else
         {
            if ( RcdFound130 == 1 )
            {
               if ( A155Servico_Codigo != Z155Servico_Codigo )
               {
                  A155Servico_Codigo = Z155Servico_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update0V130( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A155Servico_Codigo != Z155Servico_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0V130( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0V130( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow130( bcServico) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars130( bcServico, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey0V130( ) ;
         if ( RcdFound130 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A155Servico_Codigo != Z155Servico_Codigo )
            {
               A155Servico_Codigo = Z155Servico_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A155Servico_Codigo != Z155Servico_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(13);
         pr_default.close(12);
         context.RollbackDataStores( "Servico_BC");
         VarsToRow130( bcServico) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcServico.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcServico.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcServico )
         {
            bcServico = (SdtServico)(sdt);
            if ( StringUtil.StrCmp(bcServico.gxTpr_Mode, "") == 0 )
            {
               bcServico.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow130( bcServico) ;
            }
            else
            {
               RowToVars130( bcServico, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcServico.gxTpr_Mode, "") == 0 )
            {
               bcServico.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars130( bcServico, 1) ;
         return  ;
      }

      public SdtServico Servico_BC
      {
         get {
            return bcServico ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(13);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV25Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV19AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         Z608Servico_Nome = "";
         A608Servico_Nome = "";
         Z605Servico_Sigla = "";
         A605Servico_Sigla = "";
         Z1061Servico_Tela = "";
         A1061Servico_Tela = "";
         Z1072Servico_Atende = "";
         A1072Servico_Atende = "";
         Z1429Servico_ObrigaValores = "";
         A1429Servico_ObrigaValores = "";
         Z1436Servico_ObjetoControle = "";
         A1436Servico_ObjetoControle = "";
         Z2107Servico_Identificacao = "";
         A2107Servico_Identificacao = "";
         Z158ServicoGrupo_Descricao = "";
         A158ServicoGrupo_Descricao = "";
         Z2048Servico_LinNegDsc = "";
         A2048Servico_LinNegDsc = "";
         Z1557Servico_VincSigla = "";
         A1557Servico_VincSigla = "";
         Z156Servico_Descricao = "";
         A156Servico_Descricao = "";
         Z641Servico_VincDesc = "";
         A641Servico_VincDesc = "";
         BC000V8_A155Servico_Codigo = new int[1] ;
         BC000V8_A608Servico_Nome = new String[] {""} ;
         BC000V8_A156Servico_Descricao = new String[] {""} ;
         BC000V8_n156Servico_Descricao = new bool[] {false} ;
         BC000V8_A605Servico_Sigla = new String[] {""} ;
         BC000V8_A158ServicoGrupo_Descricao = new String[] {""} ;
         BC000V8_A1077Servico_UORespExclusiva = new bool[] {false} ;
         BC000V8_n1077Servico_UORespExclusiva = new bool[] {false} ;
         BC000V8_A641Servico_VincDesc = new String[] {""} ;
         BC000V8_n641Servico_VincDesc = new bool[] {false} ;
         BC000V8_A1557Servico_VincSigla = new String[] {""} ;
         BC000V8_n1557Servico_VincSigla = new bool[] {false} ;
         BC000V8_A889Servico_Terceriza = new bool[] {false} ;
         BC000V8_n889Servico_Terceriza = new bool[] {false} ;
         BC000V8_A1061Servico_Tela = new String[] {""} ;
         BC000V8_n1061Servico_Tela = new bool[] {false} ;
         BC000V8_A1072Servico_Atende = new String[] {""} ;
         BC000V8_n1072Servico_Atende = new bool[] {false} ;
         BC000V8_A1429Servico_ObrigaValores = new String[] {""} ;
         BC000V8_n1429Servico_ObrigaValores = new bool[] {false} ;
         BC000V8_A1436Servico_ObjetoControle = new String[] {""} ;
         BC000V8_A1530Servico_TipoHierarquia = new short[1] ;
         BC000V8_n1530Servico_TipoHierarquia = new bool[] {false} ;
         BC000V8_A1534Servico_PercTmp = new short[1] ;
         BC000V8_n1534Servico_PercTmp = new bool[] {false} ;
         BC000V8_A1535Servico_PercPgm = new short[1] ;
         BC000V8_n1535Servico_PercPgm = new bool[] {false} ;
         BC000V8_A1536Servico_PercCnc = new short[1] ;
         BC000V8_n1536Servico_PercCnc = new bool[] {false} ;
         BC000V8_A1545Servico_Anterior = new int[1] ;
         BC000V8_n1545Servico_Anterior = new bool[] {false} ;
         BC000V8_A1546Servico_Posterior = new int[1] ;
         BC000V8_n1546Servico_Posterior = new bool[] {false} ;
         BC000V8_A632Servico_Ativo = new bool[] {false} ;
         BC000V8_A1635Servico_IsPublico = new bool[] {false} ;
         BC000V8_A2048Servico_LinNegDsc = new String[] {""} ;
         BC000V8_n2048Servico_LinNegDsc = new bool[] {false} ;
         BC000V8_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         BC000V8_A2131Servico_PausaSLA = new bool[] {false} ;
         BC000V8_A157ServicoGrupo_Codigo = new int[1] ;
         BC000V8_A633Servico_UO = new int[1] ;
         BC000V8_n633Servico_UO = new bool[] {false} ;
         BC000V8_A2047Servico_LinNegCod = new int[1] ;
         BC000V8_n2047Servico_LinNegCod = new bool[] {false} ;
         BC000V8_A631Servico_Vinculado = new int[1] ;
         BC000V8_n631Servico_Vinculado = new bool[] {false} ;
         BC000V4_A158ServicoGrupo_Descricao = new String[] {""} ;
         BC000V5_A633Servico_UO = new int[1] ;
         BC000V5_n633Servico_UO = new bool[] {false} ;
         BC000V7_A641Servico_VincDesc = new String[] {""} ;
         BC000V7_n641Servico_VincDesc = new bool[] {false} ;
         BC000V7_A1557Servico_VincSigla = new String[] {""} ;
         BC000V7_n1557Servico_VincSigla = new bool[] {false} ;
         BC000V6_A2048Servico_LinNegDsc = new String[] {""} ;
         BC000V6_n2048Servico_LinNegDsc = new bool[] {false} ;
         BC000V9_A155Servico_Codigo = new int[1] ;
         BC000V3_A155Servico_Codigo = new int[1] ;
         BC000V3_A608Servico_Nome = new String[] {""} ;
         BC000V3_A156Servico_Descricao = new String[] {""} ;
         BC000V3_n156Servico_Descricao = new bool[] {false} ;
         BC000V3_A605Servico_Sigla = new String[] {""} ;
         BC000V3_A1077Servico_UORespExclusiva = new bool[] {false} ;
         BC000V3_n1077Servico_UORespExclusiva = new bool[] {false} ;
         BC000V3_A889Servico_Terceriza = new bool[] {false} ;
         BC000V3_n889Servico_Terceriza = new bool[] {false} ;
         BC000V3_A1061Servico_Tela = new String[] {""} ;
         BC000V3_n1061Servico_Tela = new bool[] {false} ;
         BC000V3_A1072Servico_Atende = new String[] {""} ;
         BC000V3_n1072Servico_Atende = new bool[] {false} ;
         BC000V3_A1429Servico_ObrigaValores = new String[] {""} ;
         BC000V3_n1429Servico_ObrigaValores = new bool[] {false} ;
         BC000V3_A1436Servico_ObjetoControle = new String[] {""} ;
         BC000V3_A1530Servico_TipoHierarquia = new short[1] ;
         BC000V3_n1530Servico_TipoHierarquia = new bool[] {false} ;
         BC000V3_A1534Servico_PercTmp = new short[1] ;
         BC000V3_n1534Servico_PercTmp = new bool[] {false} ;
         BC000V3_A1535Servico_PercPgm = new short[1] ;
         BC000V3_n1535Servico_PercPgm = new bool[] {false} ;
         BC000V3_A1536Servico_PercCnc = new short[1] ;
         BC000V3_n1536Servico_PercCnc = new bool[] {false} ;
         BC000V3_A1545Servico_Anterior = new int[1] ;
         BC000V3_n1545Servico_Anterior = new bool[] {false} ;
         BC000V3_A1546Servico_Posterior = new int[1] ;
         BC000V3_n1546Servico_Posterior = new bool[] {false} ;
         BC000V3_A632Servico_Ativo = new bool[] {false} ;
         BC000V3_A1635Servico_IsPublico = new bool[] {false} ;
         BC000V3_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         BC000V3_A2131Servico_PausaSLA = new bool[] {false} ;
         BC000V3_A157ServicoGrupo_Codigo = new int[1] ;
         BC000V3_A633Servico_UO = new int[1] ;
         BC000V3_n633Servico_UO = new bool[] {false} ;
         BC000V3_A2047Servico_LinNegCod = new int[1] ;
         BC000V3_n2047Servico_LinNegCod = new bool[] {false} ;
         BC000V3_A631Servico_Vinculado = new int[1] ;
         BC000V3_n631Servico_Vinculado = new bool[] {false} ;
         sMode130 = "";
         BC000V2_A155Servico_Codigo = new int[1] ;
         BC000V2_A608Servico_Nome = new String[] {""} ;
         BC000V2_A156Servico_Descricao = new String[] {""} ;
         BC000V2_n156Servico_Descricao = new bool[] {false} ;
         BC000V2_A605Servico_Sigla = new String[] {""} ;
         BC000V2_A1077Servico_UORespExclusiva = new bool[] {false} ;
         BC000V2_n1077Servico_UORespExclusiva = new bool[] {false} ;
         BC000V2_A889Servico_Terceriza = new bool[] {false} ;
         BC000V2_n889Servico_Terceriza = new bool[] {false} ;
         BC000V2_A1061Servico_Tela = new String[] {""} ;
         BC000V2_n1061Servico_Tela = new bool[] {false} ;
         BC000V2_A1072Servico_Atende = new String[] {""} ;
         BC000V2_n1072Servico_Atende = new bool[] {false} ;
         BC000V2_A1429Servico_ObrigaValores = new String[] {""} ;
         BC000V2_n1429Servico_ObrigaValores = new bool[] {false} ;
         BC000V2_A1436Servico_ObjetoControle = new String[] {""} ;
         BC000V2_A1530Servico_TipoHierarquia = new short[1] ;
         BC000V2_n1530Servico_TipoHierarquia = new bool[] {false} ;
         BC000V2_A1534Servico_PercTmp = new short[1] ;
         BC000V2_n1534Servico_PercTmp = new bool[] {false} ;
         BC000V2_A1535Servico_PercPgm = new short[1] ;
         BC000V2_n1535Servico_PercPgm = new bool[] {false} ;
         BC000V2_A1536Servico_PercCnc = new short[1] ;
         BC000V2_n1536Servico_PercCnc = new bool[] {false} ;
         BC000V2_A1545Servico_Anterior = new int[1] ;
         BC000V2_n1545Servico_Anterior = new bool[] {false} ;
         BC000V2_A1546Servico_Posterior = new int[1] ;
         BC000V2_n1546Servico_Posterior = new bool[] {false} ;
         BC000V2_A632Servico_Ativo = new bool[] {false} ;
         BC000V2_A1635Servico_IsPublico = new bool[] {false} ;
         BC000V2_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         BC000V2_A2131Servico_PausaSLA = new bool[] {false} ;
         BC000V2_A157ServicoGrupo_Codigo = new int[1] ;
         BC000V2_A633Servico_UO = new int[1] ;
         BC000V2_n633Servico_UO = new bool[] {false} ;
         BC000V2_A2047Servico_LinNegCod = new int[1] ;
         BC000V2_n2047Servico_LinNegCod = new bool[] {false} ;
         BC000V2_A631Servico_Vinculado = new int[1] ;
         BC000V2_n631Servico_Vinculado = new bool[] {false} ;
         BC000V10_A155Servico_Codigo = new int[1] ;
         BC000V13_A158ServicoGrupo_Descricao = new String[] {""} ;
         BC000V14_A641Servico_VincDesc = new String[] {""} ;
         BC000V14_n641Servico_VincDesc = new bool[] {false} ;
         BC000V14_A1557Servico_VincSigla = new String[] {""} ;
         BC000V14_n1557Servico_VincSigla = new bool[] {false} ;
         BC000V15_A2048Servico_LinNegDsc = new String[] {""} ;
         BC000V15_n2048Servico_LinNegDsc = new bool[] {false} ;
         BC000V16_A631Servico_Vinculado = new int[1] ;
         BC000V16_n631Servico_Vinculado = new bool[] {false} ;
         BC000V17_A1528ServicoFluxo_Codigo = new int[1] ;
         BC000V18_A1528ServicoFluxo_Codigo = new int[1] ;
         BC000V19_A1440ServicoPrioridade_Codigo = new int[1] ;
         BC000V20_A160ContratoServicos_Codigo = new int[1] ;
         BC000V20_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         BC000V20_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         BC000V21_A5AreaTrabalho_Codigo = new int[1] ;
         BC000V22_A828UsuarioServicos_UsuarioCod = new int[1] ;
         BC000V22_A829UsuarioServicos_ServicoCod = new int[1] ;
         BC000V23_A456ContagemResultado_Codigo = new int[1] ;
         BC000V24_A439Solicitacoes_Codigo = new int[1] ;
         BC000V25_A319ServicoAns_Codigo = new int[1] ;
         BC000V26_A160ContratoServicos_Codigo = new int[1] ;
         BC000V27_A155Servico_Codigo = new int[1] ;
         BC000V27_A1839Check_Codigo = new int[1] ;
         BC000V28_A155Servico_Codigo = new int[1] ;
         BC000V28_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         BC000V29_A155Servico_Codigo = new int[1] ;
         BC000V29_A608Servico_Nome = new String[] {""} ;
         BC000V29_A156Servico_Descricao = new String[] {""} ;
         BC000V29_n156Servico_Descricao = new bool[] {false} ;
         BC000V29_A605Servico_Sigla = new String[] {""} ;
         BC000V29_A158ServicoGrupo_Descricao = new String[] {""} ;
         BC000V29_A1077Servico_UORespExclusiva = new bool[] {false} ;
         BC000V29_n1077Servico_UORespExclusiva = new bool[] {false} ;
         BC000V29_A641Servico_VincDesc = new String[] {""} ;
         BC000V29_n641Servico_VincDesc = new bool[] {false} ;
         BC000V29_A1557Servico_VincSigla = new String[] {""} ;
         BC000V29_n1557Servico_VincSigla = new bool[] {false} ;
         BC000V29_A889Servico_Terceriza = new bool[] {false} ;
         BC000V29_n889Servico_Terceriza = new bool[] {false} ;
         BC000V29_A1061Servico_Tela = new String[] {""} ;
         BC000V29_n1061Servico_Tela = new bool[] {false} ;
         BC000V29_A1072Servico_Atende = new String[] {""} ;
         BC000V29_n1072Servico_Atende = new bool[] {false} ;
         BC000V29_A1429Servico_ObrigaValores = new String[] {""} ;
         BC000V29_n1429Servico_ObrigaValores = new bool[] {false} ;
         BC000V29_A1436Servico_ObjetoControle = new String[] {""} ;
         BC000V29_A1530Servico_TipoHierarquia = new short[1] ;
         BC000V29_n1530Servico_TipoHierarquia = new bool[] {false} ;
         BC000V29_A1534Servico_PercTmp = new short[1] ;
         BC000V29_n1534Servico_PercTmp = new bool[] {false} ;
         BC000V29_A1535Servico_PercPgm = new short[1] ;
         BC000V29_n1535Servico_PercPgm = new bool[] {false} ;
         BC000V29_A1536Servico_PercCnc = new short[1] ;
         BC000V29_n1536Servico_PercCnc = new bool[] {false} ;
         BC000V29_A1545Servico_Anterior = new int[1] ;
         BC000V29_n1545Servico_Anterior = new bool[] {false} ;
         BC000V29_A1546Servico_Posterior = new int[1] ;
         BC000V29_n1546Servico_Posterior = new bool[] {false} ;
         BC000V29_A632Servico_Ativo = new bool[] {false} ;
         BC000V29_A1635Servico_IsPublico = new bool[] {false} ;
         BC000V29_A2048Servico_LinNegDsc = new String[] {""} ;
         BC000V29_n2048Servico_LinNegDsc = new bool[] {false} ;
         BC000V29_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         BC000V29_A2131Servico_PausaSLA = new bool[] {false} ;
         BC000V29_A157ServicoGrupo_Codigo = new int[1] ;
         BC000V29_A633Servico_UO = new int[1] ;
         BC000V29_n633Servico_UO = new bool[] {false} ;
         BC000V29_A2047Servico_LinNegCod = new int[1] ;
         BC000V29_n2047Servico_LinNegCod = new bool[] {false} ;
         BC000V29_A631Servico_Vinculado = new int[1] ;
         BC000V29_n631Servico_Vinculado = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servico_bc__default(),
            new Object[][] {
                new Object[] {
               BC000V2_A155Servico_Codigo, BC000V2_A608Servico_Nome, BC000V2_A156Servico_Descricao, BC000V2_n156Servico_Descricao, BC000V2_A605Servico_Sigla, BC000V2_A1077Servico_UORespExclusiva, BC000V2_n1077Servico_UORespExclusiva, BC000V2_A889Servico_Terceriza, BC000V2_n889Servico_Terceriza, BC000V2_A1061Servico_Tela,
               BC000V2_n1061Servico_Tela, BC000V2_A1072Servico_Atende, BC000V2_n1072Servico_Atende, BC000V2_A1429Servico_ObrigaValores, BC000V2_n1429Servico_ObrigaValores, BC000V2_A1436Servico_ObjetoControle, BC000V2_A1530Servico_TipoHierarquia, BC000V2_n1530Servico_TipoHierarquia, BC000V2_A1534Servico_PercTmp, BC000V2_n1534Servico_PercTmp,
               BC000V2_A1535Servico_PercPgm, BC000V2_n1535Servico_PercPgm, BC000V2_A1536Servico_PercCnc, BC000V2_n1536Servico_PercCnc, BC000V2_A1545Servico_Anterior, BC000V2_n1545Servico_Anterior, BC000V2_A1546Servico_Posterior, BC000V2_n1546Servico_Posterior, BC000V2_A632Servico_Ativo, BC000V2_A1635Servico_IsPublico,
               BC000V2_A2092Servico_IsOrigemReferencia, BC000V2_A2131Servico_PausaSLA, BC000V2_A157ServicoGrupo_Codigo, BC000V2_A633Servico_UO, BC000V2_n633Servico_UO, BC000V2_A2047Servico_LinNegCod, BC000V2_n2047Servico_LinNegCod, BC000V2_A631Servico_Vinculado, BC000V2_n631Servico_Vinculado
               }
               , new Object[] {
               BC000V3_A155Servico_Codigo, BC000V3_A608Servico_Nome, BC000V3_A156Servico_Descricao, BC000V3_n156Servico_Descricao, BC000V3_A605Servico_Sigla, BC000V3_A1077Servico_UORespExclusiva, BC000V3_n1077Servico_UORespExclusiva, BC000V3_A889Servico_Terceriza, BC000V3_n889Servico_Terceriza, BC000V3_A1061Servico_Tela,
               BC000V3_n1061Servico_Tela, BC000V3_A1072Servico_Atende, BC000V3_n1072Servico_Atende, BC000V3_A1429Servico_ObrigaValores, BC000V3_n1429Servico_ObrigaValores, BC000V3_A1436Servico_ObjetoControle, BC000V3_A1530Servico_TipoHierarquia, BC000V3_n1530Servico_TipoHierarquia, BC000V3_A1534Servico_PercTmp, BC000V3_n1534Servico_PercTmp,
               BC000V3_A1535Servico_PercPgm, BC000V3_n1535Servico_PercPgm, BC000V3_A1536Servico_PercCnc, BC000V3_n1536Servico_PercCnc, BC000V3_A1545Servico_Anterior, BC000V3_n1545Servico_Anterior, BC000V3_A1546Servico_Posterior, BC000V3_n1546Servico_Posterior, BC000V3_A632Servico_Ativo, BC000V3_A1635Servico_IsPublico,
               BC000V3_A2092Servico_IsOrigemReferencia, BC000V3_A2131Servico_PausaSLA, BC000V3_A157ServicoGrupo_Codigo, BC000V3_A633Servico_UO, BC000V3_n633Servico_UO, BC000V3_A2047Servico_LinNegCod, BC000V3_n2047Servico_LinNegCod, BC000V3_A631Servico_Vinculado, BC000V3_n631Servico_Vinculado
               }
               , new Object[] {
               BC000V4_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               BC000V5_A633Servico_UO
               }
               , new Object[] {
               BC000V6_A2048Servico_LinNegDsc, BC000V6_n2048Servico_LinNegDsc
               }
               , new Object[] {
               BC000V7_A641Servico_VincDesc, BC000V7_n641Servico_VincDesc, BC000V7_A1557Servico_VincSigla, BC000V7_n1557Servico_VincSigla
               }
               , new Object[] {
               BC000V8_A155Servico_Codigo, BC000V8_A608Servico_Nome, BC000V8_A156Servico_Descricao, BC000V8_n156Servico_Descricao, BC000V8_A605Servico_Sigla, BC000V8_A158ServicoGrupo_Descricao, BC000V8_A1077Servico_UORespExclusiva, BC000V8_n1077Servico_UORespExclusiva, BC000V8_A641Servico_VincDesc, BC000V8_n641Servico_VincDesc,
               BC000V8_A1557Servico_VincSigla, BC000V8_n1557Servico_VincSigla, BC000V8_A889Servico_Terceriza, BC000V8_n889Servico_Terceriza, BC000V8_A1061Servico_Tela, BC000V8_n1061Servico_Tela, BC000V8_A1072Servico_Atende, BC000V8_n1072Servico_Atende, BC000V8_A1429Servico_ObrigaValores, BC000V8_n1429Servico_ObrigaValores,
               BC000V8_A1436Servico_ObjetoControle, BC000V8_A1530Servico_TipoHierarquia, BC000V8_n1530Servico_TipoHierarquia, BC000V8_A1534Servico_PercTmp, BC000V8_n1534Servico_PercTmp, BC000V8_A1535Servico_PercPgm, BC000V8_n1535Servico_PercPgm, BC000V8_A1536Servico_PercCnc, BC000V8_n1536Servico_PercCnc, BC000V8_A1545Servico_Anterior,
               BC000V8_n1545Servico_Anterior, BC000V8_A1546Servico_Posterior, BC000V8_n1546Servico_Posterior, BC000V8_A632Servico_Ativo, BC000V8_A1635Servico_IsPublico, BC000V8_A2048Servico_LinNegDsc, BC000V8_n2048Servico_LinNegDsc, BC000V8_A2092Servico_IsOrigemReferencia, BC000V8_A2131Servico_PausaSLA, BC000V8_A157ServicoGrupo_Codigo,
               BC000V8_A633Servico_UO, BC000V8_n633Servico_UO, BC000V8_A2047Servico_LinNegCod, BC000V8_n2047Servico_LinNegCod, BC000V8_A631Servico_Vinculado, BC000V8_n631Servico_Vinculado
               }
               , new Object[] {
               BC000V9_A155Servico_Codigo
               }
               , new Object[] {
               BC000V10_A155Servico_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000V13_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               BC000V14_A641Servico_VincDesc, BC000V14_n641Servico_VincDesc, BC000V14_A1557Servico_VincSigla, BC000V14_n1557Servico_VincSigla
               }
               , new Object[] {
               BC000V15_A2048Servico_LinNegDsc, BC000V15_n2048Servico_LinNegDsc
               }
               , new Object[] {
               BC000V16_A631Servico_Vinculado
               }
               , new Object[] {
               BC000V17_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               BC000V18_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               BC000V19_A1440ServicoPrioridade_Codigo
               }
               , new Object[] {
               BC000V20_A160ContratoServicos_Codigo, BC000V20_A1067ContratoServicosSistemas_ServicoCod, BC000V20_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               BC000V21_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               BC000V22_A828UsuarioServicos_UsuarioCod, BC000V22_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               BC000V23_A456ContagemResultado_Codigo
               }
               , new Object[] {
               BC000V24_A439Solicitacoes_Codigo
               }
               , new Object[] {
               BC000V25_A319ServicoAns_Codigo
               }
               , new Object[] {
               BC000V26_A160ContratoServicos_Codigo
               }
               , new Object[] {
               BC000V27_A155Servico_Codigo, BC000V27_A1839Check_Codigo
               }
               , new Object[] {
               BC000V28_A155Servico_Codigo, BC000V28_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               BC000V29_A155Servico_Codigo, BC000V29_A608Servico_Nome, BC000V29_A156Servico_Descricao, BC000V29_n156Servico_Descricao, BC000V29_A605Servico_Sigla, BC000V29_A158ServicoGrupo_Descricao, BC000V29_A1077Servico_UORespExclusiva, BC000V29_n1077Servico_UORespExclusiva, BC000V29_A641Servico_VincDesc, BC000V29_n641Servico_VincDesc,
               BC000V29_A1557Servico_VincSigla, BC000V29_n1557Servico_VincSigla, BC000V29_A889Servico_Terceriza, BC000V29_n889Servico_Terceriza, BC000V29_A1061Servico_Tela, BC000V29_n1061Servico_Tela, BC000V29_A1072Servico_Atende, BC000V29_n1072Servico_Atende, BC000V29_A1429Servico_ObrigaValores, BC000V29_n1429Servico_ObrigaValores,
               BC000V29_A1436Servico_ObjetoControle, BC000V29_A1530Servico_TipoHierarquia, BC000V29_n1530Servico_TipoHierarquia, BC000V29_A1534Servico_PercTmp, BC000V29_n1534Servico_PercTmp, BC000V29_A1535Servico_PercPgm, BC000V29_n1535Servico_PercPgm, BC000V29_A1536Servico_PercCnc, BC000V29_n1536Servico_PercCnc, BC000V29_A1545Servico_Anterior,
               BC000V29_n1545Servico_Anterior, BC000V29_A1546Servico_Posterior, BC000V29_n1546Servico_Posterior, BC000V29_A632Servico_Ativo, BC000V29_A1635Servico_IsPublico, BC000V29_A2048Servico_LinNegDsc, BC000V29_n2048Servico_LinNegDsc, BC000V29_A2092Servico_IsOrigemReferencia, BC000V29_A2131Servico_PausaSLA, BC000V29_A157ServicoGrupo_Codigo,
               BC000V29_A633Servico_UO, BC000V29_n633Servico_UO, BC000V29_A2047Servico_LinNegCod, BC000V29_n2047Servico_LinNegCod, BC000V29_A631Servico_Vinculado, BC000V29_n631Servico_Vinculado
               }
            }
         );
         Z2131Servico_PausaSLA = false;
         A2131Servico_PausaSLA = false;
         i2131Servico_PausaSLA = false;
         A1635Servico_IsPublico = false;
         Z1635Servico_IsPublico = false;
         i1635Servico_IsPublico = false;
         Z632Servico_Ativo = true;
         A632Servico_Ativo = true;
         i632Servico_Ativo = true;
         Z1530Servico_TipoHierarquia = 1;
         n1530Servico_TipoHierarquia = false;
         A1530Servico_TipoHierarquia = 1;
         n1530Servico_TipoHierarquia = false;
         i1530Servico_TipoHierarquia = 1;
         n1530Servico_TipoHierarquia = false;
         AV25Pgmname = "Servico_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E120V2 */
         E120V2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z1530Servico_TipoHierarquia ;
      private short A1530Servico_TipoHierarquia ;
      private short Z1534Servico_PercTmp ;
      private short A1534Servico_PercTmp ;
      private short Z1535Servico_PercPgm ;
      private short A1535Servico_PercPgm ;
      private short Z1536Servico_PercCnc ;
      private short A1536Servico_PercCnc ;
      private short Z640Servico_Vinculados ;
      private short A640Servico_Vinculados ;
      private short Gx_BScreen ;
      private short RcdFound130 ;
      private short GXt_int2 ;
      private short i1530Servico_TipoHierarquia ;
      private int trnEnded ;
      private int Z155Servico_Codigo ;
      private int A155Servico_Codigo ;
      private int AV26GXV1 ;
      private int AV11Insert_ServicoGrupo_Codigo ;
      private int AV15Insert_Servico_UO ;
      private int AV14Insert_Servico_Vinculado ;
      private int AV24Insert_Servico_LinNegCod ;
      private int AV17Servico_UO ;
      private int AV22Responsavel_Codigo ;
      private int AV7Servico_Codigo ;
      private int Z1545Servico_Anterior ;
      private int A1545Servico_Anterior ;
      private int Z1546Servico_Posterior ;
      private int A1546Servico_Posterior ;
      private int Z157ServicoGrupo_Codigo ;
      private int A157ServicoGrupo_Codigo ;
      private int Z633Servico_UO ;
      private int A633Servico_UO ;
      private int Z2047Servico_LinNegCod ;
      private int A2047Servico_LinNegCod ;
      private int Z631Servico_Vinculado ;
      private int A631Servico_Vinculado ;
      private int Z1551Servico_Responsavel ;
      private int A1551Servico_Responsavel ;
      private int GXt_int1 ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV25Pgmname ;
      private String Z608Servico_Nome ;
      private String A608Servico_Nome ;
      private String Z605Servico_Sigla ;
      private String A605Servico_Sigla ;
      private String Z1061Servico_Tela ;
      private String A1061Servico_Tela ;
      private String Z1072Servico_Atende ;
      private String A1072Servico_Atende ;
      private String Z1429Servico_ObrigaValores ;
      private String A1429Servico_ObrigaValores ;
      private String Z1436Servico_ObjetoControle ;
      private String A1436Servico_ObjetoControle ;
      private String Z1557Servico_VincSigla ;
      private String A1557Servico_VincSigla ;
      private String sMode130 ;
      private bool returnInSub ;
      private bool A1635Servico_IsPublico ;
      private bool Z1077Servico_UORespExclusiva ;
      private bool A1077Servico_UORespExclusiva ;
      private bool Z889Servico_Terceriza ;
      private bool A889Servico_Terceriza ;
      private bool Z632Servico_Ativo ;
      private bool A632Servico_Ativo ;
      private bool Z1635Servico_IsPublico ;
      private bool Z2092Servico_IsOrigemReferencia ;
      private bool A2092Servico_IsOrigemReferencia ;
      private bool Z2131Servico_PausaSLA ;
      private bool A2131Servico_PausaSLA ;
      private bool n1530Servico_TipoHierarquia ;
      private bool n156Servico_Descricao ;
      private bool n1077Servico_UORespExclusiva ;
      private bool n641Servico_VincDesc ;
      private bool n1557Servico_VincSigla ;
      private bool n889Servico_Terceriza ;
      private bool n1061Servico_Tela ;
      private bool n1072Servico_Atende ;
      private bool n1429Servico_ObrigaValores ;
      private bool n1534Servico_PercTmp ;
      private bool n1535Servico_PercPgm ;
      private bool n1536Servico_PercCnc ;
      private bool n1545Servico_Anterior ;
      private bool n1546Servico_Posterior ;
      private bool n2048Servico_LinNegDsc ;
      private bool n633Servico_UO ;
      private bool n2047Servico_LinNegCod ;
      private bool n631Servico_Vinculado ;
      private bool Gx_longc ;
      private bool i2131Servico_PausaSLA ;
      private bool i1635Servico_IsPublico ;
      private bool i632Servico_Ativo ;
      private String Z156Servico_Descricao ;
      private String A156Servico_Descricao ;
      private String Z641Servico_VincDesc ;
      private String A641Servico_VincDesc ;
      private String Z2107Servico_Identificacao ;
      private String A2107Servico_Identificacao ;
      private String Z158ServicoGrupo_Descricao ;
      private String A158ServicoGrupo_Descricao ;
      private String Z2048Servico_LinNegDsc ;
      private String A2048Servico_LinNegDsc ;
      private IGxSession AV10WebSession ;
      private SdtServico bcServico ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC000V8_A155Servico_Codigo ;
      private String[] BC000V8_A608Servico_Nome ;
      private String[] BC000V8_A156Servico_Descricao ;
      private bool[] BC000V8_n156Servico_Descricao ;
      private String[] BC000V8_A605Servico_Sigla ;
      private String[] BC000V8_A158ServicoGrupo_Descricao ;
      private bool[] BC000V8_A1077Servico_UORespExclusiva ;
      private bool[] BC000V8_n1077Servico_UORespExclusiva ;
      private String[] BC000V8_A641Servico_VincDesc ;
      private bool[] BC000V8_n641Servico_VincDesc ;
      private String[] BC000V8_A1557Servico_VincSigla ;
      private bool[] BC000V8_n1557Servico_VincSigla ;
      private bool[] BC000V8_A889Servico_Terceriza ;
      private bool[] BC000V8_n889Servico_Terceriza ;
      private String[] BC000V8_A1061Servico_Tela ;
      private bool[] BC000V8_n1061Servico_Tela ;
      private String[] BC000V8_A1072Servico_Atende ;
      private bool[] BC000V8_n1072Servico_Atende ;
      private String[] BC000V8_A1429Servico_ObrigaValores ;
      private bool[] BC000V8_n1429Servico_ObrigaValores ;
      private String[] BC000V8_A1436Servico_ObjetoControle ;
      private short[] BC000V8_A1530Servico_TipoHierarquia ;
      private bool[] BC000V8_n1530Servico_TipoHierarquia ;
      private short[] BC000V8_A1534Servico_PercTmp ;
      private bool[] BC000V8_n1534Servico_PercTmp ;
      private short[] BC000V8_A1535Servico_PercPgm ;
      private bool[] BC000V8_n1535Servico_PercPgm ;
      private short[] BC000V8_A1536Servico_PercCnc ;
      private bool[] BC000V8_n1536Servico_PercCnc ;
      private int[] BC000V8_A1545Servico_Anterior ;
      private bool[] BC000V8_n1545Servico_Anterior ;
      private int[] BC000V8_A1546Servico_Posterior ;
      private bool[] BC000V8_n1546Servico_Posterior ;
      private bool[] BC000V8_A632Servico_Ativo ;
      private bool[] BC000V8_A1635Servico_IsPublico ;
      private String[] BC000V8_A2048Servico_LinNegDsc ;
      private bool[] BC000V8_n2048Servico_LinNegDsc ;
      private bool[] BC000V8_A2092Servico_IsOrigemReferencia ;
      private bool[] BC000V8_A2131Servico_PausaSLA ;
      private int[] BC000V8_A157ServicoGrupo_Codigo ;
      private int[] BC000V8_A633Servico_UO ;
      private bool[] BC000V8_n633Servico_UO ;
      private int[] BC000V8_A2047Servico_LinNegCod ;
      private bool[] BC000V8_n2047Servico_LinNegCod ;
      private int[] BC000V8_A631Servico_Vinculado ;
      private bool[] BC000V8_n631Servico_Vinculado ;
      private String[] BC000V4_A158ServicoGrupo_Descricao ;
      private int[] BC000V5_A633Servico_UO ;
      private bool[] BC000V5_n633Servico_UO ;
      private String[] BC000V7_A641Servico_VincDesc ;
      private bool[] BC000V7_n641Servico_VincDesc ;
      private String[] BC000V7_A1557Servico_VincSigla ;
      private bool[] BC000V7_n1557Servico_VincSigla ;
      private String[] BC000V6_A2048Servico_LinNegDsc ;
      private bool[] BC000V6_n2048Servico_LinNegDsc ;
      private int[] BC000V9_A155Servico_Codigo ;
      private int[] BC000V3_A155Servico_Codigo ;
      private String[] BC000V3_A608Servico_Nome ;
      private String[] BC000V3_A156Servico_Descricao ;
      private bool[] BC000V3_n156Servico_Descricao ;
      private String[] BC000V3_A605Servico_Sigla ;
      private bool[] BC000V3_A1077Servico_UORespExclusiva ;
      private bool[] BC000V3_n1077Servico_UORespExclusiva ;
      private bool[] BC000V3_A889Servico_Terceriza ;
      private bool[] BC000V3_n889Servico_Terceriza ;
      private String[] BC000V3_A1061Servico_Tela ;
      private bool[] BC000V3_n1061Servico_Tela ;
      private String[] BC000V3_A1072Servico_Atende ;
      private bool[] BC000V3_n1072Servico_Atende ;
      private String[] BC000V3_A1429Servico_ObrigaValores ;
      private bool[] BC000V3_n1429Servico_ObrigaValores ;
      private String[] BC000V3_A1436Servico_ObjetoControle ;
      private short[] BC000V3_A1530Servico_TipoHierarquia ;
      private bool[] BC000V3_n1530Servico_TipoHierarquia ;
      private short[] BC000V3_A1534Servico_PercTmp ;
      private bool[] BC000V3_n1534Servico_PercTmp ;
      private short[] BC000V3_A1535Servico_PercPgm ;
      private bool[] BC000V3_n1535Servico_PercPgm ;
      private short[] BC000V3_A1536Servico_PercCnc ;
      private bool[] BC000V3_n1536Servico_PercCnc ;
      private int[] BC000V3_A1545Servico_Anterior ;
      private bool[] BC000V3_n1545Servico_Anterior ;
      private int[] BC000V3_A1546Servico_Posterior ;
      private bool[] BC000V3_n1546Servico_Posterior ;
      private bool[] BC000V3_A632Servico_Ativo ;
      private bool[] BC000V3_A1635Servico_IsPublico ;
      private bool[] BC000V3_A2092Servico_IsOrigemReferencia ;
      private bool[] BC000V3_A2131Servico_PausaSLA ;
      private int[] BC000V3_A157ServicoGrupo_Codigo ;
      private int[] BC000V3_A633Servico_UO ;
      private bool[] BC000V3_n633Servico_UO ;
      private int[] BC000V3_A2047Servico_LinNegCod ;
      private bool[] BC000V3_n2047Servico_LinNegCod ;
      private int[] BC000V3_A631Servico_Vinculado ;
      private bool[] BC000V3_n631Servico_Vinculado ;
      private int[] BC000V2_A155Servico_Codigo ;
      private String[] BC000V2_A608Servico_Nome ;
      private String[] BC000V2_A156Servico_Descricao ;
      private bool[] BC000V2_n156Servico_Descricao ;
      private String[] BC000V2_A605Servico_Sigla ;
      private bool[] BC000V2_A1077Servico_UORespExclusiva ;
      private bool[] BC000V2_n1077Servico_UORespExclusiva ;
      private bool[] BC000V2_A889Servico_Terceriza ;
      private bool[] BC000V2_n889Servico_Terceriza ;
      private String[] BC000V2_A1061Servico_Tela ;
      private bool[] BC000V2_n1061Servico_Tela ;
      private String[] BC000V2_A1072Servico_Atende ;
      private bool[] BC000V2_n1072Servico_Atende ;
      private String[] BC000V2_A1429Servico_ObrigaValores ;
      private bool[] BC000V2_n1429Servico_ObrigaValores ;
      private String[] BC000V2_A1436Servico_ObjetoControle ;
      private short[] BC000V2_A1530Servico_TipoHierarquia ;
      private bool[] BC000V2_n1530Servico_TipoHierarquia ;
      private short[] BC000V2_A1534Servico_PercTmp ;
      private bool[] BC000V2_n1534Servico_PercTmp ;
      private short[] BC000V2_A1535Servico_PercPgm ;
      private bool[] BC000V2_n1535Servico_PercPgm ;
      private short[] BC000V2_A1536Servico_PercCnc ;
      private bool[] BC000V2_n1536Servico_PercCnc ;
      private int[] BC000V2_A1545Servico_Anterior ;
      private bool[] BC000V2_n1545Servico_Anterior ;
      private int[] BC000V2_A1546Servico_Posterior ;
      private bool[] BC000V2_n1546Servico_Posterior ;
      private bool[] BC000V2_A632Servico_Ativo ;
      private bool[] BC000V2_A1635Servico_IsPublico ;
      private bool[] BC000V2_A2092Servico_IsOrigemReferencia ;
      private bool[] BC000V2_A2131Servico_PausaSLA ;
      private int[] BC000V2_A157ServicoGrupo_Codigo ;
      private int[] BC000V2_A633Servico_UO ;
      private bool[] BC000V2_n633Servico_UO ;
      private int[] BC000V2_A2047Servico_LinNegCod ;
      private bool[] BC000V2_n2047Servico_LinNegCod ;
      private int[] BC000V2_A631Servico_Vinculado ;
      private bool[] BC000V2_n631Servico_Vinculado ;
      private int[] BC000V10_A155Servico_Codigo ;
      private String[] BC000V13_A158ServicoGrupo_Descricao ;
      private String[] BC000V14_A641Servico_VincDesc ;
      private bool[] BC000V14_n641Servico_VincDesc ;
      private String[] BC000V14_A1557Servico_VincSigla ;
      private bool[] BC000V14_n1557Servico_VincSigla ;
      private String[] BC000V15_A2048Servico_LinNegDsc ;
      private bool[] BC000V15_n2048Servico_LinNegDsc ;
      private int[] BC000V16_A631Servico_Vinculado ;
      private bool[] BC000V16_n631Servico_Vinculado ;
      private int[] BC000V17_A1528ServicoFluxo_Codigo ;
      private int[] BC000V18_A1528ServicoFluxo_Codigo ;
      private int[] BC000V19_A1440ServicoPrioridade_Codigo ;
      private int[] BC000V20_A160ContratoServicos_Codigo ;
      private int[] BC000V20_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] BC000V20_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] BC000V21_A5AreaTrabalho_Codigo ;
      private int[] BC000V22_A828UsuarioServicos_UsuarioCod ;
      private int[] BC000V22_A829UsuarioServicos_ServicoCod ;
      private int[] BC000V23_A456ContagemResultado_Codigo ;
      private int[] BC000V24_A439Solicitacoes_Codigo ;
      private int[] BC000V25_A319ServicoAns_Codigo ;
      private int[] BC000V26_A160ContratoServicos_Codigo ;
      private int[] BC000V27_A155Servico_Codigo ;
      private int[] BC000V27_A1839Check_Codigo ;
      private int[] BC000V28_A155Servico_Codigo ;
      private int[] BC000V28_A1766ServicoArtefato_ArtefatoCod ;
      private int[] BC000V29_A155Servico_Codigo ;
      private String[] BC000V29_A608Servico_Nome ;
      private String[] BC000V29_A156Servico_Descricao ;
      private bool[] BC000V29_n156Servico_Descricao ;
      private String[] BC000V29_A605Servico_Sigla ;
      private String[] BC000V29_A158ServicoGrupo_Descricao ;
      private bool[] BC000V29_A1077Servico_UORespExclusiva ;
      private bool[] BC000V29_n1077Servico_UORespExclusiva ;
      private String[] BC000V29_A641Servico_VincDesc ;
      private bool[] BC000V29_n641Servico_VincDesc ;
      private String[] BC000V29_A1557Servico_VincSigla ;
      private bool[] BC000V29_n1557Servico_VincSigla ;
      private bool[] BC000V29_A889Servico_Terceriza ;
      private bool[] BC000V29_n889Servico_Terceriza ;
      private String[] BC000V29_A1061Servico_Tela ;
      private bool[] BC000V29_n1061Servico_Tela ;
      private String[] BC000V29_A1072Servico_Atende ;
      private bool[] BC000V29_n1072Servico_Atende ;
      private String[] BC000V29_A1429Servico_ObrigaValores ;
      private bool[] BC000V29_n1429Servico_ObrigaValores ;
      private String[] BC000V29_A1436Servico_ObjetoControle ;
      private short[] BC000V29_A1530Servico_TipoHierarquia ;
      private bool[] BC000V29_n1530Servico_TipoHierarquia ;
      private short[] BC000V29_A1534Servico_PercTmp ;
      private bool[] BC000V29_n1534Servico_PercTmp ;
      private short[] BC000V29_A1535Servico_PercPgm ;
      private bool[] BC000V29_n1535Servico_PercPgm ;
      private short[] BC000V29_A1536Servico_PercCnc ;
      private bool[] BC000V29_n1536Servico_PercCnc ;
      private int[] BC000V29_A1545Servico_Anterior ;
      private bool[] BC000V29_n1545Servico_Anterior ;
      private int[] BC000V29_A1546Servico_Posterior ;
      private bool[] BC000V29_n1546Servico_Posterior ;
      private bool[] BC000V29_A632Servico_Ativo ;
      private bool[] BC000V29_A1635Servico_IsPublico ;
      private String[] BC000V29_A2048Servico_LinNegDsc ;
      private bool[] BC000V29_n2048Servico_LinNegDsc ;
      private bool[] BC000V29_A2092Servico_IsOrigemReferencia ;
      private bool[] BC000V29_A2131Servico_PausaSLA ;
      private int[] BC000V29_A157ServicoGrupo_Codigo ;
      private int[] BC000V29_A633Servico_UO ;
      private bool[] BC000V29_n633Servico_UO ;
      private int[] BC000V29_A2047Servico_LinNegCod ;
      private bool[] BC000V29_n2047Servico_LinNegCod ;
      private int[] BC000V29_A631Servico_Vinculado ;
      private bool[] BC000V29_n631Servico_Vinculado ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtAuditingObject AV19AuditingObject ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class servico_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC000V8 ;
          prmBC000V8 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V4 ;
          prmBC000V4 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V5 ;
          prmBC000V5 = new Object[] {
          new Object[] {"@Servico_UO",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V7 ;
          prmBC000V7 = new Object[] {
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V6 ;
          prmBC000V6 = new Object[] {
          new Object[] {"@Servico_LinNegCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V9 ;
          prmBC000V9 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V3 ;
          prmBC000V3 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V2 ;
          prmBC000V2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V10 ;
          prmBC000V10 = new Object[] {
          new Object[] {"@Servico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Servico_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Servico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Servico_UORespExclusiva",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Terceriza",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Tela",SqlDbType.Char,3,0} ,
          new Object[] {"@Servico_Atende",SqlDbType.Char,1,0} ,
          new Object[] {"@Servico_ObrigaValores",SqlDbType.Char,1,0} ,
          new Object[] {"@Servico_ObjetoControle",SqlDbType.Char,3,0} ,
          new Object[] {"@Servico_TipoHierarquia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_Anterior",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Posterior",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_IsPublico",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_IsOrigemReferencia",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_PausaSLA",SqlDbType.Bit,4,0} ,
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_UO",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_LinNegCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V11 ;
          prmBC000V11 = new Object[] {
          new Object[] {"@Servico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Servico_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Servico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Servico_UORespExclusiva",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Terceriza",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Tela",SqlDbType.Char,3,0} ,
          new Object[] {"@Servico_Atende",SqlDbType.Char,1,0} ,
          new Object[] {"@Servico_ObrigaValores",SqlDbType.Char,1,0} ,
          new Object[] {"@Servico_ObjetoControle",SqlDbType.Char,3,0} ,
          new Object[] {"@Servico_TipoHierarquia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_Anterior",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Posterior",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_IsPublico",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_IsOrigemReferencia",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_PausaSLA",SqlDbType.Bit,4,0} ,
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_UO",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_LinNegCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V12 ;
          prmBC000V12 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V13 ;
          prmBC000V13 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V14 ;
          prmBC000V14 = new Object[] {
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V15 ;
          prmBC000V15 = new Object[] {
          new Object[] {"@Servico_LinNegCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V16 ;
          prmBC000V16 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V17 ;
          prmBC000V17 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V18 ;
          prmBC000V18 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V19 ;
          prmBC000V19 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V20 ;
          prmBC000V20 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V21 ;
          prmBC000V21 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V22 ;
          prmBC000V22 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V23 ;
          prmBC000V23 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V24 ;
          prmBC000V24 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V25 ;
          prmBC000V25 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V26 ;
          prmBC000V26 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V27 ;
          prmBC000V27 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V28 ;
          prmBC000V28 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000V29 ;
          prmBC000V29 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC000V2", "SELECT [Servico_Codigo], [Servico_Nome], [Servico_Descricao], [Servico_Sigla], [Servico_UORespExclusiva], [Servico_Terceriza], [Servico_Tela], [Servico_Atende], [Servico_ObrigaValores], [Servico_ObjetoControle], [Servico_TipoHierarquia], [Servico_PercTmp], [Servico_PercPgm], [Servico_PercCnc], [Servico_Anterior], [Servico_Posterior], [Servico_Ativo], [Servico_IsPublico], [Servico_IsOrigemReferencia], [Servico_PausaSLA], [ServicoGrupo_Codigo], [Servico_UO] AS Servico_UO, [Servico_LinNegCod] AS Servico_LinNegCod, [Servico_Vinculado] AS Servico_Vinculado FROM [Servico] WITH (UPDLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V2,1,0,true,false )
             ,new CursorDef("BC000V3", "SELECT [Servico_Codigo], [Servico_Nome], [Servico_Descricao], [Servico_Sigla], [Servico_UORespExclusiva], [Servico_Terceriza], [Servico_Tela], [Servico_Atende], [Servico_ObrigaValores], [Servico_ObjetoControle], [Servico_TipoHierarquia], [Servico_PercTmp], [Servico_PercPgm], [Servico_PercCnc], [Servico_Anterior], [Servico_Posterior], [Servico_Ativo], [Servico_IsPublico], [Servico_IsOrigemReferencia], [Servico_PausaSLA], [ServicoGrupo_Codigo], [Servico_UO] AS Servico_UO, [Servico_LinNegCod] AS Servico_LinNegCod, [Servico_Vinculado] AS Servico_Vinculado FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V3,1,0,true,false )
             ,new CursorDef("BC000V4", "SELECT [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V4,1,0,true,false )
             ,new CursorDef("BC000V5", "SELECT [UnidadeOrganizacional_Codigo] AS Servico_UO FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Servico_UO ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V5,1,0,true,false )
             ,new CursorDef("BC000V6", "SELECT [LinhaNegocio_Descricao] AS Servico_LinNegDsc FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @Servico_LinNegCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V6,1,0,true,false )
             ,new CursorDef("BC000V7", "SELECT [Servico_Descricao] AS Servico_VincDesc, [Servico_Sigla] AS Servico_VincSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Vinculado ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V7,1,0,true,false )
             ,new CursorDef("BC000V8", "SELECT TM1.[Servico_Codigo], TM1.[Servico_Nome], TM1.[Servico_Descricao], TM1.[Servico_Sigla], T2.[ServicoGrupo_Descricao], TM1.[Servico_UORespExclusiva], T3.[Servico_Descricao] AS Servico_VincDesc, T3.[Servico_Sigla] AS Servico_VincSigla, TM1.[Servico_Terceriza], TM1.[Servico_Tela], TM1.[Servico_Atende], TM1.[Servico_ObrigaValores], TM1.[Servico_ObjetoControle], TM1.[Servico_TipoHierarquia], TM1.[Servico_PercTmp], TM1.[Servico_PercPgm], TM1.[Servico_PercCnc], TM1.[Servico_Anterior], TM1.[Servico_Posterior], TM1.[Servico_Ativo], TM1.[Servico_IsPublico], T4.[LinhaNegocio_Descricao] AS Servico_LinNegDsc, TM1.[Servico_IsOrigemReferencia], TM1.[Servico_PausaSLA], TM1.[ServicoGrupo_Codigo], TM1.[Servico_UO] AS Servico_UO, TM1.[Servico_LinNegCod] AS Servico_LinNegCod, TM1.[Servico_Vinculado] AS Servico_Vinculado FROM ((([Servico] TM1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = TM1.[ServicoGrupo_Codigo]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = TM1.[Servico_Vinculado]) LEFT JOIN [LinhaNegocio] T4 WITH (NOLOCK) ON T4.[LinhadeNegocio_Codigo] = TM1.[Servico_LinNegCod]) WHERE TM1.[Servico_Codigo] = @Servico_Codigo ORDER BY TM1.[Servico_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V8,100,0,true,false )
             ,new CursorDef("BC000V9", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V9,1,0,true,false )
             ,new CursorDef("BC000V10", "INSERT INTO [Servico]([Servico_Nome], [Servico_Descricao], [Servico_Sigla], [Servico_UORespExclusiva], [Servico_Terceriza], [Servico_Tela], [Servico_Atende], [Servico_ObrigaValores], [Servico_ObjetoControle], [Servico_TipoHierarquia], [Servico_PercTmp], [Servico_PercPgm], [Servico_PercCnc], [Servico_Anterior], [Servico_Posterior], [Servico_Ativo], [Servico_IsPublico], [Servico_IsOrigemReferencia], [Servico_PausaSLA], [ServicoGrupo_Codigo], [Servico_UO], [Servico_LinNegCod], [Servico_Vinculado]) VALUES(@Servico_Nome, @Servico_Descricao, @Servico_Sigla, @Servico_UORespExclusiva, @Servico_Terceriza, @Servico_Tela, @Servico_Atende, @Servico_ObrigaValores, @Servico_ObjetoControle, @Servico_TipoHierarquia, @Servico_PercTmp, @Servico_PercPgm, @Servico_PercCnc, @Servico_Anterior, @Servico_Posterior, @Servico_Ativo, @Servico_IsPublico, @Servico_IsOrigemReferencia, @Servico_PausaSLA, @ServicoGrupo_Codigo, @Servico_UO, @Servico_LinNegCod, @Servico_Vinculado); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC000V10)
             ,new CursorDef("BC000V11", "UPDATE [Servico] SET [Servico_Nome]=@Servico_Nome, [Servico_Descricao]=@Servico_Descricao, [Servico_Sigla]=@Servico_Sigla, [Servico_UORespExclusiva]=@Servico_UORespExclusiva, [Servico_Terceriza]=@Servico_Terceriza, [Servico_Tela]=@Servico_Tela, [Servico_Atende]=@Servico_Atende, [Servico_ObrigaValores]=@Servico_ObrigaValores, [Servico_ObjetoControle]=@Servico_ObjetoControle, [Servico_TipoHierarquia]=@Servico_TipoHierarquia, [Servico_PercTmp]=@Servico_PercTmp, [Servico_PercPgm]=@Servico_PercPgm, [Servico_PercCnc]=@Servico_PercCnc, [Servico_Anterior]=@Servico_Anterior, [Servico_Posterior]=@Servico_Posterior, [Servico_Ativo]=@Servico_Ativo, [Servico_IsPublico]=@Servico_IsPublico, [Servico_IsOrigemReferencia]=@Servico_IsOrigemReferencia, [Servico_PausaSLA]=@Servico_PausaSLA, [ServicoGrupo_Codigo]=@ServicoGrupo_Codigo, [Servico_UO]=@Servico_UO, [Servico_LinNegCod]=@Servico_LinNegCod, [Servico_Vinculado]=@Servico_Vinculado  WHERE [Servico_Codigo] = @Servico_Codigo", GxErrorMask.GX_NOMASK,prmBC000V11)
             ,new CursorDef("BC000V12", "DELETE FROM [Servico]  WHERE [Servico_Codigo] = @Servico_Codigo", GxErrorMask.GX_NOMASK,prmBC000V12)
             ,new CursorDef("BC000V13", "SELECT [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V13,1,0,true,false )
             ,new CursorDef("BC000V14", "SELECT [Servico_Descricao] AS Servico_VincDesc, [Servico_Sigla] AS Servico_VincSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Vinculado ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V14,1,0,true,false )
             ,new CursorDef("BC000V15", "SELECT [LinhaNegocio_Descricao] AS Servico_LinNegDsc FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @Servico_LinNegCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V15,1,0,true,false )
             ,new CursorDef("BC000V16", "SELECT TOP 1 [Servico_Codigo] AS Servico_Vinculado FROM [Servico] WITH (NOLOCK) WHERE [Servico_Vinculado] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V16,1,0,true,true )
             ,new CursorDef("BC000V17", "SELECT TOP 1 [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (NOLOCK) WHERE [ServicoFluxo_ServicoPos] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V17,1,0,true,true )
             ,new CursorDef("BC000V18", "SELECT TOP 1 [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (NOLOCK) WHERE [ServicoFluxo_ServicoCod] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V18,1,0,true,true )
             ,new CursorDef("BC000V19", "SELECT TOP 1 [ServicoPrioridade_Codigo] FROM [ServicoPrioridade] WITH (NOLOCK) WHERE [ServicoPrioridade_SrvCod] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V19,1,0,true,true )
             ,new CursorDef("BC000V20", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod], [ContratoServicosSistemas_SistemaCod] FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE [ContratoServicosSistemas_ServicoCod] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V20,1,0,true,true )
             ,new CursorDef("BC000V21", "SELECT TOP 1 [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_ServicoPadrao] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V21,1,0,true,true )
             ,new CursorDef("BC000V22", "SELECT TOP 1 [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_ServicoCod] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V22,1,0,true,true )
             ,new CursorDef("BC000V23", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_ServicoSS] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V23,1,0,true,true )
             ,new CursorDef("BC000V24", "SELECT TOP 1 [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V24,1,0,true,true )
             ,new CursorDef("BC000V25", "SELECT TOP 1 [ServicoAns_Codigo] FROM [ServicoAns] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V25,1,0,true,true )
             ,new CursorDef("BC000V26", "SELECT TOP 1 [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V26,1,0,true,true )
             ,new CursorDef("BC000V27", "SELECT TOP 1 [Servico_Codigo], [Check_Codigo] FROM [ServicoCheck] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V27,1,0,true,true )
             ,new CursorDef("BC000V28", "SELECT TOP 1 [Servico_Codigo], [ServicoArtefato_ArtefatoCod] FROM [ServicoArtefatos] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V28,1,0,true,true )
             ,new CursorDef("BC000V29", "SELECT TM1.[Servico_Codigo], TM1.[Servico_Nome], TM1.[Servico_Descricao], TM1.[Servico_Sigla], T2.[ServicoGrupo_Descricao], TM1.[Servico_UORespExclusiva], T3.[Servico_Descricao] AS Servico_VincDesc, T3.[Servico_Sigla] AS Servico_VincSigla, TM1.[Servico_Terceriza], TM1.[Servico_Tela], TM1.[Servico_Atende], TM1.[Servico_ObrigaValores], TM1.[Servico_ObjetoControle], TM1.[Servico_TipoHierarquia], TM1.[Servico_PercTmp], TM1.[Servico_PercPgm], TM1.[Servico_PercCnc], TM1.[Servico_Anterior], TM1.[Servico_Posterior], TM1.[Servico_Ativo], TM1.[Servico_IsPublico], T4.[LinhaNegocio_Descricao] AS Servico_LinNegDsc, TM1.[Servico_IsOrigemReferencia], TM1.[Servico_PausaSLA], TM1.[ServicoGrupo_Codigo], TM1.[Servico_UO] AS Servico_UO, TM1.[Servico_LinNegCod] AS Servico_LinNegCod, TM1.[Servico_Vinculado] AS Servico_Vinculado FROM ((([Servico] TM1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = TM1.[ServicoGrupo_Codigo]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = TM1.[Servico_Vinculado]) LEFT JOIN [LinhaNegocio] T4 WITH (NOLOCK) ON T4.[LinhadeNegocio_Codigo] = TM1.[Servico_LinNegCod]) WHERE TM1.[Servico_Codigo] = @Servico_Codigo ORDER BY TM1.[Servico_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000V29,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 3) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 3) ;
                ((short[]) buf[16])[0] = rslt.getShort(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((short[]) buf[18])[0] = rslt.getShort(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((short[]) buf[22])[0] = rslt.getShort(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((int[]) buf[24])[0] = rslt.getInt(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((int[]) buf[26])[0] = rslt.getInt(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((bool[]) buf[28])[0] = rslt.getBool(17) ;
                ((bool[]) buf[29])[0] = rslt.getBool(18) ;
                ((bool[]) buf[30])[0] = rslt.getBool(19) ;
                ((bool[]) buf[31])[0] = rslt.getBool(20) ;
                ((int[]) buf[32])[0] = rslt.getInt(21) ;
                ((int[]) buf[33])[0] = rslt.getInt(22) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((int[]) buf[37])[0] = rslt.getInt(24) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(24);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 3) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 3) ;
                ((short[]) buf[16])[0] = rslt.getShort(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((short[]) buf[18])[0] = rslt.getShort(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((short[]) buf[22])[0] = rslt.getShort(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((int[]) buf[24])[0] = rslt.getInt(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((int[]) buf[26])[0] = rslt.getInt(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((bool[]) buf[28])[0] = rslt.getBool(17) ;
                ((bool[]) buf[29])[0] = rslt.getBool(18) ;
                ((bool[]) buf[30])[0] = rslt.getBool(19) ;
                ((bool[]) buf[31])[0] = rslt.getBool(20) ;
                ((int[]) buf[32])[0] = rslt.getInt(21) ;
                ((int[]) buf[33])[0] = rslt.getInt(22) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((int[]) buf[37])[0] = rslt.getInt(24) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(24);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((bool[]) buf[12])[0] = rslt.getBool(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 3) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 3) ;
                ((short[]) buf[21])[0] = rslt.getShort(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((short[]) buf[23])[0] = rslt.getShort(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((short[]) buf[25])[0] = rslt.getShort(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((short[]) buf[27])[0] = rslt.getShort(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((int[]) buf[29])[0] = rslt.getInt(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((bool[]) buf[33])[0] = rslt.getBool(20) ;
                ((bool[]) buf[34])[0] = rslt.getBool(21) ;
                ((String[]) buf[35])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(22);
                ((bool[]) buf[37])[0] = rslt.getBool(23) ;
                ((bool[]) buf[38])[0] = rslt.getBool(24) ;
                ((int[]) buf[39])[0] = rslt.getInt(25) ;
                ((int[]) buf[40])[0] = rslt.getInt(26) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(26);
                ((int[]) buf[42])[0] = rslt.getInt(27) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(27);
                ((int[]) buf[44])[0] = rslt.getInt(28) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(28);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((bool[]) buf[12])[0] = rslt.getBool(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 3) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 3) ;
                ((short[]) buf[21])[0] = rslt.getShort(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((short[]) buf[23])[0] = rslt.getShort(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((short[]) buf[25])[0] = rslt.getShort(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((short[]) buf[27])[0] = rslt.getShort(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((int[]) buf[29])[0] = rslt.getInt(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((bool[]) buf[33])[0] = rslt.getBool(20) ;
                ((bool[]) buf[34])[0] = rslt.getBool(21) ;
                ((String[]) buf[35])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(22);
                ((bool[]) buf[37])[0] = rslt.getBool(23) ;
                ((bool[]) buf[38])[0] = rslt.getBool(24) ;
                ((int[]) buf[39])[0] = rslt.getInt(25) ;
                ((int[]) buf[40])[0] = rslt.getInt(26) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(26);
                ((int[]) buf[42])[0] = rslt.getInt(27) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(27);
                ((int[]) buf[44])[0] = rslt.getInt(28) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(28);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                stmt.SetParameter(9, (String)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[26]);
                }
                stmt.SetParameter(16, (bool)parms[27]);
                stmt.SetParameter(17, (bool)parms[28]);
                stmt.SetParameter(18, (bool)parms[29]);
                stmt.SetParameter(19, (bool)parms[30]);
                stmt.SetParameter(20, (int)parms[31]);
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 21 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(21, (int)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[37]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                stmt.SetParameter(9, (String)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[26]);
                }
                stmt.SetParameter(16, (bool)parms[27]);
                stmt.SetParameter(17, (bool)parms[28]);
                stmt.SetParameter(18, (bool)parms[29]);
                stmt.SetParameter(19, (bool)parms[30]);
                stmt.SetParameter(20, (int)parms[31]);
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 21 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(21, (int)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[37]);
                }
                stmt.SetParameter(24, (int)parms[38]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
