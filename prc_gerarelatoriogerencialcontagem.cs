/*
               File: PRC_GeraRelatorioGerencialContagem
        Description: Stub for PRC_GeraRelatorioGerencialContagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:45:22.68
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_gerarelatoriogerencialcontagem : GXProcedure
   {
      public prc_gerarelatoriogerencialcontagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public prc_gerarelatoriogerencialcontagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_AreaTrabalhoCod ,
                           DateTime aP1_ContagemResultado_DataDmnIn ,
                           DateTime aP2_ContagemResultado_DataDmnFim ,
                           int aP3_ContagemResultado_ContratadaOrigemCod ,
                           int aP4_ContagemResultado_CntSrvCod ,
                           String aP5_ContagemResultado_StatusDmn ,
                           out IGxCollection aP6_SDT_RelatorioGerencial )
      {
         this.AV2Contrato_AreaTrabalhoCod = aP0_Contrato_AreaTrabalhoCod;
         this.AV3ContagemResultado_DataDmnIn = aP1_ContagemResultado_DataDmnIn;
         this.AV4ContagemResultado_DataDmnFim = aP2_ContagemResultado_DataDmnFim;
         this.AV5ContagemResultado_ContratadaOrigemCod = aP3_ContagemResultado_ContratadaOrigemCod;
         this.AV6ContagemResultado_CntSrvCod = aP4_ContagemResultado_CntSrvCod;
         this.AV7ContagemResultado_StatusDmn = aP5_ContagemResultado_StatusDmn;
         this.AV8SDT_RelatorioGerencial = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem", "GxEv3Up14_MeetrikaVs3", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP6_SDT_RelatorioGerencial=this.AV8SDT_RelatorioGerencial;
      }

      public IGxCollection executeUdp( int aP0_Contrato_AreaTrabalhoCod ,
                                       DateTime aP1_ContagemResultado_DataDmnIn ,
                                       DateTime aP2_ContagemResultado_DataDmnFim ,
                                       int aP3_ContagemResultado_ContratadaOrigemCod ,
                                       int aP4_ContagemResultado_CntSrvCod ,
                                       String aP5_ContagemResultado_StatusDmn )
      {
         this.AV2Contrato_AreaTrabalhoCod = aP0_Contrato_AreaTrabalhoCod;
         this.AV3ContagemResultado_DataDmnIn = aP1_ContagemResultado_DataDmnIn;
         this.AV4ContagemResultado_DataDmnFim = aP2_ContagemResultado_DataDmnFim;
         this.AV5ContagemResultado_ContratadaOrigemCod = aP3_ContagemResultado_ContratadaOrigemCod;
         this.AV6ContagemResultado_CntSrvCod = aP4_ContagemResultado_CntSrvCod;
         this.AV7ContagemResultado_StatusDmn = aP5_ContagemResultado_StatusDmn;
         this.AV8SDT_RelatorioGerencial = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem", "GxEv3Up14_MeetrikaVs3", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP6_SDT_RelatorioGerencial=this.AV8SDT_RelatorioGerencial;
         return AV8SDT_RelatorioGerencial ;
      }

      public void executeSubmit( int aP0_Contrato_AreaTrabalhoCod ,
                                 DateTime aP1_ContagemResultado_DataDmnIn ,
                                 DateTime aP2_ContagemResultado_DataDmnFim ,
                                 int aP3_ContagemResultado_ContratadaOrigemCod ,
                                 int aP4_ContagemResultado_CntSrvCod ,
                                 String aP5_ContagemResultado_StatusDmn ,
                                 out IGxCollection aP6_SDT_RelatorioGerencial )
      {
         prc_gerarelatoriogerencialcontagem objprc_gerarelatoriogerencialcontagem;
         objprc_gerarelatoriogerencialcontagem = new prc_gerarelatoriogerencialcontagem();
         objprc_gerarelatoriogerencialcontagem.AV2Contrato_AreaTrabalhoCod = aP0_Contrato_AreaTrabalhoCod;
         objprc_gerarelatoriogerencialcontagem.AV3ContagemResultado_DataDmnIn = aP1_ContagemResultado_DataDmnIn;
         objprc_gerarelatoriogerencialcontagem.AV4ContagemResultado_DataDmnFim = aP2_ContagemResultado_DataDmnFim;
         objprc_gerarelatoriogerencialcontagem.AV5ContagemResultado_ContratadaOrigemCod = aP3_ContagemResultado_ContratadaOrigemCod;
         objprc_gerarelatoriogerencialcontagem.AV6ContagemResultado_CntSrvCod = aP4_ContagemResultado_CntSrvCod;
         objprc_gerarelatoriogerencialcontagem.AV7ContagemResultado_StatusDmn = aP5_ContagemResultado_StatusDmn;
         objprc_gerarelatoriogerencialcontagem.AV8SDT_RelatorioGerencial = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem", "GxEv3Up14_MeetrikaVs3", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem", "GeneXus.Programs") ;
         objprc_gerarelatoriogerencialcontagem.context.SetSubmitInitialConfig(context);
         objprc_gerarelatoriogerencialcontagem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_gerarelatoriogerencialcontagem);
         aP6_SDT_RelatorioGerencial=this.AV8SDT_RelatorioGerencial;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_gerarelatoriogerencialcontagem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2Contrato_AreaTrabalhoCod,(DateTime)AV3ContagemResultado_DataDmnIn,(DateTime)AV4ContagemResultado_DataDmnFim,(int)AV5ContagemResultado_ContratadaOrigemCod,(int)AV6ContagemResultado_CntSrvCod,(String)AV7ContagemResultado_StatusDmn,(IGxCollection)AV8SDT_RelatorioGerencial} ;
         ClassLoader.Execute("aprc_gerarelatoriogerencialcontagem","GeneXus.Programs.aprc_gerarelatoriogerencialcontagem", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 7 ) )
         {
            AV8SDT_RelatorioGerencial = (IGxCollection)(args[6]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV2Contrato_AreaTrabalhoCod ;
      private int AV5ContagemResultado_ContratadaOrigemCod ;
      private int AV6ContagemResultado_CntSrvCod ;
      private String AV7ContagemResultado_StatusDmn ;
      private DateTime AV3ContagemResultado_DataDmnIn ;
      private DateTime AV4ContagemResultado_DataDmnFim ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
      private IGxCollection aP6_SDT_RelatorioGerencial ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem ))]
      private IGxCollection AV8SDT_RelatorioGerencial ;
   }

}
