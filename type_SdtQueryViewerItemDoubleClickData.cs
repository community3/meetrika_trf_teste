/*
               File: type_SdtQueryViewerItemDoubleClickData
        Description: QueryViewerItemDoubleClickData
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:58.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "QueryViewerItemDoubleClickData" )]
   [XmlType(TypeName =  "QueryViewerItemDoubleClickData" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtQueryViewerItemDoubleClickData_Element ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtQueryViewerItemDoubleClickData_Filter ))]
   [Serializable]
   public class SdtQueryViewerItemDoubleClickData : GxUserType
   {
      public SdtQueryViewerItemDoubleClickData( )
      {
         /* Constructor for serialization */
         gxTv_SdtQueryViewerItemDoubleClickData_Name = "";
         gxTv_SdtQueryViewerItemDoubleClickData_Axis = "";
         gxTv_SdtQueryViewerItemDoubleClickData_Value = "";
      }

      public SdtQueryViewerItemDoubleClickData( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtQueryViewerItemDoubleClickData deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtQueryViewerItemDoubleClickData)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtQueryViewerItemDoubleClickData obj ;
         obj = this;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Axis = deserialized.gxTpr_Axis;
         obj.gxTpr_Value = deserialized.gxTpr_Value;
         obj.gxTpr_Context = deserialized.gxTpr_Context;
         obj.gxTpr_Filters = deserialized.gxTpr_Filters;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Name") )
               {
                  gxTv_SdtQueryViewerItemDoubleClickData_Name = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Axis") )
               {
                  gxTv_SdtQueryViewerItemDoubleClickData_Axis = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Value") )
               {
                  gxTv_SdtQueryViewerItemDoubleClickData_Value = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Context") )
               {
                  if ( gxTv_SdtQueryViewerItemDoubleClickData_Context == null )
                  {
                     gxTv_SdtQueryViewerItemDoubleClickData_Context = new GxObjectCollection( context, "QueryViewerItemDoubleClickData.Element", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerItemDoubleClickData_Element", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtQueryViewerItemDoubleClickData_Context.readxmlcollection(oReader, "Context", "Element");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Filters") )
               {
                  if ( gxTv_SdtQueryViewerItemDoubleClickData_Filters == null )
                  {
                     gxTv_SdtQueryViewerItemDoubleClickData_Filters = new GxObjectCollection( context, "QueryViewerItemDoubleClickData.Filter", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerItemDoubleClickData_Filter", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtQueryViewerItemDoubleClickData_Filters.readxmlcollection(oReader, "Filters", "Filter");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "QueryViewerItemDoubleClickData";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Name", StringUtil.RTrim( gxTv_SdtQueryViewerItemDoubleClickData_Name));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Axis", StringUtil.RTrim( gxTv_SdtQueryViewerItemDoubleClickData_Axis));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Value", StringUtil.RTrim( gxTv_SdtQueryViewerItemDoubleClickData_Value));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( gxTv_SdtQueryViewerItemDoubleClickData_Context != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtQueryViewerItemDoubleClickData_Context.writexmlcollection(oWriter, "Context", sNameSpace1, "Element", sNameSpace1);
         }
         if ( gxTv_SdtQueryViewerItemDoubleClickData_Filters != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtQueryViewerItemDoubleClickData_Filters.writexmlcollection(oWriter, "Filters", sNameSpace1, "Filter", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Name", gxTv_SdtQueryViewerItemDoubleClickData_Name, false);
         AddObjectProperty("Axis", gxTv_SdtQueryViewerItemDoubleClickData_Axis, false);
         AddObjectProperty("Value", gxTv_SdtQueryViewerItemDoubleClickData_Value, false);
         if ( gxTv_SdtQueryViewerItemDoubleClickData_Context != null )
         {
            AddObjectProperty("Context", gxTv_SdtQueryViewerItemDoubleClickData_Context, false);
         }
         if ( gxTv_SdtQueryViewerItemDoubleClickData_Filters != null )
         {
            AddObjectProperty("Filters", gxTv_SdtQueryViewerItemDoubleClickData_Filters, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Name" )]
      [  XmlElement( ElementName = "Name"   )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtQueryViewerItemDoubleClickData_Name ;
         }

         set {
            gxTv_SdtQueryViewerItemDoubleClickData_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Axis" )]
      [  XmlElement( ElementName = "Axis"   )]
      public String gxTpr_Axis
      {
         get {
            return gxTv_SdtQueryViewerItemDoubleClickData_Axis ;
         }

         set {
            gxTv_SdtQueryViewerItemDoubleClickData_Axis = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Value" )]
      [  XmlElement( ElementName = "Value"   )]
      public String gxTpr_Value
      {
         get {
            return gxTv_SdtQueryViewerItemDoubleClickData_Value ;
         }

         set {
            gxTv_SdtQueryViewerItemDoubleClickData_Value = (String)(value);
         }

      }

      public class gxTv_SdtQueryViewerItemDoubleClickData_Context_SdtQueryViewerItemDoubleClickData_Element_80compatibility:SdtQueryViewerItemDoubleClickData_Element {}
      [  SoapElement( ElementName = "Context" )]
      [  XmlArray( ElementName = "Context"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtQueryViewerItemDoubleClickData_Element ), ElementName= "Element"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtQueryViewerItemDoubleClickData_Context_SdtQueryViewerItemDoubleClickData_Element_80compatibility ), ElementName= "QueryViewerItemDoubleClickData.Element"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Context_GxObjectCollection
      {
         get {
            if ( gxTv_SdtQueryViewerItemDoubleClickData_Context == null )
            {
               gxTv_SdtQueryViewerItemDoubleClickData_Context = new GxObjectCollection( context, "QueryViewerItemDoubleClickData.Element", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerItemDoubleClickData_Element", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtQueryViewerItemDoubleClickData_Context ;
         }

         set {
            if ( gxTv_SdtQueryViewerItemDoubleClickData_Context == null )
            {
               gxTv_SdtQueryViewerItemDoubleClickData_Context = new GxObjectCollection( context, "QueryViewerItemDoubleClickData.Element", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerItemDoubleClickData_Element", "GeneXus.Programs");
            }
            gxTv_SdtQueryViewerItemDoubleClickData_Context = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Context
      {
         get {
            if ( gxTv_SdtQueryViewerItemDoubleClickData_Context == null )
            {
               gxTv_SdtQueryViewerItemDoubleClickData_Context = new GxObjectCollection( context, "QueryViewerItemDoubleClickData.Element", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerItemDoubleClickData_Element", "GeneXus.Programs");
            }
            return gxTv_SdtQueryViewerItemDoubleClickData_Context ;
         }

         set {
            gxTv_SdtQueryViewerItemDoubleClickData_Context = value;
         }

      }

      public void gxTv_SdtQueryViewerItemDoubleClickData_Context_SetNull( )
      {
         gxTv_SdtQueryViewerItemDoubleClickData_Context = null;
         return  ;
      }

      public bool gxTv_SdtQueryViewerItemDoubleClickData_Context_IsNull( )
      {
         if ( gxTv_SdtQueryViewerItemDoubleClickData_Context == null )
         {
            return true ;
         }
         return false ;
      }

      public class gxTv_SdtQueryViewerItemDoubleClickData_Filters_SdtQueryViewerItemDoubleClickData_Filter_80compatibility:SdtQueryViewerItemDoubleClickData_Filter {}
      [  SoapElement( ElementName = "Filters" )]
      [  XmlArray( ElementName = "Filters"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtQueryViewerItemDoubleClickData_Filter ), ElementName= "Filter"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtQueryViewerItemDoubleClickData_Filters_SdtQueryViewerItemDoubleClickData_Filter_80compatibility ), ElementName= "QueryViewerItemDoubleClickData.Filter"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Filters_GxObjectCollection
      {
         get {
            if ( gxTv_SdtQueryViewerItemDoubleClickData_Filters == null )
            {
               gxTv_SdtQueryViewerItemDoubleClickData_Filters = new GxObjectCollection( context, "QueryViewerItemDoubleClickData.Filter", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerItemDoubleClickData_Filter", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtQueryViewerItemDoubleClickData_Filters ;
         }

         set {
            if ( gxTv_SdtQueryViewerItemDoubleClickData_Filters == null )
            {
               gxTv_SdtQueryViewerItemDoubleClickData_Filters = new GxObjectCollection( context, "QueryViewerItemDoubleClickData.Filter", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerItemDoubleClickData_Filter", "GeneXus.Programs");
            }
            gxTv_SdtQueryViewerItemDoubleClickData_Filters = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Filters
      {
         get {
            if ( gxTv_SdtQueryViewerItemDoubleClickData_Filters == null )
            {
               gxTv_SdtQueryViewerItemDoubleClickData_Filters = new GxObjectCollection( context, "QueryViewerItemDoubleClickData.Filter", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerItemDoubleClickData_Filter", "GeneXus.Programs");
            }
            return gxTv_SdtQueryViewerItemDoubleClickData_Filters ;
         }

         set {
            gxTv_SdtQueryViewerItemDoubleClickData_Filters = value;
         }

      }

      public void gxTv_SdtQueryViewerItemDoubleClickData_Filters_SetNull( )
      {
         gxTv_SdtQueryViewerItemDoubleClickData_Filters = null;
         return  ;
      }

      public bool gxTv_SdtQueryViewerItemDoubleClickData_Filters_IsNull( )
      {
         if ( gxTv_SdtQueryViewerItemDoubleClickData_Filters == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtQueryViewerItemDoubleClickData_Name = "";
         gxTv_SdtQueryViewerItemDoubleClickData_Axis = "";
         gxTv_SdtQueryViewerItemDoubleClickData_Value = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtQueryViewerItemDoubleClickData_Name ;
      protected String gxTv_SdtQueryViewerItemDoubleClickData_Axis ;
      protected String gxTv_SdtQueryViewerItemDoubleClickData_Value ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( SdtQueryViewerItemDoubleClickData_Element ))]
      protected IGxCollection gxTv_SdtQueryViewerItemDoubleClickData_Context=null ;
      [ObjectCollection(ItemType=typeof( SdtQueryViewerItemDoubleClickData_Filter ))]
      protected IGxCollection gxTv_SdtQueryViewerItemDoubleClickData_Filters=null ;
   }

   [DataContract(Name = @"QueryViewerItemDoubleClickData", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtQueryViewerItemDoubleClickData_RESTInterface : GxGenericCollectionItem<SdtQueryViewerItemDoubleClickData>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtQueryViewerItemDoubleClickData_RESTInterface( ) : base()
      {
      }

      public SdtQueryViewerItemDoubleClickData_RESTInterface( SdtQueryViewerItemDoubleClickData psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Name" , Order = 0 )]
      public String gxTpr_Name
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Name) ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "Axis" , Order = 1 )]
      public String gxTpr_Axis
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Axis) ;
         }

         set {
            sdt.gxTpr_Axis = (String)(value);
         }

      }

      [DataMember( Name = "Value" , Order = 2 )]
      public String gxTpr_Value
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Value) ;
         }

         set {
            sdt.gxTpr_Value = (String)(value);
         }

      }

      [DataMember( Name = "Context" , Order = 3 )]
      public GxGenericCollection<SdtQueryViewerItemDoubleClickData_Element_RESTInterface> gxTpr_Context
      {
         get {
            return new GxGenericCollection<SdtQueryViewerItemDoubleClickData_Element_RESTInterface>(sdt.gxTpr_Context) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Context);
         }

      }

      [DataMember( Name = "Filters" , Order = 4 )]
      public GxGenericCollection<SdtQueryViewerItemDoubleClickData_Filter_RESTInterface> gxTpr_Filters
      {
         get {
            return new GxGenericCollection<SdtQueryViewerItemDoubleClickData_Filter_RESTInterface>(sdt.gxTpr_Filters) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Filters);
         }

      }

      public SdtQueryViewerItemDoubleClickData sdt
      {
         get {
            return (SdtQueryViewerItemDoubleClickData)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtQueryViewerItemDoubleClickData() ;
         }
      }

   }

}
