/*
               File: PRC_NumaRegra
        Description: Existe Numa Regra
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:31.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_numaregra : GXProcedure
   {
      public prc_numaregra( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_numaregra( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoSrvVnc_CntSrvCod ,
                           out bool aP1_Existe )
      {
         this.A915ContratoSrvVnc_CntSrvCod = aP0_ContratoSrvVnc_CntSrvCod;
         this.AV8Existe = false ;
         initialize();
         executePrivate();
         aP0_ContratoSrvVnc_CntSrvCod=this.A915ContratoSrvVnc_CntSrvCod;
         aP1_Existe=this.AV8Existe;
      }

      public bool executeUdp( ref int aP0_ContratoSrvVnc_CntSrvCod )
      {
         this.A915ContratoSrvVnc_CntSrvCod = aP0_ContratoSrvVnc_CntSrvCod;
         this.AV8Existe = false ;
         initialize();
         executePrivate();
         aP0_ContratoSrvVnc_CntSrvCod=this.A915ContratoSrvVnc_CntSrvCod;
         aP1_Existe=this.AV8Existe;
         return AV8Existe ;
      }

      public void executeSubmit( ref int aP0_ContratoSrvVnc_CntSrvCod ,
                                 out bool aP1_Existe )
      {
         prc_numaregra objprc_numaregra;
         objprc_numaregra = new prc_numaregra();
         objprc_numaregra.A915ContratoSrvVnc_CntSrvCod = aP0_ContratoSrvVnc_CntSrvCod;
         objprc_numaregra.AV8Existe = false ;
         objprc_numaregra.context.SetSubmitInitialConfig(context);
         objprc_numaregra.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_numaregra);
         aP0_ContratoSrvVnc_CntSrvCod=this.A915ContratoSrvVnc_CntSrvCod;
         aP1_Existe=this.AV8Existe;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_numaregra)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00DP2 */
         pr_default.execute(0, new Object[] {A915ContratoSrvVnc_CntSrvCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A917ContratoSrvVnc_Codigo = P00DP2_A917ContratoSrvVnc_Codigo[0];
            AV8Existe = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00DP2_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P00DP2_A917ContratoSrvVnc_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_numaregra__default(),
            new Object[][] {
                new Object[] {
               P00DP2_A915ContratoSrvVnc_CntSrvCod, P00DP2_A917ContratoSrvVnc_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A917ContratoSrvVnc_Codigo ;
      private String scmdbuf ;
      private bool AV8Existe ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoSrvVnc_CntSrvCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00DP2_A915ContratoSrvVnc_CntSrvCod ;
      private int[] P00DP2_A917ContratoSrvVnc_Codigo ;
      private bool aP1_Existe ;
   }

   public class prc_numaregra__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DP2 ;
          prmP00DP2 = new Object[] {
          new Object[] {"@ContratoSrvVnc_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DP2", "SELECT TOP 1 [ContratoSrvVnc_CntSrvCod], [ContratoSrvVnc_Codigo] FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE [ContratoSrvVnc_CntSrvCod] = @ContratoSrvVnc_CntSrvCod ORDER BY [ContratoSrvVnc_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DP2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
