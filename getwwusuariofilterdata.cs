/*
               File: GetWWUsuarioFilterData
        Description: Get WWUsuario Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:6:23.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwusuariofilterdata : GXProcedure
   {
      public getwwusuariofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwusuariofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV21DDOName = aP0_DDOName;
         this.AV19SearchTxt = aP1_SearchTxt;
         this.AV20SearchTxtTo = aP2_SearchTxtTo;
         this.AV25OptionsJson = "" ;
         this.AV28OptionsDescJson = "" ;
         this.AV30OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV25OptionsJson;
         aP4_OptionsDescJson=this.AV28OptionsDescJson;
         aP5_OptionIndexesJson=this.AV30OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV21DDOName = aP0_DDOName;
         this.AV19SearchTxt = aP1_SearchTxt;
         this.AV20SearchTxtTo = aP2_SearchTxtTo;
         this.AV25OptionsJson = "" ;
         this.AV28OptionsDescJson = "" ;
         this.AV30OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV25OptionsJson;
         aP4_OptionsDescJson=this.AV28OptionsDescJson;
         aP5_OptionIndexesJson=this.AV30OptionIndexesJson;
         return AV30OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwusuariofilterdata objgetwwusuariofilterdata;
         objgetwwusuariofilterdata = new getwwusuariofilterdata();
         objgetwwusuariofilterdata.AV21DDOName = aP0_DDOName;
         objgetwwusuariofilterdata.AV19SearchTxt = aP1_SearchTxt;
         objgetwwusuariofilterdata.AV20SearchTxtTo = aP2_SearchTxtTo;
         objgetwwusuariofilterdata.AV25OptionsJson = "" ;
         objgetwwusuariofilterdata.AV28OptionsDescJson = "" ;
         objgetwwusuariofilterdata.AV30OptionIndexesJson = "" ;
         objgetwwusuariofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwusuariofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwusuariofilterdata);
         aP3_OptionsJson=this.AV25OptionsJson;
         aP4_OptionsDescJson=this.AV28OptionsDescJson;
         aP5_OptionIndexesJson=this.AV30OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwusuariofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV24Options = (IGxCollection)(new GxSimpleCollection());
         AV27OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV29OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_USUARIO_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADUSUARIO_PESSOANOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_USUARIO_CARGONOM") == 0 )
         {
            /* Execute user subroutine: 'LOADUSUARIO_CARGONOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_USUARIO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADUSUARIO_NOMEOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV25OptionsJson = AV24Options.ToJSonString(false);
         AV28OptionsDescJson = AV27OptionsDesc.ToJSonString(false);
         AV30OptionIndexesJson = AV29OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV32Session.Get("WWUsuarioGridState"), "") == 0 )
         {
            AV34GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWUsuarioGridState"), "");
         }
         else
         {
            AV34GridState.FromXml(AV32Session.Get("WWUsuarioGridState"), "");
         }
         AV66GXV1 = 1;
         while ( AV66GXV1 <= AV34GridState.gxTpr_Filtervalues.Count )
         {
            AV35GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV34GridState.gxTpr_Filtervalues.Item(AV66GXV1));
            if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM") == 0 )
            {
               AV10TFUsuario_PessoaNom = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM_SEL") == 0 )
            {
               AV11TFUsuario_PessoaNom_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFUSUARIO_CARGONOM") == 0 )
            {
               AV12TFUsuario_CargoNom = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFUSUARIO_CARGONOM_SEL") == 0 )
            {
               AV13TFUsuario_CargoNom_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME") == 0 )
            {
               AV14TFUsuario_Nome = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME_SEL") == 0 )
            {
               AV15TFUsuario_Nome_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFUSUARIO_ATIVO_SEL") == 0 )
            {
               AV18TFUsuario_Ativo_Sel = (short)(NumberUtil.Val( AV35GridStateFilterValue.gxTpr_Value, "."));
            }
            AV66GXV1 = (int)(AV66GXV1+1);
         }
         if ( AV34GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV36GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV34GridState.gxTpr_Dynamicfilters.Item(1));
            AV37DynamicFiltersSelector1 = AV36GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "AREATRABALHO_CODIGO") == 0 )
            {
               AV39AreaTrabalho_Codigo1 = (int)(NumberUtil.Val( AV36GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 )
            {
               AV40Usuario_PessoaNom1 = AV36GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "USUARIO_CARGONOM") == 0 )
            {
               AV38DynamicFiltersOperator1 = AV36GridStateDynamicFilter.gxTpr_Operator;
               AV41Usuario_CargoNom1 = AV36GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "USUARIO_PESSOADOC") == 0 )
            {
               AV42Usuario_PessoaDoc1 = AV36GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "USUARIO_ENTIDADE") == 0 )
            {
               AV43Usuario_Entidade1 = AV36GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "USUARIO_ATIVO") == 0 )
            {
               AV44Usuario_Ativo1 = AV36GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV34GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV45DynamicFiltersEnabled2 = true;
               AV36GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV34GridState.gxTpr_Dynamicfilters.Item(2));
               AV46DynamicFiltersSelector2 = AV36GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "AREATRABALHO_CODIGO") == 0 )
               {
                  AV48AreaTrabalho_Codigo2 = (int)(NumberUtil.Val( AV36GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 )
               {
                  AV49Usuario_PessoaNom2 = AV36GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "USUARIO_CARGONOM") == 0 )
               {
                  AV47DynamicFiltersOperator2 = AV36GridStateDynamicFilter.gxTpr_Operator;
                  AV50Usuario_CargoNom2 = AV36GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "USUARIO_PESSOADOC") == 0 )
               {
                  AV51Usuario_PessoaDoc2 = AV36GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "USUARIO_ENTIDADE") == 0 )
               {
                  AV52Usuario_Entidade2 = AV36GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "USUARIO_ATIVO") == 0 )
               {
                  AV53Usuario_Ativo2 = AV36GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV34GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV54DynamicFiltersEnabled3 = true;
                  AV36GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV34GridState.gxTpr_Dynamicfilters.Item(3));
                  AV55DynamicFiltersSelector3 = AV36GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV55DynamicFiltersSelector3, "AREATRABALHO_CODIGO") == 0 )
                  {
                     AV57AreaTrabalho_Codigo3 = (int)(NumberUtil.Val( AV36GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 )
                  {
                     AV58Usuario_PessoaNom3 = AV36GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector3, "USUARIO_CARGONOM") == 0 )
                  {
                     AV56DynamicFiltersOperator3 = AV36GridStateDynamicFilter.gxTpr_Operator;
                     AV59Usuario_CargoNom3 = AV36GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector3, "USUARIO_PESSOADOC") == 0 )
                  {
                     AV60Usuario_PessoaDoc3 = AV36GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector3, "USUARIO_ENTIDADE") == 0 )
                  {
                     AV61Usuario_Entidade3 = AV36GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector3, "USUARIO_ATIVO") == 0 )
                  {
                     AV62Usuario_Ativo3 = AV36GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADUSUARIO_PESSOANOMOPTIONS' Routine */
         AV10TFUsuario_PessoaNom = AV19SearchTxt;
         AV11TFUsuario_PessoaNom_Sel = "";
         AV68WWUsuarioDS_1_Dynamicfiltersselector1 = AV37DynamicFiltersSelector1;
         AV69WWUsuarioDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV70WWUsuarioDS_3_Areatrabalho_codigo1 = AV39AreaTrabalho_Codigo1;
         AV71WWUsuarioDS_4_Usuario_pessoanom1 = AV40Usuario_PessoaNom1;
         AV72WWUsuarioDS_5_Usuario_cargonom1 = AV41Usuario_CargoNom1;
         AV73WWUsuarioDS_6_Usuario_pessoadoc1 = AV42Usuario_PessoaDoc1;
         AV74WWUsuarioDS_7_Usuario_entidade1 = AV43Usuario_Entidade1;
         AV75WWUsuarioDS_8_Usuario_ativo1 = AV44Usuario_Ativo1;
         AV76WWUsuarioDS_9_Dynamicfiltersenabled2 = AV45DynamicFiltersEnabled2;
         AV77WWUsuarioDS_10_Dynamicfiltersselector2 = AV46DynamicFiltersSelector2;
         AV78WWUsuarioDS_11_Dynamicfiltersoperator2 = AV47DynamicFiltersOperator2;
         AV79WWUsuarioDS_12_Areatrabalho_codigo2 = AV48AreaTrabalho_Codigo2;
         AV80WWUsuarioDS_13_Usuario_pessoanom2 = AV49Usuario_PessoaNom2;
         AV81WWUsuarioDS_14_Usuario_cargonom2 = AV50Usuario_CargoNom2;
         AV82WWUsuarioDS_15_Usuario_pessoadoc2 = AV51Usuario_PessoaDoc2;
         AV83WWUsuarioDS_16_Usuario_entidade2 = AV52Usuario_Entidade2;
         AV84WWUsuarioDS_17_Usuario_ativo2 = AV53Usuario_Ativo2;
         AV85WWUsuarioDS_18_Dynamicfiltersenabled3 = AV54DynamicFiltersEnabled3;
         AV86WWUsuarioDS_19_Dynamicfiltersselector3 = AV55DynamicFiltersSelector3;
         AV87WWUsuarioDS_20_Dynamicfiltersoperator3 = AV56DynamicFiltersOperator3;
         AV88WWUsuarioDS_21_Areatrabalho_codigo3 = AV57AreaTrabalho_Codigo3;
         AV89WWUsuarioDS_22_Usuario_pessoanom3 = AV58Usuario_PessoaNom3;
         AV90WWUsuarioDS_23_Usuario_cargonom3 = AV59Usuario_CargoNom3;
         AV91WWUsuarioDS_24_Usuario_pessoadoc3 = AV60Usuario_PessoaDoc3;
         AV92WWUsuarioDS_25_Usuario_entidade3 = AV61Usuario_Entidade3;
         AV93WWUsuarioDS_26_Usuario_ativo3 = AV62Usuario_Ativo3;
         AV94WWUsuarioDS_27_Tfusuario_pessoanom = AV10TFUsuario_PessoaNom;
         AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel = AV11TFUsuario_PessoaNom_Sel;
         AV96WWUsuarioDS_29_Tfusuario_cargonom = AV12TFUsuario_CargoNom;
         AV97WWUsuarioDS_30_Tfusuario_cargonom_sel = AV13TFUsuario_CargoNom_Sel;
         AV98WWUsuarioDS_31_Tfusuario_nome = AV14TFUsuario_Nome;
         AV99WWUsuarioDS_32_Tfusuario_nome_sel = AV15TFUsuario_Nome_Sel;
         AV100WWUsuarioDS_33_Tfusuario_ativo_sel = AV18TFUsuario_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1Usuario_Codigo ,
                                              AV63Codigos ,
                                              AV68WWUsuarioDS_1_Dynamicfiltersselector1 ,
                                              AV71WWUsuarioDS_4_Usuario_pessoanom1 ,
                                              AV69WWUsuarioDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWUsuarioDS_5_Usuario_cargonom1 ,
                                              AV73WWUsuarioDS_6_Usuario_pessoadoc1 ,
                                              AV75WWUsuarioDS_8_Usuario_ativo1 ,
                                              AV76WWUsuarioDS_9_Dynamicfiltersenabled2 ,
                                              AV77WWUsuarioDS_10_Dynamicfiltersselector2 ,
                                              AV80WWUsuarioDS_13_Usuario_pessoanom2 ,
                                              AV78WWUsuarioDS_11_Dynamicfiltersoperator2 ,
                                              AV81WWUsuarioDS_14_Usuario_cargonom2 ,
                                              AV82WWUsuarioDS_15_Usuario_pessoadoc2 ,
                                              AV84WWUsuarioDS_17_Usuario_ativo2 ,
                                              AV85WWUsuarioDS_18_Dynamicfiltersenabled3 ,
                                              AV86WWUsuarioDS_19_Dynamicfiltersselector3 ,
                                              AV89WWUsuarioDS_22_Usuario_pessoanom3 ,
                                              AV87WWUsuarioDS_20_Dynamicfiltersoperator3 ,
                                              AV90WWUsuarioDS_23_Usuario_cargonom3 ,
                                              AV91WWUsuarioDS_24_Usuario_pessoadoc3 ,
                                              AV93WWUsuarioDS_26_Usuario_ativo3 ,
                                              AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel ,
                                              AV94WWUsuarioDS_27_Tfusuario_pessoanom ,
                                              AV97WWUsuarioDS_30_Tfusuario_cargonom_sel ,
                                              AV96WWUsuarioDS_29_Tfusuario_cargonom ,
                                              AV99WWUsuarioDS_32_Tfusuario_nome_sel ,
                                              AV98WWUsuarioDS_31_Tfusuario_nome ,
                                              AV100WWUsuarioDS_33_Tfusuario_ativo_sel ,
                                              AV39AreaTrabalho_Codigo1 ,
                                              AV48AreaTrabalho_Codigo2 ,
                                              AV57AreaTrabalho_Codigo3 ,
                                              A58Usuario_PessoaNom ,
                                              A1074Usuario_CargoNom ,
                                              A325Usuario_PessoaDoc ,
                                              A54Usuario_Ativo ,
                                              A2Usuario_Nome ,
                                              AV74WWUsuarioDS_7_Usuario_entidade1 ,
                                              A1083Usuario_Entidade ,
                                              AV83WWUsuarioDS_16_Usuario_entidade2 ,
                                              AV92WWUsuarioDS_25_Usuario_entidade3 },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV71WWUsuarioDS_4_Usuario_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV71WWUsuarioDS_4_Usuario_pessoanom1), 100, "%");
         lV72WWUsuarioDS_5_Usuario_cargonom1 = StringUtil.Concat( StringUtil.RTrim( AV72WWUsuarioDS_5_Usuario_cargonom1), "%", "");
         lV72WWUsuarioDS_5_Usuario_cargonom1 = StringUtil.Concat( StringUtil.RTrim( AV72WWUsuarioDS_5_Usuario_cargonom1), "%", "");
         lV73WWUsuarioDS_6_Usuario_pessoadoc1 = StringUtil.Concat( StringUtil.RTrim( AV73WWUsuarioDS_6_Usuario_pessoadoc1), "%", "");
         lV80WWUsuarioDS_13_Usuario_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV80WWUsuarioDS_13_Usuario_pessoanom2), 100, "%");
         lV81WWUsuarioDS_14_Usuario_cargonom2 = StringUtil.Concat( StringUtil.RTrim( AV81WWUsuarioDS_14_Usuario_cargonom2), "%", "");
         lV81WWUsuarioDS_14_Usuario_cargonom2 = StringUtil.Concat( StringUtil.RTrim( AV81WWUsuarioDS_14_Usuario_cargonom2), "%", "");
         lV82WWUsuarioDS_15_Usuario_pessoadoc2 = StringUtil.Concat( StringUtil.RTrim( AV82WWUsuarioDS_15_Usuario_pessoadoc2), "%", "");
         lV89WWUsuarioDS_22_Usuario_pessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV89WWUsuarioDS_22_Usuario_pessoanom3), 100, "%");
         lV90WWUsuarioDS_23_Usuario_cargonom3 = StringUtil.Concat( StringUtil.RTrim( AV90WWUsuarioDS_23_Usuario_cargonom3), "%", "");
         lV90WWUsuarioDS_23_Usuario_cargonom3 = StringUtil.Concat( StringUtil.RTrim( AV90WWUsuarioDS_23_Usuario_cargonom3), "%", "");
         lV91WWUsuarioDS_24_Usuario_pessoadoc3 = StringUtil.Concat( StringUtil.RTrim( AV91WWUsuarioDS_24_Usuario_pessoadoc3), "%", "");
         lV94WWUsuarioDS_27_Tfusuario_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV94WWUsuarioDS_27_Tfusuario_pessoanom), 100, "%");
         lV96WWUsuarioDS_29_Tfusuario_cargonom = StringUtil.Concat( StringUtil.RTrim( AV96WWUsuarioDS_29_Tfusuario_cargonom), "%", "");
         lV98WWUsuarioDS_31_Tfusuario_nome = StringUtil.PadR( StringUtil.RTrim( AV98WWUsuarioDS_31_Tfusuario_nome), 50, "%");
         /* Using cursor P00VZ2 */
         pr_default.execute(0, new Object[] {A1Usuario_Codigo, lV71WWUsuarioDS_4_Usuario_pessoanom1, lV72WWUsuarioDS_5_Usuario_cargonom1, lV72WWUsuarioDS_5_Usuario_cargonom1, lV73WWUsuarioDS_6_Usuario_pessoadoc1, A1Usuario_Codigo, lV80WWUsuarioDS_13_Usuario_pessoanom2, lV81WWUsuarioDS_14_Usuario_cargonom2, lV81WWUsuarioDS_14_Usuario_cargonom2, lV82WWUsuarioDS_15_Usuario_pessoadoc2, A1Usuario_Codigo, lV89WWUsuarioDS_22_Usuario_pessoanom3, lV90WWUsuarioDS_23_Usuario_cargonom3, lV90WWUsuarioDS_23_Usuario_cargonom3, lV91WWUsuarioDS_24_Usuario_pessoadoc3, lV94WWUsuarioDS_27_Tfusuario_pessoanom, AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel, lV96WWUsuarioDS_29_Tfusuario_cargonom, AV97WWUsuarioDS_30_Tfusuario_cargonom_sel, lV98WWUsuarioDS_31_Tfusuario_nome, AV99WWUsuarioDS_32_Tfusuario_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKVZ2 = false;
            A1073Usuario_CargoCod = P00VZ2_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = P00VZ2_n1073Usuario_CargoCod[0];
            A57Usuario_PessoaCod = P00VZ2_A57Usuario_PessoaCod[0];
            A2Usuario_Nome = P00VZ2_A2Usuario_Nome[0];
            n2Usuario_Nome = P00VZ2_n2Usuario_Nome[0];
            A54Usuario_Ativo = P00VZ2_A54Usuario_Ativo[0];
            A325Usuario_PessoaDoc = P00VZ2_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = P00VZ2_n325Usuario_PessoaDoc[0];
            A1074Usuario_CargoNom = P00VZ2_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = P00VZ2_n1074Usuario_CargoNom[0];
            A58Usuario_PessoaNom = P00VZ2_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00VZ2_n58Usuario_PessoaNom[0];
            A1Usuario_Codigo = P00VZ2_A1Usuario_Codigo[0];
            A1074Usuario_CargoNom = P00VZ2_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = P00VZ2_n1074Usuario_CargoNom[0];
            A325Usuario_PessoaDoc = P00VZ2_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = P00VZ2_n325Usuario_PessoaDoc[0];
            A58Usuario_PessoaNom = P00VZ2_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00VZ2_n58Usuario_PessoaNom[0];
            GXt_char1 = A1083Usuario_Entidade;
            new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
            A1083Usuario_Entidade = GXt_char1;
            if ( ! ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWUsuarioDS_7_Usuario_entidade1)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV74WWUsuarioDS_7_Usuario_entidade1) , 255 , "%"),  ' ' ) ) )
            {
               if ( ! ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWUsuarioDS_16_Usuario_entidade2)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV83WWUsuarioDS_16_Usuario_entidade2) , 255 , "%"),  ' ' ) ) )
               {
                  if ( ! ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWUsuarioDS_25_Usuario_entidade3)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV92WWUsuarioDS_25_Usuario_entidade3) , 255 , "%"),  ' ' ) ) )
                  {
                     AV31count = 0;
                     while ( (pr_default.getStatus(0) != 101) && ( P00VZ2_A57Usuario_PessoaCod[0] == A57Usuario_PessoaCod ) )
                     {
                        BRKVZ2 = false;
                        A1Usuario_Codigo = P00VZ2_A1Usuario_Codigo[0];
                        AV31count = (long)(AV31count+1);
                        BRKVZ2 = true;
                        pr_default.readNext(0);
                     }
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A58Usuario_PessoaNom)) )
                     {
                        AV23Option = A58Usuario_PessoaNom;
                        AV22InsertIndex = 1;
                        while ( ( AV22InsertIndex <= AV24Options.Count ) && ( StringUtil.StrCmp(((String)AV24Options.Item(AV22InsertIndex)), AV23Option) < 0 ) )
                        {
                           AV22InsertIndex = (int)(AV22InsertIndex+1);
                        }
                        AV24Options.Add(AV23Option, AV22InsertIndex);
                        AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), AV22InsertIndex);
                     }
                     if ( AV24Options.Count == 50 )
                     {
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                     }
                  }
               }
            }
            if ( ! BRKVZ2 )
            {
               BRKVZ2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADUSUARIO_CARGONOMOPTIONS' Routine */
         AV12TFUsuario_CargoNom = AV19SearchTxt;
         AV13TFUsuario_CargoNom_Sel = "";
         AV68WWUsuarioDS_1_Dynamicfiltersselector1 = AV37DynamicFiltersSelector1;
         AV69WWUsuarioDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV70WWUsuarioDS_3_Areatrabalho_codigo1 = AV39AreaTrabalho_Codigo1;
         AV71WWUsuarioDS_4_Usuario_pessoanom1 = AV40Usuario_PessoaNom1;
         AV72WWUsuarioDS_5_Usuario_cargonom1 = AV41Usuario_CargoNom1;
         AV73WWUsuarioDS_6_Usuario_pessoadoc1 = AV42Usuario_PessoaDoc1;
         AV74WWUsuarioDS_7_Usuario_entidade1 = AV43Usuario_Entidade1;
         AV75WWUsuarioDS_8_Usuario_ativo1 = AV44Usuario_Ativo1;
         AV76WWUsuarioDS_9_Dynamicfiltersenabled2 = AV45DynamicFiltersEnabled2;
         AV77WWUsuarioDS_10_Dynamicfiltersselector2 = AV46DynamicFiltersSelector2;
         AV78WWUsuarioDS_11_Dynamicfiltersoperator2 = AV47DynamicFiltersOperator2;
         AV79WWUsuarioDS_12_Areatrabalho_codigo2 = AV48AreaTrabalho_Codigo2;
         AV80WWUsuarioDS_13_Usuario_pessoanom2 = AV49Usuario_PessoaNom2;
         AV81WWUsuarioDS_14_Usuario_cargonom2 = AV50Usuario_CargoNom2;
         AV82WWUsuarioDS_15_Usuario_pessoadoc2 = AV51Usuario_PessoaDoc2;
         AV83WWUsuarioDS_16_Usuario_entidade2 = AV52Usuario_Entidade2;
         AV84WWUsuarioDS_17_Usuario_ativo2 = AV53Usuario_Ativo2;
         AV85WWUsuarioDS_18_Dynamicfiltersenabled3 = AV54DynamicFiltersEnabled3;
         AV86WWUsuarioDS_19_Dynamicfiltersselector3 = AV55DynamicFiltersSelector3;
         AV87WWUsuarioDS_20_Dynamicfiltersoperator3 = AV56DynamicFiltersOperator3;
         AV88WWUsuarioDS_21_Areatrabalho_codigo3 = AV57AreaTrabalho_Codigo3;
         AV89WWUsuarioDS_22_Usuario_pessoanom3 = AV58Usuario_PessoaNom3;
         AV90WWUsuarioDS_23_Usuario_cargonom3 = AV59Usuario_CargoNom3;
         AV91WWUsuarioDS_24_Usuario_pessoadoc3 = AV60Usuario_PessoaDoc3;
         AV92WWUsuarioDS_25_Usuario_entidade3 = AV61Usuario_Entidade3;
         AV93WWUsuarioDS_26_Usuario_ativo3 = AV62Usuario_Ativo3;
         AV94WWUsuarioDS_27_Tfusuario_pessoanom = AV10TFUsuario_PessoaNom;
         AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel = AV11TFUsuario_PessoaNom_Sel;
         AV96WWUsuarioDS_29_Tfusuario_cargonom = AV12TFUsuario_CargoNom;
         AV97WWUsuarioDS_30_Tfusuario_cargonom_sel = AV13TFUsuario_CargoNom_Sel;
         AV98WWUsuarioDS_31_Tfusuario_nome = AV14TFUsuario_Nome;
         AV99WWUsuarioDS_32_Tfusuario_nome_sel = AV15TFUsuario_Nome_Sel;
         AV100WWUsuarioDS_33_Tfusuario_ativo_sel = AV18TFUsuario_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1Usuario_Codigo ,
                                              AV63Codigos ,
                                              AV68WWUsuarioDS_1_Dynamicfiltersselector1 ,
                                              AV71WWUsuarioDS_4_Usuario_pessoanom1 ,
                                              AV69WWUsuarioDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWUsuarioDS_5_Usuario_cargonom1 ,
                                              AV73WWUsuarioDS_6_Usuario_pessoadoc1 ,
                                              AV75WWUsuarioDS_8_Usuario_ativo1 ,
                                              AV76WWUsuarioDS_9_Dynamicfiltersenabled2 ,
                                              AV77WWUsuarioDS_10_Dynamicfiltersselector2 ,
                                              AV80WWUsuarioDS_13_Usuario_pessoanom2 ,
                                              AV78WWUsuarioDS_11_Dynamicfiltersoperator2 ,
                                              AV81WWUsuarioDS_14_Usuario_cargonom2 ,
                                              AV82WWUsuarioDS_15_Usuario_pessoadoc2 ,
                                              AV84WWUsuarioDS_17_Usuario_ativo2 ,
                                              AV85WWUsuarioDS_18_Dynamicfiltersenabled3 ,
                                              AV86WWUsuarioDS_19_Dynamicfiltersselector3 ,
                                              AV89WWUsuarioDS_22_Usuario_pessoanom3 ,
                                              AV87WWUsuarioDS_20_Dynamicfiltersoperator3 ,
                                              AV90WWUsuarioDS_23_Usuario_cargonom3 ,
                                              AV91WWUsuarioDS_24_Usuario_pessoadoc3 ,
                                              AV93WWUsuarioDS_26_Usuario_ativo3 ,
                                              AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel ,
                                              AV94WWUsuarioDS_27_Tfusuario_pessoanom ,
                                              AV97WWUsuarioDS_30_Tfusuario_cargonom_sel ,
                                              AV96WWUsuarioDS_29_Tfusuario_cargonom ,
                                              AV99WWUsuarioDS_32_Tfusuario_nome_sel ,
                                              AV98WWUsuarioDS_31_Tfusuario_nome ,
                                              AV100WWUsuarioDS_33_Tfusuario_ativo_sel ,
                                              AV39AreaTrabalho_Codigo1 ,
                                              AV48AreaTrabalho_Codigo2 ,
                                              AV57AreaTrabalho_Codigo3 ,
                                              A58Usuario_PessoaNom ,
                                              A1074Usuario_CargoNom ,
                                              A325Usuario_PessoaDoc ,
                                              A54Usuario_Ativo ,
                                              A2Usuario_Nome ,
                                              AV74WWUsuarioDS_7_Usuario_entidade1 ,
                                              A1083Usuario_Entidade ,
                                              AV83WWUsuarioDS_16_Usuario_entidade2 ,
                                              AV92WWUsuarioDS_25_Usuario_entidade3 },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV71WWUsuarioDS_4_Usuario_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV71WWUsuarioDS_4_Usuario_pessoanom1), 100, "%");
         lV72WWUsuarioDS_5_Usuario_cargonom1 = StringUtil.Concat( StringUtil.RTrim( AV72WWUsuarioDS_5_Usuario_cargonom1), "%", "");
         lV72WWUsuarioDS_5_Usuario_cargonom1 = StringUtil.Concat( StringUtil.RTrim( AV72WWUsuarioDS_5_Usuario_cargonom1), "%", "");
         lV73WWUsuarioDS_6_Usuario_pessoadoc1 = StringUtil.Concat( StringUtil.RTrim( AV73WWUsuarioDS_6_Usuario_pessoadoc1), "%", "");
         lV80WWUsuarioDS_13_Usuario_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV80WWUsuarioDS_13_Usuario_pessoanom2), 100, "%");
         lV81WWUsuarioDS_14_Usuario_cargonom2 = StringUtil.Concat( StringUtil.RTrim( AV81WWUsuarioDS_14_Usuario_cargonom2), "%", "");
         lV81WWUsuarioDS_14_Usuario_cargonom2 = StringUtil.Concat( StringUtil.RTrim( AV81WWUsuarioDS_14_Usuario_cargonom2), "%", "");
         lV82WWUsuarioDS_15_Usuario_pessoadoc2 = StringUtil.Concat( StringUtil.RTrim( AV82WWUsuarioDS_15_Usuario_pessoadoc2), "%", "");
         lV89WWUsuarioDS_22_Usuario_pessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV89WWUsuarioDS_22_Usuario_pessoanom3), 100, "%");
         lV90WWUsuarioDS_23_Usuario_cargonom3 = StringUtil.Concat( StringUtil.RTrim( AV90WWUsuarioDS_23_Usuario_cargonom3), "%", "");
         lV90WWUsuarioDS_23_Usuario_cargonom3 = StringUtil.Concat( StringUtil.RTrim( AV90WWUsuarioDS_23_Usuario_cargonom3), "%", "");
         lV91WWUsuarioDS_24_Usuario_pessoadoc3 = StringUtil.Concat( StringUtil.RTrim( AV91WWUsuarioDS_24_Usuario_pessoadoc3), "%", "");
         lV94WWUsuarioDS_27_Tfusuario_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV94WWUsuarioDS_27_Tfusuario_pessoanom), 100, "%");
         lV96WWUsuarioDS_29_Tfusuario_cargonom = StringUtil.Concat( StringUtil.RTrim( AV96WWUsuarioDS_29_Tfusuario_cargonom), "%", "");
         lV98WWUsuarioDS_31_Tfusuario_nome = StringUtil.PadR( StringUtil.RTrim( AV98WWUsuarioDS_31_Tfusuario_nome), 50, "%");
         /* Using cursor P00VZ3 */
         pr_default.execute(1, new Object[] {A1Usuario_Codigo, lV71WWUsuarioDS_4_Usuario_pessoanom1, lV72WWUsuarioDS_5_Usuario_cargonom1, lV72WWUsuarioDS_5_Usuario_cargonom1, lV73WWUsuarioDS_6_Usuario_pessoadoc1, A1Usuario_Codigo, lV80WWUsuarioDS_13_Usuario_pessoanom2, lV81WWUsuarioDS_14_Usuario_cargonom2, lV81WWUsuarioDS_14_Usuario_cargonom2, lV82WWUsuarioDS_15_Usuario_pessoadoc2, A1Usuario_Codigo, lV89WWUsuarioDS_22_Usuario_pessoanom3, lV90WWUsuarioDS_23_Usuario_cargonom3, lV90WWUsuarioDS_23_Usuario_cargonom3, lV91WWUsuarioDS_24_Usuario_pessoadoc3, lV94WWUsuarioDS_27_Tfusuario_pessoanom, AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel, lV96WWUsuarioDS_29_Tfusuario_cargonom, AV97WWUsuarioDS_30_Tfusuario_cargonom_sel, lV98WWUsuarioDS_31_Tfusuario_nome, AV99WWUsuarioDS_32_Tfusuario_nome_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKVZ4 = false;
            A57Usuario_PessoaCod = P00VZ3_A57Usuario_PessoaCod[0];
            A1073Usuario_CargoCod = P00VZ3_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = P00VZ3_n1073Usuario_CargoCod[0];
            A2Usuario_Nome = P00VZ3_A2Usuario_Nome[0];
            n2Usuario_Nome = P00VZ3_n2Usuario_Nome[0];
            A54Usuario_Ativo = P00VZ3_A54Usuario_Ativo[0];
            A325Usuario_PessoaDoc = P00VZ3_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = P00VZ3_n325Usuario_PessoaDoc[0];
            A1074Usuario_CargoNom = P00VZ3_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = P00VZ3_n1074Usuario_CargoNom[0];
            A58Usuario_PessoaNom = P00VZ3_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00VZ3_n58Usuario_PessoaNom[0];
            A1Usuario_Codigo = P00VZ3_A1Usuario_Codigo[0];
            A325Usuario_PessoaDoc = P00VZ3_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = P00VZ3_n325Usuario_PessoaDoc[0];
            A58Usuario_PessoaNom = P00VZ3_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00VZ3_n58Usuario_PessoaNom[0];
            A1074Usuario_CargoNom = P00VZ3_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = P00VZ3_n1074Usuario_CargoNom[0];
            GXt_char1 = A1083Usuario_Entidade;
            new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
            A1083Usuario_Entidade = GXt_char1;
            if ( ! ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWUsuarioDS_7_Usuario_entidade1)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV74WWUsuarioDS_7_Usuario_entidade1) , 255 , "%"),  ' ' ) ) )
            {
               if ( ! ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWUsuarioDS_16_Usuario_entidade2)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV83WWUsuarioDS_16_Usuario_entidade2) , 255 , "%"),  ' ' ) ) )
               {
                  if ( ! ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWUsuarioDS_25_Usuario_entidade3)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV92WWUsuarioDS_25_Usuario_entidade3) , 255 , "%"),  ' ' ) ) )
                  {
                     AV31count = 0;
                     while ( (pr_default.getStatus(1) != 101) && ( P00VZ3_A1073Usuario_CargoCod[0] == A1073Usuario_CargoCod ) )
                     {
                        BRKVZ4 = false;
                        A1Usuario_Codigo = P00VZ3_A1Usuario_Codigo[0];
                        AV31count = (long)(AV31count+1);
                        BRKVZ4 = true;
                        pr_default.readNext(1);
                     }
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1074Usuario_CargoNom)) )
                     {
                        AV23Option = A1074Usuario_CargoNom;
                        AV22InsertIndex = 1;
                        while ( ( AV22InsertIndex <= AV24Options.Count ) && ( StringUtil.StrCmp(((String)AV24Options.Item(AV22InsertIndex)), AV23Option) < 0 ) )
                        {
                           AV22InsertIndex = (int)(AV22InsertIndex+1);
                        }
                        AV24Options.Add(AV23Option, AV22InsertIndex);
                        AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), AV22InsertIndex);
                     }
                     if ( AV24Options.Count == 50 )
                     {
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                     }
                  }
               }
            }
            if ( ! BRKVZ4 )
            {
               BRKVZ4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADUSUARIO_NOMEOPTIONS' Routine */
         AV14TFUsuario_Nome = AV19SearchTxt;
         AV15TFUsuario_Nome_Sel = "";
         AV68WWUsuarioDS_1_Dynamicfiltersselector1 = AV37DynamicFiltersSelector1;
         AV69WWUsuarioDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV70WWUsuarioDS_3_Areatrabalho_codigo1 = AV39AreaTrabalho_Codigo1;
         AV71WWUsuarioDS_4_Usuario_pessoanom1 = AV40Usuario_PessoaNom1;
         AV72WWUsuarioDS_5_Usuario_cargonom1 = AV41Usuario_CargoNom1;
         AV73WWUsuarioDS_6_Usuario_pessoadoc1 = AV42Usuario_PessoaDoc1;
         AV74WWUsuarioDS_7_Usuario_entidade1 = AV43Usuario_Entidade1;
         AV75WWUsuarioDS_8_Usuario_ativo1 = AV44Usuario_Ativo1;
         AV76WWUsuarioDS_9_Dynamicfiltersenabled2 = AV45DynamicFiltersEnabled2;
         AV77WWUsuarioDS_10_Dynamicfiltersselector2 = AV46DynamicFiltersSelector2;
         AV78WWUsuarioDS_11_Dynamicfiltersoperator2 = AV47DynamicFiltersOperator2;
         AV79WWUsuarioDS_12_Areatrabalho_codigo2 = AV48AreaTrabalho_Codigo2;
         AV80WWUsuarioDS_13_Usuario_pessoanom2 = AV49Usuario_PessoaNom2;
         AV81WWUsuarioDS_14_Usuario_cargonom2 = AV50Usuario_CargoNom2;
         AV82WWUsuarioDS_15_Usuario_pessoadoc2 = AV51Usuario_PessoaDoc2;
         AV83WWUsuarioDS_16_Usuario_entidade2 = AV52Usuario_Entidade2;
         AV84WWUsuarioDS_17_Usuario_ativo2 = AV53Usuario_Ativo2;
         AV85WWUsuarioDS_18_Dynamicfiltersenabled3 = AV54DynamicFiltersEnabled3;
         AV86WWUsuarioDS_19_Dynamicfiltersselector3 = AV55DynamicFiltersSelector3;
         AV87WWUsuarioDS_20_Dynamicfiltersoperator3 = AV56DynamicFiltersOperator3;
         AV88WWUsuarioDS_21_Areatrabalho_codigo3 = AV57AreaTrabalho_Codigo3;
         AV89WWUsuarioDS_22_Usuario_pessoanom3 = AV58Usuario_PessoaNom3;
         AV90WWUsuarioDS_23_Usuario_cargonom3 = AV59Usuario_CargoNom3;
         AV91WWUsuarioDS_24_Usuario_pessoadoc3 = AV60Usuario_PessoaDoc3;
         AV92WWUsuarioDS_25_Usuario_entidade3 = AV61Usuario_Entidade3;
         AV93WWUsuarioDS_26_Usuario_ativo3 = AV62Usuario_Ativo3;
         AV94WWUsuarioDS_27_Tfusuario_pessoanom = AV10TFUsuario_PessoaNom;
         AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel = AV11TFUsuario_PessoaNom_Sel;
         AV96WWUsuarioDS_29_Tfusuario_cargonom = AV12TFUsuario_CargoNom;
         AV97WWUsuarioDS_30_Tfusuario_cargonom_sel = AV13TFUsuario_CargoNom_Sel;
         AV98WWUsuarioDS_31_Tfusuario_nome = AV14TFUsuario_Nome;
         AV99WWUsuarioDS_32_Tfusuario_nome_sel = AV15TFUsuario_Nome_Sel;
         AV100WWUsuarioDS_33_Tfusuario_ativo_sel = AV18TFUsuario_Ativo_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A1Usuario_Codigo ,
                                              AV63Codigos ,
                                              AV68WWUsuarioDS_1_Dynamicfiltersselector1 ,
                                              AV71WWUsuarioDS_4_Usuario_pessoanom1 ,
                                              AV69WWUsuarioDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWUsuarioDS_5_Usuario_cargonom1 ,
                                              AV73WWUsuarioDS_6_Usuario_pessoadoc1 ,
                                              AV75WWUsuarioDS_8_Usuario_ativo1 ,
                                              AV76WWUsuarioDS_9_Dynamicfiltersenabled2 ,
                                              AV77WWUsuarioDS_10_Dynamicfiltersselector2 ,
                                              AV80WWUsuarioDS_13_Usuario_pessoanom2 ,
                                              AV78WWUsuarioDS_11_Dynamicfiltersoperator2 ,
                                              AV81WWUsuarioDS_14_Usuario_cargonom2 ,
                                              AV82WWUsuarioDS_15_Usuario_pessoadoc2 ,
                                              AV84WWUsuarioDS_17_Usuario_ativo2 ,
                                              AV85WWUsuarioDS_18_Dynamicfiltersenabled3 ,
                                              AV86WWUsuarioDS_19_Dynamicfiltersselector3 ,
                                              AV89WWUsuarioDS_22_Usuario_pessoanom3 ,
                                              AV87WWUsuarioDS_20_Dynamicfiltersoperator3 ,
                                              AV90WWUsuarioDS_23_Usuario_cargonom3 ,
                                              AV91WWUsuarioDS_24_Usuario_pessoadoc3 ,
                                              AV93WWUsuarioDS_26_Usuario_ativo3 ,
                                              AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel ,
                                              AV94WWUsuarioDS_27_Tfusuario_pessoanom ,
                                              AV97WWUsuarioDS_30_Tfusuario_cargonom_sel ,
                                              AV96WWUsuarioDS_29_Tfusuario_cargonom ,
                                              AV99WWUsuarioDS_32_Tfusuario_nome_sel ,
                                              AV98WWUsuarioDS_31_Tfusuario_nome ,
                                              AV100WWUsuarioDS_33_Tfusuario_ativo_sel ,
                                              AV39AreaTrabalho_Codigo1 ,
                                              AV48AreaTrabalho_Codigo2 ,
                                              AV57AreaTrabalho_Codigo3 ,
                                              A58Usuario_PessoaNom ,
                                              A1074Usuario_CargoNom ,
                                              A325Usuario_PessoaDoc ,
                                              A54Usuario_Ativo ,
                                              A2Usuario_Nome ,
                                              AV74WWUsuarioDS_7_Usuario_entidade1 ,
                                              A1083Usuario_Entidade ,
                                              AV83WWUsuarioDS_16_Usuario_entidade2 ,
                                              AV92WWUsuarioDS_25_Usuario_entidade3 },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV71WWUsuarioDS_4_Usuario_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV71WWUsuarioDS_4_Usuario_pessoanom1), 100, "%");
         lV72WWUsuarioDS_5_Usuario_cargonom1 = StringUtil.Concat( StringUtil.RTrim( AV72WWUsuarioDS_5_Usuario_cargonom1), "%", "");
         lV72WWUsuarioDS_5_Usuario_cargonom1 = StringUtil.Concat( StringUtil.RTrim( AV72WWUsuarioDS_5_Usuario_cargonom1), "%", "");
         lV73WWUsuarioDS_6_Usuario_pessoadoc1 = StringUtil.Concat( StringUtil.RTrim( AV73WWUsuarioDS_6_Usuario_pessoadoc1), "%", "");
         lV80WWUsuarioDS_13_Usuario_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV80WWUsuarioDS_13_Usuario_pessoanom2), 100, "%");
         lV81WWUsuarioDS_14_Usuario_cargonom2 = StringUtil.Concat( StringUtil.RTrim( AV81WWUsuarioDS_14_Usuario_cargonom2), "%", "");
         lV81WWUsuarioDS_14_Usuario_cargonom2 = StringUtil.Concat( StringUtil.RTrim( AV81WWUsuarioDS_14_Usuario_cargonom2), "%", "");
         lV82WWUsuarioDS_15_Usuario_pessoadoc2 = StringUtil.Concat( StringUtil.RTrim( AV82WWUsuarioDS_15_Usuario_pessoadoc2), "%", "");
         lV89WWUsuarioDS_22_Usuario_pessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV89WWUsuarioDS_22_Usuario_pessoanom3), 100, "%");
         lV90WWUsuarioDS_23_Usuario_cargonom3 = StringUtil.Concat( StringUtil.RTrim( AV90WWUsuarioDS_23_Usuario_cargonom3), "%", "");
         lV90WWUsuarioDS_23_Usuario_cargonom3 = StringUtil.Concat( StringUtil.RTrim( AV90WWUsuarioDS_23_Usuario_cargonom3), "%", "");
         lV91WWUsuarioDS_24_Usuario_pessoadoc3 = StringUtil.Concat( StringUtil.RTrim( AV91WWUsuarioDS_24_Usuario_pessoadoc3), "%", "");
         lV94WWUsuarioDS_27_Tfusuario_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV94WWUsuarioDS_27_Tfusuario_pessoanom), 100, "%");
         lV96WWUsuarioDS_29_Tfusuario_cargonom = StringUtil.Concat( StringUtil.RTrim( AV96WWUsuarioDS_29_Tfusuario_cargonom), "%", "");
         lV98WWUsuarioDS_31_Tfusuario_nome = StringUtil.PadR( StringUtil.RTrim( AV98WWUsuarioDS_31_Tfusuario_nome), 50, "%");
         /* Using cursor P00VZ4 */
         pr_default.execute(2, new Object[] {A1Usuario_Codigo, lV71WWUsuarioDS_4_Usuario_pessoanom1, lV72WWUsuarioDS_5_Usuario_cargonom1, lV72WWUsuarioDS_5_Usuario_cargonom1, lV73WWUsuarioDS_6_Usuario_pessoadoc1, A1Usuario_Codigo, lV80WWUsuarioDS_13_Usuario_pessoanom2, lV81WWUsuarioDS_14_Usuario_cargonom2, lV81WWUsuarioDS_14_Usuario_cargonom2, lV82WWUsuarioDS_15_Usuario_pessoadoc2, A1Usuario_Codigo, lV89WWUsuarioDS_22_Usuario_pessoanom3, lV90WWUsuarioDS_23_Usuario_cargonom3, lV90WWUsuarioDS_23_Usuario_cargonom3, lV91WWUsuarioDS_24_Usuario_pessoadoc3, lV94WWUsuarioDS_27_Tfusuario_pessoanom, AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel, lV96WWUsuarioDS_29_Tfusuario_cargonom, AV97WWUsuarioDS_30_Tfusuario_cargonom_sel, lV98WWUsuarioDS_31_Tfusuario_nome, AV99WWUsuarioDS_32_Tfusuario_nome_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKVZ6 = false;
            A57Usuario_PessoaCod = P00VZ4_A57Usuario_PessoaCod[0];
            A1073Usuario_CargoCod = P00VZ4_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = P00VZ4_n1073Usuario_CargoCod[0];
            A2Usuario_Nome = P00VZ4_A2Usuario_Nome[0];
            n2Usuario_Nome = P00VZ4_n2Usuario_Nome[0];
            A54Usuario_Ativo = P00VZ4_A54Usuario_Ativo[0];
            A325Usuario_PessoaDoc = P00VZ4_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = P00VZ4_n325Usuario_PessoaDoc[0];
            A1074Usuario_CargoNom = P00VZ4_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = P00VZ4_n1074Usuario_CargoNom[0];
            A58Usuario_PessoaNom = P00VZ4_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00VZ4_n58Usuario_PessoaNom[0];
            A1Usuario_Codigo = P00VZ4_A1Usuario_Codigo[0];
            A325Usuario_PessoaDoc = P00VZ4_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = P00VZ4_n325Usuario_PessoaDoc[0];
            A58Usuario_PessoaNom = P00VZ4_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00VZ4_n58Usuario_PessoaNom[0];
            A1074Usuario_CargoNom = P00VZ4_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = P00VZ4_n1074Usuario_CargoNom[0];
            GXt_char1 = A1083Usuario_Entidade;
            new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
            A1083Usuario_Entidade = GXt_char1;
            if ( ! ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWUsuarioDS_7_Usuario_entidade1)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV74WWUsuarioDS_7_Usuario_entidade1) , 255 , "%"),  ' ' ) ) )
            {
               if ( ! ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWUsuarioDS_16_Usuario_entidade2)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV83WWUsuarioDS_16_Usuario_entidade2) , 255 , "%"),  ' ' ) ) )
               {
                  if ( ! ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWUsuarioDS_25_Usuario_entidade3)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV92WWUsuarioDS_25_Usuario_entidade3) , 255 , "%"),  ' ' ) ) )
                  {
                     AV31count = 0;
                     while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00VZ4_A2Usuario_Nome[0], A2Usuario_Nome) == 0 ) )
                     {
                        BRKVZ6 = false;
                        A1Usuario_Codigo = P00VZ4_A1Usuario_Codigo[0];
                        AV31count = (long)(AV31count+1);
                        BRKVZ6 = true;
                        pr_default.readNext(2);
                     }
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2Usuario_Nome)) )
                     {
                        AV23Option = A2Usuario_Nome;
                        AV24Options.Add(AV23Option, 0);
                        AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                     }
                     if ( AV24Options.Count == 50 )
                     {
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                     }
                  }
               }
            }
            if ( ! BRKVZ6 )
            {
               BRKVZ6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV24Options = new GxSimpleCollection();
         AV27OptionsDesc = new GxSimpleCollection();
         AV29OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV32Session = context.GetSession();
         AV34GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV35GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFUsuario_PessoaNom = "";
         AV11TFUsuario_PessoaNom_Sel = "";
         AV12TFUsuario_CargoNom = "";
         AV13TFUsuario_CargoNom_Sel = "";
         AV14TFUsuario_Nome = "";
         AV15TFUsuario_Nome_Sel = "";
         AV36GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV37DynamicFiltersSelector1 = "";
         AV40Usuario_PessoaNom1 = "";
         AV41Usuario_CargoNom1 = "";
         AV42Usuario_PessoaDoc1 = "";
         AV43Usuario_Entidade1 = "";
         AV44Usuario_Ativo1 = "";
         AV46DynamicFiltersSelector2 = "";
         AV49Usuario_PessoaNom2 = "";
         AV50Usuario_CargoNom2 = "";
         AV51Usuario_PessoaDoc2 = "";
         AV52Usuario_Entidade2 = "";
         AV53Usuario_Ativo2 = "";
         AV55DynamicFiltersSelector3 = "";
         AV58Usuario_PessoaNom3 = "";
         AV59Usuario_CargoNom3 = "";
         AV60Usuario_PessoaDoc3 = "";
         AV61Usuario_Entidade3 = "";
         AV62Usuario_Ativo3 = "";
         AV68WWUsuarioDS_1_Dynamicfiltersselector1 = "";
         AV71WWUsuarioDS_4_Usuario_pessoanom1 = "";
         AV72WWUsuarioDS_5_Usuario_cargonom1 = "";
         AV73WWUsuarioDS_6_Usuario_pessoadoc1 = "";
         AV74WWUsuarioDS_7_Usuario_entidade1 = "";
         AV75WWUsuarioDS_8_Usuario_ativo1 = "";
         AV77WWUsuarioDS_10_Dynamicfiltersselector2 = "";
         AV80WWUsuarioDS_13_Usuario_pessoanom2 = "";
         AV81WWUsuarioDS_14_Usuario_cargonom2 = "";
         AV82WWUsuarioDS_15_Usuario_pessoadoc2 = "";
         AV83WWUsuarioDS_16_Usuario_entidade2 = "";
         AV84WWUsuarioDS_17_Usuario_ativo2 = "";
         AV86WWUsuarioDS_19_Dynamicfiltersselector3 = "";
         AV89WWUsuarioDS_22_Usuario_pessoanom3 = "";
         AV90WWUsuarioDS_23_Usuario_cargonom3 = "";
         AV91WWUsuarioDS_24_Usuario_pessoadoc3 = "";
         AV92WWUsuarioDS_25_Usuario_entidade3 = "";
         AV93WWUsuarioDS_26_Usuario_ativo3 = "";
         AV94WWUsuarioDS_27_Tfusuario_pessoanom = "";
         AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel = "";
         AV96WWUsuarioDS_29_Tfusuario_cargonom = "";
         AV97WWUsuarioDS_30_Tfusuario_cargonom_sel = "";
         AV98WWUsuarioDS_31_Tfusuario_nome = "";
         AV99WWUsuarioDS_32_Tfusuario_nome_sel = "";
         scmdbuf = "";
         lV71WWUsuarioDS_4_Usuario_pessoanom1 = "";
         lV72WWUsuarioDS_5_Usuario_cargonom1 = "";
         lV73WWUsuarioDS_6_Usuario_pessoadoc1 = "";
         lV80WWUsuarioDS_13_Usuario_pessoanom2 = "";
         lV81WWUsuarioDS_14_Usuario_cargonom2 = "";
         lV82WWUsuarioDS_15_Usuario_pessoadoc2 = "";
         lV89WWUsuarioDS_22_Usuario_pessoanom3 = "";
         lV90WWUsuarioDS_23_Usuario_cargonom3 = "";
         lV91WWUsuarioDS_24_Usuario_pessoadoc3 = "";
         lV94WWUsuarioDS_27_Tfusuario_pessoanom = "";
         lV96WWUsuarioDS_29_Tfusuario_cargonom = "";
         lV98WWUsuarioDS_31_Tfusuario_nome = "";
         AV63Codigos = new GxSimpleCollection();
         A58Usuario_PessoaNom = "";
         A1074Usuario_CargoNom = "";
         A325Usuario_PessoaDoc = "";
         A2Usuario_Nome = "";
         A1083Usuario_Entidade = "";
         P00VZ2_A1073Usuario_CargoCod = new int[1] ;
         P00VZ2_n1073Usuario_CargoCod = new bool[] {false} ;
         P00VZ2_A57Usuario_PessoaCod = new int[1] ;
         P00VZ2_A2Usuario_Nome = new String[] {""} ;
         P00VZ2_n2Usuario_Nome = new bool[] {false} ;
         P00VZ2_A54Usuario_Ativo = new bool[] {false} ;
         P00VZ2_A325Usuario_PessoaDoc = new String[] {""} ;
         P00VZ2_n325Usuario_PessoaDoc = new bool[] {false} ;
         P00VZ2_A1074Usuario_CargoNom = new String[] {""} ;
         P00VZ2_n1074Usuario_CargoNom = new bool[] {false} ;
         P00VZ2_A58Usuario_PessoaNom = new String[] {""} ;
         P00VZ2_n58Usuario_PessoaNom = new bool[] {false} ;
         P00VZ2_A1Usuario_Codigo = new int[1] ;
         AV23Option = "";
         P00VZ3_A57Usuario_PessoaCod = new int[1] ;
         P00VZ3_A1073Usuario_CargoCod = new int[1] ;
         P00VZ3_n1073Usuario_CargoCod = new bool[] {false} ;
         P00VZ3_A2Usuario_Nome = new String[] {""} ;
         P00VZ3_n2Usuario_Nome = new bool[] {false} ;
         P00VZ3_A54Usuario_Ativo = new bool[] {false} ;
         P00VZ3_A325Usuario_PessoaDoc = new String[] {""} ;
         P00VZ3_n325Usuario_PessoaDoc = new bool[] {false} ;
         P00VZ3_A1074Usuario_CargoNom = new String[] {""} ;
         P00VZ3_n1074Usuario_CargoNom = new bool[] {false} ;
         P00VZ3_A58Usuario_PessoaNom = new String[] {""} ;
         P00VZ3_n58Usuario_PessoaNom = new bool[] {false} ;
         P00VZ3_A1Usuario_Codigo = new int[1] ;
         P00VZ4_A57Usuario_PessoaCod = new int[1] ;
         P00VZ4_A1073Usuario_CargoCod = new int[1] ;
         P00VZ4_n1073Usuario_CargoCod = new bool[] {false} ;
         P00VZ4_A2Usuario_Nome = new String[] {""} ;
         P00VZ4_n2Usuario_Nome = new bool[] {false} ;
         P00VZ4_A54Usuario_Ativo = new bool[] {false} ;
         P00VZ4_A325Usuario_PessoaDoc = new String[] {""} ;
         P00VZ4_n325Usuario_PessoaDoc = new bool[] {false} ;
         P00VZ4_A1074Usuario_CargoNom = new String[] {""} ;
         P00VZ4_n1074Usuario_CargoNom = new bool[] {false} ;
         P00VZ4_A58Usuario_PessoaNom = new String[] {""} ;
         P00VZ4_n58Usuario_PessoaNom = new bool[] {false} ;
         P00VZ4_A1Usuario_Codigo = new int[1] ;
         GXt_char1 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwusuariofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00VZ2_A1073Usuario_CargoCod, P00VZ2_n1073Usuario_CargoCod, P00VZ2_A57Usuario_PessoaCod, P00VZ2_A2Usuario_Nome, P00VZ2_n2Usuario_Nome, P00VZ2_A54Usuario_Ativo, P00VZ2_A325Usuario_PessoaDoc, P00VZ2_n325Usuario_PessoaDoc, P00VZ2_A1074Usuario_CargoNom, P00VZ2_n1074Usuario_CargoNom,
               P00VZ2_A58Usuario_PessoaNom, P00VZ2_n58Usuario_PessoaNom, P00VZ2_A1Usuario_Codigo
               }
               , new Object[] {
               P00VZ3_A57Usuario_PessoaCod, P00VZ3_A1073Usuario_CargoCod, P00VZ3_n1073Usuario_CargoCod, P00VZ3_A2Usuario_Nome, P00VZ3_n2Usuario_Nome, P00VZ3_A54Usuario_Ativo, P00VZ3_A325Usuario_PessoaDoc, P00VZ3_n325Usuario_PessoaDoc, P00VZ3_A1074Usuario_CargoNom, P00VZ3_n1074Usuario_CargoNom,
               P00VZ3_A58Usuario_PessoaNom, P00VZ3_n58Usuario_PessoaNom, P00VZ3_A1Usuario_Codigo
               }
               , new Object[] {
               P00VZ4_A57Usuario_PessoaCod, P00VZ4_A1073Usuario_CargoCod, P00VZ4_n1073Usuario_CargoCod, P00VZ4_A2Usuario_Nome, P00VZ4_n2Usuario_Nome, P00VZ4_A54Usuario_Ativo, P00VZ4_A325Usuario_PessoaDoc, P00VZ4_n325Usuario_PessoaDoc, P00VZ4_A1074Usuario_CargoNom, P00VZ4_n1074Usuario_CargoNom,
               P00VZ4_A58Usuario_PessoaNom, P00VZ4_n58Usuario_PessoaNom, P00VZ4_A1Usuario_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV18TFUsuario_Ativo_Sel ;
      private short AV38DynamicFiltersOperator1 ;
      private short AV47DynamicFiltersOperator2 ;
      private short AV56DynamicFiltersOperator3 ;
      private short AV69WWUsuarioDS_2_Dynamicfiltersoperator1 ;
      private short AV78WWUsuarioDS_11_Dynamicfiltersoperator2 ;
      private short AV87WWUsuarioDS_20_Dynamicfiltersoperator3 ;
      private short AV100WWUsuarioDS_33_Tfusuario_ativo_sel ;
      private int AV66GXV1 ;
      private int AV39AreaTrabalho_Codigo1 ;
      private int AV48AreaTrabalho_Codigo2 ;
      private int AV57AreaTrabalho_Codigo3 ;
      private int AV70WWUsuarioDS_3_Areatrabalho_codigo1 ;
      private int AV79WWUsuarioDS_12_Areatrabalho_codigo2 ;
      private int AV88WWUsuarioDS_21_Areatrabalho_codigo3 ;
      private int A1Usuario_Codigo ;
      private int A1073Usuario_CargoCod ;
      private int A57Usuario_PessoaCod ;
      private int AV22InsertIndex ;
      private long AV31count ;
      private String AV10TFUsuario_PessoaNom ;
      private String AV11TFUsuario_PessoaNom_Sel ;
      private String AV14TFUsuario_Nome ;
      private String AV15TFUsuario_Nome_Sel ;
      private String AV40Usuario_PessoaNom1 ;
      private String AV43Usuario_Entidade1 ;
      private String AV44Usuario_Ativo1 ;
      private String AV49Usuario_PessoaNom2 ;
      private String AV52Usuario_Entidade2 ;
      private String AV53Usuario_Ativo2 ;
      private String AV58Usuario_PessoaNom3 ;
      private String AV61Usuario_Entidade3 ;
      private String AV62Usuario_Ativo3 ;
      private String AV71WWUsuarioDS_4_Usuario_pessoanom1 ;
      private String AV74WWUsuarioDS_7_Usuario_entidade1 ;
      private String AV75WWUsuarioDS_8_Usuario_ativo1 ;
      private String AV80WWUsuarioDS_13_Usuario_pessoanom2 ;
      private String AV83WWUsuarioDS_16_Usuario_entidade2 ;
      private String AV84WWUsuarioDS_17_Usuario_ativo2 ;
      private String AV89WWUsuarioDS_22_Usuario_pessoanom3 ;
      private String AV92WWUsuarioDS_25_Usuario_entidade3 ;
      private String AV93WWUsuarioDS_26_Usuario_ativo3 ;
      private String AV94WWUsuarioDS_27_Tfusuario_pessoanom ;
      private String AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel ;
      private String AV98WWUsuarioDS_31_Tfusuario_nome ;
      private String AV99WWUsuarioDS_32_Tfusuario_nome_sel ;
      private String scmdbuf ;
      private String lV71WWUsuarioDS_4_Usuario_pessoanom1 ;
      private String lV80WWUsuarioDS_13_Usuario_pessoanom2 ;
      private String lV89WWUsuarioDS_22_Usuario_pessoanom3 ;
      private String lV94WWUsuarioDS_27_Tfusuario_pessoanom ;
      private String lV98WWUsuarioDS_31_Tfusuario_nome ;
      private String A58Usuario_PessoaNom ;
      private String A2Usuario_Nome ;
      private String A1083Usuario_Entidade ;
      private String GXt_char1 ;
      private bool returnInSub ;
      private bool AV45DynamicFiltersEnabled2 ;
      private bool AV54DynamicFiltersEnabled3 ;
      private bool AV76WWUsuarioDS_9_Dynamicfiltersenabled2 ;
      private bool AV85WWUsuarioDS_18_Dynamicfiltersenabled3 ;
      private bool A54Usuario_Ativo ;
      private bool BRKVZ2 ;
      private bool n1073Usuario_CargoCod ;
      private bool n2Usuario_Nome ;
      private bool n325Usuario_PessoaDoc ;
      private bool n1074Usuario_CargoNom ;
      private bool n58Usuario_PessoaNom ;
      private bool BRKVZ4 ;
      private bool BRKVZ6 ;
      private String AV30OptionIndexesJson ;
      private String AV25OptionsJson ;
      private String AV28OptionsDescJson ;
      private String AV21DDOName ;
      private String AV19SearchTxt ;
      private String AV20SearchTxtTo ;
      private String AV12TFUsuario_CargoNom ;
      private String AV13TFUsuario_CargoNom_Sel ;
      private String AV37DynamicFiltersSelector1 ;
      private String AV41Usuario_CargoNom1 ;
      private String AV42Usuario_PessoaDoc1 ;
      private String AV46DynamicFiltersSelector2 ;
      private String AV50Usuario_CargoNom2 ;
      private String AV51Usuario_PessoaDoc2 ;
      private String AV55DynamicFiltersSelector3 ;
      private String AV59Usuario_CargoNom3 ;
      private String AV60Usuario_PessoaDoc3 ;
      private String AV68WWUsuarioDS_1_Dynamicfiltersselector1 ;
      private String AV72WWUsuarioDS_5_Usuario_cargonom1 ;
      private String AV73WWUsuarioDS_6_Usuario_pessoadoc1 ;
      private String AV77WWUsuarioDS_10_Dynamicfiltersselector2 ;
      private String AV81WWUsuarioDS_14_Usuario_cargonom2 ;
      private String AV82WWUsuarioDS_15_Usuario_pessoadoc2 ;
      private String AV86WWUsuarioDS_19_Dynamicfiltersselector3 ;
      private String AV90WWUsuarioDS_23_Usuario_cargonom3 ;
      private String AV91WWUsuarioDS_24_Usuario_pessoadoc3 ;
      private String AV96WWUsuarioDS_29_Tfusuario_cargonom ;
      private String AV97WWUsuarioDS_30_Tfusuario_cargonom_sel ;
      private String lV72WWUsuarioDS_5_Usuario_cargonom1 ;
      private String lV73WWUsuarioDS_6_Usuario_pessoadoc1 ;
      private String lV81WWUsuarioDS_14_Usuario_cargonom2 ;
      private String lV82WWUsuarioDS_15_Usuario_pessoadoc2 ;
      private String lV90WWUsuarioDS_23_Usuario_cargonom3 ;
      private String lV91WWUsuarioDS_24_Usuario_pessoadoc3 ;
      private String lV96WWUsuarioDS_29_Tfusuario_cargonom ;
      private String A1074Usuario_CargoNom ;
      private String A325Usuario_PessoaDoc ;
      private String AV23Option ;
      private IGxSession AV32Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00VZ2_A1073Usuario_CargoCod ;
      private bool[] P00VZ2_n1073Usuario_CargoCod ;
      private int[] P00VZ2_A57Usuario_PessoaCod ;
      private String[] P00VZ2_A2Usuario_Nome ;
      private bool[] P00VZ2_n2Usuario_Nome ;
      private bool[] P00VZ2_A54Usuario_Ativo ;
      private String[] P00VZ2_A325Usuario_PessoaDoc ;
      private bool[] P00VZ2_n325Usuario_PessoaDoc ;
      private String[] P00VZ2_A1074Usuario_CargoNom ;
      private bool[] P00VZ2_n1074Usuario_CargoNom ;
      private String[] P00VZ2_A58Usuario_PessoaNom ;
      private bool[] P00VZ2_n58Usuario_PessoaNom ;
      private int[] P00VZ2_A1Usuario_Codigo ;
      private int[] P00VZ3_A57Usuario_PessoaCod ;
      private int[] P00VZ3_A1073Usuario_CargoCod ;
      private bool[] P00VZ3_n1073Usuario_CargoCod ;
      private String[] P00VZ3_A2Usuario_Nome ;
      private bool[] P00VZ3_n2Usuario_Nome ;
      private bool[] P00VZ3_A54Usuario_Ativo ;
      private String[] P00VZ3_A325Usuario_PessoaDoc ;
      private bool[] P00VZ3_n325Usuario_PessoaDoc ;
      private String[] P00VZ3_A1074Usuario_CargoNom ;
      private bool[] P00VZ3_n1074Usuario_CargoNom ;
      private String[] P00VZ3_A58Usuario_PessoaNom ;
      private bool[] P00VZ3_n58Usuario_PessoaNom ;
      private int[] P00VZ3_A1Usuario_Codigo ;
      private int[] P00VZ4_A57Usuario_PessoaCod ;
      private int[] P00VZ4_A1073Usuario_CargoCod ;
      private bool[] P00VZ4_n1073Usuario_CargoCod ;
      private String[] P00VZ4_A2Usuario_Nome ;
      private bool[] P00VZ4_n2Usuario_Nome ;
      private bool[] P00VZ4_A54Usuario_Ativo ;
      private String[] P00VZ4_A325Usuario_PessoaDoc ;
      private bool[] P00VZ4_n325Usuario_PessoaDoc ;
      private String[] P00VZ4_A1074Usuario_CargoNom ;
      private bool[] P00VZ4_n1074Usuario_CargoNom ;
      private String[] P00VZ4_A58Usuario_PessoaNom ;
      private bool[] P00VZ4_n58Usuario_PessoaNom ;
      private int[] P00VZ4_A1Usuario_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV63Codigos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV34GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV35GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV36GridStateDynamicFilter ;
   }

   public class getwwusuariofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00VZ2( IGxContext context ,
                                             int A1Usuario_Codigo ,
                                             IGxCollection AV63Codigos ,
                                             String AV68WWUsuarioDS_1_Dynamicfiltersselector1 ,
                                             String AV71WWUsuarioDS_4_Usuario_pessoanom1 ,
                                             short AV69WWUsuarioDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWUsuarioDS_5_Usuario_cargonom1 ,
                                             String AV73WWUsuarioDS_6_Usuario_pessoadoc1 ,
                                             String AV75WWUsuarioDS_8_Usuario_ativo1 ,
                                             bool AV76WWUsuarioDS_9_Dynamicfiltersenabled2 ,
                                             String AV77WWUsuarioDS_10_Dynamicfiltersselector2 ,
                                             String AV80WWUsuarioDS_13_Usuario_pessoanom2 ,
                                             short AV78WWUsuarioDS_11_Dynamicfiltersoperator2 ,
                                             String AV81WWUsuarioDS_14_Usuario_cargonom2 ,
                                             String AV82WWUsuarioDS_15_Usuario_pessoadoc2 ,
                                             String AV84WWUsuarioDS_17_Usuario_ativo2 ,
                                             bool AV85WWUsuarioDS_18_Dynamicfiltersenabled3 ,
                                             String AV86WWUsuarioDS_19_Dynamicfiltersselector3 ,
                                             String AV89WWUsuarioDS_22_Usuario_pessoanom3 ,
                                             short AV87WWUsuarioDS_20_Dynamicfiltersoperator3 ,
                                             String AV90WWUsuarioDS_23_Usuario_cargonom3 ,
                                             String AV91WWUsuarioDS_24_Usuario_pessoadoc3 ,
                                             String AV93WWUsuarioDS_26_Usuario_ativo3 ,
                                             String AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel ,
                                             String AV94WWUsuarioDS_27_Tfusuario_pessoanom ,
                                             String AV97WWUsuarioDS_30_Tfusuario_cargonom_sel ,
                                             String AV96WWUsuarioDS_29_Tfusuario_cargonom ,
                                             String AV99WWUsuarioDS_32_Tfusuario_nome_sel ,
                                             String AV98WWUsuarioDS_31_Tfusuario_nome ,
                                             short AV100WWUsuarioDS_33_Tfusuario_ativo_sel ,
                                             int AV39AreaTrabalho_Codigo1 ,
                                             int AV48AreaTrabalho_Codigo2 ,
                                             int AV57AreaTrabalho_Codigo3 ,
                                             String A58Usuario_PessoaNom ,
                                             String A1074Usuario_CargoNom ,
                                             String A325Usuario_PessoaDoc ,
                                             bool A54Usuario_Ativo ,
                                             String A2Usuario_Nome ,
                                             String AV74WWUsuarioDS_7_Usuario_entidade1 ,
                                             String A1083Usuario_Entidade ,
                                             String AV83WWUsuarioDS_16_Usuario_entidade2 ,
                                             String AV92WWUsuarioDS_25_Usuario_entidade3 )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [21] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_CargoCod] AS Usuario_CargoCod, T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Nome], T1.[Usuario_Ativo], T3.[Pessoa_Docto] AS Usuario_PessoaDoc, T2.[Cargo_Nome] AS Usuario_CargoNom, T3.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Codigo] FROM (([Usuario] T1 WITH (NOLOCK) LEFT JOIN [Geral_Cargo] T2 WITH (NOLOCK) ON T2.[Cargo_Codigo] = T1.[Usuario_CargoCod]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T1.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWUsuarioDS_4_Usuario_pessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV71WWUsuarioDS_4_Usuario_pessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV71WWUsuarioDS_4_Usuario_pessoanom1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_CARGONOM") == 0 ) && ( AV69WWUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWUsuarioDS_5_Usuario_cargonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Cargo_Nome] like @lV72WWUsuarioDS_5_Usuario_cargonom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Cargo_Nome] like @lV72WWUsuarioDS_5_Usuario_cargonom1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_CARGONOM") == 0 ) && ( AV69WWUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWUsuarioDS_5_Usuario_cargonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Cargo_Nome] like '%' + @lV72WWUsuarioDS_5_Usuario_cargonom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Cargo_Nome] like '%' + @lV72WWUsuarioDS_5_Usuario_cargonom1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWUsuarioDS_6_Usuario_pessoadoc1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like '%' + @lV73WWUsuarioDS_6_Usuario_pessoadoc1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Docto] like '%' + @lV73WWUsuarioDS_6_Usuario_pessoadoc1)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV75WWUsuarioDS_8_Usuario_ativo1, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV75WWUsuarioDS_8_Usuario_ativo1, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWUsuarioDS_13_Usuario_pessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV80WWUsuarioDS_13_Usuario_pessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV80WWUsuarioDS_13_Usuario_pessoanom2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_CARGONOM") == 0 ) && ( AV78WWUsuarioDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWUsuarioDS_14_Usuario_cargonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Cargo_Nome] like @lV81WWUsuarioDS_14_Usuario_cargonom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Cargo_Nome] like @lV81WWUsuarioDS_14_Usuario_cargonom2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_CARGONOM") == 0 ) && ( AV78WWUsuarioDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWUsuarioDS_14_Usuario_cargonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Cargo_Nome] like '%' + @lV81WWUsuarioDS_14_Usuario_cargonom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Cargo_Nome] like '%' + @lV81WWUsuarioDS_14_Usuario_cargonom2)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWUsuarioDS_15_Usuario_pessoadoc2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like '%' + @lV82WWUsuarioDS_15_Usuario_pessoadoc2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Docto] like '%' + @lV82WWUsuarioDS_15_Usuario_pessoadoc2)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV84WWUsuarioDS_17_Usuario_ativo2, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV84WWUsuarioDS_17_Usuario_ativo2, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWUsuarioDS_22_Usuario_pessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV89WWUsuarioDS_22_Usuario_pessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV89WWUsuarioDS_22_Usuario_pessoanom3)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_CARGONOM") == 0 ) && ( AV87WWUsuarioDS_20_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWUsuarioDS_23_Usuario_cargonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Cargo_Nome] like @lV90WWUsuarioDS_23_Usuario_cargonom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Cargo_Nome] like @lV90WWUsuarioDS_23_Usuario_cargonom3)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_CARGONOM") == 0 ) && ( AV87WWUsuarioDS_20_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWUsuarioDS_23_Usuario_cargonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Cargo_Nome] like '%' + @lV90WWUsuarioDS_23_Usuario_cargonom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Cargo_Nome] like '%' + @lV90WWUsuarioDS_23_Usuario_cargonom3)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWUsuarioDS_24_Usuario_pessoadoc3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like '%' + @lV91WWUsuarioDS_24_Usuario_pessoadoc3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Docto] like '%' + @lV91WWUsuarioDS_24_Usuario_pessoadoc3)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWUsuarioDS_26_Usuario_ativo3, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWUsuarioDS_26_Usuario_ativo3, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWUsuarioDS_27_Tfusuario_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV94WWUsuarioDS_27_Tfusuario_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV94WWUsuarioDS_27_Tfusuario_pessoanom)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWUsuarioDS_30_Tfusuario_cargonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWUsuarioDS_29_Tfusuario_cargonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Cargo_Nome] like @lV96WWUsuarioDS_29_Tfusuario_cargonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Cargo_Nome] like @lV96WWUsuarioDS_29_Tfusuario_cargonom)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWUsuarioDS_30_Tfusuario_cargonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Cargo_Nome] = @AV97WWUsuarioDS_30_Tfusuario_cargonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Cargo_Nome] = @AV97WWUsuarioDS_30_Tfusuario_cargonom_sel)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWUsuarioDS_32_Tfusuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWUsuarioDS_31_Tfusuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Nome] like @lV98WWUsuarioDS_31_Tfusuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Nome] like @lV98WWUsuarioDS_31_Tfusuario_nome)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWUsuarioDS_32_Tfusuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Nome] = @AV99WWUsuarioDS_32_Tfusuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Nome] = @AV99WWUsuarioDS_32_Tfusuario_nome_sel)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( AV100WWUsuarioDS_33_Tfusuario_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV100WWUsuarioDS_33_Tfusuario_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 0)";
            }
         }
         if ( AV39AreaTrabalho_Codigo1 + AV48AreaTrabalho_Codigo2 + AV57AreaTrabalho_Codigo3 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV63Codigos, "T1.[Usuario_Codigo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV63Codigos, "T1.[Usuario_Codigo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Usuario_PessoaCod]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P00VZ3( IGxContext context ,
                                             int A1Usuario_Codigo ,
                                             IGxCollection AV63Codigos ,
                                             String AV68WWUsuarioDS_1_Dynamicfiltersselector1 ,
                                             String AV71WWUsuarioDS_4_Usuario_pessoanom1 ,
                                             short AV69WWUsuarioDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWUsuarioDS_5_Usuario_cargonom1 ,
                                             String AV73WWUsuarioDS_6_Usuario_pessoadoc1 ,
                                             String AV75WWUsuarioDS_8_Usuario_ativo1 ,
                                             bool AV76WWUsuarioDS_9_Dynamicfiltersenabled2 ,
                                             String AV77WWUsuarioDS_10_Dynamicfiltersselector2 ,
                                             String AV80WWUsuarioDS_13_Usuario_pessoanom2 ,
                                             short AV78WWUsuarioDS_11_Dynamicfiltersoperator2 ,
                                             String AV81WWUsuarioDS_14_Usuario_cargonom2 ,
                                             String AV82WWUsuarioDS_15_Usuario_pessoadoc2 ,
                                             String AV84WWUsuarioDS_17_Usuario_ativo2 ,
                                             bool AV85WWUsuarioDS_18_Dynamicfiltersenabled3 ,
                                             String AV86WWUsuarioDS_19_Dynamicfiltersselector3 ,
                                             String AV89WWUsuarioDS_22_Usuario_pessoanom3 ,
                                             short AV87WWUsuarioDS_20_Dynamicfiltersoperator3 ,
                                             String AV90WWUsuarioDS_23_Usuario_cargonom3 ,
                                             String AV91WWUsuarioDS_24_Usuario_pessoadoc3 ,
                                             String AV93WWUsuarioDS_26_Usuario_ativo3 ,
                                             String AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel ,
                                             String AV94WWUsuarioDS_27_Tfusuario_pessoanom ,
                                             String AV97WWUsuarioDS_30_Tfusuario_cargonom_sel ,
                                             String AV96WWUsuarioDS_29_Tfusuario_cargonom ,
                                             String AV99WWUsuarioDS_32_Tfusuario_nome_sel ,
                                             String AV98WWUsuarioDS_31_Tfusuario_nome ,
                                             short AV100WWUsuarioDS_33_Tfusuario_ativo_sel ,
                                             int AV39AreaTrabalho_Codigo1 ,
                                             int AV48AreaTrabalho_Codigo2 ,
                                             int AV57AreaTrabalho_Codigo3 ,
                                             String A58Usuario_PessoaNom ,
                                             String A1074Usuario_CargoNom ,
                                             String A325Usuario_PessoaDoc ,
                                             bool A54Usuario_Ativo ,
                                             String A2Usuario_Nome ,
                                             String AV74WWUsuarioDS_7_Usuario_entidade1 ,
                                             String A1083Usuario_Entidade ,
                                             String AV83WWUsuarioDS_16_Usuario_entidade2 ,
                                             String AV92WWUsuarioDS_25_Usuario_entidade3 )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [21] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_CargoCod] AS Usuario_CargoCod, T1.[Usuario_Nome], T1.[Usuario_Ativo], T2.[Pessoa_Docto] AS Usuario_PessoaDoc, T3.[Cargo_Nome] AS Usuario_CargoNom, T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Codigo] FROM (([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) LEFT JOIN [Geral_Cargo] T3 WITH (NOLOCK) ON T3.[Cargo_Codigo] = T1.[Usuario_CargoCod])";
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWUsuarioDS_4_Usuario_pessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV71WWUsuarioDS_4_Usuario_pessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV71WWUsuarioDS_4_Usuario_pessoanom1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_CARGONOM") == 0 ) && ( AV69WWUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWUsuarioDS_5_Usuario_cargonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV72WWUsuarioDS_5_Usuario_cargonom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV72WWUsuarioDS_5_Usuario_cargonom1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_CARGONOM") == 0 ) && ( AV69WWUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWUsuarioDS_5_Usuario_cargonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV72WWUsuarioDS_5_Usuario_cargonom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV72WWUsuarioDS_5_Usuario_cargonom1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWUsuarioDS_6_Usuario_pessoadoc1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV73WWUsuarioDS_6_Usuario_pessoadoc1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV73WWUsuarioDS_6_Usuario_pessoadoc1)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV75WWUsuarioDS_8_Usuario_ativo1, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV75WWUsuarioDS_8_Usuario_ativo1, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWUsuarioDS_13_Usuario_pessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV80WWUsuarioDS_13_Usuario_pessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV80WWUsuarioDS_13_Usuario_pessoanom2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_CARGONOM") == 0 ) && ( AV78WWUsuarioDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWUsuarioDS_14_Usuario_cargonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV81WWUsuarioDS_14_Usuario_cargonom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV81WWUsuarioDS_14_Usuario_cargonom2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_CARGONOM") == 0 ) && ( AV78WWUsuarioDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWUsuarioDS_14_Usuario_cargonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV81WWUsuarioDS_14_Usuario_cargonom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV81WWUsuarioDS_14_Usuario_cargonom2)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWUsuarioDS_15_Usuario_pessoadoc2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV82WWUsuarioDS_15_Usuario_pessoadoc2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV82WWUsuarioDS_15_Usuario_pessoadoc2)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV84WWUsuarioDS_17_Usuario_ativo2, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV84WWUsuarioDS_17_Usuario_ativo2, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWUsuarioDS_22_Usuario_pessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV89WWUsuarioDS_22_Usuario_pessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV89WWUsuarioDS_22_Usuario_pessoanom3)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_CARGONOM") == 0 ) && ( AV87WWUsuarioDS_20_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWUsuarioDS_23_Usuario_cargonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV90WWUsuarioDS_23_Usuario_cargonom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV90WWUsuarioDS_23_Usuario_cargonom3)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_CARGONOM") == 0 ) && ( AV87WWUsuarioDS_20_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWUsuarioDS_23_Usuario_cargonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV90WWUsuarioDS_23_Usuario_cargonom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV90WWUsuarioDS_23_Usuario_cargonom3)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWUsuarioDS_24_Usuario_pessoadoc3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV91WWUsuarioDS_24_Usuario_pessoadoc3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV91WWUsuarioDS_24_Usuario_pessoadoc3)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWUsuarioDS_26_Usuario_ativo3, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWUsuarioDS_26_Usuario_ativo3, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWUsuarioDS_27_Tfusuario_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV94WWUsuarioDS_27_Tfusuario_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV94WWUsuarioDS_27_Tfusuario_pessoanom)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] = @AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWUsuarioDS_30_Tfusuario_cargonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWUsuarioDS_29_Tfusuario_cargonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV96WWUsuarioDS_29_Tfusuario_cargonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV96WWUsuarioDS_29_Tfusuario_cargonom)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWUsuarioDS_30_Tfusuario_cargonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] = @AV97WWUsuarioDS_30_Tfusuario_cargonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] = @AV97WWUsuarioDS_30_Tfusuario_cargonom_sel)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWUsuarioDS_32_Tfusuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWUsuarioDS_31_Tfusuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Nome] like @lV98WWUsuarioDS_31_Tfusuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Nome] like @lV98WWUsuarioDS_31_Tfusuario_nome)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWUsuarioDS_32_Tfusuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Nome] = @AV99WWUsuarioDS_32_Tfusuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Nome] = @AV99WWUsuarioDS_32_Tfusuario_nome_sel)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( AV100WWUsuarioDS_33_Tfusuario_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV100WWUsuarioDS_33_Tfusuario_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 0)";
            }
         }
         if ( AV39AreaTrabalho_Codigo1 + AV48AreaTrabalho_Codigo2 + AV57AreaTrabalho_Codigo3 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV63Codigos, "T1.[Usuario_Codigo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV63Codigos, "T1.[Usuario_Codigo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Usuario_CargoCod]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_P00VZ4( IGxContext context ,
                                             int A1Usuario_Codigo ,
                                             IGxCollection AV63Codigos ,
                                             String AV68WWUsuarioDS_1_Dynamicfiltersselector1 ,
                                             String AV71WWUsuarioDS_4_Usuario_pessoanom1 ,
                                             short AV69WWUsuarioDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWUsuarioDS_5_Usuario_cargonom1 ,
                                             String AV73WWUsuarioDS_6_Usuario_pessoadoc1 ,
                                             String AV75WWUsuarioDS_8_Usuario_ativo1 ,
                                             bool AV76WWUsuarioDS_9_Dynamicfiltersenabled2 ,
                                             String AV77WWUsuarioDS_10_Dynamicfiltersselector2 ,
                                             String AV80WWUsuarioDS_13_Usuario_pessoanom2 ,
                                             short AV78WWUsuarioDS_11_Dynamicfiltersoperator2 ,
                                             String AV81WWUsuarioDS_14_Usuario_cargonom2 ,
                                             String AV82WWUsuarioDS_15_Usuario_pessoadoc2 ,
                                             String AV84WWUsuarioDS_17_Usuario_ativo2 ,
                                             bool AV85WWUsuarioDS_18_Dynamicfiltersenabled3 ,
                                             String AV86WWUsuarioDS_19_Dynamicfiltersselector3 ,
                                             String AV89WWUsuarioDS_22_Usuario_pessoanom3 ,
                                             short AV87WWUsuarioDS_20_Dynamicfiltersoperator3 ,
                                             String AV90WWUsuarioDS_23_Usuario_cargonom3 ,
                                             String AV91WWUsuarioDS_24_Usuario_pessoadoc3 ,
                                             String AV93WWUsuarioDS_26_Usuario_ativo3 ,
                                             String AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel ,
                                             String AV94WWUsuarioDS_27_Tfusuario_pessoanom ,
                                             String AV97WWUsuarioDS_30_Tfusuario_cargonom_sel ,
                                             String AV96WWUsuarioDS_29_Tfusuario_cargonom ,
                                             String AV99WWUsuarioDS_32_Tfusuario_nome_sel ,
                                             String AV98WWUsuarioDS_31_Tfusuario_nome ,
                                             short AV100WWUsuarioDS_33_Tfusuario_ativo_sel ,
                                             int AV39AreaTrabalho_Codigo1 ,
                                             int AV48AreaTrabalho_Codigo2 ,
                                             int AV57AreaTrabalho_Codigo3 ,
                                             String A58Usuario_PessoaNom ,
                                             String A1074Usuario_CargoNom ,
                                             String A325Usuario_PessoaDoc ,
                                             bool A54Usuario_Ativo ,
                                             String A2Usuario_Nome ,
                                             String AV74WWUsuarioDS_7_Usuario_entidade1 ,
                                             String A1083Usuario_Entidade ,
                                             String AV83WWUsuarioDS_16_Usuario_entidade2 ,
                                             String AV92WWUsuarioDS_25_Usuario_entidade3 )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [21] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_CargoCod] AS Usuario_CargoCod, T1.[Usuario_Nome], T1.[Usuario_Ativo], T2.[Pessoa_Docto] AS Usuario_PessoaDoc, T3.[Cargo_Nome] AS Usuario_CargoNom, T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Codigo] FROM (([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) LEFT JOIN [Geral_Cargo] T3 WITH (NOLOCK) ON T3.[Cargo_Codigo] = T1.[Usuario_CargoCod])";
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int6[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWUsuarioDS_4_Usuario_pessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV71WWUsuarioDS_4_Usuario_pessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV71WWUsuarioDS_4_Usuario_pessoanom1)";
            }
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_CARGONOM") == 0 ) && ( AV69WWUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWUsuarioDS_5_Usuario_cargonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV72WWUsuarioDS_5_Usuario_cargonom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV72WWUsuarioDS_5_Usuario_cargonom1)";
            }
         }
         else
         {
            GXv_int6[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_CARGONOM") == 0 ) && ( AV69WWUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWUsuarioDS_5_Usuario_cargonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV72WWUsuarioDS_5_Usuario_cargonom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV72WWUsuarioDS_5_Usuario_cargonom1)";
            }
         }
         else
         {
            GXv_int6[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWUsuarioDS_6_Usuario_pessoadoc1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV73WWUsuarioDS_6_Usuario_pessoadoc1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV73WWUsuarioDS_6_Usuario_pessoadoc1)";
            }
         }
         else
         {
            GXv_int6[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV75WWUsuarioDS_8_Usuario_ativo1, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( ( StringUtil.StrCmp(AV68WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV75WWUsuarioDS_8_Usuario_ativo1, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int6[5] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWUsuarioDS_13_Usuario_pessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV80WWUsuarioDS_13_Usuario_pessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV80WWUsuarioDS_13_Usuario_pessoanom2)";
            }
         }
         else
         {
            GXv_int6[6] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_CARGONOM") == 0 ) && ( AV78WWUsuarioDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWUsuarioDS_14_Usuario_cargonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV81WWUsuarioDS_14_Usuario_cargonom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV81WWUsuarioDS_14_Usuario_cargonom2)";
            }
         }
         else
         {
            GXv_int6[7] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_CARGONOM") == 0 ) && ( AV78WWUsuarioDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWUsuarioDS_14_Usuario_cargonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV81WWUsuarioDS_14_Usuario_cargonom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV81WWUsuarioDS_14_Usuario_cargonom2)";
            }
         }
         else
         {
            GXv_int6[8] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWUsuarioDS_15_Usuario_pessoadoc2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV82WWUsuarioDS_15_Usuario_pessoadoc2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV82WWUsuarioDS_15_Usuario_pessoadoc2)";
            }
         }
         else
         {
            GXv_int6[9] = 1;
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV84WWUsuarioDS_17_Usuario_ativo2, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV76WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV84WWUsuarioDS_17_Usuario_ativo2, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int6[10] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWUsuarioDS_22_Usuario_pessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV89WWUsuarioDS_22_Usuario_pessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV89WWUsuarioDS_22_Usuario_pessoanom3)";
            }
         }
         else
         {
            GXv_int6[11] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_CARGONOM") == 0 ) && ( AV87WWUsuarioDS_20_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWUsuarioDS_23_Usuario_cargonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV90WWUsuarioDS_23_Usuario_cargonom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV90WWUsuarioDS_23_Usuario_cargonom3)";
            }
         }
         else
         {
            GXv_int6[12] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_CARGONOM") == 0 ) && ( AV87WWUsuarioDS_20_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWUsuarioDS_23_Usuario_cargonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV90WWUsuarioDS_23_Usuario_cargonom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV90WWUsuarioDS_23_Usuario_cargonom3)";
            }
         }
         else
         {
            GXv_int6[13] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWUsuarioDS_24_Usuario_pessoadoc3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV91WWUsuarioDS_24_Usuario_pessoadoc3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV91WWUsuarioDS_24_Usuario_pessoadoc3)";
            }
         }
         else
         {
            GXv_int6[14] = 1;
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWUsuarioDS_26_Usuario_ativo3, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV85WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV86WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWUsuarioDS_26_Usuario_ativo3, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWUsuarioDS_27_Tfusuario_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV94WWUsuarioDS_27_Tfusuario_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV94WWUsuarioDS_27_Tfusuario_pessoanom)";
            }
         }
         else
         {
            GXv_int6[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] = @AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int6[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWUsuarioDS_30_Tfusuario_cargonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWUsuarioDS_29_Tfusuario_cargonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV96WWUsuarioDS_29_Tfusuario_cargonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV96WWUsuarioDS_29_Tfusuario_cargonom)";
            }
         }
         else
         {
            GXv_int6[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWUsuarioDS_30_Tfusuario_cargonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] = @AV97WWUsuarioDS_30_Tfusuario_cargonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] = @AV97WWUsuarioDS_30_Tfusuario_cargonom_sel)";
            }
         }
         else
         {
            GXv_int6[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWUsuarioDS_32_Tfusuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWUsuarioDS_31_Tfusuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Nome] like @lV98WWUsuarioDS_31_Tfusuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Nome] like @lV98WWUsuarioDS_31_Tfusuario_nome)";
            }
         }
         else
         {
            GXv_int6[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWUsuarioDS_32_Tfusuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Nome] = @AV99WWUsuarioDS_32_Tfusuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Nome] = @AV99WWUsuarioDS_32_Tfusuario_nome_sel)";
            }
         }
         else
         {
            GXv_int6[20] = 1;
         }
         if ( AV100WWUsuarioDS_33_Tfusuario_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV100WWUsuarioDS_33_Tfusuario_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 0)";
            }
         }
         if ( AV39AreaTrabalho_Codigo1 + AV48AreaTrabalho_Codigo2 + AV57AreaTrabalho_Codigo3 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV63Codigos, "T1.[Usuario_Codigo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV63Codigos, "T1.[Usuario_Codigo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Usuario_Nome]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00VZ2(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (bool)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] );
               case 1 :
                     return conditional_P00VZ3(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (bool)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] );
               case 2 :
                     return conditional_P00VZ4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (bool)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VZ2 ;
          prmP00VZ2 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV71WWUsuarioDS_4_Usuario_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV72WWUsuarioDS_5_Usuario_cargonom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV72WWUsuarioDS_5_Usuario_cargonom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV73WWUsuarioDS_6_Usuario_pessoadoc1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV80WWUsuarioDS_13_Usuario_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV81WWUsuarioDS_14_Usuario_cargonom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV81WWUsuarioDS_14_Usuario_cargonom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV82WWUsuarioDS_15_Usuario_pessoadoc2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV89WWUsuarioDS_22_Usuario_pessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV90WWUsuarioDS_23_Usuario_cargonom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV90WWUsuarioDS_23_Usuario_cargonom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV91WWUsuarioDS_24_Usuario_pessoadoc3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV94WWUsuarioDS_27_Tfusuario_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV96WWUsuarioDS_29_Tfusuario_cargonom",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV97WWUsuarioDS_30_Tfusuario_cargonom_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV98WWUsuarioDS_31_Tfusuario_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV99WWUsuarioDS_32_Tfusuario_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00VZ3 ;
          prmP00VZ3 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV71WWUsuarioDS_4_Usuario_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV72WWUsuarioDS_5_Usuario_cargonom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV72WWUsuarioDS_5_Usuario_cargonom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV73WWUsuarioDS_6_Usuario_pessoadoc1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV80WWUsuarioDS_13_Usuario_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV81WWUsuarioDS_14_Usuario_cargonom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV81WWUsuarioDS_14_Usuario_cargonom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV82WWUsuarioDS_15_Usuario_pessoadoc2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV89WWUsuarioDS_22_Usuario_pessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV90WWUsuarioDS_23_Usuario_cargonom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV90WWUsuarioDS_23_Usuario_cargonom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV91WWUsuarioDS_24_Usuario_pessoadoc3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV94WWUsuarioDS_27_Tfusuario_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV96WWUsuarioDS_29_Tfusuario_cargonom",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV97WWUsuarioDS_30_Tfusuario_cargonom_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV98WWUsuarioDS_31_Tfusuario_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV99WWUsuarioDS_32_Tfusuario_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00VZ4 ;
          prmP00VZ4 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV71WWUsuarioDS_4_Usuario_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV72WWUsuarioDS_5_Usuario_cargonom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV72WWUsuarioDS_5_Usuario_cargonom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV73WWUsuarioDS_6_Usuario_pessoadoc1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV80WWUsuarioDS_13_Usuario_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV81WWUsuarioDS_14_Usuario_cargonom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV81WWUsuarioDS_14_Usuario_cargonom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV82WWUsuarioDS_15_Usuario_pessoadoc2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV89WWUsuarioDS_22_Usuario_pessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV90WWUsuarioDS_23_Usuario_cargonom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV90WWUsuarioDS_23_Usuario_cargonom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV91WWUsuarioDS_24_Usuario_pessoadoc3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV94WWUsuarioDS_27_Tfusuario_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV95WWUsuarioDS_28_Tfusuario_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV96WWUsuarioDS_29_Tfusuario_cargonom",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV97WWUsuarioDS_30_Tfusuario_cargonom_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV98WWUsuarioDS_31_Tfusuario_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV99WWUsuarioDS_32_Tfusuario_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VZ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VZ2,100,0,true,false )
             ,new CursorDef("P00VZ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VZ3,100,0,true,false )
             ,new CursorDef("P00VZ4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VZ4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwusuariofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwusuariofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwusuariofilterdata") )
          {
             return  ;
          }
          getwwusuariofilterdata worker = new getwwusuariofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
