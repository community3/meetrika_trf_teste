/*
               File: REL_ContagemCapa
        Description: Capa do Lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:7.23
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_contagemcapa : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV26ContagemResultado_LoteAceite = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV140ContagemResultado_SS = (int)(NumberUtil.Val( GetNextPar( ), "."));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_contagemcapa( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_contagemcapa( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_LoteAceite ,
                           int aP1_ContagemResultado_SS )
      {
         this.AV26ContagemResultado_LoteAceite = aP0_ContagemResultado_LoteAceite;
         this.AV140ContagemResultado_SS = aP1_ContagemResultado_SS;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_LoteAceite ,
                                 int aP1_ContagemResultado_SS )
      {
         arel_contagemcapa objarel_contagemcapa;
         objarel_contagemcapa = new arel_contagemcapa();
         objarel_contagemcapa.AV26ContagemResultado_LoteAceite = aP0_ContagemResultado_LoteAceite;
         objarel_contagemcapa.AV140ContagemResultado_SS = aP1_ContagemResultado_SS;
         objarel_contagemcapa.context.SetSubmitInitialConfig(context);
         objarel_contagemcapa.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_contagemcapa);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_contagemcapa)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11952, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV125WWPContext) ;
            GXt_boolean1 = AV144Contratante_SSAutomatica;
            GXt_int2 = AV125WWPContext.gxTpr_Areatrabalho_codigo;
            new prc_ssautomatica(context ).execute( ref  GXt_int2, out  GXt_boolean1) ;
            AV125WWPContext.gxTpr_Areatrabalho_codigo = GXt_int2;
            AV144Contratante_SSAutomatica = GXt_boolean1;
            /* Using cursor P00472 */
            pr_default.execute(0, new Object[] {AV26ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A596Lote_Codigo = P00472_A596Lote_Codigo[0];
               A562Lote_Numero = P00472_A562Lote_Numero[0];
               AV136Lote_Numero = A562Lote_Numero;
               AV128Length = (short)(StringUtil.Len( A562Lote_Numero)-6);
               AV131cAno = StringUtil.Substring( A562Lote_Numero, StringUtil.Len( A562Lote_Numero)-3, 4);
               AV118SubTitulo = "LOTE " + AV131cAno + "/" + StringUtil.PadL( StringUtil.Substring( A562Lote_Numero, 1, AV128Length), 3, "0");
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            /* Using cursor P00474 */
            pr_default.execute(1, new Object[] {AV26ContagemResultado_LoteAceite});
            if ( (pr_default.getStatus(1) != 101) )
            {
               A40000GXC1 = P00474_A40000GXC1[0];
            }
            else
            {
               A40000GXC1 = 0;
            }
            pr_default.close(1);
            AV143UltSS = A40000GXC1;
            /* Execute user subroutine: 'PRINTDATALOTE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H470( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTDATALOTE' Routine */
         AV127x = (short)(AV127x+1);
         /* Execute user subroutine: 'PERIODO' */
         S121 ();
         if (returnInSub) return;
         if ( AV144Contratante_SSAutomatica )
         {
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV140ContagemResultado_SS ,
                                                 A1452ContagemResultado_SS ,
                                                 AV26ContagemResultado_LoteAceite ,
                                                 A597ContagemResultado_LoteAceiteCod },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor P00475 */
            pr_default.execute(2, new Object[] {AV26ContagemResultado_LoteAceite, AV140ContagemResultado_SS});
            while ( (pr_default.getStatus(2) != 101) )
            {
               BRK475 = false;
               A597ContagemResultado_LoteAceiteCod = P00475_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P00475_n597ContagemResultado_LoteAceiteCod[0];
               A512ContagemResultado_ValorPF = P00475_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P00475_n512ContagemResultado_ValorPF[0];
               A1051ContagemResultado_GlsValor = P00475_A1051ContagemResultado_GlsValor[0];
               n1051ContagemResultado_GlsValor = P00475_n1051ContagemResultado_GlsValor[0];
               A1452ContagemResultado_SS = P00475_A1452ContagemResultado_SS[0];
               n1452ContagemResultado_SS = P00475_n1452ContagemResultado_SS[0];
               A489ContagemResultado_SistemaCod = P00475_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P00475_n489ContagemResultado_SistemaCod[0];
               A509ContagemrResultado_SistemaSigla = P00475_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P00475_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P00475_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P00475_n515ContagemResultado_SistemaCoord[0];
               A484ContagemResultado_StatusDmn = P00475_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00475_n484ContagemResultado_StatusDmn[0];
               A1854ContagemResultado_VlrCnc = P00475_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = P00475_n1854ContagemResultado_VlrCnc[0];
               A456ContagemResultado_Codigo = P00475_A456ContagemResultado_Codigo[0];
               A509ContagemrResultado_SistemaSigla = P00475_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P00475_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P00475_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P00475_n515ContagemResultado_SistemaCoord[0];
               GXt_decimal3 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
               A574ContagemResultado_PFFinal = GXt_decimal3;
               AV115Sistema_Codigo = A489ContagemResultado_SistemaCod;
               AV117Sistema_Sigla = A509ContagemrResultado_SistemaSigla;
               AV116Sistema_Coordenacao = A515ContagemResultado_SistemaCoord;
               AV98Os = AV131cAno + "/" + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0)), 4, "0");
               AV112PFFinal = 0;
               while ( (pr_default.getStatus(2) != 101) && ( P00475_A597ContagemResultado_LoteAceiteCod[0] == A597ContagemResultado_LoteAceiteCod ) && ( P00475_A1452ContagemResultado_SS[0] == A1452ContagemResultado_SS ) )
               {
                  BRK475 = false;
                  A512ContagemResultado_ValorPF = P00475_A512ContagemResultado_ValorPF[0];
                  n512ContagemResultado_ValorPF = P00475_n512ContagemResultado_ValorPF[0];
                  A1051ContagemResultado_GlsValor = P00475_A1051ContagemResultado_GlsValor[0];
                  n1051ContagemResultado_GlsValor = P00475_n1051ContagemResultado_GlsValor[0];
                  A456ContagemResultado_Codigo = P00475_A456ContagemResultado_Codigo[0];
                  GXt_decimal3 = A574ContagemResultado_PFFinal;
                  new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
                  A574ContagemResultado_PFFinal = GXt_decimal3;
                  AV138GlsPF = (decimal)(AV138GlsPF+(A1051ContagemResultado_GlsValor/ (decimal)(A512ContagemResultado_ValorPF)));
                  AV137GlsValor = (decimal)(AV137GlsValor+A1051ContagemResultado_GlsValor);
                  AV112PFFinal = (decimal)(AV112PFFinal+A574ContagemResultado_PFFinal);
                  BRK475 = true;
                  pr_default.readNext(2);
               }
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV121Valor = A1854ContagemResultado_VlrCnc;
               }
               else
               {
                  AV121Valor = (decimal)((AV112PFFinal*A512ContagemResultado_ValorPF));
               }
               if ( AV121Valor < 0.001m )
               {
                  AV139strValor = StringUtil.Str( AV121Valor, 14, 4);
               }
               else
               {
                  AV139strValor = StringUtil.Str( AV121Valor, 14, 3);
               }
               H470( false, 19) ;
               getPrinter().GxDrawLine(142, Gx_line+0, 142, Gx_line+19, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(625, Gx_line+0, 625, Gx_line+19, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(333, Gx_line+0, 333, Gx_line+19, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(508, Gx_line+0, 508, Gx_line+19, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(17, Gx_line+0, 17, Gx_line+19, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(783, Gx_line+0, 783, Gx_line+19, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("R$", 633, Gx_line+0, 650, Gx_line+19, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV117Sistema_Sigla, "@!")), 342, Gx_line+0, 501, Gx_line+15, 1, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV116Sistema_Coordenacao, "@!")), 147, Gx_line+0, 331, Gx_line+15, 1, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV112PFFinal, "ZZ,ZZZ,ZZ9.999")), 531, Gx_line+0, 620, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV98Os, "")), 25, Gx_line+0, 130, Gx_line+15, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV139strValor, "")), 696, Gx_line+0, 770, Gx_line+15, 2+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+19);
               AV120Total = (decimal)(AV120Total+AV121Valor);
               if ( ! BRK475 )
               {
                  BRK475 = true;
                  pr_default.readNext(2);
               }
            }
            pr_default.close(2);
         }
         else
         {
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 AV140ContagemResultado_SS ,
                                                 A1452ContagemResultado_SS ,
                                                 AV26ContagemResultado_LoteAceite ,
                                                 A597ContagemResultado_LoteAceiteCod },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor P00476 */
            pr_default.execute(3, new Object[] {AV26ContagemResultado_LoteAceite, AV140ContagemResultado_SS});
            while ( (pr_default.getStatus(3) != 101) )
            {
               BRK477 = false;
               A597ContagemResultado_LoteAceiteCod = P00476_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P00476_n597ContagemResultado_LoteAceiteCod[0];
               A512ContagemResultado_ValorPF = P00476_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P00476_n512ContagemResultado_ValorPF[0];
               A1051ContagemResultado_GlsValor = P00476_A1051ContagemResultado_GlsValor[0];
               n1051ContagemResultado_GlsValor = P00476_n1051ContagemResultado_GlsValor[0];
               A493ContagemResultado_DemandaFM = P00476_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P00476_n493ContagemResultado_DemandaFM[0];
               A1452ContagemResultado_SS = P00476_A1452ContagemResultado_SS[0];
               n1452ContagemResultado_SS = P00476_n1452ContagemResultado_SS[0];
               A489ContagemResultado_SistemaCod = P00476_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P00476_n489ContagemResultado_SistemaCod[0];
               A509ContagemrResultado_SistemaSigla = P00476_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P00476_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P00476_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P00476_n515ContagemResultado_SistemaCoord[0];
               A484ContagemResultado_StatusDmn = P00476_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00476_n484ContagemResultado_StatusDmn[0];
               A1854ContagemResultado_VlrCnc = P00476_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = P00476_n1854ContagemResultado_VlrCnc[0];
               A456ContagemResultado_Codigo = P00476_A456ContagemResultado_Codigo[0];
               A509ContagemrResultado_SistemaSigla = P00476_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P00476_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P00476_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P00476_n515ContagemResultado_SistemaCoord[0];
               GXt_decimal3 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
               A574ContagemResultado_PFFinal = GXt_decimal3;
               AV115Sistema_Codigo = A489ContagemResultado_SistemaCod;
               AV117Sistema_Sigla = A509ContagemrResultado_SistemaSigla;
               AV116Sistema_Coordenacao = A515ContagemResultado_SistemaCoord;
               AV98Os = AV131cAno + "/" + StringUtil.PadL( StringUtil.Trim( A493ContagemResultado_DemandaFM), 4, "0");
               while ( (pr_default.getStatus(3) != 101) && ( P00476_A597ContagemResultado_LoteAceiteCod[0] == A597ContagemResultado_LoteAceiteCod ) && ( StringUtil.StrCmp(P00476_A493ContagemResultado_DemandaFM[0], A493ContagemResultado_DemandaFM) == 0 ) )
               {
                  BRK477 = false;
                  A512ContagemResultado_ValorPF = P00476_A512ContagemResultado_ValorPF[0];
                  n512ContagemResultado_ValorPF = P00476_n512ContagemResultado_ValorPF[0];
                  A1051ContagemResultado_GlsValor = P00476_A1051ContagemResultado_GlsValor[0];
                  n1051ContagemResultado_GlsValor = P00476_n1051ContagemResultado_GlsValor[0];
                  A456ContagemResultado_Codigo = P00476_A456ContagemResultado_Codigo[0];
                  GXt_decimal3 = A574ContagemResultado_PFFinal;
                  new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
                  A574ContagemResultado_PFFinal = GXt_decimal3;
                  AV138GlsPF = (decimal)(AV138GlsPF+(A1051ContagemResultado_GlsValor/ (decimal)(A512ContagemResultado_ValorPF)));
                  AV137GlsValor = (decimal)(AV137GlsValor+A1051ContagemResultado_GlsValor);
                  AV112PFFinal = (decimal)(AV112PFFinal+A574ContagemResultado_PFFinal);
                  BRK477 = true;
                  pr_default.readNext(3);
               }
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV121Valor = A1854ContagemResultado_VlrCnc;
               }
               else
               {
                  AV121Valor = (decimal)((AV112PFFinal*A512ContagemResultado_ValorPF));
               }
               if ( AV121Valor < 0.001m )
               {
                  AV139strValor = StringUtil.Str( AV121Valor, 14, 4);
               }
               else
               {
                  AV139strValor = StringUtil.Str( AV121Valor, 14, 3);
               }
               H470( false, 19) ;
               getPrinter().GxDrawLine(142, Gx_line+0, 142, Gx_line+19, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(625, Gx_line+0, 625, Gx_line+19, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(333, Gx_line+0, 333, Gx_line+19, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(508, Gx_line+0, 508, Gx_line+19, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(17, Gx_line+0, 17, Gx_line+19, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(783, Gx_line+0, 783, Gx_line+19, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("R$", 633, Gx_line+0, 650, Gx_line+19, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV117Sistema_Sigla, "@!")), 342, Gx_line+0, 501, Gx_line+15, 1, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV116Sistema_Coordenacao, "@!")), 147, Gx_line+0, 331, Gx_line+15, 1, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV112PFFinal, "ZZ,ZZZ,ZZ9.999")), 531, Gx_line+0, 620, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV98Os, "")), 25, Gx_line+0, 130, Gx_line+15, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV139strValor, "")), 696, Gx_line+0, 770, Gx_line+15, 2+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+19);
               AV120Total = (decimal)(AV120Total+AV121Valor);
               if ( ! BRK477 )
               {
                  BRK477 = true;
                  pr_default.readNext(3);
               }
            }
            pr_default.close(3);
         }
         if ( ( AV137GlsValor > Convert.ToDecimal( 0 )) )
         {
            AV98Os = "GLOSAS";
            AV116Sistema_Coordenacao = "";
            AV117Sistema_Sigla = "";
            AV112PFFinal = AV138GlsPF;
            AV121Valor = AV137GlsValor;
            H470( false, 19) ;
            getPrinter().GxDrawLine(142, Gx_line+0, 142, Gx_line+19, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(625, Gx_line+0, 625, Gx_line+19, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(333, Gx_line+0, 333, Gx_line+19, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(508, Gx_line+0, 508, Gx_line+19, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(17, Gx_line+0, 17, Gx_line+19, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(783, Gx_line+0, 783, Gx_line+19, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("R$", 633, Gx_line+0, 650, Gx_line+19, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV117Sistema_Sigla, "@!")), 342, Gx_line+0, 501, Gx_line+15, 1, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV116Sistema_Coordenacao, "@!")), 147, Gx_line+0, 331, Gx_line+15, 1, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV112PFFinal, "ZZ,ZZZ,ZZ9.999")), 531, Gx_line+0, 620, Gx_line+15, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV98Os, "")), 25, Gx_line+0, 130, Gx_line+15, 1+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV139strValor, "")), 696, Gx_line+0, 770, Gx_line+15, 2+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+19);
            AV120Total = (decimal)(AV120Total-AV137GlsValor);
         }
         AV142SS_Order = (int)(AV142SS_Order+1);
         H470( false, 2) ;
         getPrinter().GxDrawLine(18, Gx_line+0, 782, Gx_line+0, 1, 0, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+2);
         H470( false, 21) ;
         getPrinter().GxDrawLine(626, Gx_line+20, 783, Gx_line+20, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(625, Gx_line+0, 625, Gx_line+20, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(783, Gx_line+0, 783, Gx_line+20, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("R$", 633, Gx_line+0, 650, Gx_line+19, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV120Total, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 662, Gx_line+0, 770, Gx_line+20, 2, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+21);
         H470( false, 258) ;
         getPrinter().GxDrawLine(17, Gx_line+16, 781, Gx_line+16, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(17, Gx_line+16, 17, Gx_line+123, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(781, Gx_line+16, 781, Gx_line+125, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(17, Gx_line+123, 781, Gx_line+123, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(18, Gx_line+39, 782, Gx_line+39, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(433, Gx_line+240, 784, Gx_line+240, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(25, Gx_line+240, 308, Gx_line+240, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Assinatura e Carimbo do Coordenador Geral (CGAM)", 455, Gx_line+240, 761, Gx_line+258, 1+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Assinatura e Carimbo do Gestor Contratual", 41, Gx_line+240, 292, Gx_line+258, 1+256, 0, 0, 0) ;
         getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Declaro que as ordens de servi�os especificadas acima foram solicitadas pelas �reas competentes e as demandas, geradas por meio do Sistema de Gest�o de Demandas - SIRIUS, REDMINE e descritas nos termos de aceites, foram executadas de forma satisfat�ria e em conformidade com o objeto do contrato, o que afirmo total ci�ncia e responsabilidade sobre as mesmas.", 25, Gx_line+40, 775, Gx_line+119, 0+16, 0, 0, 0) ;
         getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("APROVA��O", 24, Gx_line+19, 150, Gx_line+38, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Bras�lia, ___ de ___________ de ____.", 25, Gx_line+140, 317, Gx_line+159, 0, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+258);
         /* Eject command */
         Gx_OldLine = Gx_line;
         Gx_line = (int)(P_lines+1);
      }

      protected void S121( )
      {
         /* 'PERIODO' Routine */
         /* Using cursor P00479 */
         pr_default.execute(4, new Object[] {AV26ContagemResultado_LoteAceite});
         if ( (pr_default.getStatus(4) != 101) )
         {
            A40001GXC2 = P00479_A40001GXC2[0];
            A40002GXC3 = P00479_A40002GXC3[0];
         }
         else
         {
            A40001GXC2 = DateTime.MinValue;
            A40002GXC3 = DateTime.MinValue;
         }
         pr_default.close(4);
         if ( StringUtil.StrCmp(AV136Lote_Numero, "2012016") == 0 )
         {
            AV66DataInicio = context.localUtil.CToD( "04/01/2016", 2);
            AV65DataFim = context.localUtil.CToD( "08/01/2016", 2);
         }
         else
         {
            AV66DataInicio = A40001GXC2;
            AV65DataFim = A40002GXC3;
         }
         AV130Periodo = "PER�ODO DE EXECU��O: " + context.localUtil.DToC( AV66DataInicio, 2, "/") + " a " + context.localUtil.DToC( AV65DataFim, 2, "/");
      }

      protected void H470( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("P�gina", 627, Gx_line+2, 671, Gx_line+29, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 730, Gx_line+2, 747, Gx_line+29, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 755, Gx_line+2, 773, Gx_line+29, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 676, Gx_line+2, 721, Gx_line+29, 1, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+29);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxDrawBitMap(context.GetImagePath( "6ded99f5-9d9f-4337-aeda-7b9046b912b6", "", context.GetTheme( )), 349, Gx_line+0, 429, Gx_line+80) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("MINIST�RIO DA SA�DE", 317, Gx_line+83, 462, Gx_line+102, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText("SECRETARIA EXECUTIVA", 315, Gx_line+100, 465, Gx_line+119, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText("DEPARTAMENTO DE INFORM�TICA DO SUS - DATASUS", 217, Gx_line+117, 556, Gx_line+136, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV118SubTitulo, "")), 222, Gx_line+142, 556, Gx_line+162, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV130Periodo, "")), 55, Gx_line+161, 723, Gx_line+181, 1+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+192);
               if ( AV142SS_Order <= AV143UltSS )
               {
                  getPrinter().GxDrawRect(17, Gx_line+0, 784, Gx_line+50, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(625, Gx_line+0, 625, Gx_line+49, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(508, Gx_line+0, 508, Gx_line+49, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(333, Gx_line+0, 333, Gx_line+49, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(142, Gx_line+0, 142, Gx_line+49, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Valor", 688, Gx_line+24, 722, Gx_line+43, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Quantidade", 528, Gx_line+24, 605, Gx_line+43, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("PF Final", 675, Gx_line+8, 736, Gx_line+27, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("PF Final", 536, Gx_line+8, 597, Gx_line+27, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Sistema", 395, Gx_line+17, 447, Gx_line+36, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Coordena��o", 196, Gx_line+17, 281, Gx_line+36, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("N�mero OS/TA", 28, Gx_line+17, 125, Gx_line+36, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+52);
               }
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Calibri", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV125WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         P00472_A596Lote_Codigo = new int[1] ;
         P00472_A562Lote_Numero = new String[] {""} ;
         A562Lote_Numero = "";
         AV136Lote_Numero = "";
         AV131cAno = "";
         AV118SubTitulo = "";
         P00474_A40000GXC1 = new int[1] ;
         AV143UltSS = 0;
         P00475_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00475_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00475_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00475_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00475_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P00475_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P00475_A1452ContagemResultado_SS = new int[1] ;
         P00475_n1452ContagemResultado_SS = new bool[] {false} ;
         P00475_A489ContagemResultado_SistemaCod = new int[1] ;
         P00475_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00475_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00475_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00475_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P00475_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P00475_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00475_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00475_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P00475_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P00475_A456ContagemResultado_Codigo = new int[1] ;
         A509ContagemrResultado_SistemaSigla = "";
         A515ContagemResultado_SistemaCoord = "";
         A484ContagemResultado_StatusDmn = "";
         AV117Sistema_Sigla = "";
         AV116Sistema_Coordenacao = "";
         AV98Os = "";
         AV139strValor = "";
         P00476_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00476_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00476_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00476_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00476_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P00476_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P00476_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00476_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00476_A1452ContagemResultado_SS = new int[1] ;
         P00476_n1452ContagemResultado_SS = new bool[] {false} ;
         P00476_A489ContagemResultado_SistemaCod = new int[1] ;
         P00476_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00476_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00476_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00476_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P00476_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P00476_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00476_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00476_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P00476_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P00476_A456ContagemResultado_Codigo = new int[1] ;
         A493ContagemResultado_DemandaFM = "";
         AV142SS_Order = 0;
         P00479_A40001GXC2 = new DateTime[] {DateTime.MinValue} ;
         P00479_A40002GXC3 = new DateTime[] {DateTime.MinValue} ;
         A40001GXC2 = DateTime.MinValue;
         A40002GXC3 = DateTime.MinValue;
         AV66DataInicio = DateTime.MinValue;
         AV65DataFim = DateTime.MinValue;
         AV130Periodo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_contagemcapa__default(),
            new Object[][] {
                new Object[] {
               P00472_A596Lote_Codigo, P00472_A562Lote_Numero
               }
               , new Object[] {
               P00474_A40000GXC1
               }
               , new Object[] {
               P00475_A597ContagemResultado_LoteAceiteCod, P00475_n597ContagemResultado_LoteAceiteCod, P00475_A512ContagemResultado_ValorPF, P00475_n512ContagemResultado_ValorPF, P00475_A1051ContagemResultado_GlsValor, P00475_n1051ContagemResultado_GlsValor, P00475_A1452ContagemResultado_SS, P00475_n1452ContagemResultado_SS, P00475_A489ContagemResultado_SistemaCod, P00475_n489ContagemResultado_SistemaCod,
               P00475_A509ContagemrResultado_SistemaSigla, P00475_n509ContagemrResultado_SistemaSigla, P00475_A515ContagemResultado_SistemaCoord, P00475_n515ContagemResultado_SistemaCoord, P00475_A484ContagemResultado_StatusDmn, P00475_n484ContagemResultado_StatusDmn, P00475_A1854ContagemResultado_VlrCnc, P00475_n1854ContagemResultado_VlrCnc, P00475_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00476_A597ContagemResultado_LoteAceiteCod, P00476_n597ContagemResultado_LoteAceiteCod, P00476_A512ContagemResultado_ValorPF, P00476_n512ContagemResultado_ValorPF, P00476_A1051ContagemResultado_GlsValor, P00476_n1051ContagemResultado_GlsValor, P00476_A493ContagemResultado_DemandaFM, P00476_n493ContagemResultado_DemandaFM, P00476_A1452ContagemResultado_SS, P00476_n1452ContagemResultado_SS,
               P00476_A489ContagemResultado_SistemaCod, P00476_n489ContagemResultado_SistemaCod, P00476_A509ContagemrResultado_SistemaSigla, P00476_n509ContagemrResultado_SistemaSigla, P00476_A515ContagemResultado_SistemaCoord, P00476_n515ContagemResultado_SistemaCoord, P00476_A484ContagemResultado_StatusDmn, P00476_n484ContagemResultado_StatusDmn, P00476_A1854ContagemResultado_VlrCnc, P00476_n1854ContagemResultado_VlrCnc,
               P00476_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00479_A40001GXC2, P00479_A40002GXC3
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV128Length ;
      private short AV127x ;
      private int AV26ContagemResultado_LoteAceite ;
      private int AV140ContagemResultado_SS ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int GXt_int2 ;
      private int A596Lote_Codigo ;
      private int A40000GXC1 ;
      private int AV143UltSS ;
      private int A1452ContagemResultado_SS ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV115Sistema_Codigo ;
      private int Gx_OldLine ;
      private int AV142SS_Order ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal AV112PFFinal ;
      private decimal AV138GlsPF ;
      private decimal AV137GlsValor ;
      private decimal AV121Valor ;
      private decimal AV120Total ;
      private decimal GXt_decimal3 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String scmdbuf ;
      private String A562Lote_Numero ;
      private String AV136Lote_Numero ;
      private String AV131cAno ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV117Sistema_Sigla ;
      private String AV139strValor ;
      private String AV130Periodo ;
      private DateTime A40001GXC2 ;
      private DateTime A40002GXC3 ;
      private DateTime AV66DataInicio ;
      private DateTime AV65DataFim ;
      private bool entryPointCalled ;
      private bool AV144Contratante_SSAutomatica ;
      private bool GXt_boolean1 ;
      private bool returnInSub ;
      private bool BRK475 ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n1452ContagemResultado_SS ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool BRK477 ;
      private bool n493ContagemResultado_DemandaFM ;
      private String AV118SubTitulo ;
      private String A515ContagemResultado_SistemaCoord ;
      private String AV116Sistema_Coordenacao ;
      private String AV98Os ;
      private String A493ContagemResultado_DemandaFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00472_A596Lote_Codigo ;
      private String[] P00472_A562Lote_Numero ;
      private int[] P00474_A40000GXC1 ;
      private int[] P00475_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00475_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] P00475_A512ContagemResultado_ValorPF ;
      private bool[] P00475_n512ContagemResultado_ValorPF ;
      private decimal[] P00475_A1051ContagemResultado_GlsValor ;
      private bool[] P00475_n1051ContagemResultado_GlsValor ;
      private int[] P00475_A1452ContagemResultado_SS ;
      private bool[] P00475_n1452ContagemResultado_SS ;
      private int[] P00475_A489ContagemResultado_SistemaCod ;
      private bool[] P00475_n489ContagemResultado_SistemaCod ;
      private String[] P00475_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00475_n509ContagemrResultado_SistemaSigla ;
      private String[] P00475_A515ContagemResultado_SistemaCoord ;
      private bool[] P00475_n515ContagemResultado_SistemaCoord ;
      private String[] P00475_A484ContagemResultado_StatusDmn ;
      private bool[] P00475_n484ContagemResultado_StatusDmn ;
      private decimal[] P00475_A1854ContagemResultado_VlrCnc ;
      private bool[] P00475_n1854ContagemResultado_VlrCnc ;
      private int[] P00475_A456ContagemResultado_Codigo ;
      private int[] P00476_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00476_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] P00476_A512ContagemResultado_ValorPF ;
      private bool[] P00476_n512ContagemResultado_ValorPF ;
      private decimal[] P00476_A1051ContagemResultado_GlsValor ;
      private bool[] P00476_n1051ContagemResultado_GlsValor ;
      private String[] P00476_A493ContagemResultado_DemandaFM ;
      private bool[] P00476_n493ContagemResultado_DemandaFM ;
      private int[] P00476_A1452ContagemResultado_SS ;
      private bool[] P00476_n1452ContagemResultado_SS ;
      private int[] P00476_A489ContagemResultado_SistemaCod ;
      private bool[] P00476_n489ContagemResultado_SistemaCod ;
      private String[] P00476_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00476_n509ContagemrResultado_SistemaSigla ;
      private String[] P00476_A515ContagemResultado_SistemaCoord ;
      private bool[] P00476_n515ContagemResultado_SistemaCoord ;
      private String[] P00476_A484ContagemResultado_StatusDmn ;
      private bool[] P00476_n484ContagemResultado_StatusDmn ;
      private decimal[] P00476_A1854ContagemResultado_VlrCnc ;
      private bool[] P00476_n1854ContagemResultado_VlrCnc ;
      private int[] P00476_A456ContagemResultado_Codigo ;
      private DateTime[] P00479_A40001GXC2 ;
      private DateTime[] P00479_A40002GXC3 ;
      private wwpbaseobjects.SdtWWPContext AV125WWPContext ;
   }

   public class arel_contagemcapa__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00475( IGxContext context ,
                                             int AV140ContagemResultado_SS ,
                                             int A1452ContagemResultado_SS ,
                                             int AV26ContagemResultado_LoteAceite ,
                                             int A597ContagemResultado_LoteAceiteCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [2] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_ValorPF], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_SS], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_LoteAceiteCod] = @AV26ContagemResultado_LoteAceite)";
         if ( ! (0==AV140ContagemResultado_SS) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SS] = @AV140ContagemResultado_SS)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_P00476( IGxContext context ,
                                             int AV140ContagemResultado_SS ,
                                             int A1452ContagemResultado_SS ,
                                             int AV26ContagemResultado_LoteAceite ,
                                             int A597ContagemResultado_LoteAceiteCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [2] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_ValorPF], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_SS], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_LoteAceiteCod] = @AV26ContagemResultado_LoteAceite)";
         if ( ! (0==AV140ContagemResultado_SS) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SS] = @AV140ContagemResultado_SS)";
         }
         else
         {
            GXv_int6[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_DemandaFM]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_P00475(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 3 :
                     return conditional_P00476(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00472 ;
          prmP00472 = new Object[] {
          new Object[] {"@AV26ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00474 ;
          prmP00474 = new Object[] {
          new Object[] {"@AV26ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00479 ;
          prmP00479 = new Object[] {
          new Object[] {"@AV26ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00475 ;
          prmP00475 = new Object[] {
          new Object[] {"@AV26ContagemResultado_LoteAceite",SqlDbType.Int,6,0} ,
          new Object[] {"@AV140ContagemResultado_SS",SqlDbType.Int,8,0}
          } ;
          Object[] prmP00476 ;
          prmP00476 = new Object[] {
          new Object[] {"@AV26ContagemResultado_LoteAceite",SqlDbType.Int,6,0} ,
          new Object[] {"@AV140ContagemResultado_SS",SqlDbType.Int,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00472", "SELECT [Lote_Codigo], [Lote_Numero] FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @AV26ContagemResultado_LoteAceite ORDER BY [Lote_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00472,1,0,false,true )
             ,new CursorDef("P00474", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT MAX([ContagemResultado_SS]) AS GXC1 FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV26ContagemResultado_LoteAceite ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00474,1,0,true,true )
             ,new CursorDef("P00475", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00475,100,0,true,false )
             ,new CursorDef("P00476", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00476,100,0,true,false )
             ,new CursorDef("P00479", "SELECT COALESCE( T1.[GXC2], convert( DATETIME, '17530101', 112 )) AS GXC2, COALESCE( T1.[GXC3], convert( DATETIME, '17530101', 112 )) AS GXC3 FROM (SELECT MIN(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS GXC2, MAX(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS GXC3 FROM ([ContagemResultado] T2 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T2.[ContagemResultado_Codigo]) WHERE T2.[ContagemResultado_LoteAceiteCod] = @AV26ContagemResultado_LoteAceite ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00479,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 25) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 25) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                return;
             case 4 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
