/*
               File: WP_OSVinculada
        Description:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:21:26.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_osvinculada : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_osvinculada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_osvinculada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContagemresultado_contratadacod = new GXCombobox();
         dynavContratadaorigemcod = new GXCombobox();
         dynavContratada_codigo = new GXCombobox();
         dynavContagemresultado_contadorfmcod = new GXCombobox();
         dynavContagemresultado_servico = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV20WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTRATADACODCF2( AV20WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADAORIGEMCOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV20WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADAORIGEMCODCF2( AV20WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV20WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGOCF2( AV20WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTADORFMCOD") == 0 )
            {
               AV17Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTADORFMCODCF2( AV17Contratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV17Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SERVICOCF2( AV17Contratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PACF2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTCF2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216212675");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_osvinculada.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A684ContagemResultado_PFBFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFLFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A685ContagemResultado_PFLFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICOSIGLA", StringUtil.RTrim( A801ContagemResultado_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "vRETORNO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23Retorno), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vQUEMCADASTROU", StringUtil.RTrim( AV22QuemCadastrou));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_DEMANDA", AV24Sdt_Demanda);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_DEMANDA", AV24Sdt_Demanda);
         }
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_PFBFM", StringUtil.LTrim( StringUtil.NToC( AV11ContagemResultado_PFBFM, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_PFLFM", StringUtil.LTrim( StringUtil.NToC( AV13ContagemResultado_PFLFM, 14, 5, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_DEMANDAS", AV25Sdt_Demandas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_DEMANDAS", AV25Sdt_Demandas);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV20WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV20WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_SERVICO_Text", StringUtil.RTrim( dynavContagemresultado_servico.Description));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WECF2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTCF2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_osvinculada.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_OSVinculada" ;
      }

      public override String GetPgmdesc( )
      {
         return "" ;
      }

      protected void WBCF0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_CF2( true) ;
         }
         else
         {
            wb_table1_2_CF2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_CF2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTCF2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPCF0( ) ;
      }

      protected void WSCF2( )
      {
         STARTCF2( ) ;
         EVTCF2( ) ;
      }

      protected void EVTCF2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11CF2 */
                              E11CF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_CONTRATADACOD.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12CF2 */
                              E12CF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_DEMANDA.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13CF2 */
                              E13CF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_SERVICO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14CF2 */
                              E14CF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VNEWCONTAGEMRESULTADO_DEMANDA.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15CF2 */
                              E15CF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E16CF2 */
                                    E16CF2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17CF2 */
                              E17CF2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WECF2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PACF2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContagemresultado_contratadacod.Name = "vCONTAGEMRESULTADO_CONTRATADACOD";
            dynavContagemresultado_contratadacod.WebTags = "";
            dynavContratadaorigemcod.Name = "vCONTRATADAORIGEMCOD";
            dynavContratadaorigemcod.WebTags = "";
            dynavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            dynavContratada_codigo.WebTags = "";
            dynavContagemresultado_contadorfmcod.Name = "vCONTAGEMRESULTADO_CONTADORFMCOD";
            dynavContagemresultado_contadorfmcod.WebTags = "";
            dynavContagemresultado_servico.Name = "vCONTAGEMRESULTADO_SERVICO";
            dynavContagemresultado_servico.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavContagemresultado_contratadacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACODCF2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataCF2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlCF2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataCF2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contratadacod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contratadacod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contratadacod.ItemCount > 0 )
         {
            AV6ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataCF2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H00CF2 */
         pr_default.execute(0, new Object[] {AV20WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CF2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00CF2_A41Contratada_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTRATADAORIGEMCODCF2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADAORIGEMCOD_dataCF2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADAORIGEMCOD_htmlCF2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADAORIGEMCOD_dataCF2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratadaorigemcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratadaorigemcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratadaorigemcod.ItemCount > 0 )
         {
            AV27ContratadaOrigemCod = (int)(NumberUtil.Val( dynavContratadaorigemcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27ContratadaOrigemCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27ContratadaOrigemCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADAORIGEMCOD_dataCF2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00CF3 */
         pr_default.execute(1, new Object[] {AV20WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CF3_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00CF3_A41Contratada_PessoaNom[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvCONTRATADA_CODIGOCF2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO_dataCF2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO_htmlCF2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO_dataCF2( AV20WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV17Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO_dataCF2( wwpbaseobjects.SdtWWPContext AV20WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00CF4 */
         pr_default.execute(2, new Object[] {AV20WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CF4_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00CF4_A41Contratada_PessoaNom[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFMCODCF2( int AV17Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTADORFMCOD_dataCF2( AV17Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlCF2( int AV17Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTADORFMCOD_dataCF2( AV17Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contadorfmcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contadorfmcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contadorfmcod.ItemCount > 0 )
         {
            AV21ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFMCOD_dataCF2( int AV17Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00CF5 */
         pr_default.execute(3, new Object[] {AV17Contratada_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CF5_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00CF5_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICOCF2( int AV17Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SERVICO_dataCF2( AV17Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SERVICO_htmlCF2( int AV17Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SERVICO_dataCF2( AV17Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_servico.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_servico.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV15ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICO_dataCF2( int AV17Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00CF6 */
         pr_default.execute(4, new Object[] {AV17Contratada_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Servico_Codigo), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00CF6_A605Servico_Sigla[0]));
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContagemresultado_contratadacod.ItemCount > 0 )
         {
            AV6ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0)));
         }
         if ( dynavContratadaorigemcod.ItemCount > 0 )
         {
            AV27ContratadaOrigemCod = (int)(NumberUtil.Val( dynavContratadaorigemcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27ContratadaOrigemCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27ContratadaOrigemCod), 6, 0)));
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV17Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
         }
         if ( dynavContagemresultado_contadorfmcod.ItemCount > 0 )
         {
            AV21ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0)));
         }
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV15ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFCF2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_pfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfs_Enabled), 5, 0)));
         edtavContagemresultado_pflfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pflfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pflfs_Enabled), 5, 0)));
      }

      protected void RFCF2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E17CF2 */
            E17CF2 ();
            WBCF0( ) ;
         }
      }

      protected void STRUPCF0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_pfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfs_Enabled), 5, 0)));
         edtavContagemresultado_pflfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pflfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pflfs_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11CF2 */
         E11CF2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlCF2( AV20WWPContext) ;
         GXVvCONTRATADAORIGEMCOD_htmlCF2( AV20WWPContext) ;
         GXVvCONTRATADA_CODIGO_htmlCF2( AV20WWPContext) ;
         GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlCF2( AV17Contratada_Codigo) ;
         GXVvCONTAGEMRESULTADO_SERVICO_htmlCF2( AV17Contratada_Codigo) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavContagemresultado_contratadacod.CurrentValue = cgiGet( dynavContagemresultado_contratadacod_Internalname);
            AV6ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contratadacod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0)));
            AV8ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_Demanda", AV8ContagemResultado_Demanda);
            AV28ContagemResultado_ServicoSigla = StringUtil.Upper( cgiGet( edtavContagemresultado_servicosigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultado_ServicoSigla", AV28ContagemResultado_ServicoSigla);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfs_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFBFS");
               GX_FocusControl = edtavContagemresultado_pfbfs_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12ContagemResultado_PFBFS = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV12ContagemResultado_PFBFS, 14, 5)));
            }
            else
            {
               AV12ContagemResultado_PFBFS = context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfs_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV12ContagemResultado_PFBFS, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfs_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFLFS");
               GX_FocusControl = edtavContagemresultado_pflfs_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14ContagemResultado_PFLFS = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFLFS, 14, 5)));
            }
            else
            {
               AV14ContagemResultado_PFLFS = context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfs_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFLFS, 14, 5)));
            }
            dynavContratadaorigemcod.CurrentValue = cgiGet( dynavContratadaorigemcod_Internalname);
            AV27ContratadaOrigemCod = (int)(NumberUtil.Val( cgiGet( dynavContratadaorigemcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27ContratadaOrigemCod), 6, 0)));
            dynavContratada_codigo.CurrentValue = cgiGet( dynavContratada_codigo_Internalname);
            AV17Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
            dynavContagemresultado_contadorfmcod.CurrentValue = cgiGet( dynavContagemresultado_contadorfmcod_Internalname);
            AV21ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contadorfmcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0)));
            dynavContagemresultado_servico.CurrentValue = cgiGet( dynavContagemresultado_servico_Internalname);
            AV15ContagemResultado_Servico = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_servico_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0)));
            AV19NewContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtavNewcontagemresultado_demanda_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NewContagemResultado_Demanda", AV19NewContagemResultado_Demanda);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datacnt_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data"}), 1, "vCONTAGEMRESULTADO_DATACNT");
               GX_FocusControl = edtavContagemresultado_datacnt_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7ContagemResultado_DataCnt = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_DataCnt", context.localUtil.Format(AV7ContagemResultado_DataCnt, "99/99/99"));
            }
            else
            {
               AV7ContagemResultado_DataCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datacnt_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_DataCnt", context.localUtil.Format(AV7ContagemResultado_DataCnt, "99/99/99"));
            }
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlCF2( AV20WWPContext) ;
            GXVvCONTRATADAORIGEMCOD_htmlCF2( AV20WWPContext) ;
            GXVvCONTRATADA_CODIGO_htmlCF2( AV20WWPContext) ;
            GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlCF2( AV17Contratada_Codigo) ;
            GXVvCONTAGEMRESULTADO_SERVICO_htmlCF2( AV17Contratada_Codigo) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11CF2 */
         E11CF2 ();
         if (returnInSub) return;
      }

      protected void E11CF2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV20WWPContext) ;
         bttBtnenter_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
         if ( AV20WWPContext.gxTpr_Userehadministradorgam )
         {
            dynavContratada_codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratada_codigo.Enabled), 5, 0)));
            dynavContagemresultado_contadorfmcod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfmcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contadorfmcod.Enabled), 5, 0)));
         }
         else
         {
            /* Using cursor H00CF7 */
            pr_default.execute(5, new Object[] {AV20WWPContext.gxTpr_Userid, AV20WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A69ContratadaUsuario_UsuarioCod = H00CF7_A69ContratadaUsuario_UsuarioCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H00CF7_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H00CF7_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A66ContratadaUsuario_ContratadaCod = H00CF7_A66ContratadaUsuario_ContratadaCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H00CF7_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H00CF7_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               AV17Contratada_Codigo = A66ContratadaUsuario_ContratadaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
               pr_default.readNext(5);
            }
            pr_default.close(5);
            AV21ContagemResultado_ContadorFMCod = AV20WWPContext.gxTpr_Userid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0)));
            AV7ContagemResultado_DataCnt = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_DataCnt", context.localUtil.Format(AV7ContagemResultado_DataCnt, "99/99/99"));
            dynavContratada_codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratada_codigo.Enabled), 5, 0)));
            dynavContagemresultado_contadorfmcod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfmcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contadorfmcod.Enabled), 5, 0)));
            edtavContagemresultado_datacnt_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_datacnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_datacnt_Enabled), 5, 0)));
         }
      }

      protected void E12CF2( )
      {
         /* Contagemresultado_contratadacod_Click Routine */
         if ( ! (0==AV6ContagemResultado_ContratadaCod) )
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8ContagemResultado_Demanda)) )
            {
               /* Execute user subroutine: 'BUSCADEMANDA' */
               S112 ();
               if (returnInSub) return;
            }
            else
            {
               bttBtnenter_Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
               bttBtnenter_Tooltiptext = "Confirmar";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            }
         }
      }

      protected void E13CF2( )
      {
         /* Contagemresultado_demanda_Isvalid Routine */
         /* Execute user subroutine: 'BUSCADEMANDA' */
         S112 ();
         if (returnInSub) return;
      }

      protected void E14CF2( )
      {
         /* Contagemresultado_servico_Click Routine */
         if ( ! (0==AV15ContagemResultado_Servico) )
         {
            bttBtnenter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8ContagemResultado_Demanda)) )
            {
               AV19NewContagemResultado_Demanda = StringUtil.Trim( AV8ContagemResultado_Demanda) + StringUtil.Substring( dynavContagemresultado_servico.Description, 1, 1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NewContagemResultado_Demanda", AV19NewContagemResultado_Demanda);
            }
         }
      }

      protected void E15CF2( )
      {
         /* Newcontagemresultado_demanda_Isvalid Routine */
         GXt_int1 = AV23Retorno;
         if ( new prc_existedemanda(context).executeUdp(  AV19NewContagemResultado_Demanda,  0,  "",  AV20WWPContext.gxTpr_Areatrabalho_codigo,  false, ref  GXt_int1,  AV15ContagemResultado_Servico,  AV6ContagemResultado_ContratadaCod, out  AV22QuemCadastrou) )
         {
            GX_msglist.addItem("Nova OS j� cadastrada por "+StringUtil.Trim( StringUtil.Substring( AV22QuemCadastrou, StringUtil.StringSearch( AV22QuemCadastrou, ",", 1)+2, 100))+"!");
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Nova OS j� cadastrada!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
         }
         else
         {
            bttBtnenter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E16CF2 */
         E16CF2 ();
         if (returnInSub) return;
      }

      protected void E16CF2( )
      {
         /* Enter Routine */
         GXt_int1 = AV23Retorno;
         if ( new prc_existedemanda(context).executeUdp(  AV19NewContagemResultado_Demanda,  0,  "",  AV20WWPContext.gxTpr_Areatrabalho_codigo,  false, ref  GXt_int1,  AV15ContagemResultado_Servico,  AV6ContagemResultado_ContratadaCod, out  AV22QuemCadastrou) )
         {
            GX_msglist.addItem("Nova OS j� cadastrada por "+StringUtil.Trim( StringUtil.Substring( AV22QuemCadastrou, StringUtil.StringSearch( AV22QuemCadastrou, ",", 1)+2, 100))+"!");
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Nova OS j� cadastrada";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
         }
         else if ( (0==AV6ContagemResultado_ContratadaCod) )
         {
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Informe a Contratada que exwcutou a OS!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            GX_msglist.addItem("Informe a Contratada que exwcutou a OS!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV8ContagemResultado_Demanda)) )
         {
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Informe a OS vinculada!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            GX_msglist.addItem("Informe a OS vinculada!");
         }
         else if ( (0==AV15ContagemResultado_Servico) )
         {
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Informe o Servi�o executado!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            GX_msglist.addItem("Informe o Servi�o executado!");
         }
         else if ( (0==AV21ContagemResultado_ContadorFMCod) )
         {
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Informe o Respons�vel!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            GX_msglist.addItem("Informe o Respons�vel!");
         }
         else if ( (DateTime.MinValue==AV7ContagemResultado_DataCnt) )
         {
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Informe a data do Servi�o!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            GX_msglist.addItem("Informe a data do Servi�o!");
         }
         else
         {
            /* Execute user subroutine: 'CARREGASDT' */
            S122 ();
            if (returnInSub) return;
            new prc_novaosvinculada(context ).execute( ref  AV5ContagemResultado_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Codigo), 6, 0)));
            context.wjLoc = formatLink("contagemresultado.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +AV5ContagemResultado_Codigo);
            context.wjLocDisableFrm = 1;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24Sdt_Demanda", AV24Sdt_Demanda);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25Sdt_Demandas", AV25Sdt_Demandas);
      }

      protected void S112( )
      {
         /* 'BUSCADEMANDA' Routine */
         AV32GXLvl110 = 0;
         /* Using cursor H00CF9 */
         pr_default.execute(6, new Object[] {AV6ContagemResultado_ContratadaCod, AV8ContagemResultado_Demanda, AV20WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00CF9_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00CF9_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = H00CF9_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00CF9_n601ContagemResultado_Servico[0];
            A457ContagemResultado_Demanda = H00CF9_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00CF9_n457ContagemResultado_Demanda[0];
            A490ContagemResultado_ContratadaCod = H00CF9_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00CF9_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = H00CF9_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00CF9_n52Contratada_AreaTrabalhoCod[0];
            A456ContagemResultado_Codigo = H00CF9_A456ContagemResultado_Codigo[0];
            A801ContagemResultado_ServicoSigla = H00CF9_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00CF9_n801ContagemResultado_ServicoSigla[0];
            A684ContagemResultado_PFBFSUltima = H00CF9_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00CF9_A685ContagemResultado_PFLFSUltima[0];
            A601ContagemResultado_Servico = H00CF9_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00CF9_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = H00CF9_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00CF9_n801ContagemResultado_ServicoSigla[0];
            A52Contratada_AreaTrabalhoCod = H00CF9_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00CF9_n52Contratada_AreaTrabalhoCod[0];
            A684ContagemResultado_PFBFSUltima = H00CF9_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00CF9_A685ContagemResultado_PFLFSUltima[0];
            AV32GXLvl110 = 1;
            AV5ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Codigo), 6, 0)));
            AV12ContagemResultado_PFBFS = A684ContagemResultado_PFBFSUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV12ContagemResultado_PFBFS, 14, 5)));
            AV14ContagemResultado_PFLFS = A685ContagemResultado_PFLFSUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFLFS, 14, 5)));
            AV28ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultado_ServicoSigla", AV28ContagemResultado_ServicoSigla);
            bttBtnenter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(6);
         }
         pr_default.close(6);
         if ( AV32GXLvl110 == 0 )
         {
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
            bttBtnenter_Tooltiptext = "OS informada n�o achada!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            GX_msglist.addItem("Demanda inexistente!");
            GX_FocusControl = edtavContagemresultado_demanda_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
            AV12ContagemResultado_PFBFS = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV12ContagemResultado_PFBFS, 14, 5)));
            AV14ContagemResultado_PFLFS = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFLFS, 14, 5)));
            AV28ContagemResultado_ServicoSigla = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultado_ServicoSigla", AV28ContagemResultado_ServicoSigla);
         }
      }

      protected void S122( )
      {
         /* 'CARREGASDT' Routine */
         AV24Sdt_Demanda.gxTpr_Contratada_codigo = AV17Contratada_Codigo;
         AV24Sdt_Demanda.gxTpr_Contratadaorigem_codigo = AV27ContratadaOrigemCod;
         AV24Sdt_Demanda.gxTpr_Contadorfmcod = AV21ContagemResultado_ContadorFMCod;
         AV24Sdt_Demanda.gxTpr_Demanda = AV19NewContagemResultado_Demanda;
         AV24Sdt_Demanda.gxTpr_Pfbfm = AV11ContagemResultado_PFBFM;
         AV24Sdt_Demanda.gxTpr_Pflfm = AV13ContagemResultado_PFLFM;
         AV24Sdt_Demanda.gxTpr_Servico_codigo = AV15ContagemResultado_Servico;
         AV24Sdt_Demanda.gxTpr_Datacnt = AV7ContagemResultado_DataCnt;
         AV25Sdt_Demandas.Add(AV24Sdt_Demanda, 0);
         AV26WebSession.Set("SDT_Demandas", AV25Sdt_Demandas.ToXml(false, true, "SDT_Demandas", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void nextLoad( )
      {
      }

      protected void E17CF2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_CF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock12_Internalname, "Quem executou:", "", "", lblTextblock12_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contratadacod, dynavContagemresultado_contratadacod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0)), 1, dynavContagemresultado_contratadacod_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_CONTRATADACOD.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_WP_OSVinculada.htm");
            dynavContagemresultado_contratadacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod_Internalname, "Values", (String)(dynavContagemresultado_contratadacod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "OS Vinculada:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda_Internalname, AV8ContagemResultado_Demanda, StringUtil.RTrim( context.localUtil.Format( AV8ContagemResultado_Demanda, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,15);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demanda_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock8_Internalname, "Servi�o:", "", "", lblTextblock8_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_servicosigla_Internalname, StringUtil.RTrim( AV28ContagemResultado_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( AV28ContagemResultado_ServicoSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_servicosigla_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            wb_table2_22_CF2( true) ;
         }
         else
         {
            wb_table2_22_CF2( false) ;
         }
         return  ;
      }

      protected void wb_table2_22_CF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Contratada Origem", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratadaorigemcod, dynavContratadaorigemcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27ContratadaOrigemCod), 6, 0)), 1, dynavContratadaorigemcod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_WP_OSVinculada.htm");
            dynavContratadaorigemcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27ContratadaOrigemCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigemcod_Internalname, "Values", (String)(dynavContratadaorigemcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock7_Internalname, "Prestadora", "", "", lblTextblock7_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo, dynavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)), 1, dynavContratada_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavContratada_codigo.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_WP_OSVinculada.htm");
            dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", (String)(dynavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Respons�vel:", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contadorfmcod, dynavContagemresultado_contadorfmcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0)), 1, dynavContagemresultado_contadorfmcod_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e18cf1_client"+"'", "int", "", 1, dynavContagemresultado_contadorfmcod.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_WP_OSVinculada.htm");
            dynavContagemresultado_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfmcod_Internalname, "Values", (String)(dynavContagemresultado_contadorfmcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Servi�o", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_servico, dynavContagemresultado_servico_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0)), 1, dynavContagemresultado_servico_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_SERVICO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_WP_OSVinculada.htm");
            dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_servico_Internalname, "Values", (String)(dynavContagemresultado_servico.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Nova OS:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNewcontagemresultado_demanda_Internalname, AV19NewContagemResultado_Demanda, StringUtil.RTrim( context.localUtil.Format( AV19NewContagemResultado_Demanda, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNewcontagemresultado_demanda_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "Data do Servi�o:", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datacnt_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datacnt_Internalname, context.localUtil.Format(AV7ContagemResultado_DataCnt, "99/99/99"), context.localUtil.Format( AV7ContagemResultado_DataCnt, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datacnt_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_datacnt_Enabled, 1, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OSVinculada.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datacnt_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_datacnt_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"4\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Criar OS vinculada", bttBtnenter_Jsonclick, 5, bttBtnenter_Tooltiptext, "", StyleString, ClassString, 1, bttBtnenter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_OSVinculada.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Fechar", bttButton1_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_CF2e( true) ;
         }
         else
         {
            wb_table1_2_CF2e( false) ;
         }
      }

      protected void wb_table2_22_CF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock9_Internalname, "Qtd. Entregue Bruto", "", "", lblTextblock9_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pfbfs_Internalname, StringUtil.LTrim( StringUtil.NToC( AV12ContagemResultado_PFBFS, 14, 5, ",", "")), ((edtavContagemresultado_pfbfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV12ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV12ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pfbfs_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_pfbfs_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock10_Internalname, "Qtd. Entregue Liquido", "", "", lblTextblock10_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pflfs_Internalname, StringUtil.LTrim( StringUtil.NToC( AV14ContagemResultado_PFLFS, 14, 5, ",", "")), ((edtavContagemresultado_pflfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV14ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV14ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pflfs_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_pflfs_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OSVinculada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_22_CF2e( true) ;
         }
         else
         {
            wb_table2_22_CF2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PACF2( ) ;
         WSCF2( ) ;
         WECF2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216212741");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_osvinculada.js", "?20206216212741");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock12_Internalname = "TEXTBLOCK12";
         dynavContagemresultado_contratadacod_Internalname = "vCONTAGEMRESULTADO_CONTRATADACOD";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavContagemresultado_demanda_Internalname = "vCONTAGEMRESULTADO_DEMANDA";
         lblTextblock8_Internalname = "TEXTBLOCK8";
         edtavContagemresultado_servicosigla_Internalname = "vCONTAGEMRESULTADO_SERVICOSIGLA";
         lblTextblock9_Internalname = "TEXTBLOCK9";
         edtavContagemresultado_pfbfs_Internalname = "vCONTAGEMRESULTADO_PFBFS";
         lblTextblock10_Internalname = "TEXTBLOCK10";
         edtavContagemresultado_pflfs_Internalname = "vCONTAGEMRESULTADO_PFLFS";
         tblTable3_Internalname = "TABLE3";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         dynavContratadaorigemcod_Internalname = "vCONTRATADAORIGEMCOD";
         lblTextblock7_Internalname = "TEXTBLOCK7";
         dynavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         dynavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         dynavContagemresultado_servico_Internalname = "vCONTAGEMRESULTADO_SERVICO";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavNewcontagemresultado_demanda_Internalname = "vNEWCONTAGEMRESULTADO_DEMANDA";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavContagemresultado_datacnt_Internalname = "vCONTAGEMRESULTADO_DATACNT";
         bttBtnenter_Internalname = "BTNENTER";
         bttButton1_Internalname = "BUTTON1";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavContagemresultado_pflfs_Jsonclick = "";
         edtavContagemresultado_pflfs_Enabled = 1;
         edtavContagemresultado_pfbfs_Jsonclick = "";
         edtavContagemresultado_pfbfs_Enabled = 1;
         bttBtnenter_Enabled = 1;
         edtavContagemresultado_datacnt_Jsonclick = "";
         edtavNewcontagemresultado_demanda_Jsonclick = "";
         dynavContagemresultado_servico_Jsonclick = "";
         dynavContagemresultado_contadorfmcod_Jsonclick = "";
         dynavContratada_codigo_Jsonclick = "";
         dynavContratadaorigemcod_Jsonclick = "";
         edtavContagemresultado_servicosigla_Jsonclick = "";
         edtavContagemresultado_demanda_Jsonclick = "";
         dynavContagemresultado_contratadacod_Jsonclick = "";
         bttBtnenter_Tooltiptext = "Criar OS vinculada";
         edtavContagemresultado_datacnt_Enabled = 1;
         dynavContagemresultado_contadorfmcod.Enabled = 1;
         dynavContratada_codigo.Enabled = 1;
         dynavContagemresultado_servico.Description = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Contratada_codigo( GXCombobox dynGX_Parm1 ,
                                            GXCombobox dynGX_Parm2 ,
                                            GXCombobox dynGX_Parm3 )
      {
         dynavContratada_codigo = dynGX_Parm1;
         AV17Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.CurrentValue, "."));
         dynavContagemresultado_contadorfmcod = dynGX_Parm2;
         AV21ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.CurrentValue, "."));
         dynavContagemresultado_servico = dynGX_Parm3;
         AV15ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlCF2( AV17Contratada_Codigo) ;
         GXVvCONTAGEMRESULTADO_SERVICO_htmlCF2( AV17Contratada_Codigo) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_contadorfmcod.ItemCount > 0 )
         {
            AV21ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0))), "."));
         }
         dynavContagemresultado_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFMCod), 6, 0));
         isValidOutput.Add(dynavContagemresultado_contadorfmcod);
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV15ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0))), "."));
         }
         dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_Servico), 6, 0));
         isValidOutput.Add(dynavContagemresultado_servico);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("VCONTAGEMRESULTADO_CONTRATADACOD.CLICK","{handler:'E12CF2',iparms:[{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A685ContagemResultado_PFLFSUltima',fld:'CONTAGEMRESULTADO_PFLFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''}],oparms:[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContagemResultado_PFBFS',fld:'vCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_PFLFS',fld:'vCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28ContagemResultado_ServicoSigla',fld:'vCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''}]}");
         setEventMetadata("VCONTAGEMRESULTADO_DEMANDA.ISVALID","{handler:'E13CF2',iparms:[{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV8ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A685ContagemResultado_PFLFSUltima',fld:'CONTAGEMRESULTADO_PFLFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''}],oparms:[{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContagemResultado_PFBFS',fld:'vCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_PFLFS',fld:'vCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28ContagemResultado_ServicoSigla',fld:'vCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'}]}");
         setEventMetadata("VCONTAGEMRESULTADO_SERVICO.CLICK","{handler:'E14CF2',iparms:[{av:'AV15ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'dynavContagemresultado_servico'}],oparms:[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{av:'AV19NewContagemResultado_Demanda',fld:'vNEWCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''}]}");
         setEventMetadata("VNEWCONTAGEMRESULTADO_DEMANDA.ISVALID","{handler:'E15CF2',iparms:[{av:'AV19NewContagemResultado_Demanda',fld:'vNEWCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV20WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV23Retorno',fld:'vRETORNO',pic:'ZZZ9',nv:0},{av:'AV15ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV22QuemCadastrou',fld:'vQUEMCADASTROU',pic:'',nv:''}],oparms:[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'}]}");
         setEventMetadata("VCONTAGEMRESULTADO_CONTADORFMCOD.CLICK","{handler:'E18CF1',iparms:[{av:'AV21ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E16CF2',iparms:[{av:'AV22QuemCadastrou',fld:'vQUEMCADASTROU',pic:'',nv:''},{av:'AV23Retorno',fld:'vRETORNO',pic:'ZZZ9',nv:0},{av:'AV20WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV19NewContagemResultado_Demanda',fld:'vNEWCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV15ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV21ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_DataCnt',fld:'vCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV17Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24Sdt_Demanda',fld:'vSDT_DEMANDA',pic:'',nv:null},{av:'AV27ContratadaOrigemCod',fld:'vCONTRATADAORIGEMCOD',pic:'ZZZZZ9',nv:0},{av:'AV11ContagemResultado_PFBFM',fld:'vCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV13ContagemResultado_PFLFM',fld:'vCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25Sdt_Demandas',fld:'vSDT_DEMANDAS',pic:'',nv:null}],oparms:[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24Sdt_Demanda',fld:'vSDT_DEMANDA',pic:'',nv:null},{av:'AV25Sdt_Demandas',fld:'vSDT_DEMANDAS',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV20WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A457ContagemResultado_Demanda = "";
         A801ContagemResultado_ServicoSigla = "";
         AV22QuemCadastrou = "";
         AV24Sdt_Demanda = new SdtSDT_Demandas_Demanda(context);
         AV25Sdt_Demandas = new GxObjectCollection( context, "SDT_Demandas.Demanda", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Demandas_Demanda", "GeneXus.Programs");
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00CF2_A40Contratada_PessoaCod = new int[1] ;
         H00CF2_A39Contratada_Codigo = new int[1] ;
         H00CF2_A41Contratada_PessoaNom = new String[] {""} ;
         H00CF2_n41Contratada_PessoaNom = new bool[] {false} ;
         H00CF2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CF2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CF3_A40Contratada_PessoaCod = new int[1] ;
         H00CF3_A39Contratada_Codigo = new int[1] ;
         H00CF3_A41Contratada_PessoaNom = new String[] {""} ;
         H00CF3_n41Contratada_PessoaNom = new bool[] {false} ;
         H00CF3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CF3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CF4_A40Contratada_PessoaCod = new int[1] ;
         H00CF4_A39Contratada_Codigo = new int[1] ;
         H00CF4_A41Contratada_PessoaNom = new String[] {""} ;
         H00CF4_n41Contratada_PessoaNom = new bool[] {false} ;
         H00CF4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CF4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CF5_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00CF5_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00CF5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00CF5_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00CF5_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00CF5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00CF5_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00CF5_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00CF6_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         H00CF6_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         H00CF6_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CF6_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CF6_A605Servico_Sigla = new String[] {""} ;
         H00CF6_n605Servico_Sigla = new bool[] {false} ;
         H00CF6_A39Contratada_Codigo = new int[1] ;
         AV8ContagemResultado_Demanda = "";
         AV28ContagemResultado_ServicoSigla = "";
         AV19NewContagemResultado_Demanda = "";
         AV7ContagemResultado_DataCnt = DateTime.MinValue;
         H00CF7_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00CF7_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00CF7_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00CF7_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00CF9_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00CF9_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00CF9_A601ContagemResultado_Servico = new int[1] ;
         H00CF9_n601ContagemResultado_Servico = new bool[] {false} ;
         H00CF9_A457ContagemResultado_Demanda = new String[] {""} ;
         H00CF9_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00CF9_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00CF9_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00CF9_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CF9_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CF9_A456ContagemResultado_Codigo = new int[1] ;
         H00CF9_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00CF9_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00CF9_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00CF9_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         AV26WebSession = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock12_Jsonclick = "";
         TempTags = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock8_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock7_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         bttBtnenter_Jsonclick = "";
         bttButton1_Jsonclick = "";
         lblTextblock9_Jsonclick = "";
         lblTextblock10_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_osvinculada__default(),
            new Object[][] {
                new Object[] {
               H00CF2_A40Contratada_PessoaCod, H00CF2_A39Contratada_Codigo, H00CF2_A41Contratada_PessoaNom, H00CF2_n41Contratada_PessoaNom, H00CF2_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00CF3_A40Contratada_PessoaCod, H00CF3_A39Contratada_Codigo, H00CF3_A41Contratada_PessoaNom, H00CF3_n41Contratada_PessoaNom, H00CF3_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00CF4_A40Contratada_PessoaCod, H00CF4_A39Contratada_Codigo, H00CF4_A41Contratada_PessoaNom, H00CF4_n41Contratada_PessoaNom, H00CF4_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00CF5_A70ContratadaUsuario_UsuarioPessoaCod, H00CF5_n70ContratadaUsuario_UsuarioPessoaCod, H00CF5_A69ContratadaUsuario_UsuarioCod, H00CF5_A71ContratadaUsuario_UsuarioPessoaNom, H00CF5_n71ContratadaUsuario_UsuarioPessoaNom, H00CF5_A66ContratadaUsuario_ContratadaCod, H00CF5_A1394ContratadaUsuario_UsuarioAtivo, H00CF5_n1394ContratadaUsuario_UsuarioAtivo
               }
               , new Object[] {
               H00CF6_A1595Contratada_AreaTrbSrvPdr, H00CF6_n1595Contratada_AreaTrbSrvPdr, H00CF6_A52Contratada_AreaTrabalhoCod, H00CF6_A605Servico_Sigla, H00CF6_n605Servico_Sigla, H00CF6_A39Contratada_Codigo
               }
               , new Object[] {
               H00CF7_A69ContratadaUsuario_UsuarioCod, H00CF7_A1228ContratadaUsuario_AreaTrabalhoCod, H00CF7_n1228ContratadaUsuario_AreaTrabalhoCod, H00CF7_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00CF9_A1553ContagemResultado_CntSrvCod, H00CF9_n1553ContagemResultado_CntSrvCod, H00CF9_A601ContagemResultado_Servico, H00CF9_n601ContagemResultado_Servico, H00CF9_A457ContagemResultado_Demanda, H00CF9_n457ContagemResultado_Demanda, H00CF9_A490ContagemResultado_ContratadaCod, H00CF9_n490ContagemResultado_ContratadaCod, H00CF9_A52Contratada_AreaTrabalhoCod, H00CF9_n52Contratada_AreaTrabalhoCod,
               H00CF9_A456ContagemResultado_Codigo, H00CF9_A801ContagemResultado_ServicoSigla, H00CF9_n801ContagemResultado_ServicoSigla, H00CF9_A684ContagemResultado_PFBFSUltima, H00CF9_A685ContagemResultado_PFLFSUltima
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_pfbfs_Enabled = 0;
         edtavContagemresultado_pflfs_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV23Retorno ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV32GXLvl110 ;
      private short nGXWrapped ;
      private short wbTemp ;
      private int AV17Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV5ContagemResultado_Codigo ;
      private int gxdynajaxindex ;
      private int AV6ContagemResultado_ContratadaCod ;
      private int AV27ContratadaOrigemCod ;
      private int AV21ContagemResultado_ContadorFMCod ;
      private int AV15ContagemResultado_Servico ;
      private int Servico_Codigo ;
      private int edtavContagemresultado_pfbfs_Enabled ;
      private int edtavContagemresultado_pflfs_Enabled ;
      private int bttBtnenter_Enabled ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int edtavContagemresultado_datacnt_Enabled ;
      private int GXt_int1 ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int idxLst ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal AV11ContagemResultado_PFBFM ;
      private decimal AV13ContagemResultado_PFLFM ;
      private decimal AV12ContagemResultado_PFBFS ;
      private decimal AV14ContagemResultado_PFLFS ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A801ContagemResultado_ServicoSigla ;
      private String AV22QuemCadastrou ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String dynavContagemresultado_contratadacod_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavContagemresultado_pfbfs_Internalname ;
      private String edtavContagemresultado_pflfs_Internalname ;
      private String edtavContagemresultado_demanda_Internalname ;
      private String AV28ContagemResultado_ServicoSigla ;
      private String edtavContagemresultado_servicosigla_Internalname ;
      private String dynavContratadaorigemcod_Internalname ;
      private String dynavContratada_codigo_Internalname ;
      private String dynavContagemresultado_contadorfmcod_Internalname ;
      private String dynavContagemresultado_servico_Internalname ;
      private String edtavNewcontagemresultado_demanda_Internalname ;
      private String edtavContagemresultado_datacnt_Internalname ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Tooltiptext ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock12_Internalname ;
      private String lblTextblock12_Jsonclick ;
      private String TempTags ;
      private String dynavContagemresultado_contratadacod_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavContagemresultado_demanda_Jsonclick ;
      private String lblTextblock8_Internalname ;
      private String lblTextblock8_Jsonclick ;
      private String edtavContagemresultado_servicosigla_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String dynavContratadaorigemcod_Jsonclick ;
      private String lblTextblock7_Internalname ;
      private String lblTextblock7_Jsonclick ;
      private String dynavContratada_codigo_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String dynavContagemresultado_contadorfmcod_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String dynavContagemresultado_servico_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavNewcontagemresultado_demanda_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String edtavContagemresultado_datacnt_Jsonclick ;
      private String bttBtnenter_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String tblTable3_Internalname ;
      private String lblTextblock9_Internalname ;
      private String lblTextblock9_Jsonclick ;
      private String edtavContagemresultado_pfbfs_Jsonclick ;
      private String lblTextblock10_Internalname ;
      private String lblTextblock10_Jsonclick ;
      private String edtavContagemresultado_pflfs_Jsonclick ;
      private DateTime AV7ContagemResultado_DataCnt ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n457ContagemResultado_Demanda ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n801ContagemResultado_ServicoSigla ;
      private String A457ContagemResultado_Demanda ;
      private String AV8ContagemResultado_Demanda ;
      private String AV19NewContagemResultado_Demanda ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavContagemresultado_contratadacod ;
      private GXCombobox dynavContratadaorigemcod ;
      private GXCombobox dynavContratada_codigo ;
      private GXCombobox dynavContagemresultado_contadorfmcod ;
      private GXCombobox dynavContagemresultado_servico ;
      private IDataStoreProvider pr_default ;
      private int[] H00CF2_A40Contratada_PessoaCod ;
      private int[] H00CF2_A39Contratada_Codigo ;
      private String[] H00CF2_A41Contratada_PessoaNom ;
      private bool[] H00CF2_n41Contratada_PessoaNom ;
      private int[] H00CF2_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CF2_n52Contratada_AreaTrabalhoCod ;
      private int[] H00CF3_A40Contratada_PessoaCod ;
      private int[] H00CF3_A39Contratada_Codigo ;
      private String[] H00CF3_A41Contratada_PessoaNom ;
      private bool[] H00CF3_n41Contratada_PessoaNom ;
      private int[] H00CF3_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CF3_n52Contratada_AreaTrabalhoCod ;
      private int[] H00CF4_A40Contratada_PessoaCod ;
      private int[] H00CF4_A39Contratada_Codigo ;
      private String[] H00CF4_A41Contratada_PessoaNom ;
      private bool[] H00CF4_n41Contratada_PessoaNom ;
      private int[] H00CF4_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CF4_n52Contratada_AreaTrabalhoCod ;
      private int[] H00CF5_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00CF5_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00CF5_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00CF5_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00CF5_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00CF5_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H00CF5_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00CF5_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] H00CF6_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] H00CF6_n1595Contratada_AreaTrbSrvPdr ;
      private int[] H00CF6_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CF6_n52Contratada_AreaTrabalhoCod ;
      private String[] H00CF6_A605Servico_Sigla ;
      private bool[] H00CF6_n605Servico_Sigla ;
      private int[] H00CF6_A39Contratada_Codigo ;
      private int[] H00CF7_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00CF7_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00CF7_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H00CF7_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00CF9_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00CF9_n1553ContagemResultado_CntSrvCod ;
      private int[] H00CF9_A601ContagemResultado_Servico ;
      private bool[] H00CF9_n601ContagemResultado_Servico ;
      private String[] H00CF9_A457ContagemResultado_Demanda ;
      private bool[] H00CF9_n457ContagemResultado_Demanda ;
      private int[] H00CF9_A490ContagemResultado_ContratadaCod ;
      private bool[] H00CF9_n490ContagemResultado_ContratadaCod ;
      private int[] H00CF9_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CF9_n52Contratada_AreaTrabalhoCod ;
      private int[] H00CF9_A456ContagemResultado_Codigo ;
      private String[] H00CF9_A801ContagemResultado_ServicoSigla ;
      private bool[] H00CF9_n801ContagemResultado_ServicoSigla ;
      private decimal[] H00CF9_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00CF9_A685ContagemResultado_PFLFSUltima ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV26WebSession ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Demandas_Demanda ))]
      private IGxCollection AV25Sdt_Demandas ;
      private GXWebForm Form ;
      private SdtSDT_Demandas_Demanda AV24Sdt_Demanda ;
      private wwpbaseobjects.SdtWWPContext AV20WWPContext ;
   }

   public class wp_osvinculada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00CF2 ;
          prmH00CF2 = new Object[] {
          new Object[] {"@AV20WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CF3 ;
          prmH00CF3 = new Object[] {
          new Object[] {"@AV20WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CF4 ;
          prmH00CF4 = new Object[] {
          new Object[] {"@AV20WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CF5 ;
          prmH00CF5 = new Object[] {
          new Object[] {"@AV17Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CF6 ;
          prmH00CF6 = new Object[] {
          new Object[] {"@AV17Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CF7 ;
          prmH00CF7 = new Object[] {
          new Object[] {"@AV20WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV20WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CF9 ;
          prmH00CF9 = new Object[] {
          new Object[] {"@AV6ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV20WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00CF2", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV20WWPC_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CF2,0,0,true,false )
             ,new CursorDef("H00CF3", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV20WWPC_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CF3,0,0,true,false )
             ,new CursorDef("H00CF4", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV20WWPC_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CF4,0,0,true,false )
             ,new CursorDef("H00CF5", "SELECT T3.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE (T2.[Usuario_Ativo] = 1) AND (T1.[ContratadaUsuario_ContratadaCod] = @AV17Contratada_Codigo) ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CF5,0,0,true,false )
             ,new CursorDef("H00CF6", "SELECT T3.[Servico_Codigo] AS Contratada_AreaTrbSrvPdr, T2.[AreaTrabalho_Codigo] AS Contratada_AreaTrabalhoCod, T3.[Servico_Sigla], T1.[Contratada_Codigo] FROM (([Contratada] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[AreaTrabalho_ServicoPadrao]) WHERE T1.[Contratada_Codigo] = @AV17Contratada_Codigo ORDER BY T3.[Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CF6,0,0,true,false )
             ,new CursorDef("H00CF7", "SELECT T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV20WWPContext__Userid) AND (T2.[Contratada_AreaTrabalhoCod] = @AV20WWPC_1Areatrabalho_codigo) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CF7,100,0,false,false )
             ,new CursorDef("H00CF9", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Demanda], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T4.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[ContagemResultado_Codigo], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T5 ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_ContratadaCod] = @AV6ContagemResultado_ContratadaCod) AND (T1.[ContagemResultado_Demanda] = RTRIM(LTRIM(@AV8ContagemResultado_Demanda))) AND (T4.[Contratada_AreaTrabalhoCod] = @AV20WWPC_1Areatrabalho_codigo) ORDER BY T1.[ContagemResultado_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CF9,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((decimal[]) buf[14])[0] = rslt.getDecimal(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
       }
    }

 }

}
