/*
               File: WWContratanteUsuario
        Description:  Contratante Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:29:22.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratanteusuario : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratanteusuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratanteusuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_73 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_73_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_73_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV56Usuario_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Usuario_Nome1", AV56Usuario_Nome1);
               AV19ContratanteUsuario_UsuarioPessoaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratanteUsuario_UsuarioPessoaNom1", AV19ContratanteUsuario_UsuarioPessoaNom1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV57Usuario_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Usuario_Nome2", AV57Usuario_Nome2);
               AV24ContratanteUsuario_UsuarioPessoaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratanteUsuario_UsuarioPessoaNom2", AV24ContratanteUsuario_UsuarioPessoaNom2);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV61TFContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContratanteUsuario_ContratanteCod), 6, 0)));
               AV62TFContratanteUsuario_ContratanteCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratanteUsuario_ContratanteCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContratanteUsuario_ContratanteCod_To), 6, 0)));
               AV65TFContratanteUsuario_ContratanteFan = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratanteUsuario_ContratanteFan", AV65TFContratanteUsuario_ContratanteFan);
               AV66TFContratanteUsuario_ContratanteFan_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratanteUsuario_ContratanteFan_Sel", AV66TFContratanteUsuario_ContratanteFan_Sel);
               AV69TFContratanteUsuario_ContratanteRaz = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratanteUsuario_ContratanteRaz", AV69TFContratanteUsuario_ContratanteRaz);
               AV70TFContratanteUsuario_ContratanteRaz_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratanteUsuario_ContratanteRaz_Sel", AV70TFContratanteUsuario_ContratanteRaz_Sel);
               AV73TFContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73TFContratanteUsuario_UsuarioCod), 6, 0)));
               AV74TFContratanteUsuario_UsuarioCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratanteUsuario_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74TFContratanteUsuario_UsuarioCod_To), 6, 0)));
               AV77TFContratanteUsuario_UsuarioPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContratanteUsuario_UsuarioPessoaCod), 6, 0)));
               AV78TFContratanteUsuario_UsuarioPessoaCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContratanteUsuario_UsuarioPessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78TFContratanteUsuario_UsuarioPessoaCod_To), 6, 0)));
               AV81TFContratanteUsuario_UsuarioPessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratanteUsuario_UsuarioPessoaNom", AV81TFContratanteUsuario_UsuarioPessoaNom);
               AV82TFContratanteUsuario_UsuarioPessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratanteUsuario_UsuarioPessoaNom_Sel", AV82TFContratanteUsuario_UsuarioPessoaNom_Sel);
               AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace", AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace);
               AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace", AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace);
               AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace", AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace);
               AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace", AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace);
               AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace", AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace);
               AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace", AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace);
               AV113Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV31DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
               AV30DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
               A63ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A60ContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A340ContratanteUsuario_ContratantePesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n340ContratanteUsuario_ContratantePesCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A340ContratanteUsuario_ContratantePesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A340ContratanteUsuario_ContratantePesCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV56Usuario_Nome1, AV19ContratanteUsuario_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV57Usuario_Nome2, AV24ContratanteUsuario_UsuarioPessoaNom2, AV20DynamicFiltersEnabled2, AV61TFContratanteUsuario_ContratanteCod, AV62TFContratanteUsuario_ContratanteCod_To, AV65TFContratanteUsuario_ContratanteFan, AV66TFContratanteUsuario_ContratanteFan_Sel, AV69TFContratanteUsuario_ContratanteRaz, AV70TFContratanteUsuario_ContratanteRaz_Sel, AV73TFContratanteUsuario_UsuarioCod, AV74TFContratanteUsuario_UsuarioCod_To, AV77TFContratanteUsuario_UsuarioPessoaCod, AV78TFContratanteUsuario_UsuarioPessoaCod_To, AV81TFContratanteUsuario_UsuarioPessoaNom, AV82TFContratanteUsuario_UsuarioPessoaNom_Sel, AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace, AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace, AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace, AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace, AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace, AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV113Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A340ContratanteUsuario_ContratantePesCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA8E2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START8E2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202053021292278");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratanteusuario.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_NOME1", StringUtil.RTrim( AV56Usuario_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATANTEUSUARIO_USUARIOPESSOANOM1", StringUtil.RTrim( AV19ContratanteUsuario_UsuarioPessoaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_NOME2", StringUtil.RTrim( AV57Usuario_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATANTEUSUARIO_USUARIOPESSOANOM2", StringUtil.RTrim( AV24ContratanteUsuario_UsuarioPessoaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61TFContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62TFContratanteUsuario_ContratanteCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTEFAN", StringUtil.RTrim( AV65TFContratanteUsuario_ContratanteFan));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL", StringUtil.RTrim( AV66TFContratanteUsuario_ContratanteFan_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTERAZ", StringUtil.RTrim( AV69TFContratanteUsuario_ContratanteRaz));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL", StringUtil.RTrim( AV70TFContratanteUsuario_ContratanteRaz_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73TFContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTEUSUARIO_USUARIOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74TFContratanteUsuario_UsuarioCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77TFContratanteUsuario_UsuarioPessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV78TFContratanteUsuario_UsuarioPessoaCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM", StringUtil.RTrim( AV81TFContratanteUsuario_UsuarioPessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL", StringUtil.RTrim( AV82TFContratanteUsuario_UsuarioPessoaNom_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_73", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_73), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV87GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV84DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV84DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTEUSUARIO_CONTRATANTECODTITLEFILTERDATA", AV60ContratanteUsuario_ContratanteCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTEUSUARIO_CONTRATANTECODTITLEFILTERDATA", AV60ContratanteUsuario_ContratanteCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTEUSUARIO_CONTRATANTEFANTITLEFILTERDATA", AV64ContratanteUsuario_ContratanteFanTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTEUSUARIO_CONTRATANTEFANTITLEFILTERDATA", AV64ContratanteUsuario_ContratanteFanTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTEUSUARIO_CONTRATANTERAZTITLEFILTERDATA", AV68ContratanteUsuario_ContratanteRazTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTEUSUARIO_CONTRATANTERAZTITLEFILTERDATA", AV68ContratanteUsuario_ContratanteRazTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTEUSUARIO_USUARIOCODTITLEFILTERDATA", AV72ContratanteUsuario_UsuarioCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTEUSUARIO_USUARIOCODTITLEFILTERDATA", AV72ContratanteUsuario_UsuarioCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTEUSUARIO_USUARIOPESSOACODTITLEFILTERDATA", AV76ContratanteUsuario_UsuarioPessoaCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTEUSUARIO_USUARIOPESSOACODTITLEFILTERDATA", AV76ContratanteUsuario_UsuarioPessoaCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTEUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA", AV80ContratanteUsuario_UsuarioPessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTEUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA", AV80ContratanteUsuario_UsuarioPessoaNomTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV113Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV31DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV30DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTEPESCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A340ContratanteUsuario_ContratantePesCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Caption", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Tooltip", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Cls", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Filteredtext_set", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratanteusuario_contratantecod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratanteusuario_contratantecod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Sortedstatus", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Includefilter", StringUtil.BoolToStr( Ddo_contratanteusuario_contratantecod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Filtertype", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratanteusuario_contratantecod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratanteusuario_contratantecod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Sortasc", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Sortdsc", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Cleanfilter", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Rangefilterto", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Caption", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Tooltip", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Cls", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Filteredtext_set", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Selectedvalue_set", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Includesortasc", StringUtil.BoolToStr( Ddo_contratanteusuario_contratantefan_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Includesortdsc", StringUtil.BoolToStr( Ddo_contratanteusuario_contratantefan_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Sortedstatus", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Includefilter", StringUtil.BoolToStr( Ddo_contratanteusuario_contratantefan_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Filtertype", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Filterisrange", StringUtil.BoolToStr( Ddo_contratanteusuario_contratantefan_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Includedatalist", StringUtil.BoolToStr( Ddo_contratanteusuario_contratantefan_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Datalisttype", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Datalistproc", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratanteusuario_contratantefan_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Sortasc", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Sortdsc", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Loadingdata", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Cleanfilter", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Noresultsfound", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Searchbuttontext", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Caption", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Tooltip", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Cls", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Filteredtext_set", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Selectedvalue_set", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Includesortasc", StringUtil.BoolToStr( Ddo_contratanteusuario_contratanteraz_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Includesortdsc", StringUtil.BoolToStr( Ddo_contratanteusuario_contratanteraz_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Sortedstatus", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Includefilter", StringUtil.BoolToStr( Ddo_contratanteusuario_contratanteraz_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Filtertype", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Filterisrange", StringUtil.BoolToStr( Ddo_contratanteusuario_contratanteraz_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Includedatalist", StringUtil.BoolToStr( Ddo_contratanteusuario_contratanteraz_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Datalisttype", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Datalistproc", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratanteusuario_contratanteraz_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Sortasc", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Sortdsc", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Loadingdata", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Cleanfilter", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Noresultsfound", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Searchbuttontext", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Caption", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Tooltip", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Cls", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Sortedstatus", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Includefilter", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Filtertype", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Sortasc", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Sortdsc", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Cleanfilter", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Rangefilterto", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Caption", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Tooltip", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Cls", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Filteredtext_set", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariopessoacod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariopessoacod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Sortedstatus", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Includefilter", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariopessoacod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Filtertype", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariopessoacod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariopessoacod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Sortasc", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Sortdsc", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Cleanfilter", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Rangefilterto", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Caption", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Cls", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariopessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariopessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariopessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariopessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratanteusuario_usuariopessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratanteusuario_usuariopessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Activeeventkey", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contratanteusuario_contratantecod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Activeeventkey", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Filteredtext_get", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Selectedvalue_get", StringUtil.RTrim( Ddo_contratanteusuario_contratantefan_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Activeeventkey", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Filteredtext_get", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Selectedvalue_get", StringUtil.RTrim( Ddo_contratanteusuario_contratanteraz_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Activeeventkey", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contratanteusuario_usuariocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Activeeventkey", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE8E2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT8E2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratanteusuario.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratanteUsuario" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contratante Usuario" ;
      }

      protected void WB8E0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_8E2( true) ;
         }
         else
         {
            wb_table1_2_8E2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_8E2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(86, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratanteusuario_contratantecod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61TFContratanteUsuario_ContratanteCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV61TFContratanteUsuario_ContratanteCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratanteusuario_contratantecod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratanteusuario_contratantecod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratanteusuario_contratantecod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62TFContratanteUsuario_ContratanteCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV62TFContratanteUsuario_ContratanteCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratanteusuario_contratantecod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratanteusuario_contratantecod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratanteusuario_contratantefan_Internalname, StringUtil.RTrim( AV65TFContratanteUsuario_ContratanteFan), StringUtil.RTrim( context.localUtil.Format( AV65TFContratanteUsuario_ContratanteFan, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratanteusuario_contratantefan_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratanteusuario_contratantefan_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratanteusuario_contratantefan_sel_Internalname, StringUtil.RTrim( AV66TFContratanteUsuario_ContratanteFan_Sel), StringUtil.RTrim( context.localUtil.Format( AV66TFContratanteUsuario_ContratanteFan_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratanteusuario_contratantefan_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratanteusuario_contratantefan_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratanteusuario_contratanteraz_Internalname, StringUtil.RTrim( AV69TFContratanteUsuario_ContratanteRaz), StringUtil.RTrim( context.localUtil.Format( AV69TFContratanteUsuario_ContratanteRaz, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratanteusuario_contratanteraz_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratanteusuario_contratanteraz_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratanteusuario_contratanteraz_sel_Internalname, StringUtil.RTrim( AV70TFContratanteUsuario_ContratanteRaz_Sel), StringUtil.RTrim( context.localUtil.Format( AV70TFContratanteUsuario_ContratanteRaz_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratanteusuario_contratanteraz_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratanteusuario_contratanteraz_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratanteusuario_usuariocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73TFContratanteUsuario_UsuarioCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV73TFContratanteUsuario_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratanteusuario_usuariocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratanteusuario_usuariocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratanteusuario_usuariocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74TFContratanteUsuario_UsuarioCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV74TFContratanteUsuario_UsuarioCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratanteusuario_usuariocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratanteusuario_usuariocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratanteusuario_usuariopessoacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77TFContratanteUsuario_UsuarioPessoaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV77TFContratanteUsuario_UsuarioPessoaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratanteusuario_usuariopessoacod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratanteusuario_usuariopessoacod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratanteusuario_usuariopessoacod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV78TFContratanteUsuario_UsuarioPessoaCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV78TFContratanteUsuario_UsuarioPessoaCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratanteusuario_usuariopessoacod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratanteusuario_usuariopessoacod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratanteusuario_usuariopessoanom_Internalname, StringUtil.RTrim( AV81TFContratanteUsuario_UsuarioPessoaNom), StringUtil.RTrim( context.localUtil.Format( AV81TFContratanteUsuario_UsuarioPessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratanteusuario_usuariopessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratanteusuario_usuariopessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratanteusuario_usuariopessoanom_sel_Internalname, StringUtil.RTrim( AV82TFContratanteUsuario_UsuarioPessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV82TFContratanteUsuario_UsuarioPessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratanteusuario_usuariopessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratanteusuario_usuariopessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratanteUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTEUSUARIO_CONTRATANTECODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratanteusuario_contratantecodtitlecontrolidtoreplace_Internalname, AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", 0, edtavDdo_contratanteusuario_contratantecodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratanteUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTEUSUARIO_CONTRATANTEFANContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratanteusuario_contratantefantitlecontrolidtoreplace_Internalname, AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", 0, edtavDdo_contratanteusuario_contratantefantitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratanteUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTEUSUARIO_CONTRATANTERAZContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratanteusuario_contratanteraztitlecontrolidtoreplace_Internalname, AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", 0, edtavDdo_contratanteusuario_contratanteraztitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratanteUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTEUSUARIO_USUARIOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratanteusuario_usuariocodtitlecontrolidtoreplace_Internalname, AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, edtavDdo_contratanteusuario_usuariocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratanteUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTEUSUARIO_USUARIOPESSOACODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratanteusuario_usuariopessoacodtitlecontrolidtoreplace_Internalname, AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavDdo_contratanteusuario_usuariopessoacodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratanteUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratanteusuario_usuariopessoanomtitlecontrolidtoreplace_Internalname, AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavDdo_contratanteusuario_usuariopessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratanteUsuario.htm");
         }
         wbLoad = true;
      }

      protected void START8E2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contratante Usuario", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP8E0( ) ;
      }

      protected void WS8E2( )
      {
         START8E2( ) ;
         EVT8E2( ) ;
      }

      protected void EVT8E2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E118E2 */
                              E118E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E128E2 */
                              E128E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E138E2 */
                              E138E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E148E2 */
                              E148E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTEUSUARIO_USUARIOCOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E158E2 */
                              E158E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E168E2 */
                              E168E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E178E2 */
                              E178E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E188E2 */
                              E188E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E198E2 */
                              E198E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E208E2 */
                              E208E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E218E2 */
                              E218E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E228E2 */
                              E228E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E238E2 */
                              E238E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E248E2 */
                              E248E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E258E2 */
                              E258E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_73_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
                              SubsflControlProps_732( ) ;
                              AV32Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)) ? AV111Update_GXI : context.convertURL( context.PathToRelativeUrl( AV32Update))));
                              AV33Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)) ? AV112Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV33Delete))));
                              A63ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( edtContratanteUsuario_ContratanteCod_Internalname), ",", "."));
                              A65ContratanteUsuario_ContratanteFan = StringUtil.Upper( cgiGet( edtContratanteUsuario_ContratanteFan_Internalname));
                              n65ContratanteUsuario_ContratanteFan = false;
                              A64ContratanteUsuario_ContratanteRaz = StringUtil.Upper( cgiGet( edtContratanteUsuario_ContratanteRaz_Internalname));
                              n64ContratanteUsuario_ContratanteRaz = false;
                              A60ContratanteUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContratanteUsuario_UsuarioCod_Internalname), ",", "."));
                              A61ContratanteUsuario_UsuarioPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratanteUsuario_UsuarioPessoaCod_Internalname), ",", "."));
                              n61ContratanteUsuario_UsuarioPessoaCod = false;
                              A62ContratanteUsuario_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtContratanteUsuario_UsuarioPessoaNom_Internalname));
                              n62ContratanteUsuario_UsuarioPessoaNom = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E268E2 */
                                    E268E2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E278E2 */
                                    E278E2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E288E2 */
                                    E288E2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_NOME1"), AV56Usuario_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratanteusuario_usuariopessoanom1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATANTEUSUARIO_USUARIOPESSOANOM1"), AV19ContratanteUsuario_UsuarioPessoaNom1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_NOME2"), AV57Usuario_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratanteusuario_usuariopessoanom2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATANTEUSUARIO_USUARIOPESSOANOM2"), AV24ContratanteUsuario_UsuarioPessoaNom2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratanteusuario_contratantecod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTECOD"), ",", ".") != Convert.ToDecimal( AV61TFContratanteUsuario_ContratanteCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratanteusuario_contratantecod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO"), ",", ".") != Convert.ToDecimal( AV62TFContratanteUsuario_ContratanteCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratanteusuario_contratantefan Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTEFAN"), AV65TFContratanteUsuario_ContratanteFan) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratanteusuario_contratantefan_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL"), AV66TFContratanteUsuario_ContratanteFan_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratanteusuario_contratanteraz Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTERAZ"), AV69TFContratanteUsuario_ContratanteRaz) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratanteusuario_contratanteraz_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL"), AV70TFContratanteUsuario_ContratanteRaz_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratanteusuario_usuariocod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATANTEUSUARIO_USUARIOCOD"), ",", ".") != Convert.ToDecimal( AV73TFContratanteUsuario_UsuarioCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratanteusuario_usuariocod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATANTEUSUARIO_USUARIOCOD_TO"), ",", ".") != Convert.ToDecimal( AV74TFContratanteUsuario_UsuarioCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratanteusuario_usuariopessoacod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD"), ",", ".") != Convert.ToDecimal( AV77TFContratanteUsuario_UsuarioPessoaCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratanteusuario_usuariopessoacod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO"), ",", ".") != Convert.ToDecimal( AV78TFContratanteUsuario_UsuarioPessoaCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratanteusuario_usuariopessoanom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM"), AV81TFContratanteUsuario_UsuarioPessoaNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratanteusuario_usuariopessoanom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL"), AV82TFContratanteUsuario_UsuarioPessoaNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE8E2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA8E2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("USUARIO_NOME", "Usu�rio", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATANTEUSUARIO_USUARIOPESSOANOM", "Pessoa", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("USUARIO_NOME", "Usu�rio", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATANTEUSUARIO_USUARIOPESSOANOM", "Pessoa", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_732( ) ;
         while ( nGXsfl_73_idx <= nRC_GXsfl_73 )
         {
            sendrow_732( ) ;
            nGXsfl_73_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_732( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV56Usuario_Nome1 ,
                                       String AV19ContratanteUsuario_UsuarioPessoaNom1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       String AV57Usuario_Nome2 ,
                                       String AV24ContratanteUsuario_UsuarioPessoaNom2 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       int AV61TFContratanteUsuario_ContratanteCod ,
                                       int AV62TFContratanteUsuario_ContratanteCod_To ,
                                       String AV65TFContratanteUsuario_ContratanteFan ,
                                       String AV66TFContratanteUsuario_ContratanteFan_Sel ,
                                       String AV69TFContratanteUsuario_ContratanteRaz ,
                                       String AV70TFContratanteUsuario_ContratanteRaz_Sel ,
                                       int AV73TFContratanteUsuario_UsuarioCod ,
                                       int AV74TFContratanteUsuario_UsuarioCod_To ,
                                       int AV77TFContratanteUsuario_UsuarioPessoaCod ,
                                       int AV78TFContratanteUsuario_UsuarioPessoaCod_To ,
                                       String AV81TFContratanteUsuario_UsuarioPessoaNom ,
                                       String AV82TFContratanteUsuario_UsuarioPessoaNom_Sel ,
                                       String AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace ,
                                       String AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace ,
                                       String AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace ,
                                       String AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace ,
                                       String AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace ,
                                       String AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace ,
                                       String AV113Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV31DynamicFiltersIgnoreFirst ,
                                       bool AV30DynamicFiltersRemoving ,
                                       int A63ContratanteUsuario_ContratanteCod ,
                                       int A60ContratanteUsuario_UsuarioCod ,
                                       int A340ContratanteUsuario_ContratantePesCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF8E2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTEUSUARIO_CONTRATANTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A63ContratanteUsuario_ContratanteCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTEUSUARIO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A60ContratanteUsuario_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF8E2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV113Pgmname = "WWContratanteUsuario";
         context.Gx_err = 0;
      }

      protected void RF8E2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 73;
         /* Execute user event: E278E2 */
         E278E2 ();
         nGXsfl_73_idx = 1;
         sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
         SubsflControlProps_732( ) ;
         nGXsfl_73_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_732( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1 ,
                                                 AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 ,
                                                 AV92WWContratanteUsuarioDS_3_Usuario_nome1 ,
                                                 AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ,
                                                 AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 ,
                                                 AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2 ,
                                                 AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 ,
                                                 AV97WWContratanteUsuarioDS_8_Usuario_nome2 ,
                                                 AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ,
                                                 AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod ,
                                                 AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to ,
                                                 AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel ,
                                                 AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ,
                                                 AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel ,
                                                 AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ,
                                                 AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod ,
                                                 AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to ,
                                                 AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod ,
                                                 AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to ,
                                                 AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel ,
                                                 AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ,
                                                 A2Usuario_Nome ,
                                                 A62ContratanteUsuario_UsuarioPessoaNom ,
                                                 A63ContratanteUsuario_ContratanteCod ,
                                                 A65ContratanteUsuario_ContratanteFan ,
                                                 A64ContratanteUsuario_ContratanteRaz ,
                                                 A60ContratanteUsuario_UsuarioCod ,
                                                 A61ContratanteUsuario_UsuarioPessoaCod ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV92WWContratanteUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWContratanteUsuarioDS_3_Usuario_nome1), 50, "%");
            lV92WWContratanteUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWContratanteUsuarioDS_3_Usuario_nome1), 50, "%");
            lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1), 100, "%");
            lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1), 100, "%");
            lV97WWContratanteUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV97WWContratanteUsuarioDS_8_Usuario_nome2), 50, "%");
            lV97WWContratanteUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV97WWContratanteUsuarioDS_8_Usuario_nome2), 50, "%");
            lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2), 100, "%");
            lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2), 100, "%");
            lV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = StringUtil.PadR( StringUtil.RTrim( AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan), 100, "%");
            lV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = StringUtil.PadR( StringUtil.RTrim( AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz), 100, "%");
            lV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = StringUtil.PadR( StringUtil.RTrim( AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom), 100, "%");
            /* Using cursor H008E2 */
            pr_default.execute(0, new Object[] {lV92WWContratanteUsuarioDS_3_Usuario_nome1, lV92WWContratanteUsuarioDS_3_Usuario_nome1, lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1, lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1, lV97WWContratanteUsuarioDS_8_Usuario_nome2, lV97WWContratanteUsuarioDS_8_Usuario_nome2, lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2, lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2, AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod, AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to, lV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan, AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel, lV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz, AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel, AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod, AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to, AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod, AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to, lV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom, AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_73_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2Usuario_Nome = H008E2_A2Usuario_Nome[0];
               n2Usuario_Nome = H008E2_n2Usuario_Nome[0];
               A340ContratanteUsuario_ContratantePesCod = H008E2_A340ContratanteUsuario_ContratantePesCod[0];
               n340ContratanteUsuario_ContratantePesCod = H008E2_n340ContratanteUsuario_ContratantePesCod[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H008E2_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H008E2_n62ContratanteUsuario_UsuarioPessoaNom[0];
               A61ContratanteUsuario_UsuarioPessoaCod = H008E2_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H008E2_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A60ContratanteUsuario_UsuarioCod = H008E2_A60ContratanteUsuario_UsuarioCod[0];
               A64ContratanteUsuario_ContratanteRaz = H008E2_A64ContratanteUsuario_ContratanteRaz[0];
               n64ContratanteUsuario_ContratanteRaz = H008E2_n64ContratanteUsuario_ContratanteRaz[0];
               A65ContratanteUsuario_ContratanteFan = H008E2_A65ContratanteUsuario_ContratanteFan[0];
               n65ContratanteUsuario_ContratanteFan = H008E2_n65ContratanteUsuario_ContratanteFan[0];
               A63ContratanteUsuario_ContratanteCod = H008E2_A63ContratanteUsuario_ContratanteCod[0];
               A2Usuario_Nome = H008E2_A2Usuario_Nome[0];
               n2Usuario_Nome = H008E2_n2Usuario_Nome[0];
               A61ContratanteUsuario_UsuarioPessoaCod = H008E2_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H008E2_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H008E2_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H008E2_n62ContratanteUsuario_UsuarioPessoaNom[0];
               A340ContratanteUsuario_ContratantePesCod = H008E2_A340ContratanteUsuario_ContratantePesCod[0];
               n340ContratanteUsuario_ContratantePesCod = H008E2_n340ContratanteUsuario_ContratantePesCod[0];
               A65ContratanteUsuario_ContratanteFan = H008E2_A65ContratanteUsuario_ContratanteFan[0];
               n65ContratanteUsuario_ContratanteFan = H008E2_n65ContratanteUsuario_ContratanteFan[0];
               A64ContratanteUsuario_ContratanteRaz = H008E2_A64ContratanteUsuario_ContratanteRaz[0];
               n64ContratanteUsuario_ContratanteRaz = H008E2_n64ContratanteUsuario_ContratanteRaz[0];
               /* Execute user event: E288E2 */
               E288E2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 73;
            WB8E0( ) ;
         }
         nGXsfl_73_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV92WWContratanteUsuarioDS_3_Usuario_nome1 = AV56Usuario_Nome1;
         AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = AV19ContratanteUsuario_UsuarioPessoaNom1;
         AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWContratanteUsuarioDS_8_Usuario_nome2 = AV57Usuario_Nome2;
         AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = AV24ContratanteUsuario_UsuarioPessoaNom2;
         AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod = AV61TFContratanteUsuario_ContratanteCod;
         AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to = AV62TFContratanteUsuario_ContratanteCod_To;
         AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = AV65TFContratanteUsuario_ContratanteFan;
         AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel = AV66TFContratanteUsuario_ContratanteFan_Sel;
         AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = AV69TFContratanteUsuario_ContratanteRaz;
         AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel = AV70TFContratanteUsuario_ContratanteRaz_Sel;
         AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod = AV73TFContratanteUsuario_UsuarioCod;
         AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to = AV74TFContratanteUsuario_UsuarioCod_To;
         AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod = AV77TFContratanteUsuario_UsuarioPessoaCod;
         AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to = AV78TFContratanteUsuario_UsuarioPessoaCod_To;
         AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = AV81TFContratanteUsuario_UsuarioPessoaNom;
         AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel = AV82TFContratanteUsuario_UsuarioPessoaNom_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1 ,
                                              AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 ,
                                              AV92WWContratanteUsuarioDS_3_Usuario_nome1 ,
                                              AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ,
                                              AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 ,
                                              AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2 ,
                                              AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 ,
                                              AV97WWContratanteUsuarioDS_8_Usuario_nome2 ,
                                              AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ,
                                              AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod ,
                                              AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to ,
                                              AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel ,
                                              AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ,
                                              AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel ,
                                              AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ,
                                              AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod ,
                                              AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to ,
                                              AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod ,
                                              AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to ,
                                              AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel ,
                                              AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ,
                                              A2Usuario_Nome ,
                                              A62ContratanteUsuario_UsuarioPessoaNom ,
                                              A63ContratanteUsuario_ContratanteCod ,
                                              A65ContratanteUsuario_ContratanteFan ,
                                              A64ContratanteUsuario_ContratanteRaz ,
                                              A60ContratanteUsuario_UsuarioCod ,
                                              A61ContratanteUsuario_UsuarioPessoaCod ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV92WWContratanteUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWContratanteUsuarioDS_3_Usuario_nome1), 50, "%");
         lV92WWContratanteUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWContratanteUsuarioDS_3_Usuario_nome1), 50, "%");
         lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1), 100, "%");
         lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1), 100, "%");
         lV97WWContratanteUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV97WWContratanteUsuarioDS_8_Usuario_nome2), 50, "%");
         lV97WWContratanteUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV97WWContratanteUsuarioDS_8_Usuario_nome2), 50, "%");
         lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2), 100, "%");
         lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2), 100, "%");
         lV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = StringUtil.PadR( StringUtil.RTrim( AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan), 100, "%");
         lV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = StringUtil.PadR( StringUtil.RTrim( AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz), 100, "%");
         lV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = StringUtil.PadR( StringUtil.RTrim( AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom), 100, "%");
         /* Using cursor H008E3 */
         pr_default.execute(1, new Object[] {lV92WWContratanteUsuarioDS_3_Usuario_nome1, lV92WWContratanteUsuarioDS_3_Usuario_nome1, lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1, lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1, lV97WWContratanteUsuarioDS_8_Usuario_nome2, lV97WWContratanteUsuarioDS_8_Usuario_nome2, lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2, lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2, AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod, AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to, lV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan, AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel, lV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz, AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel, AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod, AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to, AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod, AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to, lV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom, AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel});
         GRID_nRecordCount = H008E3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV92WWContratanteUsuarioDS_3_Usuario_nome1 = AV56Usuario_Nome1;
         AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = AV19ContratanteUsuario_UsuarioPessoaNom1;
         AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWContratanteUsuarioDS_8_Usuario_nome2 = AV57Usuario_Nome2;
         AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = AV24ContratanteUsuario_UsuarioPessoaNom2;
         AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod = AV61TFContratanteUsuario_ContratanteCod;
         AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to = AV62TFContratanteUsuario_ContratanteCod_To;
         AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = AV65TFContratanteUsuario_ContratanteFan;
         AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel = AV66TFContratanteUsuario_ContratanteFan_Sel;
         AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = AV69TFContratanteUsuario_ContratanteRaz;
         AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel = AV70TFContratanteUsuario_ContratanteRaz_Sel;
         AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod = AV73TFContratanteUsuario_UsuarioCod;
         AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to = AV74TFContratanteUsuario_UsuarioCod_To;
         AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod = AV77TFContratanteUsuario_UsuarioPessoaCod;
         AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to = AV78TFContratanteUsuario_UsuarioPessoaCod_To;
         AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = AV81TFContratanteUsuario_UsuarioPessoaNom;
         AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel = AV82TFContratanteUsuario_UsuarioPessoaNom_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV56Usuario_Nome1, AV19ContratanteUsuario_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV57Usuario_Nome2, AV24ContratanteUsuario_UsuarioPessoaNom2, AV20DynamicFiltersEnabled2, AV61TFContratanteUsuario_ContratanteCod, AV62TFContratanteUsuario_ContratanteCod_To, AV65TFContratanteUsuario_ContratanteFan, AV66TFContratanteUsuario_ContratanteFan_Sel, AV69TFContratanteUsuario_ContratanteRaz, AV70TFContratanteUsuario_ContratanteRaz_Sel, AV73TFContratanteUsuario_UsuarioCod, AV74TFContratanteUsuario_UsuarioCod_To, AV77TFContratanteUsuario_UsuarioPessoaCod, AV78TFContratanteUsuario_UsuarioPessoaCod_To, AV81TFContratanteUsuario_UsuarioPessoaNom, AV82TFContratanteUsuario_UsuarioPessoaNom_Sel, AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace, AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace, AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace, AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace, AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace, AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV113Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A340ContratanteUsuario_ContratantePesCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV92WWContratanteUsuarioDS_3_Usuario_nome1 = AV56Usuario_Nome1;
         AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = AV19ContratanteUsuario_UsuarioPessoaNom1;
         AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWContratanteUsuarioDS_8_Usuario_nome2 = AV57Usuario_Nome2;
         AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = AV24ContratanteUsuario_UsuarioPessoaNom2;
         AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod = AV61TFContratanteUsuario_ContratanteCod;
         AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to = AV62TFContratanteUsuario_ContratanteCod_To;
         AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = AV65TFContratanteUsuario_ContratanteFan;
         AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel = AV66TFContratanteUsuario_ContratanteFan_Sel;
         AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = AV69TFContratanteUsuario_ContratanteRaz;
         AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel = AV70TFContratanteUsuario_ContratanteRaz_Sel;
         AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod = AV73TFContratanteUsuario_UsuarioCod;
         AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to = AV74TFContratanteUsuario_UsuarioCod_To;
         AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod = AV77TFContratanteUsuario_UsuarioPessoaCod;
         AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to = AV78TFContratanteUsuario_UsuarioPessoaCod_To;
         AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = AV81TFContratanteUsuario_UsuarioPessoaNom;
         AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel = AV82TFContratanteUsuario_UsuarioPessoaNom_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV56Usuario_Nome1, AV19ContratanteUsuario_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV57Usuario_Nome2, AV24ContratanteUsuario_UsuarioPessoaNom2, AV20DynamicFiltersEnabled2, AV61TFContratanteUsuario_ContratanteCod, AV62TFContratanteUsuario_ContratanteCod_To, AV65TFContratanteUsuario_ContratanteFan, AV66TFContratanteUsuario_ContratanteFan_Sel, AV69TFContratanteUsuario_ContratanteRaz, AV70TFContratanteUsuario_ContratanteRaz_Sel, AV73TFContratanteUsuario_UsuarioCod, AV74TFContratanteUsuario_UsuarioCod_To, AV77TFContratanteUsuario_UsuarioPessoaCod, AV78TFContratanteUsuario_UsuarioPessoaCod_To, AV81TFContratanteUsuario_UsuarioPessoaNom, AV82TFContratanteUsuario_UsuarioPessoaNom_Sel, AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace, AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace, AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace, AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace, AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace, AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV113Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A340ContratanteUsuario_ContratantePesCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV92WWContratanteUsuarioDS_3_Usuario_nome1 = AV56Usuario_Nome1;
         AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = AV19ContratanteUsuario_UsuarioPessoaNom1;
         AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWContratanteUsuarioDS_8_Usuario_nome2 = AV57Usuario_Nome2;
         AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = AV24ContratanteUsuario_UsuarioPessoaNom2;
         AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod = AV61TFContratanteUsuario_ContratanteCod;
         AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to = AV62TFContratanteUsuario_ContratanteCod_To;
         AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = AV65TFContratanteUsuario_ContratanteFan;
         AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel = AV66TFContratanteUsuario_ContratanteFan_Sel;
         AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = AV69TFContratanteUsuario_ContratanteRaz;
         AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel = AV70TFContratanteUsuario_ContratanteRaz_Sel;
         AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod = AV73TFContratanteUsuario_UsuarioCod;
         AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to = AV74TFContratanteUsuario_UsuarioCod_To;
         AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod = AV77TFContratanteUsuario_UsuarioPessoaCod;
         AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to = AV78TFContratanteUsuario_UsuarioPessoaCod_To;
         AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = AV81TFContratanteUsuario_UsuarioPessoaNom;
         AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel = AV82TFContratanteUsuario_UsuarioPessoaNom_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV56Usuario_Nome1, AV19ContratanteUsuario_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV57Usuario_Nome2, AV24ContratanteUsuario_UsuarioPessoaNom2, AV20DynamicFiltersEnabled2, AV61TFContratanteUsuario_ContratanteCod, AV62TFContratanteUsuario_ContratanteCod_To, AV65TFContratanteUsuario_ContratanteFan, AV66TFContratanteUsuario_ContratanteFan_Sel, AV69TFContratanteUsuario_ContratanteRaz, AV70TFContratanteUsuario_ContratanteRaz_Sel, AV73TFContratanteUsuario_UsuarioCod, AV74TFContratanteUsuario_UsuarioCod_To, AV77TFContratanteUsuario_UsuarioPessoaCod, AV78TFContratanteUsuario_UsuarioPessoaCod_To, AV81TFContratanteUsuario_UsuarioPessoaNom, AV82TFContratanteUsuario_UsuarioPessoaNom_Sel, AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace, AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace, AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace, AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace, AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace, AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV113Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A340ContratanteUsuario_ContratantePesCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV92WWContratanteUsuarioDS_3_Usuario_nome1 = AV56Usuario_Nome1;
         AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = AV19ContratanteUsuario_UsuarioPessoaNom1;
         AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWContratanteUsuarioDS_8_Usuario_nome2 = AV57Usuario_Nome2;
         AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = AV24ContratanteUsuario_UsuarioPessoaNom2;
         AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod = AV61TFContratanteUsuario_ContratanteCod;
         AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to = AV62TFContratanteUsuario_ContratanteCod_To;
         AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = AV65TFContratanteUsuario_ContratanteFan;
         AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel = AV66TFContratanteUsuario_ContratanteFan_Sel;
         AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = AV69TFContratanteUsuario_ContratanteRaz;
         AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel = AV70TFContratanteUsuario_ContratanteRaz_Sel;
         AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod = AV73TFContratanteUsuario_UsuarioCod;
         AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to = AV74TFContratanteUsuario_UsuarioCod_To;
         AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod = AV77TFContratanteUsuario_UsuarioPessoaCod;
         AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to = AV78TFContratanteUsuario_UsuarioPessoaCod_To;
         AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = AV81TFContratanteUsuario_UsuarioPessoaNom;
         AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel = AV82TFContratanteUsuario_UsuarioPessoaNom_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV56Usuario_Nome1, AV19ContratanteUsuario_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV57Usuario_Nome2, AV24ContratanteUsuario_UsuarioPessoaNom2, AV20DynamicFiltersEnabled2, AV61TFContratanteUsuario_ContratanteCod, AV62TFContratanteUsuario_ContratanteCod_To, AV65TFContratanteUsuario_ContratanteFan, AV66TFContratanteUsuario_ContratanteFan_Sel, AV69TFContratanteUsuario_ContratanteRaz, AV70TFContratanteUsuario_ContratanteRaz_Sel, AV73TFContratanteUsuario_UsuarioCod, AV74TFContratanteUsuario_UsuarioCod_To, AV77TFContratanteUsuario_UsuarioPessoaCod, AV78TFContratanteUsuario_UsuarioPessoaCod_To, AV81TFContratanteUsuario_UsuarioPessoaNom, AV82TFContratanteUsuario_UsuarioPessoaNom_Sel, AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace, AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace, AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace, AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace, AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace, AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV113Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A340ContratanteUsuario_ContratantePesCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV92WWContratanteUsuarioDS_3_Usuario_nome1 = AV56Usuario_Nome1;
         AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = AV19ContratanteUsuario_UsuarioPessoaNom1;
         AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWContratanteUsuarioDS_8_Usuario_nome2 = AV57Usuario_Nome2;
         AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = AV24ContratanteUsuario_UsuarioPessoaNom2;
         AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod = AV61TFContratanteUsuario_ContratanteCod;
         AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to = AV62TFContratanteUsuario_ContratanteCod_To;
         AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = AV65TFContratanteUsuario_ContratanteFan;
         AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel = AV66TFContratanteUsuario_ContratanteFan_Sel;
         AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = AV69TFContratanteUsuario_ContratanteRaz;
         AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel = AV70TFContratanteUsuario_ContratanteRaz_Sel;
         AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod = AV73TFContratanteUsuario_UsuarioCod;
         AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to = AV74TFContratanteUsuario_UsuarioCod_To;
         AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod = AV77TFContratanteUsuario_UsuarioPessoaCod;
         AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to = AV78TFContratanteUsuario_UsuarioPessoaCod_To;
         AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = AV81TFContratanteUsuario_UsuarioPessoaNom;
         AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel = AV82TFContratanteUsuario_UsuarioPessoaNom_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV56Usuario_Nome1, AV19ContratanteUsuario_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV57Usuario_Nome2, AV24ContratanteUsuario_UsuarioPessoaNom2, AV20DynamicFiltersEnabled2, AV61TFContratanteUsuario_ContratanteCod, AV62TFContratanteUsuario_ContratanteCod_To, AV65TFContratanteUsuario_ContratanteFan, AV66TFContratanteUsuario_ContratanteFan_Sel, AV69TFContratanteUsuario_ContratanteRaz, AV70TFContratanteUsuario_ContratanteRaz_Sel, AV73TFContratanteUsuario_UsuarioCod, AV74TFContratanteUsuario_UsuarioCod_To, AV77TFContratanteUsuario_UsuarioPessoaCod, AV78TFContratanteUsuario_UsuarioPessoaCod_To, AV81TFContratanteUsuario_UsuarioPessoaNom, AV82TFContratanteUsuario_UsuarioPessoaNom_Sel, AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace, AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace, AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace, AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace, AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace, AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV113Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A340ContratanteUsuario_ContratantePesCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUP8E0( )
      {
         /* Before Start, stand alone formulas. */
         AV113Pgmname = "WWContratanteUsuario";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E268E2 */
         E268E2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV84DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTEUSUARIO_CONTRATANTECODTITLEFILTERDATA"), AV60ContratanteUsuario_ContratanteCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTEUSUARIO_CONTRATANTEFANTITLEFILTERDATA"), AV64ContratanteUsuario_ContratanteFanTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTEUSUARIO_CONTRATANTERAZTITLEFILTERDATA"), AV68ContratanteUsuario_ContratanteRazTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTEUSUARIO_USUARIOCODTITLEFILTERDATA"), AV72ContratanteUsuario_UsuarioCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTEUSUARIO_USUARIOPESSOACODTITLEFILTERDATA"), AV76ContratanteUsuario_UsuarioPessoaCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTEUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA"), AV80ContratanteUsuario_UsuarioPessoaNomTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV56Usuario_Nome1 = StringUtil.Upper( cgiGet( edtavUsuario_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Usuario_Nome1", AV56Usuario_Nome1);
            AV19ContratanteUsuario_UsuarioPessoaNom1 = StringUtil.Upper( cgiGet( edtavContratanteusuario_usuariopessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratanteUsuario_UsuarioPessoaNom1", AV19ContratanteUsuario_UsuarioPessoaNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV57Usuario_Nome2 = StringUtil.Upper( cgiGet( edtavUsuario_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Usuario_Nome2", AV57Usuario_Nome2);
            AV24ContratanteUsuario_UsuarioPessoaNom2 = StringUtil.Upper( cgiGet( edtavContratanteusuario_usuariopessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratanteUsuario_UsuarioPessoaNom2", AV24ContratanteUsuario_UsuarioPessoaNom2);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_contratantecod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_contratantecod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATANTEUSUARIO_CONTRATANTECOD");
               GX_FocusControl = edtavTfcontratanteusuario_contratantecod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61TFContratanteUsuario_ContratanteCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContratanteUsuario_ContratanteCod), 6, 0)));
            }
            else
            {
               AV61TFContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_contratantecod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContratanteUsuario_ContratanteCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_contratantecod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_contratantecod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO");
               GX_FocusControl = edtavTfcontratanteusuario_contratantecod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62TFContratanteUsuario_ContratanteCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratanteUsuario_ContratanteCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContratanteUsuario_ContratanteCod_To), 6, 0)));
            }
            else
            {
               AV62TFContratanteUsuario_ContratanteCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_contratantecod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratanteUsuario_ContratanteCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContratanteUsuario_ContratanteCod_To), 6, 0)));
            }
            AV65TFContratanteUsuario_ContratanteFan = StringUtil.Upper( cgiGet( edtavTfcontratanteusuario_contratantefan_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratanteUsuario_ContratanteFan", AV65TFContratanteUsuario_ContratanteFan);
            AV66TFContratanteUsuario_ContratanteFan_Sel = StringUtil.Upper( cgiGet( edtavTfcontratanteusuario_contratantefan_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratanteUsuario_ContratanteFan_Sel", AV66TFContratanteUsuario_ContratanteFan_Sel);
            AV69TFContratanteUsuario_ContratanteRaz = StringUtil.Upper( cgiGet( edtavTfcontratanteusuario_contratanteraz_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratanteUsuario_ContratanteRaz", AV69TFContratanteUsuario_ContratanteRaz);
            AV70TFContratanteUsuario_ContratanteRaz_Sel = StringUtil.Upper( cgiGet( edtavTfcontratanteusuario_contratanteraz_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratanteUsuario_ContratanteRaz_Sel", AV70TFContratanteUsuario_ContratanteRaz_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_usuariocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_usuariocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATANTEUSUARIO_USUARIOCOD");
               GX_FocusControl = edtavTfcontratanteusuario_usuariocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV73TFContratanteUsuario_UsuarioCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73TFContratanteUsuario_UsuarioCod), 6, 0)));
            }
            else
            {
               AV73TFContratanteUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_usuariocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73TFContratanteUsuario_UsuarioCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_usuariocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_usuariocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATANTEUSUARIO_USUARIOCOD_TO");
               GX_FocusControl = edtavTfcontratanteusuario_usuariocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV74TFContratanteUsuario_UsuarioCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratanteUsuario_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74TFContratanteUsuario_UsuarioCod_To), 6, 0)));
            }
            else
            {
               AV74TFContratanteUsuario_UsuarioCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_usuariocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratanteUsuario_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74TFContratanteUsuario_UsuarioCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_usuariopessoacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_usuariopessoacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD");
               GX_FocusControl = edtavTfcontratanteusuario_usuariopessoacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV77TFContratanteUsuario_UsuarioPessoaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContratanteUsuario_UsuarioPessoaCod), 6, 0)));
            }
            else
            {
               AV77TFContratanteUsuario_UsuarioPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_usuariopessoacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContratanteUsuario_UsuarioPessoaCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_usuariopessoacod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_usuariopessoacod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO");
               GX_FocusControl = edtavTfcontratanteusuario_usuariopessoacod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV78TFContratanteUsuario_UsuarioPessoaCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContratanteUsuario_UsuarioPessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78TFContratanteUsuario_UsuarioPessoaCod_To), 6, 0)));
            }
            else
            {
               AV78TFContratanteUsuario_UsuarioPessoaCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratanteusuario_usuariopessoacod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContratanteUsuario_UsuarioPessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78TFContratanteUsuario_UsuarioPessoaCod_To), 6, 0)));
            }
            AV81TFContratanteUsuario_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtavTfcontratanteusuario_usuariopessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratanteUsuario_UsuarioPessoaNom", AV81TFContratanteUsuario_UsuarioPessoaNom);
            AV82TFContratanteUsuario_UsuarioPessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratanteusuario_usuariopessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratanteUsuario_UsuarioPessoaNom_Sel", AV82TFContratanteUsuario_UsuarioPessoaNom_Sel);
            AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace = cgiGet( edtavDdo_contratanteusuario_contratantecodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace", AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace);
            AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace = cgiGet( edtavDdo_contratanteusuario_contratantefantitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace", AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace);
            AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace = cgiGet( edtavDdo_contratanteusuario_contratanteraztitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace", AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace);
            AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace = cgiGet( edtavDdo_contratanteusuario_usuariocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace", AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace);
            AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace = cgiGet( edtavDdo_contratanteusuario_usuariopessoacodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace", AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace);
            AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contratanteusuario_usuariopessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace", AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_73 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_73"), ",", "."));
            AV86GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV87GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratanteusuario_contratantecod_Caption = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Caption");
            Ddo_contratanteusuario_contratantecod_Tooltip = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Tooltip");
            Ddo_contratanteusuario_contratantecod_Cls = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Cls");
            Ddo_contratanteusuario_contratantecod_Filteredtext_set = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Filteredtext_set");
            Ddo_contratanteusuario_contratantecod_Filteredtextto_set = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Filteredtextto_set");
            Ddo_contratanteusuario_contratantecod_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Dropdownoptionstype");
            Ddo_contratanteusuario_contratantecod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Titlecontrolidtoreplace");
            Ddo_contratanteusuario_contratantecod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Includesortasc"));
            Ddo_contratanteusuario_contratantecod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Includesortdsc"));
            Ddo_contratanteusuario_contratantecod_Sortedstatus = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Sortedstatus");
            Ddo_contratanteusuario_contratantecod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Includefilter"));
            Ddo_contratanteusuario_contratantecod_Filtertype = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Filtertype");
            Ddo_contratanteusuario_contratantecod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Filterisrange"));
            Ddo_contratanteusuario_contratantecod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Includedatalist"));
            Ddo_contratanteusuario_contratantecod_Sortasc = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Sortasc");
            Ddo_contratanteusuario_contratantecod_Sortdsc = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Sortdsc");
            Ddo_contratanteusuario_contratantecod_Cleanfilter = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Cleanfilter");
            Ddo_contratanteusuario_contratantecod_Rangefilterfrom = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Rangefilterfrom");
            Ddo_contratanteusuario_contratantecod_Rangefilterto = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Rangefilterto");
            Ddo_contratanteusuario_contratantecod_Searchbuttontext = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Searchbuttontext");
            Ddo_contratanteusuario_contratantefan_Caption = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Caption");
            Ddo_contratanteusuario_contratantefan_Tooltip = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Tooltip");
            Ddo_contratanteusuario_contratantefan_Cls = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Cls");
            Ddo_contratanteusuario_contratantefan_Filteredtext_set = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Filteredtext_set");
            Ddo_contratanteusuario_contratantefan_Selectedvalue_set = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Selectedvalue_set");
            Ddo_contratanteusuario_contratantefan_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Dropdownoptionstype");
            Ddo_contratanteusuario_contratantefan_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Titlecontrolidtoreplace");
            Ddo_contratanteusuario_contratantefan_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Includesortasc"));
            Ddo_contratanteusuario_contratantefan_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Includesortdsc"));
            Ddo_contratanteusuario_contratantefan_Sortedstatus = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Sortedstatus");
            Ddo_contratanteusuario_contratantefan_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Includefilter"));
            Ddo_contratanteusuario_contratantefan_Filtertype = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Filtertype");
            Ddo_contratanteusuario_contratantefan_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Filterisrange"));
            Ddo_contratanteusuario_contratantefan_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Includedatalist"));
            Ddo_contratanteusuario_contratantefan_Datalisttype = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Datalisttype");
            Ddo_contratanteusuario_contratantefan_Datalistproc = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Datalistproc");
            Ddo_contratanteusuario_contratantefan_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratanteusuario_contratantefan_Sortasc = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Sortasc");
            Ddo_contratanteusuario_contratantefan_Sortdsc = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Sortdsc");
            Ddo_contratanteusuario_contratantefan_Loadingdata = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Loadingdata");
            Ddo_contratanteusuario_contratantefan_Cleanfilter = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Cleanfilter");
            Ddo_contratanteusuario_contratantefan_Noresultsfound = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Noresultsfound");
            Ddo_contratanteusuario_contratantefan_Searchbuttontext = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Searchbuttontext");
            Ddo_contratanteusuario_contratanteraz_Caption = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Caption");
            Ddo_contratanteusuario_contratanteraz_Tooltip = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Tooltip");
            Ddo_contratanteusuario_contratanteraz_Cls = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Cls");
            Ddo_contratanteusuario_contratanteraz_Filteredtext_set = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Filteredtext_set");
            Ddo_contratanteusuario_contratanteraz_Selectedvalue_set = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Selectedvalue_set");
            Ddo_contratanteusuario_contratanteraz_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Dropdownoptionstype");
            Ddo_contratanteusuario_contratanteraz_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Titlecontrolidtoreplace");
            Ddo_contratanteusuario_contratanteraz_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Includesortasc"));
            Ddo_contratanteusuario_contratanteraz_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Includesortdsc"));
            Ddo_contratanteusuario_contratanteraz_Sortedstatus = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Sortedstatus");
            Ddo_contratanteusuario_contratanteraz_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Includefilter"));
            Ddo_contratanteusuario_contratanteraz_Filtertype = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Filtertype");
            Ddo_contratanteusuario_contratanteraz_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Filterisrange"));
            Ddo_contratanteusuario_contratanteraz_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Includedatalist"));
            Ddo_contratanteusuario_contratanteraz_Datalisttype = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Datalisttype");
            Ddo_contratanteusuario_contratanteraz_Datalistproc = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Datalistproc");
            Ddo_contratanteusuario_contratanteraz_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratanteusuario_contratanteraz_Sortasc = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Sortasc");
            Ddo_contratanteusuario_contratanteraz_Sortdsc = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Sortdsc");
            Ddo_contratanteusuario_contratanteraz_Loadingdata = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Loadingdata");
            Ddo_contratanteusuario_contratanteraz_Cleanfilter = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Cleanfilter");
            Ddo_contratanteusuario_contratanteraz_Noresultsfound = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Noresultsfound");
            Ddo_contratanteusuario_contratanteraz_Searchbuttontext = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Searchbuttontext");
            Ddo_contratanteusuario_usuariocod_Caption = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Caption");
            Ddo_contratanteusuario_usuariocod_Tooltip = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Tooltip");
            Ddo_contratanteusuario_usuariocod_Cls = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Cls");
            Ddo_contratanteusuario_usuariocod_Filteredtext_set = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Filteredtext_set");
            Ddo_contratanteusuario_usuariocod_Filteredtextto_set = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Filteredtextto_set");
            Ddo_contratanteusuario_usuariocod_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Dropdownoptionstype");
            Ddo_contratanteusuario_usuariocod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Titlecontrolidtoreplace");
            Ddo_contratanteusuario_usuariocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Includesortasc"));
            Ddo_contratanteusuario_usuariocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Includesortdsc"));
            Ddo_contratanteusuario_usuariocod_Sortedstatus = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Sortedstatus");
            Ddo_contratanteusuario_usuariocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Includefilter"));
            Ddo_contratanteusuario_usuariocod_Filtertype = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Filtertype");
            Ddo_contratanteusuario_usuariocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Filterisrange"));
            Ddo_contratanteusuario_usuariocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Includedatalist"));
            Ddo_contratanteusuario_usuariocod_Sortasc = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Sortasc");
            Ddo_contratanteusuario_usuariocod_Sortdsc = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Sortdsc");
            Ddo_contratanteusuario_usuariocod_Cleanfilter = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Cleanfilter");
            Ddo_contratanteusuario_usuariocod_Rangefilterfrom = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Rangefilterfrom");
            Ddo_contratanteusuario_usuariocod_Rangefilterto = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Rangefilterto");
            Ddo_contratanteusuario_usuariocod_Searchbuttontext = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Searchbuttontext");
            Ddo_contratanteusuario_usuariopessoacod_Caption = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Caption");
            Ddo_contratanteusuario_usuariopessoacod_Tooltip = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Tooltip");
            Ddo_contratanteusuario_usuariopessoacod_Cls = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Cls");
            Ddo_contratanteusuario_usuariopessoacod_Filteredtext_set = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Filteredtext_set");
            Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_set = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Filteredtextto_set");
            Ddo_contratanteusuario_usuariopessoacod_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Dropdownoptionstype");
            Ddo_contratanteusuario_usuariopessoacod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Titlecontrolidtoreplace");
            Ddo_contratanteusuario_usuariopessoacod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Includesortasc"));
            Ddo_contratanteusuario_usuariopessoacod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Includesortdsc"));
            Ddo_contratanteusuario_usuariopessoacod_Sortedstatus = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Sortedstatus");
            Ddo_contratanteusuario_usuariopessoacod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Includefilter"));
            Ddo_contratanteusuario_usuariopessoacod_Filtertype = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Filtertype");
            Ddo_contratanteusuario_usuariopessoacod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Filterisrange"));
            Ddo_contratanteusuario_usuariopessoacod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Includedatalist"));
            Ddo_contratanteusuario_usuariopessoacod_Sortasc = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Sortasc");
            Ddo_contratanteusuario_usuariopessoacod_Sortdsc = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Sortdsc");
            Ddo_contratanteusuario_usuariopessoacod_Cleanfilter = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Cleanfilter");
            Ddo_contratanteusuario_usuariopessoacod_Rangefilterfrom = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Rangefilterfrom");
            Ddo_contratanteusuario_usuariopessoacod_Rangefilterto = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Rangefilterto");
            Ddo_contratanteusuario_usuariopessoacod_Searchbuttontext = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Searchbuttontext");
            Ddo_contratanteusuario_usuariopessoanom_Caption = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Caption");
            Ddo_contratanteusuario_usuariopessoanom_Tooltip = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Tooltip");
            Ddo_contratanteusuario_usuariopessoanom_Cls = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Cls");
            Ddo_contratanteusuario_usuariopessoanom_Filteredtext_set = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Filteredtext_set");
            Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_set = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Selectedvalue_set");
            Ddo_contratanteusuario_usuariopessoanom_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Dropdownoptionstype");
            Ddo_contratanteusuario_usuariopessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Titlecontrolidtoreplace");
            Ddo_contratanteusuario_usuariopessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Includesortasc"));
            Ddo_contratanteusuario_usuariopessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Includesortdsc"));
            Ddo_contratanteusuario_usuariopessoanom_Sortedstatus = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Sortedstatus");
            Ddo_contratanteusuario_usuariopessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Includefilter"));
            Ddo_contratanteusuario_usuariopessoanom_Filtertype = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Filtertype");
            Ddo_contratanteusuario_usuariopessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Filterisrange"));
            Ddo_contratanteusuario_usuariopessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Includedatalist"));
            Ddo_contratanteusuario_usuariopessoanom_Datalisttype = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Datalisttype");
            Ddo_contratanteusuario_usuariopessoanom_Datalistproc = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Datalistproc");
            Ddo_contratanteusuario_usuariopessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratanteusuario_usuariopessoanom_Sortasc = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Sortasc");
            Ddo_contratanteusuario_usuariopessoanom_Sortdsc = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Sortdsc");
            Ddo_contratanteusuario_usuariopessoanom_Loadingdata = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Loadingdata");
            Ddo_contratanteusuario_usuariopessoanom_Cleanfilter = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Cleanfilter");
            Ddo_contratanteusuario_usuariopessoanom_Noresultsfound = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Noresultsfound");
            Ddo_contratanteusuario_usuariopessoanom_Searchbuttontext = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratanteusuario_contratantecod_Activeeventkey = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Activeeventkey");
            Ddo_contratanteusuario_contratantecod_Filteredtext_get = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Filteredtext_get");
            Ddo_contratanteusuario_contratantecod_Filteredtextto_get = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD_Filteredtextto_get");
            Ddo_contratanteusuario_contratantefan_Activeeventkey = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Activeeventkey");
            Ddo_contratanteusuario_contratantefan_Filteredtext_get = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Filteredtext_get");
            Ddo_contratanteusuario_contratantefan_Selectedvalue_get = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN_Selectedvalue_get");
            Ddo_contratanteusuario_contratanteraz_Activeeventkey = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Activeeventkey");
            Ddo_contratanteusuario_contratanteraz_Filteredtext_get = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Filteredtext_get");
            Ddo_contratanteusuario_contratanteraz_Selectedvalue_get = cgiGet( "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ_Selectedvalue_get");
            Ddo_contratanteusuario_usuariocod_Activeeventkey = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Activeeventkey");
            Ddo_contratanteusuario_usuariocod_Filteredtext_get = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Filteredtext_get");
            Ddo_contratanteusuario_usuariocod_Filteredtextto_get = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOCOD_Filteredtextto_get");
            Ddo_contratanteusuario_usuariopessoacod_Activeeventkey = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Activeeventkey");
            Ddo_contratanteusuario_usuariopessoacod_Filteredtext_get = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Filteredtext_get");
            Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_get = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD_Filteredtextto_get");
            Ddo_contratanteusuario_usuariopessoanom_Activeeventkey = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Activeeventkey");
            Ddo_contratanteusuario_usuariopessoanom_Filteredtext_get = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Filteredtext_get");
            Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_get = cgiGet( "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_NOME1"), AV56Usuario_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATANTEUSUARIO_USUARIOPESSOANOM1"), AV19ContratanteUsuario_UsuarioPessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_NOME2"), AV57Usuario_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATANTEUSUARIO_USUARIOPESSOANOM2"), AV24ContratanteUsuario_UsuarioPessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTECOD"), ",", ".") != Convert.ToDecimal( AV61TFContratanteUsuario_ContratanteCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO"), ",", ".") != Convert.ToDecimal( AV62TFContratanteUsuario_ContratanteCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTEFAN"), AV65TFContratanteUsuario_ContratanteFan) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL"), AV66TFContratanteUsuario_ContratanteFan_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTERAZ"), AV69TFContratanteUsuario_ContratanteRaz) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL"), AV70TFContratanteUsuario_ContratanteRaz_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATANTEUSUARIO_USUARIOCOD"), ",", ".") != Convert.ToDecimal( AV73TFContratanteUsuario_UsuarioCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATANTEUSUARIO_USUARIOCOD_TO"), ",", ".") != Convert.ToDecimal( AV74TFContratanteUsuario_UsuarioCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD"), ",", ".") != Convert.ToDecimal( AV77TFContratanteUsuario_UsuarioPessoaCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO"), ",", ".") != Convert.ToDecimal( AV78TFContratanteUsuario_UsuarioPessoaCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM"), AV81TFContratanteUsuario_UsuarioPessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL"), AV82TFContratanteUsuario_UsuarioPessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E268E2 */
         E268E2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E268E2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "USUARIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "USUARIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfcontratanteusuario_contratantecod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratanteusuario_contratantecod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratanteusuario_contratantecod_Visible), 5, 0)));
         edtavTfcontratanteusuario_contratantecod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratanteusuario_contratantecod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratanteusuario_contratantecod_to_Visible), 5, 0)));
         edtavTfcontratanteusuario_contratantefan_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratanteusuario_contratantefan_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratanteusuario_contratantefan_Visible), 5, 0)));
         edtavTfcontratanteusuario_contratantefan_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratanteusuario_contratantefan_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratanteusuario_contratantefan_sel_Visible), 5, 0)));
         edtavTfcontratanteusuario_contratanteraz_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratanteusuario_contratanteraz_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratanteusuario_contratanteraz_Visible), 5, 0)));
         edtavTfcontratanteusuario_contratanteraz_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratanteusuario_contratanteraz_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratanteusuario_contratanteraz_sel_Visible), 5, 0)));
         edtavTfcontratanteusuario_usuariocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratanteusuario_usuariocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratanteusuario_usuariocod_Visible), 5, 0)));
         edtavTfcontratanteusuario_usuariocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratanteusuario_usuariocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratanteusuario_usuariocod_to_Visible), 5, 0)));
         edtavTfcontratanteusuario_usuariopessoacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratanteusuario_usuariopessoacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratanteusuario_usuariopessoacod_Visible), 5, 0)));
         edtavTfcontratanteusuario_usuariopessoacod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratanteusuario_usuariopessoacod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratanteusuario_usuariopessoacod_to_Visible), 5, 0)));
         edtavTfcontratanteusuario_usuariopessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratanteusuario_usuariopessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratanteusuario_usuariopessoanom_Visible), 5, 0)));
         edtavTfcontratanteusuario_usuariopessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratanteusuario_usuariopessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratanteusuario_usuariopessoanom_sel_Visible), 5, 0)));
         Ddo_contratanteusuario_contratantecod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratanteUsuario_ContratanteCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantecod_Internalname, "TitleControlIdToReplace", Ddo_contratanteusuario_contratantecod_Titlecontrolidtoreplace);
         AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace = Ddo_contratanteusuario_contratantecod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace", AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace);
         edtavDdo_contratanteusuario_contratantecodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratanteusuario_contratantecodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratanteusuario_contratantecodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratanteusuario_contratantefan_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratanteUsuario_ContratanteFan";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantefan_Internalname, "TitleControlIdToReplace", Ddo_contratanteusuario_contratantefan_Titlecontrolidtoreplace);
         AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace = Ddo_contratanteusuario_contratantefan_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace", AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace);
         edtavDdo_contratanteusuario_contratantefantitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratanteusuario_contratantefantitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratanteusuario_contratantefantitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratanteusuario_contratanteraz_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratanteUsuario_ContratanteRaz";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratanteraz_Internalname, "TitleControlIdToReplace", Ddo_contratanteusuario_contratanteraz_Titlecontrolidtoreplace);
         AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace = Ddo_contratanteusuario_contratanteraz_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace", AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace);
         edtavDdo_contratanteusuario_contratanteraztitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratanteusuario_contratanteraztitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratanteusuario_contratanteraztitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratanteusuario_usuariocod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratanteUsuario_UsuarioCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariocod_Internalname, "TitleControlIdToReplace", Ddo_contratanteusuario_usuariocod_Titlecontrolidtoreplace);
         AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace = Ddo_contratanteusuario_usuariocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace", AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace);
         edtavDdo_contratanteusuario_usuariocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratanteusuario_usuariocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratanteusuario_usuariocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratanteusuario_usuariopessoacod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratanteUsuario_UsuarioPessoaCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoacod_Internalname, "TitleControlIdToReplace", Ddo_contratanteusuario_usuariopessoacod_Titlecontrolidtoreplace);
         AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace = Ddo_contratanteusuario_usuariopessoacod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace", AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace);
         edtavDdo_contratanteusuario_usuariopessoacodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratanteusuario_usuariopessoacodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratanteusuario_usuariopessoacodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratanteusuario_usuariopessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratanteUsuario_UsuarioPessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoanom_Internalname, "TitleControlIdToReplace", Ddo_contratanteusuario_usuariopessoanom_Titlecontrolidtoreplace);
         AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace = Ddo_contratanteusuario_usuariopessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace", AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace);
         edtavDdo_contratanteusuario_usuariopessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratanteusuario_usuariopessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratanteusuario_usuariopessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contratante Usuario";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Usuario_Contratante Cod", 0);
         cmbavOrderedby.addItem("2", "Usuario_Contratante Fan", 0);
         cmbavOrderedby.addItem("3", "Usuario_Contratante Raz", 0);
         cmbavOrderedby.addItem("4", "Usuario_Cod", 0);
         cmbavOrderedby.addItem("5", "Cod", 0);
         cmbavOrderedby.addItem("6", "Nom", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV84DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV84DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E278E2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV60ContratanteUsuario_ContratanteCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64ContratanteUsuario_ContratanteFanTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68ContratanteUsuario_ContratanteRazTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72ContratanteUsuario_UsuarioCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76ContratanteUsuario_UsuarioPessoaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV80ContratanteUsuario_UsuarioPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "USUARIO_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratanteUsuario_ContratanteCod_Titleformat = 2;
         edtContratanteUsuario_ContratanteCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usuario_Contratante Cod", AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_ContratanteCod_Internalname, "Title", edtContratanteUsuario_ContratanteCod_Title);
         edtContratanteUsuario_ContratanteFan_Titleformat = 2;
         edtContratanteUsuario_ContratanteFan_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usuario_Contratante Fan", AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_ContratanteFan_Internalname, "Title", edtContratanteUsuario_ContratanteFan_Title);
         edtContratanteUsuario_ContratanteRaz_Titleformat = 2;
         edtContratanteUsuario_ContratanteRaz_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usuario_Contratante Raz", AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_ContratanteRaz_Internalname, "Title", edtContratanteUsuario_ContratanteRaz_Title);
         edtContratanteUsuario_UsuarioCod_Titleformat = 2;
         edtContratanteUsuario_UsuarioCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usuario_Cod", AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_UsuarioCod_Internalname, "Title", edtContratanteUsuario_UsuarioCod_Title);
         edtContratanteUsuario_UsuarioPessoaCod_Titleformat = 2;
         edtContratanteUsuario_UsuarioPessoaCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Cod", AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_UsuarioPessoaCod_Internalname, "Title", edtContratanteUsuario_UsuarioPessoaCod_Title);
         edtContratanteUsuario_UsuarioPessoaNom_Titleformat = 2;
         edtContratanteUsuario_UsuarioPessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nom", AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_UsuarioPessoaNom_Internalname, "Title", edtContratanteUsuario_UsuarioPessoaNom_Title);
         AV86GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86GridCurrentPage), 10, 0)));
         AV87GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87GridPageCount), 10, 0)));
         AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV92WWContratanteUsuarioDS_3_Usuario_nome1 = AV56Usuario_Nome1;
         AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = AV19ContratanteUsuario_UsuarioPessoaNom1;
         AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWContratanteUsuarioDS_8_Usuario_nome2 = AV57Usuario_Nome2;
         AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = AV24ContratanteUsuario_UsuarioPessoaNom2;
         AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod = AV61TFContratanteUsuario_ContratanteCod;
         AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to = AV62TFContratanteUsuario_ContratanteCod_To;
         AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = AV65TFContratanteUsuario_ContratanteFan;
         AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel = AV66TFContratanteUsuario_ContratanteFan_Sel;
         AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = AV69TFContratanteUsuario_ContratanteRaz;
         AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel = AV70TFContratanteUsuario_ContratanteRaz_Sel;
         AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod = AV73TFContratanteUsuario_UsuarioCod;
         AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to = AV74TFContratanteUsuario_UsuarioCod_To;
         AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod = AV77TFContratanteUsuario_UsuarioPessoaCod;
         AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to = AV78TFContratanteUsuario_UsuarioPessoaCod_To;
         AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = AV81TFContratanteUsuario_UsuarioPessoaNom;
         AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel = AV82TFContratanteUsuario_UsuarioPessoaNom_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV60ContratanteUsuario_ContratanteCodTitleFilterData", AV60ContratanteUsuario_ContratanteCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64ContratanteUsuario_ContratanteFanTitleFilterData", AV64ContratanteUsuario_ContratanteFanTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68ContratanteUsuario_ContratanteRazTitleFilterData", AV68ContratanteUsuario_ContratanteRazTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV72ContratanteUsuario_UsuarioCodTitleFilterData", AV72ContratanteUsuario_UsuarioCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV76ContratanteUsuario_UsuarioPessoaCodTitleFilterData", AV76ContratanteUsuario_UsuarioPessoaCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV80ContratanteUsuario_UsuarioPessoaNomTitleFilterData", AV80ContratanteUsuario_UsuarioPessoaNomTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E118E2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV85PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV85PageToGo) ;
         }
      }

      protected void E128E2( )
      {
         /* Ddo_contratanteusuario_contratantecod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratanteusuario_contratantecod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratanteusuario_contratantecod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantecod_Internalname, "SortedStatus", Ddo_contratanteusuario_contratantecod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratanteusuario_contratantecod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratanteusuario_contratantecod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantecod_Internalname, "SortedStatus", Ddo_contratanteusuario_contratantecod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratanteusuario_contratantecod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV61TFContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( Ddo_contratanteusuario_contratantecod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContratanteUsuario_ContratanteCod), 6, 0)));
            AV62TFContratanteUsuario_ContratanteCod_To = (int)(NumberUtil.Val( Ddo_contratanteusuario_contratantecod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratanteUsuario_ContratanteCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContratanteUsuario_ContratanteCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E138E2( )
      {
         /* Ddo_contratanteusuario_contratantefan_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratanteusuario_contratantefan_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratanteusuario_contratantefan_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantefan_Internalname, "SortedStatus", Ddo_contratanteusuario_contratantefan_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratanteusuario_contratantefan_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratanteusuario_contratantefan_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantefan_Internalname, "SortedStatus", Ddo_contratanteusuario_contratantefan_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratanteusuario_contratantefan_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFContratanteUsuario_ContratanteFan = Ddo_contratanteusuario_contratantefan_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratanteUsuario_ContratanteFan", AV65TFContratanteUsuario_ContratanteFan);
            AV66TFContratanteUsuario_ContratanteFan_Sel = Ddo_contratanteusuario_contratantefan_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratanteUsuario_ContratanteFan_Sel", AV66TFContratanteUsuario_ContratanteFan_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E148E2( )
      {
         /* Ddo_contratanteusuario_contratanteraz_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratanteusuario_contratanteraz_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratanteusuario_contratanteraz_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratanteraz_Internalname, "SortedStatus", Ddo_contratanteusuario_contratanteraz_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratanteusuario_contratanteraz_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratanteusuario_contratanteraz_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratanteraz_Internalname, "SortedStatus", Ddo_contratanteusuario_contratanteraz_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratanteusuario_contratanteraz_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV69TFContratanteUsuario_ContratanteRaz = Ddo_contratanteusuario_contratanteraz_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratanteUsuario_ContratanteRaz", AV69TFContratanteUsuario_ContratanteRaz);
            AV70TFContratanteUsuario_ContratanteRaz_Sel = Ddo_contratanteusuario_contratanteraz_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratanteUsuario_ContratanteRaz_Sel", AV70TFContratanteUsuario_ContratanteRaz_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E158E2( )
      {
         /* Ddo_contratanteusuario_usuariocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratanteusuario_usuariocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratanteusuario_usuariocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariocod_Internalname, "SortedStatus", Ddo_contratanteusuario_usuariocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratanteusuario_usuariocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratanteusuario_usuariocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariocod_Internalname, "SortedStatus", Ddo_contratanteusuario_usuariocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratanteusuario_usuariocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV73TFContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( Ddo_contratanteusuario_usuariocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73TFContratanteUsuario_UsuarioCod), 6, 0)));
            AV74TFContratanteUsuario_UsuarioCod_To = (int)(NumberUtil.Val( Ddo_contratanteusuario_usuariocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratanteUsuario_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74TFContratanteUsuario_UsuarioCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E168E2( )
      {
         /* Ddo_contratanteusuario_usuariopessoacod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratanteusuario_usuariopessoacod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratanteusuario_usuariopessoacod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoacod_Internalname, "SortedStatus", Ddo_contratanteusuario_usuariopessoacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratanteusuario_usuariopessoacod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratanteusuario_usuariopessoacod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoacod_Internalname, "SortedStatus", Ddo_contratanteusuario_usuariopessoacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratanteusuario_usuariopessoacod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV77TFContratanteUsuario_UsuarioPessoaCod = (int)(NumberUtil.Val( Ddo_contratanteusuario_usuariopessoacod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContratanteUsuario_UsuarioPessoaCod), 6, 0)));
            AV78TFContratanteUsuario_UsuarioPessoaCod_To = (int)(NumberUtil.Val( Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContratanteUsuario_UsuarioPessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78TFContratanteUsuario_UsuarioPessoaCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E178E2( )
      {
         /* Ddo_contratanteusuario_usuariopessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratanteusuario_usuariopessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratanteusuario_usuariopessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoanom_Internalname, "SortedStatus", Ddo_contratanteusuario_usuariopessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratanteusuario_usuariopessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratanteusuario_usuariopessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoanom_Internalname, "SortedStatus", Ddo_contratanteusuario_usuariopessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratanteusuario_usuariopessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV81TFContratanteUsuario_UsuarioPessoaNom = Ddo_contratanteusuario_usuariopessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratanteUsuario_UsuarioPessoaNom", AV81TFContratanteUsuario_UsuarioPessoaNom);
            AV82TFContratanteUsuario_UsuarioPessoaNom_Sel = Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratanteUsuario_UsuarioPessoaNom_Sel", AV82TFContratanteUsuario_UsuarioPessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E288E2( )
      {
         /* Grid_Load Routine */
         AV32Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV32Update);
         AV111Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratanteusuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A63ContratanteUsuario_ContratanteCod) + "," + UrlEncode("" +A60ContratanteUsuario_UsuarioCod);
         AV33Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV33Delete);
         AV112Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratanteusuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A63ContratanteUsuario_ContratanteCod) + "," + UrlEncode("" +A60ContratanteUsuario_UsuarioCod);
         edtContratanteUsuario_ContratanteRaz_Link = formatLink("viewpessoa.aspx") + "?" + UrlEncode("" +A340ContratanteUsuario_ContratantePesCod) + "," + UrlEncode(StringUtil.RTrim(""));
         edtContratanteUsuario_UsuarioPessoaNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A60ContratanteUsuario_UsuarioCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 73;
         }
         sendrow_732( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(73, GridRow);
         }
      }

      protected void E188E2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E238E2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV56Usuario_Nome1, AV19ContratanteUsuario_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV57Usuario_Nome2, AV24ContratanteUsuario_UsuarioPessoaNom2, AV20DynamicFiltersEnabled2, AV61TFContratanteUsuario_ContratanteCod, AV62TFContratanteUsuario_ContratanteCod_To, AV65TFContratanteUsuario_ContratanteFan, AV66TFContratanteUsuario_ContratanteFan_Sel, AV69TFContratanteUsuario_ContratanteRaz, AV70TFContratanteUsuario_ContratanteRaz_Sel, AV73TFContratanteUsuario_UsuarioCod, AV74TFContratanteUsuario_UsuarioCod_To, AV77TFContratanteUsuario_UsuarioPessoaCod, AV78TFContratanteUsuario_UsuarioPessoaCod_To, AV81TFContratanteUsuario_UsuarioPessoaNom, AV82TFContratanteUsuario_UsuarioPessoaNom_Sel, AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace, AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace, AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace, AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace, AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace, AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV113Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A340ContratanteUsuario_ContratantePesCod) ;
      }

      protected void E198E2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV56Usuario_Nome1, AV19ContratanteUsuario_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV57Usuario_Nome2, AV24ContratanteUsuario_UsuarioPessoaNom2, AV20DynamicFiltersEnabled2, AV61TFContratanteUsuario_ContratanteCod, AV62TFContratanteUsuario_ContratanteCod_To, AV65TFContratanteUsuario_ContratanteFan, AV66TFContratanteUsuario_ContratanteFan_Sel, AV69TFContratanteUsuario_ContratanteRaz, AV70TFContratanteUsuario_ContratanteRaz_Sel, AV73TFContratanteUsuario_UsuarioCod, AV74TFContratanteUsuario_UsuarioCod_To, AV77TFContratanteUsuario_UsuarioPessoaCod, AV78TFContratanteUsuario_UsuarioPessoaCod_To, AV81TFContratanteUsuario_UsuarioPessoaNom, AV82TFContratanteUsuario_UsuarioPessoaNom_Sel, AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace, AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace, AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace, AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace, AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace, AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV113Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A340ContratanteUsuario_ContratantePesCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E248E2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E208E2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV56Usuario_Nome1, AV19ContratanteUsuario_UsuarioPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV57Usuario_Nome2, AV24ContratanteUsuario_UsuarioPessoaNom2, AV20DynamicFiltersEnabled2, AV61TFContratanteUsuario_ContratanteCod, AV62TFContratanteUsuario_ContratanteCod_To, AV65TFContratanteUsuario_ContratanteFan, AV66TFContratanteUsuario_ContratanteFan_Sel, AV69TFContratanteUsuario_ContratanteRaz, AV70TFContratanteUsuario_ContratanteRaz_Sel, AV73TFContratanteUsuario_UsuarioCod, AV74TFContratanteUsuario_UsuarioCod_To, AV77TFContratanteUsuario_UsuarioPessoaCod, AV78TFContratanteUsuario_UsuarioPessoaCod_To, AV81TFContratanteUsuario_UsuarioPessoaNom, AV82TFContratanteUsuario_UsuarioPessoaNom_Sel, AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace, AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace, AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace, AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace, AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace, AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV113Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A340ContratanteUsuario_ContratantePesCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E258E2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E218E2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E228E2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratanteusuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratanteusuario_contratantecod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantecod_Internalname, "SortedStatus", Ddo_contratanteusuario_contratantecod_Sortedstatus);
         Ddo_contratanteusuario_contratantefan_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantefan_Internalname, "SortedStatus", Ddo_contratanteusuario_contratantefan_Sortedstatus);
         Ddo_contratanteusuario_contratanteraz_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratanteraz_Internalname, "SortedStatus", Ddo_contratanteusuario_contratanteraz_Sortedstatus);
         Ddo_contratanteusuario_usuariocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariocod_Internalname, "SortedStatus", Ddo_contratanteusuario_usuariocod_Sortedstatus);
         Ddo_contratanteusuario_usuariopessoacod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoacod_Internalname, "SortedStatus", Ddo_contratanteusuario_usuariopessoacod_Sortedstatus);
         Ddo_contratanteusuario_usuariopessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoanom_Internalname, "SortedStatus", Ddo_contratanteusuario_usuariopessoanom_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contratanteusuario_contratantecod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantecod_Internalname, "SortedStatus", Ddo_contratanteusuario_contratantecod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratanteusuario_contratantefan_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantefan_Internalname, "SortedStatus", Ddo_contratanteusuario_contratantefan_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratanteusuario_contratanteraz_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratanteraz_Internalname, "SortedStatus", Ddo_contratanteusuario_contratanteraz_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratanteusuario_usuariocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariocod_Internalname, "SortedStatus", Ddo_contratanteusuario_usuariocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratanteusuario_usuariopessoacod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoacod_Internalname, "SortedStatus", Ddo_contratanteusuario_usuariopessoacod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contratanteusuario_usuariopessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoanom_Internalname, "SortedStatus", Ddo_contratanteusuario_usuariopessoanom_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavUsuario_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome1_Visible), 5, 0)));
         edtavContratanteusuario_usuariopessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratanteusuario_usuariopessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratanteusuario_usuariopessoanom1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "USUARIO_NOME") == 0 )
         {
            edtavUsuario_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            edtavContratanteusuario_usuariopessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratanteusuario_usuariopessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratanteusuario_usuariopessoanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavUsuario_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome2_Visible), 5, 0)));
         edtavContratanteusuario_usuariopessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratanteusuario_usuariopessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratanteusuario_usuariopessoanom2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_NOME") == 0 )
         {
            edtavUsuario_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            edtavContratanteusuario_usuariopessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratanteusuario_usuariopessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratanteusuario_usuariopessoanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "USUARIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV57Usuario_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Usuario_Nome2", AV57Usuario_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV61TFContratanteUsuario_ContratanteCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContratanteUsuario_ContratanteCod), 6, 0)));
         Ddo_contratanteusuario_contratantecod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantecod_Internalname, "FilteredText_set", Ddo_contratanteusuario_contratantecod_Filteredtext_set);
         AV62TFContratanteUsuario_ContratanteCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratanteUsuario_ContratanteCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContratanteUsuario_ContratanteCod_To), 6, 0)));
         Ddo_contratanteusuario_contratantecod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantecod_Internalname, "FilteredTextTo_set", Ddo_contratanteusuario_contratantecod_Filteredtextto_set);
         AV65TFContratanteUsuario_ContratanteFan = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratanteUsuario_ContratanteFan", AV65TFContratanteUsuario_ContratanteFan);
         Ddo_contratanteusuario_contratantefan_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantefan_Internalname, "FilteredText_set", Ddo_contratanteusuario_contratantefan_Filteredtext_set);
         AV66TFContratanteUsuario_ContratanteFan_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratanteUsuario_ContratanteFan_Sel", AV66TFContratanteUsuario_ContratanteFan_Sel);
         Ddo_contratanteusuario_contratantefan_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantefan_Internalname, "SelectedValue_set", Ddo_contratanteusuario_contratantefan_Selectedvalue_set);
         AV69TFContratanteUsuario_ContratanteRaz = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratanteUsuario_ContratanteRaz", AV69TFContratanteUsuario_ContratanteRaz);
         Ddo_contratanteusuario_contratanteraz_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratanteraz_Internalname, "FilteredText_set", Ddo_contratanteusuario_contratanteraz_Filteredtext_set);
         AV70TFContratanteUsuario_ContratanteRaz_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratanteUsuario_ContratanteRaz_Sel", AV70TFContratanteUsuario_ContratanteRaz_Sel);
         Ddo_contratanteusuario_contratanteraz_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratanteraz_Internalname, "SelectedValue_set", Ddo_contratanteusuario_contratanteraz_Selectedvalue_set);
         AV73TFContratanteUsuario_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73TFContratanteUsuario_UsuarioCod), 6, 0)));
         Ddo_contratanteusuario_usuariocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariocod_Internalname, "FilteredText_set", Ddo_contratanteusuario_usuariocod_Filteredtext_set);
         AV74TFContratanteUsuario_UsuarioCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratanteUsuario_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74TFContratanteUsuario_UsuarioCod_To), 6, 0)));
         Ddo_contratanteusuario_usuariocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariocod_Internalname, "FilteredTextTo_set", Ddo_contratanteusuario_usuariocod_Filteredtextto_set);
         AV77TFContratanteUsuario_UsuarioPessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContratanteUsuario_UsuarioPessoaCod), 6, 0)));
         Ddo_contratanteusuario_usuariopessoacod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoacod_Internalname, "FilteredText_set", Ddo_contratanteusuario_usuariopessoacod_Filteredtext_set);
         AV78TFContratanteUsuario_UsuarioPessoaCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContratanteUsuario_UsuarioPessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78TFContratanteUsuario_UsuarioPessoaCod_To), 6, 0)));
         Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoacod_Internalname, "FilteredTextTo_set", Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_set);
         AV81TFContratanteUsuario_UsuarioPessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratanteUsuario_UsuarioPessoaNom", AV81TFContratanteUsuario_UsuarioPessoaNom);
         Ddo_contratanteusuario_usuariopessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoanom_Internalname, "FilteredText_set", Ddo_contratanteusuario_usuariopessoanom_Filteredtext_set);
         AV82TFContratanteUsuario_UsuarioPessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratanteUsuario_UsuarioPessoaNom_Sel", AV82TFContratanteUsuario_UsuarioPessoaNom_Sel);
         Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoanom_Internalname, "SelectedValue_set", Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "USUARIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV56Usuario_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Usuario_Nome1", AV56Usuario_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV54Session.Get(AV113Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV113Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV54Session.Get(AV113Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV114GXV1 = 1;
         while ( AV114GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV114GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTECOD") == 0 )
            {
               AV61TFContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContratanteUsuario_ContratanteCod), 6, 0)));
               AV62TFContratanteUsuario_ContratanteCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratanteUsuario_ContratanteCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContratanteUsuario_ContratanteCod_To), 6, 0)));
               if ( ! (0==AV61TFContratanteUsuario_ContratanteCod) )
               {
                  Ddo_contratanteusuario_contratantecod_Filteredtext_set = StringUtil.Str( (decimal)(AV61TFContratanteUsuario_ContratanteCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantecod_Internalname, "FilteredText_set", Ddo_contratanteusuario_contratantecod_Filteredtext_set);
               }
               if ( ! (0==AV62TFContratanteUsuario_ContratanteCod_To) )
               {
                  Ddo_contratanteusuario_contratantecod_Filteredtextto_set = StringUtil.Str( (decimal)(AV62TFContratanteUsuario_ContratanteCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantecod_Internalname, "FilteredTextTo_set", Ddo_contratanteusuario_contratantecod_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTEFAN") == 0 )
            {
               AV65TFContratanteUsuario_ContratanteFan = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratanteUsuario_ContratanteFan", AV65TFContratanteUsuario_ContratanteFan);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContratanteUsuario_ContratanteFan)) )
               {
                  Ddo_contratanteusuario_contratantefan_Filteredtext_set = AV65TFContratanteUsuario_ContratanteFan;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantefan_Internalname, "FilteredText_set", Ddo_contratanteusuario_contratantefan_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL") == 0 )
            {
               AV66TFContratanteUsuario_ContratanteFan_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratanteUsuario_ContratanteFan_Sel", AV66TFContratanteUsuario_ContratanteFan_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFContratanteUsuario_ContratanteFan_Sel)) )
               {
                  Ddo_contratanteusuario_contratantefan_Selectedvalue_set = AV66TFContratanteUsuario_ContratanteFan_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratantefan_Internalname, "SelectedValue_set", Ddo_contratanteusuario_contratantefan_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 )
            {
               AV69TFContratanteUsuario_ContratanteRaz = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratanteUsuario_ContratanteRaz", AV69TFContratanteUsuario_ContratanteRaz);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69TFContratanteUsuario_ContratanteRaz)) )
               {
                  Ddo_contratanteusuario_contratanteraz_Filteredtext_set = AV69TFContratanteUsuario_ContratanteRaz;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratanteraz_Internalname, "FilteredText_set", Ddo_contratanteusuario_contratanteraz_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL") == 0 )
            {
               AV70TFContratanteUsuario_ContratanteRaz_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratanteUsuario_ContratanteRaz_Sel", AV70TFContratanteUsuario_ContratanteRaz_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFContratanteUsuario_ContratanteRaz_Sel)) )
               {
                  Ddo_contratanteusuario_contratanteraz_Selectedvalue_set = AV70TFContratanteUsuario_ContratanteRaz_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_contratanteraz_Internalname, "SelectedValue_set", Ddo_contratanteusuario_contratanteraz_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_USUARIOCOD") == 0 )
            {
               AV73TFContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73TFContratanteUsuario_UsuarioCod), 6, 0)));
               AV74TFContratanteUsuario_UsuarioCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratanteUsuario_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74TFContratanteUsuario_UsuarioCod_To), 6, 0)));
               if ( ! (0==AV73TFContratanteUsuario_UsuarioCod) )
               {
                  Ddo_contratanteusuario_usuariocod_Filteredtext_set = StringUtil.Str( (decimal)(AV73TFContratanteUsuario_UsuarioCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariocod_Internalname, "FilteredText_set", Ddo_contratanteusuario_usuariocod_Filteredtext_set);
               }
               if ( ! (0==AV74TFContratanteUsuario_UsuarioCod_To) )
               {
                  Ddo_contratanteusuario_usuariocod_Filteredtextto_set = StringUtil.Str( (decimal)(AV74TFContratanteUsuario_UsuarioCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariocod_Internalname, "FilteredTextTo_set", Ddo_contratanteusuario_usuariocod_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_USUARIOPESSOACOD") == 0 )
            {
               AV77TFContratanteUsuario_UsuarioPessoaCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContratanteUsuario_UsuarioPessoaCod), 6, 0)));
               AV78TFContratanteUsuario_UsuarioPessoaCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContratanteUsuario_UsuarioPessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78TFContratanteUsuario_UsuarioPessoaCod_To), 6, 0)));
               if ( ! (0==AV77TFContratanteUsuario_UsuarioPessoaCod) )
               {
                  Ddo_contratanteusuario_usuariopessoacod_Filteredtext_set = StringUtil.Str( (decimal)(AV77TFContratanteUsuario_UsuarioPessoaCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoacod_Internalname, "FilteredText_set", Ddo_contratanteusuario_usuariopessoacod_Filteredtext_set);
               }
               if ( ! (0==AV78TFContratanteUsuario_UsuarioPessoaCod_To) )
               {
                  Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_set = StringUtil.Str( (decimal)(AV78TFContratanteUsuario_UsuarioPessoaCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoacod_Internalname, "FilteredTextTo_set", Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV81TFContratanteUsuario_UsuarioPessoaNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratanteUsuario_UsuarioPessoaNom", AV81TFContratanteUsuario_UsuarioPessoaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81TFContratanteUsuario_UsuarioPessoaNom)) )
               {
                  Ddo_contratanteusuario_usuariopessoanom_Filteredtext_set = AV81TFContratanteUsuario_UsuarioPessoaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoanom_Internalname, "FilteredText_set", Ddo_contratanteusuario_usuariopessoanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL") == 0 )
            {
               AV82TFContratanteUsuario_UsuarioPessoaNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratanteUsuario_UsuarioPessoaNom_Sel", AV82TFContratanteUsuario_UsuarioPessoaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFContratanteUsuario_UsuarioPessoaNom_Sel)) )
               {
                  Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_set = AV82TFContratanteUsuario_UsuarioPessoaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratanteusuario_usuariopessoanom_Internalname, "SelectedValue_set", Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_set);
               }
            }
            AV114GXV1 = (int)(AV114GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "USUARIO_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV56Usuario_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Usuario_Nome1", AV56Usuario_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV19ContratanteUsuario_UsuarioPessoaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratanteUsuario_UsuarioPessoaNom1", AV19ContratanteUsuario_UsuarioPessoaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV57Usuario_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Usuario_Nome2", AV57Usuario_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24ContratanteUsuario_UsuarioPessoaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratanteUsuario_UsuarioPessoaNom2", AV24ContratanteUsuario_UsuarioPessoaNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV30DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV54Session.Get(AV113Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV61TFContratanteUsuario_ContratanteCod) && (0==AV62TFContratanteUsuario_ContratanteCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTEUSUARIO_CONTRATANTECOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV61TFContratanteUsuario_ContratanteCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV62TFContratanteUsuario_ContratanteCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContratanteUsuario_ContratanteFan)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTEUSUARIO_CONTRATANTEFAN";
            AV11GridStateFilterValue.gxTpr_Value = AV65TFContratanteUsuario_ContratanteFan;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFContratanteUsuario_ContratanteFan_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV66TFContratanteUsuario_ContratanteFan_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69TFContratanteUsuario_ContratanteRaz)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTEUSUARIO_CONTRATANTERAZ";
            AV11GridStateFilterValue.gxTpr_Value = AV69TFContratanteUsuario_ContratanteRaz;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFContratanteUsuario_ContratanteRaz_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV70TFContratanteUsuario_ContratanteRaz_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV73TFContratanteUsuario_UsuarioCod) && (0==AV74TFContratanteUsuario_UsuarioCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTEUSUARIO_USUARIOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV73TFContratanteUsuario_UsuarioCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV74TFContratanteUsuario_UsuarioCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV77TFContratanteUsuario_UsuarioPessoaCod) && (0==AV78TFContratanteUsuario_UsuarioPessoaCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTEUSUARIO_USUARIOPESSOACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV77TFContratanteUsuario_UsuarioPessoaCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV78TFContratanteUsuario_UsuarioPessoaCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81TFContratanteUsuario_UsuarioPessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTEUSUARIO_USUARIOPESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV81TFContratanteUsuario_UsuarioPessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFContratanteUsuario_UsuarioPessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV82TFContratanteUsuario_UsuarioPessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV113Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV31DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "USUARIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Usuario_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV56Usuario_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContratanteUsuario_UsuarioPessoaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19ContratanteUsuario_UsuarioPessoaNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV57Usuario_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV57Usuario_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContratanteUsuario_UsuarioPessoaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV24ContratanteUsuario_UsuarioPessoaNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV113Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratanteUsuario";
         AV54Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_8E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_8E2( true) ;
         }
         else
         {
            wb_table2_8_8E2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_8E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_67_8E2( true) ;
         }
         else
         {
            wb_table3_67_8E2( false) ;
         }
         return  ;
      }

      protected void wb_table3_67_8E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_8E2e( true) ;
         }
         else
         {
            wb_table1_2_8E2e( false) ;
         }
      }

      protected void wb_table3_67_8E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_70_8E2( true) ;
         }
         else
         {
            wb_table4_70_8E2( false) ;
         }
         return  ;
      }

      protected void wb_table4_70_8E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_67_8E2e( true) ;
         }
         else
         {
            wb_table3_67_8E2e( false) ;
         }
      }

      protected void wb_table4_70_8E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"73\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratanteUsuario_ContratanteCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratanteUsuario_ContratanteCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratanteUsuario_ContratanteCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratanteUsuario_ContratanteFan_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratanteUsuario_ContratanteFan_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratanteUsuario_ContratanteFan_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratanteUsuario_ContratanteRaz_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratanteUsuario_ContratanteRaz_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratanteUsuario_ContratanteRaz_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratanteUsuario_UsuarioCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratanteUsuario_UsuarioCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratanteUsuario_UsuarioCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratanteUsuario_UsuarioPessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratanteUsuario_UsuarioPessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratanteUsuario_UsuarioPessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratanteUsuario_UsuarioPessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratanteUsuario_UsuarioPessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratanteUsuario_UsuarioPessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV33Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratanteUsuario_ContratanteCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratanteUsuario_ContratanteCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A65ContratanteUsuario_ContratanteFan));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratanteUsuario_ContratanteFan_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratanteUsuario_ContratanteFan_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A64ContratanteUsuario_ContratanteRaz));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratanteUsuario_ContratanteRaz_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratanteUsuario_ContratanteRaz_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratanteUsuario_ContratanteRaz_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratanteUsuario_UsuarioCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratanteUsuario_UsuarioCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratanteUsuario_UsuarioPessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratanteUsuario_UsuarioPessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A62ContratanteUsuario_UsuarioPessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratanteUsuario_UsuarioPessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratanteUsuario_UsuarioPessoaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratanteUsuario_UsuarioPessoaNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 73 )
         {
            wbEnd = 0;
            nRC_GXsfl_73 = (short)(nGXsfl_73_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_70_8E2e( true) ;
         }
         else
         {
            wb_table4_70_8E2e( false) ;
         }
      }

      protected void wb_table2_8_8E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratanteusuariotitle_Internalname, "Contratante Usuario", "", "", lblContratanteusuariotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Width100'>") ;
            wb_table5_13_8E2( true) ;
         }
         else
         {
            wb_table5_13_8E2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_8E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWContratanteUsuario.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_8E2( true) ;
         }
         else
         {
            wb_table6_23_8E2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_8E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_8E2e( true) ;
         }
         else
         {
            wb_table2_8_8E2e( false) ;
         }
      }

      protected void wb_table6_23_8E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_8E2( true) ;
         }
         else
         {
            wb_table7_28_8E2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_8E2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_8E2e( true) ;
         }
         else
         {
            wb_table6_23_8E2e( false) ;
         }
      }

      protected void wb_table7_28_8E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWContratanteUsuario.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_8E2( true) ;
         }
         else
         {
            wb_table8_37_8E2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_8E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratanteUsuario.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_WWContratanteUsuario.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_55_8E2( true) ;
         }
         else
         {
            wb_table9_55_8E2( false) ;
         }
         return  ;
      }

      protected void wb_table9_55_8E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_8E2e( true) ;
         }
         else
         {
            wb_table7_28_8E2e( false) ;
         }
      }

      protected void wb_table9_55_8E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_WWContratanteUsuario.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_nome2_Internalname, StringUtil.RTrim( AV57Usuario_Nome2), StringUtil.RTrim( context.localUtil.Format( AV57Usuario_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratanteusuario_usuariopessoanom2_Internalname, StringUtil.RTrim( AV24ContratanteUsuario_UsuarioPessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV24ContratanteUsuario_UsuarioPessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratanteusuario_usuariopessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratanteusuario_usuariopessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_55_8E2e( true) ;
         }
         else
         {
            wb_table9_55_8E2e( false) ;
         }
      }

      protected void wb_table8_37_8E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWContratanteUsuario.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_nome1_Internalname, StringUtil.RTrim( AV56Usuario_Nome1), StringUtil.RTrim( context.localUtil.Format( AV56Usuario_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratanteusuario_usuariopessoanom1_Internalname, StringUtil.RTrim( AV19ContratanteUsuario_UsuarioPessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV19ContratanteUsuario_UsuarioPessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratanteusuario_usuariopessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratanteusuario_usuariopessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_8E2e( true) ;
         }
         else
         {
            wb_table8_37_8E2e( false) ;
         }
      }

      protected void wb_table5_13_8E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_8E2e( true) ;
         }
         else
         {
            wb_table5_13_8E2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA8E2( ) ;
         WS8E2( ) ;
         WE8E2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202053021293315");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratanteusuario.js", "?202053021293315");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_732( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_73_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_73_idx;
         edtContratanteUsuario_ContratanteCod_Internalname = "CONTRATANTEUSUARIO_CONTRATANTECOD_"+sGXsfl_73_idx;
         edtContratanteUsuario_ContratanteFan_Internalname = "CONTRATANTEUSUARIO_CONTRATANTEFAN_"+sGXsfl_73_idx;
         edtContratanteUsuario_ContratanteRaz_Internalname = "CONTRATANTEUSUARIO_CONTRATANTERAZ_"+sGXsfl_73_idx;
         edtContratanteUsuario_UsuarioCod_Internalname = "CONTRATANTEUSUARIO_USUARIOCOD_"+sGXsfl_73_idx;
         edtContratanteUsuario_UsuarioPessoaCod_Internalname = "CONTRATANTEUSUARIO_USUARIOPESSOACOD_"+sGXsfl_73_idx;
         edtContratanteUsuario_UsuarioPessoaNom_Internalname = "CONTRATANTEUSUARIO_USUARIOPESSOANOM_"+sGXsfl_73_idx;
      }

      protected void SubsflControlProps_fel_732( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_73_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_73_fel_idx;
         edtContratanteUsuario_ContratanteCod_Internalname = "CONTRATANTEUSUARIO_CONTRATANTECOD_"+sGXsfl_73_fel_idx;
         edtContratanteUsuario_ContratanteFan_Internalname = "CONTRATANTEUSUARIO_CONTRATANTEFAN_"+sGXsfl_73_fel_idx;
         edtContratanteUsuario_ContratanteRaz_Internalname = "CONTRATANTEUSUARIO_CONTRATANTERAZ_"+sGXsfl_73_fel_idx;
         edtContratanteUsuario_UsuarioCod_Internalname = "CONTRATANTEUSUARIO_USUARIOCOD_"+sGXsfl_73_fel_idx;
         edtContratanteUsuario_UsuarioPessoaCod_Internalname = "CONTRATANTEUSUARIO_USUARIOPESSOACOD_"+sGXsfl_73_fel_idx;
         edtContratanteUsuario_UsuarioPessoaNom_Internalname = "CONTRATANTEUSUARIO_USUARIOPESSOANOM_"+sGXsfl_73_fel_idx;
      }

      protected void sendrow_732( )
      {
         SubsflControlProps_732( ) ;
         WB8E0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_73_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_73_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_73_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV111Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)) ? AV111Update_GXI : context.PathToRelativeUrl( AV32Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV33Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV112Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)) ? AV112Delete_GXI : context.PathToRelativeUrl( AV33Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV33Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratanteUsuario_ContratanteCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A63ContratanteUsuario_ContratanteCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratanteUsuario_ContratanteCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratanteUsuario_ContratanteFan_Internalname,StringUtil.RTrim( A65ContratanteUsuario_ContratanteFan),StringUtil.RTrim( context.localUtil.Format( A65ContratanteUsuario_ContratanteFan, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratanteUsuario_ContratanteFan_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratanteUsuario_ContratanteRaz_Internalname,StringUtil.RTrim( A64ContratanteUsuario_ContratanteRaz),StringUtil.RTrim( context.localUtil.Format( A64ContratanteUsuario_ContratanteRaz, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratanteUsuario_ContratanteRaz_Link,(String)"",(String)"",(String)"",(String)edtContratanteUsuario_ContratanteRaz_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratanteUsuario_UsuarioCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A60ContratanteUsuario_UsuarioCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratanteUsuario_UsuarioCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratanteUsuario_UsuarioPessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratanteUsuario_UsuarioPessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratanteUsuario_UsuarioPessoaNom_Internalname,StringUtil.RTrim( A62ContratanteUsuario_UsuarioPessoaNom),StringUtil.RTrim( context.localUtil.Format( A62ContratanteUsuario_UsuarioPessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratanteUsuario_UsuarioPessoaNom_Link,(String)"",(String)"",(String)"",(String)edtContratanteUsuario_UsuarioPessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTEUSUARIO_CONTRATANTECOD"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, context.localUtil.Format( (decimal)(A63ContratanteUsuario_ContratanteCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTEUSUARIO_USUARIOCOD"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, context.localUtil.Format( (decimal)(A60ContratanteUsuario_UsuarioCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_73_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_732( ) ;
         }
         /* End function sendrow_732 */
      }

      protected void init_default_properties( )
      {
         lblContratanteusuariotitle_Internalname = "CONTRATANTEUSUARIOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavUsuario_nome1_Internalname = "vUSUARIO_NOME1";
         edtavContratanteusuario_usuariopessoanom1_Internalname = "vCONTRATANTEUSUARIO_USUARIOPESSOANOM1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavUsuario_nome2_Internalname = "vUSUARIO_NOME2";
         edtavContratanteusuario_usuariopessoanom2_Internalname = "vCONTRATANTEUSUARIO_USUARIOPESSOANOM2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContratanteUsuario_ContratanteCod_Internalname = "CONTRATANTEUSUARIO_CONTRATANTECOD";
         edtContratanteUsuario_ContratanteFan_Internalname = "CONTRATANTEUSUARIO_CONTRATANTEFAN";
         edtContratanteUsuario_ContratanteRaz_Internalname = "CONTRATANTEUSUARIO_CONTRATANTERAZ";
         edtContratanteUsuario_UsuarioCod_Internalname = "CONTRATANTEUSUARIO_USUARIOCOD";
         edtContratanteUsuario_UsuarioPessoaCod_Internalname = "CONTRATANTEUSUARIO_USUARIOPESSOACOD";
         edtContratanteUsuario_UsuarioPessoaNom_Internalname = "CONTRATANTEUSUARIO_USUARIOPESSOANOM";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfcontratanteusuario_contratantecod_Internalname = "vTFCONTRATANTEUSUARIO_CONTRATANTECOD";
         edtavTfcontratanteusuario_contratantecod_to_Internalname = "vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO";
         edtavTfcontratanteusuario_contratantefan_Internalname = "vTFCONTRATANTEUSUARIO_CONTRATANTEFAN";
         edtavTfcontratanteusuario_contratantefan_sel_Internalname = "vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL";
         edtavTfcontratanteusuario_contratanteraz_Internalname = "vTFCONTRATANTEUSUARIO_CONTRATANTERAZ";
         edtavTfcontratanteusuario_contratanteraz_sel_Internalname = "vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL";
         edtavTfcontratanteusuario_usuariocod_Internalname = "vTFCONTRATANTEUSUARIO_USUARIOCOD";
         edtavTfcontratanteusuario_usuariocod_to_Internalname = "vTFCONTRATANTEUSUARIO_USUARIOCOD_TO";
         edtavTfcontratanteusuario_usuariopessoacod_Internalname = "vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD";
         edtavTfcontratanteusuario_usuariopessoacod_to_Internalname = "vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO";
         edtavTfcontratanteusuario_usuariopessoanom_Internalname = "vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM";
         edtavTfcontratanteusuario_usuariopessoanom_sel_Internalname = "vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL";
         Ddo_contratanteusuario_contratantecod_Internalname = "DDO_CONTRATANTEUSUARIO_CONTRATANTECOD";
         edtavDdo_contratanteusuario_contratantecodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE";
         Ddo_contratanteusuario_contratantefan_Internalname = "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN";
         edtavDdo_contratanteusuario_contratantefantitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE";
         Ddo_contratanteusuario_contratanteraz_Internalname = "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ";
         edtavDdo_contratanteusuario_contratanteraztitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE";
         Ddo_contratanteusuario_usuariocod_Internalname = "DDO_CONTRATANTEUSUARIO_USUARIOCOD";
         edtavDdo_contratanteusuario_usuariocodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE";
         Ddo_contratanteusuario_usuariopessoacod_Internalname = "DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD";
         edtavDdo_contratanteusuario_usuariopessoacodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE";
         Ddo_contratanteusuario_usuariopessoanom_Internalname = "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM";
         edtavDdo_contratanteusuario_usuariopessoanomtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratanteUsuario_UsuarioPessoaNom_Jsonclick = "";
         edtContratanteUsuario_UsuarioPessoaCod_Jsonclick = "";
         edtContratanteUsuario_UsuarioCod_Jsonclick = "";
         edtContratanteUsuario_ContratanteRaz_Jsonclick = "";
         edtContratanteUsuario_ContratanteFan_Jsonclick = "";
         edtContratanteUsuario_ContratanteCod_Jsonclick = "";
         edtavContratanteusuario_usuariopessoanom1_Jsonclick = "";
         edtavUsuario_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratanteusuario_usuariopessoanom2_Jsonclick = "";
         edtavUsuario_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratanteUsuario_UsuarioPessoaNom_Link = "";
         edtContratanteUsuario_ContratanteRaz_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContratanteUsuario_UsuarioPessoaNom_Titleformat = 0;
         edtContratanteUsuario_UsuarioPessoaCod_Titleformat = 0;
         edtContratanteUsuario_UsuarioCod_Titleformat = 0;
         edtContratanteUsuario_ContratanteRaz_Titleformat = 0;
         edtContratanteUsuario_ContratanteFan_Titleformat = 0;
         edtContratanteUsuario_ContratanteCod_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratanteusuario_usuariopessoanom2_Visible = 1;
         edtavUsuario_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratanteusuario_usuariopessoanom1_Visible = 1;
         edtavUsuario_nome1_Visible = 1;
         edtContratanteUsuario_UsuarioPessoaNom_Title = "Nom";
         edtContratanteUsuario_UsuarioPessoaCod_Title = "Cod";
         edtContratanteUsuario_UsuarioCod_Title = "Usuario_Cod";
         edtContratanteUsuario_ContratanteRaz_Title = "Usuario_Contratante Raz";
         edtContratanteUsuario_ContratanteFan_Title = "Usuario_Contratante Fan";
         edtContratanteUsuario_ContratanteCod_Title = "Usuario_Contratante Cod";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratanteusuario_usuariopessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratanteusuario_usuariopessoacodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratanteusuario_usuariocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratanteusuario_contratanteraztitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratanteusuario_contratantefantitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratanteusuario_contratantecodtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratanteusuario_usuariopessoanom_sel_Jsonclick = "";
         edtavTfcontratanteusuario_usuariopessoanom_sel_Visible = 1;
         edtavTfcontratanteusuario_usuariopessoanom_Jsonclick = "";
         edtavTfcontratanteusuario_usuariopessoanom_Visible = 1;
         edtavTfcontratanteusuario_usuariopessoacod_to_Jsonclick = "";
         edtavTfcontratanteusuario_usuariopessoacod_to_Visible = 1;
         edtavTfcontratanteusuario_usuariopessoacod_Jsonclick = "";
         edtavTfcontratanteusuario_usuariopessoacod_Visible = 1;
         edtavTfcontratanteusuario_usuariocod_to_Jsonclick = "";
         edtavTfcontratanteusuario_usuariocod_to_Visible = 1;
         edtavTfcontratanteusuario_usuariocod_Jsonclick = "";
         edtavTfcontratanteusuario_usuariocod_Visible = 1;
         edtavTfcontratanteusuario_contratanteraz_sel_Jsonclick = "";
         edtavTfcontratanteusuario_contratanteraz_sel_Visible = 1;
         edtavTfcontratanteusuario_contratanteraz_Jsonclick = "";
         edtavTfcontratanteusuario_contratanteraz_Visible = 1;
         edtavTfcontratanteusuario_contratantefan_sel_Jsonclick = "";
         edtavTfcontratanteusuario_contratantefan_sel_Visible = 1;
         edtavTfcontratanteusuario_contratantefan_Jsonclick = "";
         edtavTfcontratanteusuario_contratantefan_Visible = 1;
         edtavTfcontratanteusuario_contratantecod_to_Jsonclick = "";
         edtavTfcontratanteusuario_contratantecod_to_Visible = 1;
         edtavTfcontratanteusuario_contratantecod_Jsonclick = "";
         edtavTfcontratanteusuario_contratantecod_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratanteusuario_usuariopessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contratanteusuario_usuariopessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratanteusuario_usuariopessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratanteusuario_usuariopessoanom_Loadingdata = "Carregando dados...";
         Ddo_contratanteusuario_usuariopessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratanteusuario_usuariopessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contratanteusuario_usuariopessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contratanteusuario_usuariopessoanom_Datalistproc = "GetWWContratanteUsuarioFilterData";
         Ddo_contratanteusuario_usuariopessoanom_Datalisttype = "Dynamic";
         Ddo_contratanteusuario_usuariopessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_usuariopessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratanteusuario_usuariopessoanom_Filtertype = "Character";
         Ddo_contratanteusuario_usuariopessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_usuariopessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_usuariopessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_usuariopessoanom_Titlecontrolidtoreplace = "";
         Ddo_contratanteusuario_usuariopessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratanteusuario_usuariopessoanom_Cls = "ColumnSettings";
         Ddo_contratanteusuario_usuariopessoanom_Tooltip = "Op��es";
         Ddo_contratanteusuario_usuariopessoanom_Caption = "";
         Ddo_contratanteusuario_usuariopessoacod_Searchbuttontext = "Pesquisar";
         Ddo_contratanteusuario_usuariopessoacod_Rangefilterto = "At�";
         Ddo_contratanteusuario_usuariopessoacod_Rangefilterfrom = "Desde";
         Ddo_contratanteusuario_usuariopessoacod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratanteusuario_usuariopessoacod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratanteusuario_usuariopessoacod_Sortasc = "Ordenar de A � Z";
         Ddo_contratanteusuario_usuariopessoacod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratanteusuario_usuariopessoacod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_usuariopessoacod_Filtertype = "Numeric";
         Ddo_contratanteusuario_usuariopessoacod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_usuariopessoacod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_usuariopessoacod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_usuariopessoacod_Titlecontrolidtoreplace = "";
         Ddo_contratanteusuario_usuariopessoacod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratanteusuario_usuariopessoacod_Cls = "ColumnSettings";
         Ddo_contratanteusuario_usuariopessoacod_Tooltip = "Op��es";
         Ddo_contratanteusuario_usuariopessoacod_Caption = "";
         Ddo_contratanteusuario_usuariocod_Searchbuttontext = "Pesquisar";
         Ddo_contratanteusuario_usuariocod_Rangefilterto = "At�";
         Ddo_contratanteusuario_usuariocod_Rangefilterfrom = "Desde";
         Ddo_contratanteusuario_usuariocod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratanteusuario_usuariocod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratanteusuario_usuariocod_Sortasc = "Ordenar de A � Z";
         Ddo_contratanteusuario_usuariocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratanteusuario_usuariocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_usuariocod_Filtertype = "Numeric";
         Ddo_contratanteusuario_usuariocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_usuariocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_usuariocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_usuariocod_Titlecontrolidtoreplace = "";
         Ddo_contratanteusuario_usuariocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratanteusuario_usuariocod_Cls = "ColumnSettings";
         Ddo_contratanteusuario_usuariocod_Tooltip = "Op��es";
         Ddo_contratanteusuario_usuariocod_Caption = "";
         Ddo_contratanteusuario_contratanteraz_Searchbuttontext = "Pesquisar";
         Ddo_contratanteusuario_contratanteraz_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratanteusuario_contratanteraz_Cleanfilter = "Limpar pesquisa";
         Ddo_contratanteusuario_contratanteraz_Loadingdata = "Carregando dados...";
         Ddo_contratanteusuario_contratanteraz_Sortdsc = "Ordenar de Z � A";
         Ddo_contratanteusuario_contratanteraz_Sortasc = "Ordenar de A � Z";
         Ddo_contratanteusuario_contratanteraz_Datalistupdateminimumcharacters = 0;
         Ddo_contratanteusuario_contratanteraz_Datalistproc = "GetWWContratanteUsuarioFilterData";
         Ddo_contratanteusuario_contratanteraz_Datalisttype = "Dynamic";
         Ddo_contratanteusuario_contratanteraz_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_contratanteraz_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratanteusuario_contratanteraz_Filtertype = "Character";
         Ddo_contratanteusuario_contratanteraz_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_contratanteraz_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_contratanteraz_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_contratanteraz_Titlecontrolidtoreplace = "";
         Ddo_contratanteusuario_contratanteraz_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratanteusuario_contratanteraz_Cls = "ColumnSettings";
         Ddo_contratanteusuario_contratanteraz_Tooltip = "Op��es";
         Ddo_contratanteusuario_contratanteraz_Caption = "";
         Ddo_contratanteusuario_contratantefan_Searchbuttontext = "Pesquisar";
         Ddo_contratanteusuario_contratantefan_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratanteusuario_contratantefan_Cleanfilter = "Limpar pesquisa";
         Ddo_contratanteusuario_contratantefan_Loadingdata = "Carregando dados...";
         Ddo_contratanteusuario_contratantefan_Sortdsc = "Ordenar de Z � A";
         Ddo_contratanteusuario_contratantefan_Sortasc = "Ordenar de A � Z";
         Ddo_contratanteusuario_contratantefan_Datalistupdateminimumcharacters = 0;
         Ddo_contratanteusuario_contratantefan_Datalistproc = "GetWWContratanteUsuarioFilterData";
         Ddo_contratanteusuario_contratantefan_Datalisttype = "Dynamic";
         Ddo_contratanteusuario_contratantefan_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_contratantefan_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratanteusuario_contratantefan_Filtertype = "Character";
         Ddo_contratanteusuario_contratantefan_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_contratantefan_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_contratantefan_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_contratantefan_Titlecontrolidtoreplace = "";
         Ddo_contratanteusuario_contratantefan_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratanteusuario_contratantefan_Cls = "ColumnSettings";
         Ddo_contratanteusuario_contratantefan_Tooltip = "Op��es";
         Ddo_contratanteusuario_contratantefan_Caption = "";
         Ddo_contratanteusuario_contratantecod_Searchbuttontext = "Pesquisar";
         Ddo_contratanteusuario_contratantecod_Rangefilterto = "At�";
         Ddo_contratanteusuario_contratantecod_Rangefilterfrom = "Desde";
         Ddo_contratanteusuario_contratantecod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratanteusuario_contratantecod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratanteusuario_contratantecod_Sortasc = "Ordenar de A � Z";
         Ddo_contratanteusuario_contratantecod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratanteusuario_contratantecod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_contratantecod_Filtertype = "Numeric";
         Ddo_contratanteusuario_contratantecod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_contratantecod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_contratantecod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratanteusuario_contratantecod_Titlecontrolidtoreplace = "";
         Ddo_contratanteusuario_contratantecod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratanteusuario_contratantecod_Cls = "ColumnSettings";
         Ddo_contratanteusuario_contratantecod_Tooltip = "Op��es";
         Ddo_contratanteusuario_contratantecod_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contratante Usuario";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV60ContratanteUsuario_ContratanteCodTitleFilterData',fld:'vCONTRATANTEUSUARIO_CONTRATANTECODTITLEFILTERDATA',pic:'',nv:null},{av:'AV64ContratanteUsuario_ContratanteFanTitleFilterData',fld:'vCONTRATANTEUSUARIO_CONTRATANTEFANTITLEFILTERDATA',pic:'',nv:null},{av:'AV68ContratanteUsuario_ContratanteRazTitleFilterData',fld:'vCONTRATANTEUSUARIO_CONTRATANTERAZTITLEFILTERDATA',pic:'',nv:null},{av:'AV72ContratanteUsuario_UsuarioCodTitleFilterData',fld:'vCONTRATANTEUSUARIO_USUARIOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV76ContratanteUsuario_UsuarioPessoaCodTitleFilterData',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOACODTITLEFILTERDATA',pic:'',nv:null},{av:'AV80ContratanteUsuario_UsuarioPessoaNomTitleFilterData',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtContratanteUsuario_ContratanteCod_Titleformat',ctrl:'CONTRATANTEUSUARIO_CONTRATANTECOD',prop:'Titleformat'},{av:'edtContratanteUsuario_ContratanteCod_Title',ctrl:'CONTRATANTEUSUARIO_CONTRATANTECOD',prop:'Title'},{av:'edtContratanteUsuario_ContratanteFan_Titleformat',ctrl:'CONTRATANTEUSUARIO_CONTRATANTEFAN',prop:'Titleformat'},{av:'edtContratanteUsuario_ContratanteFan_Title',ctrl:'CONTRATANTEUSUARIO_CONTRATANTEFAN',prop:'Title'},{av:'edtContratanteUsuario_ContratanteRaz_Titleformat',ctrl:'CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'Titleformat'},{av:'edtContratanteUsuario_ContratanteRaz_Title',ctrl:'CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'Title'},{av:'edtContratanteUsuario_UsuarioCod_Titleformat',ctrl:'CONTRATANTEUSUARIO_USUARIOCOD',prop:'Titleformat'},{av:'edtContratanteUsuario_UsuarioCod_Title',ctrl:'CONTRATANTEUSUARIO_USUARIOCOD',prop:'Title'},{av:'edtContratanteUsuario_UsuarioPessoaCod_Titleformat',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOACOD',prop:'Titleformat'},{av:'edtContratanteUsuario_UsuarioPessoaCod_Title',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOACOD',prop:'Title'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Titleformat',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Titleformat'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Title',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Title'},{av:'AV86GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV87GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E118E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATANTEUSUARIO_CONTRATANTECOD.ONOPTIONCLICKED","{handler:'E128E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_contratantecod_Activeeventkey',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTECOD',prop:'ActiveEventKey'},{av:'Ddo_contratanteusuario_contratantecod_Filteredtext_get',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTECOD',prop:'FilteredText_get'},{av:'Ddo_contratanteusuario_contratantecod_Filteredtextto_get',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTECOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratanteusuario_contratantecod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTECOD',prop:'SortedStatus'},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_contratantefan_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_contratanteraz_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariocod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN.ONOPTIONCLICKED","{handler:'E138E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_contratantefan_Activeeventkey',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN',prop:'ActiveEventKey'},{av:'Ddo_contratanteusuario_contratantefan_Filteredtext_get',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN',prop:'FilteredText_get'},{av:'Ddo_contratanteusuario_contratantefan_Selectedvalue_get',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratanteusuario_contratantefan_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN',prop:'SortedStatus'},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'Ddo_contratanteusuario_contratantecod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTECOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_contratanteraz_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariocod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ.ONOPTIONCLICKED","{handler:'E148E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_contratanteraz_Activeeventkey',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'ActiveEventKey'},{av:'Ddo_contratanteusuario_contratanteraz_Filteredtext_get',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'FilteredText_get'},{av:'Ddo_contratanteusuario_contratanteraz_Selectedvalue_get',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratanteusuario_contratanteraz_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'SortedStatus'},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'Ddo_contratanteusuario_contratantecod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTECOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_contratantefan_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariocod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTEUSUARIO_USUARIOCOD.ONOPTIONCLICKED","{handler:'E158E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_usuariocod_Activeeventkey',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOCOD',prop:'ActiveEventKey'},{av:'Ddo_contratanteusuario_usuariocod_Filteredtext_get',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOCOD',prop:'FilteredText_get'},{av:'Ddo_contratanteusuario_usuariocod_Filteredtextto_get',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratanteusuario_usuariocod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOCOD',prop:'SortedStatus'},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_contratantecod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTECOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_contratantefan_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_contratanteraz_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD.ONOPTIONCLICKED","{handler:'E168E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_usuariopessoacod_Activeeventkey',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD',prop:'ActiveEventKey'},{av:'Ddo_contratanteusuario_usuariopessoacod_Filteredtext_get',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD',prop:'FilteredText_get'},{av:'Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_get',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratanteusuario_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD',prop:'SortedStatus'},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_contratantecod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTECOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_contratantefan_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_contratanteraz_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariocod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM.ONOPTIONCLICKED","{handler:'E178E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_usuariopessoanom_Activeeventkey',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contratanteusuario_usuariopessoanom_Filteredtext_get',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_get',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratanteusuario_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'SortedStatus'},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratanteusuario_contratantecod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTECOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_contratantefan_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_contratanteraz_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariocod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contratanteusuario_usuariopessoacod_Sortedstatus',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E288E2',iparms:[{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV32Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV33Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtContratanteUsuario_ContratanteRaz_Link',ctrl:'CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'Link'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Link',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E188E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E238E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E198E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'edtavUsuario_nome2_Visible',ctrl:'vUSUARIO_NOME2',prop:'Visible'},{av:'edtavContratanteusuario_usuariopessoanom2_Visible',ctrl:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavUsuario_nome1_Visible',ctrl:'vUSUARIO_NOME1',prop:'Visible'},{av:'edtavContratanteusuario_usuariopessoanom1_Visible',ctrl:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E248E2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavUsuario_nome1_Visible',ctrl:'vUSUARIO_NOME1',prop:'Visible'},{av:'edtavContratanteusuario_usuariopessoanom1_Visible',ctrl:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E208E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'edtavUsuario_nome2_Visible',ctrl:'vUSUARIO_NOME2',prop:'Visible'},{av:'edtavContratanteusuario_usuariopessoanom2_Visible',ctrl:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavUsuario_nome1_Visible',ctrl:'vUSUARIO_NOME1',prop:'Visible'},{av:'edtavContratanteusuario_usuariopessoanom1_Visible',ctrl:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E258E2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavUsuario_nome2_Visible',ctrl:'vUSUARIO_NOME2',prop:'Visible'},{av:'edtavContratanteusuario_usuariopessoanom2_Visible',ctrl:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E218E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTEFANTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_CONTRATANTERAZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATANTEUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A340ContratanteUsuario_ContratantePesCod',fld:'CONTRATANTEUSUARIO_CONTRATANTEPESCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV61TFContratanteUsuario_ContratanteCod',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_contratantecod_Filteredtext_set',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTECOD',prop:'FilteredText_set'},{av:'AV62TFContratanteUsuario_ContratanteCod_To',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_contratantecod_Filteredtextto_set',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTECOD',prop:'FilteredTextTo_set'},{av:'AV65TFContratanteUsuario_ContratanteFan',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'Ddo_contratanteusuario_contratantefan_Filteredtext_set',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN',prop:'FilteredText_set'},{av:'AV66TFContratanteUsuario_ContratanteFan_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL',pic:'@!',nv:''},{av:'Ddo_contratanteusuario_contratantefan_Selectedvalue_set',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN',prop:'SelectedValue_set'},{av:'AV69TFContratanteUsuario_ContratanteRaz',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'Ddo_contratanteusuario_contratanteraz_Filteredtext_set',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'FilteredText_set'},{av:'AV70TFContratanteUsuario_ContratanteRaz_Sel',fld:'vTFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL',pic:'@!',nv:''},{av:'Ddo_contratanteusuario_contratanteraz_Selectedvalue_set',ctrl:'DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ',prop:'SelectedValue_set'},{av:'AV73TFContratanteUsuario_UsuarioCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_usuariocod_Filteredtext_set',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOCOD',prop:'FilteredText_set'},{av:'AV74TFContratanteUsuario_UsuarioCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_usuariocod_Filteredtextto_set',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOCOD',prop:'FilteredTextTo_set'},{av:'AV77TFContratanteUsuario_UsuarioPessoaCod',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_usuariopessoacod_Filteredtext_set',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD',prop:'FilteredText_set'},{av:'AV78TFContratanteUsuario_UsuarioPessoaCod_To',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_set',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOACOD',prop:'FilteredTextTo_set'},{av:'AV81TFContratanteUsuario_UsuarioPessoaNom',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'Ddo_contratanteusuario_usuariopessoanom_Filteredtext_set',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'FilteredText_set'},{av:'AV82TFContratanteUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_set',ctrl:'DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavUsuario_nome1_Visible',ctrl:'vUSUARIO_NOME1',prop:'Visible'},{av:'edtavContratanteusuario_usuariopessoanom1_Visible',ctrl:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV19ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContratanteUsuario_UsuarioPessoaNom2',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'edtavUsuario_nome2_Visible',ctrl:'vUSUARIO_NOME2',prop:'Visible'},{av:'edtavContratanteusuario_usuariopessoanom2_Visible',ctrl:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E228E2',iparms:[{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratanteusuario_contratantecod_Activeeventkey = "";
         Ddo_contratanteusuario_contratantecod_Filteredtext_get = "";
         Ddo_contratanteusuario_contratantecod_Filteredtextto_get = "";
         Ddo_contratanteusuario_contratantefan_Activeeventkey = "";
         Ddo_contratanteusuario_contratantefan_Filteredtext_get = "";
         Ddo_contratanteusuario_contratantefan_Selectedvalue_get = "";
         Ddo_contratanteusuario_contratanteraz_Activeeventkey = "";
         Ddo_contratanteusuario_contratanteraz_Filteredtext_get = "";
         Ddo_contratanteusuario_contratanteraz_Selectedvalue_get = "";
         Ddo_contratanteusuario_usuariocod_Activeeventkey = "";
         Ddo_contratanteusuario_usuariocod_Filteredtext_get = "";
         Ddo_contratanteusuario_usuariocod_Filteredtextto_get = "";
         Ddo_contratanteusuario_usuariopessoacod_Activeeventkey = "";
         Ddo_contratanteusuario_usuariopessoacod_Filteredtext_get = "";
         Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_get = "";
         Ddo_contratanteusuario_usuariopessoanom_Activeeventkey = "";
         Ddo_contratanteusuario_usuariopessoanom_Filteredtext_get = "";
         Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV56Usuario_Nome1 = "";
         AV19ContratanteUsuario_UsuarioPessoaNom1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV57Usuario_Nome2 = "";
         AV24ContratanteUsuario_UsuarioPessoaNom2 = "";
         AV65TFContratanteUsuario_ContratanteFan = "";
         AV66TFContratanteUsuario_ContratanteFan_Sel = "";
         AV69TFContratanteUsuario_ContratanteRaz = "";
         AV70TFContratanteUsuario_ContratanteRaz_Sel = "";
         AV81TFContratanteUsuario_UsuarioPessoaNom = "";
         AV82TFContratanteUsuario_UsuarioPessoaNom_Sel = "";
         AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace = "";
         AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace = "";
         AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace = "";
         AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace = "";
         AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace = "";
         AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace = "";
         AV113Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV84DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV60ContratanteUsuario_ContratanteCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64ContratanteUsuario_ContratanteFanTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68ContratanteUsuario_ContratanteRazTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72ContratanteUsuario_UsuarioCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76ContratanteUsuario_UsuarioPessoaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV80ContratanteUsuario_UsuarioPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratanteusuario_contratantecod_Filteredtext_set = "";
         Ddo_contratanteusuario_contratantecod_Filteredtextto_set = "";
         Ddo_contratanteusuario_contratantecod_Sortedstatus = "";
         Ddo_contratanteusuario_contratantefan_Filteredtext_set = "";
         Ddo_contratanteusuario_contratantefan_Selectedvalue_set = "";
         Ddo_contratanteusuario_contratantefan_Sortedstatus = "";
         Ddo_contratanteusuario_contratanteraz_Filteredtext_set = "";
         Ddo_contratanteusuario_contratanteraz_Selectedvalue_set = "";
         Ddo_contratanteusuario_contratanteraz_Sortedstatus = "";
         Ddo_contratanteusuario_usuariocod_Filteredtext_set = "";
         Ddo_contratanteusuario_usuariocod_Filteredtextto_set = "";
         Ddo_contratanteusuario_usuariocod_Sortedstatus = "";
         Ddo_contratanteusuario_usuariopessoacod_Filteredtext_set = "";
         Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_set = "";
         Ddo_contratanteusuario_usuariopessoacod_Sortedstatus = "";
         Ddo_contratanteusuario_usuariopessoanom_Filteredtext_set = "";
         Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_set = "";
         Ddo_contratanteusuario_usuariopessoanom_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV32Update = "";
         AV111Update_GXI = "";
         AV33Delete = "";
         AV112Delete_GXI = "";
         A65ContratanteUsuario_ContratanteFan = "";
         A64ContratanteUsuario_ContratanteRaz = "";
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV92WWContratanteUsuarioDS_3_Usuario_nome1 = "";
         lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = "";
         lV97WWContratanteUsuarioDS_8_Usuario_nome2 = "";
         lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = "";
         lV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = "";
         lV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = "";
         lV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = "";
         AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1 = "";
         AV92WWContratanteUsuarioDS_3_Usuario_nome1 = "";
         AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = "";
         AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2 = "";
         AV97WWContratanteUsuarioDS_8_Usuario_nome2 = "";
         AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = "";
         AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel = "";
         AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = "";
         AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel = "";
         AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = "";
         AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel = "";
         AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = "";
         A2Usuario_Nome = "";
         H008E2_A2Usuario_Nome = new String[] {""} ;
         H008E2_n2Usuario_Nome = new bool[] {false} ;
         H008E2_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         H008E2_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         H008E2_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H008E2_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H008E2_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H008E2_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H008E2_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H008E2_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         H008E2_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         H008E2_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         H008E2_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         H008E2_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H008E3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV54Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratanteusuariotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratanteusuario__default(),
            new Object[][] {
                new Object[] {
               H008E2_A2Usuario_Nome, H008E2_n2Usuario_Nome, H008E2_A340ContratanteUsuario_ContratantePesCod, H008E2_n340ContratanteUsuario_ContratantePesCod, H008E2_A62ContratanteUsuario_UsuarioPessoaNom, H008E2_n62ContratanteUsuario_UsuarioPessoaNom, H008E2_A61ContratanteUsuario_UsuarioPessoaCod, H008E2_n61ContratanteUsuario_UsuarioPessoaCod, H008E2_A60ContratanteUsuario_UsuarioCod, H008E2_A64ContratanteUsuario_ContratanteRaz,
               H008E2_n64ContratanteUsuario_ContratanteRaz, H008E2_A65ContratanteUsuario_ContratanteFan, H008E2_n65ContratanteUsuario_ContratanteFan, H008E2_A63ContratanteUsuario_ContratanteCod
               }
               , new Object[] {
               H008E3_AGRID_nRecordCount
               }
            }
         );
         AV113Pgmname = "WWContratanteUsuario";
         /* GeneXus formulas. */
         AV113Pgmname = "WWContratanteUsuario";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_73 ;
      private short nGXsfl_73_idx=1 ;
      private short AV13OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_73_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 ;
      private short AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 ;
      private short edtContratanteUsuario_ContratanteCod_Titleformat ;
      private short edtContratanteUsuario_ContratanteFan_Titleformat ;
      private short edtContratanteUsuario_ContratanteRaz_Titleformat ;
      private short edtContratanteUsuario_UsuarioCod_Titleformat ;
      private short edtContratanteUsuario_UsuarioPessoaCod_Titleformat ;
      private short edtContratanteUsuario_UsuarioPessoaNom_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV61TFContratanteUsuario_ContratanteCod ;
      private int AV62TFContratanteUsuario_ContratanteCod_To ;
      private int AV73TFContratanteUsuario_UsuarioCod ;
      private int AV74TFContratanteUsuario_UsuarioCod_To ;
      private int AV77TFContratanteUsuario_UsuarioPessoaCod ;
      private int AV78TFContratanteUsuario_UsuarioPessoaCod_To ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A340ContratanteUsuario_ContratantePesCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratanteusuario_contratantefan_Datalistupdateminimumcharacters ;
      private int Ddo_contratanteusuario_contratanteraz_Datalistupdateminimumcharacters ;
      private int Ddo_contratanteusuario_usuariopessoanom_Datalistupdateminimumcharacters ;
      private int edtavTfcontratanteusuario_contratantecod_Visible ;
      private int edtavTfcontratanteusuario_contratantecod_to_Visible ;
      private int edtavTfcontratanteusuario_contratantefan_Visible ;
      private int edtavTfcontratanteusuario_contratantefan_sel_Visible ;
      private int edtavTfcontratanteusuario_contratanteraz_Visible ;
      private int edtavTfcontratanteusuario_contratanteraz_sel_Visible ;
      private int edtavTfcontratanteusuario_usuariocod_Visible ;
      private int edtavTfcontratanteusuario_usuariocod_to_Visible ;
      private int edtavTfcontratanteusuario_usuariopessoacod_Visible ;
      private int edtavTfcontratanteusuario_usuariopessoacod_to_Visible ;
      private int edtavTfcontratanteusuario_usuariopessoanom_Visible ;
      private int edtavTfcontratanteusuario_usuariopessoanom_sel_Visible ;
      private int edtavDdo_contratanteusuario_contratantecodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratanteusuario_contratantefantitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratanteusuario_contratanteraztitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratanteusuario_usuariocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratanteusuario_usuariopessoacodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratanteusuario_usuariopessoanomtitlecontrolidtoreplace_Visible ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod ;
      private int AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to ;
      private int AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod ;
      private int AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to ;
      private int AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod ;
      private int AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV85PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavUsuario_nome1_Visible ;
      private int edtavContratanteusuario_usuariopessoanom1_Visible ;
      private int edtavUsuario_nome2_Visible ;
      private int edtavContratanteusuario_usuariopessoanom2_Visible ;
      private int AV114GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV86GridCurrentPage ;
      private long AV87GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratanteusuario_contratantecod_Activeeventkey ;
      private String Ddo_contratanteusuario_contratantecod_Filteredtext_get ;
      private String Ddo_contratanteusuario_contratantecod_Filteredtextto_get ;
      private String Ddo_contratanteusuario_contratantefan_Activeeventkey ;
      private String Ddo_contratanteusuario_contratantefan_Filteredtext_get ;
      private String Ddo_contratanteusuario_contratantefan_Selectedvalue_get ;
      private String Ddo_contratanteusuario_contratanteraz_Activeeventkey ;
      private String Ddo_contratanteusuario_contratanteraz_Filteredtext_get ;
      private String Ddo_contratanteusuario_contratanteraz_Selectedvalue_get ;
      private String Ddo_contratanteusuario_usuariocod_Activeeventkey ;
      private String Ddo_contratanteusuario_usuariocod_Filteredtext_get ;
      private String Ddo_contratanteusuario_usuariocod_Filteredtextto_get ;
      private String Ddo_contratanteusuario_usuariopessoacod_Activeeventkey ;
      private String Ddo_contratanteusuario_usuariopessoacod_Filteredtext_get ;
      private String Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_get ;
      private String Ddo_contratanteusuario_usuariopessoanom_Activeeventkey ;
      private String Ddo_contratanteusuario_usuariopessoanom_Filteredtext_get ;
      private String Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_73_idx="0001" ;
      private String AV56Usuario_Nome1 ;
      private String AV19ContratanteUsuario_UsuarioPessoaNom1 ;
      private String AV57Usuario_Nome2 ;
      private String AV24ContratanteUsuario_UsuarioPessoaNom2 ;
      private String AV65TFContratanteUsuario_ContratanteFan ;
      private String AV66TFContratanteUsuario_ContratanteFan_Sel ;
      private String AV69TFContratanteUsuario_ContratanteRaz ;
      private String AV70TFContratanteUsuario_ContratanteRaz_Sel ;
      private String AV81TFContratanteUsuario_UsuarioPessoaNom ;
      private String AV82TFContratanteUsuario_UsuarioPessoaNom_Sel ;
      private String AV113Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratanteusuario_contratantecod_Caption ;
      private String Ddo_contratanteusuario_contratantecod_Tooltip ;
      private String Ddo_contratanteusuario_contratantecod_Cls ;
      private String Ddo_contratanteusuario_contratantecod_Filteredtext_set ;
      private String Ddo_contratanteusuario_contratantecod_Filteredtextto_set ;
      private String Ddo_contratanteusuario_contratantecod_Dropdownoptionstype ;
      private String Ddo_contratanteusuario_contratantecod_Titlecontrolidtoreplace ;
      private String Ddo_contratanteusuario_contratantecod_Sortedstatus ;
      private String Ddo_contratanteusuario_contratantecod_Filtertype ;
      private String Ddo_contratanteusuario_contratantecod_Sortasc ;
      private String Ddo_contratanteusuario_contratantecod_Sortdsc ;
      private String Ddo_contratanteusuario_contratantecod_Cleanfilter ;
      private String Ddo_contratanteusuario_contratantecod_Rangefilterfrom ;
      private String Ddo_contratanteusuario_contratantecod_Rangefilterto ;
      private String Ddo_contratanteusuario_contratantecod_Searchbuttontext ;
      private String Ddo_contratanteusuario_contratantefan_Caption ;
      private String Ddo_contratanteusuario_contratantefan_Tooltip ;
      private String Ddo_contratanteusuario_contratantefan_Cls ;
      private String Ddo_contratanteusuario_contratantefan_Filteredtext_set ;
      private String Ddo_contratanteusuario_contratantefan_Selectedvalue_set ;
      private String Ddo_contratanteusuario_contratantefan_Dropdownoptionstype ;
      private String Ddo_contratanteusuario_contratantefan_Titlecontrolidtoreplace ;
      private String Ddo_contratanteusuario_contratantefan_Sortedstatus ;
      private String Ddo_contratanteusuario_contratantefan_Filtertype ;
      private String Ddo_contratanteusuario_contratantefan_Datalisttype ;
      private String Ddo_contratanteusuario_contratantefan_Datalistproc ;
      private String Ddo_contratanteusuario_contratantefan_Sortasc ;
      private String Ddo_contratanteusuario_contratantefan_Sortdsc ;
      private String Ddo_contratanteusuario_contratantefan_Loadingdata ;
      private String Ddo_contratanteusuario_contratantefan_Cleanfilter ;
      private String Ddo_contratanteusuario_contratantefan_Noresultsfound ;
      private String Ddo_contratanteusuario_contratantefan_Searchbuttontext ;
      private String Ddo_contratanteusuario_contratanteraz_Caption ;
      private String Ddo_contratanteusuario_contratanteraz_Tooltip ;
      private String Ddo_contratanteusuario_contratanteraz_Cls ;
      private String Ddo_contratanteusuario_contratanteraz_Filteredtext_set ;
      private String Ddo_contratanteusuario_contratanteraz_Selectedvalue_set ;
      private String Ddo_contratanteusuario_contratanteraz_Dropdownoptionstype ;
      private String Ddo_contratanteusuario_contratanteraz_Titlecontrolidtoreplace ;
      private String Ddo_contratanteusuario_contratanteraz_Sortedstatus ;
      private String Ddo_contratanteusuario_contratanteraz_Filtertype ;
      private String Ddo_contratanteusuario_contratanteraz_Datalisttype ;
      private String Ddo_contratanteusuario_contratanteraz_Datalistproc ;
      private String Ddo_contratanteusuario_contratanteraz_Sortasc ;
      private String Ddo_contratanteusuario_contratanteraz_Sortdsc ;
      private String Ddo_contratanteusuario_contratanteraz_Loadingdata ;
      private String Ddo_contratanteusuario_contratanteraz_Cleanfilter ;
      private String Ddo_contratanteusuario_contratanteraz_Noresultsfound ;
      private String Ddo_contratanteusuario_contratanteraz_Searchbuttontext ;
      private String Ddo_contratanteusuario_usuariocod_Caption ;
      private String Ddo_contratanteusuario_usuariocod_Tooltip ;
      private String Ddo_contratanteusuario_usuariocod_Cls ;
      private String Ddo_contratanteusuario_usuariocod_Filteredtext_set ;
      private String Ddo_contratanteusuario_usuariocod_Filteredtextto_set ;
      private String Ddo_contratanteusuario_usuariocod_Dropdownoptionstype ;
      private String Ddo_contratanteusuario_usuariocod_Titlecontrolidtoreplace ;
      private String Ddo_contratanteusuario_usuariocod_Sortedstatus ;
      private String Ddo_contratanteusuario_usuariocod_Filtertype ;
      private String Ddo_contratanteusuario_usuariocod_Sortasc ;
      private String Ddo_contratanteusuario_usuariocod_Sortdsc ;
      private String Ddo_contratanteusuario_usuariocod_Cleanfilter ;
      private String Ddo_contratanteusuario_usuariocod_Rangefilterfrom ;
      private String Ddo_contratanteusuario_usuariocod_Rangefilterto ;
      private String Ddo_contratanteusuario_usuariocod_Searchbuttontext ;
      private String Ddo_contratanteusuario_usuariopessoacod_Caption ;
      private String Ddo_contratanteusuario_usuariopessoacod_Tooltip ;
      private String Ddo_contratanteusuario_usuariopessoacod_Cls ;
      private String Ddo_contratanteusuario_usuariopessoacod_Filteredtext_set ;
      private String Ddo_contratanteusuario_usuariopessoacod_Filteredtextto_set ;
      private String Ddo_contratanteusuario_usuariopessoacod_Dropdownoptionstype ;
      private String Ddo_contratanteusuario_usuariopessoacod_Titlecontrolidtoreplace ;
      private String Ddo_contratanteusuario_usuariopessoacod_Sortedstatus ;
      private String Ddo_contratanteusuario_usuariopessoacod_Filtertype ;
      private String Ddo_contratanteusuario_usuariopessoacod_Sortasc ;
      private String Ddo_contratanteusuario_usuariopessoacod_Sortdsc ;
      private String Ddo_contratanteusuario_usuariopessoacod_Cleanfilter ;
      private String Ddo_contratanteusuario_usuariopessoacod_Rangefilterfrom ;
      private String Ddo_contratanteusuario_usuariopessoacod_Rangefilterto ;
      private String Ddo_contratanteusuario_usuariopessoacod_Searchbuttontext ;
      private String Ddo_contratanteusuario_usuariopessoanom_Caption ;
      private String Ddo_contratanteusuario_usuariopessoanom_Tooltip ;
      private String Ddo_contratanteusuario_usuariopessoanom_Cls ;
      private String Ddo_contratanteusuario_usuariopessoanom_Filteredtext_set ;
      private String Ddo_contratanteusuario_usuariopessoanom_Selectedvalue_set ;
      private String Ddo_contratanteusuario_usuariopessoanom_Dropdownoptionstype ;
      private String Ddo_contratanteusuario_usuariopessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contratanteusuario_usuariopessoanom_Sortedstatus ;
      private String Ddo_contratanteusuario_usuariopessoanom_Filtertype ;
      private String Ddo_contratanteusuario_usuariopessoanom_Datalisttype ;
      private String Ddo_contratanteusuario_usuariopessoanom_Datalistproc ;
      private String Ddo_contratanteusuario_usuariopessoanom_Sortasc ;
      private String Ddo_contratanteusuario_usuariopessoanom_Sortdsc ;
      private String Ddo_contratanteusuario_usuariopessoanom_Loadingdata ;
      private String Ddo_contratanteusuario_usuariopessoanom_Cleanfilter ;
      private String Ddo_contratanteusuario_usuariopessoanom_Noresultsfound ;
      private String Ddo_contratanteusuario_usuariopessoanom_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfcontratanteusuario_contratantecod_Internalname ;
      private String edtavTfcontratanteusuario_contratantecod_Jsonclick ;
      private String edtavTfcontratanteusuario_contratantecod_to_Internalname ;
      private String edtavTfcontratanteusuario_contratantecod_to_Jsonclick ;
      private String edtavTfcontratanteusuario_contratantefan_Internalname ;
      private String edtavTfcontratanteusuario_contratantefan_Jsonclick ;
      private String edtavTfcontratanteusuario_contratantefan_sel_Internalname ;
      private String edtavTfcontratanteusuario_contratantefan_sel_Jsonclick ;
      private String edtavTfcontratanteusuario_contratanteraz_Internalname ;
      private String edtavTfcontratanteusuario_contratanteraz_Jsonclick ;
      private String edtavTfcontratanteusuario_contratanteraz_sel_Internalname ;
      private String edtavTfcontratanteusuario_contratanteraz_sel_Jsonclick ;
      private String edtavTfcontratanteusuario_usuariocod_Internalname ;
      private String edtavTfcontratanteusuario_usuariocod_Jsonclick ;
      private String edtavTfcontratanteusuario_usuariocod_to_Internalname ;
      private String edtavTfcontratanteusuario_usuariocod_to_Jsonclick ;
      private String edtavTfcontratanteusuario_usuariopessoacod_Internalname ;
      private String edtavTfcontratanteusuario_usuariopessoacod_Jsonclick ;
      private String edtavTfcontratanteusuario_usuariopessoacod_to_Internalname ;
      private String edtavTfcontratanteusuario_usuariopessoacod_to_Jsonclick ;
      private String edtavTfcontratanteusuario_usuariopessoanom_Internalname ;
      private String edtavTfcontratanteusuario_usuariopessoanom_Jsonclick ;
      private String edtavTfcontratanteusuario_usuariopessoanom_sel_Internalname ;
      private String edtavTfcontratanteusuario_usuariopessoanom_sel_Jsonclick ;
      private String edtavDdo_contratanteusuario_contratantecodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratanteusuario_contratantefantitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratanteusuario_contratanteraztitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratanteusuario_usuariocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratanteusuario_usuariopessoacodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratanteusuario_usuariopessoanomtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContratanteUsuario_ContratanteCod_Internalname ;
      private String A65ContratanteUsuario_ContratanteFan ;
      private String edtContratanteUsuario_ContratanteFan_Internalname ;
      private String A64ContratanteUsuario_ContratanteRaz ;
      private String edtContratanteUsuario_ContratanteRaz_Internalname ;
      private String edtContratanteUsuario_UsuarioCod_Internalname ;
      private String edtContratanteUsuario_UsuarioPessoaCod_Internalname ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String edtContratanteUsuario_UsuarioPessoaNom_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV92WWContratanteUsuarioDS_3_Usuario_nome1 ;
      private String lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ;
      private String lV97WWContratanteUsuarioDS_8_Usuario_nome2 ;
      private String lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ;
      private String lV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ;
      private String lV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ;
      private String lV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ;
      private String AV92WWContratanteUsuarioDS_3_Usuario_nome1 ;
      private String AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ;
      private String AV97WWContratanteUsuarioDS_8_Usuario_nome2 ;
      private String AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ;
      private String AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel ;
      private String AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ;
      private String AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel ;
      private String AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ;
      private String AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel ;
      private String AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ;
      private String A2Usuario_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavUsuario_nome1_Internalname ;
      private String edtavContratanteusuario_usuariopessoanom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavUsuario_nome2_Internalname ;
      private String edtavContratanteusuario_usuariopessoanom2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratanteusuario_contratantecod_Internalname ;
      private String Ddo_contratanteusuario_contratantefan_Internalname ;
      private String Ddo_contratanteusuario_contratanteraz_Internalname ;
      private String Ddo_contratanteusuario_usuariocod_Internalname ;
      private String Ddo_contratanteusuario_usuariopessoacod_Internalname ;
      private String Ddo_contratanteusuario_usuariopessoanom_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratanteUsuario_ContratanteCod_Title ;
      private String edtContratanteUsuario_ContratanteFan_Title ;
      private String edtContratanteUsuario_ContratanteRaz_Title ;
      private String edtContratanteUsuario_UsuarioCod_Title ;
      private String edtContratanteUsuario_UsuarioPessoaCod_Title ;
      private String edtContratanteUsuario_UsuarioPessoaNom_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContratanteUsuario_ContratanteRaz_Link ;
      private String edtContratanteUsuario_UsuarioPessoaNom_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContratanteusuariotitle_Internalname ;
      private String lblContratanteusuariotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavUsuario_nome2_Jsonclick ;
      private String edtavContratanteusuario_usuariopessoanom2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavUsuario_nome1_Jsonclick ;
      private String edtavContratanteusuario_usuariopessoanom1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_73_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratanteUsuario_ContratanteCod_Jsonclick ;
      private String edtContratanteUsuario_ContratanteFan_Jsonclick ;
      private String edtContratanteUsuario_ContratanteRaz_Jsonclick ;
      private String edtContratanteUsuario_UsuarioCod_Jsonclick ;
      private String edtContratanteUsuario_UsuarioPessoaCod_Jsonclick ;
      private String edtContratanteUsuario_UsuarioPessoaNom_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV31DynamicFiltersIgnoreFirst ;
      private bool AV30DynamicFiltersRemoving ;
      private bool n340ContratanteUsuario_ContratantePesCod ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratanteusuario_contratantecod_Includesortasc ;
      private bool Ddo_contratanteusuario_contratantecod_Includesortdsc ;
      private bool Ddo_contratanteusuario_contratantecod_Includefilter ;
      private bool Ddo_contratanteusuario_contratantecod_Filterisrange ;
      private bool Ddo_contratanteusuario_contratantecod_Includedatalist ;
      private bool Ddo_contratanteusuario_contratantefan_Includesortasc ;
      private bool Ddo_contratanteusuario_contratantefan_Includesortdsc ;
      private bool Ddo_contratanteusuario_contratantefan_Includefilter ;
      private bool Ddo_contratanteusuario_contratantefan_Filterisrange ;
      private bool Ddo_contratanteusuario_contratantefan_Includedatalist ;
      private bool Ddo_contratanteusuario_contratanteraz_Includesortasc ;
      private bool Ddo_contratanteusuario_contratanteraz_Includesortdsc ;
      private bool Ddo_contratanteusuario_contratanteraz_Includefilter ;
      private bool Ddo_contratanteusuario_contratanteraz_Filterisrange ;
      private bool Ddo_contratanteusuario_contratanteraz_Includedatalist ;
      private bool Ddo_contratanteusuario_usuariocod_Includesortasc ;
      private bool Ddo_contratanteusuario_usuariocod_Includesortdsc ;
      private bool Ddo_contratanteusuario_usuariocod_Includefilter ;
      private bool Ddo_contratanteusuario_usuariocod_Filterisrange ;
      private bool Ddo_contratanteusuario_usuariocod_Includedatalist ;
      private bool Ddo_contratanteusuario_usuariopessoacod_Includesortasc ;
      private bool Ddo_contratanteusuario_usuariopessoacod_Includesortdsc ;
      private bool Ddo_contratanteusuario_usuariopessoacod_Includefilter ;
      private bool Ddo_contratanteusuario_usuariopessoacod_Filterisrange ;
      private bool Ddo_contratanteusuario_usuariopessoacod_Includedatalist ;
      private bool Ddo_contratanteusuario_usuariopessoanom_Includesortasc ;
      private bool Ddo_contratanteusuario_usuariopessoanom_Includesortdsc ;
      private bool Ddo_contratanteusuario_usuariopessoanom_Includefilter ;
      private bool Ddo_contratanteusuario_usuariopessoanom_Filterisrange ;
      private bool Ddo_contratanteusuario_usuariopessoanom_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n65ContratanteUsuario_ContratanteFan ;
      private bool n64ContratanteUsuario_ContratanteRaz ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 ;
      private bool n2Usuario_Nome ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV32Update_IsBlob ;
      private bool AV33Delete_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV63ddo_ContratanteUsuario_ContratanteCodTitleControlIdToReplace ;
      private String AV67ddo_ContratanteUsuario_ContratanteFanTitleControlIdToReplace ;
      private String AV71ddo_ContratanteUsuario_ContratanteRazTitleControlIdToReplace ;
      private String AV75ddo_ContratanteUsuario_UsuarioCodTitleControlIdToReplace ;
      private String AV79ddo_ContratanteUsuario_UsuarioPessoaCodTitleControlIdToReplace ;
      private String AV83ddo_ContratanteUsuario_UsuarioPessoaNomTitleControlIdToReplace ;
      private String AV111Update_GXI ;
      private String AV112Delete_GXI ;
      private String AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1 ;
      private String AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2 ;
      private String AV32Update ;
      private String AV33Delete ;
      private IGxSession AV54Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private String[] H008E2_A2Usuario_Nome ;
      private bool[] H008E2_n2Usuario_Nome ;
      private int[] H008E2_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] H008E2_n340ContratanteUsuario_ContratantePesCod ;
      private String[] H008E2_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H008E2_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] H008E2_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H008E2_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H008E2_A60ContratanteUsuario_UsuarioCod ;
      private String[] H008E2_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] H008E2_n64ContratanteUsuario_ContratanteRaz ;
      private String[] H008E2_A65ContratanteUsuario_ContratanteFan ;
      private bool[] H008E2_n65ContratanteUsuario_ContratanteFan ;
      private int[] H008E2_A63ContratanteUsuario_ContratanteCod ;
      private long[] H008E3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV60ContratanteUsuario_ContratanteCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64ContratanteUsuario_ContratanteFanTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV68ContratanteUsuario_ContratanteRazTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV72ContratanteUsuario_UsuarioCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV76ContratanteUsuario_UsuarioPessoaCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV80ContratanteUsuario_UsuarioPessoaNomTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV84DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontratanteusuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H008E2( IGxContext context ,
                                             String AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1 ,
                                             short AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 ,
                                             String AV92WWContratanteUsuarioDS_3_Usuario_nome1 ,
                                             String AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ,
                                             bool AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 ,
                                             String AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2 ,
                                             short AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 ,
                                             String AV97WWContratanteUsuarioDS_8_Usuario_nome2 ,
                                             String AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ,
                                             int AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod ,
                                             int AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to ,
                                             String AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel ,
                                             String AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ,
                                             String AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel ,
                                             String AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ,
                                             int AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod ,
                                             int AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to ,
                                             int AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod ,
                                             int AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to ,
                                             String AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel ,
                                             String AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ,
                                             String A2Usuario_Nome ,
                                             String A62ContratanteUsuario_UsuarioPessoaNom ,
                                             int A63ContratanteUsuario_ContratanteCod ,
                                             String A65ContratanteUsuario_ContratanteFan ,
                                             String A64ContratanteUsuario_ContratanteRaz ,
                                             int A60ContratanteUsuario_UsuarioCod ,
                                             int A61ContratanteUsuario_UsuarioPessoaCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [25] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Usuario_Nome], T4.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T5.[Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz, T4.[Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod";
         sFromString = " FROM (((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Contratante] T4 WITH (NOLOCK) ON T4.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratante_PessoaCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContratanteUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV92WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV92WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContratanteUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like '%' + @lV92WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like '%' + @lV92WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWContratanteUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV97WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV97WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWContratanteUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like '%' + @lV97WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like '%' + @lV97WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] >= @AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] >= @AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] <= @AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] <= @AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] like @lV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] like @lV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] = @AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] = @AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (0==AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] >= @AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] >= @AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (0==AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] <= @AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] <= @AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (0==AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] >= @AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (0==AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] <= @AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_ContratanteCod]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_ContratanteCod] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Contratante_NomeFantasia]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Contratante_NomeFantasia] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_UsuarioCod]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_UsuarioCod] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Usuario_PessoaCod]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Usuario_PessoaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H008E3( IGxContext context ,
                                             String AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1 ,
                                             short AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 ,
                                             String AV92WWContratanteUsuarioDS_3_Usuario_nome1 ,
                                             String AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ,
                                             bool AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 ,
                                             String AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2 ,
                                             short AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 ,
                                             String AV97WWContratanteUsuarioDS_8_Usuario_nome2 ,
                                             String AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ,
                                             int AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod ,
                                             int AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to ,
                                             String AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel ,
                                             String AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ,
                                             String AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel ,
                                             String AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ,
                                             int AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod ,
                                             int AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to ,
                                             int AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod ,
                                             int AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to ,
                                             String AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel ,
                                             String AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ,
                                             String A2Usuario_Nome ,
                                             String A62ContratanteUsuario_UsuarioPessoaNom ,
                                             int A63ContratanteUsuario_ContratanteCod ,
                                             String A65ContratanteUsuario_ContratanteFan ,
                                             String A64ContratanteUsuario_ContratanteRaz ,
                                             int A60ContratanteUsuario_UsuarioCod ,
                                             int A61ContratanteUsuario_UsuarioPessoaCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [20] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) INNER JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContratanteUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV92WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV92WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContratanteUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV92WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV92WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV90WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV91WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWContratanteUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV97WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV97WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWContratanteUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV97WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV97WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV94WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV96WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] >= @AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] >= @AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] <= @AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] <= @AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] like @lV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_NomeFantasia] like @lV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] = @AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_NomeFantasia] = @AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (0==AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] >= @AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] >= @AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (0==AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] <= @AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] <= @AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (0==AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_PessoaCod] >= @AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_PessoaCod] >= @AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (0==AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_PessoaCod] <= @AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_PessoaCod] <= @AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H008E2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (short)dynConstraints[28] , (bool)dynConstraints[29] );
               case 1 :
                     return conditional_H008E3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (short)dynConstraints[28] , (bool)dynConstraints[29] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH008E2 ;
          prmH008E2 = new Object[] {
          new Object[] {"@lV92WWContratanteUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWContratanteUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV97WWContratanteUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWContratanteUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan",SqlDbType.Char,100,0} ,
          new Object[] {"@AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz",SqlDbType.Char,100,0} ,
          new Object[] {"@AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH008E3 ;
          prmH008E3 = new Object[] {
          new Object[] {"@lV92WWContratanteUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWContratanteUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV97WWContratanteUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWContratanteUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV98WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV99WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV100WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV101WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan",SqlDbType.Char,100,0} ,
          new Object[] {"@AV102WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV103WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz",SqlDbType.Char,100,0} ,
          new Object[] {"@AV104WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV105WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV106WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV107WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV108WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV109WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV110WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H008E2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008E2,11,0,true,false )
             ,new CursorDef("H008E3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008E3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                return;
       }
    }

 }

}
