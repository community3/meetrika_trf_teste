/*
               File: ContratoOcorrenciaNotificacao
        Description: Contrato Ocorrencia Notificacao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:0:40.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoocorrencianotificacao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_19") == 0 )
         {
            A303ContratoOcorrenciaNotificacao_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A303ContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_19( A303ContratoOcorrenciaNotificacao_Responsavel) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoOcorrenciaNotificacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOOCORRENCIANOTIFICACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9")));
               A294ContratoOcorrencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contrato Ocorrencia Notificacao", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoOcorrenciaNotificacao_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoocorrencianotificacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoocorrencianotificacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoOcorrenciaNotificacao_Codigo ,
                           ref int aP2_ContratoOcorrencia_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoOcorrenciaNotificacao_Codigo = aP1_ContratoOcorrenciaNotificacao_Codigo;
         this.A294ContratoOcorrencia_Codigo = aP2_ContratoOcorrencia_Codigo;
         executePrivate();
         aP2_ContratoOcorrencia_Codigo=this.A294ContratoOcorrencia_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1I51( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1I51e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0, ",", "")), ((edtContratoOcorrenciaNotificacao_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoOcorrenciaNotificacao_Codigo_Visible, edtContratoOcorrenciaNotificacao_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrencia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrencia_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoOcorrencia_Codigo_Visible, edtContratoOcorrencia_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")), ((edtContrato_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContrato_Codigo_Visible, edtContrato_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoOcorrenciaNotificacao.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1I51( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1I51( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1I51e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_66_1I51( true) ;
         }
         return  ;
      }

      protected void wb_table3_66_1I51e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1I51e( true) ;
         }
         else
         {
            wb_table1_2_1I51e( false) ;
         }
      }

      protected void wb_table3_66_1I51( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_66_1I51e( true) ;
         }
         else
         {
            wb_table3_66_1I51e( false) ;
         }
      }

      protected void wb_table2_5_1I51( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1I51( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1I51e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1I51e( true) ;
         }
         else
         {
            wb_table2_5_1I51e( false) ;
         }
      }

      protected void wb_table4_13_1I51( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N� Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Numero_Enabled, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencia_descricao_Internalname, "Ocorr�ncia", "", "", lblTextblockcontratoocorrencia_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrencia_Descricao_Internalname, A296ContratoOcorrencia_Descricao, StringUtil.RTrim( context.localUtil.Format( A296ContratoOcorrencia_Descricao, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrencia_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoOcorrencia_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencia_data_Internalname, "Data da Ocorr�ncia", "", "", lblTextblockcontratoocorrencia_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContratoOcorrencia_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrencia_Data_Internalname, context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"), context.localUtil.Format( A295ContratoOcorrencia_Data, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrencia_Data_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoOcorrencia_Data_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtContratoOcorrencia_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoOcorrencia_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_data_Internalname, "Data", "", "", lblTextblockcontratoocorrencianotificacao_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoOcorrenciaNotificacao_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Data_Internalname, context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"), context.localUtil.Format( A298ContratoOcorrenciaNotificacao_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Data_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoOcorrenciaNotificacao_Data_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtContratoOcorrenciaNotificacao_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoOcorrenciaNotificacao_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_descricao_Internalname, "Descri��o", "", "", lblTextblockcontratoocorrencianotificacao_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Descricao_Internalname, A300ContratoOcorrenciaNotificacao_Descricao, StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoOcorrenciaNotificacao_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_prazo_Internalname, "Prazo", "", "", lblTextblockcontratoocorrencianotificacao_prazo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0, ",", "")), ((edtContratoOcorrenciaNotificacao_Prazo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9")) : context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Prazo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoOcorrenciaNotificacao_Prazo_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_cumprido_Internalname, "Cumprido", "", "", lblTextblockcontratoocorrencianotificacao_cumprido_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoOcorrenciaNotificacao_Cumprido_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Cumprido_Internalname, context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"), context.localUtil.Format( A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoOcorrenciaNotificacao_Cumprido_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtContratoOcorrenciaNotificacao_Cumprido_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoOcorrenciaNotificacao_Cumprido_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_protocolo_Internalname, "Protocolo", "", "", lblTextblockcontratoocorrencianotificacao_protocolo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Protocolo_Internalname, A302ContratoOcorrenciaNotificacao_Protocolo, StringUtil.RTrim( context.localUtil.Format( A302ContratoOcorrenciaNotificacao_Protocolo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoOcorrenciaNotificacao_Protocolo_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_responsavel_Internalname, "Respons�vel", "", "", lblTextblockcontratoocorrencianotificacao_responsavel_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Responsavel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoOcorrenciaNotificacao_Responsavel_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoOcorrenciaNotificacao.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_303_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_303_Link, "", "", context.GetTheme( ), imgprompt_303_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_nome_Internalname, "Nome", "", "", lblTextblockcontratoocorrencianotificacao_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Nome_Internalname, StringUtil.RTrim( A304ContratoOcorrenciaNotificacao_Nome), StringUtil.RTrim( context.localUtil.Format( A304ContratoOcorrenciaNotificacao_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoOcorrenciaNotificacao_Nome_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_docto_Internalname, "Documento", "", "", lblTextblockcontratoocorrencianotificacao_docto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Docto_Internalname, A306ContratoOcorrenciaNotificacao_Docto, StringUtil.RTrim( context.localUtil.Format( A306ContratoOcorrenciaNotificacao_Docto, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Docto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoOcorrenciaNotificacao_Docto_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1I51e( true) ;
         }
         else
         {
            wb_table4_13_1I51e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111I2 */
         E111I2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               A296ContratoOcorrencia_Descricao = StringUtil.Upper( cgiGet( edtContratoOcorrencia_Descricao_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A296ContratoOcorrencia_Descricao", A296ContratoOcorrencia_Descricao);
               A295ContratoOcorrencia_Data = context.localUtil.CToD( cgiGet( edtContratoOcorrencia_Data_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A295ContratoOcorrencia_Data", context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"));
               if ( context.localUtil.VCDate( cgiGet( edtContratoOcorrenciaNotificacao_Data_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data de emiss�o"}), 1, "CONTRATOOCORRENCIANOTIFICACAO_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoOcorrenciaNotificacao_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A298ContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A298ContratoOcorrenciaNotificacao_Data", context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"));
               }
               else
               {
                  A298ContratoOcorrenciaNotificacao_Data = context.localUtil.CToD( cgiGet( edtContratoOcorrenciaNotificacao_Data_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A298ContratoOcorrenciaNotificacao_Data", context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"));
               }
               A300ContratoOcorrenciaNotificacao_Descricao = StringUtil.Upper( cgiGet( edtContratoOcorrenciaNotificacao_Descricao_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A300ContratoOcorrenciaNotificacao_Descricao", A300ContratoOcorrenciaNotificacao_Descricao);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Prazo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Prazo_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoOcorrenciaNotificacao_Prazo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A299ContratoOcorrenciaNotificacao_Prazo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A299ContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0)));
               }
               else
               {
                  A299ContratoOcorrenciaNotificacao_Prazo = (short)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Prazo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A299ContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0)));
               }
               if ( context.localUtil.VCDate( cgiGet( edtContratoOcorrenciaNotificacao_Cumprido_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data de cumprimento"}), 1, "CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoOcorrenciaNotificacao_Cumprido_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A301ContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
                  n301ContratoOcorrenciaNotificacao_Cumprido = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A301ContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
               }
               else
               {
                  A301ContratoOcorrenciaNotificacao_Cumprido = context.localUtil.CToD( cgiGet( edtContratoOcorrenciaNotificacao_Cumprido_Internalname), 2);
                  n301ContratoOcorrenciaNotificacao_Cumprido = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A301ContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
               }
               n301ContratoOcorrenciaNotificacao_Cumprido = ((DateTime.MinValue==A301ContratoOcorrenciaNotificacao_Cumprido) ? true : false);
               A302ContratoOcorrenciaNotificacao_Protocolo = cgiGet( edtContratoOcorrenciaNotificacao_Protocolo_Internalname);
               n302ContratoOcorrenciaNotificacao_Protocolo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A302ContratoOcorrenciaNotificacao_Protocolo", A302ContratoOcorrenciaNotificacao_Protocolo);
               n302ContratoOcorrenciaNotificacao_Protocolo = (String.IsNullOrEmpty(StringUtil.RTrim( A302ContratoOcorrenciaNotificacao_Protocolo)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Responsavel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Responsavel_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL");
                  AnyError = 1;
                  GX_FocusControl = edtContratoOcorrenciaNotificacao_Responsavel_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A303ContratoOcorrenciaNotificacao_Responsavel = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A303ContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
               }
               else
               {
                  A303ContratoOcorrenciaNotificacao_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Responsavel_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A303ContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
               }
               A304ContratoOcorrenciaNotificacao_Nome = StringUtil.Upper( cgiGet( edtContratoOcorrenciaNotificacao_Nome_Internalname));
               n304ContratoOcorrenciaNotificacao_Nome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A304ContratoOcorrenciaNotificacao_Nome", A304ContratoOcorrenciaNotificacao_Nome);
               A306ContratoOcorrenciaNotificacao_Docto = cgiGet( edtContratoOcorrenciaNotificacao_Docto_Internalname);
               n306ContratoOcorrenciaNotificacao_Docto = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A306ContratoOcorrenciaNotificacao_Docto", A306ContratoOcorrenciaNotificacao_Docto);
               A297ContratoOcorrenciaNotificacao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
               A294ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrencia_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               /* Read saved values. */
               Z297ContratoOcorrenciaNotificacao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z297ContratoOcorrenciaNotificacao_Codigo"), ",", "."));
               Z298ContratoOcorrenciaNotificacao_Data = context.localUtil.CToD( cgiGet( "Z298ContratoOcorrenciaNotificacao_Data"), 0);
               Z299ContratoOcorrenciaNotificacao_Prazo = (short)(context.localUtil.CToN( cgiGet( "Z299ContratoOcorrenciaNotificacao_Prazo"), ",", "."));
               Z300ContratoOcorrenciaNotificacao_Descricao = cgiGet( "Z300ContratoOcorrenciaNotificacao_Descricao");
               Z301ContratoOcorrenciaNotificacao_Cumprido = context.localUtil.CToD( cgiGet( "Z301ContratoOcorrenciaNotificacao_Cumprido"), 0);
               n301ContratoOcorrenciaNotificacao_Cumprido = ((DateTime.MinValue==A301ContratoOcorrenciaNotificacao_Cumprido) ? true : false);
               Z302ContratoOcorrenciaNotificacao_Protocolo = cgiGet( "Z302ContratoOcorrenciaNotificacao_Protocolo");
               n302ContratoOcorrenciaNotificacao_Protocolo = (String.IsNullOrEmpty(StringUtil.RTrim( A302ContratoOcorrenciaNotificacao_Protocolo)) ? true : false);
               Z303ContratoOcorrenciaNotificacao_Responsavel = (int)(context.localUtil.CToN( cgiGet( "Z303ContratoOcorrenciaNotificacao_Responsavel"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N294ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( "N294ContratoOcorrencia_Codigo"), ",", "."));
               N303ContratoOcorrenciaNotificacao_Responsavel = (int)(context.localUtil.CToN( cgiGet( "N303ContratoOcorrenciaNotificacao_Responsavel"), ",", "."));
               AV7ContratoOcorrenciaNotificacao_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_CODIGO"), ",", "."));
               AV11Insert_ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATOOCORRENCIA_CODIGO"), ",", "."));
               AV12Insert_ContratoOcorrenciaNotificacao_Responsavel = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL"), ",", "."));
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoOcorrenciaNotificacao";
               A297ContratoOcorrenciaNotificacao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A297ContratoOcorrenciaNotificacao_Codigo != Z297ContratoOcorrenciaNotificacao_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratoocorrencianotificacao:[SecurityCheckFailed value for]"+"ContratoOcorrenciaNotificacao_Codigo:"+context.localUtil.Format( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contratoocorrencianotificacao:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A297ContratoOcorrenciaNotificacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode51 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode51;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound51 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1I0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATOOCORRENCIANOTIFICACAO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContratoOcorrenciaNotificacao_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111I2 */
                           E111I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121I2 */
                           E121I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121I2 */
            E121I2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1I51( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1I51( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1I0( )
      {
         BeforeValidate1I51( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1I51( ) ;
            }
            else
            {
               CheckExtendedTable1I51( ) ;
               CloseExtendedTableCursors1I51( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1I0( )
      {
      }

      protected void E111I2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContratoOcorrencia_Codigo") == 0 )
               {
                  AV11Insert_ContratoOcorrencia_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ContratoOcorrencia_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContratoOcorrenciaNotificacao_Responsavel") == 0 )
               {
                  AV12Insert_ContratoOcorrenciaNotificacao_Responsavel = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_ContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_ContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtContratoOcorrenciaNotificacao_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Codigo_Visible), 5, 0)));
         edtContratoOcorrencia_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrencia_Codigo_Visible), 5, 0)));
         edtContrato_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Visible), 5, 0)));
      }

      protected void E121I2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratoocorrencianotificacao.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A294ContratoOcorrencia_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1I51( short GX_JID )
      {
         if ( ( GX_JID == 17 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z298ContratoOcorrenciaNotificacao_Data = T001I3_A298ContratoOcorrenciaNotificacao_Data[0];
               Z299ContratoOcorrenciaNotificacao_Prazo = T001I3_A299ContratoOcorrenciaNotificacao_Prazo[0];
               Z300ContratoOcorrenciaNotificacao_Descricao = T001I3_A300ContratoOcorrenciaNotificacao_Descricao[0];
               Z301ContratoOcorrenciaNotificacao_Cumprido = T001I3_A301ContratoOcorrenciaNotificacao_Cumprido[0];
               Z302ContratoOcorrenciaNotificacao_Protocolo = T001I3_A302ContratoOcorrenciaNotificacao_Protocolo[0];
               Z303ContratoOcorrenciaNotificacao_Responsavel = T001I3_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            }
            else
            {
               Z298ContratoOcorrenciaNotificacao_Data = A298ContratoOcorrenciaNotificacao_Data;
               Z299ContratoOcorrenciaNotificacao_Prazo = A299ContratoOcorrenciaNotificacao_Prazo;
               Z300ContratoOcorrenciaNotificacao_Descricao = A300ContratoOcorrenciaNotificacao_Descricao;
               Z301ContratoOcorrenciaNotificacao_Cumprido = A301ContratoOcorrenciaNotificacao_Cumprido;
               Z302ContratoOcorrenciaNotificacao_Protocolo = A302ContratoOcorrenciaNotificacao_Protocolo;
               Z303ContratoOcorrenciaNotificacao_Responsavel = A303ContratoOcorrenciaNotificacao_Responsavel;
            }
         }
         if ( GX_JID == -17 )
         {
            Z294ContratoOcorrencia_Codigo = A294ContratoOcorrencia_Codigo;
            Z297ContratoOcorrenciaNotificacao_Codigo = A297ContratoOcorrenciaNotificacao_Codigo;
            Z298ContratoOcorrenciaNotificacao_Data = A298ContratoOcorrenciaNotificacao_Data;
            Z299ContratoOcorrenciaNotificacao_Prazo = A299ContratoOcorrenciaNotificacao_Prazo;
            Z300ContratoOcorrenciaNotificacao_Descricao = A300ContratoOcorrenciaNotificacao_Descricao;
            Z301ContratoOcorrenciaNotificacao_Cumprido = A301ContratoOcorrenciaNotificacao_Cumprido;
            Z302ContratoOcorrenciaNotificacao_Protocolo = A302ContratoOcorrenciaNotificacao_Protocolo;
            Z303ContratoOcorrenciaNotificacao_Responsavel = A303ContratoOcorrenciaNotificacao_Responsavel;
            Z296ContratoOcorrencia_Descricao = A296ContratoOcorrencia_Descricao;
            Z295ContratoOcorrencia_Data = A295ContratoOcorrencia_Data;
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z77Contrato_Numero = A77Contrato_Numero;
            Z304ContratoOcorrenciaNotificacao_Nome = A304ContratoOcorrenciaNotificacao_Nome;
            Z306ContratoOcorrenciaNotificacao_Docto = A306ContratoOcorrenciaNotificacao_Docto;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContratoOcorrenciaNotificacao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Codigo_Enabled), 5, 0)));
         AV14Pgmname = "ContratoOcorrenciaNotificacao";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         imgprompt_303_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptpessoa.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL"+"'), id:'"+"CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"CONTRATOOCORRENCIANOTIFICACAO_NOME"+"'), id:'"+"CONTRATOOCORRENCIANOTIFICACAO_NOME"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         edtContratoOcorrenciaNotificacao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoOcorrenciaNotificacao_Codigo) )
         {
            A297ContratoOcorrenciaNotificacao_Codigo = AV7ContratoOcorrenciaNotificacao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
         }
         /* Using cursor T001I4 */
         pr_default.execute(2, new Object[] {A294ContratoOcorrencia_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Ocorr�ncia'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A296ContratoOcorrencia_Descricao = T001I4_A296ContratoOcorrencia_Descricao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A296ContratoOcorrencia_Descricao", A296ContratoOcorrencia_Descricao);
         A295ContratoOcorrencia_Data = T001I4_A295ContratoOcorrencia_Data[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A295ContratoOcorrencia_Data", context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"));
         A74Contrato_Codigo = T001I4_A74Contrato_Codigo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         pr_default.close(2);
         /* Using cursor T001I6 */
         pr_default.execute(4, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A77Contrato_Numero = T001I6_A77Contrato_Numero[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         pr_default.close(4);
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContratoOcorrencia_Codigo) )
         {
            edtContratoOcorrencia_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrencia_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtContratoOcorrencia_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrencia_Codigo_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_ContratoOcorrenciaNotificacao_Responsavel) )
         {
            edtContratoOcorrenciaNotificacao_Responsavel_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Responsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Responsavel_Enabled), 5, 0)));
         }
         else
         {
            edtContratoOcorrenciaNotificacao_Responsavel_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Responsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Responsavel_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_ContratoOcorrenciaNotificacao_Responsavel) )
         {
            A303ContratoOcorrenciaNotificacao_Responsavel = AV12Insert_ContratoOcorrenciaNotificacao_Responsavel;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A303ContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T001I5 */
            pr_default.execute(3, new Object[] {A303ContratoOcorrenciaNotificacao_Responsavel});
            A304ContratoOcorrenciaNotificacao_Nome = T001I5_A304ContratoOcorrenciaNotificacao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A304ContratoOcorrenciaNotificacao_Nome", A304ContratoOcorrenciaNotificacao_Nome);
            n304ContratoOcorrenciaNotificacao_Nome = T001I5_n304ContratoOcorrenciaNotificacao_Nome[0];
            A306ContratoOcorrenciaNotificacao_Docto = T001I5_A306ContratoOcorrenciaNotificacao_Docto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A306ContratoOcorrenciaNotificacao_Docto", A306ContratoOcorrenciaNotificacao_Docto);
            n306ContratoOcorrenciaNotificacao_Docto = T001I5_n306ContratoOcorrenciaNotificacao_Docto[0];
            pr_default.close(3);
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContratoOcorrencia_Codigo) )
            {
               A294ContratoOcorrencia_Codigo = AV11Insert_ContratoOcorrencia_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
            }
         }
      }

      protected void Load1I51( )
      {
         /* Using cursor T001I7 */
         pr_default.execute(5, new Object[] {A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound51 = 1;
            A296ContratoOcorrencia_Descricao = T001I7_A296ContratoOcorrencia_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A296ContratoOcorrencia_Descricao", A296ContratoOcorrencia_Descricao);
            A295ContratoOcorrencia_Data = T001I7_A295ContratoOcorrencia_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A295ContratoOcorrencia_Data", context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"));
            A77Contrato_Numero = T001I7_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A298ContratoOcorrenciaNotificacao_Data = T001I7_A298ContratoOcorrenciaNotificacao_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A298ContratoOcorrenciaNotificacao_Data", context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"));
            A299ContratoOcorrenciaNotificacao_Prazo = T001I7_A299ContratoOcorrenciaNotificacao_Prazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A299ContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0)));
            A300ContratoOcorrenciaNotificacao_Descricao = T001I7_A300ContratoOcorrenciaNotificacao_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A300ContratoOcorrenciaNotificacao_Descricao", A300ContratoOcorrenciaNotificacao_Descricao);
            A301ContratoOcorrenciaNotificacao_Cumprido = T001I7_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A301ContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
            n301ContratoOcorrenciaNotificacao_Cumprido = T001I7_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = T001I7_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A302ContratoOcorrenciaNotificacao_Protocolo", A302ContratoOcorrenciaNotificacao_Protocolo);
            n302ContratoOcorrenciaNotificacao_Protocolo = T001I7_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A304ContratoOcorrenciaNotificacao_Nome = T001I7_A304ContratoOcorrenciaNotificacao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A304ContratoOcorrenciaNotificacao_Nome", A304ContratoOcorrenciaNotificacao_Nome);
            n304ContratoOcorrenciaNotificacao_Nome = T001I7_n304ContratoOcorrenciaNotificacao_Nome[0];
            A306ContratoOcorrenciaNotificacao_Docto = T001I7_A306ContratoOcorrenciaNotificacao_Docto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A306ContratoOcorrenciaNotificacao_Docto", A306ContratoOcorrenciaNotificacao_Docto);
            n306ContratoOcorrenciaNotificacao_Docto = T001I7_n306ContratoOcorrenciaNotificacao_Docto[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = T001I7_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A303ContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
            A74Contrato_Codigo = T001I7_A74Contrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            ZM1I51( -17) ;
         }
         pr_default.close(5);
         OnLoadActions1I51( ) ;
      }

      protected void OnLoadActions1I51( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContratoOcorrencia_Codigo) )
         {
            A294ContratoOcorrencia_Codigo = AV11Insert_ContratoOcorrencia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
         }
      }

      protected void CheckExtendedTable1I51( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContratoOcorrencia_Codigo) )
         {
            A294ContratoOcorrencia_Codigo = AV11Insert_ContratoOcorrencia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
         }
         if ( ! ( (DateTime.MinValue==A298ContratoOcorrenciaNotificacao_Data) || ( A298ContratoOcorrenciaNotificacao_Data >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data de emiss�o fora do intervalo", "OutOfRange", 1, "CONTRATOOCORRENCIANOTIFICACAO_DATA");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrenciaNotificacao_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (DateTime.MinValue==A298ContratoOcorrenciaNotificacao_Data) )
         {
            GX_msglist.addItem("Data de emiss�o � obrigat�rio.", 1, "CONTRATOOCORRENCIANOTIFICACAO_DATA");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrenciaNotificacao_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (0==A299ContratoOcorrenciaNotificacao_Prazo) )
         {
            GX_msglist.addItem("Prazo de cumprimento � obrigat�rio.", 1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrenciaNotificacao_Prazo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A300ContratoOcorrenciaNotificacao_Descricao)) )
         {
            GX_msglist.addItem("Descri��o � obrigat�rio.", 1, "CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrenciaNotificacao_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A301ContratoOcorrenciaNotificacao_Cumprido) || ( A301ContratoOcorrenciaNotificacao_Cumprido >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data de cumprimento fora do intervalo", "OutOfRange", 1, "CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrenciaNotificacao_Cumprido_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T001I5 */
         pr_default.execute(3, new Object[] {A303ContratoOcorrenciaNotificacao_Responsavel});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contrato Ocorrencia Notificacao_Responsavel'.", "ForeignKeyNotFound", 1, "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrenciaNotificacao_Responsavel_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A304ContratoOcorrenciaNotificacao_Nome = T001I5_A304ContratoOcorrenciaNotificacao_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A304ContratoOcorrenciaNotificacao_Nome", A304ContratoOcorrenciaNotificacao_Nome);
         n304ContratoOcorrenciaNotificacao_Nome = T001I5_n304ContratoOcorrenciaNotificacao_Nome[0];
         A306ContratoOcorrenciaNotificacao_Docto = T001I5_A306ContratoOcorrenciaNotificacao_Docto[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A306ContratoOcorrenciaNotificacao_Docto", A306ContratoOcorrenciaNotificacao_Docto);
         n306ContratoOcorrenciaNotificacao_Docto = T001I5_n306ContratoOcorrenciaNotificacao_Docto[0];
         pr_default.close(3);
         if ( (0==A303ContratoOcorrenciaNotificacao_Responsavel) )
         {
            GX_msglist.addItem("C�digo da pessoa respons�vel � obrigat�rio.", 1, "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrenciaNotificacao_Responsavel_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors1I51( )
      {
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_19( int A303ContratoOcorrenciaNotificacao_Responsavel )
      {
         /* Using cursor T001I8 */
         pr_default.execute(6, new Object[] {A303ContratoOcorrenciaNotificacao_Responsavel});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contrato Ocorrencia Notificacao_Responsavel'.", "ForeignKeyNotFound", 1, "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrenciaNotificacao_Responsavel_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A304ContratoOcorrenciaNotificacao_Nome = T001I8_A304ContratoOcorrenciaNotificacao_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A304ContratoOcorrenciaNotificacao_Nome", A304ContratoOcorrenciaNotificacao_Nome);
         n304ContratoOcorrenciaNotificacao_Nome = T001I8_n304ContratoOcorrenciaNotificacao_Nome[0];
         A306ContratoOcorrenciaNotificacao_Docto = T001I8_A306ContratoOcorrenciaNotificacao_Docto[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A306ContratoOcorrenciaNotificacao_Docto", A306ContratoOcorrenciaNotificacao_Docto);
         n306ContratoOcorrenciaNotificacao_Docto = T001I8_n306ContratoOcorrenciaNotificacao_Docto[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A304ContratoOcorrenciaNotificacao_Nome))+"\""+","+"\""+GXUtil.EncodeJSConstant( A306ContratoOcorrenciaNotificacao_Docto)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey1I51( )
      {
         /* Using cursor T001I9 */
         pr_default.execute(7, new Object[] {A297ContratoOcorrenciaNotificacao_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound51 = 1;
         }
         else
         {
            RcdFound51 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001I3 */
         pr_default.execute(1, new Object[] {A297ContratoOcorrenciaNotificacao_Codigo});
         if ( (pr_default.getStatus(1) != 101) && ( T001I3_A294ContratoOcorrencia_Codigo[0] == A294ContratoOcorrencia_Codigo ) )
         {
            ZM1I51( 17) ;
            RcdFound51 = 1;
            A297ContratoOcorrenciaNotificacao_Codigo = T001I3_A297ContratoOcorrenciaNotificacao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
            A298ContratoOcorrenciaNotificacao_Data = T001I3_A298ContratoOcorrenciaNotificacao_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A298ContratoOcorrenciaNotificacao_Data", context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"));
            A299ContratoOcorrenciaNotificacao_Prazo = T001I3_A299ContratoOcorrenciaNotificacao_Prazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A299ContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0)));
            A300ContratoOcorrenciaNotificacao_Descricao = T001I3_A300ContratoOcorrenciaNotificacao_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A300ContratoOcorrenciaNotificacao_Descricao", A300ContratoOcorrenciaNotificacao_Descricao);
            A301ContratoOcorrenciaNotificacao_Cumprido = T001I3_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A301ContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
            n301ContratoOcorrenciaNotificacao_Cumprido = T001I3_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = T001I3_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A302ContratoOcorrenciaNotificacao_Protocolo", A302ContratoOcorrenciaNotificacao_Protocolo);
            n302ContratoOcorrenciaNotificacao_Protocolo = T001I3_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = T001I3_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A303ContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
            Z297ContratoOcorrenciaNotificacao_Codigo = A297ContratoOcorrenciaNotificacao_Codigo;
            sMode51 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1I51( ) ;
            if ( AnyError == 1 )
            {
               RcdFound51 = 0;
               InitializeNonKey1I51( ) ;
            }
            Gx_mode = sMode51;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound51 = 0;
            InitializeNonKey1I51( ) ;
            sMode51 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode51;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1I51( ) ;
         if ( RcdFound51 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound51 = 0;
         /* Using cursor T001I10 */
         pr_default.execute(8, new Object[] {A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T001I10_A297ContratoOcorrenciaNotificacao_Codigo[0] < A297ContratoOcorrenciaNotificacao_Codigo ) ) && ( T001I10_A294ContratoOcorrencia_Codigo[0] == A294ContratoOcorrencia_Codigo ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T001I10_A297ContratoOcorrenciaNotificacao_Codigo[0] > A297ContratoOcorrenciaNotificacao_Codigo ) ) && ( T001I10_A294ContratoOcorrencia_Codigo[0] == A294ContratoOcorrencia_Codigo ) )
            {
               A297ContratoOcorrenciaNotificacao_Codigo = T001I10_A297ContratoOcorrenciaNotificacao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
               RcdFound51 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound51 = 0;
         /* Using cursor T001I11 */
         pr_default.execute(9, new Object[] {A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T001I11_A297ContratoOcorrenciaNotificacao_Codigo[0] > A297ContratoOcorrenciaNotificacao_Codigo ) ) && ( T001I11_A294ContratoOcorrencia_Codigo[0] == A294ContratoOcorrencia_Codigo ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T001I11_A297ContratoOcorrenciaNotificacao_Codigo[0] < A297ContratoOcorrenciaNotificacao_Codigo ) ) && ( T001I11_A294ContratoOcorrencia_Codigo[0] == A294ContratoOcorrencia_Codigo ) )
            {
               A297ContratoOcorrenciaNotificacao_Codigo = T001I11_A297ContratoOcorrenciaNotificacao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
               RcdFound51 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1I51( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoOcorrenciaNotificacao_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1I51( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound51 == 1 )
            {
               if ( A297ContratoOcorrenciaNotificacao_Codigo != Z297ContratoOcorrenciaNotificacao_Codigo )
               {
                  A297ContratoOcorrenciaNotificacao_Codigo = Z297ContratoOcorrenciaNotificacao_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOOCORRENCIANOTIFICACAO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoOcorrenciaNotificacao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoOcorrenciaNotificacao_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1I51( ) ;
                  GX_FocusControl = edtContratoOcorrenciaNotificacao_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A297ContratoOcorrenciaNotificacao_Codigo != Z297ContratoOcorrenciaNotificacao_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContratoOcorrenciaNotificacao_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1I51( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOOCORRENCIANOTIFICACAO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContratoOcorrenciaNotificacao_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContratoOcorrenciaNotificacao_Data_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1I51( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A297ContratoOcorrenciaNotificacao_Codigo != Z297ContratoOcorrenciaNotificacao_Codigo )
         {
            A297ContratoOcorrenciaNotificacao_Codigo = Z297ContratoOcorrenciaNotificacao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOOCORRENCIANOTIFICACAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrenciaNotificacao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoOcorrenciaNotificacao_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1I51( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001I2 */
            pr_default.execute(0, new Object[] {A297ContratoOcorrenciaNotificacao_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoOcorrenciaNotificacao"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z298ContratoOcorrenciaNotificacao_Data != T001I2_A298ContratoOcorrenciaNotificacao_Data[0] ) || ( Z299ContratoOcorrenciaNotificacao_Prazo != T001I2_A299ContratoOcorrenciaNotificacao_Prazo[0] ) || ( StringUtil.StrCmp(Z300ContratoOcorrenciaNotificacao_Descricao, T001I2_A300ContratoOcorrenciaNotificacao_Descricao[0]) != 0 ) || ( Z301ContratoOcorrenciaNotificacao_Cumprido != T001I2_A301ContratoOcorrenciaNotificacao_Cumprido[0] ) || ( StringUtil.StrCmp(Z302ContratoOcorrenciaNotificacao_Protocolo, T001I2_A302ContratoOcorrenciaNotificacao_Protocolo[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z303ContratoOcorrenciaNotificacao_Responsavel != T001I2_A303ContratoOcorrenciaNotificacao_Responsavel[0] ) )
            {
               if ( Z298ContratoOcorrenciaNotificacao_Data != T001I2_A298ContratoOcorrenciaNotificacao_Data[0] )
               {
                  GXUtil.WriteLog("contratoocorrencianotificacao:[seudo value changed for attri]"+"ContratoOcorrenciaNotificacao_Data");
                  GXUtil.WriteLogRaw("Old: ",Z298ContratoOcorrenciaNotificacao_Data);
                  GXUtil.WriteLogRaw("Current: ",T001I2_A298ContratoOcorrenciaNotificacao_Data[0]);
               }
               if ( Z299ContratoOcorrenciaNotificacao_Prazo != T001I2_A299ContratoOcorrenciaNotificacao_Prazo[0] )
               {
                  GXUtil.WriteLog("contratoocorrencianotificacao:[seudo value changed for attri]"+"ContratoOcorrenciaNotificacao_Prazo");
                  GXUtil.WriteLogRaw("Old: ",Z299ContratoOcorrenciaNotificacao_Prazo);
                  GXUtil.WriteLogRaw("Current: ",T001I2_A299ContratoOcorrenciaNotificacao_Prazo[0]);
               }
               if ( StringUtil.StrCmp(Z300ContratoOcorrenciaNotificacao_Descricao, T001I2_A300ContratoOcorrenciaNotificacao_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoocorrencianotificacao:[seudo value changed for attri]"+"ContratoOcorrenciaNotificacao_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z300ContratoOcorrenciaNotificacao_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T001I2_A300ContratoOcorrenciaNotificacao_Descricao[0]);
               }
               if ( Z301ContratoOcorrenciaNotificacao_Cumprido != T001I2_A301ContratoOcorrenciaNotificacao_Cumprido[0] )
               {
                  GXUtil.WriteLog("contratoocorrencianotificacao:[seudo value changed for attri]"+"ContratoOcorrenciaNotificacao_Cumprido");
                  GXUtil.WriteLogRaw("Old: ",Z301ContratoOcorrenciaNotificacao_Cumprido);
                  GXUtil.WriteLogRaw("Current: ",T001I2_A301ContratoOcorrenciaNotificacao_Cumprido[0]);
               }
               if ( StringUtil.StrCmp(Z302ContratoOcorrenciaNotificacao_Protocolo, T001I2_A302ContratoOcorrenciaNotificacao_Protocolo[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoocorrencianotificacao:[seudo value changed for attri]"+"ContratoOcorrenciaNotificacao_Protocolo");
                  GXUtil.WriteLogRaw("Old: ",Z302ContratoOcorrenciaNotificacao_Protocolo);
                  GXUtil.WriteLogRaw("Current: ",T001I2_A302ContratoOcorrenciaNotificacao_Protocolo[0]);
               }
               if ( Z303ContratoOcorrenciaNotificacao_Responsavel != T001I2_A303ContratoOcorrenciaNotificacao_Responsavel[0] )
               {
                  GXUtil.WriteLog("contratoocorrencianotificacao:[seudo value changed for attri]"+"ContratoOcorrenciaNotificacao_Responsavel");
                  GXUtil.WriteLogRaw("Old: ",Z303ContratoOcorrenciaNotificacao_Responsavel);
                  GXUtil.WriteLogRaw("Current: ",T001I2_A303ContratoOcorrenciaNotificacao_Responsavel[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoOcorrenciaNotificacao"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1I51( )
      {
         BeforeValidate1I51( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1I51( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1I51( 0) ;
            CheckOptimisticConcurrency1I51( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1I51( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1I51( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001I12 */
                     pr_default.execute(10, new Object[] {A294ContratoOcorrencia_Codigo, A298ContratoOcorrenciaNotificacao_Data, A299ContratoOcorrenciaNotificacao_Prazo, A300ContratoOcorrenciaNotificacao_Descricao, n301ContratoOcorrenciaNotificacao_Cumprido, A301ContratoOcorrenciaNotificacao_Cumprido, n302ContratoOcorrenciaNotificacao_Protocolo, A302ContratoOcorrenciaNotificacao_Protocolo, A303ContratoOcorrenciaNotificacao_Responsavel});
                     A297ContratoOcorrenciaNotificacao_Codigo = T001I12_A297ContratoOcorrenciaNotificacao_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoOcorrenciaNotificacao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1I0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1I51( ) ;
            }
            EndLevel1I51( ) ;
         }
         CloseExtendedTableCursors1I51( ) ;
      }

      protected void Update1I51( )
      {
         BeforeValidate1I51( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1I51( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1I51( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1I51( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1I51( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001I13 */
                     pr_default.execute(11, new Object[] {A294ContratoOcorrencia_Codigo, A298ContratoOcorrenciaNotificacao_Data, A299ContratoOcorrenciaNotificacao_Prazo, A300ContratoOcorrenciaNotificacao_Descricao, n301ContratoOcorrenciaNotificacao_Cumprido, A301ContratoOcorrenciaNotificacao_Cumprido, n302ContratoOcorrenciaNotificacao_Protocolo, A302ContratoOcorrenciaNotificacao_Protocolo, A303ContratoOcorrenciaNotificacao_Responsavel, A297ContratoOcorrenciaNotificacao_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoOcorrenciaNotificacao") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoOcorrenciaNotificacao"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1I51( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1I51( ) ;
         }
         CloseExtendedTableCursors1I51( ) ;
      }

      protected void DeferredUpdate1I51( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1I51( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1I51( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1I51( ) ;
            AfterConfirm1I51( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1I51( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001I14 */
                  pr_default.execute(12, new Object[] {A297ContratoOcorrenciaNotificacao_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoOcorrenciaNotificacao") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode51 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1I51( ) ;
         Gx_mode = sMode51;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1I51( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001I15 */
            pr_default.execute(13, new Object[] {A303ContratoOcorrenciaNotificacao_Responsavel});
            A304ContratoOcorrenciaNotificacao_Nome = T001I15_A304ContratoOcorrenciaNotificacao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A304ContratoOcorrenciaNotificacao_Nome", A304ContratoOcorrenciaNotificacao_Nome);
            n304ContratoOcorrenciaNotificacao_Nome = T001I15_n304ContratoOcorrenciaNotificacao_Nome[0];
            A306ContratoOcorrenciaNotificacao_Docto = T001I15_A306ContratoOcorrenciaNotificacao_Docto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A306ContratoOcorrenciaNotificacao_Docto", A306ContratoOcorrenciaNotificacao_Docto);
            n306ContratoOcorrenciaNotificacao_Docto = T001I15_n306ContratoOcorrenciaNotificacao_Docto[0];
            pr_default.close(13);
         }
      }

      protected void EndLevel1I51( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1I51( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            context.CommitDataStores( "ContratoOcorrenciaNotificacao");
            if ( AnyError == 0 )
            {
               ConfirmValues1I0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            context.RollbackDataStores( "ContratoOcorrenciaNotificacao");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1I51( )
      {
         /* Scan By routine */
         /* Using cursor T001I16 */
         pr_default.execute(14, new Object[] {A294ContratoOcorrencia_Codigo});
         RcdFound51 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound51 = 1;
            A297ContratoOcorrenciaNotificacao_Codigo = T001I16_A297ContratoOcorrenciaNotificacao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1I51( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound51 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound51 = 1;
            A297ContratoOcorrenciaNotificacao_Codigo = T001I16_A297ContratoOcorrenciaNotificacao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1I51( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm1I51( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1I51( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1I51( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1I51( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1I51( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1I51( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1I51( )
      {
         edtContrato_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Numero_Enabled), 5, 0)));
         edtContratoOcorrencia_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrencia_Descricao_Enabled), 5, 0)));
         edtContratoOcorrencia_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrencia_Data_Enabled), 5, 0)));
         edtContratoOcorrenciaNotificacao_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Data_Enabled), 5, 0)));
         edtContratoOcorrenciaNotificacao_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Descricao_Enabled), 5, 0)));
         edtContratoOcorrenciaNotificacao_Prazo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Prazo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Prazo_Enabled), 5, 0)));
         edtContratoOcorrenciaNotificacao_Cumprido_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Cumprido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Cumprido_Enabled), 5, 0)));
         edtContratoOcorrenciaNotificacao_Protocolo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Protocolo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Protocolo_Enabled), 5, 0)));
         edtContratoOcorrenciaNotificacao_Responsavel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Responsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Responsavel_Enabled), 5, 0)));
         edtContratoOcorrenciaNotificacao_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Nome_Enabled), 5, 0)));
         edtContratoOcorrenciaNotificacao_Docto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Docto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Docto_Enabled), 5, 0)));
         edtContratoOcorrenciaNotificacao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrenciaNotificacao_Codigo_Enabled), 5, 0)));
         edtContratoOcorrencia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrencia_Codigo_Enabled), 5, 0)));
         edtContrato_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1I0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282304244");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoocorrencianotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoOcorrenciaNotificacao_Codigo) + "," + UrlEncode("" +A294ContratoOcorrencia_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z297ContratoOcorrenciaNotificacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z298ContratoOcorrenciaNotificacao_Data", context.localUtil.DToC( Z298ContratoOcorrenciaNotificacao_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z299ContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z299ContratoOcorrenciaNotificacao_Prazo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z300ContratoOcorrenciaNotificacao_Descricao", Z300ContratoOcorrenciaNotificacao_Descricao);
         GxWebStd.gx_hidden_field( context, "Z301ContratoOcorrenciaNotificacao_Cumprido", context.localUtil.DToC( Z301ContratoOcorrenciaNotificacao_Cumprido, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z302ContratoOcorrenciaNotificacao_Protocolo", Z302ContratoOcorrenciaNotificacao_Protocolo);
         GxWebStd.gx_hidden_field( context, "Z303ContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z303ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N303ContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.NToC( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOOCORRENCIANOTIFICACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoOcorrenciaNotificacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATOOCORRENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ContratoOcorrencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOOCORRENCIANOTIFICACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoOcorrenciaNotificacao";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoocorrencianotificacao:[SendSecurityCheck value for]"+"ContratoOcorrenciaNotificacao_Codigo:"+context.localUtil.Format( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contratoocorrencianotificacao:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoocorrencianotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoOcorrenciaNotificacao_Codigo) + "," + UrlEncode("" +A294ContratoOcorrencia_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoOcorrenciaNotificacao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Ocorrencia Notificacao" ;
      }

      protected void InitializeNonKey1I51( )
      {
         A303ContratoOcorrenciaNotificacao_Responsavel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A303ContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
         A298ContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A298ContratoOcorrenciaNotificacao_Data", context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"));
         A299ContratoOcorrenciaNotificacao_Prazo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A299ContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0)));
         A300ContratoOcorrenciaNotificacao_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A300ContratoOcorrenciaNotificacao_Descricao", A300ContratoOcorrenciaNotificacao_Descricao);
         A301ContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         n301ContratoOcorrenciaNotificacao_Cumprido = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A301ContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
         n301ContratoOcorrenciaNotificacao_Cumprido = ((DateTime.MinValue==A301ContratoOcorrenciaNotificacao_Cumprido) ? true : false);
         A302ContratoOcorrenciaNotificacao_Protocolo = "";
         n302ContratoOcorrenciaNotificacao_Protocolo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A302ContratoOcorrenciaNotificacao_Protocolo", A302ContratoOcorrenciaNotificacao_Protocolo);
         n302ContratoOcorrenciaNotificacao_Protocolo = (String.IsNullOrEmpty(StringUtil.RTrim( A302ContratoOcorrenciaNotificacao_Protocolo)) ? true : false);
         A304ContratoOcorrenciaNotificacao_Nome = "";
         n304ContratoOcorrenciaNotificacao_Nome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A304ContratoOcorrenciaNotificacao_Nome", A304ContratoOcorrenciaNotificacao_Nome);
         A306ContratoOcorrenciaNotificacao_Docto = "";
         n306ContratoOcorrenciaNotificacao_Docto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A306ContratoOcorrenciaNotificacao_Docto", A306ContratoOcorrenciaNotificacao_Docto);
         Z298ContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         Z299ContratoOcorrenciaNotificacao_Prazo = 0;
         Z300ContratoOcorrenciaNotificacao_Descricao = "";
         Z301ContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         Z302ContratoOcorrenciaNotificacao_Protocolo = "";
         Z303ContratoOcorrenciaNotificacao_Responsavel = 0;
      }

      protected void InitAll1I51( )
      {
         A297ContratoOcorrenciaNotificacao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
         InitializeNonKey1I51( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282304263");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoocorrencianotificacao.js", "?20204282304263");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontrato_numero_Internalname = "TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         lblTextblockcontratoocorrencia_descricao_Internalname = "TEXTBLOCKCONTRATOOCORRENCIA_DESCRICAO";
         edtContratoOcorrencia_Descricao_Internalname = "CONTRATOOCORRENCIA_DESCRICAO";
         lblTextblockcontratoocorrencia_data_Internalname = "TEXTBLOCKCONTRATOOCORRENCIA_DATA";
         edtContratoOcorrencia_Data_Internalname = "CONTRATOOCORRENCIA_DATA";
         lblTextblockcontratoocorrencianotificacao_data_Internalname = "TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_DATA";
         edtContratoOcorrenciaNotificacao_Data_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         lblTextblockcontratoocorrencianotificacao_descricao_Internalname = "TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
         edtContratoOcorrenciaNotificacao_Descricao_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
         lblTextblockcontratoocorrencianotificacao_prazo_Internalname = "TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_PRAZO";
         edtContratoOcorrenciaNotificacao_Prazo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PRAZO";
         lblTextblockcontratoocorrencianotificacao_cumprido_Internalname = "TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
         edtContratoOcorrenciaNotificacao_Cumprido_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
         lblTextblockcontratoocorrencianotificacao_protocolo_Internalname = "TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
         edtContratoOcorrenciaNotificacao_Protocolo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
         lblTextblockcontratoocorrencianotificacao_responsavel_Internalname = "TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL";
         edtContratoOcorrenciaNotificacao_Responsavel_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL";
         lblTextblockcontratoocorrencianotificacao_nome_Internalname = "TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_NOME";
         edtContratoOcorrenciaNotificacao_Nome_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_NOME";
         lblTextblockcontratoocorrencianotificacao_docto_Internalname = "TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_DOCTO";
         edtContratoOcorrenciaNotificacao_Docto_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DOCTO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratoOcorrenciaNotificacao_Codigo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CODIGO";
         edtContratoOcorrencia_Codigo_Internalname = "CONTRATOOCORRENCIA_CODIGO";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         Form.Internalname = "FORM";
         imgprompt_303_Internalname = "PROMPT_303";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Notifica��o";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contrato Ocorrencia Notificacao";
         edtContratoOcorrenciaNotificacao_Docto_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Docto_Enabled = 0;
         edtContratoOcorrenciaNotificacao_Nome_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Nome_Enabled = 0;
         imgprompt_303_Visible = 1;
         imgprompt_303_Link = "";
         edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Responsavel_Enabled = 1;
         edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Protocolo_Enabled = 1;
         edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Cumprido_Enabled = 1;
         edtContratoOcorrenciaNotificacao_Prazo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Prazo_Enabled = 1;
         edtContratoOcorrenciaNotificacao_Descricao_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Descricao_Enabled = 1;
         edtContratoOcorrenciaNotificacao_Data_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Data_Enabled = 1;
         edtContratoOcorrencia_Data_Jsonclick = "";
         edtContratoOcorrencia_Data_Enabled = 0;
         edtContratoOcorrencia_Descricao_Jsonclick = "";
         edtContratoOcorrencia_Descricao_Enabled = 0;
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Numero_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContrato_Codigo_Jsonclick = "";
         edtContrato_Codigo_Enabled = 0;
         edtContrato_Codigo_Visible = 1;
         edtContratoOcorrencia_Codigo_Jsonclick = "";
         edtContratoOcorrencia_Codigo_Enabled = 0;
         edtContratoOcorrencia_Codigo_Visible = 1;
         edtContratoOcorrenciaNotificacao_Codigo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Codigo_Enabled = 0;
         edtContratoOcorrenciaNotificacao_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Contratoocorrencianotificacao_responsavel( int GX_Parm1 ,
                                                                   String GX_Parm2 ,
                                                                   String GX_Parm3 )
      {
         A303ContratoOcorrenciaNotificacao_Responsavel = GX_Parm1;
         A304ContratoOcorrenciaNotificacao_Nome = GX_Parm2;
         n304ContratoOcorrenciaNotificacao_Nome = false;
         A306ContratoOcorrenciaNotificacao_Docto = GX_Parm3;
         n306ContratoOcorrenciaNotificacao_Docto = false;
         /* Using cursor T001I15 */
         pr_default.execute(13, new Object[] {A303ContratoOcorrenciaNotificacao_Responsavel});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contrato Ocorrencia Notificacao_Responsavel'.", "ForeignKeyNotFound", 1, "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrenciaNotificacao_Responsavel_Internalname;
         }
         A304ContratoOcorrenciaNotificacao_Nome = T001I15_A304ContratoOcorrenciaNotificacao_Nome[0];
         n304ContratoOcorrenciaNotificacao_Nome = T001I15_n304ContratoOcorrenciaNotificacao_Nome[0];
         A306ContratoOcorrenciaNotificacao_Docto = T001I15_A306ContratoOcorrenciaNotificacao_Docto[0];
         n306ContratoOcorrenciaNotificacao_Docto = T001I15_n306ContratoOcorrenciaNotificacao_Docto[0];
         pr_default.close(13);
         if ( (0==A303ContratoOcorrenciaNotificacao_Responsavel) )
         {
            GX_msglist.addItem("C�digo da pessoa respons�vel � obrigat�rio.", 1, "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL");
            AnyError = 1;
            GX_FocusControl = edtContratoOcorrenciaNotificacao_Responsavel_Internalname;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A304ContratoOcorrenciaNotificacao_Nome = "";
            n304ContratoOcorrenciaNotificacao_Nome = false;
            A306ContratoOcorrenciaNotificacao_Docto = "";
            n306ContratoOcorrenciaNotificacao_Docto = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A304ContratoOcorrenciaNotificacao_Nome));
         isValidOutput.Add(A306ContratoOcorrenciaNotificacao_Docto);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoOcorrenciaNotificacao_Codigo',fld:'vCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121I2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z298ContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         Z300ContratoOcorrenciaNotificacao_Descricao = "";
         Z301ContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         Z302ContratoOcorrenciaNotificacao_Protocolo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         A77Contrato_Numero = "";
         lblTextblockcontratoocorrencia_descricao_Jsonclick = "";
         A296ContratoOcorrencia_Descricao = "";
         lblTextblockcontratoocorrencia_data_Jsonclick = "";
         A295ContratoOcorrencia_Data = DateTime.MinValue;
         lblTextblockcontratoocorrencianotificacao_data_Jsonclick = "";
         A298ContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         lblTextblockcontratoocorrencianotificacao_descricao_Jsonclick = "";
         A300ContratoOcorrenciaNotificacao_Descricao = "";
         lblTextblockcontratoocorrencianotificacao_prazo_Jsonclick = "";
         lblTextblockcontratoocorrencianotificacao_cumprido_Jsonclick = "";
         A301ContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         lblTextblockcontratoocorrencianotificacao_protocolo_Jsonclick = "";
         A302ContratoOcorrenciaNotificacao_Protocolo = "";
         lblTextblockcontratoocorrencianotificacao_responsavel_Jsonclick = "";
         lblTextblockcontratoocorrencianotificacao_nome_Jsonclick = "";
         A304ContratoOcorrenciaNotificacao_Nome = "";
         lblTextblockcontratoocorrencianotificacao_docto_Jsonclick = "";
         A306ContratoOcorrenciaNotificacao_Docto = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode51 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z296ContratoOcorrencia_Descricao = "";
         Z295ContratoOcorrencia_Data = DateTime.MinValue;
         Z77Contrato_Numero = "";
         Z304ContratoOcorrenciaNotificacao_Nome = "";
         Z306ContratoOcorrenciaNotificacao_Docto = "";
         T001I4_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         T001I4_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         T001I4_A74Contrato_Codigo = new int[1] ;
         T001I6_A77Contrato_Numero = new String[] {""} ;
         T001I5_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         T001I5_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         T001I5_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         T001I5_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         T001I7_A294ContratoOcorrencia_Codigo = new int[1] ;
         T001I7_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         T001I7_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         T001I7_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         T001I7_A77Contrato_Numero = new String[] {""} ;
         T001I7_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         T001I7_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         T001I7_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         T001I7_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         T001I7_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         T001I7_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         T001I7_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         T001I7_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         T001I7_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         T001I7_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         T001I7_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         T001I7_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         T001I7_A74Contrato_Codigo = new int[1] ;
         T001I8_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         T001I8_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         T001I8_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         T001I8_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         T001I9_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         T001I3_A294ContratoOcorrencia_Codigo = new int[1] ;
         T001I3_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         T001I3_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         T001I3_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         T001I3_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         T001I3_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         T001I3_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         T001I3_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         T001I3_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         T001I3_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         T001I10_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         T001I10_A294ContratoOcorrencia_Codigo = new int[1] ;
         T001I11_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         T001I11_A294ContratoOcorrencia_Codigo = new int[1] ;
         T001I2_A294ContratoOcorrencia_Codigo = new int[1] ;
         T001I2_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         T001I2_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         T001I2_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         T001I2_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         T001I2_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         T001I2_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         T001I2_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         T001I2_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         T001I2_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         T001I12_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         T001I15_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         T001I15_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         T001I15_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         T001I15_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         T001I16_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoocorrencianotificacao__default(),
            new Object[][] {
                new Object[] {
               T001I2_A294ContratoOcorrencia_Codigo, T001I2_A297ContratoOcorrenciaNotificacao_Codigo, T001I2_A298ContratoOcorrenciaNotificacao_Data, T001I2_A299ContratoOcorrenciaNotificacao_Prazo, T001I2_A300ContratoOcorrenciaNotificacao_Descricao, T001I2_A301ContratoOcorrenciaNotificacao_Cumprido, T001I2_n301ContratoOcorrenciaNotificacao_Cumprido, T001I2_A302ContratoOcorrenciaNotificacao_Protocolo, T001I2_n302ContratoOcorrenciaNotificacao_Protocolo, T001I2_A303ContratoOcorrenciaNotificacao_Responsavel
               }
               , new Object[] {
               T001I3_A294ContratoOcorrencia_Codigo, T001I3_A297ContratoOcorrenciaNotificacao_Codigo, T001I3_A298ContratoOcorrenciaNotificacao_Data, T001I3_A299ContratoOcorrenciaNotificacao_Prazo, T001I3_A300ContratoOcorrenciaNotificacao_Descricao, T001I3_A301ContratoOcorrenciaNotificacao_Cumprido, T001I3_n301ContratoOcorrenciaNotificacao_Cumprido, T001I3_A302ContratoOcorrenciaNotificacao_Protocolo, T001I3_n302ContratoOcorrenciaNotificacao_Protocolo, T001I3_A303ContratoOcorrenciaNotificacao_Responsavel
               }
               , new Object[] {
               T001I4_A296ContratoOcorrencia_Descricao, T001I4_A295ContratoOcorrencia_Data, T001I4_A74Contrato_Codigo
               }
               , new Object[] {
               T001I5_A304ContratoOcorrenciaNotificacao_Nome, T001I5_n304ContratoOcorrenciaNotificacao_Nome, T001I5_A306ContratoOcorrenciaNotificacao_Docto, T001I5_n306ContratoOcorrenciaNotificacao_Docto
               }
               , new Object[] {
               T001I6_A77Contrato_Numero
               }
               , new Object[] {
               T001I7_A294ContratoOcorrencia_Codigo, T001I7_A297ContratoOcorrenciaNotificacao_Codigo, T001I7_A296ContratoOcorrencia_Descricao, T001I7_A295ContratoOcorrencia_Data, T001I7_A77Contrato_Numero, T001I7_A298ContratoOcorrenciaNotificacao_Data, T001I7_A299ContratoOcorrenciaNotificacao_Prazo, T001I7_A300ContratoOcorrenciaNotificacao_Descricao, T001I7_A301ContratoOcorrenciaNotificacao_Cumprido, T001I7_n301ContratoOcorrenciaNotificacao_Cumprido,
               T001I7_A302ContratoOcorrenciaNotificacao_Protocolo, T001I7_n302ContratoOcorrenciaNotificacao_Protocolo, T001I7_A304ContratoOcorrenciaNotificacao_Nome, T001I7_n304ContratoOcorrenciaNotificacao_Nome, T001I7_A306ContratoOcorrenciaNotificacao_Docto, T001I7_n306ContratoOcorrenciaNotificacao_Docto, T001I7_A303ContratoOcorrenciaNotificacao_Responsavel, T001I7_A74Contrato_Codigo
               }
               , new Object[] {
               T001I8_A304ContratoOcorrenciaNotificacao_Nome, T001I8_n304ContratoOcorrenciaNotificacao_Nome, T001I8_A306ContratoOcorrenciaNotificacao_Docto, T001I8_n306ContratoOcorrenciaNotificacao_Docto
               }
               , new Object[] {
               T001I9_A297ContratoOcorrenciaNotificacao_Codigo
               }
               , new Object[] {
               T001I10_A297ContratoOcorrenciaNotificacao_Codigo, T001I10_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               T001I11_A297ContratoOcorrenciaNotificacao_Codigo, T001I11_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               T001I12_A297ContratoOcorrenciaNotificacao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001I15_A304ContratoOcorrenciaNotificacao_Nome, T001I15_n304ContratoOcorrenciaNotificacao_Nome, T001I15_A306ContratoOcorrenciaNotificacao_Docto, T001I15_n306ContratoOcorrenciaNotificacao_Docto
               }
               , new Object[] {
               T001I16_A297ContratoOcorrenciaNotificacao_Codigo
               }
            }
         );
         N294ContratoOcorrencia_Codigo = 0;
         Z294ContratoOcorrencia_Codigo = 0;
         A294ContratoOcorrencia_Codigo = 0;
         AV14Pgmname = "ContratoOcorrenciaNotificacao";
      }

      private short Z299ContratoOcorrenciaNotificacao_Prazo ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A299ContratoOcorrenciaNotificacao_Prazo ;
      private short RcdFound51 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ContratoOcorrenciaNotificacao_Codigo ;
      private int wcpOA294ContratoOcorrencia_Codigo ;
      private int Z297ContratoOcorrenciaNotificacao_Codigo ;
      private int Z303ContratoOcorrenciaNotificacao_Responsavel ;
      private int N294ContratoOcorrencia_Codigo ;
      private int N303ContratoOcorrenciaNotificacao_Responsavel ;
      private int A303ContratoOcorrenciaNotificacao_Responsavel ;
      private int AV7ContratoOcorrenciaNotificacao_Codigo ;
      private int A294ContratoOcorrencia_Codigo ;
      private int trnEnded ;
      private int A297ContratoOcorrenciaNotificacao_Codigo ;
      private int edtContratoOcorrenciaNotificacao_Codigo_Enabled ;
      private int edtContratoOcorrenciaNotificacao_Codigo_Visible ;
      private int edtContratoOcorrencia_Codigo_Visible ;
      private int edtContratoOcorrencia_Codigo_Enabled ;
      private int A74Contrato_Codigo ;
      private int edtContrato_Codigo_Enabled ;
      private int edtContrato_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContrato_Numero_Enabled ;
      private int edtContratoOcorrencia_Descricao_Enabled ;
      private int edtContratoOcorrencia_Data_Enabled ;
      private int edtContratoOcorrenciaNotificacao_Data_Enabled ;
      private int edtContratoOcorrenciaNotificacao_Descricao_Enabled ;
      private int edtContratoOcorrenciaNotificacao_Prazo_Enabled ;
      private int edtContratoOcorrenciaNotificacao_Cumprido_Enabled ;
      private int edtContratoOcorrenciaNotificacao_Protocolo_Enabled ;
      private int edtContratoOcorrenciaNotificacao_Responsavel_Enabled ;
      private int imgprompt_303_Visible ;
      private int edtContratoOcorrenciaNotificacao_Nome_Enabled ;
      private int edtContratoOcorrenciaNotificacao_Docto_Enabled ;
      private int AV11Insert_ContratoOcorrencia_Codigo ;
      private int AV12Insert_ContratoOcorrenciaNotificacao_Responsavel ;
      private int AV15GXV1 ;
      private int Z294ContratoOcorrencia_Codigo ;
      private int Z74Contrato_Codigo ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoOcorrenciaNotificacao_Data_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Codigo_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtContratoOcorrencia_Codigo_Internalname ;
      private String edtContratoOcorrencia_Codigo_Jsonclick ;
      private String edtContrato_Codigo_Internalname ;
      private String edtContrato_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String edtContrato_Numero_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontratoocorrencia_descricao_Internalname ;
      private String lblTextblockcontratoocorrencia_descricao_Jsonclick ;
      private String edtContratoOcorrencia_Descricao_Internalname ;
      private String edtContratoOcorrencia_Descricao_Jsonclick ;
      private String lblTextblockcontratoocorrencia_data_Internalname ;
      private String lblTextblockcontratoocorrencia_data_Jsonclick ;
      private String edtContratoOcorrencia_Data_Internalname ;
      private String edtContratoOcorrencia_Data_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_data_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_data_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Data_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_descricao_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_descricao_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Descricao_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Descricao_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_prazo_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_prazo_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Prazo_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Prazo_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_cumprido_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_cumprido_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Cumprido_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_protocolo_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_protocolo_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Protocolo_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_responsavel_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_responsavel_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Responsavel_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick ;
      private String imgprompt_303_Internalname ;
      private String imgprompt_303_Link ;
      private String lblTextblockcontratoocorrencianotificacao_nome_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_nome_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Nome_Internalname ;
      private String A304ContratoOcorrenciaNotificacao_Nome ;
      private String edtContratoOcorrenciaNotificacao_Nome_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_docto_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_docto_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Docto_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Docto_Jsonclick ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode51 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z77Contrato_Numero ;
      private String Z304ContratoOcorrenciaNotificacao_Nome ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z298ContratoOcorrenciaNotificacao_Data ;
      private DateTime Z301ContratoOcorrenciaNotificacao_Cumprido ;
      private DateTime A295ContratoOcorrencia_Data ;
      private DateTime A298ContratoOcorrenciaNotificacao_Data ;
      private DateTime A301ContratoOcorrenciaNotificacao_Cumprido ;
      private DateTime Z295ContratoOcorrencia_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool n302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool n304ContratoOcorrenciaNotificacao_Nome ;
      private bool n306ContratoOcorrenciaNotificacao_Docto ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String Z300ContratoOcorrenciaNotificacao_Descricao ;
      private String Z302ContratoOcorrenciaNotificacao_Protocolo ;
      private String A296ContratoOcorrencia_Descricao ;
      private String A300ContratoOcorrenciaNotificacao_Descricao ;
      private String A302ContratoOcorrenciaNotificacao_Protocolo ;
      private String A306ContratoOcorrenciaNotificacao_Docto ;
      private String Z296ContratoOcorrencia_Descricao ;
      private String Z306ContratoOcorrenciaNotificacao_Docto ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_ContratoOcorrencia_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] T001I4_A296ContratoOcorrencia_Descricao ;
      private DateTime[] T001I4_A295ContratoOcorrencia_Data ;
      private int[] T001I4_A74Contrato_Codigo ;
      private String[] T001I6_A77Contrato_Numero ;
      private String[] T001I5_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] T001I5_n304ContratoOcorrenciaNotificacao_Nome ;
      private String[] T001I5_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] T001I5_n306ContratoOcorrenciaNotificacao_Docto ;
      private int[] T001I7_A294ContratoOcorrencia_Codigo ;
      private int[] T001I7_A297ContratoOcorrenciaNotificacao_Codigo ;
      private String[] T001I7_A296ContratoOcorrencia_Descricao ;
      private DateTime[] T001I7_A295ContratoOcorrencia_Data ;
      private String[] T001I7_A77Contrato_Numero ;
      private DateTime[] T001I7_A298ContratoOcorrenciaNotificacao_Data ;
      private short[] T001I7_A299ContratoOcorrenciaNotificacao_Prazo ;
      private String[] T001I7_A300ContratoOcorrenciaNotificacao_Descricao ;
      private DateTime[] T001I7_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] T001I7_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private String[] T001I7_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] T001I7_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private String[] T001I7_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] T001I7_n304ContratoOcorrenciaNotificacao_Nome ;
      private String[] T001I7_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] T001I7_n306ContratoOcorrenciaNotificacao_Docto ;
      private int[] T001I7_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private int[] T001I7_A74Contrato_Codigo ;
      private String[] T001I8_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] T001I8_n304ContratoOcorrenciaNotificacao_Nome ;
      private String[] T001I8_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] T001I8_n306ContratoOcorrenciaNotificacao_Docto ;
      private int[] T001I9_A297ContratoOcorrenciaNotificacao_Codigo ;
      private int[] T001I3_A294ContratoOcorrencia_Codigo ;
      private int[] T001I3_A297ContratoOcorrenciaNotificacao_Codigo ;
      private DateTime[] T001I3_A298ContratoOcorrenciaNotificacao_Data ;
      private short[] T001I3_A299ContratoOcorrenciaNotificacao_Prazo ;
      private String[] T001I3_A300ContratoOcorrenciaNotificacao_Descricao ;
      private DateTime[] T001I3_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] T001I3_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private String[] T001I3_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] T001I3_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private int[] T001I3_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private int[] T001I10_A297ContratoOcorrenciaNotificacao_Codigo ;
      private int[] T001I10_A294ContratoOcorrencia_Codigo ;
      private int[] T001I11_A297ContratoOcorrenciaNotificacao_Codigo ;
      private int[] T001I11_A294ContratoOcorrencia_Codigo ;
      private int[] T001I2_A294ContratoOcorrencia_Codigo ;
      private int[] T001I2_A297ContratoOcorrenciaNotificacao_Codigo ;
      private DateTime[] T001I2_A298ContratoOcorrenciaNotificacao_Data ;
      private short[] T001I2_A299ContratoOcorrenciaNotificacao_Prazo ;
      private String[] T001I2_A300ContratoOcorrenciaNotificacao_Descricao ;
      private DateTime[] T001I2_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] T001I2_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private String[] T001I2_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] T001I2_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private int[] T001I2_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private int[] T001I12_A297ContratoOcorrenciaNotificacao_Codigo ;
      private String[] T001I15_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] T001I15_n304ContratoOcorrenciaNotificacao_Nome ;
      private String[] T001I15_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] T001I15_n306ContratoOcorrenciaNotificacao_Docto ;
      private int[] T001I16_A297ContratoOcorrenciaNotificacao_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class contratoocorrencianotificacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001I4 ;
          prmT001I4 = new Object[] {
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I6 ;
          prmT001I6 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I7 ;
          prmT001I7 = new Object[] {
          new Object[] {"@ContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I5 ;
          prmT001I5 = new Object[] {
          new Object[] {"@ContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I8 ;
          prmT001I8 = new Object[] {
          new Object[] {"@ContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I9 ;
          prmT001I9 = new Object[] {
          new Object[] {"@ContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I3 ;
          prmT001I3 = new Object[] {
          new Object[] {"@ContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I10 ;
          prmT001I10 = new Object[] {
          new Object[] {"@ContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I11 ;
          prmT001I11 = new Object[] {
          new Object[] {"@ContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I2 ;
          prmT001I2 = new Object[] {
          new Object[] {"@ContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I12 ;
          prmT001I12 = new Object[] {
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@ContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I13 ;
          prmT001I13 = new Object[] {
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@ContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I14 ;
          prmT001I14 = new Object[] {
          new Object[] {"@ContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I16 ;
          prmT001I16 = new Object[] {
          new Object[] {"@ContratoOcorrencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001I15 ;
          prmT001I15 = new Object[] {
          new Object[] {"@ContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001I2", "SELECT [ContratoOcorrencia_Codigo], [ContratoOcorrenciaNotificacao_Codigo], [ContratoOcorrenciaNotificacao_Data], [ContratoOcorrenciaNotificacao_Prazo], [ContratoOcorrenciaNotificacao_Descricao], [ContratoOcorrenciaNotificacao_Cumprido], [ContratoOcorrenciaNotificacao_Protocolo], [ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel FROM [ContratoOcorrenciaNotificacao] WITH (UPDLOCK) WHERE [ContratoOcorrenciaNotificacao_Codigo] = @ContratoOcorrenciaNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001I2,1,0,true,false )
             ,new CursorDef("T001I3", "SELECT [ContratoOcorrencia_Codigo], [ContratoOcorrenciaNotificacao_Codigo], [ContratoOcorrenciaNotificacao_Data], [ContratoOcorrenciaNotificacao_Prazo], [ContratoOcorrenciaNotificacao_Descricao], [ContratoOcorrenciaNotificacao_Cumprido], [ContratoOcorrenciaNotificacao_Protocolo], [ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel FROM [ContratoOcorrenciaNotificacao] WITH (NOLOCK) WHERE [ContratoOcorrenciaNotificacao_Codigo] = @ContratoOcorrenciaNotificacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001I3,1,0,true,false )
             ,new CursorDef("T001I4", "SELECT [ContratoOcorrencia_Descricao], [ContratoOcorrencia_Data], [Contrato_Codigo] FROM [ContratoOcorrencia] WITH (NOLOCK) WHERE [ContratoOcorrencia_Codigo] = @ContratoOcorrencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001I4,1,0,true,false )
             ,new CursorDef("T001I5", "SELECT [Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, [Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratoOcorrenciaNotificacao_Responsavel ",true, GxErrorMask.GX_NOMASK, false, this,prmT001I5,1,0,true,false )
             ,new CursorDef("T001I6", "SELECT [Contrato_Numero] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001I6,1,0,true,false )
             ,new CursorDef("T001I7", "SELECT TM1.[ContratoOcorrencia_Codigo], TM1.[ContratoOcorrenciaNotificacao_Codigo], T2.[ContratoOcorrencia_Descricao], T2.[ContratoOcorrencia_Data], T3.[Contrato_Numero], TM1.[ContratoOcorrenciaNotificacao_Data], TM1.[ContratoOcorrenciaNotificacao_Prazo], TM1.[ContratoOcorrenciaNotificacao_Descricao], TM1.[ContratoOcorrenciaNotificacao_Cumprido], TM1.[ContratoOcorrenciaNotificacao_Protocolo], T4.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T4.[Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto, TM1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T2.[Contrato_Codigo] FROM ((([ContratoOcorrenciaNotificacao] TM1 WITH (NOLOCK) INNER JOIN [ContratoOcorrencia] T2 WITH (NOLOCK) ON T2.[ContratoOcorrencia_Codigo] = TM1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = TM1.[ContratoOcorrenciaNotificacao_Responsavel]) WHERE TM1.[ContratoOcorrenciaNotificacao_Codigo] = @ContratoOcorrenciaNotificacao_Codigo and TM1.[ContratoOcorrencia_Codigo] = @ContratoOcorrencia_Codigo ORDER BY TM1.[ContratoOcorrenciaNotificacao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001I7,100,0,true,false )
             ,new CursorDef("T001I8", "SELECT [Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, [Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratoOcorrenciaNotificacao_Responsavel ",true, GxErrorMask.GX_NOMASK, false, this,prmT001I8,1,0,true,false )
             ,new CursorDef("T001I9", "SELECT [ContratoOcorrenciaNotificacao_Codigo] FROM [ContratoOcorrenciaNotificacao] WITH (NOLOCK) WHERE [ContratoOcorrenciaNotificacao_Codigo] = @ContratoOcorrenciaNotificacao_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001I9,1,0,true,false )
             ,new CursorDef("T001I10", "SELECT TOP 1 [ContratoOcorrenciaNotificacao_Codigo], [ContratoOcorrencia_Codigo] FROM [ContratoOcorrenciaNotificacao] WITH (NOLOCK) WHERE ( [ContratoOcorrenciaNotificacao_Codigo] > @ContratoOcorrenciaNotificacao_Codigo) and [ContratoOcorrencia_Codigo] = @ContratoOcorrencia_Codigo ORDER BY [ContratoOcorrenciaNotificacao_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001I10,1,0,true,true )
             ,new CursorDef("T001I11", "SELECT TOP 1 [ContratoOcorrenciaNotificacao_Codigo], [ContratoOcorrencia_Codigo] FROM [ContratoOcorrenciaNotificacao] WITH (NOLOCK) WHERE ( [ContratoOcorrenciaNotificacao_Codigo] < @ContratoOcorrenciaNotificacao_Codigo) and [ContratoOcorrencia_Codigo] = @ContratoOcorrencia_Codigo ORDER BY [ContratoOcorrenciaNotificacao_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001I11,1,0,true,true )
             ,new CursorDef("T001I12", "INSERT INTO [ContratoOcorrenciaNotificacao]([ContratoOcorrencia_Codigo], [ContratoOcorrenciaNotificacao_Data], [ContratoOcorrenciaNotificacao_Prazo], [ContratoOcorrenciaNotificacao_Descricao], [ContratoOcorrenciaNotificacao_Cumprido], [ContratoOcorrenciaNotificacao_Protocolo], [ContratoOcorrenciaNotificacao_Responsavel]) VALUES(@ContratoOcorrencia_Codigo, @ContratoOcorrenciaNotificacao_Data, @ContratoOcorrenciaNotificacao_Prazo, @ContratoOcorrenciaNotificacao_Descricao, @ContratoOcorrenciaNotificacao_Cumprido, @ContratoOcorrenciaNotificacao_Protocolo, @ContratoOcorrenciaNotificacao_Responsavel); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001I12)
             ,new CursorDef("T001I13", "UPDATE [ContratoOcorrenciaNotificacao] SET [ContratoOcorrencia_Codigo]=@ContratoOcorrencia_Codigo, [ContratoOcorrenciaNotificacao_Data]=@ContratoOcorrenciaNotificacao_Data, [ContratoOcorrenciaNotificacao_Prazo]=@ContratoOcorrenciaNotificacao_Prazo, [ContratoOcorrenciaNotificacao_Descricao]=@ContratoOcorrenciaNotificacao_Descricao, [ContratoOcorrenciaNotificacao_Cumprido]=@ContratoOcorrenciaNotificacao_Cumprido, [ContratoOcorrenciaNotificacao_Protocolo]=@ContratoOcorrenciaNotificacao_Protocolo, [ContratoOcorrenciaNotificacao_Responsavel]=@ContratoOcorrenciaNotificacao_Responsavel  WHERE [ContratoOcorrenciaNotificacao_Codigo] = @ContratoOcorrenciaNotificacao_Codigo", GxErrorMask.GX_NOMASK,prmT001I13)
             ,new CursorDef("T001I14", "DELETE FROM [ContratoOcorrenciaNotificacao]  WHERE [ContratoOcorrenciaNotificacao_Codigo] = @ContratoOcorrenciaNotificacao_Codigo", GxErrorMask.GX_NOMASK,prmT001I14)
             ,new CursorDef("T001I15", "SELECT [Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, [Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratoOcorrenciaNotificacao_Responsavel ",true, GxErrorMask.GX_NOMASK, false, this,prmT001I15,1,0,true,false )
             ,new CursorDef("T001I16", "SELECT [ContratoOcorrenciaNotificacao_Codigo] FROM [ContratoOcorrenciaNotificacao] WITH (NOLOCK) WHERE [ContratoOcorrencia_Codigo] = @ContratoOcorrencia_Codigo ORDER BY [ContratoOcorrenciaNotificacao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001I16,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(9);
                ((String[]) buf[10])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(10);
                ((String[]) buf[12])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(11);
                ((String[]) buf[14])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(12);
                ((int[]) buf[16])[0] = rslt.getInt(13) ;
                ((int[]) buf[17])[0] = rslt.getInt(14) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[7]);
                }
                stmt.SetParameter(7, (int)parms[8]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[7]);
                }
                stmt.SetParameter(7, (int)parms[8]);
                stmt.SetParameter(8, (int)parms[9]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
