/*
               File: type_SdtGxCbb
        Description: GxCbb
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 21:5:42.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGxCbb : GxUserType, IGxExternalObject
   {
      public SdtGxCbb( )
      {
         initialize();
      }

      public SdtGxCbb( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void generateimage( String gxTp_encoding ,
                                 int gxTp_scale ,
                                 int gxTp_version ,
                                 String gxTp_errorCorrect ,
                                 String gxTp_data )
      {
         if ( GxCbb_externalReference == null )
         {
            GxCbb_externalReference = new ThoughtWorks.QRCode.GxCbb();
         }
         GxCbb_externalReference.GenerateImage(gxTp_encoding, gxTp_scale, gxTp_version, gxTp_errorCorrect, gxTp_data);
         return  ;
      }

      public void save( String gxTp_path )
      {
         if ( GxCbb_externalReference == null )
         {
            GxCbb_externalReference = new ThoughtWorks.QRCode.GxCbb();
         }
         GxCbb_externalReference.save(gxTp_path);
         return  ;
      }

      public Object ExternalInstance
      {
         get {
            if ( GxCbb_externalReference == null )
            {
               GxCbb_externalReference = new ThoughtWorks.QRCode.GxCbb();
            }
            return GxCbb_externalReference ;
         }

         set {
            GxCbb_externalReference = (ThoughtWorks.QRCode.GxCbb)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected ThoughtWorks.QRCode.GxCbb GxCbb_externalReference=null ;
   }

}
