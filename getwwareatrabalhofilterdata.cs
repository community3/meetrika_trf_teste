/*
               File: GetWWAreaTrabalhoFilterData
        Description: Get WWArea Trabalho Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/31/2020 1:13:42.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwareatrabalhofilterdata : GXProcedure
   {
      public getwwareatrabalhofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwareatrabalhofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwareatrabalhofilterdata objgetwwareatrabalhofilterdata;
         objgetwwareatrabalhofilterdata = new getwwareatrabalhofilterdata();
         objgetwwareatrabalhofilterdata.AV24DDOName = aP0_DDOName;
         objgetwwareatrabalhofilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetwwareatrabalhofilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetwwareatrabalhofilterdata.AV28OptionsJson = "" ;
         objgetwwareatrabalhofilterdata.AV31OptionsDescJson = "" ;
         objgetwwareatrabalhofilterdata.AV33OptionIndexesJson = "" ;
         objgetwwareatrabalhofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwareatrabalhofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwareatrabalhofilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwareatrabalhofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_AREATRABALHO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADAREATRABALHO_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATANTE_RAZAOSOCIAL") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTE_RAZAOSOCIALOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATANTE_CNPJ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTE_CNPJOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATANTE_TELEFONE") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTE_TELEFONEOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_ESTADO_UF") == 0 )
         {
            /* Execute user subroutine: 'LOADESTADO_UFOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_MUNICIPIO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADMUNICIPIO_NOMEOPTIONS' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("WWAreaTrabalhoGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWAreaTrabalhoGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("WWAreaTrabalhoGridState"), "");
         }
         AV87GXV1 = 1;
         while ( AV87GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV87GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_DESCRICAO") == 0 )
            {
               AV10TFAreaTrabalho_Descricao = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_DESCRICAO_SEL") == 0 )
            {
               AV11TFAreaTrabalho_Descricao_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_RAZAOSOCIAL") == 0 )
            {
               AV12TFContratante_RazaoSocial = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_RAZAOSOCIAL_SEL") == 0 )
            {
               AV13TFContratante_RazaoSocial_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_CNPJ") == 0 )
            {
               AV14TFContratante_CNPJ = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_CNPJ_SEL") == 0 )
            {
               AV15TFContratante_CNPJ_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_TELEFONE") == 0 )
            {
               AV16TFContratante_Telefone = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_TELEFONE_SEL") == 0 )
            {
               AV17TFContratante_Telefone_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_VALIDAOSFM_SEL") == 0 )
            {
               AV18TFAreaTrabalho_ValidaOSFM_Sel = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_DIASPARAPAGAR") == 0 )
            {
               AV19TFAreaTrabalho_DiasParaPagar = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV20TFAreaTrabalho_DiasParaPagar_To = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFESTADO_UF") == 0 )
            {
               AV73TFEstado_UF = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFESTADO_UF_SEL") == 0 )
            {
               AV74TFEstado_UF_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME") == 0 )
            {
               AV59TFMunicipio_Nome = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME_SEL") == 0 )
            {
               AV60TFMunicipio_Nome_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_CALCULOPFINAL_SEL") == 0 )
            {
               AV61TFAreaTrabalho_CalculoPFinal_SelsJson = AV38GridStateFilterValue.gxTpr_Value;
               AV62TFAreaTrabalho_CalculoPFinal_Sels.FromJSonString(AV61TFAreaTrabalho_CalculoPFinal_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_VERTA_SEL") == 0 )
            {
               AV83TFAreaTrabalho_VerTA_SelsJson = AV38GridStateFilterValue.gxTpr_Value;
               AV84TFAreaTrabalho_VerTA_Sels.FromJSonString(AV83TFAreaTrabalho_VerTA_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_ATIVO_SEL") == 0 )
            {
               AV21TFAreaTrabalho_Ativo_Sel = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
            }
            AV87GXV1 = (int)(AV87GXV1+1);
         }
         if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(1));
            AV44DynamicFiltersSelector1 = AV39GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "AREATRABALHO_DESCRICAO") == 0 )
            {
               AV45AreaTrabalho_Descricao1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "ORGANIZACAO_NOME") == 0 )
            {
               AV65DynamicFiltersOperator1 = AV39GridStateDynamicFilter.gxTpr_Operator;
               AV66Organizacao_Nome1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "AREATRABALHO_ATIVO") == 0 )
            {
               AV71AreaTrabalho_Ativo1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV47DynamicFiltersEnabled2 = true;
               AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(2));
               AV48DynamicFiltersSelector2 = AV39GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "AREATRABALHO_DESCRICAO") == 0 )
               {
                  AV49AreaTrabalho_Descricao2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "ORGANIZACAO_NOME") == 0 )
               {
                  AV68DynamicFiltersOperator2 = AV39GridStateDynamicFilter.gxTpr_Operator;
                  AV69Organizacao_Nome2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "AREATRABALHO_ATIVO") == 0 )
               {
                  AV72AreaTrabalho_Ativo2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV75DynamicFiltersEnabled3 = true;
                  AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(3));
                  AV76DynamicFiltersSelector3 = AV39GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV76DynamicFiltersSelector3, "AREATRABALHO_DESCRICAO") == 0 )
                  {
                     AV78AreaTrabalho_Descricao3 = AV39GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV76DynamicFiltersSelector3, "ORGANIZACAO_NOME") == 0 )
                  {
                     AV77DynamicFiltersOperator3 = AV39GridStateDynamicFilter.gxTpr_Operator;
                     AV79Organizacao_Nome3 = AV39GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV76DynamicFiltersSelector3, "AREATRABALHO_ATIVO") == 0 )
                  {
                     AV80AreaTrabalho_Ativo3 = AV39GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADAREATRABALHO_DESCRICAOOPTIONS' Routine */
         AV10TFAreaTrabalho_Descricao = AV22SearchTxt;
         AV11TFAreaTrabalho_Descricao_Sel = "";
         AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 = AV44DynamicFiltersSelector1;
         AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 = AV65DynamicFiltersOperator1;
         AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = AV45AreaTrabalho_Descricao1;
         AV92WWAreaTrabalhoDS_4_Organizacao_nome1 = AV66Organizacao_Nome1;
         AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 = AV71AreaTrabalho_Ativo1;
         AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 = AV68DynamicFiltersOperator2;
         AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = AV49AreaTrabalho_Descricao2;
         AV98WWAreaTrabalhoDS_10_Organizacao_nome2 = AV69Organizacao_Nome2;
         AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 = AV72AreaTrabalho_Ativo2;
         AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 = AV75DynamicFiltersEnabled3;
         AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 = AV76DynamicFiltersSelector3;
         AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 = AV77DynamicFiltersOperator3;
         AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = AV78AreaTrabalho_Descricao3;
         AV104WWAreaTrabalhoDS_16_Organizacao_nome3 = AV79Organizacao_Nome3;
         AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 = AV80AreaTrabalho_Ativo3;
         AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = AV10TFAreaTrabalho_Descricao;
         AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel = AV11TFAreaTrabalho_Descricao_Sel;
         AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = AV12TFContratante_RazaoSocial;
         AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel = AV13TFContratante_RazaoSocial_Sel;
         AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = AV14TFContratante_CNPJ;
         AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel = AV15TFContratante_CNPJ_Sel;
         AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = AV16TFContratante_Telefone;
         AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel = AV17TFContratante_Telefone_Sel;
         AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel = AV18TFAreaTrabalho_ValidaOSFM_Sel;
         AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar = AV19TFAreaTrabalho_DiasParaPagar;
         AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to = AV20TFAreaTrabalho_DiasParaPagar_To;
         AV117WWAreaTrabalhoDS_29_Tfestado_uf = AV73TFEstado_UF;
         AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel = AV74TFEstado_UF_Sel;
         AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = AV59TFMunicipio_Nome;
         AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel = AV60TFMunicipio_Nome_Sel;
         AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels = AV62TFAreaTrabalho_CalculoPFinal_Sels;
         AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels = AV84TFAreaTrabalho_VerTA_Sels;
         AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel = AV21TFAreaTrabalho_Ativo_Sel;
         AV124Udparg36 = new prc_areasdousuario(context).executeUdp(  AV9WWPContext.gxTpr_Userid);
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A642AreaTrabalho_CalculoPFinal ,
                                              AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels ,
                                              A2081AreaTrabalho_VerTA ,
                                              AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels ,
                                              A5AreaTrabalho_Codigo ,
                                              AV124Udparg36 ,
                                              AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 ,
                                              AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ,
                                              AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 ,
                                              AV92WWAreaTrabalhoDS_4_Organizacao_nome1 ,
                                              AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 ,
                                              AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 ,
                                              AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 ,
                                              AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ,
                                              AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 ,
                                              AV98WWAreaTrabalhoDS_10_Organizacao_nome2 ,
                                              AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 ,
                                              AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 ,
                                              AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 ,
                                              AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ,
                                              AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 ,
                                              AV104WWAreaTrabalhoDS_16_Organizacao_nome3 ,
                                              AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 ,
                                              AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel ,
                                              AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ,
                                              AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel ,
                                              AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ,
                                              AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel ,
                                              AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ,
                                              AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel ,
                                              AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ,
                                              AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel ,
                                              AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar ,
                                              AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to ,
                                              AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel ,
                                              AV117WWAreaTrabalhoDS_29_Tfestado_uf ,
                                              AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel ,
                                              AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ,
                                              AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels.Count ,
                                              AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels.Count ,
                                              AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel ,
                                              AV9WWPContext.gxTpr_Contratante_codigo ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              A6AreaTrabalho_Descricao ,
                                              A1214Organizacao_Nome ,
                                              A72AreaTrabalho_Ativo ,
                                              A9Contratante_RazaoSocial ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A834AreaTrabalho_ValidaOSFM ,
                                              A855AreaTrabalho_DiasParaPagar ,
                                              A23Estado_UF ,
                                              A26Municipio_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1), "%", "");
         lV92WWAreaTrabalhoDS_4_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1), 50, "%");
         lV92WWAreaTrabalhoDS_4_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1), 50, "%");
         lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2), "%", "");
         lV98WWAreaTrabalhoDS_10_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2), 50, "%");
         lV98WWAreaTrabalhoDS_10_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2), 50, "%");
         lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3), "%", "");
         lV104WWAreaTrabalhoDS_16_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3), 50, "%");
         lV104WWAreaTrabalhoDS_16_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3), 50, "%");
         lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = StringUtil.Concat( StringUtil.RTrim( AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao), "%", "");
         lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial), 100, "%");
         lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj), "%", "");
         lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone), 20, "%");
         lV117WWAreaTrabalhoDS_29_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV117WWAreaTrabalhoDS_29_Tfestado_uf), 2, "%");
         lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome), 50, "%");
         /* Using cursor P00E32 */
         pr_default.execute(0, new Object[] {lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1, lV92WWAreaTrabalhoDS_4_Organizacao_nome1, lV92WWAreaTrabalhoDS_4_Organizacao_nome1, lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2, lV98WWAreaTrabalhoDS_10_Organizacao_nome2, lV98WWAreaTrabalhoDS_10_Organizacao_nome2, lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3, lV104WWAreaTrabalhoDS_16_Organizacao_nome3, lV104WWAreaTrabalhoDS_16_Organizacao_nome3, lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao, AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel, lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial, AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel, lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj, AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel, lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone, AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel, AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar, AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to, lV117WWAreaTrabalhoDS_29_Tfestado_uf, AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel, lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome, AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKE32 = false;
            A29Contratante_Codigo = P00E32_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00E32_n29Contratante_Codigo[0];
            A25Municipio_Codigo = P00E32_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00E32_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00E32_A335Contratante_PessoaCod[0];
            A1216AreaTrabalho_OrganizacaoCod = P00E32_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = P00E32_n1216AreaTrabalho_OrganizacaoCod[0];
            A6AreaTrabalho_Descricao = P00E32_A6AreaTrabalho_Descricao[0];
            A5AreaTrabalho_Codigo = P00E32_A5AreaTrabalho_Codigo[0];
            A2081AreaTrabalho_VerTA = P00E32_A2081AreaTrabalho_VerTA[0];
            n2081AreaTrabalho_VerTA = P00E32_n2081AreaTrabalho_VerTA[0];
            A642AreaTrabalho_CalculoPFinal = P00E32_A642AreaTrabalho_CalculoPFinal[0];
            A26Municipio_Nome = P00E32_A26Municipio_Nome[0];
            A23Estado_UF = P00E32_A23Estado_UF[0];
            A855AreaTrabalho_DiasParaPagar = P00E32_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = P00E32_n855AreaTrabalho_DiasParaPagar[0];
            A834AreaTrabalho_ValidaOSFM = P00E32_A834AreaTrabalho_ValidaOSFM[0];
            n834AreaTrabalho_ValidaOSFM = P00E32_n834AreaTrabalho_ValidaOSFM[0];
            A31Contratante_Telefone = P00E32_A31Contratante_Telefone[0];
            A12Contratante_CNPJ = P00E32_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00E32_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00E32_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00E32_n9Contratante_RazaoSocial[0];
            A72AreaTrabalho_Ativo = P00E32_A72AreaTrabalho_Ativo[0];
            A1214Organizacao_Nome = P00E32_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00E32_n1214Organizacao_Nome[0];
            A25Municipio_Codigo = P00E32_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00E32_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00E32_A335Contratante_PessoaCod[0];
            A31Contratante_Telefone = P00E32_A31Contratante_Telefone[0];
            A26Municipio_Nome = P00E32_A26Municipio_Nome[0];
            A23Estado_UF = P00E32_A23Estado_UF[0];
            A12Contratante_CNPJ = P00E32_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00E32_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00E32_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00E32_n9Contratante_RazaoSocial[0];
            A1214Organizacao_Nome = P00E32_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00E32_n1214Organizacao_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00E32_A6AreaTrabalho_Descricao[0], A6AreaTrabalho_Descricao) == 0 ) )
            {
               BRKE32 = false;
               A5AreaTrabalho_Codigo = P00E32_A5AreaTrabalho_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKE32 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A6AreaTrabalho_Descricao)) )
            {
               AV26Option = A6AreaTrabalho_Descricao;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKE32 )
            {
               BRKE32 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATANTE_RAZAOSOCIALOPTIONS' Routine */
         AV12TFContratante_RazaoSocial = AV22SearchTxt;
         AV13TFContratante_RazaoSocial_Sel = "";
         AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 = AV44DynamicFiltersSelector1;
         AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 = AV65DynamicFiltersOperator1;
         AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = AV45AreaTrabalho_Descricao1;
         AV92WWAreaTrabalhoDS_4_Organizacao_nome1 = AV66Organizacao_Nome1;
         AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 = AV71AreaTrabalho_Ativo1;
         AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 = AV68DynamicFiltersOperator2;
         AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = AV49AreaTrabalho_Descricao2;
         AV98WWAreaTrabalhoDS_10_Organizacao_nome2 = AV69Organizacao_Nome2;
         AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 = AV72AreaTrabalho_Ativo2;
         AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 = AV75DynamicFiltersEnabled3;
         AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 = AV76DynamicFiltersSelector3;
         AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 = AV77DynamicFiltersOperator3;
         AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = AV78AreaTrabalho_Descricao3;
         AV104WWAreaTrabalhoDS_16_Organizacao_nome3 = AV79Organizacao_Nome3;
         AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 = AV80AreaTrabalho_Ativo3;
         AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = AV10TFAreaTrabalho_Descricao;
         AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel = AV11TFAreaTrabalho_Descricao_Sel;
         AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = AV12TFContratante_RazaoSocial;
         AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel = AV13TFContratante_RazaoSocial_Sel;
         AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = AV14TFContratante_CNPJ;
         AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel = AV15TFContratante_CNPJ_Sel;
         AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = AV16TFContratante_Telefone;
         AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel = AV17TFContratante_Telefone_Sel;
         AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel = AV18TFAreaTrabalho_ValidaOSFM_Sel;
         AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar = AV19TFAreaTrabalho_DiasParaPagar;
         AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to = AV20TFAreaTrabalho_DiasParaPagar_To;
         AV117WWAreaTrabalhoDS_29_Tfestado_uf = AV73TFEstado_UF;
         AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel = AV74TFEstado_UF_Sel;
         AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = AV59TFMunicipio_Nome;
         AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel = AV60TFMunicipio_Nome_Sel;
         AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels = AV62TFAreaTrabalho_CalculoPFinal_Sels;
         AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels = AV84TFAreaTrabalho_VerTA_Sels;
         AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel = AV21TFAreaTrabalho_Ativo_Sel;
         AV127Udparg37 = new prc_areasdousuario(context).executeUdp(  AV9WWPContext.gxTpr_Userid);
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A642AreaTrabalho_CalculoPFinal ,
                                              AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels ,
                                              A2081AreaTrabalho_VerTA ,
                                              AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels ,
                                              A5AreaTrabalho_Codigo ,
                                              AV127Udparg37 ,
                                              AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 ,
                                              AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ,
                                              AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 ,
                                              AV92WWAreaTrabalhoDS_4_Organizacao_nome1 ,
                                              AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 ,
                                              AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 ,
                                              AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 ,
                                              AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ,
                                              AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 ,
                                              AV98WWAreaTrabalhoDS_10_Organizacao_nome2 ,
                                              AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 ,
                                              AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 ,
                                              AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 ,
                                              AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ,
                                              AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 ,
                                              AV104WWAreaTrabalhoDS_16_Organizacao_nome3 ,
                                              AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 ,
                                              AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel ,
                                              AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ,
                                              AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel ,
                                              AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ,
                                              AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel ,
                                              AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ,
                                              AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel ,
                                              AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ,
                                              AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel ,
                                              AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar ,
                                              AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to ,
                                              AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel ,
                                              AV117WWAreaTrabalhoDS_29_Tfestado_uf ,
                                              AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel ,
                                              AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ,
                                              AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels.Count ,
                                              AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels.Count ,
                                              AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel ,
                                              AV9WWPContext.gxTpr_Contratante_codigo ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              A6AreaTrabalho_Descricao ,
                                              A1214Organizacao_Nome ,
                                              A72AreaTrabalho_Ativo ,
                                              A9Contratante_RazaoSocial ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A834AreaTrabalho_ValidaOSFM ,
                                              A855AreaTrabalho_DiasParaPagar ,
                                              A23Estado_UF ,
                                              A26Municipio_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1), "%", "");
         lV92WWAreaTrabalhoDS_4_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1), 50, "%");
         lV92WWAreaTrabalhoDS_4_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1), 50, "%");
         lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2), "%", "");
         lV98WWAreaTrabalhoDS_10_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2), 50, "%");
         lV98WWAreaTrabalhoDS_10_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2), 50, "%");
         lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3), "%", "");
         lV104WWAreaTrabalhoDS_16_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3), 50, "%");
         lV104WWAreaTrabalhoDS_16_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3), 50, "%");
         lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = StringUtil.Concat( StringUtil.RTrim( AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao), "%", "");
         lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial), 100, "%");
         lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj), "%", "");
         lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone), 20, "%");
         lV117WWAreaTrabalhoDS_29_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV117WWAreaTrabalhoDS_29_Tfestado_uf), 2, "%");
         lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome), 50, "%");
         /* Using cursor P00E33 */
         pr_default.execute(1, new Object[] {lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1, lV92WWAreaTrabalhoDS_4_Organizacao_nome1, lV92WWAreaTrabalhoDS_4_Organizacao_nome1, lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2, lV98WWAreaTrabalhoDS_10_Organizacao_nome2, lV98WWAreaTrabalhoDS_10_Organizacao_nome2, lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3, lV104WWAreaTrabalhoDS_16_Organizacao_nome3, lV104WWAreaTrabalhoDS_16_Organizacao_nome3, lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao, AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel, lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial, AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel, lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj, AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel, lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone, AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel, AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar, AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to, lV117WWAreaTrabalhoDS_29_Tfestado_uf, AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel, lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome, AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKE34 = false;
            A29Contratante_Codigo = P00E33_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00E33_n29Contratante_Codigo[0];
            A25Municipio_Codigo = P00E33_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00E33_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00E33_A335Contratante_PessoaCod[0];
            A1216AreaTrabalho_OrganizacaoCod = P00E33_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = P00E33_n1216AreaTrabalho_OrganizacaoCod[0];
            A9Contratante_RazaoSocial = P00E33_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00E33_n9Contratante_RazaoSocial[0];
            A5AreaTrabalho_Codigo = P00E33_A5AreaTrabalho_Codigo[0];
            A2081AreaTrabalho_VerTA = P00E33_A2081AreaTrabalho_VerTA[0];
            n2081AreaTrabalho_VerTA = P00E33_n2081AreaTrabalho_VerTA[0];
            A642AreaTrabalho_CalculoPFinal = P00E33_A642AreaTrabalho_CalculoPFinal[0];
            A26Municipio_Nome = P00E33_A26Municipio_Nome[0];
            A23Estado_UF = P00E33_A23Estado_UF[0];
            A855AreaTrabalho_DiasParaPagar = P00E33_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = P00E33_n855AreaTrabalho_DiasParaPagar[0];
            A834AreaTrabalho_ValidaOSFM = P00E33_A834AreaTrabalho_ValidaOSFM[0];
            n834AreaTrabalho_ValidaOSFM = P00E33_n834AreaTrabalho_ValidaOSFM[0];
            A31Contratante_Telefone = P00E33_A31Contratante_Telefone[0];
            A12Contratante_CNPJ = P00E33_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00E33_n12Contratante_CNPJ[0];
            A72AreaTrabalho_Ativo = P00E33_A72AreaTrabalho_Ativo[0];
            A1214Organizacao_Nome = P00E33_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00E33_n1214Organizacao_Nome[0];
            A6AreaTrabalho_Descricao = P00E33_A6AreaTrabalho_Descricao[0];
            A25Municipio_Codigo = P00E33_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00E33_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00E33_A335Contratante_PessoaCod[0];
            A31Contratante_Telefone = P00E33_A31Contratante_Telefone[0];
            A26Municipio_Nome = P00E33_A26Municipio_Nome[0];
            A23Estado_UF = P00E33_A23Estado_UF[0];
            A9Contratante_RazaoSocial = P00E33_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00E33_n9Contratante_RazaoSocial[0];
            A12Contratante_CNPJ = P00E33_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00E33_n12Contratante_CNPJ[0];
            A1214Organizacao_Nome = P00E33_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00E33_n1214Organizacao_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00E33_A9Contratante_RazaoSocial[0], A9Contratante_RazaoSocial) == 0 ) )
            {
               BRKE34 = false;
               A29Contratante_Codigo = P00E33_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00E33_n29Contratante_Codigo[0];
               A335Contratante_PessoaCod = P00E33_A335Contratante_PessoaCod[0];
               A5AreaTrabalho_Codigo = P00E33_A5AreaTrabalho_Codigo[0];
               A335Contratante_PessoaCod = P00E33_A335Contratante_PessoaCod[0];
               AV34count = (long)(AV34count+1);
               BRKE34 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A9Contratante_RazaoSocial)) )
            {
               AV26Option = A9Contratante_RazaoSocial;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKE34 )
            {
               BRKE34 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATANTE_CNPJOPTIONS' Routine */
         AV14TFContratante_CNPJ = AV22SearchTxt;
         AV15TFContratante_CNPJ_Sel = "";
         AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 = AV44DynamicFiltersSelector1;
         AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 = AV65DynamicFiltersOperator1;
         AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = AV45AreaTrabalho_Descricao1;
         AV92WWAreaTrabalhoDS_4_Organizacao_nome1 = AV66Organizacao_Nome1;
         AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 = AV71AreaTrabalho_Ativo1;
         AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 = AV68DynamicFiltersOperator2;
         AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = AV49AreaTrabalho_Descricao2;
         AV98WWAreaTrabalhoDS_10_Organizacao_nome2 = AV69Organizacao_Nome2;
         AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 = AV72AreaTrabalho_Ativo2;
         AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 = AV75DynamicFiltersEnabled3;
         AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 = AV76DynamicFiltersSelector3;
         AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 = AV77DynamicFiltersOperator3;
         AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = AV78AreaTrabalho_Descricao3;
         AV104WWAreaTrabalhoDS_16_Organizacao_nome3 = AV79Organizacao_Nome3;
         AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 = AV80AreaTrabalho_Ativo3;
         AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = AV10TFAreaTrabalho_Descricao;
         AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel = AV11TFAreaTrabalho_Descricao_Sel;
         AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = AV12TFContratante_RazaoSocial;
         AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel = AV13TFContratante_RazaoSocial_Sel;
         AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = AV14TFContratante_CNPJ;
         AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel = AV15TFContratante_CNPJ_Sel;
         AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = AV16TFContratante_Telefone;
         AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel = AV17TFContratante_Telefone_Sel;
         AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel = AV18TFAreaTrabalho_ValidaOSFM_Sel;
         AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar = AV19TFAreaTrabalho_DiasParaPagar;
         AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to = AV20TFAreaTrabalho_DiasParaPagar_To;
         AV117WWAreaTrabalhoDS_29_Tfestado_uf = AV73TFEstado_UF;
         AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel = AV74TFEstado_UF_Sel;
         AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = AV59TFMunicipio_Nome;
         AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel = AV60TFMunicipio_Nome_Sel;
         AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels = AV62TFAreaTrabalho_CalculoPFinal_Sels;
         AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels = AV84TFAreaTrabalho_VerTA_Sels;
         AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel = AV21TFAreaTrabalho_Ativo_Sel;
         AV130Udparg38 = new prc_areasdousuario(context).executeUdp(  AV9WWPContext.gxTpr_Userid);
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A642AreaTrabalho_CalculoPFinal ,
                                              AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels ,
                                              A2081AreaTrabalho_VerTA ,
                                              AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels ,
                                              A5AreaTrabalho_Codigo ,
                                              AV130Udparg38 ,
                                              AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 ,
                                              AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ,
                                              AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 ,
                                              AV92WWAreaTrabalhoDS_4_Organizacao_nome1 ,
                                              AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 ,
                                              AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 ,
                                              AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 ,
                                              AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ,
                                              AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 ,
                                              AV98WWAreaTrabalhoDS_10_Organizacao_nome2 ,
                                              AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 ,
                                              AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 ,
                                              AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 ,
                                              AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ,
                                              AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 ,
                                              AV104WWAreaTrabalhoDS_16_Organizacao_nome3 ,
                                              AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 ,
                                              AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel ,
                                              AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ,
                                              AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel ,
                                              AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ,
                                              AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel ,
                                              AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ,
                                              AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel ,
                                              AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ,
                                              AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel ,
                                              AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar ,
                                              AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to ,
                                              AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel ,
                                              AV117WWAreaTrabalhoDS_29_Tfestado_uf ,
                                              AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel ,
                                              AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ,
                                              AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels.Count ,
                                              AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels.Count ,
                                              AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel ,
                                              AV9WWPContext.gxTpr_Contratante_codigo ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              A6AreaTrabalho_Descricao ,
                                              A1214Organizacao_Nome ,
                                              A72AreaTrabalho_Ativo ,
                                              A9Contratante_RazaoSocial ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A834AreaTrabalho_ValidaOSFM ,
                                              A855AreaTrabalho_DiasParaPagar ,
                                              A23Estado_UF ,
                                              A26Municipio_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1), "%", "");
         lV92WWAreaTrabalhoDS_4_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1), 50, "%");
         lV92WWAreaTrabalhoDS_4_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1), 50, "%");
         lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2), "%", "");
         lV98WWAreaTrabalhoDS_10_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2), 50, "%");
         lV98WWAreaTrabalhoDS_10_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2), 50, "%");
         lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3), "%", "");
         lV104WWAreaTrabalhoDS_16_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3), 50, "%");
         lV104WWAreaTrabalhoDS_16_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3), 50, "%");
         lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = StringUtil.Concat( StringUtil.RTrim( AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao), "%", "");
         lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial), 100, "%");
         lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj), "%", "");
         lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone), 20, "%");
         lV117WWAreaTrabalhoDS_29_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV117WWAreaTrabalhoDS_29_Tfestado_uf), 2, "%");
         lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome), 50, "%");
         /* Using cursor P00E34 */
         pr_default.execute(2, new Object[] {lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1, lV92WWAreaTrabalhoDS_4_Organizacao_nome1, lV92WWAreaTrabalhoDS_4_Organizacao_nome1, lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2, lV98WWAreaTrabalhoDS_10_Organizacao_nome2, lV98WWAreaTrabalhoDS_10_Organizacao_nome2, lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3, lV104WWAreaTrabalhoDS_16_Organizacao_nome3, lV104WWAreaTrabalhoDS_16_Organizacao_nome3, lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao, AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel, lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial, AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel, lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj, AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel, lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone, AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel, AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar, AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to, lV117WWAreaTrabalhoDS_29_Tfestado_uf, AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel, lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome, AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKE36 = false;
            A29Contratante_Codigo = P00E34_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00E34_n29Contratante_Codigo[0];
            A25Municipio_Codigo = P00E34_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00E34_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00E34_A335Contratante_PessoaCod[0];
            A1216AreaTrabalho_OrganizacaoCod = P00E34_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = P00E34_n1216AreaTrabalho_OrganizacaoCod[0];
            A12Contratante_CNPJ = P00E34_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00E34_n12Contratante_CNPJ[0];
            A5AreaTrabalho_Codigo = P00E34_A5AreaTrabalho_Codigo[0];
            A2081AreaTrabalho_VerTA = P00E34_A2081AreaTrabalho_VerTA[0];
            n2081AreaTrabalho_VerTA = P00E34_n2081AreaTrabalho_VerTA[0];
            A642AreaTrabalho_CalculoPFinal = P00E34_A642AreaTrabalho_CalculoPFinal[0];
            A26Municipio_Nome = P00E34_A26Municipio_Nome[0];
            A23Estado_UF = P00E34_A23Estado_UF[0];
            A855AreaTrabalho_DiasParaPagar = P00E34_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = P00E34_n855AreaTrabalho_DiasParaPagar[0];
            A834AreaTrabalho_ValidaOSFM = P00E34_A834AreaTrabalho_ValidaOSFM[0];
            n834AreaTrabalho_ValidaOSFM = P00E34_n834AreaTrabalho_ValidaOSFM[0];
            A31Contratante_Telefone = P00E34_A31Contratante_Telefone[0];
            A9Contratante_RazaoSocial = P00E34_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00E34_n9Contratante_RazaoSocial[0];
            A72AreaTrabalho_Ativo = P00E34_A72AreaTrabalho_Ativo[0];
            A1214Organizacao_Nome = P00E34_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00E34_n1214Organizacao_Nome[0];
            A6AreaTrabalho_Descricao = P00E34_A6AreaTrabalho_Descricao[0];
            A25Municipio_Codigo = P00E34_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00E34_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00E34_A335Contratante_PessoaCod[0];
            A31Contratante_Telefone = P00E34_A31Contratante_Telefone[0];
            A26Municipio_Nome = P00E34_A26Municipio_Nome[0];
            A23Estado_UF = P00E34_A23Estado_UF[0];
            A12Contratante_CNPJ = P00E34_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00E34_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00E34_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00E34_n9Contratante_RazaoSocial[0];
            A1214Organizacao_Nome = P00E34_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00E34_n1214Organizacao_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00E34_A12Contratante_CNPJ[0], A12Contratante_CNPJ) == 0 ) )
            {
               BRKE36 = false;
               A29Contratante_Codigo = P00E34_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00E34_n29Contratante_Codigo[0];
               A335Contratante_PessoaCod = P00E34_A335Contratante_PessoaCod[0];
               A5AreaTrabalho_Codigo = P00E34_A5AreaTrabalho_Codigo[0];
               A335Contratante_PessoaCod = P00E34_A335Contratante_PessoaCod[0];
               AV34count = (long)(AV34count+1);
               BRKE36 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A12Contratante_CNPJ)) )
            {
               AV26Option = A12Contratante_CNPJ;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKE36 )
            {
               BRKE36 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATANTE_TELEFONEOPTIONS' Routine */
         AV16TFContratante_Telefone = AV22SearchTxt;
         AV17TFContratante_Telefone_Sel = "";
         AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 = AV44DynamicFiltersSelector1;
         AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 = AV65DynamicFiltersOperator1;
         AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = AV45AreaTrabalho_Descricao1;
         AV92WWAreaTrabalhoDS_4_Organizacao_nome1 = AV66Organizacao_Nome1;
         AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 = AV71AreaTrabalho_Ativo1;
         AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 = AV68DynamicFiltersOperator2;
         AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = AV49AreaTrabalho_Descricao2;
         AV98WWAreaTrabalhoDS_10_Organizacao_nome2 = AV69Organizacao_Nome2;
         AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 = AV72AreaTrabalho_Ativo2;
         AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 = AV75DynamicFiltersEnabled3;
         AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 = AV76DynamicFiltersSelector3;
         AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 = AV77DynamicFiltersOperator3;
         AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = AV78AreaTrabalho_Descricao3;
         AV104WWAreaTrabalhoDS_16_Organizacao_nome3 = AV79Organizacao_Nome3;
         AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 = AV80AreaTrabalho_Ativo3;
         AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = AV10TFAreaTrabalho_Descricao;
         AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel = AV11TFAreaTrabalho_Descricao_Sel;
         AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = AV12TFContratante_RazaoSocial;
         AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel = AV13TFContratante_RazaoSocial_Sel;
         AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = AV14TFContratante_CNPJ;
         AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel = AV15TFContratante_CNPJ_Sel;
         AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = AV16TFContratante_Telefone;
         AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel = AV17TFContratante_Telefone_Sel;
         AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel = AV18TFAreaTrabalho_ValidaOSFM_Sel;
         AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar = AV19TFAreaTrabalho_DiasParaPagar;
         AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to = AV20TFAreaTrabalho_DiasParaPagar_To;
         AV117WWAreaTrabalhoDS_29_Tfestado_uf = AV73TFEstado_UF;
         AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel = AV74TFEstado_UF_Sel;
         AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = AV59TFMunicipio_Nome;
         AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel = AV60TFMunicipio_Nome_Sel;
         AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels = AV62TFAreaTrabalho_CalculoPFinal_Sels;
         AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels = AV84TFAreaTrabalho_VerTA_Sels;
         AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel = AV21TFAreaTrabalho_Ativo_Sel;
         AV133Udparg39 = new prc_areasdousuario(context).executeUdp(  AV9WWPContext.gxTpr_Userid);
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A642AreaTrabalho_CalculoPFinal ,
                                              AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels ,
                                              A2081AreaTrabalho_VerTA ,
                                              AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels ,
                                              A5AreaTrabalho_Codigo ,
                                              AV133Udparg39 ,
                                              AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 ,
                                              AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ,
                                              AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 ,
                                              AV92WWAreaTrabalhoDS_4_Organizacao_nome1 ,
                                              AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 ,
                                              AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 ,
                                              AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 ,
                                              AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ,
                                              AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 ,
                                              AV98WWAreaTrabalhoDS_10_Organizacao_nome2 ,
                                              AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 ,
                                              AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 ,
                                              AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 ,
                                              AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ,
                                              AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 ,
                                              AV104WWAreaTrabalhoDS_16_Organizacao_nome3 ,
                                              AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 ,
                                              AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel ,
                                              AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ,
                                              AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel ,
                                              AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ,
                                              AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel ,
                                              AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ,
                                              AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel ,
                                              AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ,
                                              AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel ,
                                              AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar ,
                                              AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to ,
                                              AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel ,
                                              AV117WWAreaTrabalhoDS_29_Tfestado_uf ,
                                              AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel ,
                                              AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ,
                                              AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels.Count ,
                                              AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels.Count ,
                                              AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel ,
                                              AV9WWPContext.gxTpr_Contratante_codigo ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              A6AreaTrabalho_Descricao ,
                                              A1214Organizacao_Nome ,
                                              A72AreaTrabalho_Ativo ,
                                              A9Contratante_RazaoSocial ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A834AreaTrabalho_ValidaOSFM ,
                                              A855AreaTrabalho_DiasParaPagar ,
                                              A23Estado_UF ,
                                              A26Municipio_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1), "%", "");
         lV92WWAreaTrabalhoDS_4_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1), 50, "%");
         lV92WWAreaTrabalhoDS_4_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1), 50, "%");
         lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2), "%", "");
         lV98WWAreaTrabalhoDS_10_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2), 50, "%");
         lV98WWAreaTrabalhoDS_10_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2), 50, "%");
         lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3), "%", "");
         lV104WWAreaTrabalhoDS_16_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3), 50, "%");
         lV104WWAreaTrabalhoDS_16_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3), 50, "%");
         lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = StringUtil.Concat( StringUtil.RTrim( AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao), "%", "");
         lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial), 100, "%");
         lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj), "%", "");
         lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone), 20, "%");
         lV117WWAreaTrabalhoDS_29_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV117WWAreaTrabalhoDS_29_Tfestado_uf), 2, "%");
         lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome), 50, "%");
         /* Using cursor P00E35 */
         pr_default.execute(3, new Object[] {lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1, lV92WWAreaTrabalhoDS_4_Organizacao_nome1, lV92WWAreaTrabalhoDS_4_Organizacao_nome1, lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2, lV98WWAreaTrabalhoDS_10_Organizacao_nome2, lV98WWAreaTrabalhoDS_10_Organizacao_nome2, lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3, lV104WWAreaTrabalhoDS_16_Organizacao_nome3, lV104WWAreaTrabalhoDS_16_Organizacao_nome3, lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao, AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel, lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial, AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel, lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj, AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel, lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone, AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel, AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar, AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to, lV117WWAreaTrabalhoDS_29_Tfestado_uf, AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel, lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome, AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKE38 = false;
            A29Contratante_Codigo = P00E35_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00E35_n29Contratante_Codigo[0];
            A25Municipio_Codigo = P00E35_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00E35_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00E35_A335Contratante_PessoaCod[0];
            A1216AreaTrabalho_OrganizacaoCod = P00E35_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = P00E35_n1216AreaTrabalho_OrganizacaoCod[0];
            A31Contratante_Telefone = P00E35_A31Contratante_Telefone[0];
            A5AreaTrabalho_Codigo = P00E35_A5AreaTrabalho_Codigo[0];
            A2081AreaTrabalho_VerTA = P00E35_A2081AreaTrabalho_VerTA[0];
            n2081AreaTrabalho_VerTA = P00E35_n2081AreaTrabalho_VerTA[0];
            A642AreaTrabalho_CalculoPFinal = P00E35_A642AreaTrabalho_CalculoPFinal[0];
            A26Municipio_Nome = P00E35_A26Municipio_Nome[0];
            A23Estado_UF = P00E35_A23Estado_UF[0];
            A855AreaTrabalho_DiasParaPagar = P00E35_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = P00E35_n855AreaTrabalho_DiasParaPagar[0];
            A834AreaTrabalho_ValidaOSFM = P00E35_A834AreaTrabalho_ValidaOSFM[0];
            n834AreaTrabalho_ValidaOSFM = P00E35_n834AreaTrabalho_ValidaOSFM[0];
            A12Contratante_CNPJ = P00E35_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00E35_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00E35_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00E35_n9Contratante_RazaoSocial[0];
            A72AreaTrabalho_Ativo = P00E35_A72AreaTrabalho_Ativo[0];
            A1214Organizacao_Nome = P00E35_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00E35_n1214Organizacao_Nome[0];
            A6AreaTrabalho_Descricao = P00E35_A6AreaTrabalho_Descricao[0];
            A25Municipio_Codigo = P00E35_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00E35_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00E35_A335Contratante_PessoaCod[0];
            A31Contratante_Telefone = P00E35_A31Contratante_Telefone[0];
            A26Municipio_Nome = P00E35_A26Municipio_Nome[0];
            A23Estado_UF = P00E35_A23Estado_UF[0];
            A12Contratante_CNPJ = P00E35_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00E35_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00E35_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00E35_n9Contratante_RazaoSocial[0];
            A1214Organizacao_Nome = P00E35_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00E35_n1214Organizacao_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00E35_A31Contratante_Telefone[0], A31Contratante_Telefone) == 0 ) )
            {
               BRKE38 = false;
               A29Contratante_Codigo = P00E35_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00E35_n29Contratante_Codigo[0];
               A5AreaTrabalho_Codigo = P00E35_A5AreaTrabalho_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKE38 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A31Contratante_Telefone)) )
            {
               AV26Option = A31Contratante_Telefone;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKE38 )
            {
               BRKE38 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADESTADO_UFOPTIONS' Routine */
         AV73TFEstado_UF = AV22SearchTxt;
         AV74TFEstado_UF_Sel = "";
         AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 = AV44DynamicFiltersSelector1;
         AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 = AV65DynamicFiltersOperator1;
         AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = AV45AreaTrabalho_Descricao1;
         AV92WWAreaTrabalhoDS_4_Organizacao_nome1 = AV66Organizacao_Nome1;
         AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 = AV71AreaTrabalho_Ativo1;
         AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 = AV68DynamicFiltersOperator2;
         AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = AV49AreaTrabalho_Descricao2;
         AV98WWAreaTrabalhoDS_10_Organizacao_nome2 = AV69Organizacao_Nome2;
         AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 = AV72AreaTrabalho_Ativo2;
         AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 = AV75DynamicFiltersEnabled3;
         AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 = AV76DynamicFiltersSelector3;
         AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 = AV77DynamicFiltersOperator3;
         AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = AV78AreaTrabalho_Descricao3;
         AV104WWAreaTrabalhoDS_16_Organizacao_nome3 = AV79Organizacao_Nome3;
         AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 = AV80AreaTrabalho_Ativo3;
         AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = AV10TFAreaTrabalho_Descricao;
         AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel = AV11TFAreaTrabalho_Descricao_Sel;
         AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = AV12TFContratante_RazaoSocial;
         AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel = AV13TFContratante_RazaoSocial_Sel;
         AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = AV14TFContratante_CNPJ;
         AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel = AV15TFContratante_CNPJ_Sel;
         AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = AV16TFContratante_Telefone;
         AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel = AV17TFContratante_Telefone_Sel;
         AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel = AV18TFAreaTrabalho_ValidaOSFM_Sel;
         AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar = AV19TFAreaTrabalho_DiasParaPagar;
         AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to = AV20TFAreaTrabalho_DiasParaPagar_To;
         AV117WWAreaTrabalhoDS_29_Tfestado_uf = AV73TFEstado_UF;
         AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel = AV74TFEstado_UF_Sel;
         AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = AV59TFMunicipio_Nome;
         AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel = AV60TFMunicipio_Nome_Sel;
         AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels = AV62TFAreaTrabalho_CalculoPFinal_Sels;
         AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels = AV84TFAreaTrabalho_VerTA_Sels;
         AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel = AV21TFAreaTrabalho_Ativo_Sel;
         AV136Udparg40 = new prc_areasdousuario(context).executeUdp(  AV9WWPContext.gxTpr_Userid);
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A642AreaTrabalho_CalculoPFinal ,
                                              AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels ,
                                              A2081AreaTrabalho_VerTA ,
                                              AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels ,
                                              A5AreaTrabalho_Codigo ,
                                              AV136Udparg40 ,
                                              AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 ,
                                              AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ,
                                              AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 ,
                                              AV92WWAreaTrabalhoDS_4_Organizacao_nome1 ,
                                              AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 ,
                                              AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 ,
                                              AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 ,
                                              AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ,
                                              AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 ,
                                              AV98WWAreaTrabalhoDS_10_Organizacao_nome2 ,
                                              AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 ,
                                              AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 ,
                                              AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 ,
                                              AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ,
                                              AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 ,
                                              AV104WWAreaTrabalhoDS_16_Organizacao_nome3 ,
                                              AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 ,
                                              AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel ,
                                              AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ,
                                              AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel ,
                                              AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ,
                                              AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel ,
                                              AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ,
                                              AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel ,
                                              AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ,
                                              AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel ,
                                              AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar ,
                                              AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to ,
                                              AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel ,
                                              AV117WWAreaTrabalhoDS_29_Tfestado_uf ,
                                              AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel ,
                                              AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ,
                                              AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels.Count ,
                                              AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels.Count ,
                                              AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel ,
                                              AV9WWPContext.gxTpr_Contratante_codigo ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              A6AreaTrabalho_Descricao ,
                                              A1214Organizacao_Nome ,
                                              A72AreaTrabalho_Ativo ,
                                              A9Contratante_RazaoSocial ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A834AreaTrabalho_ValidaOSFM ,
                                              A855AreaTrabalho_DiasParaPagar ,
                                              A23Estado_UF ,
                                              A26Municipio_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1), "%", "");
         lV92WWAreaTrabalhoDS_4_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1), 50, "%");
         lV92WWAreaTrabalhoDS_4_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1), 50, "%");
         lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2), "%", "");
         lV98WWAreaTrabalhoDS_10_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2), 50, "%");
         lV98WWAreaTrabalhoDS_10_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2), 50, "%");
         lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3), "%", "");
         lV104WWAreaTrabalhoDS_16_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3), 50, "%");
         lV104WWAreaTrabalhoDS_16_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3), 50, "%");
         lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = StringUtil.Concat( StringUtil.RTrim( AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao), "%", "");
         lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial), 100, "%");
         lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj), "%", "");
         lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone), 20, "%");
         lV117WWAreaTrabalhoDS_29_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV117WWAreaTrabalhoDS_29_Tfestado_uf), 2, "%");
         lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome), 50, "%");
         /* Using cursor P00E36 */
         pr_default.execute(4, new Object[] {lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1, lV92WWAreaTrabalhoDS_4_Organizacao_nome1, lV92WWAreaTrabalhoDS_4_Organizacao_nome1, lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2, lV98WWAreaTrabalhoDS_10_Organizacao_nome2, lV98WWAreaTrabalhoDS_10_Organizacao_nome2, lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3, lV104WWAreaTrabalhoDS_16_Organizacao_nome3, lV104WWAreaTrabalhoDS_16_Organizacao_nome3, lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao, AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel, lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial, AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel, lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj, AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel, lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone, AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel, AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar, AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to, lV117WWAreaTrabalhoDS_29_Tfestado_uf, AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel, lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome, AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKE310 = false;
            A29Contratante_Codigo = P00E36_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00E36_n29Contratante_Codigo[0];
            A25Municipio_Codigo = P00E36_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00E36_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00E36_A335Contratante_PessoaCod[0];
            A1216AreaTrabalho_OrganizacaoCod = P00E36_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = P00E36_n1216AreaTrabalho_OrganizacaoCod[0];
            A23Estado_UF = P00E36_A23Estado_UF[0];
            A5AreaTrabalho_Codigo = P00E36_A5AreaTrabalho_Codigo[0];
            A2081AreaTrabalho_VerTA = P00E36_A2081AreaTrabalho_VerTA[0];
            n2081AreaTrabalho_VerTA = P00E36_n2081AreaTrabalho_VerTA[0];
            A642AreaTrabalho_CalculoPFinal = P00E36_A642AreaTrabalho_CalculoPFinal[0];
            A26Municipio_Nome = P00E36_A26Municipio_Nome[0];
            A855AreaTrabalho_DiasParaPagar = P00E36_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = P00E36_n855AreaTrabalho_DiasParaPagar[0];
            A834AreaTrabalho_ValidaOSFM = P00E36_A834AreaTrabalho_ValidaOSFM[0];
            n834AreaTrabalho_ValidaOSFM = P00E36_n834AreaTrabalho_ValidaOSFM[0];
            A31Contratante_Telefone = P00E36_A31Contratante_Telefone[0];
            A12Contratante_CNPJ = P00E36_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00E36_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00E36_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00E36_n9Contratante_RazaoSocial[0];
            A72AreaTrabalho_Ativo = P00E36_A72AreaTrabalho_Ativo[0];
            A1214Organizacao_Nome = P00E36_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00E36_n1214Organizacao_Nome[0];
            A6AreaTrabalho_Descricao = P00E36_A6AreaTrabalho_Descricao[0];
            A25Municipio_Codigo = P00E36_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00E36_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00E36_A335Contratante_PessoaCod[0];
            A31Contratante_Telefone = P00E36_A31Contratante_Telefone[0];
            A23Estado_UF = P00E36_A23Estado_UF[0];
            A26Municipio_Nome = P00E36_A26Municipio_Nome[0];
            A12Contratante_CNPJ = P00E36_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00E36_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00E36_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00E36_n9Contratante_RazaoSocial[0];
            A1214Organizacao_Nome = P00E36_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00E36_n1214Organizacao_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00E36_A23Estado_UF[0], A23Estado_UF) == 0 ) )
            {
               BRKE310 = false;
               A29Contratante_Codigo = P00E36_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00E36_n29Contratante_Codigo[0];
               A25Municipio_Codigo = P00E36_A25Municipio_Codigo[0];
               n25Municipio_Codigo = P00E36_n25Municipio_Codigo[0];
               A5AreaTrabalho_Codigo = P00E36_A5AreaTrabalho_Codigo[0];
               A25Municipio_Codigo = P00E36_A25Municipio_Codigo[0];
               n25Municipio_Codigo = P00E36_n25Municipio_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKE310 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A23Estado_UF)) )
            {
               AV26Option = A23Estado_UF;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKE310 )
            {
               BRKE310 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      protected void S171( )
      {
         /* 'LOADMUNICIPIO_NOMEOPTIONS' Routine */
         AV59TFMunicipio_Nome = AV22SearchTxt;
         AV60TFMunicipio_Nome_Sel = "";
         AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 = AV44DynamicFiltersSelector1;
         AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 = AV65DynamicFiltersOperator1;
         AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = AV45AreaTrabalho_Descricao1;
         AV92WWAreaTrabalhoDS_4_Organizacao_nome1 = AV66Organizacao_Nome1;
         AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 = AV71AreaTrabalho_Ativo1;
         AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 = AV68DynamicFiltersOperator2;
         AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = AV49AreaTrabalho_Descricao2;
         AV98WWAreaTrabalhoDS_10_Organizacao_nome2 = AV69Organizacao_Nome2;
         AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 = AV72AreaTrabalho_Ativo2;
         AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 = AV75DynamicFiltersEnabled3;
         AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 = AV76DynamicFiltersSelector3;
         AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 = AV77DynamicFiltersOperator3;
         AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = AV78AreaTrabalho_Descricao3;
         AV104WWAreaTrabalhoDS_16_Organizacao_nome3 = AV79Organizacao_Nome3;
         AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 = AV80AreaTrabalho_Ativo3;
         AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = AV10TFAreaTrabalho_Descricao;
         AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel = AV11TFAreaTrabalho_Descricao_Sel;
         AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = AV12TFContratante_RazaoSocial;
         AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel = AV13TFContratante_RazaoSocial_Sel;
         AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = AV14TFContratante_CNPJ;
         AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel = AV15TFContratante_CNPJ_Sel;
         AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = AV16TFContratante_Telefone;
         AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel = AV17TFContratante_Telefone_Sel;
         AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel = AV18TFAreaTrabalho_ValidaOSFM_Sel;
         AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar = AV19TFAreaTrabalho_DiasParaPagar;
         AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to = AV20TFAreaTrabalho_DiasParaPagar_To;
         AV117WWAreaTrabalhoDS_29_Tfestado_uf = AV73TFEstado_UF;
         AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel = AV74TFEstado_UF_Sel;
         AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = AV59TFMunicipio_Nome;
         AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel = AV60TFMunicipio_Nome_Sel;
         AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels = AV62TFAreaTrabalho_CalculoPFinal_Sels;
         AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels = AV84TFAreaTrabalho_VerTA_Sels;
         AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel = AV21TFAreaTrabalho_Ativo_Sel;
         AV139Udparg41 = new prc_areasdousuario(context).executeUdp(  AV9WWPContext.gxTpr_Userid);
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              A642AreaTrabalho_CalculoPFinal ,
                                              AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels ,
                                              A2081AreaTrabalho_VerTA ,
                                              AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels ,
                                              A5AreaTrabalho_Codigo ,
                                              AV139Udparg41 ,
                                              AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 ,
                                              AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ,
                                              AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 ,
                                              AV92WWAreaTrabalhoDS_4_Organizacao_nome1 ,
                                              AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 ,
                                              AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 ,
                                              AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 ,
                                              AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ,
                                              AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 ,
                                              AV98WWAreaTrabalhoDS_10_Organizacao_nome2 ,
                                              AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 ,
                                              AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 ,
                                              AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 ,
                                              AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ,
                                              AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 ,
                                              AV104WWAreaTrabalhoDS_16_Organizacao_nome3 ,
                                              AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 ,
                                              AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel ,
                                              AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ,
                                              AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel ,
                                              AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ,
                                              AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel ,
                                              AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ,
                                              AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel ,
                                              AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ,
                                              AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel ,
                                              AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar ,
                                              AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to ,
                                              AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel ,
                                              AV117WWAreaTrabalhoDS_29_Tfestado_uf ,
                                              AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel ,
                                              AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ,
                                              AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels.Count ,
                                              AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels.Count ,
                                              AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel ,
                                              AV9WWPContext.gxTpr_Contratante_codigo ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              A6AreaTrabalho_Descricao ,
                                              A1214Organizacao_Nome ,
                                              A72AreaTrabalho_Ativo ,
                                              A9Contratante_RazaoSocial ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A834AreaTrabalho_ValidaOSFM ,
                                              A855AreaTrabalho_DiasParaPagar ,
                                              A23Estado_UF ,
                                              A26Municipio_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1), "%", "");
         lV92WWAreaTrabalhoDS_4_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1), 50, "%");
         lV92WWAreaTrabalhoDS_4_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1), 50, "%");
         lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2), "%", "");
         lV98WWAreaTrabalhoDS_10_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2), 50, "%");
         lV98WWAreaTrabalhoDS_10_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2), 50, "%");
         lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3), "%", "");
         lV104WWAreaTrabalhoDS_16_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3), 50, "%");
         lV104WWAreaTrabalhoDS_16_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3), 50, "%");
         lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = StringUtil.Concat( StringUtil.RTrim( AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao), "%", "");
         lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial), 100, "%");
         lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj), "%", "");
         lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone), 20, "%");
         lV117WWAreaTrabalhoDS_29_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV117WWAreaTrabalhoDS_29_Tfestado_uf), 2, "%");
         lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome), 50, "%");
         /* Using cursor P00E37 */
         pr_default.execute(5, new Object[] {lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1, lV92WWAreaTrabalhoDS_4_Organizacao_nome1, lV92WWAreaTrabalhoDS_4_Organizacao_nome1, lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2, lV98WWAreaTrabalhoDS_10_Organizacao_nome2, lV98WWAreaTrabalhoDS_10_Organizacao_nome2, lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3, lV104WWAreaTrabalhoDS_16_Organizacao_nome3, lV104WWAreaTrabalhoDS_16_Organizacao_nome3, lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao, AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel, lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial, AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel, lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj, AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel, lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone, AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel, AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar, AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to, lV117WWAreaTrabalhoDS_29_Tfestado_uf, AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel, lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome, AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel});
         while ( (pr_default.getStatus(5) != 101) )
         {
            BRKE312 = false;
            A29Contratante_Codigo = P00E37_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00E37_n29Contratante_Codigo[0];
            A25Municipio_Codigo = P00E37_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00E37_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00E37_A335Contratante_PessoaCod[0];
            A1216AreaTrabalho_OrganizacaoCod = P00E37_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = P00E37_n1216AreaTrabalho_OrganizacaoCod[0];
            A26Municipio_Nome = P00E37_A26Municipio_Nome[0];
            A5AreaTrabalho_Codigo = P00E37_A5AreaTrabalho_Codigo[0];
            A2081AreaTrabalho_VerTA = P00E37_A2081AreaTrabalho_VerTA[0];
            n2081AreaTrabalho_VerTA = P00E37_n2081AreaTrabalho_VerTA[0];
            A642AreaTrabalho_CalculoPFinal = P00E37_A642AreaTrabalho_CalculoPFinal[0];
            A23Estado_UF = P00E37_A23Estado_UF[0];
            A855AreaTrabalho_DiasParaPagar = P00E37_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = P00E37_n855AreaTrabalho_DiasParaPagar[0];
            A834AreaTrabalho_ValidaOSFM = P00E37_A834AreaTrabalho_ValidaOSFM[0];
            n834AreaTrabalho_ValidaOSFM = P00E37_n834AreaTrabalho_ValidaOSFM[0];
            A31Contratante_Telefone = P00E37_A31Contratante_Telefone[0];
            A12Contratante_CNPJ = P00E37_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00E37_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00E37_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00E37_n9Contratante_RazaoSocial[0];
            A72AreaTrabalho_Ativo = P00E37_A72AreaTrabalho_Ativo[0];
            A1214Organizacao_Nome = P00E37_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00E37_n1214Organizacao_Nome[0];
            A6AreaTrabalho_Descricao = P00E37_A6AreaTrabalho_Descricao[0];
            A25Municipio_Codigo = P00E37_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00E37_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00E37_A335Contratante_PessoaCod[0];
            A31Contratante_Telefone = P00E37_A31Contratante_Telefone[0];
            A26Municipio_Nome = P00E37_A26Municipio_Nome[0];
            A23Estado_UF = P00E37_A23Estado_UF[0];
            A12Contratante_CNPJ = P00E37_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00E37_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00E37_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00E37_n9Contratante_RazaoSocial[0];
            A1214Organizacao_Nome = P00E37_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00E37_n1214Organizacao_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(5) != 101) && ( StringUtil.StrCmp(P00E37_A26Municipio_Nome[0], A26Municipio_Nome) == 0 ) )
            {
               BRKE312 = false;
               A29Contratante_Codigo = P00E37_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00E37_n29Contratante_Codigo[0];
               A25Municipio_Codigo = P00E37_A25Municipio_Codigo[0];
               n25Municipio_Codigo = P00E37_n25Municipio_Codigo[0];
               A5AreaTrabalho_Codigo = P00E37_A5AreaTrabalho_Codigo[0];
               A25Municipio_Codigo = P00E37_A25Municipio_Codigo[0];
               n25Municipio_Codigo = P00E37_n25Municipio_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKE312 = true;
               pr_default.readNext(5);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A26Municipio_Nome)) )
            {
               AV26Option = A26Municipio_Nome;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKE312 )
            {
               BRKE312 = true;
               pr_default.readNext(5);
            }
         }
         pr_default.close(5);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFAreaTrabalho_Descricao = "";
         AV11TFAreaTrabalho_Descricao_Sel = "";
         AV12TFContratante_RazaoSocial = "";
         AV13TFContratante_RazaoSocial_Sel = "";
         AV14TFContratante_CNPJ = "";
         AV15TFContratante_CNPJ_Sel = "";
         AV16TFContratante_Telefone = "";
         AV17TFContratante_Telefone_Sel = "";
         AV73TFEstado_UF = "";
         AV74TFEstado_UF_Sel = "";
         AV59TFMunicipio_Nome = "";
         AV60TFMunicipio_Nome_Sel = "";
         AV61TFAreaTrabalho_CalculoPFinal_SelsJson = "";
         AV62TFAreaTrabalho_CalculoPFinal_Sels = new GxSimpleCollection();
         AV83TFAreaTrabalho_VerTA_SelsJson = "";
         AV84TFAreaTrabalho_VerTA_Sels = new GxSimpleCollection();
         AV39GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV44DynamicFiltersSelector1 = "";
         AV45AreaTrabalho_Descricao1 = "";
         AV66Organizacao_Nome1 = "";
         AV71AreaTrabalho_Ativo1 = "";
         AV48DynamicFiltersSelector2 = "";
         AV49AreaTrabalho_Descricao2 = "";
         AV69Organizacao_Nome2 = "";
         AV72AreaTrabalho_Ativo2 = "";
         AV76DynamicFiltersSelector3 = "";
         AV78AreaTrabalho_Descricao3 = "";
         AV79Organizacao_Nome3 = "";
         AV80AreaTrabalho_Ativo3 = "";
         AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 = "";
         AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = "";
         AV92WWAreaTrabalhoDS_4_Organizacao_nome1 = "";
         AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 = "";
         AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 = "";
         AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = "";
         AV98WWAreaTrabalhoDS_10_Organizacao_nome2 = "";
         AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 = "";
         AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 = "";
         AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = "";
         AV104WWAreaTrabalhoDS_16_Organizacao_nome3 = "";
         AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 = "";
         AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = "";
         AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel = "";
         AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = "";
         AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel = "";
         AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = "";
         AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel = "";
         AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = "";
         AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel = "";
         AV117WWAreaTrabalhoDS_29_Tfestado_uf = "";
         AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel = "";
         AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = "";
         AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel = "";
         AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels = new GxSimpleCollection();
         AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels = new GxSimpleCollection();
         AV124Udparg36 = new GxSimpleCollection();
         scmdbuf = "";
         lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 = "";
         lV92WWAreaTrabalhoDS_4_Organizacao_nome1 = "";
         lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 = "";
         lV98WWAreaTrabalhoDS_10_Organizacao_nome2 = "";
         lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 = "";
         lV104WWAreaTrabalhoDS_16_Organizacao_nome3 = "";
         lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao = "";
         lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial = "";
         lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj = "";
         lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone = "";
         lV117WWAreaTrabalhoDS_29_Tfestado_uf = "";
         lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome = "";
         A642AreaTrabalho_CalculoPFinal = "";
         A6AreaTrabalho_Descricao = "";
         A1214Organizacao_Nome = "";
         A9Contratante_RazaoSocial = "";
         A12Contratante_CNPJ = "";
         A31Contratante_Telefone = "";
         A23Estado_UF = "";
         A26Municipio_Nome = "";
         P00E32_A29Contratante_Codigo = new int[1] ;
         P00E32_n29Contratante_Codigo = new bool[] {false} ;
         P00E32_A25Municipio_Codigo = new int[1] ;
         P00E32_n25Municipio_Codigo = new bool[] {false} ;
         P00E32_A335Contratante_PessoaCod = new int[1] ;
         P00E32_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         P00E32_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         P00E32_A6AreaTrabalho_Descricao = new String[] {""} ;
         P00E32_A5AreaTrabalho_Codigo = new int[1] ;
         P00E32_A2081AreaTrabalho_VerTA = new short[1] ;
         P00E32_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         P00E32_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         P00E32_A26Municipio_Nome = new String[] {""} ;
         P00E32_A23Estado_UF = new String[] {""} ;
         P00E32_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         P00E32_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         P00E32_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00E32_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00E32_A31Contratante_Telefone = new String[] {""} ;
         P00E32_A12Contratante_CNPJ = new String[] {""} ;
         P00E32_n12Contratante_CNPJ = new bool[] {false} ;
         P00E32_A9Contratante_RazaoSocial = new String[] {""} ;
         P00E32_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00E32_A72AreaTrabalho_Ativo = new bool[] {false} ;
         P00E32_A1214Organizacao_Nome = new String[] {""} ;
         P00E32_n1214Organizacao_Nome = new bool[] {false} ;
         AV26Option = "";
         AV127Udparg37 = new GxSimpleCollection();
         P00E33_A29Contratante_Codigo = new int[1] ;
         P00E33_n29Contratante_Codigo = new bool[] {false} ;
         P00E33_A25Municipio_Codigo = new int[1] ;
         P00E33_n25Municipio_Codigo = new bool[] {false} ;
         P00E33_A335Contratante_PessoaCod = new int[1] ;
         P00E33_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         P00E33_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         P00E33_A9Contratante_RazaoSocial = new String[] {""} ;
         P00E33_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00E33_A5AreaTrabalho_Codigo = new int[1] ;
         P00E33_A2081AreaTrabalho_VerTA = new short[1] ;
         P00E33_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         P00E33_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         P00E33_A26Municipio_Nome = new String[] {""} ;
         P00E33_A23Estado_UF = new String[] {""} ;
         P00E33_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         P00E33_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         P00E33_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00E33_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00E33_A31Contratante_Telefone = new String[] {""} ;
         P00E33_A12Contratante_CNPJ = new String[] {""} ;
         P00E33_n12Contratante_CNPJ = new bool[] {false} ;
         P00E33_A72AreaTrabalho_Ativo = new bool[] {false} ;
         P00E33_A1214Organizacao_Nome = new String[] {""} ;
         P00E33_n1214Organizacao_Nome = new bool[] {false} ;
         P00E33_A6AreaTrabalho_Descricao = new String[] {""} ;
         AV130Udparg38 = new GxSimpleCollection();
         P00E34_A29Contratante_Codigo = new int[1] ;
         P00E34_n29Contratante_Codigo = new bool[] {false} ;
         P00E34_A25Municipio_Codigo = new int[1] ;
         P00E34_n25Municipio_Codigo = new bool[] {false} ;
         P00E34_A335Contratante_PessoaCod = new int[1] ;
         P00E34_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         P00E34_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         P00E34_A12Contratante_CNPJ = new String[] {""} ;
         P00E34_n12Contratante_CNPJ = new bool[] {false} ;
         P00E34_A5AreaTrabalho_Codigo = new int[1] ;
         P00E34_A2081AreaTrabalho_VerTA = new short[1] ;
         P00E34_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         P00E34_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         P00E34_A26Municipio_Nome = new String[] {""} ;
         P00E34_A23Estado_UF = new String[] {""} ;
         P00E34_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         P00E34_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         P00E34_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00E34_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00E34_A31Contratante_Telefone = new String[] {""} ;
         P00E34_A9Contratante_RazaoSocial = new String[] {""} ;
         P00E34_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00E34_A72AreaTrabalho_Ativo = new bool[] {false} ;
         P00E34_A1214Organizacao_Nome = new String[] {""} ;
         P00E34_n1214Organizacao_Nome = new bool[] {false} ;
         P00E34_A6AreaTrabalho_Descricao = new String[] {""} ;
         AV133Udparg39 = new GxSimpleCollection();
         P00E35_A29Contratante_Codigo = new int[1] ;
         P00E35_n29Contratante_Codigo = new bool[] {false} ;
         P00E35_A25Municipio_Codigo = new int[1] ;
         P00E35_n25Municipio_Codigo = new bool[] {false} ;
         P00E35_A335Contratante_PessoaCod = new int[1] ;
         P00E35_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         P00E35_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         P00E35_A31Contratante_Telefone = new String[] {""} ;
         P00E35_A5AreaTrabalho_Codigo = new int[1] ;
         P00E35_A2081AreaTrabalho_VerTA = new short[1] ;
         P00E35_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         P00E35_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         P00E35_A26Municipio_Nome = new String[] {""} ;
         P00E35_A23Estado_UF = new String[] {""} ;
         P00E35_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         P00E35_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         P00E35_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00E35_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00E35_A12Contratante_CNPJ = new String[] {""} ;
         P00E35_n12Contratante_CNPJ = new bool[] {false} ;
         P00E35_A9Contratante_RazaoSocial = new String[] {""} ;
         P00E35_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00E35_A72AreaTrabalho_Ativo = new bool[] {false} ;
         P00E35_A1214Organizacao_Nome = new String[] {""} ;
         P00E35_n1214Organizacao_Nome = new bool[] {false} ;
         P00E35_A6AreaTrabalho_Descricao = new String[] {""} ;
         AV136Udparg40 = new GxSimpleCollection();
         P00E36_A29Contratante_Codigo = new int[1] ;
         P00E36_n29Contratante_Codigo = new bool[] {false} ;
         P00E36_A25Municipio_Codigo = new int[1] ;
         P00E36_n25Municipio_Codigo = new bool[] {false} ;
         P00E36_A335Contratante_PessoaCod = new int[1] ;
         P00E36_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         P00E36_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         P00E36_A23Estado_UF = new String[] {""} ;
         P00E36_A5AreaTrabalho_Codigo = new int[1] ;
         P00E36_A2081AreaTrabalho_VerTA = new short[1] ;
         P00E36_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         P00E36_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         P00E36_A26Municipio_Nome = new String[] {""} ;
         P00E36_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         P00E36_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         P00E36_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00E36_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00E36_A31Contratante_Telefone = new String[] {""} ;
         P00E36_A12Contratante_CNPJ = new String[] {""} ;
         P00E36_n12Contratante_CNPJ = new bool[] {false} ;
         P00E36_A9Contratante_RazaoSocial = new String[] {""} ;
         P00E36_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00E36_A72AreaTrabalho_Ativo = new bool[] {false} ;
         P00E36_A1214Organizacao_Nome = new String[] {""} ;
         P00E36_n1214Organizacao_Nome = new bool[] {false} ;
         P00E36_A6AreaTrabalho_Descricao = new String[] {""} ;
         AV139Udparg41 = new GxSimpleCollection();
         P00E37_A29Contratante_Codigo = new int[1] ;
         P00E37_n29Contratante_Codigo = new bool[] {false} ;
         P00E37_A25Municipio_Codigo = new int[1] ;
         P00E37_n25Municipio_Codigo = new bool[] {false} ;
         P00E37_A335Contratante_PessoaCod = new int[1] ;
         P00E37_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         P00E37_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         P00E37_A26Municipio_Nome = new String[] {""} ;
         P00E37_A5AreaTrabalho_Codigo = new int[1] ;
         P00E37_A2081AreaTrabalho_VerTA = new short[1] ;
         P00E37_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         P00E37_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         P00E37_A23Estado_UF = new String[] {""} ;
         P00E37_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         P00E37_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         P00E37_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00E37_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00E37_A31Contratante_Telefone = new String[] {""} ;
         P00E37_A12Contratante_CNPJ = new String[] {""} ;
         P00E37_n12Contratante_CNPJ = new bool[] {false} ;
         P00E37_A9Contratante_RazaoSocial = new String[] {""} ;
         P00E37_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00E37_A72AreaTrabalho_Ativo = new bool[] {false} ;
         P00E37_A1214Organizacao_Nome = new String[] {""} ;
         P00E37_n1214Organizacao_Nome = new bool[] {false} ;
         P00E37_A6AreaTrabalho_Descricao = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwareatrabalhofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00E32_A29Contratante_Codigo, P00E32_n29Contratante_Codigo, P00E32_A25Municipio_Codigo, P00E32_n25Municipio_Codigo, P00E32_A335Contratante_PessoaCod, P00E32_A1216AreaTrabalho_OrganizacaoCod, P00E32_n1216AreaTrabalho_OrganizacaoCod, P00E32_A6AreaTrabalho_Descricao, P00E32_A5AreaTrabalho_Codigo, P00E32_A2081AreaTrabalho_VerTA,
               P00E32_n2081AreaTrabalho_VerTA, P00E32_A642AreaTrabalho_CalculoPFinal, P00E32_A26Municipio_Nome, P00E32_A23Estado_UF, P00E32_A855AreaTrabalho_DiasParaPagar, P00E32_n855AreaTrabalho_DiasParaPagar, P00E32_A834AreaTrabalho_ValidaOSFM, P00E32_n834AreaTrabalho_ValidaOSFM, P00E32_A31Contratante_Telefone, P00E32_A12Contratante_CNPJ,
               P00E32_n12Contratante_CNPJ, P00E32_A9Contratante_RazaoSocial, P00E32_n9Contratante_RazaoSocial, P00E32_A72AreaTrabalho_Ativo, P00E32_A1214Organizacao_Nome, P00E32_n1214Organizacao_Nome
               }
               , new Object[] {
               P00E33_A29Contratante_Codigo, P00E33_n29Contratante_Codigo, P00E33_A25Municipio_Codigo, P00E33_n25Municipio_Codigo, P00E33_A335Contratante_PessoaCod, P00E33_A1216AreaTrabalho_OrganizacaoCod, P00E33_n1216AreaTrabalho_OrganizacaoCod, P00E33_A9Contratante_RazaoSocial, P00E33_n9Contratante_RazaoSocial, P00E33_A5AreaTrabalho_Codigo,
               P00E33_A2081AreaTrabalho_VerTA, P00E33_n2081AreaTrabalho_VerTA, P00E33_A642AreaTrabalho_CalculoPFinal, P00E33_A26Municipio_Nome, P00E33_A23Estado_UF, P00E33_A855AreaTrabalho_DiasParaPagar, P00E33_n855AreaTrabalho_DiasParaPagar, P00E33_A834AreaTrabalho_ValidaOSFM, P00E33_n834AreaTrabalho_ValidaOSFM, P00E33_A31Contratante_Telefone,
               P00E33_A12Contratante_CNPJ, P00E33_n12Contratante_CNPJ, P00E33_A72AreaTrabalho_Ativo, P00E33_A1214Organizacao_Nome, P00E33_n1214Organizacao_Nome, P00E33_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00E34_A29Contratante_Codigo, P00E34_n29Contratante_Codigo, P00E34_A25Municipio_Codigo, P00E34_n25Municipio_Codigo, P00E34_A335Contratante_PessoaCod, P00E34_A1216AreaTrabalho_OrganizacaoCod, P00E34_n1216AreaTrabalho_OrganizacaoCod, P00E34_A12Contratante_CNPJ, P00E34_n12Contratante_CNPJ, P00E34_A5AreaTrabalho_Codigo,
               P00E34_A2081AreaTrabalho_VerTA, P00E34_n2081AreaTrabalho_VerTA, P00E34_A642AreaTrabalho_CalculoPFinal, P00E34_A26Municipio_Nome, P00E34_A23Estado_UF, P00E34_A855AreaTrabalho_DiasParaPagar, P00E34_n855AreaTrabalho_DiasParaPagar, P00E34_A834AreaTrabalho_ValidaOSFM, P00E34_n834AreaTrabalho_ValidaOSFM, P00E34_A31Contratante_Telefone,
               P00E34_A9Contratante_RazaoSocial, P00E34_n9Contratante_RazaoSocial, P00E34_A72AreaTrabalho_Ativo, P00E34_A1214Organizacao_Nome, P00E34_n1214Organizacao_Nome, P00E34_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00E35_A29Contratante_Codigo, P00E35_n29Contratante_Codigo, P00E35_A25Municipio_Codigo, P00E35_n25Municipio_Codigo, P00E35_A335Contratante_PessoaCod, P00E35_A1216AreaTrabalho_OrganizacaoCod, P00E35_n1216AreaTrabalho_OrganizacaoCod, P00E35_A31Contratante_Telefone, P00E35_A5AreaTrabalho_Codigo, P00E35_A2081AreaTrabalho_VerTA,
               P00E35_n2081AreaTrabalho_VerTA, P00E35_A642AreaTrabalho_CalculoPFinal, P00E35_A26Municipio_Nome, P00E35_A23Estado_UF, P00E35_A855AreaTrabalho_DiasParaPagar, P00E35_n855AreaTrabalho_DiasParaPagar, P00E35_A834AreaTrabalho_ValidaOSFM, P00E35_n834AreaTrabalho_ValidaOSFM, P00E35_A12Contratante_CNPJ, P00E35_n12Contratante_CNPJ,
               P00E35_A9Contratante_RazaoSocial, P00E35_n9Contratante_RazaoSocial, P00E35_A72AreaTrabalho_Ativo, P00E35_A1214Organizacao_Nome, P00E35_n1214Organizacao_Nome, P00E35_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00E36_A29Contratante_Codigo, P00E36_n29Contratante_Codigo, P00E36_A25Municipio_Codigo, P00E36_n25Municipio_Codigo, P00E36_A335Contratante_PessoaCod, P00E36_A1216AreaTrabalho_OrganizacaoCod, P00E36_n1216AreaTrabalho_OrganizacaoCod, P00E36_A23Estado_UF, P00E36_A5AreaTrabalho_Codigo, P00E36_A2081AreaTrabalho_VerTA,
               P00E36_n2081AreaTrabalho_VerTA, P00E36_A642AreaTrabalho_CalculoPFinal, P00E36_A26Municipio_Nome, P00E36_A855AreaTrabalho_DiasParaPagar, P00E36_n855AreaTrabalho_DiasParaPagar, P00E36_A834AreaTrabalho_ValidaOSFM, P00E36_n834AreaTrabalho_ValidaOSFM, P00E36_A31Contratante_Telefone, P00E36_A12Contratante_CNPJ, P00E36_n12Contratante_CNPJ,
               P00E36_A9Contratante_RazaoSocial, P00E36_n9Contratante_RazaoSocial, P00E36_A72AreaTrabalho_Ativo, P00E36_A1214Organizacao_Nome, P00E36_n1214Organizacao_Nome, P00E36_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00E37_A29Contratante_Codigo, P00E37_n29Contratante_Codigo, P00E37_A25Municipio_Codigo, P00E37_n25Municipio_Codigo, P00E37_A335Contratante_PessoaCod, P00E37_A1216AreaTrabalho_OrganizacaoCod, P00E37_n1216AreaTrabalho_OrganizacaoCod, P00E37_A26Municipio_Nome, P00E37_A5AreaTrabalho_Codigo, P00E37_A2081AreaTrabalho_VerTA,
               P00E37_n2081AreaTrabalho_VerTA, P00E37_A642AreaTrabalho_CalculoPFinal, P00E37_A23Estado_UF, P00E37_A855AreaTrabalho_DiasParaPagar, P00E37_n855AreaTrabalho_DiasParaPagar, P00E37_A834AreaTrabalho_ValidaOSFM, P00E37_n834AreaTrabalho_ValidaOSFM, P00E37_A31Contratante_Telefone, P00E37_A12Contratante_CNPJ, P00E37_n12Contratante_CNPJ,
               P00E37_A9Contratante_RazaoSocial, P00E37_n9Contratante_RazaoSocial, P00E37_A72AreaTrabalho_Ativo, P00E37_A1214Organizacao_Nome, P00E37_n1214Organizacao_Nome, P00E37_A6AreaTrabalho_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV18TFAreaTrabalho_ValidaOSFM_Sel ;
      private short AV19TFAreaTrabalho_DiasParaPagar ;
      private short AV20TFAreaTrabalho_DiasParaPagar_To ;
      private short AV21TFAreaTrabalho_Ativo_Sel ;
      private short AV65DynamicFiltersOperator1 ;
      private short AV68DynamicFiltersOperator2 ;
      private short AV77DynamicFiltersOperator3 ;
      private short AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 ;
      private short AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 ;
      private short AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 ;
      private short AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel ;
      private short AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar ;
      private short AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to ;
      private short AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel ;
      private short A2081AreaTrabalho_VerTA ;
      private short A855AreaTrabalho_DiasParaPagar ;
      private int AV87GXV1 ;
      private int AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels_Count ;
      private int AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels_Count ;
      private int AV9WWPContext_gxTpr_Contratante_codigo ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int A25Municipio_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int A1216AreaTrabalho_OrganizacaoCod ;
      private long AV34count ;
      private String AV12TFContratante_RazaoSocial ;
      private String AV13TFContratante_RazaoSocial_Sel ;
      private String AV16TFContratante_Telefone ;
      private String AV17TFContratante_Telefone_Sel ;
      private String AV73TFEstado_UF ;
      private String AV74TFEstado_UF_Sel ;
      private String AV59TFMunicipio_Nome ;
      private String AV60TFMunicipio_Nome_Sel ;
      private String AV66Organizacao_Nome1 ;
      private String AV71AreaTrabalho_Ativo1 ;
      private String AV69Organizacao_Nome2 ;
      private String AV72AreaTrabalho_Ativo2 ;
      private String AV79Organizacao_Nome3 ;
      private String AV80AreaTrabalho_Ativo3 ;
      private String AV92WWAreaTrabalhoDS_4_Organizacao_nome1 ;
      private String AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 ;
      private String AV98WWAreaTrabalhoDS_10_Organizacao_nome2 ;
      private String AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 ;
      private String AV104WWAreaTrabalhoDS_16_Organizacao_nome3 ;
      private String AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 ;
      private String AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ;
      private String AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel ;
      private String AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ;
      private String AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel ;
      private String AV117WWAreaTrabalhoDS_29_Tfestado_uf ;
      private String AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel ;
      private String AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ;
      private String AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel ;
      private String scmdbuf ;
      private String lV92WWAreaTrabalhoDS_4_Organizacao_nome1 ;
      private String lV98WWAreaTrabalhoDS_10_Organizacao_nome2 ;
      private String lV104WWAreaTrabalhoDS_16_Organizacao_nome3 ;
      private String lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ;
      private String lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ;
      private String lV117WWAreaTrabalhoDS_29_Tfestado_uf ;
      private String lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ;
      private String A642AreaTrabalho_CalculoPFinal ;
      private String A1214Organizacao_Nome ;
      private String A9Contratante_RazaoSocial ;
      private String A31Contratante_Telefone ;
      private String A23Estado_UF ;
      private String A26Municipio_Nome ;
      private bool returnInSub ;
      private bool AV47DynamicFiltersEnabled2 ;
      private bool AV75DynamicFiltersEnabled3 ;
      private bool AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 ;
      private bool AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 ;
      private bool A72AreaTrabalho_Ativo ;
      private bool A834AreaTrabalho_ValidaOSFM ;
      private bool BRKE32 ;
      private bool n29Contratante_Codigo ;
      private bool n25Municipio_Codigo ;
      private bool n1216AreaTrabalho_OrganizacaoCod ;
      private bool n2081AreaTrabalho_VerTA ;
      private bool n855AreaTrabalho_DiasParaPagar ;
      private bool n834AreaTrabalho_ValidaOSFM ;
      private bool n12Contratante_CNPJ ;
      private bool n9Contratante_RazaoSocial ;
      private bool n1214Organizacao_Nome ;
      private bool BRKE34 ;
      private bool BRKE36 ;
      private bool BRKE38 ;
      private bool BRKE310 ;
      private bool BRKE312 ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String AV61TFAreaTrabalho_CalculoPFinal_SelsJson ;
      private String AV83TFAreaTrabalho_VerTA_SelsJson ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV10TFAreaTrabalho_Descricao ;
      private String AV11TFAreaTrabalho_Descricao_Sel ;
      private String AV14TFContratante_CNPJ ;
      private String AV15TFContratante_CNPJ_Sel ;
      private String AV44DynamicFiltersSelector1 ;
      private String AV45AreaTrabalho_Descricao1 ;
      private String AV48DynamicFiltersSelector2 ;
      private String AV49AreaTrabalho_Descricao2 ;
      private String AV76DynamicFiltersSelector3 ;
      private String AV78AreaTrabalho_Descricao3 ;
      private String AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 ;
      private String AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ;
      private String AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 ;
      private String AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ;
      private String AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 ;
      private String AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ;
      private String AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ;
      private String AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel ;
      private String AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ;
      private String AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel ;
      private String lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ;
      private String lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ;
      private String lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ;
      private String lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ;
      private String lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ;
      private String A6AreaTrabalho_Descricao ;
      private String A12Contratante_CNPJ ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00E32_A29Contratante_Codigo ;
      private bool[] P00E32_n29Contratante_Codigo ;
      private int[] P00E32_A25Municipio_Codigo ;
      private bool[] P00E32_n25Municipio_Codigo ;
      private int[] P00E32_A335Contratante_PessoaCod ;
      private int[] P00E32_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] P00E32_n1216AreaTrabalho_OrganizacaoCod ;
      private String[] P00E32_A6AreaTrabalho_Descricao ;
      private int[] P00E32_A5AreaTrabalho_Codigo ;
      private short[] P00E32_A2081AreaTrabalho_VerTA ;
      private bool[] P00E32_n2081AreaTrabalho_VerTA ;
      private String[] P00E32_A642AreaTrabalho_CalculoPFinal ;
      private String[] P00E32_A26Municipio_Nome ;
      private String[] P00E32_A23Estado_UF ;
      private short[] P00E32_A855AreaTrabalho_DiasParaPagar ;
      private bool[] P00E32_n855AreaTrabalho_DiasParaPagar ;
      private bool[] P00E32_A834AreaTrabalho_ValidaOSFM ;
      private bool[] P00E32_n834AreaTrabalho_ValidaOSFM ;
      private String[] P00E32_A31Contratante_Telefone ;
      private String[] P00E32_A12Contratante_CNPJ ;
      private bool[] P00E32_n12Contratante_CNPJ ;
      private String[] P00E32_A9Contratante_RazaoSocial ;
      private bool[] P00E32_n9Contratante_RazaoSocial ;
      private bool[] P00E32_A72AreaTrabalho_Ativo ;
      private String[] P00E32_A1214Organizacao_Nome ;
      private bool[] P00E32_n1214Organizacao_Nome ;
      private int[] P00E33_A29Contratante_Codigo ;
      private bool[] P00E33_n29Contratante_Codigo ;
      private int[] P00E33_A25Municipio_Codigo ;
      private bool[] P00E33_n25Municipio_Codigo ;
      private int[] P00E33_A335Contratante_PessoaCod ;
      private int[] P00E33_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] P00E33_n1216AreaTrabalho_OrganizacaoCod ;
      private String[] P00E33_A9Contratante_RazaoSocial ;
      private bool[] P00E33_n9Contratante_RazaoSocial ;
      private int[] P00E33_A5AreaTrabalho_Codigo ;
      private short[] P00E33_A2081AreaTrabalho_VerTA ;
      private bool[] P00E33_n2081AreaTrabalho_VerTA ;
      private String[] P00E33_A642AreaTrabalho_CalculoPFinal ;
      private String[] P00E33_A26Municipio_Nome ;
      private String[] P00E33_A23Estado_UF ;
      private short[] P00E33_A855AreaTrabalho_DiasParaPagar ;
      private bool[] P00E33_n855AreaTrabalho_DiasParaPagar ;
      private bool[] P00E33_A834AreaTrabalho_ValidaOSFM ;
      private bool[] P00E33_n834AreaTrabalho_ValidaOSFM ;
      private String[] P00E33_A31Contratante_Telefone ;
      private String[] P00E33_A12Contratante_CNPJ ;
      private bool[] P00E33_n12Contratante_CNPJ ;
      private bool[] P00E33_A72AreaTrabalho_Ativo ;
      private String[] P00E33_A1214Organizacao_Nome ;
      private bool[] P00E33_n1214Organizacao_Nome ;
      private String[] P00E33_A6AreaTrabalho_Descricao ;
      private int[] P00E34_A29Contratante_Codigo ;
      private bool[] P00E34_n29Contratante_Codigo ;
      private int[] P00E34_A25Municipio_Codigo ;
      private bool[] P00E34_n25Municipio_Codigo ;
      private int[] P00E34_A335Contratante_PessoaCod ;
      private int[] P00E34_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] P00E34_n1216AreaTrabalho_OrganizacaoCod ;
      private String[] P00E34_A12Contratante_CNPJ ;
      private bool[] P00E34_n12Contratante_CNPJ ;
      private int[] P00E34_A5AreaTrabalho_Codigo ;
      private short[] P00E34_A2081AreaTrabalho_VerTA ;
      private bool[] P00E34_n2081AreaTrabalho_VerTA ;
      private String[] P00E34_A642AreaTrabalho_CalculoPFinal ;
      private String[] P00E34_A26Municipio_Nome ;
      private String[] P00E34_A23Estado_UF ;
      private short[] P00E34_A855AreaTrabalho_DiasParaPagar ;
      private bool[] P00E34_n855AreaTrabalho_DiasParaPagar ;
      private bool[] P00E34_A834AreaTrabalho_ValidaOSFM ;
      private bool[] P00E34_n834AreaTrabalho_ValidaOSFM ;
      private String[] P00E34_A31Contratante_Telefone ;
      private String[] P00E34_A9Contratante_RazaoSocial ;
      private bool[] P00E34_n9Contratante_RazaoSocial ;
      private bool[] P00E34_A72AreaTrabalho_Ativo ;
      private String[] P00E34_A1214Organizacao_Nome ;
      private bool[] P00E34_n1214Organizacao_Nome ;
      private String[] P00E34_A6AreaTrabalho_Descricao ;
      private int[] P00E35_A29Contratante_Codigo ;
      private bool[] P00E35_n29Contratante_Codigo ;
      private int[] P00E35_A25Municipio_Codigo ;
      private bool[] P00E35_n25Municipio_Codigo ;
      private int[] P00E35_A335Contratante_PessoaCod ;
      private int[] P00E35_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] P00E35_n1216AreaTrabalho_OrganizacaoCod ;
      private String[] P00E35_A31Contratante_Telefone ;
      private int[] P00E35_A5AreaTrabalho_Codigo ;
      private short[] P00E35_A2081AreaTrabalho_VerTA ;
      private bool[] P00E35_n2081AreaTrabalho_VerTA ;
      private String[] P00E35_A642AreaTrabalho_CalculoPFinal ;
      private String[] P00E35_A26Municipio_Nome ;
      private String[] P00E35_A23Estado_UF ;
      private short[] P00E35_A855AreaTrabalho_DiasParaPagar ;
      private bool[] P00E35_n855AreaTrabalho_DiasParaPagar ;
      private bool[] P00E35_A834AreaTrabalho_ValidaOSFM ;
      private bool[] P00E35_n834AreaTrabalho_ValidaOSFM ;
      private String[] P00E35_A12Contratante_CNPJ ;
      private bool[] P00E35_n12Contratante_CNPJ ;
      private String[] P00E35_A9Contratante_RazaoSocial ;
      private bool[] P00E35_n9Contratante_RazaoSocial ;
      private bool[] P00E35_A72AreaTrabalho_Ativo ;
      private String[] P00E35_A1214Organizacao_Nome ;
      private bool[] P00E35_n1214Organizacao_Nome ;
      private String[] P00E35_A6AreaTrabalho_Descricao ;
      private int[] P00E36_A29Contratante_Codigo ;
      private bool[] P00E36_n29Contratante_Codigo ;
      private int[] P00E36_A25Municipio_Codigo ;
      private bool[] P00E36_n25Municipio_Codigo ;
      private int[] P00E36_A335Contratante_PessoaCod ;
      private int[] P00E36_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] P00E36_n1216AreaTrabalho_OrganizacaoCod ;
      private String[] P00E36_A23Estado_UF ;
      private int[] P00E36_A5AreaTrabalho_Codigo ;
      private short[] P00E36_A2081AreaTrabalho_VerTA ;
      private bool[] P00E36_n2081AreaTrabalho_VerTA ;
      private String[] P00E36_A642AreaTrabalho_CalculoPFinal ;
      private String[] P00E36_A26Municipio_Nome ;
      private short[] P00E36_A855AreaTrabalho_DiasParaPagar ;
      private bool[] P00E36_n855AreaTrabalho_DiasParaPagar ;
      private bool[] P00E36_A834AreaTrabalho_ValidaOSFM ;
      private bool[] P00E36_n834AreaTrabalho_ValidaOSFM ;
      private String[] P00E36_A31Contratante_Telefone ;
      private String[] P00E36_A12Contratante_CNPJ ;
      private bool[] P00E36_n12Contratante_CNPJ ;
      private String[] P00E36_A9Contratante_RazaoSocial ;
      private bool[] P00E36_n9Contratante_RazaoSocial ;
      private bool[] P00E36_A72AreaTrabalho_Ativo ;
      private String[] P00E36_A1214Organizacao_Nome ;
      private bool[] P00E36_n1214Organizacao_Nome ;
      private String[] P00E36_A6AreaTrabalho_Descricao ;
      private int[] P00E37_A29Contratante_Codigo ;
      private bool[] P00E37_n29Contratante_Codigo ;
      private int[] P00E37_A25Municipio_Codigo ;
      private bool[] P00E37_n25Municipio_Codigo ;
      private int[] P00E37_A335Contratante_PessoaCod ;
      private int[] P00E37_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] P00E37_n1216AreaTrabalho_OrganizacaoCod ;
      private String[] P00E37_A26Municipio_Nome ;
      private int[] P00E37_A5AreaTrabalho_Codigo ;
      private short[] P00E37_A2081AreaTrabalho_VerTA ;
      private bool[] P00E37_n2081AreaTrabalho_VerTA ;
      private String[] P00E37_A642AreaTrabalho_CalculoPFinal ;
      private String[] P00E37_A23Estado_UF ;
      private short[] P00E37_A855AreaTrabalho_DiasParaPagar ;
      private bool[] P00E37_n855AreaTrabalho_DiasParaPagar ;
      private bool[] P00E37_A834AreaTrabalho_ValidaOSFM ;
      private bool[] P00E37_n834AreaTrabalho_ValidaOSFM ;
      private String[] P00E37_A31Contratante_Telefone ;
      private String[] P00E37_A12Contratante_CNPJ ;
      private bool[] P00E37_n12Contratante_CNPJ ;
      private String[] P00E37_A9Contratante_RazaoSocial ;
      private bool[] P00E37_n9Contratante_RazaoSocial ;
      private bool[] P00E37_A72AreaTrabalho_Ativo ;
      private String[] P00E37_A1214Organizacao_Nome ;
      private bool[] P00E37_n1214Organizacao_Nome ;
      private String[] P00E37_A6AreaTrabalho_Descricao ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV84TFAreaTrabalho_VerTA_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV124Udparg36 ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV127Udparg37 ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV130Udparg38 ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV133Udparg39 ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV136Udparg40 ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV139Udparg41 ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV62TFAreaTrabalho_CalculoPFinal_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV39GridStateDynamicFilter ;
   }

   public class getwwareatrabalhofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00E32( IGxContext context ,
                                             String A642AreaTrabalho_CalculoPFinal ,
                                             IGxCollection AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels ,
                                             short A2081AreaTrabalho_VerTA ,
                                             IGxCollection AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV124Udparg36 ,
                                             String AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 ,
                                             String AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ,
                                             short AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 ,
                                             String AV92WWAreaTrabalhoDS_4_Organizacao_nome1 ,
                                             String AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 ,
                                             bool AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 ,
                                             String AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 ,
                                             String AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ,
                                             short AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 ,
                                             String AV98WWAreaTrabalhoDS_10_Organizacao_nome2 ,
                                             String AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 ,
                                             bool AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 ,
                                             String AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 ,
                                             String AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ,
                                             short AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 ,
                                             String AV104WWAreaTrabalhoDS_16_Organizacao_nome3 ,
                                             String AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 ,
                                             String AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel ,
                                             String AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ,
                                             String AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel ,
                                             String AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ,
                                             String AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel ,
                                             String AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ,
                                             String AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel ,
                                             String AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ,
                                             short AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel ,
                                             short AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar ,
                                             short AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to ,
                                             String AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel ,
                                             String AV117WWAreaTrabalhoDS_29_Tfestado_uf ,
                                             String AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel ,
                                             String AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ,
                                             int AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels_Count ,
                                             int AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels_Count ,
                                             short AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel ,
                                             int AV9WWPContext_gxTpr_Contratante_codigo ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String A6AreaTrabalho_Descricao ,
                                             String A1214Organizacao_Nome ,
                                             bool A72AreaTrabalho_Ativo ,
                                             String A9Contratante_RazaoSocial ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             bool A834AreaTrabalho_ValidaOSFM ,
                                             short A855AreaTrabalho_DiasParaPagar ,
                                             String A23Estado_UF ,
                                             String A26Municipio_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [23] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Municipio_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T1.[AreaTrabalho_Descricao], T1.[AreaTrabalho_Codigo], T1.[AreaTrabalho_VerTA], T1.[AreaTrabalho_CalculoPFinal], T3.[Municipio_Nome], T3.[Estado_UF], T1.[AreaTrabalho_DiasParaPagar], T1.[AreaTrabalho_ValidaOSFM], T2.[Contratante_Telefone], T4.[Pessoa_Docto] AS Contratante_CNPJ, T4.[Pessoa_Nome] AS Contratante_RazaoSocial, T1.[AreaTrabalho_Ativo], T5.[Organizacao_Nome] FROM (((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) LEFT JOIN [Organizacao] T5 WITH (NOLOCK) ON T5.[Organizacao_Codigo] = T1.[AreaTrabalho_OrganizacaoCod])";
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like @lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like @lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] = @AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] = @AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_Telefone] like @lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_Telefone] = @AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_ValidaOSFM] = 1)";
            }
         }
         if ( AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_ValidaOSFM] = 0)";
            }
         }
         if ( ! (0==AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] >= @AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_DiasParaPagar] >= @AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (0==AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] <= @AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_DiasParaPagar] <= @AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWAreaTrabalhoDS_29_Tfestado_uf)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV117WWAreaTrabalhoDS_29_Tfestado_uf)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_UF] like @lV117WWAreaTrabalhoDS_29_Tfestado_uf)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_UF] = @AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] = @AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
            }
         }
         if ( AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
            }
         }
         if ( AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 0)";
            }
         }
         if ( AV9WWPContext_gxTpr_Contratante_codigo + AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV124Udparg36, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV124Udparg36, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[AreaTrabalho_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00E33( IGxContext context ,
                                             String A642AreaTrabalho_CalculoPFinal ,
                                             IGxCollection AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels ,
                                             short A2081AreaTrabalho_VerTA ,
                                             IGxCollection AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV127Udparg37 ,
                                             String AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 ,
                                             String AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ,
                                             short AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 ,
                                             String AV92WWAreaTrabalhoDS_4_Organizacao_nome1 ,
                                             String AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 ,
                                             bool AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 ,
                                             String AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 ,
                                             String AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ,
                                             short AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 ,
                                             String AV98WWAreaTrabalhoDS_10_Organizacao_nome2 ,
                                             String AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 ,
                                             bool AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 ,
                                             String AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 ,
                                             String AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ,
                                             short AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 ,
                                             String AV104WWAreaTrabalhoDS_16_Organizacao_nome3 ,
                                             String AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 ,
                                             String AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel ,
                                             String AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ,
                                             String AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel ,
                                             String AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ,
                                             String AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel ,
                                             String AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ,
                                             String AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel ,
                                             String AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ,
                                             short AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel ,
                                             short AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar ,
                                             short AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to ,
                                             String AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel ,
                                             String AV117WWAreaTrabalhoDS_29_Tfestado_uf ,
                                             String AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel ,
                                             String AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ,
                                             int AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels_Count ,
                                             int AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels_Count ,
                                             short AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel ,
                                             int AV9WWPContext_gxTpr_Contratante_codigo ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String A6AreaTrabalho_Descricao ,
                                             String A1214Organizacao_Nome ,
                                             bool A72AreaTrabalho_Ativo ,
                                             String A9Contratante_RazaoSocial ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             bool A834AreaTrabalho_ValidaOSFM ,
                                             short A855AreaTrabalho_DiasParaPagar ,
                                             String A23Estado_UF ,
                                             String A26Municipio_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [23] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Municipio_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T4.[Pessoa_Nome] AS Contratante_RazaoSocial, T1.[AreaTrabalho_Codigo], T1.[AreaTrabalho_VerTA], T1.[AreaTrabalho_CalculoPFinal], T3.[Municipio_Nome], T3.[Estado_UF], T1.[AreaTrabalho_DiasParaPagar], T1.[AreaTrabalho_ValidaOSFM], T2.[Contratante_Telefone], T4.[Pessoa_Docto] AS Contratante_CNPJ, T1.[AreaTrabalho_Ativo], T5.[Organizacao_Nome], T1.[AreaTrabalho_Descricao] FROM (((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) LEFT JOIN [Organizacao] T5 WITH (NOLOCK) ON T5.[Organizacao_Codigo] = T1.[AreaTrabalho_OrganizacaoCod])";
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like @lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like @lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] = @AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] = @AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_Telefone] like @lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_Telefone] = @AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_ValidaOSFM] = 1)";
            }
         }
         if ( AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_ValidaOSFM] = 0)";
            }
         }
         if ( ! (0==AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] >= @AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_DiasParaPagar] >= @AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (0==AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] <= @AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_DiasParaPagar] <= @AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWAreaTrabalhoDS_29_Tfestado_uf)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV117WWAreaTrabalhoDS_29_Tfestado_uf)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_UF] like @lV117WWAreaTrabalhoDS_29_Tfestado_uf)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_UF] = @AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] = @AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
            }
         }
         if ( AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
            }
         }
         if ( AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 0)";
            }
         }
         if ( AV9WWPContext_gxTpr_Contratante_codigo + AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV127Udparg37, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV127Udparg37, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00E34( IGxContext context ,
                                             String A642AreaTrabalho_CalculoPFinal ,
                                             IGxCollection AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels ,
                                             short A2081AreaTrabalho_VerTA ,
                                             IGxCollection AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV130Udparg38 ,
                                             String AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 ,
                                             String AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ,
                                             short AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 ,
                                             String AV92WWAreaTrabalhoDS_4_Organizacao_nome1 ,
                                             String AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 ,
                                             bool AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 ,
                                             String AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 ,
                                             String AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ,
                                             short AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 ,
                                             String AV98WWAreaTrabalhoDS_10_Organizacao_nome2 ,
                                             String AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 ,
                                             bool AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 ,
                                             String AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 ,
                                             String AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ,
                                             short AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 ,
                                             String AV104WWAreaTrabalhoDS_16_Organizacao_nome3 ,
                                             String AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 ,
                                             String AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel ,
                                             String AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ,
                                             String AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel ,
                                             String AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ,
                                             String AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel ,
                                             String AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ,
                                             String AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel ,
                                             String AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ,
                                             short AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel ,
                                             short AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar ,
                                             short AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to ,
                                             String AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel ,
                                             String AV117WWAreaTrabalhoDS_29_Tfestado_uf ,
                                             String AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel ,
                                             String AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ,
                                             int AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels_Count ,
                                             int AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels_Count ,
                                             short AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel ,
                                             int AV9WWPContext_gxTpr_Contratante_codigo ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String A6AreaTrabalho_Descricao ,
                                             String A1214Organizacao_Nome ,
                                             bool A72AreaTrabalho_Ativo ,
                                             String A9Contratante_RazaoSocial ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             bool A834AreaTrabalho_ValidaOSFM ,
                                             short A855AreaTrabalho_DiasParaPagar ,
                                             String A23Estado_UF ,
                                             String A26Municipio_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [23] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Municipio_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T4.[Pessoa_Docto] AS Contratante_CNPJ, T1.[AreaTrabalho_Codigo], T1.[AreaTrabalho_VerTA], T1.[AreaTrabalho_CalculoPFinal], T3.[Municipio_Nome], T3.[Estado_UF], T1.[AreaTrabalho_DiasParaPagar], T1.[AreaTrabalho_ValidaOSFM], T2.[Contratante_Telefone], T4.[Pessoa_Nome] AS Contratante_RazaoSocial, T1.[AreaTrabalho_Ativo], T5.[Organizacao_Nome], T1.[AreaTrabalho_Descricao] FROM (((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) LEFT JOIN [Organizacao] T5 WITH (NOLOCK) ON T5.[Organizacao_Codigo] = T1.[AreaTrabalho_OrganizacaoCod])";
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like @lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like @lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] = @AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] = @AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_Telefone] like @lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_Telefone] = @AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_ValidaOSFM] = 1)";
            }
         }
         if ( AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_ValidaOSFM] = 0)";
            }
         }
         if ( ! (0==AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] >= @AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_DiasParaPagar] >= @AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (0==AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] <= @AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_DiasParaPagar] <= @AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWAreaTrabalhoDS_29_Tfestado_uf)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV117WWAreaTrabalhoDS_29_Tfestado_uf)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_UF] like @lV117WWAreaTrabalhoDS_29_Tfestado_uf)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_UF] = @AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] = @AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
            }
         }
         if ( AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
            }
         }
         if ( AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 0)";
            }
         }
         if ( AV9WWPContext_gxTpr_Contratante_codigo + AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV130Udparg38, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV130Udparg38, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Docto]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00E35( IGxContext context ,
                                             String A642AreaTrabalho_CalculoPFinal ,
                                             IGxCollection AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels ,
                                             short A2081AreaTrabalho_VerTA ,
                                             IGxCollection AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV133Udparg39 ,
                                             String AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 ,
                                             String AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ,
                                             short AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 ,
                                             String AV92WWAreaTrabalhoDS_4_Organizacao_nome1 ,
                                             String AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 ,
                                             bool AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 ,
                                             String AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 ,
                                             String AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ,
                                             short AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 ,
                                             String AV98WWAreaTrabalhoDS_10_Organizacao_nome2 ,
                                             String AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 ,
                                             bool AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 ,
                                             String AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 ,
                                             String AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ,
                                             short AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 ,
                                             String AV104WWAreaTrabalhoDS_16_Organizacao_nome3 ,
                                             String AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 ,
                                             String AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel ,
                                             String AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ,
                                             String AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel ,
                                             String AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ,
                                             String AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel ,
                                             String AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ,
                                             String AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel ,
                                             String AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ,
                                             short AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel ,
                                             short AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar ,
                                             short AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to ,
                                             String AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel ,
                                             String AV117WWAreaTrabalhoDS_29_Tfestado_uf ,
                                             String AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel ,
                                             String AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ,
                                             int AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels_Count ,
                                             int AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels_Count ,
                                             short AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel ,
                                             int AV9WWPContext_gxTpr_Contratante_codigo ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String A6AreaTrabalho_Descricao ,
                                             String A1214Organizacao_Nome ,
                                             bool A72AreaTrabalho_Ativo ,
                                             String A9Contratante_RazaoSocial ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             bool A834AreaTrabalho_ValidaOSFM ,
                                             short A855AreaTrabalho_DiasParaPagar ,
                                             String A23Estado_UF ,
                                             String A26Municipio_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [23] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Municipio_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T2.[Contratante_Telefone], T1.[AreaTrabalho_Codigo], T1.[AreaTrabalho_VerTA], T1.[AreaTrabalho_CalculoPFinal], T3.[Municipio_Nome], T3.[Estado_UF], T1.[AreaTrabalho_DiasParaPagar], T1.[AreaTrabalho_ValidaOSFM], T4.[Pessoa_Docto] AS Contratante_CNPJ, T4.[Pessoa_Nome] AS Contratante_RazaoSocial, T1.[AreaTrabalho_Ativo], T5.[Organizacao_Nome], T1.[AreaTrabalho_Descricao] FROM (((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) LEFT JOIN [Organizacao] T5 WITH (NOLOCK) ON T5.[Organizacao_Codigo] = T1.[AreaTrabalho_OrganizacaoCod])";
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like @lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like @lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] = @AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] = @AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_Telefone] like @lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_Telefone] = @AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_ValidaOSFM] = 1)";
            }
         }
         if ( AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_ValidaOSFM] = 0)";
            }
         }
         if ( ! (0==AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] >= @AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_DiasParaPagar] >= @AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( ! (0==AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] <= @AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_DiasParaPagar] <= @AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWAreaTrabalhoDS_29_Tfestado_uf)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV117WWAreaTrabalhoDS_29_Tfestado_uf)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_UF] like @lV117WWAreaTrabalhoDS_29_Tfestado_uf)";
            }
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_UF] = @AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)";
            }
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] = @AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
            }
         }
         if ( AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
            }
         }
         if ( AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 0)";
            }
         }
         if ( AV9WWPContext_gxTpr_Contratante_codigo + AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV133Udparg39, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV133Udparg39, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contratante_Telefone]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00E36( IGxContext context ,
                                             String A642AreaTrabalho_CalculoPFinal ,
                                             IGxCollection AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels ,
                                             short A2081AreaTrabalho_VerTA ,
                                             IGxCollection AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV136Udparg40 ,
                                             String AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 ,
                                             String AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ,
                                             short AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 ,
                                             String AV92WWAreaTrabalhoDS_4_Organizacao_nome1 ,
                                             String AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 ,
                                             bool AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 ,
                                             String AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 ,
                                             String AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ,
                                             short AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 ,
                                             String AV98WWAreaTrabalhoDS_10_Organizacao_nome2 ,
                                             String AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 ,
                                             bool AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 ,
                                             String AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 ,
                                             String AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ,
                                             short AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 ,
                                             String AV104WWAreaTrabalhoDS_16_Organizacao_nome3 ,
                                             String AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 ,
                                             String AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel ,
                                             String AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ,
                                             String AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel ,
                                             String AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ,
                                             String AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel ,
                                             String AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ,
                                             String AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel ,
                                             String AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ,
                                             short AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel ,
                                             short AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar ,
                                             short AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to ,
                                             String AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel ,
                                             String AV117WWAreaTrabalhoDS_29_Tfestado_uf ,
                                             String AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel ,
                                             String AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ,
                                             int AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels_Count ,
                                             int AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels_Count ,
                                             short AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel ,
                                             int AV9WWPContext_gxTpr_Contratante_codigo ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String A6AreaTrabalho_Descricao ,
                                             String A1214Organizacao_Nome ,
                                             bool A72AreaTrabalho_Ativo ,
                                             String A9Contratante_RazaoSocial ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             bool A834AreaTrabalho_ValidaOSFM ,
                                             short A855AreaTrabalho_DiasParaPagar ,
                                             String A23Estado_UF ,
                                             String A26Municipio_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [23] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Municipio_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T3.[Estado_UF], T1.[AreaTrabalho_Codigo], T1.[AreaTrabalho_VerTA], T1.[AreaTrabalho_CalculoPFinal], T3.[Municipio_Nome], T1.[AreaTrabalho_DiasParaPagar], T1.[AreaTrabalho_ValidaOSFM], T2.[Contratante_Telefone], T4.[Pessoa_Docto] AS Contratante_CNPJ, T4.[Pessoa_Nome] AS Contratante_RazaoSocial, T1.[AreaTrabalho_Ativo], T5.[Organizacao_Nome], T1.[AreaTrabalho_Descricao] FROM (((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) LEFT JOIN [Organizacao] T5 WITH (NOLOCK) ON T5.[Organizacao_Codigo] = T1.[AreaTrabalho_OrganizacaoCod])";
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)";
            }
         }
         else
         {
            GXv_int9[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
         }
         else
         {
            GXv_int9[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
         }
         else
         {
            GXv_int9[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)";
            }
         }
         else
         {
            GXv_int9[3] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
         }
         else
         {
            GXv_int9[4] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
         }
         else
         {
            GXv_int9[5] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)";
            }
         }
         else
         {
            GXv_int9[6] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like @lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like @lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)";
            }
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] = @AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] = @AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)";
            }
         }
         else
         {
            GXv_int9[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)";
            }
         }
         else
         {
            GXv_int9[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)";
            }
         }
         else
         {
            GXv_int9[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)";
            }
         }
         else
         {
            GXv_int9[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)";
            }
         }
         else
         {
            GXv_int9[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_Telefone] like @lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)";
            }
         }
         else
         {
            GXv_int9[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_Telefone] = @AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)";
            }
         }
         else
         {
            GXv_int9[16] = 1;
         }
         if ( AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_ValidaOSFM] = 1)";
            }
         }
         if ( AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_ValidaOSFM] = 0)";
            }
         }
         if ( ! (0==AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] >= @AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_DiasParaPagar] >= @AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar)";
            }
         }
         else
         {
            GXv_int9[17] = 1;
         }
         if ( ! (0==AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] <= @AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_DiasParaPagar] <= @AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to)";
            }
         }
         else
         {
            GXv_int9[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWAreaTrabalhoDS_29_Tfestado_uf)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV117WWAreaTrabalhoDS_29_Tfestado_uf)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_UF] like @lV117WWAreaTrabalhoDS_29_Tfestado_uf)";
            }
         }
         else
         {
            GXv_int9[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_UF] = @AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)";
            }
         }
         else
         {
            GXv_int9[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int9[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] = @AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int9[22] = 1;
         }
         if ( AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
            }
         }
         if ( AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
            }
         }
         if ( AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 0)";
            }
         }
         if ( AV9WWPContext_gxTpr_Contratante_codigo + AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV136Udparg40, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV136Udparg40, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Estado_UF]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      protected Object[] conditional_P00E37( IGxContext context ,
                                             String A642AreaTrabalho_CalculoPFinal ,
                                             IGxCollection AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels ,
                                             short A2081AreaTrabalho_VerTA ,
                                             IGxCollection AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV139Udparg41 ,
                                             String AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1 ,
                                             String AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1 ,
                                             short AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 ,
                                             String AV92WWAreaTrabalhoDS_4_Organizacao_nome1 ,
                                             String AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1 ,
                                             bool AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 ,
                                             String AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2 ,
                                             String AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2 ,
                                             short AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 ,
                                             String AV98WWAreaTrabalhoDS_10_Organizacao_nome2 ,
                                             String AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2 ,
                                             bool AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 ,
                                             String AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3 ,
                                             String AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3 ,
                                             short AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 ,
                                             String AV104WWAreaTrabalhoDS_16_Organizacao_nome3 ,
                                             String AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3 ,
                                             String AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel ,
                                             String AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao ,
                                             String AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel ,
                                             String AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial ,
                                             String AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel ,
                                             String AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj ,
                                             String AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel ,
                                             String AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone ,
                                             short AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel ,
                                             short AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar ,
                                             short AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to ,
                                             String AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel ,
                                             String AV117WWAreaTrabalhoDS_29_Tfestado_uf ,
                                             String AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel ,
                                             String AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome ,
                                             int AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels_Count ,
                                             int AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels_Count ,
                                             short AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel ,
                                             int AV9WWPContext_gxTpr_Contratante_codigo ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String A6AreaTrabalho_Descricao ,
                                             String A1214Organizacao_Nome ,
                                             bool A72AreaTrabalho_Ativo ,
                                             String A9Contratante_RazaoSocial ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             bool A834AreaTrabalho_ValidaOSFM ,
                                             short A855AreaTrabalho_DiasParaPagar ,
                                             String A23Estado_UF ,
                                             String A26Municipio_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int11 ;
         GXv_int11 = new short [23] ;
         Object[] GXv_Object12 ;
         GXv_Object12 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Municipio_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T3.[Municipio_Nome], T1.[AreaTrabalho_Codigo], T1.[AreaTrabalho_VerTA], T1.[AreaTrabalho_CalculoPFinal], T3.[Estado_UF], T1.[AreaTrabalho_DiasParaPagar], T1.[AreaTrabalho_ValidaOSFM], T2.[Contratante_Telefone], T4.[Pessoa_Docto] AS Contratante_CNPJ, T4.[Pessoa_Nome] AS Contratante_RazaoSocial, T1.[AreaTrabalho_Ativo], T5.[Organizacao_Nome], T1.[AreaTrabalho_Descricao] FROM (((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) LEFT JOIN [Organizacao] T5 WITH (NOLOCK) ON T5.[Organizacao_Codigo] = T1.[AreaTrabalho_OrganizacaoCod])";
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1)";
            }
         }
         else
         {
            GXv_int11[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
         }
         else
         {
            GXv_int11[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV90WWAreaTrabalhoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWAreaTrabalhoDS_4_Organizacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV92WWAreaTrabalhoDS_4_Organizacao_nome1)";
            }
         }
         else
         {
            GXv_int11[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( ( StringUtil.StrCmp(AV89WWAreaTrabalhoDS_1_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV93WWAreaTrabalhoDS_5_Areatrabalho_ativo1, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2)";
            }
         }
         else
         {
            GXv_int11[3] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
         }
         else
         {
            GXv_int11[4] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV96WWAreaTrabalhoDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAreaTrabalhoDS_10_Organizacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV98WWAreaTrabalhoDS_10_Organizacao_nome2)";
            }
         }
         else
         {
            GXv_int11[5] = 1;
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV94WWAreaTrabalhoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAreaTrabalhoDS_7_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV99WWAreaTrabalhoDS_11_Areatrabalho_ativo2, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3)";
            }
         }
         else
         {
            GXv_int11[6] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
         }
         else
         {
            GXv_int11[7] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV102WWAreaTrabalhoDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAreaTrabalhoDS_16_Organizacao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Organizacao_Nome] like '%' + @lV104WWAreaTrabalhoDS_16_Organizacao_nome3)";
            }
         }
         else
         {
            GXv_int11[8] = 1;
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV100WWAreaTrabalhoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWAreaTrabalhoDS_13_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV105WWAreaTrabalhoDS_17_Areatrabalho_ativo3, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like @lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like @lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao)";
            }
         }
         else
         {
            GXv_int11[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] = @AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] = @AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel)";
            }
         }
         else
         {
            GXv_int11[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial)";
            }
         }
         else
         {
            GXv_int11[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel)";
            }
         }
         else
         {
            GXv_int11[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj)";
            }
         }
         else
         {
            GXv_int11[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel)";
            }
         }
         else
         {
            GXv_int11[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_Telefone] like @lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone)";
            }
         }
         else
         {
            GXv_int11[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratante_Telefone] = @AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel)";
            }
         }
         else
         {
            GXv_int11[16] = 1;
         }
         if ( AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_ValidaOSFM] = 1)";
            }
         }
         if ( AV114WWAreaTrabalhoDS_26_Tfareatrabalho_validaosfm_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_ValidaOSFM] = 0)";
            }
         }
         if ( ! (0==AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] >= @AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_DiasParaPagar] >= @AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar)";
            }
         }
         else
         {
            GXv_int11[17] = 1;
         }
         if ( ! (0==AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] <= @AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_DiasParaPagar] <= @AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to)";
            }
         }
         else
         {
            GXv_int11[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWAreaTrabalhoDS_29_Tfestado_uf)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV117WWAreaTrabalhoDS_29_Tfestado_uf)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_UF] like @lV117WWAreaTrabalhoDS_29_Tfestado_uf)";
            }
         }
         else
         {
            GXv_int11[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_UF] = @AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel)";
            }
         }
         else
         {
            GXv_int11[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int11[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] = @AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int11[22] = 1;
         }
         if ( AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV121WWAreaTrabalhoDS_33_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
            }
         }
         if ( AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV122WWAreaTrabalhoDS_34_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
            }
         }
         if ( AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 1)";
            }
         }
         if ( AV123WWAreaTrabalhoDS_35_Tfareatrabalho_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Ativo] = 0)";
            }
         }
         if ( AV9WWPContext_gxTpr_Contratante_codigo + AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV139Udparg41, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV139Udparg41, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Municipio_Nome]";
         GXv_Object12[0] = scmdbuf;
         GXv_Object12[1] = GXv_int11;
         return GXv_Object12 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00E32(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (short)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (bool)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (short)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] );
               case 1 :
                     return conditional_P00E33(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (short)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (bool)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (short)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] );
               case 2 :
                     return conditional_P00E34(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (short)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (bool)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (short)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] );
               case 3 :
                     return conditional_P00E35(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (short)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (bool)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (short)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] );
               case 4 :
                     return conditional_P00E36(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (short)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (bool)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (short)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] );
               case 5 :
                     return conditional_P00E37(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (short)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (bool)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (short)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00E32 ;
          prmP00E32 = new Object[] {
          new Object[] {"@lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV92WWAreaTrabalhoDS_4_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWAreaTrabalhoDS_4_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWAreaTrabalhoDS_10_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWAreaTrabalhoDS_10_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV104WWAreaTrabalhoDS_16_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWAreaTrabalhoDS_16_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV117WWAreaTrabalhoDS_29_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00E33 ;
          prmP00E33 = new Object[] {
          new Object[] {"@lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV92WWAreaTrabalhoDS_4_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWAreaTrabalhoDS_4_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWAreaTrabalhoDS_10_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWAreaTrabalhoDS_10_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV104WWAreaTrabalhoDS_16_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWAreaTrabalhoDS_16_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV117WWAreaTrabalhoDS_29_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00E34 ;
          prmP00E34 = new Object[] {
          new Object[] {"@lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV92WWAreaTrabalhoDS_4_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWAreaTrabalhoDS_4_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWAreaTrabalhoDS_10_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWAreaTrabalhoDS_10_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV104WWAreaTrabalhoDS_16_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWAreaTrabalhoDS_16_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV117WWAreaTrabalhoDS_29_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00E35 ;
          prmP00E35 = new Object[] {
          new Object[] {"@lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV92WWAreaTrabalhoDS_4_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWAreaTrabalhoDS_4_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWAreaTrabalhoDS_10_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWAreaTrabalhoDS_10_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV104WWAreaTrabalhoDS_16_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWAreaTrabalhoDS_16_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV117WWAreaTrabalhoDS_29_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00E36 ;
          prmP00E36 = new Object[] {
          new Object[] {"@lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV92WWAreaTrabalhoDS_4_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWAreaTrabalhoDS_4_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWAreaTrabalhoDS_10_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWAreaTrabalhoDS_10_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV104WWAreaTrabalhoDS_16_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWAreaTrabalhoDS_16_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV117WWAreaTrabalhoDS_29_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00E37 ;
          prmP00E37 = new Object[] {
          new Object[] {"@lV91WWAreaTrabalhoDS_3_Areatrabalho_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV92WWAreaTrabalhoDS_4_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWAreaTrabalhoDS_4_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWAreaTrabalhoDS_9_Areatrabalho_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWAreaTrabalhoDS_10_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWAreaTrabalhoDS_10_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWAreaTrabalhoDS_15_Areatrabalho_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV104WWAreaTrabalhoDS_16_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWAreaTrabalhoDS_16_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV106WWAreaTrabalhoDS_18_Tfareatrabalho_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV107WWAreaTrabalhoDS_19_Tfareatrabalho_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV108WWAreaTrabalhoDS_20_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV109WWAreaTrabalhoDS_21_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV110WWAreaTrabalhoDS_22_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV111WWAreaTrabalhoDS_23_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV112WWAreaTrabalhoDS_24_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV113WWAreaTrabalhoDS_25_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV115WWAreaTrabalhoDS_27_Tfareatrabalho_diasparapagar",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV116WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV117WWAreaTrabalhoDS_29_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV118WWAreaTrabalhoDS_30_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV119WWAreaTrabalhoDS_31_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV120WWAreaTrabalhoDS_32_Tfmunicipio_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00E32", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00E32,100,0,true,false )
             ,new CursorDef("P00E33", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00E33,100,0,true,false )
             ,new CursorDef("P00E34", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00E34,100,0,true,false )
             ,new CursorDef("P00E35", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00E35,100,0,true,false )
             ,new CursorDef("P00E36", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00E36,100,0,true,false )
             ,new CursorDef("P00E37", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00E37,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 2) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 2) ;
                ((short[]) buf[14])[0] = rslt.getShort(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((bool[]) buf[16])[0] = rslt.getBool(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((String[]) buf[18])[0] = rslt.getString(13, 20) ;
                ((String[]) buf[19])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((String[]) buf[21])[0] = rslt.getString(15, 100) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((bool[]) buf[23])[0] = rslt.getBool(16) ;
                ((String[]) buf[24])[0] = rslt.getString(17, 50) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(17);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 2) ;
                ((String[]) buf[13])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[14])[0] = rslt.getString(10, 2) ;
                ((short[]) buf[15])[0] = rslt.getShort(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((bool[]) buf[17])[0] = rslt.getBool(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((String[]) buf[19])[0] = rslt.getString(13, 20) ;
                ((String[]) buf[20])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((bool[]) buf[22])[0] = rslt.getBool(15) ;
                ((String[]) buf[23])[0] = rslt.getString(16, 50) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((String[]) buf[25])[0] = rslt.getVarchar(17) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 2) ;
                ((String[]) buf[13])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[14])[0] = rslt.getString(10, 2) ;
                ((short[]) buf[15])[0] = rslt.getShort(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((bool[]) buf[17])[0] = rslt.getBool(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((String[]) buf[19])[0] = rslt.getString(13, 20) ;
                ((String[]) buf[20])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((bool[]) buf[22])[0] = rslt.getBool(15) ;
                ((String[]) buf[23])[0] = rslt.getString(16, 50) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((String[]) buf[25])[0] = rslt.getVarchar(17) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 20) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 2) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 2) ;
                ((short[]) buf[14])[0] = rslt.getShort(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((bool[]) buf[16])[0] = rslt.getBool(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((String[]) buf[18])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((String[]) buf[20])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((bool[]) buf[22])[0] = rslt.getBool(15) ;
                ((String[]) buf[23])[0] = rslt.getString(16, 50) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((String[]) buf[25])[0] = rslt.getVarchar(17) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 2) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 2) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((short[]) buf[13])[0] = rslt.getShort(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((bool[]) buf[15])[0] = rslt.getBool(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 20) ;
                ((String[]) buf[18])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((String[]) buf[20])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((bool[]) buf[22])[0] = rslt.getBool(15) ;
                ((String[]) buf[23])[0] = rslt.getString(16, 50) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((String[]) buf[25])[0] = rslt.getVarchar(17) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 2) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 2) ;
                ((short[]) buf[13])[0] = rslt.getShort(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((bool[]) buf[15])[0] = rslt.getBool(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 20) ;
                ((String[]) buf[18])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((String[]) buf[20])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((bool[]) buf[22])[0] = rslt.getBool(15) ;
                ((String[]) buf[23])[0] = rslt.getString(16, 50) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((String[]) buf[25])[0] = rslt.getVarchar(17) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwareatrabalhofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwareatrabalhofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwareatrabalhofilterdata") )
          {
             return  ;
          }
          getwwareatrabalhofilterdata worker = new getwwareatrabalhofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
